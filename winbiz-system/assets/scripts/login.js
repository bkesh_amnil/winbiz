var Login = function () {


    return {

        //main function to initiate the module
        init: function () {

            jQuery('#forget-password').click(function () {
                jQuery('#form_login').hide();
                jQuery('#forgotform').show(200);
            });

            jQuery('#forget-btn').click(function () {
                jQuery('#forgotform').submit();
                jQuery('#form_login').slideDown(200);
                jQuery('#forgotform').slideUp(200);
            });
        }

    };

}();