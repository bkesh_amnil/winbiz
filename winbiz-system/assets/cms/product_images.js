$('.imageList li').live("mouseover", function(){
  $(this).stop().animate({
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0
  }, 300);
  var obj = $(this);
  $(this).find('img').stop().animate({
    opacity: 0.85,
  }, 300, function (){
    obj.find('.actions').slideDown(200);
  });
}).live("mouseout", function(){
  $(this).stop().animate({
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0
  }, 100);
  var obj = $(this);
  $(this).find('img').stop().animate({
    opacity: 1,
  }, 100, function (){
    obj.find('.actions').slideUp(200);  
  });
});
$('.actionStatusYes').live("click", function(){
  var obj = $(this);
  var id = $(this).attr('rel');
  $.ajax({
    type : 'POST',
    url : '<?php echo site_url('products/change_image_status/no') ?>/'+id,
    dataType : 'html',
    success: function(data){
      if(data == 'success')
        obj.addClass('actionStatusNo').removeClass('actionStatusYes');
      else 
        alert("Error in changing status.");
    },
    error : function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Error in changing status");
    }
  });
})
$('.actionStatusNo').live("click", function(){
  var obj = $(this);
  var id = $(this).attr('rel');
  $.ajax({
    type : 'POST',
    url : '<?php echo site_url('gallery/change_image_status/yes') ?>/'+id,
    dataType : 'html',
    success: function(data){
      if(data == 'success')
        obj.addClass('actionStatusYes').removeClass('actionStatusNo');
      else 
        alert("Error in changing status.");
    },
    error : function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Error in changing status.");
    }
  });
})
$('.actionDeleteTemp').live("click", function(){
  var file = $(this).attr('rel');
  var li = $(this).parent('div').parent('li');
  li.fadeOut(300, function(){
    li.remove();
    $.ajax({
      type : 'POST',
      url : '<?php echo site_url('gallery/deleteImages?file=') ?>'+file,
      dataType : 'html',
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Error Deleting File");
      }
    });
  })
})
$('.actionDelete').live("click", function(){
  var id = $(this).attr('rel');
  var li = $(this).parent('div').parent('li');
  li.fadeOut(300, function(){
    li.remove();
    $.ajax({
      type : 'POST',
      url : '<?php echo site_url('gallery/deleteImages?id=') ?>'+id,
      dataType : 'html',
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Error Deleting File");
      }
    }); 
  })
})