// JavaScript Document
$(document).ready(function(e) {
	var div_container = $('div.field_container').html();
	
    $('a.add_field').click(function(e) {
        $('div.inner_container:last').after(div_container);
		$('a.add_field:last').html('[Remove]').addClass('remove_field').removeClass('add_field');
		return false;
    });
	
	$('a.remove_field').live('click', function(){
		$(this).parents('div.inner_container').remove();
		return false;
	});
	
	$('.field_type').live('change', function(){
		if($(this).find('option:selected').attr('multiple_values') == 1) {
			$(this).siblings('.add_values').show();
		} else {
			$(this).siblings('.add_values').hide();
			$(this).parents('.inner_container').find('.div_values').html('');
		}
	});
	
	$('.add_values').live('click', function(){
		$(this).parents('.inner_container').find('.div_values').append('<span>Display Text : <input type="text" class="display_text" name="display_text[]" /></span>');
		return false;
	});
	
	$('#btnSave').click(function(e) {
		$('div.field_container div.inner_container').each(function(index, element) {
            $(this).find('input.display_text').attr('name', 'display_text[' + index + '][]');
			$(this).find('input.field_value').attr('name', 'field_value[' + index + '][]');
			$(this).find('input.validation_rule').attr('name', 'validation[' + index + '][]');
        });    

		var blnSubmit = true;
		$('.field_label').each(function(index, element) {			
            if($(this).val() == '') {
				blnSubmit = false;
				alert('This is required field.');
				$(this).focus();
				return false;
			}
        });
		
		if( ! blnSubmit) {
			return false;
		}
    });
	
	//deleting form attributes
	$('.field_remove').click(function(){
		var $this = $(this);
		$.ajax({
			url:'./form_fields/remove_field',
			type:'POST',
			data: {'field_id' : $(this).attr('rel')},
			success:function(msg){
				if(msg == 'success')
				{
					$this.parents('tr').fadeOut(function(){
						$(this).remove() ;
					});
				}
				else 
				{
					alert('Error in removing field.');
				}					
			}, 
			failure: function(){
				alert('Error in removing field.');
			}
		});
		return false;
	});
	
	//saving fields
	$('.field_save').click(function(){
		var $this = $(this);
		var field_id = $(this).attr('rel')

		$.ajax({
			url:'./form_fields/save_field',
			type:'POST',
			data: $('tr.row_' + field_id + ' input, tr.row_' + field_id + ' select') .serialize(),
			success:function(msg){
				if(msg == 'success')
				{
					alert('Data updated.');
					window.location = document.URL;
				}
				else 
				{
					alert(msg);
					$this.parents('tr').find('.field_label').focus();
				}					
			}, 
			failure: function(){
				alert('Error in saving field.');
			}
		});
		return false;
	});
	
	$('.remove_cur_value').click(function(e) {
		var $this = $(this);
		var id = $(this).attr('rel')

		$.ajax({
			url:'./form_fields/remove_cur_value',
			type:'POST',
			data: {'id' : id},
			success:function(msg){
				if(msg == 'success')
				{
					$this.parents('div.cur_value').fadeOut(function(){
						$this.parents('div.cur_value').remove();
					})
				}
				else 
				{
					alert('Error in removing data.');
				}					
			}, 
			failure: function(){
				alert('Error in removing data.');
			}
		});
        return false;
    });
});