/* when the DOM is ready */
jQuery(document).ready(function() {
	/* grab important elements */
	var sortInput = jQuery('#selectedBanners');
	var list = jQuery('.bannerList');
	/* create requesting function to avoid duplicate code */
	
	/* worker function */
	var fnSubmit = function(save) {
		var sortOrder = [];
		list.children('li').each(function(){
			if($(this).hasClass('selectedRow'))
				sortOrder.push(jQuery(this).data('id'));
		});
		sortInput.val(sortOrder.join(','));
		//console.log(sortInput.val());
	};
	/* store values */
	list.children('li').each(function() {
		var li = jQuery(this);
		li.data('id',li.attr('title')).attr('title','');
	});
	/* sortables */
	list.sortable({
		opacity: 0.7,
		update: function() {
			fnSubmit(1);
		}
	});
	list.disableSelection();	
	
	function attach_drag()
	{
		$('.div_main_content').each(function(index, element) {
			$(this).find('.selectedValue').addClass('selValue_' + index);
			$(this).find('.contentListModules').addClass('ListModules_' + index);
			
            /* grab important elements */
			var sortInput = jQuery('.selValue_' + index);
			var list = jQuery('.ListModules_' + index);
			/* create requesting function to avoid duplicate code */
			
			/* worker function */
			var fnSubmit = function(save) {
				var sortOrder = [];
				list.children('li').each(function(){
					if($(this).hasClass('selectedRow'))
						sortOrder.push(jQuery(this).attr('title'));
				});
				sortInput.val(sortOrder.join(','));
				//console.log(sortInput.val());
			};
			/* store values */
			list.children('li').each(function() {
				var li = jQuery(this);
				//li.data('id',li.attr('title')).attr('title','');
			});
			/* sortables */
			list.sortable({
				opacity: 0.7,
				update: function() {
					fnSubmit(1);
				}
			});
			list.disableSelection();	
        });
		
	}
	
	attach_drag();
});
