// JavaScript Document
$(document).ready(function(e) {		
	$('#site_id').change(function(){
		var siteId = parseInt($(this).val());	
		if(siteId > 0){
			$.ajax({
				type : 'POST',
				url : '<?php echo site_url(); ?>ajax/getSiteMenuTypes/',
				dataType : 'html',
				data: {"to": "getSiteMenuTypes", "siteId": siteId}, 
				success : function(data){
					$('#menuType').val('0').html(data);
					$('#menuType').change();
					$('#side_menu').val('0').html(data);
					$('#side_menu').change();
					$('#footer_menu').val('0').html(data);
					$('#footer_menu').change();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//$("#list"+id).html('<center>Unknown Error Occurred</center>');
				}
			});	
			
			$.ajax({
				type : 'POST',
				url : '<?php echo site_url(); ?>ajax/get_modules/',
				dataType : 'html',
				data: {"site_id": siteId, "output" : "select"}, 
				success : function(data){
					$('.module_id').html(data);
					$('.module_id').change();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//$("#list"+id).html('<center>Unknown Error Occurred</center>');
				}
			});	
		}
	});
	
	$('#btn_submit').click(function(e) {
		$('#form1').append('<input type="hidden" name="add" value="yes" />');
		$('#form1').submit();
	});
	
	$('.add_module').click(function(e){
		var cloned = $('.div_main_content:eq(0)').clone();
		var length = parseInt($(this).attr('length'));
		var newLength = length + 1;
		var hiddenElm = "";
		$(this).attr('length', newLength);
		cloned.find('select:first').attr('name', "module_id["+ length +"]").val("");
		cloned.find('select:last').attr('name', "category_id["+ length +"]").val("");
		cloned.find('.div_module_detail').html("");
		cloned.attr("position", length);
		cloned.find('input[type=hidden]').attr('name', "selectedModulesIds["+ length +"]").attr('class', "selectedValue selectedModulesIds_"+length).val("");
		cloned.find('input[type=text]').attr('name', "module_title["+ length +"]").val("");
		cloned.find('.addRemove').text("[ Remove ]").addClass("remove_module");
		$(cloned).appendTo('#moduleSettings');
		return false;
	});
	
	$('.remove_module').live('click', function(){
		if(confirm("Are you sure you are removing the selected module?")){
			$(this).parents('.div_main_content').remove();
		}
		return false;
	});
	
	$('.module_id, .category_id').live('change', function(e) {
		var element = $(this);
		var site_id = parseInt($('#site_id').val());	
		var moduleId = element.parents('.div_main_content').find('.module_id').val();
		var categoryId = element.parents('.div_main_content').find('.category_id').val();
		var selecteds = element.parents('.div_main_content').find('.selectedValue').val(); 
		if(moduleId == "" || moduleId == "0"){
			element.parents('.div_main_content').find('.div_module_detail').html("");
		}else{
			var position = $(this).parents('.div_main_content').attr('position');
			element.parents('.div_main_content').find('.div_module_detail').html("Loading.... Please Wait!!");
			$.ajax({
				type : 'POST',
				url : '<?php echo site_url(); ?>/ajax/getModuleContents/'+moduleId+'/',
				dataType : 'html',
				data: {"to": "getModuleContents", "siteId":site_id, "moduleId": moduleId, "position": position, "category_id":categoryId, "selecteds" : selecteds}, 
				success : function(data){
					element.parents('.div_main_content').find('.div_module_detail').html(data);
					checkClickedModules(element.parents('.div_main_content'));
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//$("#list"+id).html('<center>Unknown Error Occurred</center>');
				}
			});
		}
		
	});		
	$('.selectAllWithinCategory').live('click', function(){
		var parentElm = $(this).parent('li');
		if($(this).is(':checked')){
			$(this).parents('ul').find('li').each(function(){
				if(!$(this).find('input[type=checkbox]').hasClass('selectAllWithinCategory')){
					$(this).hide();	
				}
			})	
		}else{
			$(this).parents('ul').find('li').each(function(){
				//if(!$(this).find('input[type=checkbox]').hasClass('selectAllWithinCategory')){
					$(this).show();	
				//}
			})
		}
	})
	$('#menu_link_type').change(function(){
		var valSelected = $(this).val();
		$('.setingsRow').hide('fast', function(){ 
			$('.'+valSelected).show();
		});
	});
	$('#menu_link_type').change();
	
	$('#side_menu').change(function(){
		var typeId = $(this).val();
		if(typeId == ""){
			$("#sideMenuSettings").html('');
		}else{
			$("#sideMenuSettings").html("Loading.... Please Wait!!");
			$.ajax({
				type : 'POST',
				url : '<?php echo site_url(); ?>ajax/getMenu/'+typeId,
				dataType : 'html',
				data: {"to": "GetMenu", "for":"side"}, 
				success : function(data){
					$("#sideMenuSettings").html('<div class="menuListings">'+data+'</div>');
					$("#sideMenuSettings").find("input, select, textarea").each(function(){
						$.uniform.update($(this));	
					})
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//$("#list"+id).html('<center>Unknown Error Occurred</center>');
				}
			});
		}
	})
	$('#footer_menu').change(function(){
		var typeId = $(this).val();
		if(typeId == ""){
			$("#footerMenuSettings").html('');
		}else{
			$("#footerMenuSettings").html("Loading.... Please Wait!!");
			$.ajax({
				type : 'POST',
				url : '<?php echo site_url(); ?>ajax/getMenu/'+typeId,
				dataType : 'html',
				data: {"to": "GetMenu", "for":"footer"}, 
				success : function(data){
					$("#footerMenuSettings").html('<div class="bannerListings">'+data+'</div>', function(){
						alert("Done");	
					});
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//$("#list"+id).html('<center>Unknown Error Occurred</center>');
				}
			});
		}
	})
	$('#display_banner').change(function(){
		if($(this).val() == "1"){
			$("#bannerSettings").fadeIn();				
		}else{
			$('#bannerSettings').fadeOut();
		}
	});
	
	$('.bannerListings').find('li').live('click', function(){
		var selectedId = $(this).attr('rel');
		var checkedIds = "";
		if($(this).hasClass('selectedRow')){
			$(this).removeClass('selectedRow');	
		}else{
			$(this).addClass('selectedRow');	
		}
		$(this).parents('ul').find('li').each(function(){
			if($(this).hasClass('selectedRow')){
				if(checkedIds == ""){
					checkedIds = $(this).attr('rel');	
				}else{
					checkedIds = checkedIds + "," + $(this).attr('rel');	
				}
			}
		})
		$('#selectedBanners').val(checkedIds);
	})
	$('#menuType').change(function(){
		var menuType = $(this).val();
		$("#parentList").html("Loading.... Please Wait!!");
		$.ajax({
			type : 'POST',
			url : '<?php echo site_url('/ajax/getAvailableMenus/'); ?>',
			dataType : 'html',
			data: {"to": "GetAvailableMenus", "siteId": "<?php echo $siteId; ?>", "menuType": menuType}, 
			success : function(data){
				$("#parentList").html(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				//$("#list"+id).html('<center>Unknown Error Occurred</center>');
			}
		});
	})
	$('#sideMenuSettings').find('input[type=checkbox]').live('click',function(){ 
		var obj = $(this);
		if(obj.is(":checked")){
			boxchecked = true; 
		}else{
			boxchecked = false;
		}
		var parentLi = obj.parent("div").parent("li");
		parentLi.find("ul > li").find('input[type=checkbox]').attr("checked",boxchecked);
		checkSideMenuIds();
	});
	$('#footerMenuSettings').find('input[type=checkbox]').live('click',function(){ 
		checkFooterMenuIds();
	});
	$('.div_main_content').find('input[type=checkbox]').live('click',function(){ 
		var checkedIds = "";
		var elm = $(this).parents('.div_main_content');
		var position = elm.attr('position');
		elm.find('input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if($(this).val() != "true"){
					$(this).parents('li').addClass('selectedRow');
					if(checkedIds == ""){
						checkedIds = $(this).val();	
					}else{
						checkedIds = checkedIds + "," + $(this).val();	
					}
				}
			}else{
				$(this).parents('li').removeClass('selectedRow');//$(this).remove();	
			}
		})
		$('.selectedModulesIds_'+position).val(checkedIds);
	});
	$('.menuListRow').live('click', function(evnt){
		var clickedElm = $(this);
		var target = evnt.target.nodeName;
		if(target == "INPUT" || target == "SELECT" || target == "OPTION" || target == "BUTTON" || target == "SUBMIT" || target == "RADIO"){
			return true;	
		}
		$(this).parent('li').children('ul').slideToggle('slow', function(){
		if($(this).is(':visible')){
			clickedElm.addClass('expanded').removeClass('collapsed');	
		}else{
			clickedElm.addClass('collapsed').removeClass('expanded');	
		}
		});	
	})
	$('.menuListRow').each(function(){
		var clickedElm = $(this);
		var licount = $(this).parent('li').children('ul').children('li').length;
		if($(this).parent('li').children('ul').is(':visible')){
			clickedElm.parent('li').children('ul').hide();
			clickedElm.addClass('collapsed').removeClass('expanded');	
		}
		//$(this).text(text + " [ "+ licount + " Childs ]");
	})
});
function checkClickedModules(elm){
	var checkedIds = "";
	var position = elm.attr('position');
	elm.find('input[type=checkbox]').each(function(){
		if($(this).is(':checked')){
			if($(this).val() != "true"){
				$(this).parents('li').addClass('selectedRow');
				if(checkedIds == ""){
					checkedIds = $(this).val();	
				}else{
					checkedIds = checkedIds + "," + $(this).val();	
				}
			}
		}else{
			$(this).parents('li').removeClass('selectedRow');//$(this).remove();	
		}
	})
	$('.selectedModulesIds_'+position).val(checkedIds);	
}
function checkSideMenuIds(){
	var checkedIds = "";
	$('#sideMenuSettings').find('input[type=checkbox]').each(function(){
		if($(this).is(':checked')){
			if($(this).val() != "true"){
				$(this).parents('.menuListRow').addClass('selectedRow');
				if(checkedIds == ""){
					checkedIds = $(this).val();	
				}else{
					checkedIds = checkedIds + "," + $(this).val();	
				}
			}
		}else{
			$(this).parents('.menuListRow').removeClass('selectedRow');//$(this).remove();	
		}
	})
	$('#sideMenuIds').val(checkedIds);	
}
function checkFooterMenuIds(){
	var checkedIds = "";
	$('#footerMenuSettings').find('input[type=checkbox]').each(function(){
		if($(this).is(':checked')){
			if($(this).val() != "true"){
				$(this).parents('.menuListRow').addClass('selectedRow');
				if(checkedIds == ""){
					checkedIds = $(this).val();	
				}else{
					checkedIds = checkedIds + "," + $(this).val();	
				}
			}
		}else{
			$(this).parents('.menuListRow').removeClass('selectedRow');//$(this).remove();	
		}
	})
	$('#footerMenuIds').val(checkedIds);
}