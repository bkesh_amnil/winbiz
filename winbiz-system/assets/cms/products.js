/*$(document).ready(function() {
	var ent_cat_id = $("#enterprise_category_id option:selected").val();
	if($.isNumeric(ent_cat_id)) {
		var url = $("#get_dropdown").val(); alert(url);
		var ent_id = $("#hidden_selected_enterprise_id").val();
		var prod_id = $("#hidden_selected_product_category_id").val();
		$.ajax({
			url: url,
			type: "POST",
			data: {id: ent_cat_id, option_id: ent_id, type: "enterprise"},
			success: function(data) {
				console.log(data);
				$("select#enterprise_id")
				    .find("option")
				    .remove()
				    .end()
				    .append(data)
				;
				$.ajax({
					url: url,
					type: "POST",
					data: {id: ent_cat_id, option_id: prod_id, type: "product_category"},
					success: function(data1) {
						console.log(data1);
						$("select#product_category_id")
							.find("option")
							.remove()
							.end()
							.append(data1)
						;
					},
					error: function() {
						alert("error");
					}
				})
			},
			error: function() {
				alert("error");
			}
		})
	}
})*/
$(document).on("change", "#enterprise_category_id", function() {
	var url = $("#get_dropdown").val();
	var ent_id = $("#enterprise_category_id option:selected").val();
	
	$.ajax({
		url: url,
		type: "POST",
		data: {id: ent_id, type: "enterprise"},
		success: function(data) {
			$("select#enterprise_id")
			    .find("option")
			    .remove()
			    .end()
			    .append(data)
			;
			/*$.ajax({
				url: url,
				type: "POST",
				data: {id: ent_id, type: "product_category"},
				success: function(data1) {
					$("select#product_category_id")
						.find("option")
						.remove()
						.end()
						.append(data1)
					;
				},
				error: function() {
					alert("error");
				}
			})*/
		},
		error: function() {
			alert("error");
		}
	})
})

$(document).on("click", "input[name=show_product_price]", function() {
	var value = $(this).val();
	if(value == "1") {
		$(".prod_price").css("display", "block");
	} else {
		$(".prod_price").css("display", "none");
	}
})