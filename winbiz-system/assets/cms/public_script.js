// JavaScript Document
$(document).ready(function(e) {
	$('input[name="btn_dynamic_form"]').click(function(e) {
		var $form = $(this).parents('form');
	   $.ajax({
			url : "./dynamic_form/form_submit",
			dataType: "JSON",
			data: $form.serialize(),
			type : "POST",
			success: function(data){
				if(data.action == "error"){
					alert(data.msg);
				} else {
					$form.submit();
				}
			},
			failure : function(){
				alert('Error in posting data.');
			}
		});		
		return false;
	});
});