$(document).ready(function() {
	/* corresponding name and alias starts */
	$("input[name=enterprise_category_name]").keyup(function(e) {
	  var txtValue = $(this).val();
	  var newValue = txtValue.toLowerCase().replace(/[!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi,'-');
	  $("input[name=enterprise_category_alias]").val(newValue);
	});
	
	/* corresponding name and alias ends */
})
/* add multiple product category for the enterprise category starts */
$(document).on('click', '.add_multiple', function() {
	var product_name = $('input[name="product_category_name"]').val();
	var product_alias = $('input[name="product_category_alias"]').val();
	$('#product-multi-table tbody').append('<tr class="new-multiple"><td><input type="hidden" name="product_name_multiple[]" value="' + product_name + '" class="hidden_product_name" />' + product_name + '</td><td><input type="hidden" name="product_alias_multiple[]" value="' + product_alias + '" class="hidden_product_alias" />' + product_alias + '</td><td><span class="edit-multiple">[ Edit ]</span><span class="remove-multiple"> [ Remove ]</span></td></tr>');
	$('input[name="product_category_name"]').val('');
	$('input[name="product_category_alias"]').val('');
})
/* add multiple product category for the enterprise category ends */
/* edit multiple product category for the enterprise category starts */
$(document).on('click', '.edit-multiple', function(){
	var parents = $(this).parents('tr.new-multiple');
	if(confirm("Do you want to edit data?")) {
		parents.remove();
		var product_name_data = parents.find('.hidden_product_name').val();
		$('input[name=product_category_name]').val(product_name_data);
		var product_alias_data = parents.find('.hidden_product_alias').val();
		$("input[name=product_category_alias]").val(product_alias_data);
	}
})
/* edit multiple product category for the enterprise category ends */
/* delete multiple product category for the enterprise category starts */
$(document).on('click', '.remove-multiple', function(){
	var parents = $(this).parents('tr.new-multiple');
	if(confirm("Do you want to remove data?")) {
		parents.remove();
	}
})
/* delete multiple product category for the enterprise category ends */
$(document).on('click', '.view_products', function() {
	var url = $("#hidden_view_product_url").val();
	var ent_id = $(this).attr('value');
	$.ajax({
		url : url,
		type : 'POST',
		data : {ent_id: ent_id},
		success : function() {

		},
		error : function() {

		}
	})
})