<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<base href="<?php echo base_url(); ?>">
<meta charset="utf-8" />
<title>CMS | ADMIN</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/themes/default.css" rel="stylesheet" id="style_color" />
<link href="<?php echo base_url(); ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" />


<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/plugins/fancytree/skin-xp/ui.fancytree.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url(); ?>assets/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css"  /> -->
<!-- <link href="<?php echo base_url(); ?>assets/plugins/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url(); ?>assets/cms/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css" /> -->

<!-- END PAGE LEVEL STYLES -->

<?php 
  if(isset($head_scripts) && is_array($head_scripts)){
    foreach ($head_scripts as $head_js) : 
      if(file_exists("./assets/".$head_js) && !is_dir("./assets/".$head_js)){
      ?>
<script src="<?= base_url(); ?>assets/<?= $head_js ?>" type="text/javascript"></script>
<?php 
      }
    endforeach; 
  }
  ?>

<?php if($this->input->get('database')) { //for color animation ?>  
<style type="text/css">
	.table-striped tbody > tr.newly_updated > td {	background:none; }
 	.newly_updated { background-color:#FF9900; }
</style>
<script type="text/javascript" src="assets/plugins/fancytree/jquery.js"></script>
<script type="text/javascript" src="assets/plugins/fancytree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="assets/plugins/fancytree/jquery.fancytree.js"></script>
<script type="text/javascript" src="assets/scripts/jquery.animate-colors-min.js"></script>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('.rowCheckBox').each(function(index, element) {
            if($(this).val() == '<?php echo $this->input->get('database') ?>') {
				$(this).parents('tr').addClass('newly_updated');				
				$('.newly_updated').animate({backgroundColor: '#FFFFFF'}, 3000, function(){
					$('.newly_updated').removeClass('newly_updated');
				});
			}
        });
    });
</script>
<?php } ?>


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<div id="header" class="navbar navbar-inverse navbar-fixed-top"> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
    <div class="container-fluid"> 
      <!-- BEGIN LOGO --> 
      <a class="brand" href="<?php echo base_url(); ?>"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Conquer"/> </a> 
      <!-- END LOGO --> 
 
      
      <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="arrow"></span> </a>
      <div class="top-nav">
        <ul class="nav pull-right" id="top_menu">
        <li class="welcomedetails"><span><i class="icon-key"></i>Welcome <a href="<?php echo site_url('my_profile') ?>"><?php echo $this->admin_user_model->display_name(current_admin_id()) ?></a></span></li>
        <li class="divider-vertical hidden-phone hidden-tablet"></li>
        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <b class="caret"></b> </a>
            <ul class="dropdown-menu">
            	
              	<li> <?php echo anchor('login/logout', '<i class="icon-key"></i>Logout'); ?></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- END HEADER -->

<div id="container" class="row-fluid">
