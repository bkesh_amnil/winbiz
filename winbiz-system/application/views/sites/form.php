<?php
	if(!isset($name)){
		$name = "";	
	}
?>
<style type="text/css">
	.listOfModules {
		list-style: none;
		padding: 0;
		margin: 0;
		overflow:hidden;
	}
	.listOfModules li {
		padding: 5px;
		width: 215px;
		float: left;
		margin: 5px;
		border: 1px solid #CCC;
	}
	.block {
		padding-right: 10px;
		float:left;
		background:none;
		height:70px;
	}

	.typemenudetails{
		display:block;
		float:left;
		clear:both;
	}

	.control-group{
		clear:both;
	}
</style>
<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?> </a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i><?php echo $title ?> Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Site Title<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_input('site_title', $site_title, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Sub domain :</label>
                      <div class="controls"><?php echo form_input('sub_domain', $sub_domain, 'class="span12"') . ' .' . $this->global_model->setting('domain_name') ?></div>
                    </div>
                    <!-- <div class="control-group">
                      <label class="control-label">Site Title<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_input('site_title', $site_title, 'class="span12"') ?></div>
                    </div> -->
                    <div class="control-group">
                      <label class="control-label">Site Status:</label>
                      <div class="controls">
                        <div class="controls"><label class="radio">
                          <?php 
                          if(empty($site_offline) || $site_offline == "yes"){
                  					$checkedYes = false;
                  					$checkedNo = true;  
                  			  }else{
                  				 	$checkedYes = true;
                  					$checkedNo = false;  
                  			  }
                  			   echo form_radio('site_offline', "no", $checkedYes) ?>
                          Online</label> <label class="radio"><?php echo form_radio('site_offline', "yes", $checkedNo) ?> Offline</label></div>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site Offline Message :</span></label>
                      <div class="controls"><?php echo form_textarea('site_offline_msg', $site_offline_msg, 'class="span12"') ?></div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">Keywords :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_textarea('site_keyword', $site_keyword, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Description :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_textarea('site_description', $site_description, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                    	<label class="control-label">Facebook Link</label>
                    	<!-- <input type="hidden" name="social_media_name[]" value="facebook" /> -->
                    	<div class="controls">
                        <?php //echo form_input('social_media_url[]', $facebook_url, 'class="span12"') ?>
                        <?php echo form_input('facebook_url', $facebook_url, 'class="span12"') ?>
                      </div>
                    </div>
                    <div class="control-group">
                    	<label class="control-label">Twitter Link</label>
                    	<!-- <input type="hidden" name="social_media_name[]" value="twitter" /> -->  
                    	<div class="controls">
                        <?php //echo form_input('social_media_url[]', $twitter_url, 'class="span12"') ?>
                        <?php echo form_input('twitter_url', $twitter_url, 'class="span12"') ?>
                      </div>
                    </div>
                	<div class="control-group">
                		<label class="control-label">Linkedin Link</label>
                		<!-- <input type="hidden" name="social_media_name[]" value="linkedin" /> -->
                		<div class="controls">
                      <?php //echo form_input('social_media_url[]', $linkedin_url, 'class="span12"') ?>
                      <?php echo form_input('linkedin_url', $linkedin_url, 'class="span12"') ?>
                    </div>
                	</div>
                	<div class="control-group">
                		<label class="control-label">Youtube Link</label>
                		<!-- <input type="hidden" name="social_media_name[]" value="youtube" /> -->
                		<div class="controls">
                      <?php //echo form_input('social_media_url[]', $youtube_url, 'class="span12"') ?>
                      <?php echo form_input('youtube_url', $youtube_url, 'class="span12"') ?>
                    </div>
                	</div>
                	<div class="control-group">
                		<label class="control-label">Instagram Link</label>
                		<!-- <input type="hidden" name="social_media_name[]" value="instagram" /> -->
                		<div class="controls">
                      <?php //echo form_input('social_media_url[]', $instagram_url, 'class="span12"') ?>
                      <?php echo form_input('instagram_url', $instagram_url, 'class="span12"') ?>
                    </div>
                	</div>
                  </div>
                  <div class="span6">
                    <?php if($enterprise_id == '0') { ?>
                    <div class="control-group">
                      <label class="control-label">Modules Enabled :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_checkbox('enableAllModules', 'true', $enableAllModules, 'class="enableAllModules"') ?> Enable All Modules
                        <?php 
                			  $modules = $this->administrator_model->get_modules(0, TRUE, 0, 'module_title'); 
                			  $enabledMods= explode(",", $modules_enabled);
                			  if(isset($modules) && is_array($modules)){
                				?>
                        <ul class="listOfModules">
                          <?php foreach($modules as $ind=>$val){ ?>
                          <li>
                            <input type="checkbox" name="modulesEnabled[<?php echo $val->id ?>]" class="modulesEnabled" value="<?php echo $val->id; ?>" <?php if(in_array($val->id, $enabledMods) || isset($modulesEnabled[$val->id])){ echo 'checked="checked"'; } ?> />
                            <?php echo $val->module_title; ?></li>
                          <?php } ?>
                        </ul>
                        <?php } ?>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="control-group">
                      <label class="control-label">Number of Menus :<span class="required">*</span></label>
                      <div class="controls">
                        <?php echo form_input('no_of_menus', $no_of_menus, 'class="span12" id="noofmenus" style="width:50px; float:left; margin-right:10px;"');  ?>
                        <a class="btn" id="btn_nomOk" style="margin-top:0px !important;"><span>Ok</span></a>
                        <div class="clear"></div>
                        <div class="typemenudetails">
                        </div> 
                       </div>
                    </div>
                    <?php if(is_array($menuName)) foreach($menuName as $ind=>$val){ ?>
                    <div class="control-group addedMenus">
                      <label class="control-label">Description :<span class="required">*</span></label>
                      <div class="controls"> <span class="block">Type Name : <?php echo form_input('menuName[' . $ind .']', $val, 'class="span12 inline"') ?></span> <span class="block">&nbsp;&nbsp;Sub Menus :
                        <?php 
            							if(empty($submenus[$ind]) || $submenus[$ind] == "yes"){
            								$yes = TRUE; $no = FALSE;
            							} else {
            								$yes = FALSE; $no = TRUE;
            							}
            							echo form_radio("submenus[{$ind}]", 'yes', $yes) . "Yes ";
                          echo form_radio("submenus[{$ind}]", 'no', $no) . "No " ?>
                        </span> <span class="block">&nbsp;&nbsp;Depth: <?php echo form_input('depth[' . $ind .']', $depth[$ind], 'class="span12 inline"') ?> </span></div>
                    </div>
                    <?php } ?>
                    <div class="control-group logo_row">
                      <label class="control-label">Site Logo :</label>
                      <div class="controls">
                        <?php  if($site_logo) {  ?>
                        <img src="./uploaded_files/site_logo/<?php echo $site_logo ?>" height="100" /><a href="#" id="remove_logo" onclick="return false">Remove</a>
                        <?php	 } else 
					               echo form_upload('site_logo', $site_logo, 'class="span12"')?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site Banner Size :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_input('siteBanner', $siteBanner, 'class="span12"') ?><small>Provide Banner Size in the site if you have enabled. (width,height) in pixels</small></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site Images Sizes :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_input('imageSize', $imageSize, 'class="span12"') ?> <small>Provide Different Sizes of image that site will contain. format(width,height|width,height..) in pixels</small></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site Email :<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_input('site_from_email', $site_from_email, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Google Analytics Code :</label>
                      <div class="controls"> <?php echo form_input('google_analytics_code', $google_analytics_code, 'class="span12"') ?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site facebook/Twitter Share Title:</label>
                      <div class="controls"> <?php echo form_input('share_title', $share_title, 'class="span12"') ?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site facebook/Twitter Share Image:</label>
                      <div class="controls"> <?php  if($share_image) {  ?>
                        <img src="./uploaded_files/site_logo/<?php echo $share_image?>" style="height:100px; " /><a href="#" id="remove_share_image" onclick="return false">Remove</a>
                        <?php	 } else 
				echo form_upload('share_image', $share_image, 'class="span12"')?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site facebook/Twitter Share Link:</label>
                      <div class="controls"> <?php echo form_input('share_link', $share_link, 'class="span12"')?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Site facebook/Twitter Share Description:</label>
                      <div class="controls"> <?php echo form_textarea('share_description', $share_description, 'class="span12"') ?> </div>
                    </div>
                  </div>
                  
                </div>
                <div class="form-actions"> 
                  <input type="hidden" name="hidden_enteprise_id" value="<?php echo $enterprise_id ?>"/>
                  <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
        </div>
        <table border="0">
          <tr id="site_offline_message">
            <th scope="row"> </th>
            <td></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
		
		<?php  if($site_logo) {  ?>
		$('#remove_logo').click(function(e) {
            $(this).parents('div.controls').html('<?php echo form_upload('site_logo', $site_logo, 'class="span12"')?>');
        });
        <?php } ?>
<?php  if($share_image) {  ?>
		$('#remove_share_image').click(function(e) {
            $(this).parents('div.controls').html('<?php echo form_upload('share_image', $share_image, 'class="span12"')?>');
        });
		<?php } ?>
		
		if($('#site_offline').val() == 1){
			$('#site_offline_message').show();
		}
		
		$('#site_offline').change(function(e) {
            $('#site_offline_message').toggle();
        });
		jQuery('.enableAllModules').change(function () {
	      var set = ".modulesEnabled";
	      var checked = jQuery(this).is(":checked");
	      jQuery(set).each(function () {
	          if (checked) {
	              $(this).attr("checked", true);
	          } else {
	              $(this).attr("checked", false);
	          }
	      });
	      jQuery.uniform.update(set);
	    });
	    jQuery('.modulesEnabled').change(function () {
	      	var set = ".enableAllModules";
	      	var allchecked = true;
			$('.modulesEnabled').each(function(){
	            if($(this).is(":checked")){
				}else{
					allchecked = false;	
				}
	        });	
	        if(allchecked == true){
				$(set).attr("checked", true);
			}else{
				$(set).attr("checked", false);
			}
			jQuery.uniform.update(set);
	    });
		$('#btn_nomOk').live("click", function(){
			var nums = parseInt($('#noofmenus').val());
			if(nums > 0){
				var available = $('.addedMenus').length;
				//alert(available);
				if(available > nums){
					//alert("remove");
					RemoveRows(nums);
				}else{
					//alert("add");
					AddRows((nums - available), $(this).parent('.controls').find(".typemenudetails"))
				}
			}
		});
		$('#btn_tonOk').live("click", function(){
			var nums = parseInt($('#noofnewstypes').val());
			if(nums > 0){
				var available = $('.addedNewsTypes').length;
				//alert(available);
				if(available > nums){
					//alert("remove");
					RemoveRowsNews(nums);
				}else{
					//alert("add");
					AddRowsNews((nums - available), $(this).parent('.controls'));
				}
			}
		});
	});
	function RemoveRows(nums){
		$('.addedMenus').each(function(i){
			if(i >= nums){
				$(this).remove();	
				resetAll();
			}
		})
	}
	function RemoveRowsNews(nums){
		$('.addedNewsTypes').each(function(i){
			if(i >= nums){
				$(this).remove();	
				resetAll();
			}
		})
	}
	function AddRows(nums, elm){
			var available = $('.addedMenus').length;
			for(var i = 0; i < nums; i++){
				var randNum = (Math.floor( Math.random()* (1 + (available + 20) - available) ) ) + available;
				var addElm = '<div class="addedMenus"><div><span class="block">Type Name : <input type="text" name="menuName['+randNum+']" class="span12 inline" /></span><span class="block">&nbsp;&nbsp;Sub Menus :<label> <input type="radio" name="submenus['+randNum+']" value="yes" checked="checked" /> Yes</label><label> <input type="radio" name="submenus['+randNum+']" value="no" /> No</label></span><span class="block">&nbsp;&nbsp;Depth: <input type="text" name="depth['+randNum+']" class="span12 inline" value="0" /></span></div></div>';
				elm.append(addElm);
			}
			resetAll();
	}
	function resetAll(){
		$('.addedMenus').each(function(i) {
            $(this).find('input').each(function() {
                var name = $(this).attr("name");
				name = name.split('[');
				var newName = name[0]+"["+i+"]";
				$(this).attr("name", newName);
            });
        });
	}
	function AddRowsNews(nums, elm){
			var available = $('.addedNewsTypes').length;
			for(var i = 0; i < nums; i++){
				var randNum = (Math.floor( Math.random()* (1 + (available + 20) - available) ) ) + available;
				var addElm = '<tr class="addedNewsTypes"><td></td><td><span class="block">News Type Name : <input type="text" name="newsName['+randNum+']" class="span12 inline" /></span></td></tr>';
				$('.logo_row').before(addElm);
			}
			resetAllNews();
	}
	function resetAllNews(){
		$('.addedNewsTypes').each(function(i) {
            $(this).find('input').each(function() {
                var name = $(this).attr("name");
				name = name.split('[');
				var newName = name[0]+"["+i+"]";
				$(this).attr("name", newName);
            });
        });
	}
</script> 