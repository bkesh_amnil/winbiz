<?php
//printr($siteInfo);
//printr($tableStatus);
?>
<style type="text/css">
 th { text-align: left; vertical-align:top; height: 30px;}	
 td{
	padding:5px 0;
	vertical-align:top; 
 }
 .tableSiteData{
	list-style:none;
	margin:0;
	padding:0; 
	margin-top:20px;
 }
 .tableSiteData li{
	padding:3px; 
 }
 .tableSiteData li .tableName, .tableSiteData li .dataCount{
	margin-right:20px;
	font-weight:bold; 
 }
</style>
<!-- CONTENT START -->
<div class="grid_16" id="content"> 
  <!--  TITLE START  -->
  <div class="grid_9">
    <h1 class="settings"><?php echo $title ?></h1>
  </div>
  <div class="clear"> </div>
  <!--  TITLE END  --> 
  
  <!-- #PORTLETS START -->
  <div id="portlets"> 
    
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
        <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data">
          <table border="0">
            <tr>
              <th width="171" height="21" scope="row" colspan="2">You Are Going To Delete <?php echo $siteInfo->site_title; ?> Site</th>
            </tr>
            <tr>
              <th height="21" scope="row" width="200">Site Name : </th>
              <td><?php echo $siteInfo->site_name; ?></td>
            </tr>
            <tr>
              <th height="21" scope="row">Sub Domain Name : </th>
              <td><?php echo $siteInfo->sub_domain; ?></td>
            </tr>
            <tr>
              <th height="21" scope="row">Site Offline  : </th>
              <td>
			  <?php
              	if($siteInfo->site_offline == "yes"){
				?>
                Site Is Running.
                <?php	
				}else{
				?>
                Site Is Offline.
                <?php		
				}
			  ?>
              </td>
            </tr>
            <tr>
              <th scope="row">Site Created By : </th>
              <td><?php echo $this->admin_user_model->display_name($siteInfo->created_by) ?></td>
            </tr>
            <tr>
              <th scope="row">Site Created Date : </th>
              <td><?php echo $siteInfo->created_date; ?></td>
            </tr>
            <tr>
              <th scope="row">Site Updated By : </th>
              <td><?php echo $this->admin_user_model->display_name($siteInfo->modified_by) ?></td>
            </tr>
            <tr>
              <th scope="row">Site Updated Date : </th>
              <td><?php echo $siteInfo->modified_date; ?></td>
            </tr>
            <tr>
              <th scope="row">Site Status :</th>
              <td>
              	<?php 
				if(!empty($tableStatus) && is_array($tableStatus)){
					?>
                    <span style="font-weight:bold">This Site Has Associated Data On The Following Tables</span><br>
                    <ul class="tableSiteData">
                    <?php 
					foreach($tableStatus as $ind=>$val){
					?>
                    	<li>
                        <span class="tableName">
						<?php 
						$tblNme = $val["tableName"];
						$tblNme = explode("_",$tblNme);
						$tblNme = $tblNme[(count($tblNme) - 1)];
						echo ucfirst($tblNme)." Table :"; 
						?>
                        </span>
                        <span class="dataCount"><?php echo $val["rows"]." Row/s"; ?></span></li>
                    <?php	
					}
					?>
                    </ul>
                    <?php
				}else{
					echo "This Site Has No Data Associated";	
				}
				?>
              </td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td>
              	Are You Sure You Want To Delete?
              </td>
            </tr>
            <tr class="submitRowLast">
              <th scope="row">&nbsp;</th>
              <td><input type="hidden" name="confirmDeleteSite" id="confirmDeleteSite" value="no" /><a class="button" id="btn_submit"><span>Yes Confirm Delete</span></a> <a class="button_grey" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="clear"> </div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
			$("#confirmDeleteSite").val("sure");
            $('#form1').submit();
        });
	});
</script>