<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1" enctype="multipart/form-data">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Banner Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Select Site : *</label>
                      <div class="controls"><?php echo form_dropdown('site_id', $sites, $site_id, 'class="span12"')?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Title : *</label>
                      <div class="controls">
                        <input id="form_title" type="text" name="form_title" maxlength="255" value="<?php echo $form_title; ?>" class="span12"  />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Name: *</label>
                      <div class="controls">
                        <input id="form_name" type="text" name="form_name" maxlength="50" value="<?php echo $form_name; ?>" class="span12" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Related To:</label>
                      <div class="controls">
                        <select id="form_related" name="form_related" class="span12">
                          <option value="0">None</option>
                          <?php
                if(isset($availableForms) && !empty($availableForms) && is_array($availableForms)){
					foreach($availableForms as $find=>$fdata){
					?>
                          <option value="<?php echo $fdata->id; ?>" <?php echo (($form_related == $fdata->id) ? 'selected="selected"' : ""); ?>><?php echo ucfirst($fdata->form_name); ?></option>
                          <?php
					}
				}
				?>
                        </select>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Relation Link Text:</label>
                      <div class="controls">
                        <input id="form_relation_link" type="text" name="form_relation_link" maxlength="50" value="<?php echo $form_relation_link; ?>" class="span12" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Description:</label>
                      <div class="controls">
                        <?php
					include_once('assets/ckeditor/ckeditor.php');
					include_once('assets/ckfinder/ckfinder.php');
					$ckeditor = new CKEditor();
					$ckeditor->basePath = site_url().'assets/ckeditor/';
					CKFinder::SetupCKEditor($ckeditor, site_url().'assets/ckfinder/');
					$ckeditor->editor('form_description', $form_description);
				?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Class :</label>
                      <div class="controls">
                        <input id="form_class" type="text" name="form_class" maxlength="255" value="<?php echo $form_class; ?>" class="span12" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Attributes :</label>
                      <div class="controls">
                        <input id="form_attribute" type="text" name="form_attribute" maxlength="255" value="<?php echo $form_attribute; ?>"  class="span12" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Form Status :</label>
                      <div class="controls"><?php echo form_dropdown('form_status', $statuses, $form_status, 'class="span12"')?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Submit Action :</label>
                      <div class="controls"><?php echo form_dropdown('submit_action', $submit_actions, $submit_action, 'class="span12"')?></div>
                    </div>
                  </div>
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Success Message :</label>
                      <div class="controls"><?php echo form_textarea('success_msg', $success_msg, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Admin Email :</label>
                      <div class="controls">
                        <input id="admin_email" type="text" name="admin_email" maxlength="255" value="<?php echo $admin_email; ?>" class="span12" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Admin Email Message :</label>
                      <div class="controls"><?php echo form_textarea('admin_email_msg', $admin_email_msg, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Email to user :</label>
                      <div class="controls"><?php echo form_dropdown('email_to_user', $email_to_users, $email_to_user, ' class="span12"')?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">User Email Message :</label>
                      <div class="controls"><?php echo form_textarea('user_email_msg', $user_email_msg, ' class="span12"') ?></div>
                    </div>
                  </div>
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
          <?php
          $this->load->view("user_detail_common");
          ?>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
			$('#Save').val("true");
            $('#form1').submit();
        });			
	});	
</script> 
