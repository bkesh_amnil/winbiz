<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="<?php echo base_url("dynamic_form"); ?>">Forms</a> <span class="divider">/</span> </li>
          <li><a href="#">Data :: <?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php  
    $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body"> <?php echo form_open('', 'method="get"'); ?>
              <div class="searchBar">
                <div class="searchBySite">
                  <select name="search_in" class="span12">
                    <?php foreach($fields as $field) { ?>
                    <option value="<?php echo $field->id ?>" <?php if($this->input->get('search_in') == $field->id) echo 'selected' ?>><?php echo $field->field_label ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="searchByTitle">
                  <input type="text" name="search" id="searchtext" value="<?php echo $this->input->get('search'); ?>" class="searchFld span12" />
                </div>
                <div class="searchByTitle">
                  <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
                </div>
                <div class="clear"></div>
              </div>
              <?php echo form_close() ?>
              <div class="actionsBar">
                <?php 
                if($add) { 
                ?>
                <a href="<?php echo site_url("{$page_name}/form"); ?>" rel="addnewfield">
                <button class="btn btn-danger btn-success addIcon"> <i class="icon-plus icon-white"></i> Create </button>
                </a>
                <?php } ?>
                <?php if($edit) { ?>
                <a href="#" title="Edit Selected" rel="editfield">
                <button class="btn btn-danger btn-info editIcon"><i class="icon-pencil icon-white"></i> Edit</button>
                </a>
                <?php } ?>
                <?php if($delete) { ?>
                <button class="btn btn-danger deleteIcon"><i class="icon-remove icon-white"></i> Delete</button>
                <div class="actionIcons deleteIcon"><a href="#" title="Delete Selected">&nbsp;</a></div>
                <?php } ?>
                <a href="<?php echo base_url("dynamic_form"); ?>">
                <button class="btn backIcon"><i class="icon-arrow-left icon-white"></i> Back</button>
                </a> </div>
            </div>
            <div class="widget-body">
              <div id="portlets">
                <div class="column"> </div>
                
                <!--THIS IS A WIDE PORTLET-->
                <div class="portlet">
                  <table id="box-table-a" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                    <thead>
                      <tr>
                        <th scope="col" width="10">S.N.</th>
                        <th scope="col" width="20">&nbsp;</th>
                        <?php foreach($fields as $field) {?>
                        <th scope="col"><?php echo $field->field_label ?></th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if( ! ($submitted_forms)) { ?>
                      <tr>
                        <td colspan="<?php echo count($fields) + 2 ?>" align="center">No records found.</td>
                      </tr>
                      <?php }  
		  		$i = $start + 1; 
				foreach($submitted_forms as $submitted_form_id => $submitted_form) { ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td scope="col"><input type="checkbox" name="selected[]" value="<?php echo $submitted_form_id; ?>" class="rowCheckBox" /></td>
                        <?php foreach($fields as $field) {?>
                        <td scope="col" class="rightOperation" rel="<?php echo $submitted_form_id ?>">&nbsp;<?php echo $submitted_form[$field->id] ?></td>
                        <?php } ?>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	$(document).ready(function(e) {
		var cur_page = '<?php echo $start ?>';
		
		<?php if($edit) { ?>
        $('.editIcon').click(function(){
            if($('.rowCheckBox:checked').size() == 0){
                alert('Select Item First');
				return false;
            }
			
			var id = $('.rowCheckBox:checked:first').val();
			window.location =  '<?php echo site_url("{$page_name}/form/") ?>/' + id + '?cur_page=' + cur_page;
			
            return false;
        });
		<?php } ?>
		
		<?php if($delete) { ?>
        $('.deleteIcon').click(function(){
            if($('.rowCheckBox:checked').size() == 0){
                alert('Select Item First');
				return false;
            }
			
			var id = $('.rowCheckBox:checked:first').val();
			window.location =  '<?php echo site_url("{$page_name}/delete/") ?>/' + id + '?cur_page=' + cur_page;
			
            return false;
        });
		<?php } ?>
    });
</script> 
