<script type="text/javascript">
	$(document).ready(function(e) {

		var settings = {
			flash_url : "<?php echo base_url() ?>js/swfupload/swfupload.swf",
			upload_url: "<?php echo site_url('dynamic_form_upload/multiple_uploads/') ?>",
			post_params: {"form_submission_id" : "<?php echo $submitted_form_id; ?>", "form_field_id" : "<?php echo $form_field_id ?>"},
			file_size_limit : "5 MB",
			file_types : "*.jpg;*.png;*.gif;*.zip",
			file_types_description : "Files",
			file_upload_limit : 100,
			file_queue_limit : 0,
			custom_settings : {
				progressTarget : "fsUploadProgress_<?php echo $form_field_id ?>",
				cancelButtonId : "btnCancel_<?php echo $form_field_id ?>"
			},
			debug: false,
	
			// Button settings
			button_image_url: "<?php echo base_url() ?>js/swfupload/wdp_buttons_upload_114x29.png",
			button_width: "114",
			button_height: "29",
			button_placeholder_id: "spanButtonPlaceHolder_<?php echo $form_field_id ?>",
			button_text: '',
			button_text_style: ".theFont { font-size: 16; }",
			button_text_left_padding: 12,
			button_text_top_padding: 3,
			
			// The event handler functions are defined in handlers.js
			file_queued_handler : fileQueued,
			file_queue_error_handler : fileQueueError,
			file_dialog_complete_handler : fileDialogComplete,
			upload_start_handler : uploadStart,
			upload_progress_handler : uploadProgress,
			upload_error_handler : uploadError,
			upload_success_handler : uploaded_<?php echo $form_field_id ?>,
			upload_complete_handler : uploadComplete,
			queue_complete_handler : queueComplete_<?php echo $form_field_id ?>	// Queue plugin event
		};
		var swfu = new SWFUpload(settings);
		 
		 function uploaded_<?php echo $form_field_id ?>(file, serverData)
		 {
			var progress = new FileProgress(file, this.customSettings.progressTarget);
			var message = "uploaded";
			progress.setComplete();
			progress.setStatus(message);
			progress.toggleCancel(false);
			$('#multi_uploaded_files_<?php echo $form_field_id ?>').append(serverData);
		 }
		 
		 function queueComplete_<?php echo $form_field_id ?>(numFilesUploaded) {
			var status = document.getElementById("divStatus_<?php echo $form_field_id ?>");
			status.innerHTML = numFilesUploaded + " file" + (numFilesUploaded === 1 ? "" : "s") + " uploaded.";
		}
		
		$("#btnCancel_<?php echo $form_field_id ?>").click(function(e) {
            swfu.cancelQueue();
        });
	 });
</script>

<div class="multiple_upload">
  <div class="fieldset flash" id="fsUploadProgress_<?php echo $form_field_id ?>"> <span class="legend">Upload Queue</span> </div>
  <div id="divStatus_<?php echo $form_field_id ?>" style="margin-bottom:10px;">0 File Uploaded</div>
  <div> <span id="spanButtonPlaceHolder_<?php echo $form_field_id ?>"></span>
    <input id="btnCancel_<?php echo $form_field_id ?>" type="button" value="Cancel All Uploads" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
  </div>
  <div id="multi_uploaded_files_<?php echo $form_field_id ?>">
  	<?php foreach($result as $row) { ?>
    <div class="row">&raquo; <?php echo $row->form_display_text . ' &nbsp; &nbsp; '. anchor("downloads/files/{$submitted_form_id}/{$form_field_id}/" . $row->id, 'Download') . ' &nbsp; ' . anchor('#', 'Remove', 'rel="'. $form_field_id . '" class="remove_file" multi_upload="true"'); ?></div>
    <?php } ?>
  </div>
</div>
