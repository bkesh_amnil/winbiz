<?php

	/*$this->db->where('id', $gallery_id);
	$row = $this->db->get("gallery")->row();
	$site_id = $row->site_id;
	$row = "";*/
?>
<script type="text/javascript">
		var swfu;
		window.onload = function() {
			var settings = {
				flash_url : "<?php echo base_url() ?>js/swfupload/swfupload.swf",
				upload_url: "<?php echo base_url() . 'upload/uploadImages/1/dynamic_uploads/true' ?>",
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>"},
				file_size_limit : "5 MB",
				file_types : "*.jpg;*.png;*.gif",
				file_types_description : "Images",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "<?php echo base_url() ?>js/swfupload/wdp_buttons_upload_114x29.png",
				button_width: "114",
				button_height: "29",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '',
				button_text_style: ".theFont { font-size: 16; }",
				button_text_left_padding: 12,
				button_text_top_padding: 3,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess_1,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
		 
		 function uploadSuccess_1(file, serverData) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		//alert(serverData);
		serverData = serverData.split(',');
		var response = serverData[0].split(':');
		response = response[1];
		var message = serverData[1].split(':');
		message = message[1];
		if(response == "error"){
			progress.setError();
			progress.setStatus("Error!!! "+message);
			progress.toggleCancel(false);
		}else{
			var file = serverData[2].split(':');
			file = file[1];
			var ts = Math.round((new Date()).getTime());
			var randomnumber = Math.floor(Math.random()*11111);
			var key = ts+"_"+randomnumber;
			var htmlObject = '<li><div class="Image"><img src="upload/showImage?file='+file+'&type=temp&width=75&height=75" width="75" height="75" /></div><div class="actions"><span class="actionDeleteTemp" rel="'+file+'"></span></div><span class="label-value"><input type="hidden" name="imageProp['+key+'][file]" value="'+file+'" /></span></div></div></li>';
			$('.imageList').prepend(htmlObject);
			progress.setComplete();
			progress.setStatus(message);
			progress.toggleCancel(false);
		}

	} catch (ex) {
		this.debug(ex);
	}
}
	</script>
<style type="text/css" >
	/* -- Form Styles ------------------------------- */
form {	
	margin: 0;
	padding: 0;
}



div.fieldset {
	border:  1px solid #afe14c;
	margin: 10px 0;
	padding: 20px 10px;
}
div.fieldset span.legend {
	position: relative;
	background-color: #FFF;
	padding: 3px;
	top: -30px;
	font: 700 14px Arial, Helvetica, sans-serif;
	color: #73b304;
}

div.flash {
	width: 375px;
	margin: 10px 5px;
	border-color: #D9E4FF;

	-moz-border-radius-topleft : 5px;
	-webkit-border-top-left-radius : 5px;
    -moz-border-radius-topright : 5px;
    -webkit-border-top-right-radius : 5px;
    -moz-border-radius-bottomleft : 5px;
    -webkit-border-bottom-left-radius : 5px;
    -moz-border-radius-bottomright : 5px;
    -webkit-border-bottom-right-radius : 5px;

}

button,
input,
select,
textarea { 
	border-width: 1px; 
	margin-bottom: 10px;
	padding: 2px 3px;
}



input[disabled]{ border: 1px solid #ccc } /* FF 2 Fix */


label { 
	width: 150px; 
	text-align: right; 
	display:block;
	margin-right: 5px;
}

#btnSubmit { margin: 0 0 0 155px ; }

/* -- Table Styles ------------------------------- */
td {
	font: 10pt Helvetica, Arial, sans-serif;
	vertical-align: top;
}

.progressWrapper {
	width: 357px;
	overflow: hidden;
}

.progressContainer {
	margin: 5px;
	padding: 4px;
	border: solid 1px #E8E8E8;
	background-color: #F7F7F7;
	overflow: hidden;
}
/* Message */
.message {
	margin: 1em 0;
	padding: 10px 20px;
	border: solid 1px #FFDD99;
	background-color: #FFFFCC;
	overflow: hidden;
}
/* Error */
.red {
	border: solid 1px #B50000;
	background-color: #FFEBEB;
}

/* Current */
.green {
	border: solid 1px #DDF0DD;
	background-color: #EBFFEB;
}

/* Complete */
.blue {
	border: solid 1px #CEE2F2;
	background-color: #F0F5FF;
}

.progressName {
	font-size: 8pt;
	font-weight: 700;
	color: #555;
	width: 323px;
	height: 14px;
	text-align: left;
	white-space: nowrap;
	overflow: hidden;
}

.progressBarInProgress,
.progressBarComplete,
.progressBarError {
	font-size: 0;
	width: 0%;
	height: 2px;
	background-color: blue;
	margin-top: 2px;
}

.progressBarComplete {
	width: 100%;
	background-color: green;
	visibility: hidden;
}

.progressBarError {
	width: 100%;
	background-color: red;
	visibility: hidden;
}

.progressBarStatus {
	margin-top: 2px;
	width: 337px;
	font-size: 7pt;
	font-family: Arial;
	text-align: left;
	white-space: nowrap;
}

a.progressCancel {
	font-size: 0;
	display: block;
	height: 14px;
	width: 14px;
	background-image: url(../images/cancelbutton.gif);
	background-repeat: no-repeat;
	background-position: -14px 0px;
	float: right;
}

a.progressCancel:hover {
	background-position: 0px 0px;
}


/* -- SWFUpload Object Styles ------------------------------- */
.swfupload {
	vertical-align: top;
}

 .imageList{
	list-style:none;
	margin:0;
	padding:0 
 }
 .imageList li{
	width:75px;
	padding:10px;
	height:75px;
	float:left;
	border:1px solid #999;
	border-radius:5px;
	background-color:#DDDDDD;
	/*box-shadow:2px 2px 5px 0px #999;*/
	margin:10px;
 }
  .imageList li:hover{
	box-shadow:2px 2px 5px 0px #2d2e2e;
	border:1px solid #FFF;
 }
 
 .imageList li .Details{
	height:135px;
	overflow:hidden;
	width:100%;
	margin-top:-1px;
 }
  .imageList li .Details div{
	height:auto;
	padding:3px;
	background-color:#E1E1E1;
	border:1px solid #FFF;
 }
 .imageList li .Image{
	margin-bottom:3px; 
 }
 .actions {
    background: url("images/1pxdot.png") repeat scroll left top transparent;
    float: left;
    height: 18px;
    position: relative;
    top: -87px;
    width: 100%;
}
 .actions span{
	width:16px;
	height:16px;
	margin-right:10px;
	float:right; 
 }
 .actionStatusYes{
	background:url(assets/img/icons/published.png) top left no-repeat;
	cursor:pointer;
 }
 .actionStatusNo{
	background:url(assets/img/icons/unpublished.png) top left no-repeat;
	cursor:pointer;
 }
 .actionDelete, .actionDeleteTemp{
	background:url(assets/img/icons/delete16.png) top left no-repeat;
	cursor:pointer;
 }
 .imageDesc{
	width: 260px;
	border: 1px solid #CCC;
	outline: none;
 }
</style>

 <div class="multiple_upload">   
     <table>
            <tr>
              <th width="171" scope="row">Upload Images: *</th>
              <td width="348">
                <div class="fieldset flash" id="fsUploadProgress">
                        <span class="legend">Upload Queue</span>
                </div>
                <div id="divStatus">0 Files Uploaded</div>
                <div>
                        <span id="spanButtonPlaceHolder"></span>
                        <input id="btnCancel" type="button" value="Cancel All Uploads" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;" />
                </div>
              </td>
            </tr>
            <tr>
            <td>
            <ul class="imageList">
            
            </ul>
            </td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td>
              <input type="hidden" name="form_field_id" value="<?php echo $form_field_id; ?>" />
           
            </td>
             </tr>
     </table>
            </div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
			$('#Save').val("true");
            $('#form1').submit();
        });
		$('.imageList li').live("mouseover", function(){
			$(this).stop().animate({
				borderBottomRightRadius: 30,
				borderTopLeftRadius: 30
			}, 300);
			var obj = $(this);
			$(this).find('img').stop().animate({
				opacity: 0.85
			}, 300, function (){
				obj.find('.actions').slideDown(200);
			});
		}).live("mouseout", function(){
			$(this).stop().animate({
				borderBottomRightRadius: 5,
				borderTopLeftRadius: 5
			}, 100);
			var obj = $(this);
			$(this).find('img').stop().animate({
				opacity: 1
			}, 100, function (){
				obj.find('.actions').slideUp(200);	
			});
		});
		$("#menuType").change(function(){
			var selvalue = $(this).val();
			$.ajax(function(){
					
			})
			//alert("Sub Menus "+ $(this).attr("submenus") + " Depth "+ $(this).attr("depth"));	
		});
		$('.actionDeleteTemp').live("click", function(){
			var file = $(this).attr('rel');
			var li = $(this).parent('div').parent('li');
			li.fadeOut(300, function(){
				li.remove();
				$.ajax({
					type : 'POST',
					url : '<?php echo site_url('form_controller/deleteImages_temp?file=') ?>'+file,
					dataType : 'html',
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert("Error Deleting File");
					}
				});
			})
		})
		$('.actionDelete').live("click", function(){
			var id = $(this).attr('rel');
			var li = $(this).parent('div').parent('li');
			li.fadeOut(300, function(){
				li.remove();
				$.ajax({
					type : 'POST',
					url : '<?php echo site_url('gallery/deleteImages?id=') ?>'+id,
					dataType : 'html',
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert("Error Deleting File");
					}
				});	
			})
		})
	});
</script> 
