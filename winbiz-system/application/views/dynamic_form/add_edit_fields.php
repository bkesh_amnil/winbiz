<script type="text/javascript">
    $(document).ready(function(){
         var debugStr, sort_order;
        // var sortInput = jQuery('#sort_order');
        $("#box-table-a").tableDnD({
	    onDragClass: "myDragClass",
	    onDrop: function(table, row) {
                var rows = table.tBodies[0].rows; 
               debugStr = "";
                for (var i=0; i<rows.length; i++) {
                    debugStr += rows[i].id+" "; 
                }
                //alert(debugStr);
	        //$('#debugArea').html(debugStr);
                 $.ajax({
                data: ({debugStr : debugStr}),//'sort_order=' + debugStr.value,
               type : 'post',
               url : '<?php echo site_url('form_fields/sort_fields') ?>',
               success : function(){
                      $('#debugArea').text('Database Updated').fadeOut('5000');
               },
               error : function(){
                  $('#debugArea').text('Failure');
               }
           })
	    }
            
	});
          
    });
</script>
<!-- CONTENT START -->
<?php
$form_id = false;
foreach($fields as $field) {//echo $field->form_title
           if(!$form_id){
               $form_id = $field->form_id;
           } 
}
?>

<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="<?php echo base_url("dynamic_form"); ?>">Forms</a> <span class="divider">/</span> </li>
          <li><a href="#">Structure :: <?php echo $form_title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
    $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?> :: <?php echo $form_title ?></h4>
            </div>
            <div class="widget-body"> <?php echo form_open('', 'method="get" class="FLeft"'); ?>
              <div class="searchBar">
                <div class="searchBySite">
                  <input type="text" name="search" id="searchtext" value="<?php echo $this->input->get('search'); ?>" class="searchFld span12" />
                </div>
                <div class="searchByTitle">
                  <button type="submit" class="btn" id="btnSearch"  value="Search"><i class="icon-search icon-white"></i></button>
                </div>
                <div class="clear"></div>
              </div>
              <?php echo form_close() ?>
              <div class="actionsBar">
                <?php 
                    if($add) { 
                    ?>
                <div class="actionIcons addIcon"> <a href="<?php echo site_url("dynamic_form_ajax/add_edit_fields/$form_id"); ?>" rel="addnewfield" class="btn btn-danger btn-success"><i class="icon-plus icon-white"></i> Create</a> </div>
                <?php 
                    } 
                    ?>
                <?php 
                    if($edit) { 
                    ?>
                <div class="actionIcons editIcon"> <a href="#" rel="editfield" class="btn btn-danger btn-info"><i class="icon-pencil icon-white"></i> Edit</a> </div>
                <?php 
                    } 
                    ?>
                <?php 
                    if($delete) { 
                    ?>
                <div class="actionIcons deleteIcon"> <a href="#" rel="editfield" class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</a> </div>
                <?php 
                    } 
                    ?>
                <div class="actionIcons backIcon"> <a class="btn" href="<?php echo site_url('dynamic_form') ?>" title="Back"><i class="icon-arrow-left icon-white"></i>Back</a> </div>
              </div>
            </div>
            <div class="widget-body">
              <div id="portlets"> 
                
                <!--THIS IS A WIDE PORTLET-->
                <div class="portlet">
                  <div class="portlet-content nopadding">
                    <div id="debugArea"></div>
                    <table id="box-table-a" name="sort" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                      <thead>
                        <tr>
                          <th width="47" scope="col">S.N.</th>
                          <th width="40" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                          <th width="223" scope="col">Label</th>
                          <th width="634" scope="col">Type</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(count($fields) == 0 ){ 	?>
                        <tr>
                          <td colspan="6" style="text-align:center !important;">No Records Found! </td>
                        </tr>
                        <?php } $i = 1; 
          foreach($fields as $field) {//echo $field->form_title
           if(!$form_id){
               $form_id = $field->form_id;
           }  
              
          ?>
                        <tr class="row_<?php echo $field->id ?>" id="<?php echo $field->id ?>">
                          <td><?php echo $i ?>
                            <input  type="hidden" value="<?php echo $field->id?>" name="id" /></td>
                          <td><input type="checkbox" name="selected[]" value="<?php echo $field->id; ?>" class="rowCheckBox" data="<?php echo $field->form_id ?>"/></td>
                          <!--<td class="rightOperation" rel="<?php echo $field->id; ?>"><?php echo $row->form_title ?></td>-->
                          <td><?php echo $field->field_label ?></td>
                          <td><?php 
                    $row = $this->db->where('field_type', $field->field_type)->get('field_type')->row();
                    if($row)
                    { 
                        if($field->field_type=='textarea')
                        { ?>
                            <textarea></textarea>
                            <?php   }elseif($field->field_type=='upload')
                        { ?>
                            <input type="file"/>
                            <?php   }elseif($field->field_type=='checkbox')
                        {
                            $options = $this->db->query("SELECT display_text FROM {PREFIX}form_field_value WHERE form_field_id='".$field->id."'")->result_array();
                            if($options)
                            {
                                foreach($options as $option)
                               { ?>
                            <input type="checkbox"/>
                            &nbsp;<?php echo $option['display_text'] ?>&nbsp;&nbsp;
                            <?php        }
                            }
                        }elseif($field->field_type=='radio')
                        {
                            $options = $this->db->query("SELECT display_text FROM {PREFIX}form_field_value WHERE form_field_id='".$field->id."'")->result_array();
                            if($options)
                            {
                                foreach($options as $option)
                                { ?>
                            <input type="radio"/>
                            <?php echo $option['display_text'] ?>&nbsp;&nbsp;
                            <?php           } 
                            } 
                         }elseif($field->field_type=='select')
                         {
                            $options = $this->db->query("SELECT display_text FROM {PREFIX}form_field_value WHERE form_field_id='".$field->id."'")->result_array();
                            if($options)
                            { ?>
                            <select>
                              <?php 
                                foreach($options as $option)
                                { ?>
                              <option>
                              <?php  echo $option['display_text'] ?>
                              </option>
                              <?php           } ?>
                            </select>
                            <?php       }
                        }else
                        {?>
                            <input type="<?php echo $row->field_type; ?>"/>
                            <?php   } 
                    }
                ?></td>
                        </tr>
                        <?php $i++; 
          
          } ?>
                      <input type="hidden" id="sort_order" name="sort_order"/>
                        </tbody>
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
       $('a[rel=addnewfield]').facebox();

       $('.rowCheckBox').change(function(){
           if(this.checked){
               var checked = parseInt($(".rowCheckBox:checked").length);
                    if(checked > 0){
                        if(checked > 1){
                                var excessCount = confirm("Select Only One.");
                                return false;
                        }else{
                                var excessCount = true;	
                        }
                        if(excessCount){
                            var url = "<?php echo site_url("dynamic_form_ajax/edit_fields/") . "/" ?>" + $(".rowCheckBox:checked:first").attr('data')+ "/" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
                            $('.editIcon a').attr('href', url);
                            $('a[rel=editfield]').facebox();
                        }
                    }
                return false;
            }
       });
        
        
        $('.editIcon a').click(function(){
            if($(this).attr('href') == '#'){
                alert('Select Item First');
            }
            return false;
        });
		
		 $('.deleteIcon a').click(function(){
            if($('.rowCheckBox:checked').size() == 0){
                alert('Select Item First');
				return false;
            }
			
			if( ! confirm('Are you sure to delete?')) {
				return false;
			}			
		
			var checkboxs = $('.rowCheckBox').serializeArray();
			$.ajax({
				url:'form_fields/remove_field',
				type:'POST',
				data: {'fields' : checkboxs},
				success:function(msg){
					if(msg == 'success')
					{
						$(checkboxs).each(function(index){
							var tr = $('.rowCheckBox[value="' + checkboxs[index].value + '"]').parents('tr');
							tr.fadeOut(function(){
								$(this).remove() ;
							});
						});
					}
					else 
					{
						alert('Error in removing field.');
					}					
				}, 
				failure: function(){
					alert('Error in removing field.');
				}
			});
			return false;
        })
		
    })
</script>