<?php
$curAdmnId = current_admin_id();
if(!empty($curAdmnId)){
?>
<script type="text/javascript" src="./assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="./assets/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
			$('textarea').each(function(){
					var id = $(this).attr('id');
					 var newCKEdit = CKEDITOR.replace(id);
					CKFinder.setupCKEditor(newCKEdit, '../resources/ckfinder/');
			})
	})
</script>
<?php
}
?>
<style type="text/css">
th {
	text-align: left;
	vertical-align: top;
	height: 25px;
}
.span12 {
	width: 200px;
}
select.span12 {
	width: 208px;
}
td {
	padding: 5px;
}
textarea {
	height: 50px;
}
div.fieldset {
	border: 1px solid #afe14c;
	margin: 10px 0;
	padding: 20px 10px;
}
div.fieldset span.legend {
	position: relative;
	background-color: #FFF;
	padding: 3px;
	top: -30px;
	font: 700 14px Arial, Helvetica, sans-serif;
	color: #73b304;
}
div.flash {
	width: 375px;
	margin: 10px 5px;
	border-color: #D9E4FF;
	-moz-border-radius-topleft : 5px;
	-webkit-border-top-left-radius : 5px;
	-moz-border-radius-topright : 5px;
	-webkit-border-top-right-radius : 5px;
	-moz-border-radius-bottomleft : 5px;
	-webkit-border-bottom-left-radius : 5px;
	-moz-border-radius-bottomright : 5px;
	-webkit-border-bottom-right-radius : 5px;
}
/* -- Table Styles ------------------------------- */
td {
	font: 10pt Helvetica, Arial, sans-serif;
	vertical-align: top;
}
.progressWrapper {
	width: 357px;
	overflow: hidden;
}
.progressContainer {
	margin: 5px;
	padding: 4px;
	border: solid 1px #E8E8E8;
	background-color: #F7F7F7;
	overflow: hidden;
}
/* Message */
.message {
	margin: 1em 0;
	padding: 10px 20px;
	border: solid 1px #FFDD99;
	background-color: #FFFFCC;
	overflow: hidden;
}
/* Error */
.red {
	border: solid 1px #B50000;
	background-color: #FFEBEB;
}
/* Current */
.green {
	border: solid 1px #DDF0DD;
	background-color: #EBFFEB;
}
/* Complete */
.blue {
	border: solid 1px #CEE2F2;
	background-color: #F0F5FF;
}
.progressName {
	font-size: 8pt;
	font-weight: 700;
	color: #555;
	width: 323px;
	height: 14px;
	text-align: left;
	white-space: nowrap;
	overflow: hidden;
}
.progressBarInProgress, .progressBarComplete, .progressBarError {
	font-size: 0;
	width: 0%;
	height: 2px;
	background-color: blue;
	margin-top: 2px;
}
.progressBarComplete {
	width: 100%;
	background-color: green;
	visibility: hidden;
}
.progressBarError {
	width: 100%;
	background-color: red;
	visibility: hidden;
}
.progressBarStatus {
	margin-top: 2px;
	width: 337px;
	font-size: 7pt;
	font-family: Arial;
	text-align: left;
	white-space: nowrap;
}
a.progressCancel {
	font-size: 0;
	display: block;
	height: 14px;
	width: 14px;
	background-image: url(../images/cancelbutton.gif);
	background-repeat: no-repeat;
	background-position: -14px 0px;
	float: right;
}
a.progressCancel:hover {
	background-position: 0px 0px;
}
/* -- SWFUpload Object Styles ------------------------------- */
.swfupload {
	vertical-align: top;
}
.imageList {
	list-style: none;
	margin: 0;
	padding: 0
}
.imageList li {
	width: 275px;
	padding: 10px;
	height: 310px;
	float: left;
	border: 1px solid #999;
	border-radius: 5px;
	background-color: #DDDDDD;
	/*box-shadow:2px 2px 5px 0px #999;*/
	margin: 10px;
}
.imageList li:hover {
	box-shadow: 2px 2px 5px 0px #2d2e2e;
	border: 1px solid #FFF;
}
.imageList li .Details {
	height: 135px;
	overflow: hidden;
	width: 100%;
	margin-top: -1px;
}
.imageList li .Details div {
	height: auto;
	padding: 3px;
	background-color: #E1E1E1;
	border: 1px solid #FFF;
}
.imageList li .Image {
	margin-bottom: 3px;
}
.actions {
	position: relative;
	float: left;
	height: 18px;
	background: url(images/1pxdot.png) top left repeat;
	width: 100%;
	top: -178px;
	display: none;
}
.actions span {
	width: 16px;
	height: 16px;
	margin-right: 10px;
	float: right;
}
.actionStatusYes {
	background: url(assets/img/icons/published.png) top left no-repeat;
	cursor: pointer;
}
.actionStatusNo {
	background: url(assets/img/icons/unpublished.png) top left no-repeat;
	cursor: pointer;
}
.actionDelete, .actionDeleteTemp {
	background: url(assets/img/icons/delete16.png) top left no-repeat;
	cursor: pointer;
}
.imageDesc {
	width: 260px;
	border: 1px solid #CCC;
	outline: none;
}
</style>

<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $form_title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#">Content</a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <?php echo validation_errors(); ?>
    <div id="page" class="dashboard">
      <div class="ajax_error"></div>
      <?php 
			$attributes = array('name' => $form_name, 'method' => 'post', 'class' => $form_class, 'id' => 'form1');
		echo form_open_multipart('', $attributes) ?>
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Content</h4>
            </div>
            <div class="widget-body form">
              <div class="row-fluid">
                <div class="span6">
                  <?php foreach($fields as $field) { ?>
                  <div class="control-group">
                    <label class="control-label"><?php echo $field->field_label ?></label>
                    <div class="controls">
                      <?php 
			$attribute ='id="dynamic_field_'.$field->id.'" class="' . $field->field_class .' span12" ' . $field->field_attribute; 
			$field_name = 'field_name[' . $field->id . ']';
			switch($field->field_type)
			{
				/*No mutliple values */
				case "textarea":
					echo form_textarea($field_name, $field->default_value, $attribute);
					break;
				
				case "upload":
					if( $field->default_value && file_exists('./dynamic_uploads/' . $field->default_value))
					{
						echo '<span>'. anchor("downloads/files/{$submitted_form_id}/{$field->id}", 'Download') . ' &nbsp; ' . anchor('#', 'Remove', 'rel="'. $field->id . '" class="remove_file"') . '</span>';
					}
					else
					{
						echo form_upload($field_name, '', $attribute);
					}
					break;
					
				case "password":
					echo form_password($field_name, $field->default_value, $attribute);
					break;
				
				case "hidden":
					echo form_hidden($field_name, $field->default_value, $attribute);
					 break;
						
				// Multiple values	
				case "select"	:
					$result = $this->db->where('form_field_id', $field->id)->order_by('display_text')->get('form_field_value')->result();
					$dropdowns = dropdown_data($result, 'display_text', 'display_text');
					echo form_dropdown($field_name, $dropdowns, $field->default_value, $attribute);
					break;
				
				case "radio":
					$result = $this->db->where('form_field_id', $field->id)->order_by('display_text')->get('form_field_value')->result();
					foreach($result as $row)
					{
						$checked = ($row->display_text == $field->default_value)?TRUE:FALSE;
						echo form_radio($field_name, $row->display_text, $checked, $attribute) . $row->display_text . ' &nbsp; ';
					}
					break;		
				
				case "multiple_upload":
					if( ! $submitted_form_id)
					{
						//echo "The multiple uploads will be available only in edit page.";
						$data['form_field_id'] = $field->id;
						$this->load->view('dynamic_form/add_multi_image',$data);
					}
					else
					{
						$data['form_field_id'] = $field->id;
						$this->db->where('form_submission_id', $submitted_form_id);
						$this->db->where('form_field_id', $field->id);
						$data['result'] = $this->db->get('form_submission_fields')->result();
					//	echo $this->db->last_query();
					//	printr($data['result']);
						$this->load->view('dynamic_form/multiple_upload', $data);
					}
					break;
					
				//Default is textbox			
				default:
					echo form_input($field_name, $field->default_value, $attribute);						
			}
	?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-actions">
           		<!-- <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> -->
           		<a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
           	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		var cur_page = '<?php echo $this->input->get('cur_page') ?>';
		if(cur_page != '') {
			$('#form1').attr('action', $('#form1').attr('action') + '?cur_page=' + cur_page);
		}
		
		<?php if($submitted_form_id) { ?>
		$('.remove_file').live('click', function(e) {
			var field_id = $(this).attr('rel');
			var $this = $(this);
			if($(this).attr('multi_upload')) 
				var multi = "true";
			else
				var multi = "false";
			$.ajax({
					url: "<?php echo site_url("form_controller/$form_unique_name/remove_file") ?>",
					data: {"submitted" : "<?php echo $submitted_form_id ?>", "field_id" : field_id, "multi" : multi},
					type : "POST",
					success: function(data){
						if(data == "fail"){
							$('div.ajax_error').html('Error in removing data');
						} else if(data == "multi_upload"){
							$this.parents('.row').remove();
						}else {
							var upload = '<input type="file" name=field_name[' + field_id + '] class="span12" />';
							$this.parents('span').html(upload);	
						}
					},
					failure : function(){
						$('div.ajax_error').html('Error in posting data.');
					}
			});
			return false;
		});
		<?php } ?>
		
		$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
		
		var url = $('#form1').attr('action');		
        $('#form1').submit(call_ajax);
		
		var blnSubmit = false;
		function call_ajax()
		{	
			$('div.ajax_error').html('');
			if( ! blnSubmit)
			{
				$.ajax({
					url: url,
					data: $('#form1').serialize(),
					type : "POST",
					dataType : "JSON",
					success: function(data){
						if(data.action == "error"){
							$('div.ajax_error').html(data.msg);
						} else {
							blnSubmit = true;
							$('#form1').submit();
						}
					},
					failure : function(){
						$('div.ajax_error').html('Error in posting data.');
					}
				});
			}
			return blnSubmit;
        }
    });
</script>