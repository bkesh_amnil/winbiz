<link href="css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
    $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body">
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php $this->load->view('action_icon'); ?>
              </div>
            </div>
            <div class="widget-body">
              <form action="<?php echo site_url('content/') ?>" method="get" id="gridForm" autocomplete="off">
                <table id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th width="30" scope="col">S.N.</th>
                      <th width="27" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                      <th width="170" scope="col">Form Title</th>
                      <th width="170" scope="col">Form For</th>
                      <th width="113" scope="col">Created By </th>
                      <th width="114" scope="col">Created Date</th>
                      <th width="88" scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                if(count($result) == 0 ){ 
              ?>
                    <tr>
                      <td colspan="8" align="center" height="24">No Records Found! </td>
                    </tr>
                    <?php 
                }else{ 
                    $i = $start + 1; foreach($result as $row){ 
              ?>
                    <tr>
                      <td><?php echo $i ?></td>
                      <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" data="<?php echo $row->form_unique_name ?>"/></td>
                      <td class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->form_title ?></td>
                      <td><?php
                    $serLink = site_url("{$page_name}?site=".$row->site_id);
                    if($this->input->get('search')){
                        $serLink = $serLink."&search=".$this->input->get('search');	
                    }
                    ?>
                        <a href="<?php echo $serLink; ?>"><?php echo $this->global_model->get_single_data('site', 'site_title', $row->site_id); ?></a></td>
                      <td><?php echo $this->admin_user_model->display_name($row->created_by) ?></td>
                      <td><?php echo $row->created_date ?></td>
                      <td><?php 
                    if($row->form_status == 'Published'){ ?>
                        <a href="<?php echo site_url('dynamic_form/change_status/0/'.$row->id) . get_url(); ?>" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>
                        <?php }else{ ?>
                        <a href="<?php echo site_url('dynamic_form/change_status/1/'.$row->id) . get_url(); ?>" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>
                        <?php    
                        }
                    ?></td>
                    </tr>
                    <?php 
              $i++; 
                } 
              } 
              ?>
                   
                  </tbody>
                </table>
              </form>
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		$('.delete_form').click(function(){
			if(confirm("Are you sure you are deleting this form?")){
				return true;	
			}else{
				return false;	
			}
		})
    });
</script>