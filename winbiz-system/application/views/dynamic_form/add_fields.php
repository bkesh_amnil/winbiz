<form action="" method="post" id="form_field" style="margin:0; padding:0;">
  <div class="row-fluid">
    <div class="span12" id="id1"> 
      <!-- BEGIN EXAMPLE TABLE PORTLET-->
      <div class="widget" style="margin:0;">
        <div class="widget-title">
          <h4><i class="icon-reorder"></i>Content</h4>
        </div>
        <div class="widget-body form">
          <div class="row-fluid">
            <div class="span6">
              <div class="control-group">
                <label class="control-label">Label</label>
                <div class="controls">
                  <input name="field_label" type="text" class="field_label" />
                </div>
              </div>
              <div class="control-group inner_container">
                <label class="control-label">Type</label>
                <div class="controls"><?php echo form_dropdown('field_type', $field_types, '', 'class="field_type"') ?> <a href="#" class="add_values btn btn-success" style="display:none;margin-top:-10px;"><i class="icon-plus icon-white"></i></a>
                  <div class="div_values"></div>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Field Name</label>
                <div class="controls">
                  <input name="field_name" type="text" class="" id="field_name" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Default value</label>
                <div class="controls">
                  <input name="default_value" type="text" class="" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Field Class</label>
                <div class="controls">
                  <input type="text" name="field_class" class="" />
                </div>
              </div>
            </div>
            <div class="span6">
              <div class="control-group">
                <label class="control-label">Front Display</label>
                <div class="controls"><?php echo form_dropdown('front_display', $front_displays, '', 'class=""') ?></div>
              </div>
              <div class="control-group">
                <label class="control-label">Order</label>
                <div class="controls">
                  <input type="text" name="field_order" class="" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Show in grid</label>
                <div class="controls"><?php echo form_dropdown('show_in_grid', $show_in_grids, '', 'class=""') ?></div>
              </div>
              <div class="control-group">
                <label class="control-label">Validation Rules</label>
                <div class="controls">
                  <ul class="validation_controls_options">
                    <?php 
                    foreach($validations as $validation) {
                    ?>
                    <li>
                      <?php
    	                   echo form_checkbox('validation_rule[]', $validation->validation_rule, '', 'class="validation"'). $validation->description;
                      ?>
                    </li>
                    <?php
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#btn_submit').click(function(e) {
            $('#form_field').submit();
        });
    });
</script>