<form action="<?= site_url("form_fields/save_field") ?>" method="post" id="form_field" name="form_field" style="margin:0;">
  <div class="row-fluid">
    <div class="span12" id="id1"> 
      <!-- BEGIN EXAMPLE TABLE PORTLET-->
      <div class="widget">
        <div class="widget-title">
          <h4><i class="icon-reorder"></i>Content</h4>
        </div>
        <div class="widget-body form" style="margin:0; padding:0;">
          <div class="row-fluid">
            <div class="span6">
              <div class="control-group">
                <label class="control-label">Label</label>
                <div class="controls">
                  <input name="field_label" type="text" class="field_label" value="<?php echo $valueArr['field_label'] ?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Type</label>
                <div class="controls">
                  <?php 		  		
              $type = $valueArr['field_type'];
                echo form_dropdown('field_type', $field_types , $type, 'class="field_type" style="width:200px"') ?>
                  <a href="#" class="add_values btn btn-success" style="display:none; margin-top:-10px;"><i class="icon-plus icon-white"></i></a>
                  <?php if($type == 'radio' || $type == 'checkbox' || $type='select'){
                      $values = $this->db->query("SELECT id, display_text FROM {PREFIX}form_field_value WHERE form_field_id='".$valueArr['id']."'")->result_array();
                      foreach($values as $value){ ?>
                  <div class="div_option"><?php echo $value['display_text'] . ' &nbsp; ' . anchor('#', '[Remove]', 'class="remove_option" rel="' . $value['id'] . '"'); ?> </div>
                  <?php }
                  } ?>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Field Name</label>
                <div class="controls">
                  <input name="field_name" type="text" class="span12" id="field_name" value="<?php echo $valueArr['field_name'] ?>" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Default value</label>
                <div class="controls">
                  <input name="default_value" type="text" class="span12" value="<?php echo $valueArr['default_value']?>" />
                </div>
              </div>
            </div>
            <div class="span6">
              <div class="control-group">
                <label class="control-label">Field Class</label>
                <div class="controls">
                  <input type="text" name="field_class" class="span12" value="<?php echo $valueArr['field_class'] ?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Front Display</label>
                <div class="controls">
                  <?php 
              $front = $valueArr['front_display'];
              echo form_dropdown('front_display', $front_displays, $front, 'class="span12"') ?>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                  <input type="hidden" name="field_order" class="span12" value="<?php echo $valueArr['field_order'] ?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Show in grid</label>
                <div class="controls">
                  <?php 
                  $grid = $valueArr['show_in_grid'];  
                  echo form_dropdown('show_in_grid', $show_in_grids, $grid) ?>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Validation Rules</label>
                <div class="controls">
                  <ul class="validation_controls_options">
                    <?php 
                  $valid = explode("|", $valueArr['validation_rule']);
                  foreach($validations as $validation) { 
                    ?>
                    <li>
                      <?php                                  
  	                     echo form_checkbox('validation_rule[]', $validation->validation_rule, in_array($validation->validation_rule, $valid), 'class="validation"'). $validation->description; 
                      ?>
                    </li>
                    <?php
                  }
                  ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <input type="hidden" name="form_field_id" value="<?php echo $valueArr['id']?>"/>
            <input type="hidden" name="form_id" value="<?php echo $valueArr['form_id'] ?>"/>
             <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="field_container">
    <div class="inner_container" id="id1"> </div>
  </div>
</form>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#btn_submit').click(function(e) {
            $('#form_field').submit();
        });
		$('.remove_option').live('click', function(){
			var $this = $(this);
			$.ajax({
				url: '<?php echo site_url('dynamic_form_ajax/remove_option/' . $valueArr['id']) ?>',
				data: {'option_id' : $(this).attr('rel')},
				type: "POST",
				success: function(msg){
					if(msg == "success")
					{
						$this.parents('.div_option').remove();
					}
					else
					{
						alert("Error in removing option.");
					}
				},
				error : function(){
					alert('Error in loading data.')
				}
			});
			return false;
		});
		$('.field_type').change();
    });
</script>