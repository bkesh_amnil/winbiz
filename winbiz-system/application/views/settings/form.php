<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" class="form-horizontal">
      <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i><?php echo $title ?> Form</h4>
              </div>
              <div class="widget-body form">
        <div class="control-group">
          <label class="control-label">Multiple Sites : *</label>
          <div class="controls">
            <label class="radio"> <?php echo form_radio('multipleSites', 'yes', (($multipleSites == "yes") ? TRUE : FALSE)); ?> Yes</label>
            <label class="radio"> <?php echo form_radio('multipleSites', 'no', (($multipleSites == "no") ? TRUE : FALSE)); ?> No </label>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">Limit Of Sites :</label>
          <div class="controls"> <?php echo form_input('siteLimits', $siteLimits, 'class="colorpicker-default"') ?> </div>
        </div>
        <div class="control-group">
          <label class="control-label">Site Structure :</label>
          <div class="controls">
            <label class="radio"><?php echo form_radio('siteStructure', 'sub_domain', (($siteStructure == "sub_domain") ? TRUE : FALSE)); ?> Sub Domain</label>
            <label class="radio"> <?php echo form_radio('siteStructure', 'folder', (($siteStructure == "folder") ? TRUE : FALSE)); ?> Folder</label>
          </div>
        </div>
        <div class="form-actions"> <?php echo form_hidden('settingId', $settingId) ?> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
        </div>
        </div>
        </div>
        </div>
        
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
		
		if($('#site_offline').val() == 1){
			$('#site_offline_message').show();
		}
		
		$('#site_offline').change(function(e) {
            $('#site_offline_message').toggle();
        });
	});
</script> 
