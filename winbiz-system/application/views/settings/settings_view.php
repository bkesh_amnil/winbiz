<!-- CONTENT START -->
<div id="body">
<div class="container-fluid"> 
  <!-- BEGIN PAGE HEADER-->
  <div class="row-fluid">
    <div class="span12"> 
      
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
      <h3 class="page-title"> <?php echo $title ?> </h3>
      <ul class="breadcrumb">
        <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
        <li><a href="#"><?php echo $title ?></a></li>
      </ul>
      <!-- END PAGE TITLE & BREADCRUMB--> 
    </div>
  </div>
  <!-- END PAGE HEADER--> 
  
</div>
</div>

<div class="grid_16" id="content"> 
  <!--  TITLE START  -->
  <div class="grid_9">
    <h1 class="settings">
      <?= $title ?>
    </h1>
  </div>
  <div class="clear"> </div>
  <!--  TITLE END  --> 
  
  <!-- #PORTLETS START -->
  <div id="portlets">
    <div class="column"> </div>
    <?php  if ($this->session->flashdata('class')): ?>
    <p class="info" id="<?php echo $this->session->flashdata('class') ?>"><span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span></p>
    <?php  endif; ?>
    
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
      <div class="portlet-content nopadding">
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
          <thead>
            <tr>
              <th width="30" scope="col">S.N.</th>
              <th width="272" scope="col">Setting Title</th>
              <th width="308" scope="col">Setting Name</th>
              <th colspan="2" scope="col">Setting Value</th>
              <th width="65" scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php if(count($rows) == 0 ) : ?>
            <tr>
              <td colspan="6" align="center" height="24">No Records Found! </td>
            </tr>
            <?php else: ?>
            <?php $i = $start + 1; foreach($rows as $row) : ?>
            <tr>
              <td><?php echo $i ?></td>
              <td><?php echo $row->setting_title ?></td>
              <td><?php echo $row->setting_name ?></td>
              <td colspan="2"><?php echo $row->setting_value ?></td>
              <td width="65"><?php if($edit) { ?>
                <a href="<?= site_url("$page_name/form/" . $row->id) ?>" class="edit_icon" title="Edit"></a>
                <?php } if($delete) { ?>
                <a href="<?= site_url("$page_name/delete/" . $row->id ) ?>" class="delete_icon" title="Delete" onClick = "return confirm('Are you sure to delete?')"></a>
                <?php } ?></td>
            </tr>
            <?php $i++; endforeach; endif;
			?>
            
          </tbody>
        </table>
      </div>
    </div>
    <!--  END #PORTLETS --> 
  </div>
  <div class="clear"> </div>
  <!-- END CONTENT--> 
</div>
<div class="clear"> </div>
