<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8" />
        <base href="<?php echo base_url() ?>">
        <title>Amnil CMS - Installation</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="Amnil Content Management System" name="description" />
        <meta content="Amnil Technologies" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="#" rel="stylesheet" id="style_metro" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <div id="header" class="navbar navbar-inverse navbar-fixed-top"> 
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="navbar-inner">
                <div class="container-fluid"> 
                    <!-- BEGIN LOGO --> 
                    <a class="brand" href="index.html"> <img src="assets/img/logo.png" alt="Conquer"/> </a> 
                    <!-- END LOGO --> 
                </div>
            </div>
            <!-- END TOP NAVIGATION BAR --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid"> 
            <!-- BEGIN PAGE -->
            <div id="body"> 
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid"> 
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <h3 class="page-title"> Amnil CMS Installation <small>copyright @ Amnil Technologies</small> </h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER--> 
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget box blue" id="form_wizard_1">
                                <div class="widget-title">
                                    <h4> <i class="icon-reorder"></i> Installation Wizard - <span class="step-title">Step 1 of 4</span> </h4>
                                    <span class="tools"> <a href="javascript:;" class="icon-chevron-down"></a> <a href="#widget-config" data-toggle="modal" class="icon-wrench"></a> <a href="javascript:;" class="icon-refresh hidden-phone"></a> <a href="javascript:;" class="icon-remove hidden-phone"></a> </span> </div>
                                <div class="widget-body form">
                                    <form action="#" class="form-horizontal" id="frm-setup" method="post">
                                        <div class="form-wizard">
                                            <div class="navbar steps">
                                                <div class="navbar-inner">
                                                    <ul class="row-fluid">
                                                        <li class="span3"> <a href="#tab1" data-toggle="tab" class="step active"> <span class="number">1</span> <span class="desc"><i class="icon-ok"></i> Account Setup</span> </a> </li>
                                                        <li class="span3"> <a href="#tab2" data-toggle="tab" class="step"> <span class="number">2</span> <span class="desc"><i class="icon-ok"></i> Profile Setup</span> </a> </li>
                                                        <li class="span3"> <a href="#tab3" data-toggle="tab" class="step"> <span class="number">3</span> <span class="desc"><i class="icon-ok"></i>Site Setup</span> </a> </li>
                                                        <li class="span3"> <a href="#tab4" data-toggle="tab" class="step"> <span class="number">4</span> <span class="desc"><i class="icon-ok"></i>Database Setup</span> </a> </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div id="bar" class="progress progress-success progress-striped">
                                                <div class="bar"></div>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                    <h4>Provide super administrator account details</h4>
                                                    <div class="control-group">
                                                        <label class="control-label">Username</label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="user_name" name="user_name" />
                                                            <span class="help-inline">Provide your username</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Password</label>
                                                        <div class="controls">
                                                            <input type="password" class="span6" id="pass_word" name="pass_word"/>
                                                            <span class="help-inline">Provide your password</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Confirm Password</label>
                                                        <div class="controls">
                                                            <input type="password" class="span6" id="conf_pass_word" name="conf_pass_word" />
                                                            <span class="help-inline">Confirm your password</span> </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                    <h4>Provide super administrator profile details</h4>
                                                    <div class="control-group">
                                                        <label class="control-label">First Name * </label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="first_name" name="first_name"/>
                                                            <span class="help-inline">Provide your first name</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Last Name</label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="last_name" name="last_name" />
                                                            <span class="help-inline">Provide your last name</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Email * </label>
                                                        <div class="controls">
                                                            <input type="email" class="span6" id="email" name="email"/>
                                                            <span class="help-inline">Provide your email address</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Gender</label>
                                                        <div class="controls">
                                                            <label class="radio">
                                                                <input type="radio" name="gender" value="Male" checked />
                                                                Male </label>
                                                            <label class="radio">
                                                                <input type="radio" name="gender" value="Female" />
                                                                Female </label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Mobile Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="mobile_number" name="mobile_number"/>
                                                            <span class="help-inline">Provide your phone number</span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Address</label>
                                                        <div class="controls">
                                                            <textarea class="span6" rows="3" id="address" name="address"></textarea>
                                                            <span class="help-inline">Provide your city or town</span> </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab3">
                                                    <h4>Provide site information</h4>
                                                    <div class="control-group">
                                                        <label class="control-label">Site Name * </label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="site_name" name="site_name"/>
                                                            <span class="help-inline"></span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Site Offline Message</label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="site_offline_msg" name="site_offline_msg"/>
                                                            <span class="help-inline"></span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Site E-mail </label>
                                                        <div class="controls">
                                                            <input type="text" class="span6" id="site_email" name="site_email" />
                                                            <span class="help-inline"></span> </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Site Description</label>
                                                        <div class="controls">
                                                            <textarea class="span6" rows="3" id="site_description" name="site_description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Site Keywords</label>
                                                        <div class="controls">
                                                            <textarea class="span6" rows="3" id="site_keyword" name="site_keyword"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Google Analytics Code</label>
                                                        <div class="controls">
                                                            <textarea class="span6" rows="3" id="analytics_code" name="analytics_code"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab4">
                                                    <div class="tab-pane active" id="tab1">
                                                        <h4>Provide database setting details</h4>
                                                        <div class="control-group">
                                                            <label class="control-label">Server *</label>                        
                                                            <div class="controls">
                                                                <input name="server_name" type="text" class="span6" id="server_name" value="localhost" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Database Name *</label>                        
                                                            <div class="controls">
                                                                <input type="text" class="span6" id="db_name" name="db_name" />
                                                                <span class="help-inline">May sure you have already created database</span>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Database username * </label>
                                                            <div class="controls">
                                                                <input type="text" class="span6" id="db_user_name" name="db_user_name"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Database Password</label>
                                                            <div class="controls">
                                                                <input type="password" class="span6" id="db_pass_word" name="db_pass_word" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Table Prefix </label>
                                                            <div class="controls">
                                                                <input type="text" class="span6" id="db_prefix" name="db_prefix"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions clearfix"> <a href="javascript:;" class="btn button-previous"> <i class="icon-angle-left"></i> Back </a> <a href="javascript:;" class="btn btn-primary blue button-next"> Continue <i class="icon-angle-right"></i> </a> <a href="javascript:;" class="btn btn-success button-submit"> Submit <i class="icon-ok"></i> </a> </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT--> 
                </div>
                <!-- END PAGE CONTAINER--> 
            </div>
            <!-- END PAGE --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div id="footer"> 2013 &copy; Amnil Technologies
            <div class="span pull-right"> <span class="go-top"><i class="icon-arrow-up"></i></span> </div>
        </div>
        <!-- END FOOTER --> 
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) --> 
        <!-- BEGIN CORE PLUGINS --> 
        <script src="assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
        <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip --> 
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <!--[if lt IE 9]>
           <script src="assets/plugins/excanvas.js"></script>
           <script src="assets/plugins/respond.js"></script>  
           <![endif]--> 
        <script src="assets/plugins/breakpoints/breakpoints.js" type="text/javascript"></script> 
        <!-- IMPORTANT! jquery.slimscroll.min.js depends on jquery-ui-1.10.1.custom.min.js --> 
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/jquery.blockui.js" type="text/javascript"></script> 
        <script src="assets/plugins/jquery.cookie.js" type="text/javascript"></script> 
        <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script type="text/javascript" src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
        <script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
        <!-- END PAGE LEVEL PLUGINS --> 
        <!-- BEGIN PAGE LEVEL SCRIPTS --> 
        <script src="assets/scripts/app.js"></script> 
        <script src="assets/scripts/form-wizard.js"></script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                App.init();
                FormWizard.init();

                $('.go-top').click(function () {
                    App.scrollTo($('.page-title'));
                });

                $('#form_wizard_1 .button-submit').unbind('click');
                $('#form_wizard_1 .button-submit').click(function (e) {
                    var server_name = $.trim($('#server_name').val());
                    var db_name = $.trim($('#db_name').val());
                    var db_user_name = $.trim($('#db_user_name').val());

                    if (server_name.length < 3)
                    {
                        alert("Please enter server name.");
                        $('#server_name').focus();
                        return false;
                    }

                    if (db_name.length < 5)
                    {
                        alert("Please enter database name of length greater than 5.");
                        $('#db_name').focus();
                        return false;
                    }

                    if (db_user_name.length < 3)
                    {
                        alert("Please enter database user name of length greater than 3.");
                        $('#db_user_name').focus();
                        return false;
                    }

                    $(this).parent('div').hide();
                    $('#frm-setup').submit();
                    return false;
                });
            });

            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    return false;
                },
                onNext: function (tab, navigation, index) {

                    if (index == 1)
                    {
                        var user_name = $.trim($('#user_name').val());
                        var pass_word = $('#pass_word').val();
                        var conf_pass_word = $('#conf_pass_word').val();

                        if (user_name.length < 5)
                        {
                            alert("Please enter user name of length greater than 4.");
                            $('#user_name').focus();
                            return false;
                        }

                        if (pass_word.length < 5)
                        {
                            alert("Please enter password of length greater than 4.");
                            $('#pass_word').focus();
                            return false;
                        }

                        if (pass_word != conf_pass_word)
                        {
                            alert("Password confirmation failed.");
                            $('#pass_word').focus();
                            return false;
                        }

                    }
                    else if (index == 2)
                    {
                        var first_name = $.trim($('#first_name').val());
                        var email = $.trim($('#email').val());

                        if (first_name.length < 6)
                        {
                            alert("Please enter first name of length greater than 5.");
                            $('#first_name').focus();
                            return false;
                        }

                        var email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                        if (!email_pattern.test(email))
                        {
                            alert("Please enter valid email address!");
                            $('#email').focus();
                            return false;
                        }

                    }
                    else if (index == 3)
                    {
                        var site_name = $.trim($('#site_name').val());
                        if (site_name.length < 6)
                        {
                            alert("Please enter site name of length greater than 5.");
                            $('#site_name').focus();
                            return false;
                        }
                    }

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }
                    App.scrollTo($('.page-title'));

                },
                onPrevious: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }

                    App.scrollTo($('.page-title'));
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.bar').css({
                        width: $percent + '%'
                    });
                }

            });

        </script> 
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>