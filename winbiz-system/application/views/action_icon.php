<?php if ($page_name == 'dynamic_form') { ?>
    <button class="btn btn-danger btn-success structureIcon"><i class="icon-plus icon-white"></i>Structure</button>
    <button class="btn btn-danger btn-success dataIcon"><i class="icon-plus icon-white"></i>Data</button>
    <?php
}
?>
<?php if ($add && $page_name != 'enquiry_log' && $page_name != 'product_review' && $page_name != 'users') { ?>
    <button class="btn btn-danger btn-success addIcon"><i class="icon-plus icon-white"></i> Create</button>
    <?php
} if ($edit) {
    if ($page_name != 'users') {
        ?>
        <button class="btn btn-danger btn-info editIcon"><i class="icon-pencil icon-white"></i> Edit</button>
    <?php }if ($page_name != 'enquiry_log' && $page_name != 'product_review') { ?>
        <button class="btn btn-danger btn-info publishIcon"><i class="icon-circle icon-white"></i> Publish</button>
        <button class="btn btn-danger btn-info unpublishIcon"><i class="icon-circle-blank icon-white"></i> Unpublish</button>
    <?php } ?>
<?php } if ($delete && $page_name != 'enquiry_log' && $page_name != 'product_review' && $page_name != 'users') { ?>
    <button class="btn btn-danger deleteIcon"><i class="icon-remove icon-white"></i> Delete</button>
<?php } ?>
