<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Email Management Grid</h4>
            </div>
            <div class="widget-body">
              <?php  if ($this->session->flashdata('class')): ?>
              <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                <button data-dismiss="alert" class="close">×</button>
                <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
              <?php  endif; ?>
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php $this->load->view('action_icon'); ?>
              </div>
            </div>
            <div class="widget-body">
              <form action="<?php echo site_url('manage_emails/') ?>" method="get" id="gridForm" autocomplete="off">
                <table id="box-table-a" summary="Employee Pay Sheet" class="table table-striped table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th width="20" scope="col">S.N.</th>
                      <th width="20" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                      <th width="115" scope="col">Enterprise</th>
                      <th width="230" soppe="col">Emails</th>
                      <th width="88" scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($rows) == 0 ){ ?>
                    <tr>
                      <td colspan="9" align="center" height="24">No Records Found! </td>
                    </tr>
                    <?php 
              			}else{ 
                      $i = 1;
              				foreach($rows as $row){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><input type="checkbox" name="selected[]" value="<?php echo $row->id ?>" class="rowCheckBox" /></td>
                        <td class="rightOperation" rel="<?php echo $row->id ?>"><?php echo $row->enterprise_name ?></td>
                        <td><?php echo $row->emails; ?></td>
                        <td>
                          <?php if($row->status == 1){ ?>
                          <a href="<?php echo site_url('manage_emails/change_status/0/'.$row->id) ?>" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>
                          <?php }else{ ?>
                          <a href="<?php echo site_url('manage_emails/change_status/1/'.$row->id) ?>" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>
                          <?php } ?>
                        </td>
                      </tr>
                      <?php
                      $i++;
                      }
              			}
              			?>
                  </tbody>
                </table>
              </form>
              <input type="hidden" id="hidden_view_product_url" value="<?php echo site_url("$page_name/view_enterprise_product_categories"); ?>" >
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<link href="assets/css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<div class="clear"> </div>
<!--<script type="text/javascript">
  var sort_url = "<?php echo site_url('category/sort_category') ?>";
  var site_url = "<?php echo site_url(); ?>";
</script> -->