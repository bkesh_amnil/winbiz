<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Menu Grid</h4>
            </div>
            <div class="widget-body">
                <?php  if ($this->session->flashdata('class')): ?>
              <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                <button data-dismiss="alert" class="close">×</button>
                <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
              <?php  endif; ?>
              <div class="actionsBar">
                <div class="actionIcons sortIcon" style="float:left; margin-right:5px;">
                	<a class="btn btn-danger btn-success addIcon" href="<?php echo site_url('menu/sort_menu') ?>" rel="sortFacebox" class="sortFaceboxt" title="Sort" onclick="return false;" id="menu"><i class="icon-sort icon-white"></i>Sort</a>
                </div>
                <?php $this->load->view('action_icon'); ?>
              </div>
              <div class="searchBar">
                <form action="<?php echo site_url('menu/') ?>" method="get" autocomplete="off">
                  <div class="searchBySite" style="width:250px;"> <span style="width:75px;">Site Name</span> <?php echo form_dropdown('site', $site_names, $site_id, 'id="sites" class="searchFld span12" style="width:150px;"') ?> </div>
                  <div class="searchBySite" style="width:250px;"> <span style="width:75px;">Menu Type</span> <?php echo form_dropdown('menutype', $menu_types, $menu_type, 'id="menutype" class="searchFld span12" style="width:150px;"') ?> </div>
                  <div class="searchByTitle" style="width:400px;"> <span style="width:100px;">Title/Keyword</span>
                    <input type="text" name="search" id="searchtext" value="<?php echo $this->input->get('search'); ?>" class="searchFld span12" style="width:150px;" />
                    <button type="submit" class="btn" id="btnSearch" style="margin-top:-10px;"><i class="icon-search icon-white"></i></button>
                  </div>
                  <div class="clear"></div>
                </form>
              </div>
            </div>
            <div class="widget-body">
              <form action="<?php echo site_url('menu/') ?>" method="get" id="gridForm" autocomplete="off">
                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th width="30" scope="col">S.N.</th>
                      <th width="25" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                      <th width="141" scope="col">Menu Name</th>
                      <th width="141" scope="col">Menu For</th>
                      <th width="100" scope="col">Created By </th>
                      <th width="112" scope="col">Created Date</th>
                      <th width="127" scope="col">Last Modified By</th>
                      <th width="166" scope="col">Last Modified Date</th>
                      <th width="49" scope="col">Status</th>
                      <!-- <th width="29" scope="col">Sort</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($rows)){ ?>
                    <tr>
                      <td colspan="10" align="center" height="24">No Records Found! </td>
                    </tr>
                    <?php }else{ 
				              echo $rows;
			              } ?>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET--> 
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clear"> </div>
<script type="text/javascript">
  var sort_url = "<?php echo site_url('menu/sort_menu') ?>/";
  var site_url = "<?php echo site_url(); ?>";
</script> 