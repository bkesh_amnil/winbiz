<script type="text/javascript">
    $(window).load(function () {
        $(".chosen-select").chosen()
    });
</script>
<?php if ($page_name != 'shop') { ?>
    <?php echo form_open('', 'name="search" method="get" class="FLeft"'); ?>
    <div class="searchBar">
        <div class="searchBySite">
            <?php $siteNames = $this->administrator_model->get_sites(); ?>
            <select name="site" id="sites" class="searchFld span12">
                <?php if (count($siteNames) != 1) { ?>
                    <option value="0">All Sites</option>
                <?php } foreach ($siteNames as $ind => $val) { ?>
                    <option value="<?php echo $val->id; ?>" <?php if ($this->input->get('site') == $val->id) { ?> selected="selected" <?php } ?>><?php echo $val->site_title; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="searchBySite">
            <?php
            $status_id = ($this->input->get('status')) ? $this->input->get('status') : '';
            echo form_dropdown('status', $status, $status_id, 'class="span12" id ="" ')
            ?>  
        </div>
        <div class="searchBySite">
            <!--<input type="text" name="search" id="search_providers" placeholder="Search By Providers">-->
            <?php
            $sel_user = ($this->input->get('user')) ? $this->input->get('user') : '';
//            echo form_input('user', $user, 'class="span12" id="search_users" placeholder="Search By Registered Users"')
            echo form_dropdown('user', $users, $sel_user, 'class="span12 chosen-select" id ="select_user_id" ')
            ?>
            
        </div>
        <div class="searchBySite">
            <?php
            $start = ($this->input->get('start')) ? $this->input->get('start') : '';
            $end = ($this->input->get('end')) ? $this->input->get('end') : '';
            ?>
            <input type="text" name="start" value="<?php echo $start ?>" class="span6 show_date_picker" id="" placeholder="Select Start Date<?php //echo date('Y-m-d')          ?>" />
            <input type="text" name="end" value="<?php echo $end ?>" class="span6 show_date_picker" id="" placeholder="Select End date<?php //echo date('Y-m-d')          ?>" />
        </div>
        <!--        <div class="searchByTitle">
                    <input type="text" placeholder="Search title" name="search" id="searchtext" value="<?php echo $this->input->get('search'); ?>" class="searchFld span12" />
                </div>-->
        <div class="searchByTitle">
            <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
        </div>
    </div>
    <?php echo form_close() ?>
<?php } ?>
<input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url() ?>"/>



