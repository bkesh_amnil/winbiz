<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          <?= $con_title ?>
        </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Content Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Select Site<span class="required">*</span></label>
                      <div class="controls"> <?php echo form_dropdown('site_id', $site_names, $site_id, 'class="span12" id="site_id"') ?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Content Title : *</label>
                      <div class="controls"> <?php echo form_input('title', $title, 'class="span12"') ?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Content Alias: *</label>
                      <div class="controls"> <?php echo form_input('alias', $alias, 'class="span12"') ?> </div>
                    </div>
                  </div>
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label" for="lastName">Category :</label>
                      <div class="controls">
                        <select name="category_id">
                         <?php
                          if(isset($categories) && !empty($categories) && is_array($categories)) {
                            foreach($categories as $cat) {
                              $selected = '';
                              if($cat->id == $category) {
                                $selected = ' selected="selected"';
                              }
                              ?>
                              <option value="<?php echo $cat->id; ?>"<?php echo $selected; ?>><?php echo $cat->category_name; ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                          <?php
                          /*$selectattrs["name"] = "category";
                          $selectattrs["class"] = "";
                          $selectattrs["rel"] = "testclsrel";
                          $optattrs["class"] = "optcls";
                          $optattrs["rel"] = "optrel";
                          echo get_category_dropdown($rows, "id", "category_id", $selectattrs, $optattrs, "Select.".$category);*/
                          ?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" for="lastName">Content Link :</label>
                      <div class="controls">
                        <?php echo form_input('content_link',  $content_link, 'class="span12" ') ?>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" for="lastName">Image:</label>
                      <div class="controls">
                        <?php if(!empty($title_image) && !is_dir('./uploaded_files/content/'.$title_image) && file_exists('./uploaded_files/content/'.$title_image)){ ?>
                        <img src="<?php echo site_url('upload/showImage?file='.$title_image.'&type=content&width=200&height=150'); ?>" width="200" height="150" /><br />
                        <!-- Change This Image --><br />
                        <?php } ?>
                        <?php echo form_upload('title_image', $title_image,'class="span12" ') ?> </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Image Status:</label>
                      <div class="controls">
                        <label class="radio"> <?php echo form_radio('imagestatus', "yes", (($imagestatus == "yes" || empty($imagestatus)) ? "checked" : "")) ?> Yes</label>
                        <label class="radio"><?php echo form_radio('imagestatus', "no", (($imagestatus == "no") ? "checked" : "")) ?> No</label>
                      </div>
                    </div>
                    </div>
                </div>
                <div class="row-fluid">
                  <div class="span12">
                    <div class="control-group">
                      <label class="control-label">Content</label>
                      <div class="controls">
                        <?php
                					include_once('assets/ckeditor/ckeditor.php');
                					include_once('assets/ckfinder/ckfinder.php');
                					$ckeditor = new CKEditor();
                					$ckeditor->basePath = site_url().'assets/ckeditor/';
                					CKFinder::SetupCKEditor($ckeditor, site_url().'assets/ckfinder/');
                					$ckeditor->editor('description', $description);
                				?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Status :</label>
                      <div class="controls">
                        <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                        <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
          <?php $this->load->view("user_detail_common"); ?>
          <div class="span3">
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Keywords</h4>
              </div>
              <div class="widget-body form">
                <div class="control-group">
                  <label for="Keywords For Content" class="control-label">Keywords For Content:</label>
                  <div class="controls"> <?php echo form_textarea('keywords', $keywords, 'class="span12"') ?> </div>
                  <div class="control-group">
                    <label for="Description For Conten" class="control-label">Description For Content :</label>
                    <div class="controls"> <?php echo form_textarea('metadescription', $metadescription, 'class="span12"') ?> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- END CONTENT--> 

<script type="text/javascript">
	$(document).ready(function(e) {
    $("input[name=title]").keyup(function(e) {
      var txtValue = $(this).val();
      var newValue = txtValue.toLowerCase().replace(/[!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi,'-');
      $("input[name=alias]").val(newValue);
    });
  	$('#btn_submit').click(function(e) {
			$('#Save').val("true");
        $('#form1').submit();
    });	
		/*$("#categoryAjax").autocomplete(
		  "<?php echo base_url(); ?>category/autocomplete",
		  {
				delay:4,
				minChars:1,
				matchSubset:1,
				matchContains:1,
				cacheLength:10,
				onItemSelect:selectItem,
				onFindValue:findValue,
				formatItem:formatItem,
				autoFill:true
			}
		);*/	
	});
	/*function findValue(li) {
		if( li == null ) return alert("No match!");
		// if coming from an AJAX call, let's use the CityId as the value
		if( !!li.extra ) var sValue = li.extra[0];
		// otherwise, let's just display the value in the text box
		else var sValue = li.selectValue;
		//alert("The value you selected was: " + sValue);
	}
  
	function selectItem(li) {
		  findValue(li);
	}
  
	function formatItem(row) {
		  return row[0];
	}
  
	function lookupAjax(){
	  var oSuggest = $("#CityAjax")[0].autocompleter;
	  oSuggest.findValue();
	  return false;
	}
  
	function lookupLocal(){
		  var oSuggest = $("#CityLocal")[0].autocompleter;
  
		  oSuggest.findValue();
  
		  return false;
	}*/
</script> 