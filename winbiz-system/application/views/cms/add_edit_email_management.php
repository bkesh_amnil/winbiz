<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><?= $con_title ?></h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?= $con_title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php $this->load->view("show_errors"); ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Email Management Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Enterprise Name  : *</label>
                      <div class="controls">
                        <?php
                        if(isset($profile_enterprise_name) && !empty($profile_enterprise_name)) {
                          echo form_input('enterprise_name', $profile_enterprise_name, 'class="span12" id="enterprise" readonly');
                        } else {
                          echo form_input('enterprise_name', $enterprise_name, 'class="span12" id="enterprise"');
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Email  : *(Multiple email seperated by commas)</label>
                      <div class="controls"><?php echo form_input('emails', $emails, 'class="span12"') ?></div>
                    </div>
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Status :</label>
                      <div class="controls">
                        <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                        <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <input type="hidden" name="id" value="<?php echo $id; ?>" />
                  <input type="hidden" name="site_id" value="<?php echo $siteId ?>" />
                  <input type="hidden" name="save" value="" id="Save" />                  
                  <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                </div>
                <input type="hidden" id="remove_image_url" value="<?php echo site_url("$page_name/remove_image"); ?>" >
              </div>
            </div>
          </div>
          <?php $this->load->view("user_detail_common"); ?>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    $("#btn_submit").click(function(e) {
      $('#Save').val("true");
      $("#form1").submit();
    });	
    $("#enterprise").autocomplete(
      "<?php echo base_url(); ?>enterprise/autocomplete",
      {
        delay:1,
        minChars:1,
        matchSubset:1,
        matchContains:1,
        cacheLength:10,
        onItemSelect:selectItem,
        onFindValue:findValue,
        formatItem:formatItem,
        autoFill:true
      }
    );  
  });
  /* autocomplete functions */
  function findValue(li) {
    if( li == null ) return alert("No match!");
    // if coming from an AJAX call, let's use the CityId as the value
    if( !!li.extra ) var sValue = li.extra[0];
    // otherwise, let's just display the value in the text box
    else var sValue = li.selectValue;
    //alert("The value you selected was: " + sValue);
  }
  
  function selectItem(li) {
      findValue(li);
  }
  
  function formatItem(row) {
      return row[0];
  }
  /* autocomplete functions */
</script>