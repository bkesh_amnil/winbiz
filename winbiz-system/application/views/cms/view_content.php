<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Content Grid</h4>
            </div>
            <div class="widget-body">
              <?php  if ($this->session->flashdata('class')): ?>
              <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                <button data-dismiss="alert" class="close">×</button>
                <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
              <?php  endif; ?>
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php $this->load->view('action_icon'); ?>
              </div>
            </div>
            <div class="widget-body">
              <form action="<?php echo site_url('content/') ?>" method="get" id="gridForm" autocomplete="off">
                <table id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th width="31" scope="col">S.N.</th>
                      <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                      <th width="230" scope="col">Content Title</th>
                      <th width="150" scope="col">Content For</th>
                      <th width="139" scope="col">Created By </th>
                      <th width="162" scope="col">Created Date</th>
                      <th width="159" scope="col">Last Modified By</th>
                      <th width="186" scope="col">Last Modified Date</th>
                      <th width="88" scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($rows) == 0 ){ ?>
                    <tr>
                      <td colspan="9" align="center" height="24">No Records Found! </td>
                    </tr>
                    <?php 
              			}else{ 
              				$i = $start + 1; 
              				foreach($rows as $row){
                        $serLink = site_url('content?site='.$row->site_id);
                			?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                        <td class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->title ?></td>
                        <td><a href="<?php echo $serLink ?>"><?php echo $row->site_title ?></a></td>
                        <td><?php echo $this->admin_user_model->display_name($row->created_by) ?></td>
                        <td><?php echo $row->created_date ?></td>
                        <td><?php echo $this->admin_user_model->display_name($row->updated_by) ?></td>
                        <td><?php echo $row->updated_date ?></td>
                        <td>
                          <?php if($row->status == "1"){ 	?>
                          <a href="<?php echo site_url('content/change_status/0/'.$row->id) . get_url(); ?>" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>
                          <?php }else{ ?>
                          <a href="<?php echo site_url('content/change_status/1/'.$row->id) . get_url(); ?>" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>
                          <?php } ?>
                        </td>
                      </tr>
                      <?php 
            					$i++; 
              				}
            			 }
			             ?>
                  </tbody>
                </table>
              </form>
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<link href="css/admincss/jquery.contextMenu.css" rel="stylesheet" type="text/css" />