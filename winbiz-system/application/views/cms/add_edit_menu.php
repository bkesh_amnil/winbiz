<style type="text/css">
  th {
    text-align: left;
    vertical-align: top;
    height: 30px;
  }

  #site_offline_message {
    display: none;
  }
  textarea {
    height: 50px;
  }
  .menuListings, .bannerListings {
    padding: 5px;
    margin: 5px;
    border: 1px solid #ccc;
    display:block;
  }
  .menuList {
    list-style: none;
    padding: 10px;
    margin: 0;
  }
  .setingsRow td {
    padding-top: 3px;
  }
  .div_main_content {
    margin-bottom: 10px;
    border: 1px solid #ccc;
    padding: 10px 0;
    overflow:hidden;
    margin-right:1%;
  }
  .add_module, .addRemove {
    padding: 5px;
    color: #156ABF;
    font-weight: bold;
    cursor: pointer;
  }
  .selectModule {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
  }
  .selectCategory {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
  }
  .selectCategory span, .selectModule span {
    display: block;
    float: left;
    font-weight: bold;
  }
  .addRemove {
    border: 1px solid;
    border-radius: 5px 5px 5px 5px;
    display: table-row;
    float: right;
    height: 19px;
    margin-right: 30px;
    margin-top: 13px;
    padding-top: 7px;
    vertical-align: middle;
    width: 68px;
  }
  .addRemove:hover {
    background-color: #156ABF;
    color: #fff;
  }
  .div_module_detail {
    padding: 10px;
  }
  .menuListings ul, .bannerListings ul, .div_module_detail ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  .menuListings ul li {
    width: auto;
    padding: 5px;
    margin: 3px;
    border:1px dashed #ccc;
    float:left;
  }
  .div_module_detail ul li, .bannerListings ul li {
    width: auto;
    padding: 5px;
    border: 1px dashed #ccc;
    float: left;
    margin: 3px;
  }
  .bannerListings ul li {
    cursor: pointer;
  }
  .selectedRow {
    background-color: #0994D7;
  }
  .contentListModules li:hover, .menuListRow:hover {
    background-color: #F1F1F1;
    cursor: pointer;
  }
  .collapsed {
    background-color: #BFC;
  }
  .bannerListings .ui-sortable-placeholder {
    height: 64px !important;
    width: 96px;
    background: none repeat scroll 0 0 #EBEBEB;
    border: 1px dashed #CCCCCC;
  }
  .div_module_detail .ui-sortable-placeholder {
    height: 21px !important;
    width: 96px !important;
    background: none repeat scroll 0 0 #EBEBEB;
    border: 1px dashed #CCCCCC;
  }
  .div_module_detail .ui-sortable-helper {
    height: 21px !important;
    width: 96px !important;
    background: none repeat scroll 0 0 #EBEBEB;
    border: 1px dashed #CCCCCC;
  }
</style>
<!-- CONTENT START -->

<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><?php echo $title ?></h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Menu Form</h4>
              </div>
              <div class="widget-body form">
                <div class="CMSform">
                  <h3 class="form-section">Menu Details</h3>
                  <div class="row-fluid">
                    <div class="span6">
                      <div class="control-group">
                        <label class="control-label">Select Site : *</label>
                        <div class="controls">
                          <?= form_dropdown('site_id', $site_names, $site_id, 'class="span12" id="site_id"') ?>
                        </div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Type:" class="control-label">Menu Type: *</label>
                        <div class="controls"> <?php echo form_dropdown('menu_type_id', $menu_types, $menu_type_id, 'class="span12" id="menuType"'); ?> </div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Title" class="control-label">Menu Title: *</label>
                        <div class="controls"> <?php echo form_input('menu_title', $menu_title, 'class="span12"') ?> </div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Alias" class="control-label">Menu Alias : *</label>
                        <div class="controls"> <?php echo form_input('menu_alias', $menu_alias, 'class="span12"')// . ' .' . $this->global_model->setting('domain_name') ?> </div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Alias" class="control-label">Menu Parent : *</label>
                        <?php 
                    if($menu_type_id > 0){
                        $availableMenus = $this->administrator_model->getAvailableMenus($menu_type_id, $menu_parent);
                    }else{
                        $availableMenus = '<option value="">Parent</option>'; 
                    }
                    ?>
                        <div class="controls">
                          <select name="menu_parent" class="span12" id="parentList">
                            <?php echo $availableMenus; ?>
                          </select>
                        </div>
                      </div>
                      
                    </div>
                    <div class="span6">
                      <div class="control-group">
                        <label for="Menu Type:" class="control-label">Page Title For Menu: *</label>
                        <div class="controls"><?php echo form_input('page_title', $page_title, 'class="span12"') ?></div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Type:" class="control-label">Keywords For Menu:</label>
                        <div class="controls"> <?php echo form_input('menu_keywords', $menu_keywords, 'class="span12"') ?> </div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Type:" class="control-label">Heading For Menu: *</label>
                        <div class="controls"> <?php echo form_input('menu_heading', $menu_heading, 'class="span12"') ?></div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Type:" class="control-label">Description For Menu :</label>
                        <div class="controls"> <?php echo form_textarea('menu_description', $menu_description, 'class="span12"') ?></div>
                      </div>
                      <div class="control-group">
                        <label for="Menu Alias" class="control-label">Status : *</label>
                        <div class="controls"> <?php echo form_dropdown('status', $statuses, $status, 'class="span12"') ?> </div>
                      </div>
                    </div>
                  </div>
                  <h3 class="form-section">Menu Page Settings:</h3>
                  <div class="row-fluid">
                    <div class="span6">
                      <div class="control-group">
                        <label class="control-label">Menu Link Type : *</label>
                        <div class="controls">
                          <?php
                $menu_link_types['none']    = "None";
                $menu_link_types['url']     = "URL";
                $menu_link_types['page']    = "Page";
                $menu_link_types['site']    = "Site";
                
              ?>
                          <?php
                          if(empty($menu_link_type) || $menu_link_type == "none"){
                              $pageDisplay = 'display:none;';
                              $urlDisplay = 'display:none;';
                              $siteDisplay = 'display:none;';
                          }else if($menu_link_type == "url"){
                              $pageDisplay = 'display:none;';
                              $urlDisplay = 'display:table-row;';
                              $siteDisplay = 'display:none;';
                          }else if($menu_link_type == "page"){
                              $pageDisplay = 'display:table-row;';
                              $urlDisplay = 'display:none;';
                              $siteDisplay = 'display:none;';
                          }else if($menu_link_type == "site"){
                              $pageDisplay = 'display:none;';
                              $urlDisplay = 'display:none;';
                              $siteDisplay = 'display:table-row;';
                          }
                        ?>
                        <?php echo form_dropdown('menu_link_type', $menu_link_types, $menu_link_type, 'class="span12" id="menu_link_type"') ?> </div>
                      </div>
                    </div>
                    <div class="span6">
                      <div class="control-group">
                            <label class="control-label">Link Opens In : *</label>
                            <div class="controls"> <?php echo form_dropdown('menu_open', $menu_opens, $menu_open, 'class="span12" id="menu_link_type"') ?> </div>
                        </div>
                    </div>
                  </div>
                  <div class="row-fluid">
                    <div class="span12">
                    <table width="100%">
                        <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                           <td colspan="2">
                              <div class="span6">
                                    <div class="control-group">
                                        <label class="control-label">Display Side Menu : *</label>
                                        <div class="controls"> <?php echo form_dropdown('side_menu_id', $menu_types, $side_menu_id, 'class="span12" id="side_menu"') ?>
                                          <input type="hidden" name="sideMenuIds" id="sideMenuIds" value="<?php echo $side_menus; ?>" />
                                        </div>
                                    </div>
                                </div>
                           </td>
                      </tr>
                      <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <td colspan="2" id="sideMenuSettings">
            <?php
              if($side_menu_id > 0){
              ?>
                        <div class="menuListings">
                          <div> <span style="float:left;">Menu Title : </span>
                            <input type="text" class="span12" style="float:left; margin-left:10px;" name="side_title" value="<?php echo $side_menu_title; ?>" />
                          </div>
                          <div style="clear:both"></div>
                          <?php echo $sideMenus; ?> </div>
                        <?php
                      }
                  ?>
                    </td>
                    </tr>
                    <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <td colspan="2">
            <div class="span6">
                          <div class="control-group">
              <input type="hidden" name="footerMenuIds" id="footerMenuIds" value="<?php echo $footer_menus; ?>" />
                            <label for="Menu Title" class="control-label">Display Footer Menu : </label>
                            <div class="controls">
                              <?php echo form_dropdown('footer_menu_id', $menu_types, $footer_menu_id, 'class="span12" id="footer_menu"') ?>
                                <input type="hidden" name="footerMenuIds" id="footerMenuIds" value="<?php echo $footer_menus; ?>" />

                            </div>
                          </div>
                      </div>
            
                        </td>
                    </tr>
                    <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <td colspan="2" id="footerMenuSettings"><?php
                    if($footer_menu_id > 0){
                  ?>
                        <div class="menuListings">
                          <div style=""><span style="float:left;">Menu Title : </span>
                            <input type="text" class="" style="float:left; margin-left:10px;" name="footer_title" value="<?php echo $footer_menu_title; ?>" />
                          </div>
                          <div style="clear:both"></div>
                          <?php echo $footerMenus; ?> </div>
                        <?php
                    }
                ?></td>
                    </tr>
                    <!-- <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <td colspan="2">
                          <div class="span6">
                              <div class="control-group">
                                <input type="hidden" name="selectedBanners" id="selectedBanners" value="<?php echo $selected_banners; ?>" />
                                <label for="Menu Title" class="control-label">Display Banner : * </label>
                                <div class="controls">
                                    <?php echo form_dropdown('banner', $banners, $banner, 'class="span12" id="display_banner"') ?>
                                </div>
                              </div>
                          </div>
                       </td>
                    </tr>
                    <tr class="setingsRow page">
                      <td colspan="2" id="bannerSettings" <?php echo ($banner)?'':'style="display:none"';?>>
                      <?php echo $availabeBanners;?>
                      </td>
                    </tr> -->
                    <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <th scope="row">Main Content Modules Setting: </th>
                      <td><span class="add_module" length="<?php echo count($settings); ?>">[Add]</span></td>
                    </tr>
                    <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                      <td colspan="2" id="moduleSettings"><?php 
                    if(isset($settings) && is_array($settings) && count($settings) > 0){ 
                        $i = 0; 
                        foreach($settings as $setting){
                                  //printr($settings); 
                            if($i > 0){
                                $removeCls = " remove_module";
                                $removeText = "Remove";
                            }else{
                                $removeCls = "";
                                $removeText = "";  
                            }
                    ?>
                        <div class="div_main_content" position="<?php echo $i ?>">
                          <?php
                        if(isset($setting->category_id) && intval($setting->category_id) > 0){
                            $categoryName = $this->administrator_model->getCategoryAlias($setting->category_id);
                        }
                        else
                        {
                            $categoryName="";
                        }
                    ?>
                          <div class="selectModule"> <span>Module Title</span><br />
                            <span>
                            <input type="text" class="span12" name="module_title[<?php echo $i; ?>]" value="<?php echo isset($setting->module_title) ? $setting->module_title : (isset($setting["module_title"]) ? $setting["module_title"] : "" ); ?>" />
                            </span> </div>
                          <div class="selectModule"> <span>Select Module</span><br />
                            <span>
                            <?php
                            $moduleId = isset($setting->module_id) ? $setting->module_id : (isset($setting["module_id"]) ? $setting["module_id"] : "");
                            echo form_dropdown("module_id[$i]", $modules, $moduleId, 'class="span12 module_id"') ?>
                            </span> </div>
                          <div class="selectCategory"> <span>Select Category</span><br />
                            <span>
                            <?php
                            echo form_dropdown("category_id[$i]", $categories, $categoryName, 'class="span12 category_id"') ?>
                            </span> </div>
                          <input type="hidden" class="selectedValue selectedModulesIds_<?php echo $i; ?>" name="selectedModulesIds[<?php echo $i; ?>]" value="<?php echo(isset( $setting->content_ids)? $setting->content_ids : "" ) ;?>" />
                          <?php if(!empty($removeCls)){ ?>
                          <span class="addRemove<?php echo $removeCls; ?>"><?php echo $removeText; ?></span>
                          <?php } ?>
                          <div style="clear:both"></div>
                          <div class="div_module_detail">
                            <?php
                        if(isset($setting->module_id) && isset ($setting->content_ids) && isset ($categoryName)){
                            $this->administrator_model->get_module_contents($site_id, $setting->module_id, $i, $setting->content_ids, $categoryName);
                        }else if(isset($setting["module_id"]) && isset ($setting["content_ids"]) && isset ($categoryName)){
                            $this->administrator_model->get_module_contents($site_id, $setting["module_id"], $i, $setting["content_ids"], $categoryName);
                        }
                    ?>
                          </div>
                          <div style="clear:both"></div>
                        </div>
                        <?php $i++; }  
        }else{
                ?>
                        <div class="div_main_content" position="0">
                          <div class="selectModule"> <span>Module Title</span><br />
                            <span>
                            <input type="text" class="span12" name="module_title[0]" value="" />
                            </span> </div>
                          <div class="selectModule"> <span>Select Module</span><br />
                            <span> <?php echo form_dropdown("module_id[0]", $modules, '', 'class="span12 module_id"') ?> </span> </div>
                          <div class="selectCategory"> <span>Select Category</span><br />
                            <span> <?php echo form_dropdown("category_id[0]", $categories, '', 'class="span12 category_id"') ?> </span> </div>
                          <input type="hidden" class="selectedValue selectedModulesIds_0" name="selectedModulesIds[0]" value="" />
                          <span class="addRemove"></span>
                          <div style="clear:both"></div>
                          <div class="div_module_detail"></div>
                          <div style="clear:both"></div>
                        </div>
                        <?php 
                    } 
                ?></td>
                    </tr>
                    <tr class="setingsRow url" style="<?php echo $urlDisplay; ?>">
                      <td colspan="2">
                <div class="span6">
                              <div class="control-group">
                                <label for="Menu Title" class="control-label">Url For The Menu:</label>
                                <div class="controls">
                                    <?php echo form_input('menu_url', $menu_url, 'class="span12"') ?>
                                </div>
                              </div>
                            </div>
            </td>
                    </tr>
                    <tr class="setingsRow site" style="<?php echo $siteDisplay; ?>">
                      <td colspan="2">
                <div class="span6">
                              <div class="control-group">
                                <label for="Menu Title" class="control-label">Site Link For The Menu:</label>
                                <div class="controls">
                                    <?php echo form_dropdown('menu_site', $site_names, $menu_site, 'class="span12"') ?>
                                </div>
                              </div>
                            </div>
            </td>
                    </tr>
                  </table>
                    </div>
                  </div>
                  <div class="form-actions">
                  <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <input type="hidden" name="save" value="" id="Save" />
                  <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> 
                  <a class="btn" href="<?= site_url("$page_name/") . get_url()?>"><span>Cancel</span></a> </div>
                </div>
              </div>
            </div>
          </div>
          <?php
          $this->load->view("user_detail_common");
          ?>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!-- END CONTENT-->

<div class="clear"> </div>
<script type="text/javascript">
  $(document).ready(function() {
     $("input[name=menu_title]").keyup(function(e) {
        var txtValue = $(this).val();
        var newValue = txtValue.toLowerCase().replace(/[!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi,'-');
        $("input[name=menu_alias]").val(newValue);
      });
  })
  var site_url = "<?php echo site_url(); ?>";
  var base_url = "<?php echo base_url(); ?>";
  var siteId  = "<?php echo $siteId; ?>";
</script> 
