
<?php $this->load->view('advertisement/common_scripts.php'); ?>
<?php if ($page_name != 'shop') { ?>
    <?php echo form_open('', 'name="search" method="get" class="FLeft"'); ?>
    <div class="searchBar">
        <?php if ($page_name != 'enterprise' && $page_name != 'product_category') { ?>
            <div class="searchBySite">
                <?php $siteNames = $this->administrator_model->get_sites(); ?>
                <select name="site" id="sites" class="searchFld span12">
                    <?php if (count($siteNames) != 1) { ?>
                        <option value="0">All Sites</option>
                    <?php } foreach ($siteNames as $ind => $val) { ?>
                        <option value="<?php echo $val->id; ?>" <?php if ($this->input->get('site') == $val->id) { ?> selected="selected" <?php } ?>><?php echo $val->site_title; ?></option>
                    <?php } ?>
                </select>
            </div>
        <?php } ?>
        <div class="searchBySite">
            <!--<input type="text" name="search" id="search_providers" placeholder="Search By Providers">-->
            <?php
            $provider_id = ($this->input->get('provider')) ? $this->input->get('provider') : '';
            echo form_dropdown('provider', $providers, $provider_id, 'class="span12" id ="select_provider_id" ')
            ?>  
        </div>
        <div class="searchBySite">
            <?php
            $start = ($this->input->get('start')) ? $this->input->get('start') : '';
            $end = ($this->input->get('end')) ? $this->input->get('end') : '';
            ?>
            <input type="text" name="start" value="<?php echo $start ?>" class="span6 show_date_picker" id="" placeholder="Select Start Date<?php //echo date('Y-m-d') ?>" />
            <input type="text" name="end" value="<?php echo $end ?>" class="span6 show_date_picker" id="" placeholder="Select End date<?php //echo date('Y-m-d') ?>" />
        </div>
        <?php if ($page_name != 'product_review' && $page_name != 'enquiry_log' && $page_name != 'advertisement_price') { ?>
            <div class="searchByTitle">
                <input type="text" placeholder="Search title" name="search" id="searchtext" value="<?php echo $this->input->get('search'); ?>" class="searchFld span12" />
            </div>
        <?php } ?>
        <div class="searchByTitle">
            <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
        </div>
    </div>
    <?php echo form_close() ?>
<?php } else { ?>
    <?php echo form_open('', 'name="search" method="post" class="FLeft"'); ?>
    <div class="searchBar">
        <div class="searchBySite">
            <?php $siteNames = $this->administrator_model->get_sites(); ?>
            <select name="site" id="sites" class="searchFld span12">
                <?php if (count($siteNames) != 1) { ?>
                    <option value="0">All Sites</option>
                <?php } foreach ($siteNames as $ind => $val) { ?>
                    <option value="<?php echo $val->id; ?>" <?php if ($this->input->get('site') == $val->id) { ?> selected="selected" <?php } ?>><?php echo $val->site_title; ?></option>
                <?php } ?>
            </select>
        </div>
        <select name="srch_zones" id="srch_zones">
            <option>Zones</option>
            <?php
            foreach ($zones as $zone) {
                $selected = '';
                if ($zone->ZoneID == $srch_zone)
                    $selected = 'selected="selected"';
                ?>
                <option value="<?php echo $zone->ZoneID ?>" <?php echo $selected ?>><?php echo $zone->ZoneCode ?></option>
            <?php } ?>
        </select>
        <select name="srch_districts" id="srch_districts">
            <option>Districts</option>
            <?php
            foreach ($all_districts as $district) {
                $selected = '';
                if ($district->DistrictID == $srch_district)
                    $selected = 'selected="selected"';
                ?>
                <option value="<?php echo $district->DistrictID ?>" <?php echo $selected ?>><?php echo $district->DistrictCode ?></option>
            <?php } ?>
        </select>
        <select name="srch_city" id="srch_city">
            <option>City</option>
            <?php
            foreach ($all_cities as $city) {
                $selected = '';
                if ($city->CityID == $srch_city)
                    $selected = 'selected="selected"';
                ?>
                <option value="<?php echo $city->CityID ?>" <?php echo $selected ?>><?php echo $city->CityCode ?></option>
            <?php } ?>
        </select>
        <select name="srch_area" id="srch_area">
            <option>Area</option>
            <?php
            foreach ($all_areas as $area) {
                $selected = '';
                if ($area->AreaID == $srch_areas)
                    $selected = 'selected="selected"';
                ?>
                <option value="<?php echo $area->AreaID ?>"><?php echo $area->AreaCode ?></option>
            <?php } ?>
        </select>
        <input type="text" name="srch_text" id="srch_text" value="<?php echo $srch_srchtxt ?>">
        <div>
            <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
        </div>
        <div class="clear"></div>
    </div>
    <?php echo form_close() ?>
<?php } ?>
<input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url() ?>"/>

