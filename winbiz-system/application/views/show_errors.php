<div class="message_div">
<?php if(isset($error_mess) && !empty($error_mess)) {  ?>
<div class="alert  alert-error">
<a data-dismiss="alert" class="close alert-close">×</a>
<span class="info_inner">
<?php	echo $error_mess; ?>
</span>
</div>
<?php } ?>
  <?php  
	$errors = validation_errors(); 
	if($errors){
		echo $errors;
	}
	if ($this->session->flashdata('class')): 
	?>
  <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
    <a data-dismiss="alert" class="close alert-close">×</a>
    <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
  <?php  
	endif; 
	?>
</div>
