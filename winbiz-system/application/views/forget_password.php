
  <div class="grid_6 prefix_5 suffix_5">
   	  <h1> Reset My Password</h1>
    	<div id="login">
    	  <p class="tip">You need to enter both your username and  email address!</p>
          <?php if($this->session->flashdata('class')): ?>
          <p class="<?= $this->session->flashdata('class') ?>"><?= $this->session->flashdata('login') ?></p>        
          <?php endif; ?>
    	  <form id="form1" name="form1" method="post" action="" autocomplete="off">
    	    <p>
    	      <label><strong>Username</strong>
<input type="text" name="user_name" class="inputText" id="user_name" />
    	      </label>
  	      </p>
    	    <p>
    	      <label><strong>E - mail </strong>
                <input type="email" name="email" class="inputText" id="email" />
  	        </label>
    	    </p>
    		<a class="black_button" style="width:120px;"><span>Reset My Password</span></a>
    	  </form>
		  <br clear="all" />
    	</div>
        <div id="forgot">      
        <?= anchor('login', '<span>Know your username or password?</span>', 'class="forgotlink"'); ?>
         
        </div>
  </div>

<br clear="all" />

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#user_name').focus();
		
		$('#user_name, #pass_word').keypress(function(e) {
            if(e.keyCode == 13)
			 $('#form1').submit();
        });
		
        $('.black_button').click(function(e) {
            $('#form1').submit();
        });
    });

</script>