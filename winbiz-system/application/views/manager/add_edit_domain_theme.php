<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          <?= $con_title ?>
        </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Domain Theme Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Domain Theme Name : *</label>
                      <div class="controls"> <?php echo form_input('domain_theme_name', $domain_theme_name, 'class="span12"') ?> </div>
                    </div>
            	   </div>
            	   <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Domain Theme Code: *</label>
                      <div class="controls"> <?php echo form_input('domain_theme_code', $domain_theme_code, 'class="span12"') ?> </div>
                   </div> 
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Status :</label>
                      <div class="controls">
                        <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                        <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
          <?php $this->load->view("user_detail_common"); ?>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END CONTENT-->
<script type="text/javascript">
	$(document).ready(function(e) {
		$("#btn_submit").click(function(e) {
        $('#form1').submit();
      });
	});
</script>