<style type="text/css">
 th { text-align: left; vertical-align:top; height: 35px;}	
 .span12 { width: 200px; } 
 select.span12 { width: 208px; } 
</style>
<!-- CONTENT START -->
<div class="grid_16" id="content"> 
  <!--  TITLE START  -->
  <div class="grid_9">
    <h1 class="settings"><?= $title ?></h1>
  </div>
  <div class="clear"> </div>
  <!--  TITLE END  --> 
  
  <!-- #PORTLETS START -->
  <div id="portlets"> 
    
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
      <div class="portlet-content-edited"> <?php echo validation_errors(); ?>
        <form action="" method="post" autocomplete="off" style="width:320px;" id="form1">
          <table width="529" height="71" border="0">
            <tr>
              <th height="21" scope="row">Dropdown Display Text: *</th>
              <td><?php echo form_input('dropdown_text', $dropdown_text, 'class="span12"') ?></td>
            </tr>
            <tr>
              <th height="21" scope="row">Dropdown Key : *</th>
              <td><?php echo form_input('dropdown_key', $dropdown_key, 'class="span12"') ?></td>
            </tr>
            <tr>
              <th width="171" height="21" scope="row">&nbsp;</th>
              <td width="348"><a class="button" id="btn_submit"><span>Submit this form</span></a> <a class="button_grey" href="<?= site_url("$page_name/") ?>"><span>Cancel this form</span></a></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="clear"> </div>
<!-- END CONTENT-->

<div class="clear"> </div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
	});
</script> 
