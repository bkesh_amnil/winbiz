<style type="text/css">
th {
	text-align: left;
	vertical-align:top;
	height: 30px;
}
.span12 {
	width: 200px;
}
select.span12 {
	width: 208px;
}
.container_16 .grid_9 {
	width:700px !important;
}
</style>
<!-- CONTENT START -->
<div class="grid_16" id="content"> 
  <!--  TITLE START  -->
  <div class="grid_9">
    <h1 class="settings">
      <?= $title ?>
    </h1>
  </div>
  <div class="clear"> </div>
  <!--  TITLE END  --> 
  
  <!-- #PORTLETS START -->
  <div id="portlets"> 
    
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
      <div class="portlet-content-edited"> <?php echo validation_errors(); ?>
        <form action="" method="post" autocomplete="off" style="width:320px;" id="form1">
          <div style="width:881px;" id="div_table">
            <div class="row">
              <table width="881" border="0" style="margin-bottom:5px;">
                <tr>
                  <th width="182" height="21" scope="row">Dropdown Display Text: *</th>
                  <td width="228"><?php echo form_input('dropdown_detail_text[]', $dropdown_detail_text, 'class="span12"') ?></td>
                  <th width="138" scope="row">Dropdown Value : *</th>
                  <td width="218"><?php echo form_input('dropdown_detail_key[]', $dropdown_detail_key, 'class="span12"') ?></td>
                  <td width="93" style="vertical-align:middle">&nbsp;
                  <?php if( $dropdowndetails_id == 0) { ?>
                  <a href="#" onclick="return false;" class="add_dropdowns">[Add]</a>
                  <?php } ?>
                   </td>
                </tr>
              </table>
            </div>
          </div>
          <table width="881" height="48" border="0">
            <tr>
              <th width="183" height="21" scope="row">&nbsp;</th>
              <td width="252"><a class="button" id="btn_submit"><span>Submit this form</span></a> <a class="button_grey" href="<?= site_url("$page_name/manage_dropdowns/$dropdown_id") ?>"><span>Cancel this form</span></a></td>
              <th width="149" scope="row">&nbsp;</th>
              <td width="279">&nbsp;</td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="clear"> </div>
<!-- END CONTENT-->

<div class="clear"> </div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
		
		$('.add_dropdowns').click(function(e) {
            $('#div_table').append($('.row:first').html());
			$('.add_dropdowns:gt(0)').html('[Remove]').attr('class', 'remove_dropdown');
        });
		
		$('.remove_dropdown').live('click', function(){
			$(this).parents('table').remove();
		})
	});
</script> 
