<!-- CONTENT START -->

<div class="grid_16" id="content"> 
  <!--  TITLE START  -->
  <div class="grid_9">
    <h1 class="settings"><?= $title ?></h1>
  </div>
  <div class="clear"> </div>
  <!--  TITLE END  --> 
  
  <!-- #PORTLETS START -->
  <div id="portlets">
    <div class="column"> </div>
    <?php  if ($this->session->flashdata('class')): ?>
    <p class="info" id="<?php echo $this->session->flashdata('class') ?>"><span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span></p>
    <?php  endif; ?>
    
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
      <div class="portlet-content nopadding">
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
          <thead>
            <tr>
              <th width="29" scope="col">S.N.</th>
              <th width="236" scope="col">Dropdown Text</th>
              <th width="143" scope="col">Dropdown Key</th>
              <th width="151" scope="col">Created By</th>
              <th width="101" scope="col">Created Date</th>
              <th width="147" scope="col">Modified By</th>
              <th width="113" scope="col">Modified Date</th>
              <th width="75" scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php if(count($rows) == 0 ) : ?>
            <tr>
              <td colspan="8" align="center" height="24">No Records Found! </td>
            </tr>
            <?php else: ?>
            <?php $i = 1; foreach($rows as $row) : ?>
            <tr>
              <td><?php echo $i ?></td>
              <td><?php echo $row->dropdown_detail_text ?></td>
              <td><?php echo $row->dropdown_detail_key ?></td>
              <td><?php echo $this->admin_user->display_name($row->created_by) ?></td>
              <td><?php echo $row->created_date ?></td>
              <td><?php echo $this->admin_user->display_name($row->modified_by) ?></td>
              <td><?php echo $row->modified_date ?></td>
              <td width="75">
			    <?php if($edit) { ?>
                <a href="<?= site_url("$page_name/form_dropdowns/$dropdown_id/" . $row->id) ?>" class="edit_icon" title="Edit"></a>
                <?php } if($delete) { ?>
                <a href="<?= site_url("$page_name/delete_dropdowns/$dropdown_id/" . $row->id ) ?>" class="delete_icon" title="Delete" onClick = "return confirm('Are you sure to delete?')"></a>
                <?php } ?></td>
            </tr>
            <?php $i++; endforeach; endif;
			?>
            <tr class="footer">
              <td colspan="2"><?php if($add) echo anchor("$page_name/form_dropdowns/$dropdown_id", "[Add New]") ?> &nbsp; <?php if($add) echo anchor("$page_name", "[Cancel]") ?></td>
              <td colspan="6" align="right"><!--  PAGINATION START  --> 
               &nbsp;
                <!--  PAGINATION END  --></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!--  END #PORTLETS --> 
  </div>
  <div class="clear"> </div>
  <!-- END CONTENT--> 
</div>
<div class="clear"> </div>
