<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>

<script type="text/javascript">
/* when the DOM is ready */
jQuery(document).ready(function() {
	/* grab important elements */
	var sortInput = jQuery('#sort_order');
	var submit = 1;
	var messageBox = jQuery('#message-box');
	var list = jQuery('#sortable-list');
	/* create requesting function to avoid duplicate code */
	var request = function() {
		jQuery.ajax({
			beforeSend: function() {
				messageBox.text('Updating the sort order in the database.');
			},
			data: 'sort_order=' + sortInput[0].value, //need [0]?
			type: 'post',
			url: '<?php echo site_url('modules/sort_modules') ?>',
			success: function() {
				messageBox.text('Database has been updated.');
			},
			error: function(){
				messageBox.text('Error while sorting data.');
			}
			
		});
	};
	/* worker function */
	var fnSubmit = function(save) {
		var sortOrder = [];
		list.children('li').each(function(){
			sortOrder.push(jQuery(this).data('id'));
		});
		sortInput.val(sortOrder.join(','));
		//console.log(sortInput.val());
		if(save) {
			request();
		}
	};
	/* store values */
	list.children('li').each(function() {
		var li = jQuery(this);
		li.data('id',li.attr('title')).attr('title','');
	});
	/* sortables */
	list.sortable({
		opacity: 0.7,
		update: function() {
			fnSubmit(1);
		}
	});
	list.disableSelection();
	/* ajax form submission */
	jQuery('#dd-form').bind('submit',function(e) {
		if(e) e.preventDefault();
		fnSubmit(true);
	});
});
</script>

<style type="text/css">
	#facebox li { list-style: none; border: thin solid #CCC; height:15px; padding:5px; background-color:#CCCCCC; 
	margin:5px; }
	#message-box {width:300px; font-size:13px; color:#FB6F11; margin:5px; height:15px; }
	.ui-sortable-placeholder { height:15px !important; }
</style>

<table cellpadding="5" cellspacing="0" border="0" align="center" width="100%" id="" class="dataTable">
  <tr>
    <td align="right" style="font-size:11px; color:#000; padding:5px; text-align:justify;">
    <div id="message-box">
    <?php if( ! count($modules))  { echo "No child in this module."; } ?>
    </div>
    <?php if(count($modules)) { ?>
      <ul id="sortable-list" style="margin:0px; padding:0px;">
      <?php foreach($modules as $module) { ?>
        <li class="sortableParent" title="<?php echo $module->id ?>"><?php echo $module->module_title ?></li>
        <?php } ?>
      </ul>
      <input type="hidden" name="sort_order" id="sort_order" value="" />
      <?php } ?>
      </td>
  </tr>
</table>