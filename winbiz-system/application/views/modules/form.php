<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?> </a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i><?php echo $title ?> Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Module Title : *</label>
                      <div class="controls"> <?php echo form_input('module_title', $module_title, 'class="span12"') ?></div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Module Name  : *</label>
                      <div class="controls"><?php echo form_input('module_name', $module_name, 'class="span12"') ?></div>
                    </div>
                    </div>
                    <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Parent Module : *</label>
                      <div class="controls"><?php echo form_dropdown('parent_module', $modules, $parent_module, 'class="span12"') ?></div>
                    </div>
                  </div>
                  <!-- <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Global Module : *</label>
                      <div class="controls"><?php echo form_dropdown('global_module', $global_modules, $global_module, 'class="span12"') ?></div>
                    </div>
                  </div> -->
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
	});
</script> 
