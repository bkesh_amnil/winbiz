<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body">
              <?php  if ($this->session->flashdata('class')): ?>
              <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                <button data-dismiss="alert" class="close">×</button>
                <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
              <?php  endif; ?>
                	<form action="<?php echo site_url('modules') ?>" method="post" autocomplete="off" style="float:left; margin-bottom:0;">
                    <div class="searchByTitle">
    	<input type="text" name="search" value="" style="display:inline !important;" />
        </div>
        <div class="searchByTitle">
       <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
       </div>
    </form>
 
              <div class="actionsBar">
                  <div class="actionIcons sortIcon" style="float:left; margin-right:5px;">
                    <a class="btn btn-danger btn-success addIcon" href="<?php echo site_url('modules/sort_modules') ?>" rel="sortFacebox" class="sortFaceboxt" title="Sort" onclick="return false;" id="modules"><i class="icon-sort icon-white"></i>Sort</a>
                  </div>
                  <?php 
                  if($add) { 
                    echo anchor("$page_name/form", '<button class="btn btn-danger btn-success addIcon"><i class="icon-plus icon-white"></i> Create</button>');
                    ?>
                  <?php 
                  }
                  if($edit) { 
                  ?>
                    <a href="<?= site_url("$page_name/form/") ?>" class="edit_icon editIcon" title="Edit">
                      <button class="btn btn-danger btn-info"><i class="icon-pencil icon-white"></i> Edit</button>
                    </a>
                    <?php 
                  } if($delete) { 
                    ?>
                    <a href="<?= site_url("$page_name/delete/") ?>" class="delete_icon deleteIcon" title="Delete">
                      <button class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</button>
                    </a>
                    <?php 
                  } 
                  ?>
              </div>
            </div>
            <div class="widget-body">

 
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th width="33" scope="col">S.N.</th>
              <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
              <th width="226" scope="col">Module Title</th>
              <th width="477" scope="col">Module Name</th>
              <th width="477" scope="col">Parent Module</th>
              <!-- <th width="171" scope="col">Global Module</th> -->
            </tr>
          </thead>
          <tbody>
            <?php if(count($rows) == 0 ) : ?>
            <tr>
              <td colspan="5" align="center" height="24">No Records Found! </td>
            </tr>
            <?php else: ?>
            <?php $i = $start + 1; foreach($rows as $row) : ?>
            <tr>
              <td><?php echo $i ?></td>
               <td>
                  <input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" />
                </td>
              <td class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->module_title ?></td>
              <td><?php echo $row->ChildName ?></td>
              <td><?php echo $row->ParentName ?></td>
            </tr>
            <?php $i++; endforeach; endif;
			?>
          </tbody>
        </table>
        
       </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var sort_url = "<?php echo site_url('modules/sort_modules') ?>/";
  var site_url = "<?php echo site_url(); ?>";
</script> 