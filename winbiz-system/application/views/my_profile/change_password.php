<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#">Content</a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" enctype="multipart/form-data" class="horizontal-form" id="form1">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Content</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Old Password<span class="required">*</span></label>
                      <div class="controls">
                        <input name="old_password" type="password" class="span12" id="old_password" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">New Password<span class="required">*</span></label>
                      <div class="controls">
                        <input name="new_password" type="password" class="span12" id="new_password" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Confirm Password<span class="required">*</span></label>
                      <div class="controls">
                        <input name="confirm_password" type="password" class="span12" id="confirm_password" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
	});
</script> 
