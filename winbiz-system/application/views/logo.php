<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          <?= $con_title ?>
        </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Logo Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span4">
                    <div class="control-group">
                      <div class="controls">
                        <?php if(!empty($first_logo)){?>
                         <img src="<?php echo base_url('uploaded_files/logos/'.$first_logo)?>" style="height:100px"/>
                    <?php } ?>
                         <?php echo form_upload('first_logo',$first_logo, 'class="span12" ') ?> 
                         <label>Title:</label><input type="text" name="first_title" value="<?php echo $first_title?>">
                         <label>Link:</label><input type="text" name="first_link" value="<?php echo $first_link?>">
                      </div>
                    </div>
                  </div>
                  <div class="span4">
                    <div class="control-group">
                      <div class="controls">
                      <?php if(!empty($second_logo)){?>
                         <img src="<?php echo base_url('uploaded_files/logos/'.$second_logo)?>" style="height:100px"/>
                    <?php } ?>
                         <?php echo form_upload('second_logo',$second_logo, 'class="span12" ') ?> 
                         <label>Title:</label><input type="text" name="second_title" value="<?php echo $second_title?>">
                         <label>Link:</label><input type="text" name="second_link" value="<?php echo $second_link?>">
                      </div>
                    </div>
                  </div>
                  <div class="span4">
                    <div class="control-group">
                      <div class="controls">
                      <?php if(!empty($third_logo)){?>
                         <img src="<?php echo base_url('uploaded_files/logos/'.$third_logo)?>" style="height:100px"/>
                    <?php } ?>
                         <?php echo form_upload('third_logo',$third_logo, 'class="span12" ') ?> 
                         <label>Title:</label><input type="text" name="third_title" value="<?php echo $third_title?>">
                         <label>Link:</label><input type="text" name="third_link" value="<?php echo $third_link?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    </div>
    </div>
  

<!-- END CONTENT--> 

<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
		
		$('#remove_first_image').click(function(e) {
            $(this).parents('div.controls').html('<?php echo form_upload('first_logo', $first_logo, 'class="span12"')?>');
        });
        
		$('#remove_second_image').click(function(e) {
            $(this).parents('div.controls').html('<?php echo form_upload('second_logo', $second_logo, 'class="span12"')?>');
        });
        
		$('#remove_third_image').click(function(e) {
            $(this).parents('div.controls').html('<?php echo form_upload('third_logo', $third_logo, 'class="span12"')?>');
        });
       
        });
        </script>