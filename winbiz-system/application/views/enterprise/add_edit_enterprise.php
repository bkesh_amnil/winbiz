<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet"/>
<div id="body">
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><?= $con_title ?></h3>
                <ul class="breadcrumb">
                    <li><i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span
                            class="divider">/</span></li>
                    <li><a href="#"><?= $con_title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php $this->load->view("show_errors"); ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1"
                  enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span9">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Enterprise Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Category : *</label>

                                            <div class="controls">
                                                <?php
                                                $disabled = '';
                                                if (isset($profile_enterprise_cat_id) && !empty($profile_enterprise_cat_id)) {
                                                    $disabled = ' disabled="disabled"';
                                                }
                                                ?>
                                                <select name="enterprise_category_id[]"<?php echo $disabled; ?>
                                                        class="span12" multiple>
                                                    <option>Select Enterprise Category</option>
                                                    <?php
                                                    if (isset($enterprise_categories) && !empty($enterprise_categories) && !empty($enterprise_categories)) {
                                                        foreach ($enterprise_categories as $ent_cat) {
                                                            $selected = '';
                                                            foreach ($enterprise_category_id as $ent_cat_id) {
                                                                if (($ent_cat->id == $ent_cat_id->enterprise_category_id) || (isset($profile_enterprise_cat_id) && $ent_cat->id == $profile_enterprise_cat_id)) {
                                                                    $selected = 'selected="selected"';
                                                                }
                                                            }
                                                            ?>
                                                            <option
                                                                value="<?php echo $ent_cat->id ?>"<?php echo $selected; ?>><?php echo $ent_cat->enterprise_category_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Name : *</label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_name', $enterprise_name, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Alias : *</label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_alias', $enterprise_alias, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Logo (180X105) :</label>

                                            <div class="controls">
                                                <?php
                                                if (!empty($enterprise_logo) && !is_dir('./uploaded_files/enterprise/' . $enterprise_logo) && file_exists('./uploaded_files/enterprise/' . $enterprise_logo)) {
                                                    list($width, $height) = getimagesize('./uploaded_files/enterprise/' . $enterprise_logo);
                                                    $ratioBanner = $width / $height;
                                                    $thmWidth = 150;
                                                    $thmHeight = intval($thmWidth / $ratioBanner);
                                                    echo '<div id="old_img">';
                                                    ?>
                                                    <input type="hidden" name="uploaded_enterprise_old"
                                                           value="<?php echo $enterprise_logo; ?>"/>
                                                    <img
                                                        src="<?php echo base_url('uploaded_files/enterprise/' . $enterprise_logo) ?>"
                                                        width="<?php echo $thmWidth; ?>"
                                                        height="<?php echo $thmHeight; ?>"/><br/>
                                                    <span class="remove_img">Remove Image</span>
                                                    <?php
                                                    echo '</div>';
                                                }
                                                echo form_upload('enterprise_logo', $enterprise_logo, 'class="span12"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Website :</label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_website', $enterprise_website, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Facebook Link :</label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_facebook_link', $enterprise_facebook_link, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Twitter Link :<span id="image_size"></label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_twitter_link', $enterprise_twitter_link, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Linkedin Link :<span
                                                    id="image_size"></label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_linkedin_link', $enterprise_linkedin_link, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Youtube Link :<span id="image_size"></label>

                                            <div
                                                class="controls"><?php echo form_input('enterprise_youtube_link', $enterprise_youtube_link, 'class="span12"') ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Short Description</label>

                                            <div class="controls">
                                                <textarea name="enterprise_short_description" rows="8"
                                                          id="form_short_description"
                                                          style="width:760px;"><?php echo $enterprise_short_description ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Description</label>

                                            <div class="controls">
                                                <?php
                                                include_once('assets/ckeditor/ckeditor.php');
                                                include_once('assets/ckfinder/ckfinder.php');
                                                $ckeditor = new CKEditor();
                                                $ckeditor->basePath = site_url() . 'assets/ckeditor/';
                                                CKFinder::SetupCKEditor($ckeditor, site_url() . 'assets/ckfinder/');
                                                $ckeditor->editor('enterprise_description', $enterprise_description);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Status :</label>

                                            <div class="controls">
                                                <label
                                                    class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?>
                                                    Publish </label>
                                                <label
                                                    class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?>
                                                    Unpublish </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                    <input type="hidden" name="site_id" value="1"/>
                                    <input type="hidden" name="save" value="" id="Save"/>
                                    <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn"
                                                                                                        href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $disable = '';
                    if (isset($id) && empty($id)) {
                        $disable = ' disabled="disabled"';
                    }
                    ?>
                    <div class="span3">
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i> Domain Details</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="DataLog">
                                    <ul class="todo-list">
                                        <li>
                                            <div class="alert" id="theme_id" style="display:none;">
                                                <span class="info_inner"></span>
                                            </div>
                                            <span class="title1">Domain Theme:</span>
                                            <span class="title2">
                                                <select name="domain_theme_id"<?php echo $disable ?>>
                                                    <?php
                                                    if (isset($domain_themes) && !empty($domain_themes)) {
                                                        foreach ($domain_themes as $domain_theme) {
                                                            $selected = '';
                                                            if ($domain_theme->id == $domain_theme_id) {
                                                                $selected = ' selected="selected"';
                                                            }
                                                            ?>
                                                            <option
                                                                value="<?php echo $domain_theme->id ?>"<?php echo $selected ?>><?php echo $domain_theme->domain_theme_name ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                </select>
                                            </span>
                                        </li>
                                        <li>
                                            <a class="btn btn-primary" id="btn_theme_apply"><span>Apply Theme</span></a>
                                        </li>
                                        <li>
                                            <div class="alert" id="domain_name_id" style="display:none;">
                                                <span class="info_inner"></span>
                                            </div>
                                            <span class="title1">Domain Name:</span>
                                            <span class="title2"><input type="text" class="span12" name="domain_name"
                                                                        value="<?php echo $domain_name ?>"<?php echo $disable ?> /></span>
                                        </li>
                                        <li>
                                            <a class="btn btn-primary" id="btn_domain_save"><span>Save Domain</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view("user_detail_common"); ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/chosen.jquery.js') ?>"></script>
<script type="text/javascript">
    $('.testcls').chosen();
    $(document).ready(function (e) {
        $("input[name=enterprise_name]").keyup(function (e) {
            var txtValue = $(this).val();
            var newValue = txtValue.toLowerCase().replace(/[!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi, '-');
            $("input[name=enterprise_alias]").val(newValue);
        });
        $("#btn_submit").click(function (e) {
            $('#Save').val("true");
            $('#form1').submit();
        });

        /* character limit starts */
        var options = {
            'maxCharacterSize': 450,
            'originalStyle': 'originalDisplayInfo',
            'warningStyle': 'warningDisplayInfo',
            'warningNumber': 445,
            'displayFormat': '#input Characters | #left Characters Left | #words Words'
        };
        $('#form_short_description').textareaCount(options);
        /* character limit ends */
        $("#btn_theme_apply").click(function (e) {
            var theme_id = $("select[name=domain_theme_id] option:selected").val();
            var enterprise_id = $('input[name=id]').val();
            $.ajax({
                url: $("#baseUrl").val() + $("#hidcontname").val() + '/apply_theme/',
                data: {enterprise_id: enterprise_id, theme_id: theme_id},
                dataType: 'JSON',
                type: 'POST',
                success: function (data) {
                    $("#theme_id").css("display", "block");
                    $("#theme_id span.info_inner").html(data.msg);
                    return false;
                },
                error: function () {
                    alert('error occured');
                    return false;
                }
            })
        });
        $("#btn_domain_save").click(function (e) {
            var domain_name = $("input[name=domain_name]").val();
            if (domain_name == '') {
                $("#domain_name_id").css("display", "block");
                $("#domain_name_id span.info_inner").html("Domain Name can't be empty");
                return false;
            }
            var regex = /^[a-z0-9]/;
            if (!regex.test(domain_name)) {
                $("#domain_name_id").css("display", "block");
                $("#domain_name_id span.info_inner").html('Domain Name in small letter');
                return false;
            }

            var enterprise_id = $('input[name=id]').val();
            $.ajax({
                url: $("#baseUrl").val() + $("#hidcontname").val() + '/save_domain/',
                data: {enterprise_id: enterprise_id, domain_name: domain_name},
                dataType: 'JSON',
                type: 'POST',
                success: function (data) {
                    $("#domain_name_id").css("display", "block");
                    $("#domain_name_id span.info_inner").html(data.msg);
                    return false;
                },
                error: function () {
                    alert('error occured');
                    return false;
                }
            })
        });
    });
</script> 