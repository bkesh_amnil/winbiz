<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><?= $con_title ?></h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?= $con_title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php $this->load->view("show_errors"); ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i>Enterprise Category Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Enterprise Category Name  : *</label>
                      <div class="controls"><?php echo form_input('enterprise_category_name', $enterprise_category_name, 'class="span12"') ?></div>
                    </div>
                    <!-- <div class="control-group">
                      <label class="control-label">Product Category Name  : *</label>
                      <div class="controls"><?php echo form_input('product_category_name', '', 'class="span12"') ?></div>
                    </div> -->
                  </div>
                  <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Enterprise Category Alias  : *</label>
                      <div class="controls"><?php echo form_input('enterprise_category_alias', $enterprise_category_alias, 'class="span12"') ?></div>
                    </div>
                    <!-- <div class="control-group">
                      <label class="control-label">Product Category Alias  : *</label>
                      <div class="controls"><?php echo form_input('product_category_alias', '', 'class="span12"') ?></div>
                    </div> -->
                  </div>
                  <!-- <span class="add_multiple"> [ Add Product ]</span>
                  <table id="product-multi-table" class="table table-bordered" width="100%">
                    <thead>
                      <tr>
                        <th>Product Category Name</th>
                        <th>Product Category Alias</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      if(isset($prod_rows) && !empty($prod_rows) && is_array($prod_rows)) {
                        foreach($prod_rows as $prod_row) {
                        ?>
                        <tr class="new-multiple">
                          <td>
                            <input type="hidden" name="product_name_multiple[]" value="<?php echo $prod_row->product_category_name ?>" class="hidden_product_name" />
                            <?php echo $prod_row->product_category_name; ?>
                          </td>
                          <td>
                            <input type="hidden" name="product_alias_multiple[]" value="<?php echo $prod_row->product_category_alias ?>" class="hidden_product_alias" />
                            <?php echo $prod_row->product_category_alias; ?>
                          </td>
                          <td>
                            <span class="edit-multiple">[ Edit ]</span>
                            <span class="remove-multiple"> [ Remove ]</span>
                          </td>
                        </tr>
                        <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table> -->
                  <!-- <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Enterprise Category Image  :</label>
                      <div class="controls">
                        <?php 
                        if(!empty($enterprise_category_image) && !is_dir('./uploaded_files/enterprise_category/'.$enterprise_category_image) && file_exists('./uploaded_files/enterprise_category/'.$enterprise_category_image)) {
                          echo '<div id="old_img">';
                          list($width, $height) = getimagesize('./uploaded_files/enterprise_category/'.$enterprise_category_image);
                          $ratioBanner = $width / $height;
                          $thmWidth = 150;
                          $thmHeight = intval($thmWidth / $ratioBanner);
                          ?>
                          <input type="hidden" name="uploaded_enterprise_category_old" value="<?php echo $enterprise_category_image; ?>" />
                          <img src="<?php echo base_url('uploaded_files/enterprise_category/'.$enterprise_category_image) ?>" width="<?php echo $thmWidth; ?>" height="<?php echo $thmHeight; ?>" /><br />
                          <span class="remove_img">Remove Image</span>
                          <?php 
                          echo '</div>';
                        }
                        echo form_upload('enterprise_category_image', $enterprise_category_image, 'class="span12"');
                        ?>
                      </div>
                    </div>
                  </div> -->
                </div>
                <!-- <div class="row-fluid">
                  <div class="span12">
                    <div class="control-group">
                      <label class="control-label">Enterprise Category Description</label>
                      <div class="controls">
                        <?php
                          include_once('assets/ckeditor/ckeditor.php');
                          include_once('assets/ckfinder/ckfinder.php');
                          $ckeditor = new CKEditor();
                          $ckeditor->basePath = site_url().'assets/ckeditor/';
                          CKFinder::SetupCKEditor($ckeditor, site_url().'assets/ckfinder/');
                          $ckeditor->editor('enterprise_category_description', $enterprise_category_description);
                        ?>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="row-fluid">
                  <div class="span6 ">
                    <div class="control-group">
                      <label class="control-label">Status :</label>
                      <div class="controls">
                        <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                        <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <input type="hidden" name="id" value="<?php echo $id; ?>" />
                  <input type="hidden" name="site_id" value="1" />
                  <input type="hidden" name="save" value="" id="Save" />                  
                  <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                </div>
                <input type="hidden" id="remove_image_url" value="<?php echo site_url("$page_name/remove_image"); ?>" >
              </div>
            </div>
          </div>
          <?php $this->load->view("user_detail_common"); ?>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    $("#btn_submit").click(function(e) {
        $("#form1").submit();
    });	
    /*$(".remove_img").click(function(e) {
      var url = $("#remove_image_url").val();
      var img_name = $("input[name=uploaded_enterprise_category_old]").val();
      
      $.ajax({
        url : url,
        type : 'POST',
        data : {img: img_name},
        success : function(data) {
          $("#old_img").remove();
        },
        error : function() {
          alert('error');
        }
      })
    });*/
  });
</script>