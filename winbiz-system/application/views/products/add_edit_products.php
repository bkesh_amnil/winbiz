<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet"/>
<div id="body">
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><?= $con_title ?></h3>
                <ul class="breadcrumb">
                    <li><i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span
                            class="divider">/</span></li>
                    <li><a href="#"><?= $con_title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php $this->load->view("show_errors"); ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1"
                  enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span9">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Product Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Enterprise Category : *</label>

                                            <div class="controls">
                                                <?php
                                                $disabled = '';
                                                if (isset($profile_enterprise_cat_id) && !empty($profile_enterprise_cat_id)) {
                                                    $disabled = ' disabled="disabled"';
                                                }
                                                ?>
                                                <select id="enterprise_category_id"
                                                        name="enterprise_category_id"<?php echo $disabled; ?>
                                                        class="span12">
                                                    <option>Select Enterprise Category</option>
                                                    <?php
                                                    if (isset($enterprise_categories) && !empty($enterprise_categories) && !empty($enterprise_categories)) {
                                                        foreach ($enterprise_categories as $ent_cat) {
                                                            $selected = '';
                                                            if (($ent_cat->id == $enterprise_category_id) || (isset($profile_enterprise_cat_id) && $ent_cat->id == $profile_enterprise_cat_id)) {
                                                                $selected = ' selected="selected"';
                                                            }
                                                            ?>
                                                            <option
                                                                value="<?php echo $ent_cat->id ?>"<?php echo $selected; ?>><?php echo $ent_cat->enterprise_category_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Enterprise : *</label>

                                            <div class="controls">
                                                <?php if ($siteId == 1) { ?>
                                                    <?php
                                                    $disabled = '';
                                                    if (isset($profile_enterprise_id) && !empty($profile_enterprise_id)) {
                                                        $disabled = ' disabled="disabled"';
                                                    }
                                                    ?>
                                                    <input type="hidden" name="enterprise_id"
                                                           value="<?php echo $profile_enterprise_id; ?>"/>
                                                    <select id="enterprise_id"
                                                            name="enterprise_id"<?php echo $disabled; ?> class="span12">
                                                                <?php
                                                                if (isset($enterprise) && !empty($enterprise) && is_array($enterprise)) {
                                                                    foreach ($enterprise as $ents) {
                                                                        $selected = "";
                                                                        if (($ents->id == $enterprise_id) || (isset($profile_enterprise_id) && $ents->id == $profile_enterprise_id)) {
                                                                            $selected = " selected='selected'";
                                                                        }
                                                                        ?>
                                                                <option
                                                                    value="<?php echo $ents->id ?>"<?php echo $selected; ?>><?php echo $ents->enterprise_name; ?></option>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                            <option>Select Enterprise</option>

                                                        <?php } ?>
                                                    </select>
                                                <?php } else { ?>
                                                    <input type="text" class="span12"
                                                           value="<?php echo $enterprise->site_title ?>" disabled/>
                                                    <input type="hidden" name="enterprise_id"
                                                           value="<?php echo $enterprise->enterprise_id; ?>"/>
                                                       <?php } ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Product Category : *</label>

                                            <div class="controls">
                                                <?php echo form_input('product_category_name', $product_category_name, 'class="span12" id="product_category_name"') ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Product Name : *</label>

                                            <div
                                                class="controls"><?php echo form_input('product_name', $product_name, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Product Alias : *</label>

                                            <div
                                                class="controls"><?php echo form_input('product_alias', $product_alias, 'class="span12"') ?></div>
                                        </div>
                                    </div>
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Show Product Price : *</label>

                                            <div class="controls">
                                                <label
                                                    class="radio"><?php echo form_radio('show_product_price', "1", (($show_product_price == "1") ? "checked" : "")) ?>
                                                    Yes </label>
                                                <label
                                                    class="radio"> <?php echo form_radio('show_product_price', "0", (($show_product_price == "0" || empty($show_product_price)) ? "checked" : "")) ?>
                                                    No </label>
                                            </div>
                                        </div>
                                        <?php
                                        $style = "style='display:none'";
                                        if (isset($product_price) && !empty($product_price)) {
                                            if (isset($show_product_price) && $show_product_price != '0') {
                                                $style = "style='display:block'";
                                            }
                                        }
                                        ?>
                                        <div class="control-group prod_price"<?php echo $style; ?>>
                                            <label class="control-label">Product Price : *</label>

                                            <div
                                                class="controls"><?php echo form_input('product_price', $product_price, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Product Cover Image :</label>

                                            <div class="controls">
                                                <?php
                                                if (!empty($product_cover_image) && !is_dir('./uploaded_files/products/' . $product_cover_image) && file_exists('./uploaded_files/products/' . $product_cover_image)) {
                                                    list($width, $height) = getimagesize('./uploaded_files/products/' . $product_cover_image);
                                                    $ratioBanner = $width / $height;
                                                    $thmWidth = 150;
                                                    $thmHeight = intval($thmWidth / $ratioBanner);
                                                    echo '<div id="old_img">';
                                                    ?>
                                                    <input type="hidden" name="uploaded_product_old"
                                                           value="<?php echo $product_cover_image; ?>"/>
                                                    <img
                                                        src="<?php echo base_url('uploaded_files/products/' . $product_cover_image) ?>"
                                                        width="<?php echo $thmWidth; ?>"
                                                        height="<?php echo $thmHeight; ?>"/><br/>
                                                    <span class="remove_img">Remove Image</span>
                                                    <?php
                                                    echo '</div>';
                                                }
                                                echo form_upload('product_cover_image', $product_cover_image, 'class="span12"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label">Product Short Description</label>

                                            <div class="controls">
                                                <?php
                                                include_once('assets/ckeditor/ckeditor.php');
                                                include_once('assets/ckfinder/ckfinder.php');
                                                $ckeditor = new CKEditor();
                                                $ckeditor->basePath = site_url() . 'assets/ckeditor/';
                                                CKFinder::SetupCKEditor($ckeditor, site_url() . 'assets/ckfinder/');
                                                $ckeditor->editor('product_short_description', $product_short_description);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label">Product Description</label>

                                            <div class="controls">
                                                <?php
                                                include_once('assets/ckeditor/ckeditor.php');
                                                include_once('assets/ckfinder/ckfinder.php');
                                                $ckeditor = new CKEditor();
                                                $ckeditor->basePath = site_url() . 'assets/ckeditor/';
                                                CKFinder::SetupCKEditor($ckeditor, site_url() . 'assets/ckfinder/');
                                                $ckeditor->editor('product_description', $product_description);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Status :</label>

                                            <div class="controls">
                                                <label
                                                    class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?>
                                                    Publish </label>
                                                <label
                                                    class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?>
                                                    Unpublish </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                    <input type="hidden" name="site_id" value="<?php echo $siteId ?>"/>
                                    <input type="hidden" name="save" value="" id="Save"/>
                                    <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn"
                                                                                                        href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                                </div>
                                <input type="hidden" id="remove_image_url"
                                       value="<?php echo site_url("$page_name/remove_image"); ?>">
                                <input type="hidden" id="get_dropdown"
                                       value="<?php echo site_url("$page_name/get_dropdown_values"); ?>">
                                <!-- <input type="hidden" id="hidden_selected_enterprise_id" value="<?php echo $selected_enterprise_id ?>" />
                <input type="hidden" id="hidden_selected_product_category_id" value="<?php echo $selected_product_category_id ?>" /> -->
                            </div>
                        </div>
                    </div>
                    <?php
                    $this->load->view("user_detail_common");
                    ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/chosen.jquery.js') ?>"></script>
<script type="text/javascript">
    $('.testcls').chosen();
    $(document).ready(function (e) {
        $("input[name=product_name]").keyup(function (e) {
            var txtValue = $(this).val();
            var newValue = txtValue.toLowerCase().replace(/[!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi, '-');
            $("input[name=product_alias]").val(newValue);
        });
        $("#btn_submit").click(function (e) {
            $('#Save').val("true");
            $('#form1').submit();
        });
        $("#product_category_name").autocomplete(
                "<?php echo base_url(); ?>product_category/autocomplete",
                {
                    delay: 1,
                    minChars: 1,
                    matchSubset: 1,
                    matchContains: 1,
                    cacheLength: 10,
                    onItemSelect: selectItem,
                    onFindValue: findValue,
                    formatItem: formatItem,
                    autoFill: true
                }
        );
        $(".remove_img").click(function (e) {
            var url = $("#remove_image_url").val();
            var img_name = $("input[name=uploaded_enterprise_old]").val();

            $.ajax({
                url: url,
                type: 'POST',
                data: {img: img_name},
                success: function (data) {
                    $("#old_img").remove();
                },
                error: function () {
                    alert('error');
                }
            })
        });
    });
    /* autocomplete functions */
    function findValue(li) {
        if (li == null)
            return alert("No match!");
        // if coming from an AJAX call, let's use the CityId as the value
        if (!!li.extra)
            var sValue = li.extra[0];
        // otherwise, let's just display the value in the text box
        else
            var sValue = li.selectValue;
        //alert("The value you selected was: " + sValue);
    }

    function selectItem(li) {
        findValue(li);
    }

    function formatItem(row) {
        return row[0];
    }
    /* autocomplete functions */
</script> 