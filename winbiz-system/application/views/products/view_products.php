<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Product Grid</h4>
            </div>
            <div class="widget-body">
              <?php  if ($this->session->flashdata('class')): ?>
              <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                <button data-dismiss="alert" class="close">×</button>
                <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
              <?php  endif; ?>
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php $this->load->view('action_icon'); ?>
              </div>
            </div>
            <div class="widget-body">
              <form action="" method="get" id="gridForm" autocomplete="off">
                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th width="31" scope="col"></th>
                      <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                      <th width="230" scope="col">Product Name</th>
                      <th width="230" scope="col">Product For</th>
                      <th width="160" scope="col">Enterprise</th>
                      <th width="230" scope="col">Product Category</th>
                      <th width="100" scope="col">Product Image</th>
                      <th width="100" scope="col">Product Viewed</th>
                      <th width="139" scope="col">Created By</th>
                      <th width="159" scope="col">Updated By</th>
                      <th width="88" scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($rows) == 0 ){ ?>
                    <tr>
                      <td colspan="9" align="center" height="24">No Records Found! </td>
                    </tr>
                    <?php 
                    }else{ 
              				$i = $start + 1; 
            				  foreach($rows as $row){
                        $serLink = site_url('products?site='.$row->site_id);
                        $this->db->select('product_image');
                        $this->db->where('products_id', $row->id);
                        $this->db->from('tbl_product_images');
                        $count =  $this->db->count_all_results();
                  			?>
                        <tr>
                          <td style="vertical-align:top !important;"><?php echo $i ?></td>
                          <td style="vertical-align:top !important;"><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                          <td style="vertical-align:top !important;" class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->product_name ?></td>
                          <td style="vertical-align:top !important;"><a href="<?php echo $serLink ?>"><?php echo $row->site_title ?></a></td>
                          <td style="vertical-align:top !important;"><?php echo $row->enterprise_name; ?></td>
                          <td style="vertical-align:top !important;"><?php echo $row->product_category_name; ?></td>
                          <td style="vertical-align:top !important;">
                            <?php echo $count .' product images available'; ?>
                            <!-- <a href="<?php //echo base_url('products/product_images/' .$row->id) ?>">Product Images</a> -->
                            <a href="<?php echo base_url('products/images/' .$row->id) ?>">Product Images</a>
                          </td>
                          <td style="vertical-align:top !important;"><?php echo $row->view_count; ?></td>
                          <td style="vertical-align:top !important;">
                            <?php echo $this->admin_user_model->display_name($row->created_by).'<br/>'.$row->created_date; ?>
                          </td>
                          <td style="vertical-align:top !important;">
                            <?php echo $this->admin_user_model->display_name($row->updated_by).'<br/>'.$row->updated_date; ?>
                          </td>
                          <td style="vertical-align:top !important;">
                            <?php if($row->status == '1'){ ?>
                              <a href="<?php echo site_url('products/change_status/0/'.$row->id) . get_url(); ?>" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>
                            <?php }else{ ?>
                              <a href="<?php echo site_url('products/change_status/1/'.$row->id) . get_url(); ?>" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>
                            <?php } ?>
                          </td>
                        </tr>
                        <?php 
    					          $i++; 
                      }
			              }      
			              ?>
                  </tbody>
                </table>
              </form>
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
