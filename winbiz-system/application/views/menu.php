<?php
$user_detail = $this->admin_user_model->get_user_detail(current_admin_id());
$site_setting = $this->administrator_model->get_data('site', $user_detail->site_id);

$cred_users = $this->admin_user_model->get_credentials('users');
$cred_backend = $this->admin_user_model->get_credentials('backend');
$cred_settings = $this->admin_user_model->get_credentials('settings');
$cred_cms = $this->admin_user_model->get_credentials('cms');
$cred_product = $this->admin_user_model->get_credentials('product');
$menu_list = get_cms_menus();

if (!isset($page_name)) {
    $page_name = false;
}
$page_parent = get_class_parent($page_name);
?>
<!-- BEGIN SIDEBAR -->
<div id="sidebar" class="nav-collapse collapse">
    <div class="sidebar-toggler hidden-phone"></div>
    <!-- BEGIN SIDEBAR MENU -->
    <ul>
        <?php
//        dumparray($menu_list);
        if ($menu_list && is_array($menu_list)) {
            foreach ($menu_list as $ind => $val) {
                $has_childs = false;
                if (!empty($val["childs"]) && is_array($val["childs"])) {
                    $subclass = " has-sub";
                    $url = "javascript:;";
                    $has_childs = true;
                    if (isset($page_parent) && $page_parent == $val["controller"]) {
                        $subclass = "active" . $subclass;
                    }
                } else {
                    $subclass = "";
                    $url = site_url($val["controller"]);
                    if (isset($page_name) && $page_name == $val["controller"]) {
                        $subclass = "active" . $subclass;
                    }
                }
                ?>
                <li class="<?php echo $subclass; ?>">
                    <a href="<?php echo $url; ?>">
                        <i class="icon-<?php echo $val["controller"]; ?>"></i> <span class="title"><?php echo $val["name"]; //echo " --> ".$$val["controller"];  ?></span>
                        <?php
                        if (!empty($subclass)) {
                            ?>
                            <span class="arrow"></span>
                            <?php
                        }
                        ?>
                    </a>  
                    <?php
                    if ($has_childs) {
                        ?>
                        <ul class="sub">
                            <?php
                            foreach ($val["childs"] as $childInd => $childVal) {
                                if (isset($page_name) && $page_name == $childVal["controller"]) {
                                    $subliclass = "active";
                                } else {
                                    $subliclass = "";
                                }
                                ?>
                                <li class="<?php echo $subliclass; ?>"><a class="" href="<?php echo site_url($childVal["controller"]) ?>"><?php echo $childVal["name"]; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </li>
                <?php
            }
        }
        ?>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
