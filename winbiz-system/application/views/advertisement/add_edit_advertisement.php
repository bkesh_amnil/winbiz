<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" />
<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><?= $con_title ?></h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?= $con_title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php $this->load->view("show_errors"); ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1" enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span9"> 
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Advertisement Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Name : *</label>
                                            <div class="controls"><?php echo form_input('advertisement_name', $advertisement_name, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Provided by : *</label>
                                            <div class="controls">
                                                <?php echo form_dropdown('provider', $providers, $provider_id, 'class="span12" id =""') ?>    
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Image : *<span id="image_size"><?php //echo $advertisement_image_size;     ?></span></label>
                                            <div class="controls">
                                                <?php
                                                if (!empty($advertisement_image) && !is_dir('./uploaded_files/bigyapan/' . $advertisement_image) && file_exists('./uploaded_files/bigyapan/' . $advertisement_image)) {
                                                    list($width, $height) = getimagesize('./uploaded_files/bigyapan/' . $advertisement_image);
                                                    $ratioBanner = $width / $height;
                                                    $thmWidth = 150;
                                                    $thmHeight = intval($thmWidth / $ratioBanner);
                                                    ?>
                                                    <input type="hidden" name="uploaded_advertisement_old" value="<?php echo $advertisement_image; ?>" />
                                                    <img src="<?php echo base_url('uploaded_files/bigyapan/' . $advertisement_image) ?>" width="<?php echo $thmWidth; ?>" height="<?php echo $thmHeight; ?>" /><br />
                                                    <?php
                                                }
                                                echo form_upload('advertisement_image', $advertisement_image, 'class="span12"');
                                                ?>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="span6 ">
                                        <?php
                                        $contentDisplay = "none";
                                        $urlDisplay = "none";
                                        $productDisplay = "none";
                                        $opensDisplay = "none";
                                        $contentChecked = '';
                                        $urlChecked = '';
                                        $productChecked = '';
                                        $noneChecked = 'checked="checked"';
                                        if ($advertisement_link_opens == "new") {
                                            $newTabOpen = 'checked="checked"';
                                            $sameTabOpen = '';
                                        } elseif ($advertisement_link_opens == "same") {
                                            $sameTabOpen = 'checked="checked"';
                                            $newTabOpen = '';
                                        } else {
                                            $sameTabOpen = '';
                                            $newTabOpen = '';
                                        }
                                        $advertisement_link_type = strtolower($advertisement_link_type);
                                        if ($advertisement_link_type == "content") {
                                            $contentChecked = 'checked="checked"';
                                            $noneChecked = '';
                                            $contentDisplay = 'table-row';
                                            $opensDisplay = 'table-row';
                                        } else if ($advertisement_link_type == "url") {
                                            $urlChecked = 'checked="checked"';
                                            $noneChecked = '';
                                            $urlDisplay = 'table-row';
                                            $opensDisplay = 'table-row';
                                        } else if ($advertisement_link_type == "product") {
                                            $productChecked = 'checked="checked"';
                                            $noneChecked = '';
                                            $productDisplay = 'table-row';
                                            $opensDisplay = 'table-row';
                                        }
                                        ?>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Link Type : *</label>
                                            <div class="controls">
                                                <!--                                                <label class="radio">
                                                                                                    <input type="radio" class="linkType" name="advertisement_link_type" value="Internal Content" <?php echo $contentChecked; ?> />
                                                                                                    Content</label>-->
                                                <label class="radio">
                                                    <input type="radio" class="linkType" name="advertisement_link_type" value="External URL" <?php echo $urlChecked; ?> />
                                                    URL</label>
                                                <label class="radio">
                                                    <input type="radio" class="linkType" name="advertisement_link_type" value="Product" <?php echo $productChecked; ?> />
                                                    Product</label>
                                                <label class="radio">
                                                    <input type="radio" class="linkType" name="advertisement_link_type" value="None" <?php echo $noneChecked; ?> />
                                                    None</label>
                                            </div>
                                        </div>
                                        <div class="control-group linkIdContent" style="display:<?php echo $contentDisplay; ?>">
                                            <label class="control-label">Content : </label>
                                            <div class="controls">
                                                <?php
                                                $start = 0;
//                                                $this->db->where('site_id', $siteId);
                                                $this->db->where('status', '1');
                                                $contentList = ''; //$this->administrator_model->get_data("content", 0, TRUE, $start, 'title');
                                                if (!empty($contentList) && is_array($contentList)) {
                                                    ?>
                                                    <select name="content_id" id="linkIdContent" class="span12">
                                                        <option value="0">Select Content</option>
                                                        <?php foreach ($contentList as $ind => $cval) { ?>
                                                            <option value="<?php echo $cval->id; ?>" <?php if ($content_id == $cval->id) { ?> selected="selected" <?php } ?>><?php echo $cval->title; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="control-group linkUrl" style="display:<?php echo $urlDisplay; ?>">
                                            <label class="control-label">Link Url :</label>
                                            <div class="controls"><?php echo form_input('external_url', $external_url, 'class="span12" ') ?></div>
                                        </div>
                                        <div class="control-group linkIdProduct" style="display:<?php echo $productDisplay; ?>">
                                            <label class="control-label">Products : </label>
                                            <div class="controls">
                                                <?php
                                                $start = 0;
                                                //$this->db->where('enterprise_id', $ent_id);
                                                $this->db->where('status', '1');
                                                $productList = $this->administrator_model->get_data("products", 0, TRUE, $start, 'product_name');
                                                if (!empty($productList) && is_array($productList)) {
                                                    ?>
                                                    <select name="product_id" id="linkIdProduct" class="span12">
                                                        <option value="0">Select Product</option>
                                                        <?php foreach ($productList as $ind => $pval) { ?>
                                                            <option value="<?php echo $pval->id; ?>" <?php if ($product_id == $pval->id) { ?> selected="selected" <?php } ?>><?php echo $pval->product_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="control-group opens" style="display:<?php echo $opensDisplay; ?>">
                                            <label class="control-label">Advertisement Link Opens :</label>
                                            <div class="controls">
                                                <label class="radio">
                                                    <input type="radio" name="advertisement_link_opens" value="same"<?php echo $sameTabOpen; ?> />
                                                    Within same page</label>
                                                <label class="radio">
                                                    <input type="radio" name="advertisement_link_opens" value="new"<?php echo $newTabOpen; ?> />
                                                    New Tab/Window</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Description</label>
                                            <div class="controls">
                                                <?php
                                                include_once('assets/ckeditor/ckeditor.php');
                                                include_once('assets/ckfinder/ckfinder.php');
                                                $ckeditor = new CKEditor();
                                                $ckeditor->basePath = site_url() . 'assets/ckeditor/';
                                                CKFinder::SetupCKEditor($ckeditor, site_url() . 'assets/ckfinder/');
                                                $ckeditor->editor('advertisement_description', $advertisement_description);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Status :</label>
                                            <div class="controls">
                                                <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                                                <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($isEdit == FALSE) { ?>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Site to show Ad. : *</label>
                                                <div class="controls">

                                                    <?php echo form_dropdown('site_id', $sites, '', 'class="span12" id ="select_site_id"') ?>  
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Advertisement Position : *</label>
                                                <div class="controls">
                                                    <select name="advertisement_position_id" id="advertisement_position_id" class="span12">
                                                        <option>Select Advertisement Position</option>
                                                    </select>
                                                    
                                                <span class="chk-pos"></span>
                                                </div>
                                                <input type="hidden" name="advertisement_image_size" id="advertisement_image_size"/>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Publish Date : *</label>
                                                <div class="controls">
                                                    <input type="text" name="publish_date" value="<?php //echo $publish_date    ?>" class="span12" id="publish_date" placeholder="<?php echo date('Y-m-d') ?>" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Unpublish Date : *</label>
                                                <div class="controls">
                                                    <input type="text" name="unpublish_date" value="<?php //echo $unpublish_date    ?>" class="span12" id="unpublish_date" placeholder="<?php echo date('Y-m-d') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Est. Price (Rs.)(Total days: <span class="total-days">0</span>): *</label>
                                                <div class="controls">
                                                    <input type="text" readonly name="est_price" value="<?php echo $est_price ?>" class="span12" id="" placeholder="" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Paid Amount (Rs.): *</label>
                                                <div class="controls">
                                                    <input type="text" name="paid_price" value="<?php echo $paid_price ?>" class="span12" id="" placeholder="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>


                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <!--<input type="hidden" name="site_id" value="<?php //echo $siteId       ?>" />-->
                                    <input type="hidden" name="save" value="" id="Save" />
                                    <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this->load->view("user_detail_common");
                    ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/chosen.jquery.js') ?>"></script>
<script type="text/javascript">
    $('.testcls').chosen();
    $(document).ready(function (e) {
        $("#btn_submit").click(function (e) {
            $('#Save').val("true");
            $('#form1').submit();
        });
        $(".linkType").click(function () {
            var selvalue = $(this).val();
            if (selvalue === "None") {
                $('.opens, .linkIdMenu, .linkIdContent, .linkUrl, .linkIdProduct').hide();
            } else if (selvalue === "Internal Content") {
                $('.linkIdMenu, .linkUrl, .linkIdProduct').hide();
                $('.opens, .linkIdContent').show();
            } else if (selvalue === "External URL") {
                $('.linkIdMenu, .linkIdContent, .linkIdProduct').hide();
                $('.opens, .linkUrl').show();
            } else if (selvalue === "Product") {
                $('.linkIdMenu, .linkUrl, .linkIdContent').hide();
                $('.opens, .linkIdProduct').show();
            }
        });
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var start_project_date = $('#publish_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : 'disabled';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > end_project_date.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                end_project_date.setValue(newDate);
            }
            start_project_date.hide();
            $('#unpublish_date')[0].focus();
        }).data('datepicker');
        var end_project_date = $('#unpublish_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            onRender: function (date) {
                return date.valueOf() <= start_project_date.date.valueOf() ? 'disabled' : 'disabled';
            }
        }).on('changeDate', function (ev) {
            end_project_date.hide();
        }).data('datepicker');
    });
    $(document).on("change", "#advertisement_position_id", function () {
        var size = $("#advertisement_position_id option:selected").attr("rel");
        $("#advertisement_image_size").val(size);
        $("#image_size").text(' Image Size : ' + size);
    })
</script>
<?php $this->load->view('advertisement/common_scripts.php'); ?>