<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" />
<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><?= $con_title ?></h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?= $con_title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php $this->load->view("show_errors"); ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1" enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span9"> 
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Advertisement Session Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Name : *</label>
                                            <div class="controls"><?php echo $ad_data->advertisement_name ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Provided by : *</label>
                                            <div class="controls">
                                                <?php echo $ad_data->name ?> 
                                                <input type="hidden" name="provider_id" value="<?php echo $ad_data->provider_id ?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Image : *</label>
                                            <div class="controls">
                                                <?php
                                                $file = $ad_data->advertisement_image;
                                                if (file_exists('./uploaded_files/bigyapan/' . $file)) {
                                                    $thmWidth = 150;
                                                    $thmHeight = 100;
                                                    ?>
                                                    <img src="<?php echo base_url('uploaded_files/bigyapan/' . $file) ?>" width="<?php echo $thmWidth; ?>" height="<?php echo $thmHeight; ?>"/> 
                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Site to show Ad. : *</label>
                                            <div class="controls">

                                                <?php
                                                $readonly = ($isEdit == TRUE) ? 'disabled' : '';
                                                echo form_dropdown('site_id', $sites, $site_id, 'class="span12" id ="select_site_id" ' . $readonly)
                                                ?>  
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Advertisement Position : *</label>
                                            <div class="controls">
                                                <select name="advertisement_position_id" id="advertisement_position_id" class="span12" <?php echo $readonly ?>>
                                                    <option value="">Select Advertisement Position</option>
                                                </select>
                                                <?php if($isEdit){?>
                                                <input type="hidden" name="advertisement_position_id" value="<?php echo $advertisement_position_id ?>">
                                                <?php }?>
                                                <span class="chk-pos"></span>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Publish Date : *</label>
                                            <div class="controls">
                                                <input type="text" name="publish_date" value="<?php echo $publish_date ?>" class="span12" id="publish_date" placeholder="<?php echo date('Y-m-d') ?>" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Unpublish Date : *</label>
                                            <div class="controls">
                                                <input type="text" name="unpublish_date" value="<?php echo $unpublish_date ?>" class="span12" id="unpublish_date" placeholder="<?php echo date('Y-m-d') ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Est. Price (Rs.)(Total days: <span class="total-days">0</span>): *</label>
                                            <div class="controls">
                                                <input type="text" readonly name="est_price" value="<?php echo $est_price ?>" class="span12" id="" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Paid Amount (Rs.): *</label>
                                            <div class="controls">
                                                <input type="text" name="paid_price" value="<?php echo $paid_price ?>" class="span12" id="" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <!--<input type="hidden" name="site_id" value="<?php //echo $siteId                       ?>" />-->
                                    <input type="hidden" name="save" value="" id="Save" />
                                    <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this->load->view("user_detail_common");
                    ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/chosen.jquery.js') ?>"></script>
<script type="text/javascript">
    $('.testcls').chosen();
    $(document).ready(function (e) {
        $("#btn_submit").click(function (e) {
            $('#Save').val("true");
            $('#form1').submit();
        });
        $(".linkType").click(function () {
            var selvalue = $(this).val();
            if (selvalue === "None") {
                $('.opens, .linkIdMenu, .linkIdContent, .linkUrl, .linkIdProduct').hide();
            } else if (selvalue === "Internal Content") {
                $('.linkIdMenu, .linkUrl, .linkIdProduct').hide();
                $('.opens, .linkIdContent').show();
            } else if (selvalue === "External URL") {
                $('.linkIdMenu, .linkIdContent, .linkIdProduct').hide();
                $('.opens, .linkUrl').show();
            } else if (selvalue === "Product") {
                $('.linkIdMenu, .linkUrl, .linkIdContent').hide();
                $('.opens, .linkIdProduct').show();
            }
        });
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var start_project_date = $('#publish_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : 'disabled';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > end_project_date.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                end_project_date.setValue(newDate);
            }
            start_project_date.hide();
            $('#unpublish_date')[0].focus();
        }).data('datepicker');
        var end_project_date = $('#unpublish_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            onRender: function (date) {
                return date.valueOf() <= start_project_date.date.valueOf() ? 'disabled' : 'disabled';
            }
        }).on('changeDate', function (ev) {
            end_project_date.hide();
        }).data('datepicker');


    });
    $(document).on("change", "#advertisement_position_id", function () {
        var size = $("#advertisement_position_id option:selected").attr("rel");
        $("#advertisement_image_size").val(size);
        $("#image_size").text(' Image Size : ' + size);
    })
</script>

<?php $this->load->view('advertisement/common_scripts.php'); ?>