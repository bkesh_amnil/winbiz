<script type="text/javascript">
    $(document).ready(function (e) {
        $("#search_providers").focus(function () {
            $(this).select();
        });
        $("#search_providers").autocomplete(
                "<?php echo base_url(); ?>advertisement/autocomplete",
                {
                    delay: 1,
                    minChars: 1,
                    matchSubset: 1,
                    matchContains: 1,
                    cacheLength: 10,
                    onItemSelect: selectItem,
                    onFindValue: findValue,
                    formatItem: formatItem,
                    autoFill: true
                }
        );
        /* autocomplete functions */
        function findValue(li) {
            if (li == null)
                return alert("No match!");
            // if coming from an AJAX call, let's use the CityId as the value
            if (!!li.extra)
                var sValue = li.extra[0];
            // otherwise, let's just display the value in the text box
            else
                var sValue = li.selectValue;
            //alert("The value you selected was: " + sValue);
        }


        function selectItem(li) {
            findValue(li);
        }

        function formatItem(row) {
            return row[0];
        }



        $(function () {
//        $("#date_time").datepicker({dateFormat: 'yy/mm/dd', firstDay: 1, minDate: '0'});
//            $("#date_time").datepicker({dateFormat: 'yy/mm/dd', firstDay: 1});
            $(".show_date_picker").datepicker({dateFormat: 'yy/mm/dd', firstDay: 0});
//            $(".show_date_picker").datepicker({dateFormat: 'DD, d MM, yy', firstDay: 0});
            //$( ".holding-date" ).datepicker({ minDate: '0' });
        });


        //check on load
        var siteid = $('#select_site_id').val();
        if (siteid !== '') {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('advertisement/get_ad_positions') ?>",
                data: 'siteid=' + siteid + '&selected=<?php echo isset($position_id) ? $position_id : ''; ?>',
                dataType: "json",
                success: function (data) {
                    $('#advertisement_position_id').html(data);
                }
            });
        }

        //lists the position of ad as per the site change
        $('#select_site_id').on('change', function () {
            var siteid = $(this).val();
            if (siteid !== '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('advertisement/get_ad_positions') ?>",
                    data: 'siteid=' + siteid,
                    dataType: "json",
                    success: function (data) {
                        $('#advertisement_position_id').html(data);
                    }
                });
            }
        });
        //calculates the price as per the site and its posiition choosen
        $(body).on('blur', '[name="publish_date"],[name="unpublish_date"],[name="advertisement_position_id"],[name="site_id"]', function () {
            var siteid = $('[name="site_id"]').val();
            var publish_date = $('[name="publish_date"]').val();
            var unpublish_date = $('[name="unpublish_date"]').val();
            var advertisement_position_id = $('[name="advertisement_position_id"]').val();
            if (siteid !== '' && publish_date != '' && unpublish_date != '' && advertisement_position_id != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('advertisement_session/get_ad_price') ?>", data: 'site_id=' + siteid + '&publish_date=' + publish_date + '&unpublish_date=' + unpublish_date + '&advertisement_position_id=' + advertisement_position_id,
                    dataType: "json",
                    success: function (data) {
                        $('[name="est_price"]').val(data.price);
                        $('.total-days').html(data.days + ' days');
                    }
                });
            }
        });


        //checks if the space is occupied or not by ad
        $(body).on('change', '[name="advertisement_position_id"]', function () {
            var siteid = $('[name="site_id"]').val();
            var advertisement_position_id = $('[name="advertisement_position_id"]').val();
            $('span.chk-pos').html('');
            if (advertisement_position_id != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('advertisement_session/check_ad_space') ?>",
                    data: 'site_id=' + siteid + '&advertisement_position_id=' + advertisement_position_id,
                    dataType: "json",
                    success: function (data) {
                        if (data == 0) {
                            $('span.chk-pos').html('This position is Occupied. Please select another position');
                            $('[name="advertisement_position_id"]').val('');
                        }
                    }
                });
            }

        });


        //#updating the status of order
        $('[name="order_status"]').on('change', function () {
            var _this = $(this);
            var id = _this.prev('[name="checkout_id"]').val();
            var status = _this.val();
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('orders/changeStatus') ?>",
                dataType: 'JSON',
                data: 'id=' + id + '&status=' + status,
                beforeSend: function () {
                    _this.css('background', 'grey');
                },
                success: function (data) {
                    _this.css('background', '');
                }
            });

        });
    });
</script>
