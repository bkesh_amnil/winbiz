<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"> <?php echo $title ?> </h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?php echo $title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="page" class="dashboard">
            <div class="row-fluid">
                <div class="span12"> 
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Advertisement Grid</h4>
                        </div>
                        <div class="widget-body">
                            <?php if ($this->session->flashdata('class')): ?>
                                <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                                    <button data-dismiss="alert" class="close">×</button>
                                    <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
                            <?php endif; ?>
                            <?php $this->load->view('cms/ad_session_details') ?>
                            <div class="actionsBar">
                                <?php $this->load->view('action_icon_session'); ?>
                            </div>
                        </div>
                        <div class="widget-body">
                            <form action="" method="get" id="gridForm" autocomplete="off">
                                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th width="31" scope="col"></th>
                                            <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                                            <th width="230" scope="col">Site Name</th>
                                            <th width="150">Position </th>
                                            <th width="230" scope="col">Start Date</th>
                                            <th width="160" scope="col">End Date</th>
                                            <th width="160" scope="col">Est. Price</th>
                                            <th width="160" scope="col">Paid Price</th>
<!--                                            <th width="139" scope="col">Created By</th>
                                            <th width="159" scope="col">Updated By</th>-->
                                            <th width="88" scope="col">Status</th>
                                            <th width="88" scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($rows) == 0) { ?>
                                            <tr>
                                                <td colspan="9" align="center" height="24">No Records Found! </td>
                                            </tr>
                                            <?php
                                        } else {
                                            $i = $start + 1;
                                            foreach ($rows as $row) {
                                                $serLink = 'javascript:void()'; //site_url('advertisement?site='.$row->site_id);
                                                $ad_post_count = get_ad_history($row->id);
                                                $ad_total = $ad_post_count['total'];
                                                $ad_running = $ad_post_count['running'];
                                                $ad_link = site_url('advertisement/ad_history/' . $row->id);
                                                $now = get_now();
                                                $status_check = ($row->start_date <= $now && $row->end_date >= $now) ? 'Running' : 'Expired';
                                                ?>
                                                <tr>
                                                    <td style="vertical-align:top !important;"><?php echo $i ?></td>
                                                    <td style="vertical-align:top !important;"><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                                                    <td style="vertical-align:top !important;" class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->site_title ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->advertisement_position_name ?></td>

                                                    <td style="vertical-align:top !important;"><?php echo user_format($row->start_date); ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo user_format($row->end_date); ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->est_price; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->paid_price; ?></td>
        <!--                                                    <td style="vertical-align:top !important;">
                                                    <?php //echo $this->admin_user_model->display_name($row->created_by) . '<br/>' . $row->created_date; ?>
                                                    </td>
                                                    <td style="vertical-align:top !important;">
                                                    <?php //echo $this->admin_user_model->display_name($row->updated_by) . '<br/>' . $row->updated_date; ?>
                                                    </td>-->
                                                    <td style="vertical-align:top !important;"><?php echo $status_check;?></td>
                                                    <td style="vertical-align:top !important;"><a href="<?php echo site_url('advertisement_session/form/' . $ad_data->id . "/$row->id") ?>">Edit</a></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET--> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
