<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"> <?php echo $title ?> </h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?php echo $title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="page" class="dashboard">
            <div class="row-fluid">
                <div class="span12"> 
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Advertisement Grid</h4>
                        </div>
                        <div class="widget-body">
                            <?php if ($this->session->flashdata('class')): ?>
                                <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                                    <button data-dismiss="alert" class="close">×</button>
                                    <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
                            <?php endif; ?>
                            <?php $this->load->view('cms/search') ?>
                            <div class="actionsBar">
                                <?php //$this->load->view('action_icon'); ?>
                            </div>
                        </div>
                        <div class="widget-body">
                            <form action="<?php echo site_url('advertisement_price/udpatePrice') . get_url(); ?>" method="post" id="gridForm" autocomplete="off">
                                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class=" table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th width="31" scope="col"></th>
                                            <th width="230" scope="col">Advertisement Position Name</th>
                                            <th width="150">position</th>
                                            <th width="160" scope="col">advertisement_size</th>
                                            <th width="230" scope="col">Ad. Session Overview</th>
                                            <th width="230" scope="col">Price (per day)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $rows = $advertisement_positions;
                                        if (empty($rows)) {
                                            ?>
                                            <tr>
                                                <td colspan="9" align="center" height="24">Please select the site to view the price! </td>
                                            </tr>
                                            <?php
                                        } else {
                                            $i = 1;
                                            foreach ($rows as $row) {
                                                $ad_post_count = get_ad_history($row->id, 'position_id', $siteId);
                                                $ad_total = $ad_post_count['total'];
                                                $ad_running = $ad_post_count['running'];
                                                ?>
                                                <tr>
                                                    <td style="vertical-align:top !important;"><?php echo $i ?></td>
                                                    <td style="vertical-align:top !important;" class="rightOperation"><?php echo $row->advertisement_position_name; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->position; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->advertisement_size; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo " Total Posted:$ad_total / Running: $ad_running "; ?></td>
                                                    <td style="vertical-align:top !important;">
                                                        <input type="text" name="price[<?php echo $row->id ?>]" value="<?php echo @$price[$row->id]; ?>"></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>


                                <div class="form-actions">
                                    <?php
                                    if (!empty($rows)) {
                                        ?>
                                        <input type="hidden" value="<?php echo $siteId ?>" name="site_id">
                                        <button type="submit" class="btn btn-primary" id="btn_submit"><span>Save</span></a> </button>
                                    <?php }
                                    ?>
                                </div>
                            </form>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET--> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
