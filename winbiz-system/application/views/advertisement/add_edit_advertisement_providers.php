<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" />
<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><?= $con_title ?></h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?= $con_title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php $this->load->view("show_errors"); ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1" enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span9"> 
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Advertisement Providers Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label"> Name : *</label>
                                            <div class="controls"><?php echo form_input('name', $name, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">company : *</label>
                                            <div class="controls">
                                                <?php echo form_input('company', $company, 'class="span12" id =""') ?>    
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">email : *</label>
                                            <div class="controls">
                                                <?php echo form_input('email', $email, 'class="span12" id =""') ?>    
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">contact : *</label>
                                            <div class="controls">
                                                <?php echo form_input('contact', $contact, 'class="span12" id =""') ?>    
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">address : *</label>
                                            <div class="controls">
                                                <?php echo form_input('address', $address, 'class="span12" id =""') ?>    
                                            </div>
                                        </div>


                                    </div>

                                </div>

                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label">Status :</label>
                                            <div class="controls">
                                                <label class="radio"><?php echo form_radio('status', "1", (($status == "1" || empty($status)) ? "checked" : "")) ?> Publish </label>
                                                <label class="radio"> <?php echo form_radio('status', "0", (($status == "0") ? "checked" : "")) ?> Unpublish </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <!--<input type="hidden" name="site_id" value="<?php echo $siteId ?>" />-->
                                    <input type="hidden" name="save" value="" id="Save" />
                                    <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $this->load->view("user_detail_common");
                    ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (e) {
        $("#btn_submit").click(function (e) {
            $('#Save').val("true");
            $('#form1').submit();
        });
    });
</script>