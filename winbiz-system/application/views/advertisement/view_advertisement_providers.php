<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"> <?php echo $title ?> </h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?php echo $title ?></a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="page" class="dashboard">
            <div class="row-fluid">
                <div class="span12"> 
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Advertisement Grid</h4>
                        </div>
                        <div class="widget-body">
                            <?php if ($this->session->flashdata('class')): ?>
                                <div class="alert" id="<?php echo $this->session->flashdata('class') ?>">
                                    <button data-dismiss="alert" class="close">×</button>
                                    <span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span> </div>
                            <?php endif; ?>
                            <?php $this->load->view('cms/search') ?>
                            <div class="actionsBar">
                                <?php $this->load->view('action_icon'); ?>
                            </div>
                        </div>
                        <div class="widget-body">
                            <form action="" method="get" id="gridForm" autocomplete="off">
                                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th width="31" scope="col"></th>
                                            <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                                            <th width="230" scope="col">Name</th>
                                            <th width="150">Company</th>
                                            <th width="160" scope="col">Email</th>
                                            <th width="230" scope="col">Contact</th>
                                            <th width="230" scope="col">Address</th>
                                            <th width="230" scope="col">Ad. Session Overview</th>
                                            <th width="139" scope="col">Created By</th>
                                            <th width="159" scope="col">Updated By</th>
                                            <th width="88" scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($rows) == 0) { ?>
                                            <tr>
                                                <td colspan="9" align="center" height="24">No Records Found! </td>
                                            </tr>
                                            <?php
                                        } else {
                                            $i = $start + 1;
                                            foreach ($rows as $row) {
                                                $ad_post_count = get_ad_history($row->id, 'provider_id');
                                                $ad_payment = get_ad_payment_report($row->id);
                                                $est = $ad_payment['est'];
                                                $paid = $ad_payment['paid'];
                                                $ad_total = $ad_post_count['total'];
                                                $ad_running = $ad_post_count['running'];
//                                                $site_id = isset($_GET['site']) ? $_GET['site'] : '0';
                                                $ad_link = site_url("advertisement?site=0&provider=$row->name");
                                                ?>
                                                <tr>
                                                    <td style="vertical-align:top !important;"><?php echo $i ?></td>
                                                    <td style="vertical-align:top !important;"><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                                                    <td style="vertical-align:top !important;" class="rightOperation"><?php echo $row->name; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->company; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->email; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->contact; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo $row->address; ?></td>
                                                    <td style="vertical-align:top !important;"><?php echo " Total Posted:$ad_total / Running: $ad_running <br><hr> Est Cost: Rs. $est <br> Paid Cost: Rs. $paid <hr><a target='_blank' href='$ad_link'>View Detail</a>  "; ?></td>

                                                    <td style="vertical-align:top !important;">
                                                        <?php echo $this->admin_user_model->display_name($row->created_by) . '<br/>' . $row->created_at; ?>
                                                    </td>
                                                    <td style="vertical-align:top !important;">
                                                        <?php echo $this->admin_user_model->display_name($row->updated_by) . '<br/>' . $row->updated_at; ?>
                                                    </td>
                                                    <td style="vertical-align:top !important;">
                                                        <?php if ($row->status == '1') { ?>
                                                            <a href="<?php echo site_url('advertisement_provider/change_status/0/' . $row->id) . get_url(); ?>" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo site_url('advertisement_provider/change_status/1/' . $row->id) . get_url(); ?>" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET--> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
