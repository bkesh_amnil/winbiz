<?php
	if(isset($sites)) {
		$sites = 'class = "current"';
	} else {
		$sites = '';
	}	
	$data = $this->admin_user_model->get_credentials('sites');
	if($data['view'])
	{
		$this->data['sub_menus'][] = anchor('sites/', '<span>Manage Sites</span>', $sites);
	}	
	
	if(isset($modules)) {
		$modules = 'class = "current"';
	} else {
		$modules = '';
	}	
	$data = $this->admin_user_model->get_credentials('modules');
	if($data['view'])
	{
		$this->data['sub_menus'][] = anchor('modules/', '<span>Manage Modules</span>', $modules);
	}	
	
	

?>
