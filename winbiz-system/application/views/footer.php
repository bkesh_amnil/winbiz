<input type="hidden" id="baseUrl" value="<?php echo base_url() ?>" />
<input type="hidden" name="hidcontname" id="hidcontname" value="<?php echo $page_name ?>">
</div>
<!-- BEGIN FOOTER -->
<div id="footer">
    2013 &copy; AMNIL Technologies Pvt. Ltd.
    <div class="span pull-right">
        <span class="go-top"><i class="icon-arrow-up"></i></span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<?php
if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $js){
        if (file_exists("./assets/" . $js) && !is_dir("./assets/" . $js)) {
            ?>
            <script src="<?= base_url(); ?>assets/<?= $js ?>" type="text/javascript"></script>
            <?php
        }
    }
}
?>
<!--<script type="text/javascript" src="<?php echo base_url('assets/scripts/jquery.autocomplete.js') ?>"></script>-->
<!--[if lt IE 9]>
<script src="assets/plugins/excanvas.js"></script>
<script src="assets/plugins/respond.js"></script>	
<![endif]-->	
<script>
    jQuery(document).ready(function() {
        App.init(); // initlayout and core plugins
        //FormComponents.init();
        var pageName = $("#hidcontname").val();
        if (pageName != 'shop') {
            if ($(".applyDataTable").find("tbody").find("tr").length > 1) {
                var lstIndex = $(".applyDataTable").find("tbody").find("tr:eq(0)").find("td").length;
                $(".applyDataTable").dataTable({
                    "sDom": '<"top">rt<"bottom"lp<"clear">><"clear">',
                    "bStateSave": true,
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "aoColumnDefs": [
                        {'bSortable': false, 'aTargets': [1]},
                        {'bSortable': false, 'aTargets': [(lstIndex - 1)]}
                    ]

                });
            }
        }
        if ($(".no_sort").find("tbody").find("tr").length > 1) {
            var lstIndex = $(".applyDataTable").find("tbody").find("tr:eq(0)").find("td").length;
            $(".no_sort").dataTable({
                "sDom": '<"top">rt<"bottom"lp<"clear">><"clear">',
                "bStateSave": true,
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": false,
                "bSort": true,
                "bInfo": false,
                "bAutoWidth": false,
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [1]},
                    {'bSortable': false, 'aTargets': [(lstIndex - 1)]}
                ]

            });
        }
<?php if (!isset($page_name) || empty($page_name)) { ?>
    Index.init();
    Index.initJQVMAP(); // init index page's custom scripts
    Index.initKnowElements(); // init circle stats(knob elements)
    Index.initPeityElements(); // init pierty elements
    Index.initCalendar(); // init index page's custom scripts
    Index.initCharts(); // init index page's custom scripts
    Index.initChat();
    Index.initDashboardDaterange();
    Index.initIntro();
<?php } ?>
    $(".alert-close").click(function() {
        $(this).parents(".alert").animate({
            height: "hide",
            opacity: 0
        }, 300, function() {
            $(this).remove();
        })
        return false;
    })
});
$('.icon-arrow-up').click(function() {
    $("html, body").animate({scrollTop: 0}, 1000);
    return false;
})
</script>
<script>
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }
    $("#autocomplete-multiple")
        // don't navigate away from the field on tab when selecting an item
        .bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                event.preventDefault();

            }
        }).autocomplete({
            source: function(request, response) {
            // $.getJSON("<?php echo base_url('ajax/getSearchCategoryList'); ?>", {
            //    term: extractLast(request.term)
            //}, response);
            },
            search: function() {
                // custom minLength
            var term = extractLast(this.value);
            if (term.length < 2) {
                return false;
            }
        }   ,
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function(event, ui) {
            $("<div></div>")
                .addClass("ui-autocomplete-multiselect-item")
                .text(ui.item.label)
                .append(
                    $("<span></span>")
                    .addClass("ui-icon ui-icon-close")
                    .attr('data-id', ui.item.id)
                    .click(function() {
                        var item = $(this).parent();
                        var removelist = ',' + ($(this).attr('data-id'));
                        var categorylists = $('#autocomplete-multiple').attr('data-id');
                        item.remove();
                        $('#autocomplete-multiple').attr('data-id', categorylists.replace(removelist, ''));
                    })
                )
                .insertBefore(this);
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                this.value = '';
                var idterms = split($('#autocomplete-multiple').attr('data-id'));
                if (idterms.length > 0) {
                    idterms += (",") + (ui.item.id);
                }
                $("#autocomplete-multiple").attr('data-id', idterms);
                return false;
        }
    });
</script>
</body>
<div class="modal mymodal modal-styled fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Styled Modal</h3>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-tertiary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-content-model">Save</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</html>