<?php if(isset($id) && !empty($id)){ ?>
<div class="span3">
  <div class="widget">
    <div class="widget-title">
      <h4><i class="icon-reorder"></i> Author Details</h4>
    </div>
    <div class="widget-body form">
      <div class="DataLog">
          <ul class="todo-list">
            <li><span class="title1">Created By:</span><span class="title2"><?php echo $this->admin_user_model->display_name($created_by) ?></span></li>
            <li><span class="title1">Created Date:</span><span class="title2"><?php echo date("jS M, Y (D) g:i a", strtotime($created_date)); ?></span></li>
            <li><span class="title1">Last Updated By:</span><span class="title2"><?php echo $this->admin_user_model->display_name($updated_by) ?></span></li>
            <li><span class="title1">Last Updated Date:</span><span class="title2"><?php echo date("jS M, Y (D) g:i a", strtotime($updated_date)); ?></span></li>
            <?php
            if(isset($hits) && $hits > 0){
              ?>
              <li><span class="title1">Hits:</span><span class="title2"><?php echo $hits; ?></span></li>
              <?php
            }
            ?>
          </ul>
          <div class="clear"></div>
        </div>
      </div>
  </div>
</div>
<?php 
}else{
  ?>
  <div class="span3">
    <div class="widget">
      <div class="widget-title">
        <h4><i class="icon-reorder"></i>Author Details</h4>
      </div>
      <div class="widget-body form">
        <div class="DataLog">
            <ul class="todo-list">
              <li><span class="title1">Created By:</span><span class="title2"><?php echo $this->admin_user_model->display_name($this->session->userdata("admin_id")) ?></span></li>
              <li><span class="title1">Created Date:</span><span class="title2">To Be Created Now</span></li>
              <li><span class="title1">Last Updated By:</span><span class="title2">&nbsp;</span></li>
              <li><span class="title1">Last Updated Date:</span><span class="title2">&nbsp;</span></li>
              <?php
              if(isset($hits) && $hits > 0){
              ?>
                <li><span class="title1">Hits:</span><span class="title2">&nbsp;</span></li>
              <?php
              }
              ?>
            </ul>
            <div class="clear"></div>
          </div>
        </div>
    </div>
  </div>
  <?php
} 
?>
                      
  