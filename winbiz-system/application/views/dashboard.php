<?php /*if($same_password) : ?>

<p class="info" id="warning"><span class="info_inner">Your password is as same as your username. Please change your password for security reason. <?php echo anchor(site_url('my_profile/change_password'), 'Click here to change your password') ?></span></p>
<?php endif; ?>
<?php  if ($this->session->flashdata('class')): ?>
<p class="info" id="<?php echo $this->session->flashdata('class') ?>"><span class="info_inner"><?php echo $this->session->flashdata('msg') ?></span></p>
<?php  endif; */ ?>

<!-- BEGIN PAGE -->

<div id="body"> 
  <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  <div id="widget-config" class="modal hide">
    <div class="modal-header">
      <button data-dismiss="modal" class="close" type="button">×</button>
      <h3>Widget Settings</h3>
    </div>
    <div class="modal-body">
      <p>Here will be a configuration form</p>
    </div>
  </div>
  <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM--> 
  <!-- BEGIN PAGE CONTAINER-->
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> Dashboard <small>statistics and more</small> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="#">Home</a> <span class="divider">/</span> </li>
          <li><a href="#">Dashboard</a></li>
          <li class="pull-right dashboard-report-li">
            <div id="dashboard-report-range" class="dashboard-report-range-container no-text-shadow tooltips"  data-placement="top" data-original-title="Change dashboard date range"><i class="icon-calendar icon-large"></i><span></span> <b class="caret"></b> </div>
          </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER--> 
    <!-- BEGIN PAGE CONTENT-->
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN SERVER LOAD PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i>Menu</h4>
              <span class="tools"> <a href="javascript:;" class="icon-chevron-down"></a> </span> </div>
            <div class="widget-body">
                <?php
                if(isset($dashboardmenus) && !empty($dashboardmenus) && is_array($dashboardmenus)){ 
                    $count = count($dashboardmenus);
                    $i = 1;
                ?>
                <div class="row-fluid"> 
                <?php 
                foreach($dashboardmenus as $ind=>$menu){
                    if($ind%4 == 0){
                ?>
                </div>
                <div class="row-fluid">
                    <?php
                    }
                    ?>
                    <a href="<?php echo site_url($menu->controller) ?>" class="icon-btn span3"> 
                        <?php
                        $iconclass = explode('/', $menu->controller);
                        $iconclass = implode('-', $iconclass);
                        ?>
                        <i class="icon-<?php echo $iconclass ?>"></i>
                        <div><?php echo $menu->name ?></div>
                    </a> 
                <?php } ?>
                </div>
                <?php
                }
                ?>
            </div>
          </div>
          <!-- END SERVER LOAD PORTLET--> 
        </div>
      </div>
    </div>
    <!-- END PAGE CONTENT--> 
  </div>
  <!-- END PAGE CONTAINER--> 
</div>