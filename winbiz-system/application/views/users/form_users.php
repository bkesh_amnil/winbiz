<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 

                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"> <?php echo $title ?> </h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?php echo $title ?> </a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php
        $this->load->view("show_errors");
        ?>
        <div id="page" class="dashboard">
            <form action="" method="post" autocomplete="off" id="form1" class="horizontal-form">
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i><?php echo $title ?> Form</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label">Select Profile : *</label>
                                            <div class="controls">
                                                <?php
                                                $style = " style='display:none'";
                                                $disabled = "";
                                                if (isset($enterprise_name) && !empty($enterprise_name)) {
                                                    $style = " style='display:block'";
                                                    $disabled = " disabled='disabled'";
                                                }
                                                ?>
                                                <select name="profile_id" class="span12"<?php echo $disabled ?>>
                                                    <option>Select Profile</option>
                                                    <?php
                                                    if (isset($profiles) && !empty($profiles) && is_array($profiles)) {
                                                        foreach ($profiles as $profile) {
                                                            $selected = '';
                                                            $disabled1 = '';
                                                            if ($profile->id == $profile_id) {
                                                                $selected = ' selected="selected"';
                                                            }
                                                            if ($profile->id == $current_profile_id) {
                                                                $disabled1 = " disabled='disabled'";
                                                            }
                                                            ?>
                                                            <option value="<?php echo $profile->id ?>" rel="<?php echo $profile->enterprise_related ?>"<?php echo $selected ?><?php echo $disabled1 ?>><?php echo $profile->profile_name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <?php //echo form_dropdown('profile_id', $profiles, $profile_id, 'class="span12"') ?>
                                            </div>
                                        </div>
                                        <?php
                                        $style = " style='display:none'";
                                        $disabled = "";
                                        if (isset($enterprise_name) && !empty($enterprise_name)) {
                                            $style = " style='display:block'";
                                            $disabled = " readonly";
                                        }
                                        ?>
                                        <div class="control-group enterprise_id"<?php echo $style ?>>
                                            <label class="control-group">Select Enterprise : *</label>
                                            <div class="controls">
                                                <?php echo form_input('enterprise_name', $enterprise_name, 'class="span12" id="enterprise"' . $disabled) ?>
                                            </div>
                                            <!-- <div class="controls">
                                              <select name="enterprise_id" class="span12">
                                            <?php
                                            if (isset($enterprises) && !empty($enterprises) && is_array($enterprises)) {
                                                foreach ($enterprises as $enterprise) {
                                                    $selected = "";
                                                    if ($enterprise->id == $enterprise_id) {
                                                        $selected = " selected='selected'";
                                                    }
                                                    ?>
                                                          <option value="<?php echo $enterprise->id; ?>"<?php echo $selected ?>><?php echo $enterprise->enterprise_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                              </select>
                                            </div> -->
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Username : *</label>
                                            <div class="controls"><?php echo form_input('user_name', $user_name, 'class="span12" ' . $readonly) ?><span class="help-inline">Password will be same as username.</span></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">First Name : *</label>
                                            <div class="controls"><?php echo form_input('first_name', $first_name, 'class="span12" ') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Last Name :</label>
                                            <div class="controls"><?php echo form_input('last_name', $last_name, 'class="span12" ') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">E-mail : *</label>
                                            <div class="controls"><?php echo form_input('email', $email, 'class="span12" ') ?></div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label">Gender : *</label>
                                            <div class="controls"><?php echo form_dropdown('gender', $genders, $gender, 'class="span12"') ?></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Mobile Number :</label>
                                            <div class="controls"> <?php echo form_input('mobile_num', $mobile_num, 'class="span12" ') ?> </div>
                                            <div class="control-group">
                                                <label class="control-label">Address :</label>
                                                <div class="controls"><?php echo form_input('address', $address, 'class="span12" ') ?></div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"> Status : </label>
                                                <div class="controls"><?php echo form_dropdown('status', $statuses, $status, 'class="span12"') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a> </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#btn_submit').click(function (e) {
            $('#form1').submit();
        });
        $("#enterprise").autocomplete(
                "<?php echo base_url(); ?>enterprise/autocomplete",
                {
                    delay: 1,
                    minChars: 1,
                    matchSubset: 1,
                    matchContains: 1,
                    cacheLength: 10,
                    onItemSelect: selectItem,
                    onFindValue: findValue,
                    formatItem: formatItem,
                    autoFill: true
                }
        );
    });
    /* autocomplete functions */
    function findValue(li) {
        if (li == null)
            return alert("No match!");
        // if coming from an AJAX call, let's use the CityId as the value
        if (!!li.extra)
            var sValue = li.extra[0];
        // otherwise, let's just display the value in the text box
        else
            var sValue = li.selectValue;
        //alert("The value you selected was: " + sValue);
    }

    function selectItem(li) {
        findValue(li);
    }

    function formatItem(row) {
        return row[0];
    }
    /* autocomplete functions */
    $(document).on("change", "select[name=profile_id]", function () {
        var value = $("select[name=profile_id] option:selected").attr('rel');
        if (value == "1") {
            $("div.enterprise_id").css("display", "block");
        } else {
            $("div.enterprise_id").css("display", "none");
        }
    })
</script> 
