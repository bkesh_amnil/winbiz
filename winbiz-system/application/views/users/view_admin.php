<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
    $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body">
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php 
                // if($add) { 
                //   echo anchor("$page_name/form", '<button class="btn btn-danger btn-success addIcon"><i class="icon-plus icon-white"></i> Create</button>');
                ?>
                <?php 
                // }
                if($edit) { 
                ?>
                  <a href="<?= site_url("$page_name/form/") ?>" class="edit_icon editIcon" title="Edit">
                    <button class="btn btn-danger btn-info"><i class="icon-pencil icon-white"></i> Edit</button>
                  </a>
                <?php 
                } 
                // if($delete) { 
                  ?>
                 <!--   <a href="<?= site_url("$page_name/delete/") ?>" class="delete_icon deleteIcon" title="Delete">
                    <button class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</button>
                  </a> -->
                  <?php 
                // }
                ?>
              </div>
            </div>
            <div class="widget-body">
              <table id="box-table-a" summary="Employee Pay Sheet" class="applyDataTable table table-striped table-bordered table-hover dataTable">
                <thead>
                  <tr>
                    <th scope="col">S.N.</th>
                    <th scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                    <th scope="col">Site Title</th>
                    <th scope="col">Profile Name</th>
                    <th scope="col">Username</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">E - mail</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($rows) == 0 ) : ?>
                  <tr>
                    <td colspan="7" align="center" height="24">No Records Found! </td>
                  </tr>
                  <?php else: ?>
                  <?php $i = $start + 1; foreach($rows as $row) : ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td>
                    <input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->site_title ?></td>
                    <td><?php echo $row->profile_name ?></td>
                    <td><?php echo $row->user_name ?></td>
                    <td><?php echo $row->first_name . ' ' . $row->last_name ?></td>
                    <td><?php echo $row->email ?></td>
                  </tr>
                  <?php $i++; endforeach; endif;
			?>
                  
                </tbody>
              </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#Search').click(function(){
		var searchPrm = $.trim($('#searchtext').val());
		var site = parseInt($('#sites').val());
		var url = "<?php echo site_url('admin_user') ?>";
		var siteSer = false;
		if(site > 0){
			url = url+"?site="+site;
			siteSer = true;
		}
		if(searchPrm != ""){
			if(siteSer == true){
				url = url+"&search="+searchPrm;
			}else{
				url = url+"?search="+searchPrm;
			}
		}
		window.location = url;
		return false;
	});
});
</script> 
