<div id="body">
    <div class="container-fluid"> 
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12"> 

                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"> <?php echo $title ?> </h3>
                <ul class="breadcrumb">
                    <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
                    <li><a href="#"><?php echo $title ?> </a></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB--> 
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php
        $this->load->view("show_errors");
        $modulesCount = count($modules);
        $viewCount = 0;
        $addCount = 0;
        $editCount = 0;
        $deleteCount = 0;
        foreach ($modules as $module) {
            $credentials = $this->admin_user_model->get_page_settings($profile_id, $module->id);
            if ($credentials['view']) {
                $viewCount++;
            }
            if ($credentials['add']) {
                $addCount++;
            }
            if ($credentials['edit']) {
                $editCount++;
            }
            if ($credentials['delete']) {
                $deleteCount++;
            }
        }
        if ($modulesCount == $viewCount && $modulesCount == $addCount && $modulesCount == $editCount && $modulesCount == $deleteCount) {
            $checkedWhole = 'checked';
        }
        if ($modulesCount == $viewCount) {
            $checkedView = 'checked';
        }
        if ($modulesCount == $addCount) {
            $checkedAdd = 'checked';
        }
        if ($modulesCount == $editCount) {
            $checkedEdit = 'checked';
        }
        if ($modulesCount == $deleteCount) {
            $checkedDelete = 'checked';
        }
        ?>
        <div id="page" class="dashboard">
            <form action="<?php echo site_url('profile/manage_credentials') ?>" method="get" autocomplete="off" style="float:left; margin-bottom:0;">
                <div class="searchByTitle">
                    <input type="text" class="search" name="search" value="" style="display:inline !important;" />
                </div>
                <div class="searchByTitle">
                    <button type="submit" class="btn green" id="btnSearch"  value="Search"> <i class="icon-search icon-white"></i> </button>
                </div>
            </form>
            <form id="form1" method="post" autocomplete="off" class="horizontal-form">
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="widget">
                            <div class="widget-title">
                                <h4><i class="icon-reorder"></i>Content</h4>
                            </div>
                            <div class="widget-body form">
                                <div class="row-fluid">
                                    <table class="table table-striped table-bordered table-hover dataTable" id="box-table-a" summary="Employee Pay Sheet">
                                        <thead>
                                            <tr>
                                                <th width="10" scope="col">S.N.</th>
                                                <th width="412" scope="col">Module Name</th>
                                                <th width="149" scope="col">All
                                                    <input type="checkbox" id="checkboxWhole" name="checkboxWhole[]" value="whole" class="CheckWhole" <?php echo isset($checkedWhole) ? $checkedWhole : ''; ?> />
                                                </th>
                                                <th width="149" scope="col">View
                                                    <input type="checkbox" id="checkboxViewAll" rel="view" name="checkboxViewAll[]" value="viewall" class="check_all CheckViewAll" <?php echo isset($checkedView) ? $checkedView : ''; ?>/>
                                                </th>
                                                <th width="146" scope="col">Add
                                                    <input type="checkbox" id="checkboxAddAll" rel="add" name="checkboxAddAll[]" value="addall" class="check_all CheckAddAll" <?php echo isset($checkedAdd) ? $checkedAdd : ''; ?>>
                                                </th>
                                                <th width="146" scope="col">Edit
                                                    <input type="checkbox" id="checkboxEditAll" rel="edit" name="checkboxEditAll[]" value="editall" class="check_all CheckEditAll" <?php echo isset($checkedEdit) ? $checkedEdit : ''; ?>/>
                                                </th>
                                                <th width="146" scope="col">Delete
                                                    <input type="checkbox" id="checkboxDeleteAll" rel="delete" name="checkboxDeleteAll[]" value="deleteall" class="check_all CheckDeleteAll" <?php echo isset($checkedDelete) ? $checkedDelete : ''; ?>/>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $array = array();
                                            $i = 1;
                                            $enabledModules = $this->global_model->get_single_data("site", "modules_enabled", $site_id);
                                            
                                            $enabledModules = explode(",", $enabledModules);
                                            $modulesCount = count($modules);
                                            $HTML = '';

                                            foreach ($modules as $module) {
                                                if (in_array($module->id, $enabledModules)) {
                                                    $childs = $this->module_model->getModulesByParentId($module->id);

                                                    $parentHTML = '<tr class = "parent" rel = "' . $module->id . '" data-relation = "parent">
                        <td>' . $i . '</td>
                        <td>' . $module->module_title . '
                        <input type="hidden" name="module_ids[]" value="' . $module->id . '" /></td>';
                                                    $credentials = $this->admin_user_model->get_page_settings($profile_id, $module->id);
                                                    $parentHTML .= '<td>
                        <input type="checkbox" name="checkboxAll[]" value="all" class="CheckAll"';
                                                    if ($credentials['view'] && $credentials['add'] && $credentials['edit'] && $credentials['delete']) {
                                                        $parentHTML .= 'checked="checked"';
                                                    }
                                                    $parentHTML .= 'rel="' . $module->id . '" /></td>
                        <td width="149"><input name="view[' . $module->id . ']" class="all view checkBox' . $module->id . '" data-rel="view" type="checkbox" value="yes" rel="' . $module->id . '"';
                                                    if ($credentials['view']) {
                                                        $parentHTML .= 'checked="checked"';
                                                    }
                                                    $parentHTML .= '/></td>
                        <td width="146"><input name="add_mod[' . $module->id . ']" class="all add checkBox' . $module->id . '" data-rel="add" type="checkbox" value="yes" rel="' . $module->id . '"';
                                                    if ($credentials['add']) {
                                                        $parentHTML .= 'checked="checked"';
                                                    }
                                                    $parentHTML .= '/></td>
                        <td width="135"><input name="edit[' . $module->id . ']" class="all edit checkBox' . $module->id . '" data-rel="edit" type="checkbox" value="yes" rel="' . $module->id . '"';
                                                    if ($credentials['edit']) {
                                                        $parentHTML .= 'checked="checked"';
                                                    }
                                                    $parentHTML .= '/></td>
                        <td><input name="delete[' . $module->id . ']" class="all delete checkBox' . $module->id . '" data-rel="delete" type="checkbox" value="yes"  rel="' . $module->id . '"';
                                                    if ($credentials['delete']) {
                                                        $parentHTML .= 'checked="checked"';
                                                    }
                                                    $parentHTML .= '/></td>
                      </tr>';

                                                    if (isset($childs)) {
                                                        $childHTML = '';
                                                        foreach ($childs as $child) {

                                                            $childHTML .= '<tr class ="child_' . $module->id . '"  rel = "' . $child['id'] . '" parent_module = "' . $module->id . '" data-relation = "child">
                        <td>&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $child['module_title'];
                                                            $childHTML .= '<input type="hidden" name="module_ids[]" value="' . $child['id'] . '" /></td>';
                                                            $credentials = $this->admin_user_model->get_page_settings($profile_id, $child['id']);
                                                            $childHTML .= '<td>
                        <input type="checkbox" name="checkboxAll[]" value="all" class="CheckAll"';
                                                            if ($credentials['view'] && $credentials['add'] && $credentials['edit'] && $credentials['delete']) {
                                                                $childHTML.= 'checked="checked"';
                                                            }
                                                            $childHTML .= 'rel="' . $child['id'] . '" /></td>
                        <td width="149"><input name="view[' . $child['id'] . ']" class="all view checkBox' . $child['id'] . '" data-rel="view" type="checkbox" value="yes" rel="' . $child['id'] . '"';
                                                            if ($credentials['view']) {
                                                                $childHTML .= 'checked="checked"';
                                                            }
                                                            $childHTML .= '/></td>
                        <td width="146"><input name="add_mod[' . $child['id'] . ']" class="all add checkBox' . $child['id'] . '" data-rel="add" type="checkbox" value="yes" rel="' . $child['id'] . '"';
                                                            if ($credentials['add']) {
                                                                $childHTML .= 'checked="checked"';
                                                            }
                                                            $childHTML .= '/></td>
                        <td width="135"><input name="edit[' . $child['id'] . ']" class="all edit checkBox' . $child['id'] . '" data-rel="edit" type="checkbox" value="yes" rel="' . $child['id'] . '"';
                                                            if ($credentials['edit']) {
                                                                $childHTML .= 'checked="checked"';
                                                            }
                                                            $childHTML .= '/></td>
                        <td><input name="delete[' . $child['id'] . ']" class="all delete checkBox' . $child['id'] . '" data-rel="delete" type="checkbox" value="yes" rel="' . $child['id'] . '"';
                                                            if ($credentials['delete']) {
                                                                $childHTML .= 'checked="checked"';
                                                            }
                                                            $childHTML .= '/></td>
                      </tr>';
                                                        }
                                                    }
                                                    $i++;
                                                    $HTML .= $parentHTML . $childHTML;
                                                }
                                            }
                                            echo $HTML;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions"> <a class="btn btn-primary" id="btn_submit"><span>Save</span></a> <a class="btn" href="<?= site_url("$page_name") ?>"><span>Cancel</span></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#btn_submit').click(function (e) {
            $('#form1').submit();
        });
        $('input.search').quicksearch('table.dataTable tbody tr');
        jQuery('.CheckWhole').change(function () {
            var checked = jQuery(this).is(":checked");
            var set = ".CheckAll";
            var set1 = ".check_all";
            var set2 = ".all";
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery(set1).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery(set2).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
            jQuery.uniform.update(set1);
            jQuery.uniform.update(set2);
        });

        jQuery('.check_all').change(function () {
            var attrRel = $(this).attr('rel');
            var checked_length = $(".check_all:checked").length;
            var checked = $(this).is(':checked');
            if (checked) {
                $('#box-table-a').find('input[data-rel="' + attrRel + '"]').each(function (i, v) {
                    $(this).attr("checked", true);
                });
            } else {
                $('#box-table-a').find('input[data-rel="' + attrRel + '"]').each(function (i, v) {
                    $(this).attr("checked", false);
                });
            }
            jQuery.uniform.update('input[data-rel=' + attrRel + ']');
            $('#box-table-a').find('input[data-rel="' + attrRel + '"]').each(function (i, v) {
                var parentTr = $(this).parents('tr');
                var checkedLength = parentTr.find('.all:checked').length;
                if (checkedLength == 4) {
                    parentTr.find('.CheckAll').attr("checked", true);
                } else {
                    parentTr.find('.CheckAll').attr("checked", false);
                }

            });
            if (checked_length == 4) {
                $('.CheckWhole').attr("checked", true);
            }
            else {
                $('.CheckWhole').attr("checked", false);
            }
            jQuery.uniform.update('.CheckWhole');
            jQuery.uniform.update('.CheckAll');
        });

        jQuery('.CheckAll').change(function () {
            var parentid = $(this).parents('tr').attr('parent_module');
            if (parentid != undefined) {
                var rowLength = $('.CheckAll').length;
                var checkedLength = $('.CheckAll:checked').length;
                var set1 = ".CheckWhole";
                var set2 = ".check_all";
                var checked = jQuery(this).is(":checked");
                var childLength = $('.child_' + parentid).find('.CheckAll').length;
                var childLengthChecked = $('.child_' + parentid).find('.CheckAll:checked').length;
                $(this).parents('tr').find('.all').each(function (i, v) {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                jQuery.uniform.update('.all');
                if (rowLength == checkedLength) {
                    jQuery(set1).each(function () {
                        $(this).attr("checked", true);
                    });
                    jQuery(set2).each(function () {
                        $(this).attr("checked", true);
                    });
                } else {
                    jQuery(set1).each(function () {
                        $(this).attr("checked", false);
                    });
                    jQuery(set2).each(function () {
                        $(this).attr("checked", false);
                    });
                }
                console.log(childLengthChecked + ' > ' + childLength)
                if (childLengthChecked == childLength) {
                    $('tr[rel="' + parentid + '"] .CheckAll').attr("checked", true);
                    $('tr[rel="' + parentid + '"] .all').attr("checked", true);
                }
                else {
                    if (childLengthChecked > 0) {
                        $('tr[rel="' + parentid + '"] .all').attr("checked", true);
                    } else {
                        $('tr[rel="' + parentid + '"] .all').attr("checked", false);
                    }
                    $('tr[rel="' + parentid + '"] .CheckAll').attr("checked", false);
                }
                jQuery.uniform.update('.CheckAll');
                jQuery.uniform.update('.all');
                jQuery.uniform.update(set1);
                jQuery.uniform.update(set2);
                var totalLength = $('.CheckAll').length;
                var checkedLength = $('.CheckAll:checked').length;
                if (checkedLength == totalLength) {
                    $('.CheckWhole').attr("checked", true);
                    $('.check_all').attr("checked", true);
                } else {
                    $('.CheckWhole').attr("checked", false);
                    $('.check_all').attr("checked", false);
                }
                jQuery.uniform.update('.CheckWhole');
                jQuery.uniform.update('.check_all');
            }
        });

        $('.all').change(function () {
            var attRel = $(this).attr("data-rel");
            var rowLength = $('.CheckAll').length;
            var parentTr = $(this).parents('tr');
            var all = parentTr.find('.all');
            var checkRel = parentTr.find('.CheckAll').attr("rel");
            var checkedLength = parentTr.find('.all:checked').length;
            var parentid = $(this).parents('tr').attr('parent_module');
            var dataRel = $(this).parents('tr').attr('data-relation');
            var childLength = $('.child_' + parentid).find('.CheckAll').length;
            var checkedParent = $('tr[rel="' + parent + '"]').find('.all:checked').length;
            var childLengthChecked = $('.child_' + parentid).find('input[data-rel="' + attRel + '"]:checked').length;
            if (dataRel == 'child') {
                //to check the box of parentTr CheckAll
                if (checkedLength == 4) {
                    parentTr.find('.CheckAll').attr("checked", true);
                } else {
                    parentTr.find('.CheckAll').attr("checked", false);
                }
                jQuery.uniform.update('.CheckAll');
                //to check the parentTr CheckAll
                if (childLengthChecked > 0) {
                    $('tr[rel="' + parentid + '"]').find('input[data-rel="' + attRel + '"]').attr("checked", true);
                } else {
                    $('tr[rel="' + parentid + '"]').find('input[data-rel="' + attRel + '"]').attr("checked", false);
                }
                jQuery.uniform.update('.all');
                var allLengthChecked = $('.child_' + parentid).find('.CheckAll:checked').length;
                if (allLengthChecked == childLength) {
                    $('tr[rel="' + parentid + '"] .CheckAll').attr("checked", true);
                } else {
                    $('tr[rel="' + parentid + '"] .CheckAll').attr("checked", false);
                }
                jQuery.uniform.update('.CheckAll');
                jQuery.uniform.update('.all');
            } else {
                var rel = $(this).parents('tr').attr('rel');
                var checked = $(this).is(':checked');
                var checkedLength = $(this).parents('tr').find('.all:checked').length;
                jQuery('tr[parent_module="' + rel + '"]').find('input[data-rel="' + attRel + '"]').each(function (i, v) {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
                if (checkedLength == 4) {
                    $(this).parents('tr').find('.CheckAll').attr('checked', true);
                    $('.child_' + rel).find('.CheckAll').attr('checked', true);
                } else {
                    $(this).parents('tr').find('.CheckAll').attr('checked', false);
                    $('.child_' + rel).find('.CheckAll').attr('checked', false);
                }
                jQuery.uniform.update('tr[parent_module="' + rel + '"] input[data-rel="' + attRel + '"]');
                jQuery.uniform.update('.CheckAll');
            }

            //to check the check_all if every all checked
            if ($('#box-table-a').find('input[data-rel="' + attRel + '"]:checked').length == rowLength) {
                $('#box-table-a').find('input[rel="' + attRel + '"]').attr('checked', true);
            } else {
                $('#box-table-a').find('input[rel="' + attRel + '"]').attr('checked', false);
            }
            jQuery.uniform.update('input[rel="' + attRel + '"]');
            //to enable the checkWhole if all check_all checked
            var checked_length = $(".check_all:checked").length;
            if (checked_length == 4) {
                $('.CheckWhole').attr("checked", true);
            }
            else {
                $('.CheckWhole').attr("checked", false);
            }
            jQuery.uniform.update('.CheckWhole');
            //to disable the parentTr of all if length 0
            if (parentTr.parents('tbody').find('tr[parent_module="' + parentid + '"]').find('input[data-rel="' + attRel + '"]:checked').length == 0) {
                parentTr.parents('tbody').find('tr[rel="' + parentid + '"]').find('input[data-rel="' + attRel + '"]').attr('checked', false);
            } else {
                parentTr.parents('tbody').find('tr[parentidrel="' + parentid + '"]').find('input[data-rel="' + attRel + '"]').attr('checked', true);
            }
            jQuery.uniform.update('.parent input[data-rel="' + attRel + '"]');

            //to enable the parentTr checkAll if parentTr all checked
            if (checkedParent == 4) {
                $('.child_' + parent + ' .CheckAll').attr("checked", true);
            } else {
                $('.child_' + parent + ' .CheckAll').attr("checked", false);
            }

        });

        jQuery('.parent .CheckAll').change(function () {
            var totalLength = $('.CheckAll').length;
            var checked = jQuery(this).is(":checked");
            var parentid = $(this).attr('rel');
            var parentset = 'tr[rel="' + parentid + '"] .all';
            var set = ".child_" + parentid + " .CheckAll";
            var set1 = ".child_" + parentid + " .all";
            jQuery(parentset).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery(set1).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(parentset);
            jQuery.uniform.update(set);
            jQuery.uniform.update(set1);
            var checkedLength = $('.CheckAll:checked').length;
            if (checkedLength == totalLength) {
                $('.CheckWhole').attr("checked", true);
                $('.check_all').attr("checked", true);
            } else {
                $('.CheckWhole').attr("checked", false);
                $('.check_all').attr("checked", false);
            }
            jQuery.uniform.update('.CheckWhole');
            jQuery.uniform.update('.check_all');
        });
    });
</script> 
