<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
    $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body">
              <?php $this->load->view('cms/search') ?>
              <div class="actionsBar">
                <?php 
                if($add) { 
                  echo anchor("$page_name/form", '<button class="btn btn-danger btn-success addIcon"><i class="icon-plus icon-white"></i> Create</button>');
                  ?>
                <?php 
                }
                if($edit) { 
                ?>
                  <a href="<?= site_url("$page_name/manage_credentials/") ?>" class="credential_icon credentialIcon" title="Manage Credentials">
                    <button class="btn btn-danger btn-info"><i class="icon-pencil icon-white"></i> Manage</button>
                  </a>
                  <a href="<?= site_url("$page_name/form/") ?>" class="edit_icon editIcon" title="Edit">
                    <button class="btn btn-danger btn-info"><i class="icon-pencil icon-white"></i> Edit</button>
                  </a>
                  <?php 
                } if($delete) { 
                  ?>
                  <a href="<?= site_url("$page_name/delete/") ?>" class="delete_icon deleteIcon" title="Delete">
                    <button class="btn btn-danger"><i class="icon-remove icon-white"></i> Delete</button>
                  </a>
                  <?php 
                } 
                ?>
              </div>
            </div>
            <div class="widget-body">
              <table class="applyDataTable table table-striped table-bordered table-hover dataTable" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                  <tr>
                    <th width="30" scope="col">S.N.</th>
                    <th width="31" scope="col"><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
                    <th width="172" scope="col">Site Title</th>
                    <th width="140" scope="col">Profile Name</th>
                    <th width="114" scope="col">Created By </th>
                    <th width="136" scope="col">Created Date</th>
                    <th width="122" scope="col">Last Modified By</th>
                    <th width="140" scope="col">Last Modified Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($rows) == 0 ) : ?>
                  <tr>
                    <td colspan="9" align="center" height="24">No Records Found! </td>
                  </tr>
                  <?php else: ?>
                  <?php $i = $start + 1; foreach($rows as $row) : ?>
                  <tr superadmin="<?php echo $row->super_admin; ?>">
                    <td><?php echo $i ?></td>
                    <td>
                    <input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td class="rightOperation" rel="<?php echo $row->id; ?>"><?php echo $row->site_title ?></td>
                    <td class="profilename"><?php echo $row->profile_name ?></td>
                    <td><?php echo $this->admin_user_model->display_name($row->created_by) ?></td>
                    <td><?php echo $row->created_date ?></td>
                    <td><?php echo $this->admin_user_model->display_name($row->modified_by) ?></td>
                    <td><?php echo $row->modified_date ?></td>
                  </tr>
                  <?php $i++; endforeach; endif;
			?>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="hidden_profile_name" value="<?php echo $profile_name->profile_name ?>" />
<script type="text/javascript">
$(document).ready(function(){
	$('#Search').click(function(){
		var searchPrm = $.trim($('#searchtext').val());
		var site = parseInt($('#sites').val());
		var url = "<?php echo site_url('profile') ?>";
		var siteSer = false;
		if(site > 0){
			url = url+"?site="+site;
			siteSer = true;
		}
		if(searchPrm != ""){
			if(siteSer == true){
				url = url+"&search="+searchPrm;
			}else{
				url = url+"?search="+searchPrm;
			}
		}
		window.location = url;
		return false;
	});
});
</script> 
