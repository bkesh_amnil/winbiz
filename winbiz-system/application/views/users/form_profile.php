<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?= $con_title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <form action="" method="post" autocomplete="off" class="horizontal-form" id="form1">
        <div class="row-fluid">
          <div class="span9"> 
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="widget">
              <div class="widget-title">
                <h4><i class="icon-reorder"></i><?php echo $title ?> Form</h4>
              </div>
              <div class="widget-body form">
                <div class="row-fluid">
                  <div class="span6 ">
                    <!-- <div class="control-group">
                      <label class="control-label">Site Title : *</label>
                      <div class="controls"> <?php echo form_dropdown('site_id', $site_ids, $site_id, 'class="span12"') ?> </div>
                    </div> -->
                    <div class="control-group">
                      <label class="control-label">Profile Name : *</label>
                      <div class="controls"><?php echo form_input('profile_name', $profile_name, 'class="span12"') ?></div>
                    </div>
                  </div>
                  <div class="span6">
                    <?php if($user_id == '1') { ?>
                      <div class="control-group">
                        <label class="control-label">Super Administrator : *</label>
                        <div class="controls"><?php echo form_dropdown('super_admin', $super_admins, $super_admin, 'class="span12"') ?></div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Related to Enterprise : *</label>
                        <div class="controls"><?php echo form_dropdown('enterprise_related', $enterprise_relations, $enterprise_related, 'class="span12"') ?></div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-actions">
                  <input type="hidden" name="id" value="<?php echo $user_id; ?>" />
                  <input type="hidden" name="site_id" value="<?php echo $siteId ?>" />
                  <a class="btn btn-primary" id="btn_submit"><span>Save</span></a>
                  <a class="btn" href="<?= site_url("$page_name/") ?>"><span>Cancel</span></a>
                </div>
              </div>
            </div>
          </div>
          <?php
          $this->load->view("user_detail_common");
          ?>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
    	$('#btn_submit').click(function(e) {
            $('#form1').submit();
        });
	});
</script> 
