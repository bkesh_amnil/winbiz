<?php
	if(isset($profile)) {
		$profile = 'class = "current"';
	} else {
		$profile = '';
	}	
	$data = $this->admin_user_model->get_credentials('profile');
	if($data['view'])
	{
		$this->data['sub_menus'][] = anchor('profile/', '<span>Manage Profiles</span>', $profile);
	}	
	
	if(isset($admin_user)) {
		$admin_user = 'class = "current"';
	} else {
		$admin_user = '';
	}
	$data = $this->admin_user_model->get_credentials('admin_user');
	if($data['view'])
	{
		$this->data['sub_menus'][] = anchor('admin_user/', '<span>Manage SuperAdmin</span>', $admin_user);
	}	
	
	if(isset($other_users)) {
		$other_users = 'class = "current"';
	} else {
		$other_users = '';
	}
	$data = $this->admin_user_model->get_credentials('other_users');
	if($data['view'])
	{
		$this->data['sub_menus'][] = anchor('other_users/', '<span>Manage Other Users</span>', $other_users);
	}	
		
	

?>
