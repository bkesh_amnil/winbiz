<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>AMNIL CMS :: Login</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/css/themes/default.css" rel="stylesheet" id="style_color" />
  <link href="<?php echo base_url(); ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL STYLES -->  
  <link href="<?php echo base_url(); ?>assets/css/pages/login.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN LOGO -->
<div id="logo" class="center"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="logo" class="center" /> </div>
<!-- END LOGO --> 

<!-- BEGIN LOGIN -->
<div id="login"> 
  <!-- BEGIN LOGIN FORM -->
  <form id="form_login" name="form_login" method="post" action="" autocomplete="off" class="form-vertical no-padding no-margin">
    <?php 
    if($this->session->flashdata('class')): 
    ?>
      <div class="alert alert-error">
        <a class="alert-close close" data-dismiss="alert">×</a>
        <span class="info_inner"><?= $this->session->flashdata('login') ?></span>
      </div>      
    <?php 
    endif; 
    ?>
    <p class="center">Enter your username and password.</p>
    <div class="control-group">
      <div class="controls">
        <div class="input-prepend"> <span class="add-on"><i class="icon-user"></i></span>
          <input id="input-username" type="text" name="user_name" placeholder="Username" />
        </div>
      </div>
    </div>
    <div class="control-group">
      <div class="controls">
        <div class="input-prepend"> <span class="add-on"><i class="icon-lock"></i></span>
          <input id="input-password" type="password" name="pass_word" placeholder="Password" />
        </div>
        <div class="block-hint pull-right"> <a href="javascript:;" class="" id="forget-password">Forgot Password?</a> </div>
        <div class="clearfix space5"></div>
      </div>
    </div>
    <?php
    $redirect = $this->input->get("redirect");
    if($redirect){
      ?>
      <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
      <?php
    }
    ?>
    <input type="submit" id="login-btn" class="btn btn-block btn-inverse" value="Login" />
  </form>
  <!-- END LOGIN FORM --> 
  <!-- BEGIN FORGOT PASSWORD FORM -->
  <form id="forgotform" class="form-vertical no-padding no-margin hide" action="<?php echo base_url('login/forget') ?>" method="POST">
    <p class="center">Enter your username & e-mail address below to reset your password.</p>
    <div class="control-group">
      <div class="controls">
        <div class="input-prepend"> <span class="add-on"><i class="icon-user"></i></span>
          <input id="input-username" type="text" placeholder="Username" name="user_name"/>
        </div>
        <!-- <div class="input-prepend"> <span class="add-on"><i class="icon-envelope"></i></span>
          <input id="input-email" type="text" placeholder="Email" name="email"/>
        </div> -->
      </div>
      <div class="space10"></div>
    </div>
    <input type="button" id="forget-btn" class="btn btn-block btn-inverse" value="Submit" />
  </form>
  <!-- END FORGOT PASSWORD FORM --> 
</div>
<!-- END LOGIN --> 
<!-- BEGIN COPYRIGHT -->
<div id="login-copyright">
  <div id="footer"> 2015 &copy; AMNIL Technologies Pvt. Ltd.</div>
</div>
<!-- END COPYRIGHT --> 
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
  <!-- BEGIN CORE PLUGINS -->
  <script src="<?php echo base_url(); ?>assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
  <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->  
  <script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>    
  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
  <script src="assets/plugins/excanvas.js"></script>
  <script src="assets/plugins/respond.js"></script> 
  <![endif]-->  
  <script src="<?php echo base_url(); ?>assets/plugins/breakpoints/breakpoints.js" type="text/javascript"></script>  
  <script src="<?php echo base_url(); ?>assets/plugins/jquery.blockui.js" type="text/javascript"></script> 
  <script src="<?php echo base_url(); ?>assets/plugins/jquery.cookie.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>  
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="<?php echo base_url(); ?>assets/scripts/app.js"></script>
  <script src="<?php echo base_url(); ?>assets/scripts/login.js"></script> 
  <!-- END PAGE LEVEL SCRIPTS --> 
  <script>
    jQuery(document).ready(function() {     
      // initiate layout and plugins
      App.init();
      Login.init();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
</html>