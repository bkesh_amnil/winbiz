<div id="body">
  <div class="container-fluid"> 
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"> <?php echo $title ?> </h3>
        <ul class="breadcrumb">
          <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span> </li>
          <li><a href="#"><?php echo $title ?></a></li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <?php
      $this->load->view("show_errors");
    ?>
    <div id="page" class="dashboard">
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="widget">
            <div class="widget-title">
              <h4><i class="icon-reorder"></i><?php echo $title ?></h4>
            </div>
            <div class="widget-body">
              
              <table border="0" class="table table-striped table-bordered table-hover">
                <tr>
                  <th height="28" scope="row">Enterprise Name : </th>
                  <td><?php echo $enterprise_name ?></td>
                </tr>
                <tr>
                  <th width="159" height="24" scope="row">Product : </th>
                  <td width="221"><?php echo $product_name ?></td>
                </tr>
                <tr>
                  <th height="25" scope="row">Customer Name : </th>
                  <td><?php echo $customer_name ?></td>
                </tr>
                <tr>
                  <th height="28" scope="row">Email : </th>
                  <td><?php echo $email ?></td>
                </tr>
                <tr>
                  <th height="28" scope="row">Contact Number :</th>
                  <td><?php echo $contact_number ?></td>
                </tr>
                <tr>
                  <th height="28" scope="row">Message : </th>
                  <td><?php echo $message ?></td>
                </tr>
                <tr>
                  <th height="28" scope="row">Enquiry Date  : </th>
                  <td><?php echo $enquiry_date ?></td>
                </tr>
                
              </table>
            </div>
            <!-- END EXAMPLE TABLE PORTLET--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
