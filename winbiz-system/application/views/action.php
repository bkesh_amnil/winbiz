<?php if($edit || $delete || $dynamic) {  ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/cms/jquery.contextMenu.js" type="text/javascript"></script>
<ul id="myMenu" class="contextMenu">
        <?php if($page_name == 'dynamic_form') { ?>
        <li class="struct"><a href="#structure">Structure</a></li>
        <li class="data"><a href="#data">Data</a></li>
        <?php } ?>
	<?php if($edit) { ?>
    <?php  $controller = explode('/', $page_name);
	if($controller[0] != 'form_controller' && $controller[0] != 'other_users' && $controller[0] != 'admin_user' && $controller[0] != 'enquiry_log' && $controller[0] != 'product_review') { ?>
	<li class="publish"><a href="#publish">Publish</a></li>
	<li class="unpublish"><a href="#unpublish">Unpublish</a></li>
    <?php } ?>
	<li class="edit"><a href="#edit">Edit</a></li>
    <?php } if($delete) { ?>
    <?php  $controller = explode('/', $page_name);
	if($controller[0] != 'admin_user' && $controller[0] != 'enquiry_log' && $controller[0] != 'product_review') { ?>
	<li class="delete separator"><a href="#delete">Delete</a></li>
    <?php } ?>
    <?php } ?>
</ul>

<script type="text/javascript">
	$(document).ready(function(){
		function filterAction(action, el){
			$('.rowCheckBox').each(function(){
				if($(this).val() == el.attr('rel')){
					$(this).attr('checked',true);
					$('.'+action+'Icon').click();
					return false;
				}else{
					$(this).attr('checked',false);
				}
			})			
		}
		
		$(".rightOperation").contextMenu({
				menu: "myMenu"
			}, function(action, el) {
				filterAction(action, el);		
		});
	});

</script>

<?php if($controller[0] != 'form_controller') { ?>
<script type="text/javascript">
	$(document).ready(function(e) {
		jQuery('.selectAll').change(function () {
            var set = ".rowCheckBox";
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });
        /*$(".selectAll").click(function(){
			if($(this).is(":checked")){
				$(".rowCheckBox").attr("checked", true);
			}else{
				$(".rowCheckBox").attr("checked", false);
			}
		})
		*/
		$(".addIcon").click(function(){
			var siteId = parseInt($("#sites").val());
			if(isNaN(siteId)){
				siteId = 0;	
			}
			var url = "<?php echo site_url("$page_name/form/"); ?>?site="+siteId;
			window.location = url;
			return false;
		});
		
		$(".structureIcon").click(function(){
            var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked > 0){
				if(checked > 1){
					var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
				}else{
					var excessCount = true;	
				}
                if(excessCount){
                    var url = "<?php echo site_url("form_fields/index/") . "/" ?>" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
                    window.location = url;
                }
            }else{
                alert("Please Select Some Items First");	
            }
                return false;
        });
        $(".credentialIcon").click(function(){
            var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked > 0){
				if(checked > 1){
					var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
				}else{
					var excessCount = true;	
				}
                if(excessCount){
                	if($(".rowCheckBox:checked:first").parents("tr").attr("superadmin") == "True"){
                		alert("Credintials not available for super admin.");
                		return false;
                	}else{
                		var prof_name = $(".rowCheckBox:checked").parents("tr").find("td.profilename").text();
                		var hidden_prof_name = $("#hidden_profile_name").val();
                		if(prof_name == hidden_prof_name) {
                			alert("You can't manage your own credentials");
                			return false;
                		}
	                    var url = $(this).attr("href") + "/" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
	                    window.location = url;
	                }
                }
            }else{
                alert("Please Select Some Items First");	
            }
                return false;
        });
                
                $(".dataIcon").click(function(){
                var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked > 0){
				if(checked > 1){
					var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
				}else{
					var excessCount = true;	
				}
                                if(excessCount){
                                    var url = "<?php echo site_url("form_controller/") . "/" ?>" + $(".rowCheckBox:checked:first").attr('data') + "<?php echo get_url() ?>";
                                    window.location = url;
                                }
                        }else{
                            alert("Please Select Some Items First");	
                        }
                        return false;
                });
                
		$(".editIcon").click(function(){
			var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked > 0){
				if(checked > 1){
					var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
				}else{
					var excessCount = true;	
				}
				if(excessCount){
					var url = "<?php echo site_url("$page_name/form/") . "/" ?>" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
					window.location = url;
				}
			}else{
				alert("Please Select Some Items To Edit");	
			}
			return false;
		});
		
		$(".publishIcon").click(function(){
			var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked == 1 )
			{
				var url = "<?php echo site_url("$page_name/change_status/1") . "/" ?>" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
				window.location = url;
			}
			else if(checked > 1){
				$('#gridForm').attr('method', 'post');
				$("#gridForm").attr("action", "<?php echo site_url("$page_name/change_status/1") . get_url() ?>");
				$("#gridForm").submit();
			}
			else
			{
				alert("Please Select Some Items To Publish");	
			}
			return false;
		});
		
		$(".unpublishIcon").click(function(){
			var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked == 1 )
			{
				var url = "<?php echo site_url("$page_name/change_status/0") . "/" ?>" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
				window.location = url;
			}
			else if(checked > 1)
			{
				$('#gridForm').attr('method', 'post');
				$("#gridForm").attr("action", "<?php echo site_url("$page_name/change_status/0") . get_url() ?>");
				$("#gridForm").submit();
			}else{
				alert("Please Select Some Items To Unpublish");	
			}
			return false;
		});
		
		$(".deleteIcon").click(function(){
			var checked = parseInt($(".rowCheckBox:checked").length);
			if(checked == 1 )
			{
				if(confirm("Are you sure to delete data?"))
				{
					var url = "<?php echo site_url("$page_name/delete") . "/" ?>" + $(".rowCheckBox:checked:first").val() + "<?php echo get_url() ?>";
					window.location = url;
				}
			}
			else if(checked > 1){
				if(confirm("Are you sure to delete data?"))
				{
					$('#gridForm').attr('method', 'post');
					$("#gridForm").attr("action", "<?php echo site_url("$page_name/delete") . get_url()  ?>");
					$("#gridForm").submit();
				}
			}else{
				alert("Please Select Some Items To Delete");	
			}
			return false;
		});	
    });
</script>
<?php } ?>

<?php } ?>