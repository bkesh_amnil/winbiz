<?php

class Admin_product_category_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $product_category = 'product_categories';

    function display_name($user_id) {
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (count($row) == 1) {
            if ($row->first_name != '') {
                return $row->first_name . ' ' . $row->last_name;
            } else {
                return $row->user_name;
            }
        } else {
            return '';
        }
    }

    function get_user_detail($user_id) {
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $data = $this->db->get($this->admin_user)->row();
        if (count($data) == 0) {
            redirect('login/logout');
        }
        return $data;
    }

    function get_product_category($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $row = $this->get_user_detail(current_admin_id());  //user of sub site will have access to their users only.
        if (!$row->main_site) {
            $this->db->where('site_id', $row->site_id);
        }

        if ($this->input->get('search')) {
            $this->db->like('product_category_name', $this->input->get('search'));
        }
        if ($this->input->get('site')) {
            $this->db->like($this->site . '.id', $this->input->get('site'));
        }

        $this->db->select($this->product_category . '.*, ' . $this->site . '.site_title');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->product_category . '.id', $order);

        $this->db->join($this->site, $this->product_category . '.site_id = ' . $this->site . '.id');

        if ($id == 0) {
            $result = $this->db->get($this->product_category)->result();
        } else {
            $this->db->where($this->product_category . ".id", $id);
            $result = $this->db->get($this->product_category)->row();
        }
        return $result;
    }

    function get_productcategory_id_or_name($prod_cat_name = '', $prod_cat_id = NULL) {
        if ($prod_cat_id != 0 || !empty($prod_cat_name)) {
            if ($prod_cat_id != 0) {
                $this->db->select('product_category_name as res');
                $this->db->where('id', $prod_cat_id);
            }

            if ($prod_cat_name != '') {
                $this->db->select('id as res');
                $this->db->where('product_category_name', $prod_cat_name);
            }

            $row = $this->db->get($this->product_category)->row();

            if (isset($row) && !empty($row)) {
                return $row->res;
            }
        }

        return '';
    }

}

?>