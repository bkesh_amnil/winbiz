<?php
	class Admin_menu_model extends CI_Model {
		var $site = 'site';
		var $menu = 'menu';
		var $site_id  = 0;
		var $countRow = 1;
		
		function get_menu($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc', $parent = 0, $padding = 0)
		{
			$row = $this->admin_user_model->get_user_detail(current_admin_id());	//user of sub site will have access to their users only.
			
			if( ! $row->main_site)
			{
				$this->db->where('site_menus.site_id', $row->site_id);
			}
			
			if($this->input->get('search'))
			{
				$this->db->like('menu_title', $this->input->get('search'));
			}
			
			if($this->input->get('menutype'))
			{
				$this->db->where($this->menu . '.menu_type_id', $this->input->get('menutype'));
			}
			
			if($this->site_id)
			{
				$this->db->where($this->site . '.id', $this->site_id);
			}
			
			if($this->input->get('site'))
			{
				$this->db->where($this->site . '.id', $this->input->get('site'));
			}
			
			$this->db->select($this->menu . '.*, '.$this->site.'.id as site_id, '.$this->site.'.site_title');
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
				//$this->db->limit($per_page, $uri);
			}
			
			$this->db->order_by('site_title', $order);
			$this->db->order_by($order_by, $order);
			
			$this->db->join('site_menus', $this->menu . '.menu_type_id = site_menus.id');
			$this->db->join($this->site, 'site_menus.site_id = ' . $this->site . '.id');
			if($id == 0 ) 
			{
				$this->db->where("menu_parent", $parent);
				$result = $this->db->get($this->menu)->result();
				$finList = array();
				if(!empty($result) && is_array($result)){
					foreach($result as $ind=>$resultval){
						$menuListGen = "";
						if(!empty($resultval)){
							foreach($resultval as $resultind=>$val){
								$menuListGen[$resultind] = $val;	
							}
							$menuListGen["cellpadding"] = $padding;
							$menuListGen["childList"] = $this->get_menu($id, $paginate, $uri, $order_by, $order, $resultval->id, ($padding + 10));
							array_push($finList, $menuListGen);
						}
					}
				}
			}else{
				$this->db->where($this->menu . ".id", $id);
				$result = $this->db->get($this->menu)->row();
				$finList = $result;
			}
			
			return $finList;	
		}
		
		function generateHtml($list, $page_name = "")
		{
		 	if(!empty($list) && is_array($list))
			{
				$html = "";
				foreach($list as $ind=>$val)
				{
					$html .= '<tr>
                      <td>'.$this->countRow.'</td>
                      <td>
                        <input type="checkbox" name="selected[]" value="'.$val['id'].'" class="rowCheckBox" />
                      </td>';
					  $arrows = $val['cellpadding'] /10;
					  $arrs = '';
					  for($i = 0; $i < $arrows; $i++){
							$arrs .= "- ";  
					  }
					  
					  $html .= '<td class="rightOperation" rel="'.$val['id'].'" >'.$arrs.$val['menu_title'].'</td>
                      <td>';
					   	$serLink = site_url('menu?site='.$val['site_id']);
						if($this->input->get('search')){
							$serLink = $serLink."&search=".$this->input->get('search');	
						}
					  $html .= '<a href="'.$serLink.'">'.$val['site_title'].'</a>
                       </td>
                      <td>'.$this->admin_user_model->display_name($val['created_by']).'</td>
                      <td>'.$val['created_date'].'</td>
                      <td>'.$this->admin_user_model->display_name($val['created_by']).'</td>
                      <td>'.$val['updated_date'].'</td>
                      <td>';
					  	if($val['status'] == "1"){ 
                        	$html .= '<a href="'.site_url("".$page_name."/change_status/0/".$val['id'] . get_url()).'" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>';
						}else{
                        	$html .= '<a href="'.site_url("".$page_name."/change_status/1/".$val['id']. get_url()).'" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>';
						}
					  $html .= '</td>
                      <td width="88">&nbsp;';
					  if(!empty($val['childList']))
					  {
					 	 $html .= '<a class="sortFaceBox" onlcick="return false;" href="' . site_url('menu/sort_menu/' . $val['id']) . '" rel="sortFacebox" title="Sort" style="cursor:pointer;">Sort</a>';
					  }
					  $html .= '</td>
                    </tr>';
					$this->countRow++;
					if(!empty($val['childList']) && is_array($val['childList'])){
						$html .= $this->generateHtml($val['childList'], $page_name);
					}
				}
				return $html;
			}
			return false;
		}
		
		function get_menu_page_settings($id = 0)
		{
			if($id == 0){
				return false;	
			}
			$this->db->where('menu_id', $id);
			$this->db->order_by('position', 'asc');
			$result = $this->db->get('menu_page_settings')->result();
			if($result)
			{
				return $result;
			}
			return false;
		}
		
		function getMenuSettings($id = 0)
		{
			if($id == 0){
				return false;	
			}
			$this->db->where('menu_id', $id);
			$this->db->order_by('position', 'asc');
			$result = $this->db->get('menu_page_settings')->result();
			if(!empty($result) && is_array($result)){
				return $result;
			}
			return false;
		}
		
		function get_menu_for_sort($parent_id, $menu_type_id)
		{
			$this->db->where('id', $parent_id);
			$this->db->select('menu_type_id');
			$result = $this->db->get('menu')->row();
			if($result || $menu_type_id)
			{
				$this->db->select('id, menu_title');
				
				if($menu_type_id)
				{
					$this->db->where('menu_type_id', $parent_id);	
					$this->db->where('menu_parent', 0);
				}
				else
				{
					$this->db->where('menu_type_id', $result->menu_type_id);	
					$this->db->where('menu_parent', $parent_id);
				}				
				$this->db->order_by('menu_position');
				$result = $this->db->get('menu')->result();
			}
			return $result;
		}
		
		function get_menu_types_dropdown($site_id)
		{
			$this->db->where("site_id", $site_id);
			$this->db->order_by('name');
			$result = $this->db->get('site_menus')->result();
			
			$tmp['']  = 'Select';
			foreach($result as $row) 
			{
				$tmp[$row->id] =  $row->name;
			}
			return $tmp;
		}
		
		function getMenuByType($typeId, $parent=0)
		{
			$this->db->where("menu_type_id", $typeId);
			$this->db->where("menu_parent", $parent);
			$this->db->where("status", "1");
			$this->db->order_by('menu_position');
			$result = $this->db->get('menu')->result();
			$tmp = array();
			foreach($result as $row) 
			{
				$list = array("id"=>$row->id, "name"=>$row->menu_title, "childs"=>$this->getMenuByType($typeId, $row->id));
				array_push($tmp, $list);
			}
			return $tmp;
		}
		function generateMenuList($arr)
		{
			
		}
		function get_enabled_module($site_id)
		{
			$row = $this->db->select('modules_enabled')->where('id', $site_id)->get('site')->row();
			if($row)
				$modules = $row->modules_enabled;
			else 
				$modules = '';
				
			$modules =  explode(',', $modules);

			$this->db->where_in('id', $modules);
			$this->db->where('public_module', 'yes');
			$result = $this->db->get('module')->result();
			
			$tmp['']  = 'Select Module';
			foreach($result as $row) 
			{
				$tmp[$row->id] =  $row->module_title;
			}
			return $tmp;
		}
	}
?>
