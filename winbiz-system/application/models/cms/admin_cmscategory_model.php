<?php
	class Admin_cmscategory_model extends CI_Model {
		var $admin_user = 'admin_user';
		var $profile = 'profile';
		var $profile_detail = 'profile_detail';
		var $module = 'module';
		var $site = 'site';
		var $category = 'categories';
		
        function display_name($user_id){
            $this->db->where('id', $user_id);
            $row = $this->db->get($this->admin_user)->row();
            if(count($row) == 1 ){
                if($row->first_name != ''){
                    return $row->first_name . ' ' . $row->last_name;
                }else{
                    return $row->user_name;
                }
            }else{
                return '';
            }			
		}	
		
		function get_user_detail($user_id){
            $this->db->where($this->admin_user . '.id', $user_id);
            $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
            $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
            $data = $this->db->get($this->admin_user)->row();
            if(count($data) == 0){
                redirect('login/logout');
            }
            return $data;
		}
                
		function get_category($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc'){
            $row = $this->get_user_detail(current_admin_id());	
            $showTree = true;
            if($this->input->get('search')){
                $this->db->where('category_name', $this->input->get('search'));
                $showTree = false;
            }else{
                $this->db->where('parent_category', 0);
            }
            $this->db->order_by($order_by, $order);
            if($id == 0 ) {
                if($showTree){
                    $this->db->where('parent_category', 0);
            }
                $result = $this->db->get($this->category)->result();	
            } else {
                $this->db->where($this->category . ".id", $id);
                $result = $this->db->get($this->category)->row();
                return $result;
            }
            return $result;
        }
                
        function get_categories_list($parent_cat = 0, $status = "all"){
            $this->db->where('parent_category', $parent_cat);
            //$this->db->where('is_default', 'no');
            if($status != "all"){
                $this->db->where('status', $status);
            }
            $this->db->order_by('is_default', 'ASC');
            $this->db->order_by('category_name', 'ASC');
            $result = $this->db->get($this->category)->result();
            //printr($this->db->last_query(), true);
            $catList = false;	
            if(!empty($result)){
                foreach($result as $ind=>$val){
                    $dataArr = array();
                    foreach($val as $index=>$value){
                        $dataArr[$index] = $value;	
                    }
                    if(!empty($dataArr)){
                        $dataArr["childs"] = $this->get_categories_list($val->id, $status);	
                    }
                    $dataArr = (object) $dataArr;
                    $catList[] = $dataArr;
                }
            }
            return $catList;
        }
                
        function getListHtml($list, $start = 1, $level = 0, $childs = false){
            $i = $start; 
            $html = '';
            $space = '';
            for($lev = 0; $lev < $level; $lev++){
                    $space .= "&shy;-&shy;";	
            }
            if(!empty($list) && is_array($list)){
                foreach($list as $row){
                    if($childs == false){
                        $level = 0;	
                    }
                    $str = '<tr>
                          <td>'.$i.'</td>
                          <td><input type="checkbox" name="selected[]" value="'.$row->id.'" class="rowCheckBox" /></td>
                          <td class="rightOperation" rel="'.$row->id.'">'.$space." ".$row->category_name.'</td>
                          <td>';
                    if($row->status == "1"){ 
                            $str .= '<a href="'.site_url('category/change_status/0/'.$row->id) . get_url().'" title="Click To Unpublish"><img src="assets/img/icons/published.png" width="16" height="16" /></a>';
                    }else{
                            $str .= '<a href="'.site_url('category/change_status/1/'.$row->id) . get_url().'" title="Click To Publish"><img src="assets/img/icons/unpublished.png" width="16" height="16" /></a>';
                    }
                    $str .= '</td>
                    </tr>';
                    $html .= $str;
                    $i++; 
                    if(isset($row->childs) && !empty($row->childs)){
                            $html .= $this->getListHtml($row->childs, $i, ++$level, true);
                    }
                }
            }
            return $html;
        }
                
        function get_categoryedit($id){
            $this->db->where('status', '1');
            $this->db->where('id', $id);
            $result = $this->db->get($this->category)->row();
            return $result;
        }

        function get_all_categories() {
            $this->db->select('id, category_name');
            $this->db->where('status', '1');
            $result = $this->db->get($this->category)->result();

            if(isset($result) && !empty($result) && is_array($result)) {
                return $result;
            }

            return array();

        }
		
	}
?>