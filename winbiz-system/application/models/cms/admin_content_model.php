<?php
	class Admin_content_model extends CI_Model {
		var $admin_user = 'admin_user';
		var $profile = 'profile';
		var $profile_detail = 'profile_detail';
		var $module = 'module';
		var $site = 'site';
		var $content = 'content';
		
		function display_name($user_id)
		{
			$this->db->where('id', $user_id);
			$row = $this->db->get($this->admin_user)->row();
			if(count($row) == 1 )
			{
				if($row->first_name != '')
				{
					return $row->first_name . ' ' . $row->last_name;
				}
				else
				{
					return $row->user_name;
				}
			}
			else
			{
				return '';
			}			
		}	
		
		function get_user_detail($user_id)
		{
			$this->db->where($this->admin_user . '.id', $user_id);
			$this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
			$this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
			$data = $this->db->get($this->admin_user)->row();
			if(count($data) == 0)
			{
				redirect('login/logout');
			}
			return $data;
		}
		
		function get_content($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
		{
			$row = $this->get_user_detail(current_admin_id());	//user of sub site will have access to their users only.
			if( ! $row->main_site)
			{
				$this->db->where('site_id', $row->site_id);
			}
			
			if($this->input->get('search'))
			{
				$this->db->like('title', $this->input->get('search'));
			}
			if($this->input->get('site'))
			{
				$this->db->select($this->content .'.*');
				$this->db->where($this->site . '.id', $this->input->get('site'));
			}
			
			$this->db->select($this->content . '.*, '.$this->site.'.id as site_id, '.$this->site.'.site_title');
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
				//$this->db->limit($per_page, $uri);
			}
			
			$this->db->order_by('site_title', $order);
			$this->db->order_by($order_by, $order);
			
			$this->db->join($this->site, $this->content . '.site_id = ' . $this->site . '.id');
			
			if($id == 0 ) {
				$result = $this->db->get($this->content)->result();	
			} 	else  {
				$this->db->where($this->content . ".id", $id);
				$result = $this->db->get($this->content)->row();
			}
			return $result;	
		}
		
	}
?>