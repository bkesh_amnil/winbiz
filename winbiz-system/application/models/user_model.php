<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin User_model Model
 * @package Model
 * @subpackage Model
 * Date created: Feb 8 2016()
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class User_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $users = 'users';

    public function __construct() {
        parent::__construct();
    }

    function get_user($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $this->db->select("*");
        $this->db->from('users_profile as up');
        $this->db->join('users as u', 'u.id = up.user_id');
        if ($this->input->get('search')) {
//            $this->db->like('up.first_name', $this->input->get('search'));
            $terms = explode(' ', $this->input->get('search'));
            if (isset($terms) && !empty($terms)) {
                if (!empty($terms[0])) {
                    $this->db->like('up.first_name', $terms[0]);
                }
                if (!empty($terms[1])) {
                    if (count($terms) == 2) {
                        $this->db->like('up.last_name', $terms[1]);
                    } else {
                        $this->db->like('up.middle_name', $terms[1]);
                    }
                }
                if (!empty($terms[2])) {
                    $this->db->like('up.last_name', $terms[2]);
                }
            }
        }
        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        if ($id == 0) {
            $result = $this->db->get()->result();
        } else {
            $this->db->where("u.id", $id);
            $result = $this->db->get()->row();
        }
//        printQuery();
        return $result;
    }

    public function get_user_detail($uid) {
        $this->db->select("*");
        $this->db->from('users_profile as up');
        $this->db->join('users as u', 'u.id = up.user_id');
        $this->db->where('u.id', $uid);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->row();
        }
    }

    function get_user_name($user_id) {
        $row = $this->get_user_detail($user_id);
        if (FALSE == $row) {
            return '-';
        } else {
            return $row->first_name . ' ' . $row->last_name;
        }
    }

    public function check_username($username) {
        return $this->db->get_where('users', array('username' => $username))->num_rows();
    }

    public function check_email($email) {
        return $this->db->get_where('users', array('email' => $email))->num_rows();
    }

    public function check_edited_username($username, $id) {
        $a = $this->db->get_where('users', array('id' => $id))->row()->username;
        if ($a == $username):
            return 0;
        else:
            return $this->db->get_where('users', array('username' => $username))->num_rows();
        endif;
    }

    public function check_edited_email($email, $id) {
        $a = $this->db->get_where('users', array('id' => $id))->row()->email;
        if ($a == $email):
            return 0;
        else:
            return $this->db->get_where('users', array('email' => $email))->num_rows();
        endif;
    }

    public function check_oldpassword($oldpassword) {
        $id = $this->session->userdata['admin_user_profile']['user_id'];
        $this->db->select('salt');
        $this->db->where('id', $id);
        $result = $this->db->get('admins');
        $salt = $result->row()->salt;
        $enc_password = make_hash($oldpassword, $salt);

        $query = $this->db->get_where('admins', array('password' => $enc_password));

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function get_users_select_options() {
        $this->db->select("*");
        $this->db->from('users_profile as up');
        $this->db->join('users as u', 'u.id = up.user_id');
        $this->db->where('u.status', '1');
        $query = $this->db->get();
        $record[' '] = 'Select Customer.';
        if ($query->num_rows() != 0) {
            $list = $query->result();
            foreach ($list as $k => $v) {
                $record[$v->user_id] = $v->first_name . '  ' . $v->middle_name . ' ' . $v->last_name;
            }
        }
        return $record;
    }

}

/* End of file User_model.php
 * Location: ./application/modules/admin/models/User_model.php */