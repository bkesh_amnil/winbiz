<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Module_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';

    function getModulesByParentId($id) {
        $module_data = array();

        $module_query = $this->db->query("SELECT * FROM tbl_module WHERE parent_module = '" . (int) $id . "'");

        foreach ($module_query->result() as $module) {
            $module_data[] = array(
                'id' => $module->id,
                'module_title' => $module->module_title,
                'module_name' => $module->module_name,
                'parent_module' => $module->parent_module
            );

            $children = $this->getModulesByParentId($module->id);

            if ($children) {
                $module_data = array_merge($children, $module_data);
            }
        }

        return $module_data;
    }

    function getModuleName($id = NULL) {
        if ($id) {
            $result = $this->getModulesByParentId($id);
            if (!empty($result)) {
                foreach ($result as $res) {
                    $array1[] = $res['id'];
                }
                $imploded_id = implode(",", $array1);
                $query1 = 'SELECT `id`, `module_title` from `tbl_module` where `id` NOT IN (' . $imploded_id . ') AND `id`<>' . $id . '';
                return $this->db->query($query1)->result();
            } else {
                $query = 'SELECT `id`, `module_title` from `tbl_module` where `id`<> ' . $id . '';
                return $this->db->query($query)->result();
            }
        } else {
            $query = 'SELECT 
              child.id,
              child.module_title 
            FROM
              (`tbl_module` child) 
              LEFT JOIN `tbl_module` parent 
              ON `parent`.`id` = `child`.`parent_module`
              ORDER BY `id` ';
        }

        return $this->db->query($query)->result();
    }

    function getParentModule($search = NULL) {
        if (!$search) {
            return $this->db->query('SELECT 
                                    child.id,
                                    child.module_title,
                                    child.module_name ChildName,
                                    parent.module_title ParentName
                                  FROM
                                    (`tbl_module` child) 
                                    LEFT JOIN `tbl_module` parent 
                                    ON `parent`.`id` = `child`.`parent_module` 
                                    ORDER BY `id` ', FALSE)
                            ->result();
        } else {
            return $this->db->query('SELECT 
                                    child.id,
                                    child.module_title,
                                    child.module_name ChildName,
                                    parent.module_title ParentName
                                  FROM
                                    (`tbl_module` child) 
                                    LEFT JOIN `tbl_module` parent 
                                    ON `parent`.`id` = `child`.`parent_module`
                                    WHERE
                                    (child.module_title like "%' . $search . '%") 
                                    ORDER BY `id` ', FALSE)
                            ->result();
        }
    }

    function getAllParents() {
        $parent = $this->db->select('id,module_title')
                ->where('parent_module', 0)
                ->get('module')
                ->result();
        return $parent;
    }

    function get_module_for_sort($parent_id, $menu_type_id) {
        $this->db->where('id', $parent_id);
        $result = $this->db->get($this->module)->row();
        if ($result || $menu_type_id) {
            $this->db->select('id, module_title');
            $this->db->where('parent_module', $parent_id);
            $this->db->order_by('module_position');
            $result = $this->db->get($this->module)->result();
        }
        return $result;
    }

}

/* End of file module_model.php */
/* Location: ./application/models/module_model.php */