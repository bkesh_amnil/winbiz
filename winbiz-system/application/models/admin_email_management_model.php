<?php
	class Admin_email_management_model extends CI_Model {
		var $admin_user = 'admin_user';
		var $profile = 'profile';
		var $profile_detail = 'profile_detail';
		var $module = 'module';
		var $site = 'site';
		var $email_management = 'email_management';
		var $enterprise = 'enterprises';

		function display_name($user_id){
	      $this->db->where('id', $user_id);
	      $row = $this->db->get($this->admin_user)->row();
	      if(count($row) == 1 ){
	          if($row->first_name != ''){
	              return $row->first_name . ' ' . $row->last_name;
	          }else{
	              return $row->user_name;
	          }
	      }else{
	          return '';
	      }			
		}	
			
		function get_user_detail($user_id){
	      $this->db->where($this->admin_user . '.id', $user_id);
	      $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
	      $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
	      $data = $this->db->get($this->admin_user)->row();
	      if(count($data) == 0){
	          redirect('login/logout');
	      }
	      return $data;
		}

		function get_email_managements($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc', $admin_enterprise_id = 0){
			$row = $this->get_user_detail(current_admin_id());
			if( ! $row->main_site)
			{
				$this->db->where($this->email_management . '.site_id', $row->site_id);
			}
			
			if($this->input->get('search'))
			{
				//$this->db->like('product_name', $this->input->get('search'));
			}
			if($this->input->get('site'))
			{
				$this->db->like($this->site . '.id', $this->input->get('site'));
			}
			
			$this->db->select($this->email_management . '.*, '. $this->enterprise.'.enterprise_name, ' .$this->site.'.site_title');
			
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
			}
			
			$this->db->order_by($this->email_management.'.id', $order);

			$this->db->join($this->site, $this->email_management . '.site_id = ' . $this->site . '.id');

			$this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->email_management . '.enterprise_id');
			
			if($id == 0) {
				/*if($admin_enterprise_id != '0') {
					$this->db->where('enterprise_id', $admin_enterprise_id);
				}*/
				if($row->enterprise_related == '1') {
					$this->db->where($this->email_management.'.enterprise_id', $row->enterprise_id);
				}
				$result = $this->db->get($this->email_management)->result();
			} 	else  {
				$this->db->where($this->email_management . ".id", $id);
				$result = $this->db->get($this->email_management)->row();

			}

			return $result;
		}

	}
?>