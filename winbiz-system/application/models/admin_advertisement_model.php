<?php

class Admin_advertisement_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $advertisement = 'advertisement';
    var $advertisement_session = 'advertisement_session';
    var $advertisement_provider = 'advertisement_provider';
    var $advertisement_price = 'advertisement_price';
    var $advertisement_position = 'advertisement_position';

    function display_name($user_id) {
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (count($row) == 1) {
            if ($row->first_name != '') {
                return $row->first_name . ' ' . $row->last_name;
            } else {
                return $row->user_name;
            }
        } else {
            return '';
        }
    }

    function get_user_detail($user_id) {
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $data = $this->db->get($this->admin_user)->row();
        if (count($data) == 0) {
            redirect('login/logout');
        }
        return $data;
    }

    function get_advertisements($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.
        if ($row->enterprise_id != 0) {
            //wont run for the super admin BTW
            $ent_id = $row->enterprise_id;
            $created_bys = $this->db->select('id')->get_where($this->admin_user, array('enterprise_id' => $ent_id))->result();
            $created_by_ids = array();
            foreach ($created_bys as $cb) {
                $created_by_ids[] = $cb->id;
            }
            $this->db->where_in($this->advertisement . '.created_by', $created_by_ids);
        }

        $this->db->join($this->advertisement_session, $this->advertisement . '.id = ' . $this->advertisement_session . '.ad_id');
        $this->db->join($this->advertisement_provider, $this->advertisement . '.provider_id = ' . $this->advertisement_provider . '.id');
        $this->db->group_by($this->advertisement_session . '.ad_id');


        if ($this->input->get('search')) {
            $this->db->like('advertisement_name', $this->input->get('search'));
        }
        if ($this->input->get('site')) {
            $this->db->where($this->advertisement_session . '.site_id', $this->input->get('site'));
        }
        if ($this->input->get('provider')) {
            $this->db->group_by($this->advertisement_provider . '.name');
//            $this->db->join($this->advertisement_provider, $this->advertisement_provider . '.id = ' . $this->advertisement_session . '.provider_id');
            $this->db->like($this->advertisement_provider . '.name', $this->input->get('provider'));
        }
        if ($this->input->get('start') && $this->input->get('end')) {
            $this->db->where($this->advertisement_session . '.end_date >=', db_format($this->input->get('start')));
            $this->db->where($this->advertisement_session . '.end_date <=', db_format($this->input->get('end')));
        }

        $this->db->select($this->advertisement . '.*, ' . $this->advertisement_provider . '.name ,' . $this->advertisement_provider . '.id as  provider_id,' . $this->advertisement_session . '.position_id');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->advertisement . '.id', $order);


//        $this->db->join($this->advertisement_position, $this->advertisement_position . '.id = ' . $this->advertisement . '.advertisement_position_id');

        if ($id == 0) {
            $result = $this->db->get($this->advertisement)->result();
        } else {
            $this->db->where($this->advertisement . ".id", $id);
            $result = $this->db->get($this->advertisement)->row();
        }
//        printQuery();
//        dumparray($result);
        return $result;
    }

    function list_advertisement_providers() {
        $result = $this->db->get($this->advertisement_provider)->result();
        return $result;
    }

    function get_advertisement_providers($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.

        if ($this->input->get('search')) {
            $this->db->like('name', $this->input->get('search'));
        }
        $this->db->select('*');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        if ($id == 0) {
            $result = $this->db->get($this->advertisement_provider)->result();
        } else {
            $this->db->where($this->advertisement_provider . ".id", $id);
            $result = $this->db->get($this->advertisement_provider)->row();
        }
        return $result;
    }

    function get_advertisement_session($ad_id, $id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {

        $this->db->select($this->advertisement_session . '.*, ' . $this->advertisement_provider . '.name, ' . $this->advertisement_position . '.advertisement_position_name, ' . $this->site . '.site_title ');
//        $this->db->select('*');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->advertisement_session . '.id', $order);

        $this->db->join($this->advertisement_provider, $this->advertisement_session . '.provider_id = ' . $this->advertisement_provider . '.id');

        $this->db->join($this->advertisement_position, $this->advertisement_session . '.position_id = ' . $this->advertisement_position . '.id');
        $this->db->join($this->site, $this->advertisement_session . '.site_id = ' . $this->site . '.id');

        $this->db->where($this->advertisement_session . '.ad_id', $ad_id);
        if ($id == 0) {
            $result = $this->db->get($this->advertisement_session)->result();
        } else {
            $this->db->where($this->advertisement_session . ".id", $id);
            $result = $this->db->get($this->advertisement_session)->row();
        }
//        printQuery();
//        dumparray($result);
        return $result;
    }

    function get_advertisement_price($site_id) {
        $this->db->select('*');
        $this->db->where('site_id', $site_id);
        $result = $this->db->get($this->advertisement_price)->result();
        if (isset($result) && !empty($result) && is_array($result)) {
            $array = array();
            foreach ($result as $res) {
                $array[$res->ad_position_id] = $res->price;
            }
            return $array;
        }
        return array();
    }

    function get_advertisement_position() {
        $this->db->select('id, advertisement_position_name,position,advertisement_position_value, advertisement_size');
        $this->db->where('status', '1');
        $result = $this->db->get($this->advertisement_position)->result();

        if (isset($result) && !empty($result) && is_array($result)) {
            return $result;
        }
        return array();
    }

    /* function getLinkType($id){
      $data =  $this->db->select('LinkType')
      ->where('id',$id)
      ->get('Advertisement')
      ->row();
      return $data->LinkType;
      }

      function getAdvertisements($siteId){
      $this->db->where('site_id', $siteId);
      $row = $this->db->get('site')->row();
      if($row->main_site != 1){
      $this->db->where('site_id', $siteId);
      }
      $this->db->where("status", $ActiveStatus);
      $result = $this->db->get('Advertisement')->result();
      $tmp = array();
      foreach($result as $row)
      {
      $list = array("id"=>$row->id, "site_id"=>$row->site_id, "file"=>$row->FileName);
      array_push($tmp, $list);
      }
      return $tmp;
      } */

    function calculate_ad_price($site_id = 3, $position = 10, $start_date = '', $end_date = '') {
//        $date = new DateTime('2016-07-14');
//        $date->add(new DateInterval('P2D'));
//        echo $date->format('Y-m-d') . "<br>";
//        exit;
        $price = $this->db->get_where('advertisement_price', array('site_id' => $site_id, 'ad_position_id' => $position));
        if ($price->num_rows() == 0) {
            $price = 0;
        } else {
            $price = $price->row()->price;
        }
        $start = new DateTime($start_date);
        $end = new DateTime($end_date);
        $diff = $start->diff($end);
        $days = $diff->days;
        $total_price = $price * $days;
        $price = number_format($total_price, 2);
        $array = array('price' => $price, 'days' => $days);
        return $array;
    }

}

?>