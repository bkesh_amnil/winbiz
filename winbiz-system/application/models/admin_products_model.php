<?php

class Admin_products_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $products = 'products';
    var $product_count = 'product_count';
    var $product_images = 'product_images';
    var $enterprise_category = 'enterprise_categories';
    var $enterprise = 'enterprises';
    var $product_category = 'product_categories';
    var $product_review = 'product_review';
    var $product_rate = 'product_rate';

    function display_name($user_id) {
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (count($row) == 1) {
            if ($row->first_name != '') {
                return $row->first_name . ' ' . $row->last_name;
            } else {
                return $row->user_name;
            }
        } else {
            return '';
        }
    }

    function get_user_detail($user_id) {
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $data = $this->db->get($this->admin_user)->row();
        if (count($data) == 0) {
            redirect('login/logout');
        }
        return $data;
    }

    function get_products($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'desc', $admin_enterprise_id = 0) {
        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.

        if (!$row->main_site) {
            $this->db->where($this->products . '.site_id', $row->site_id);
        }

        if ($this->input->get('search')) {
            $this->db->like('product_name', $this->input->get('search'));
        }
        if ($this->input->get('site')) {
            $this->db->like($this->site . '.id', $this->input->get('site'));
        }

        $this->db->select($this->products . '.*, ' . $this->enterprise . '.enterprise_name	, ' . $this->product_category . '.product_category_name, ' . $this->site . '.site_title,' . $this->product_count . '.view_count');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->products . '.id', $order);

        $this->db->join($this->site, $this->products . '.site_id = ' . $this->site . '.id');

        $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->products . '.enterprise_id');

        $this->db->join($this->product_category, $this->product_category . '.id = ' . $this->products . '.product_category_id');

        $this->db->join($this->product_count, $this->products . '.id = ' . $this->product_count . '.product_id');

        if ($id == 0) {
            /* if($admin_enterprise_id != '0') {
              $this->db->where('enterprise_id', $admin_enterprise_id);
              } */
            if ($row->enterprise_related == '1') {
                $this->db->where($this->products . '.enterprise_id', $row->enterprise_id);
            }
            $result = $this->db->get($this->products)->result();
        } else {
            $this->db->where($this->products . ".id", $id);
            $result = $this->db->get($this->products)->row();
        }

        return $result;
    }

    function get_enterprise_category($id) {
        $this->db->select($this->enterprise . '.enterprise_category_id');
        $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->products . '.enterprise_id');
        $this->db->where($this->products . '.id', $id);
        $row = $this->db->get($this->products)->row();

        if (isset($row) && !empty($row)) {
            return $row->enterprise_category_id;
        }

        return '';
    }

    function get_available_product_images($id) {
        $this->db->where('products_id', $id);
        $result = $this->db->get($this->product_images)->result();

        if (isset($result) && !empty($result)) {
            return $result;
        }

        return array();
    }

    function get_product_review($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'desc') {
        $row = $this->get_user_detail(current_admin_id());

        if ($this->input->get('site')) {
            if ($this->input->get('site') != '1') {
                $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->product_review . '.enterprise_id');
                $this->db->like($this->site . '.id', $this->input->get('site'));
            }
        }

        $this->db->select($this->product_review . '.*, ' . $this->products . '.product_name, ' . $this->enterprise . '.enterprise_name, ' . $this->product_rate . '.rate');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->product_review . '.id', 'DESC');

        $this->db->join($this->products, $this->product_review . '.product_id = ' . $this->products . '.id');

        $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->product_review . '.enterprise_id');

        $this->db->join($this->product_rate, $this->product_rate . '.ip_address = ' . $this->product_review . '.ip_address AND tbl_product_rate.enterprise_id = tbl_product_review.enterprise_id  AND tbl_product_rate.product_id = tbl_product_review.product_id AND tbl_product_rate.email = tbl_product_review.email', 'LEFT');
        if ($id == 0) {
            if ($row->enterprise_related == '1') {
                $this->db->where($this->product_review . '.enterprise_id ', $row->enterprise_id);
            }
            $result = $this->db->get($this->product_review)->result();
        } else {
            $this->db->where($this->product_review . ".id", $id);
            $result = $this->db->get($this->product_review)->row();
        }
        return $result;
    }

}

?>