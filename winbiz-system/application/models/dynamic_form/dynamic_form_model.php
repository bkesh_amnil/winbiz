<?php

class Dynamic_form_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	// --------------------------------------------------------------------

      /** 
       * function SaveForm()
       *
       * insert form data
       * @param $form_data - array
       * @return Bool - TRUE or FALSE
       */

	function SaveForm($form_data, $id)
	{
		if($id)
		{
			$form_data['updated_by']	= current_admin_id(); 
			$form_data['updated_date']	= get_now();
			$this->db->where('id', $id);
			$this->db->update('form', $form_data);
		}
		else
		{
			$form_data['created_by']	= current_admin_id(); 
			$form_data['created_date']	= get_now();
			$this->db->insert('form', $form_data);
		}
		
		if ($this->db->affected_rows() == '1')
		{
			$id = $this->db->insert_id();
			$data['form_unique_name'] = $form_data['form_name'] . '-' . $id;
			$this->db->where('id', $id);
			$this->db->update('form', $data);
			return TRUE;
		}
		
		return FALSE;
	}
	
	function get_forms($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
	{
		$row = $this->admin_user_model->get_user_detail(current_admin_id());			
		if( ! $row->main_site)
		{
			$this->db->where('site_id', $row->id);
		}
		$table_name = 'form';
		if($paginate) 
		{
			$per_page = $this->custom_pagination->per_page();
			//$this->db->limit($per_page, $uri);
		}
		
		$this->db->order_by($order_by, $order);
		
		if($this->input->get('search'))
		{
			$this->db->like('form_title', $this->input->get('search'));
		}
		if($this->input->get('site'))
		{
			$this->db->like('site_id', $this->input->get('site'));
		}
		if($id == 0 ) {
			$result = $this->db->get($table_name)->result();	
		} 	else  {
			$this->db->where("{$table_name}.id", $id);
			$result = $this->db->get($table_name)->row();
		}
		//echo $this->db->last_query();
		return $result;	
	}
	function get_available_forms($id = false, $site_id = false){
		$this->db->select("id, form_name");
		if($id){
			$this->db->where("id !=", $id);
		}
		if($site_id){
			$this->db->where("site_id", $site_id);	
		}
		$data = $this->db->get("form")->result();
		if(!empty($data)){
			return $data;	
		}else{
			return false;	
		}
	}
}
?>