<?php

class Form_model extends CI_Model {
	
	private $all_group_name =array();
	
	function form_fields($form_id, $show_in_grid = 0, $group_id = -1)
	{
		if($show_in_grid)
			$this->db->where('show_in_grid', 1);
		if($group_id != -1)
			$this->db->where('group_id', $group_id);
		
		$this->db->order_by('field_order', 'ASC');
		return $this->db->where('form_id', $form_id)->get('form_field')->result();
	}
	
	function get_group_parent($form_id, $group_id = 0)
	{
		$this->db->where('form_id', $form_id)->order_by('id','asc');
		//$this->db->where('group_parent', '0');
		if($group_id){
			//$this->db->order_by('id','desc');
			$this->db->where('id',$group_id);
			$row = $this->db->get('form_group')->row();
			return $row;
		} else {
			$result = $this->db->get('form_group')->result();
			return $result;
		}
	}
	
	function form_fields_group($form_id,$show_in_grid = 0)
	{		
		//printr($result);
		foreach($result as $row)
		{
			$group_id  = $row->id;
			$this->db->where('group_id',$group_id);
			$this->db->where('form_id',$form_id);
			$this->db->get('form_field')->result();	
		}
	}
	
	function get_submitted_data($form_submission_id, $form_field_id)
	{
		$this->db->where('form_submission_id', $form_submission_id);
		$this->db->where('form_field_id', $form_field_id);
		$row = $this->db->get('form_submission_fields')->row();
		if($row)
		{	
			return $row->form_field_value;
		} 
		else
		{ 
			return '';
		}
	}
	
	function get_search_submitted_data($form_submission_id, $form_field_id, $search)
	{
		$this->db->where('form_submission_id', $form_submission_id);
		$this->db->where('form_field_id', $form_field_id);
		$this->db->like('form_field_value', $search);
		$row = $this->db->get('form_submission_fields')->row();

		if($row)
		{	
			return true;
		} 
		else
		{ 
			return false;
		}
	}
	
	function public_form_fields($form_id, $front_display = 0)
	{
		if($front_display)
			$this->db->where('front_display', 1);
		$this->db->order_by('field_order', 'ASC');
		return $this->db->where('form_id', $form_id)->get('form_field')->result();
	}
	
	function get_group_types($form_id,$parent_group="0")
	{
		$this->db->where("group_parent","$parent_group");
		$this->db->where("form_id","$form_id");
		$this->db->order_by('position','ASC');
		return $this->db->get('form_group')->result();
		
		
	}
	function get_group_name($group_id, $first = 0)
	{		
		if($first) {
			$this->all_group_name = array();
		}
		$this->db->where('id',$group_id);
		$row = $this->db->get('form_group')->row();
		$this->all_group_name[] = $row;
		
		if($row)
		{
			if($row->group_parent != 0)
			{
				$this->get_group_name($row->group_parent);
			}
		}
		
		return $this->all_group_name;
		
	}
	function get_group($parent_id)
	{
		$this->db->where('group_parent',$parent_id);
		return $this->db->get('form_group')->result();
		
	}
	function getGrpupOptions($form_id, $group_parent = 0, $level = 0){
		$formOptions = false;
		if(empty($form_id)){
			return false;	
		}
		$this->db->where('form_id',$form_id);
		$this->db->where('group_parent',$group_parent);
		$this->db->order_by("position", "asc");
		$result  =  $this->db->get('form_group')->result();
		//echo $this->db->last_query()."<br />";
		$space = '';
		if($level > 0){
			for($i = 0; $i < $level; $i++){
				$space .= "&nbsp;&nbsp;&nbsp";	
			}
		}				//printr($result);die();
		if($result){
			foreach($result as $ind=>$val){
				$formOptions .= '<option value="'.$val->id.'">'.$space."-".$val->group_name.'</option>';
				$formOptions .= $this->getGrpupOptions($form_id, $val->id, ( $level + 1 ));	
			}
		}
		return $formOptions;
	}
	function getGroupOptions_edit($form_id, $group_parent = 0, $level = 0){
		$formOptions = false;
		if(empty($form_id)){
			return false;	
		}
		$this->db->where('form_id',$form_id);
		$this->db->where('group_parent',$group_parent);
		$this->db->order_by("position", "asc");
		$result  =  $this->db->get('form_group')->result();
		//echo $this->db->last_query()."<br />";
		$space = '';
		if($level > 0){
			for($i = 0; $i < $level; $i++){
				$space .= "&nbsp;&nbsp;&nbsp";	
			}
		}
		if($result){
			foreach($result as $ind=>$val){
				$formOptions .= '<option value="'.$val->id.'">'.$space."-".$val->group_name.'</option>';
				$formOptions .= $this->getGrpupOptions($form_id, $val->id, ( $level + 1 ));	
			}
		}
		return $formOptions;
	}
}