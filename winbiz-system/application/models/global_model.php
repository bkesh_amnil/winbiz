<?php
class Global_model extends CI_Model {
	
	function insert_check_duplicate($table_name, $field_value)
	{
		$field = array_keys($field_value);
		$value = array_values($field_value);
		
		$this->db->where($field[0], $value[0]);
		if($this->db->count_all_results($table_name) > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function update_check_duplicate($table_name, $field_value, $id)
	{
		$field = array_keys($field_value);
		$value = array_values($field_value);
		
		$this->db->where($field[0], $value[0]);
		$this->db->where('id != ', $id);
		if($this->db->count_all_results($table_name) > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_single_data($table_name, $select_field, $value, $field = 'id')
	{
		$this->db->where($field, $value);
		$row = $this->db->get($table_name)->row();
		
		if(count($row) > 0)
		{
			return $row->$select_field;	
		}
		else
		{
			return '';
		}
	}
	function get_single_data_ad($table_name, $select_field, $value, $field = 'id')
	{
		$this->db->where($field, $value);
		$row = $this->db->get($table_name)->row();
		
		if(count($row) > 0)
		{
			return $row->$select_field;	
		}
		else
		{
			return '';
		}
	}

	function get_single_data_prod($table_name, $select_field, $value, $field = 'product_id')
	{
		$this->db->where($field, $value);
		$row = $this->db->get($table_name)->row();
		
		if(count($row) > 0)
		{
			return $row->$select_field;	
		}
		else
		{
			return '';
		}
	}
	
	function setting($setting_name)
	{
		$this->db->where('setting_name', $setting_name);
		$row = $this->db->get('setting')->row();
		
		if($row)
		{
			return $row->setting_value;
		}
		else
		{
			return '';
		}
	}
	
}
?>