<?php
	class Admin_domain_theme_model extends CI_Model {
		var $domain_theme = 'domain_theme';

		function get_domain_themes($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc'){
			if($this->input->get('search'))
			{
				$this->db->like('domain_theme_name', $this->input->get('search'));
			}
			
			$this->db->select($this->domain_theme . '.*');
			
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
				//$this->db->limit($per_page, $uri);
			}
			
			$this->db->order_by($this->domain_theme.'.id', $order);
			
			if($id == 0 ) {
				$result = $this->db->get($this->domain_theme)->result();	
			} 	else  {
				$this->db->where($this->domain_theme . ".id", $id);
				$result = $this->db->get($this->domain_theme)->row();

			}
			return $result;
		}

		function get_all_domain_themes() {
			$this->db->select('id, domain_theme_name');
			$this->db->where('status', 1);
			$result = $this->db->get($this->domain_theme)->result();

			return (isset($result) && !empty($result)) ? $result : array();
		}

	}
?>