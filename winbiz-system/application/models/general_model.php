<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin General_model Model
 * @package Model
 * @subpackage Model
 * Date created: July 19 2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class General_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update($table, $data, $where) {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function getWhere($table, $where, $result = FALSE, $order = '', $limit = '', $start = 0) {
        $ci = & get_instance();
        $ci->db->where($where);
        if ($order != '') {
            $ci->db->order_by($order);
        }
        if ($limit != '') {
            $ci->db->limit($limit, $start);
        }
        $query = $ci->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            if ($result == TRUE) {
                return $query->result();
            } else {
                return $query->row();
            }
        }
    }

    function getAll($table, $where = NULL, $orderBy = NULL, $select = NULL, $limit = NULL, $group_by = NULL) {
        if ($select)
            $this->db->select($select);
        if ($where)
            $this->db->where($where);
        if ($orderBy)
            $this->db->order_by($orderBy);
        if ($limit)
            $this->db->group_by($limit);
        if ($group_by)
            $this->db->group_by($group_by);
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function getAll_Array($table, $where = NULL, $orderBy = NULL, $select = NULL, $limit = NULL, $group_by = NULL) {
        if ($select)
            $this->db->select($select);
        if ($where)
            $this->db->where($where);
        if ($orderBy)
            $this->db->order_by($orderBy);
        if ($limit)
            $this->db->group_by($limit);
        if ($group_by)
            $this->db->group_by($group_by);
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    function getById($table, $fieldId, $id, $array = FALSE, $select = '*') {
        $this->db->select($select);
        $this->db->where($fieldId . " = '" . $id . "'");
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            if ($array == TRUE) {
                return $query->row_array();
            } else {
                return $query->row();
            }
        }
    }

    function countTotal($table, $where = NULL) {
        if ($where) {
            $this->db->where($where);
        }
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    /**
     * upload the  multiple products image at once.
     * @param type $folder
     * @param type $encode
     * @return type
     */
    public function uploadProductFiles($folder, $encode = TRUE) {
        $files = $_FILES;
        $image_name = array();
        if (!empty($files['userfile']['name'][0])) {
            $this->load->library('image_moo');
            $this->load->library('upload', $this->set_upload_options($folder));
            $count = count($_FILES['userfile']['name']);
            for ($i = 0; $i < $count; $i++) {
                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                $this->upload->initialize($this->set_upload_options($folder));
                if ($this->upload->do_upload() == false) {
                    $error = array('error' => $this->upload->display_errors());
//                print_r($error);exit;
                }
                $imginfo = $this->upload->data();
                $imgname = $imginfo['raw_name'] . $imginfo['file_ext'];
                $image_name[] = ($imgname);

                //resample image
                $this->image_moo->load('uploads/products/' . $folder . '/' . $imgname);
                $this->image_moo->resize_crop(config_item('product_image_width'), config_item('product_image_height'));
                $thumb = 'uploads/products/' . $folder . '/thumbs/' . $imgname;
                $this->image_moo->save($thumb, $overwrite = TRUE);
            }
        }
        if ($encode == TRUE) {
            return (json_encode($image_name));
        } else {
            return $image_name;
        }
    }

    private function set_upload_options($folder) {
        $file = time() . '_' . random_string('alnum', 2);
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/products/' . $folder;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['file_name'] = $file;
        $config['overwrite'] = TRUE;
        return $config;
    }

    function upload_file($folder, $w = '', $h = '', $fileName = 'userfile') {
//        echo $fileName;exit;
        $file = time();
        $config['upload_path'] = config_item('uploads_path') . $folder;
        $config['allowed_types'] = "gif|jpg|jpeg|png";
        $config['max_size'] = "0";
        $config['max_width'] = "0";
        $config['max_height'] = "0";
        $config['file_name'] = $file;
        $this->load->library('upload', $config);

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($fileName)) {
            $error = array('error' => $this->upload->display_errors());
//            dumparray($error);
//            dumparray($config['upload_path']);
            $thumb = '';
        } else {
            $finfo = $this->upload->data();
            $thumb = $finfo['raw_name'] . $finfo['file_ext'];

            //resample image
            $ww = $w == '' ? config_item('product_image_width') : $w;
            $hh = $h == '' ? config_item('product_image_height') : $h;
            $this->load->library('image_moo');
            $this->image_moo->load('uploads/' . $folder . '/' . $thumb);
            $this->image_moo->resize_crop($ww, $hh);
            $thumbimg = 'uploads/' . $folder . '/thumbs/' . $thumb;
            $this->image_moo->save($thumbimg, $overwrite = TRUE);
        }
        return $thumb;
    }

    /**
     * core function to upload the image to the server (currently being used in carts decoration method to upload logo).
     * @param type $temp_name
     * @param type $name
     * @param type $folder
     * @return string
     */
    function simply_upload_file($temp_name, $name, $folder) {
//        echo 'here 1';exit;
//        echo $name;exit;
        if ($name != '') {
            $file = time();
            $tempFile = $temp_name;
            $targetPath = config_item('uploads_path') . $folder;
            $new_name = $file . '_' . $name;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_name;

            // Validate the file type
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'flv', 'xml', 'mp3', 'mp4'); // File extensions
            $fileParts = pathinfo($name);

            if (in_array($fileParts['extension'], $fileTypes)) {
                move_uploaded_file($tempFile, $targetFile);
//            echo $new_name;
                return $new_name;
            } else {
//            echo 'no_name';
                return '';
            }
        } else {
            return '';
        }
    }

    function del_img($table, $where, $folder, $feild = 'image') {
        $this->db->where($where);
        $query = $this->db->get($table)->row();
        $img = $query->$feild;
        if ($img != '') {
            $path = $this->config->item('uploads_path') . $folder . '/' . $img;
            $path_th = $this->config->item('uploads_path') . $folder . '/thumbs/' . $img;
            if (file_exists($path)) {
                unlink($path);
                unlink($path_th);
            }
        }
        return true;
    }

    function del_img_json($table, $where, $folder, $feild = 'image') {
        $this->db->where($where);
        $query = $this->db->get($table)->row();
        $img = $query->$feild;
        if ($img != '') {
            $allimg = json_decode($img);
            if (!empty($allimg)) {
                foreach ($allimg as $imgg) {
                    $path = $this->config->item('product_uploads_path') . $folder . '/' . $imgg;
                    $path_th = $this->config->item('product_uploads_path') . $folder . '/thumbs/' . $imgg;
                    if (file_exists($path)) {
                        unlink($path);
                        unlink($path_th);
                    }
                }
            }
        }
        return TRUE;
    }

    function unlink_img($folder, $name) {
        if ($name != '') {
            $path = config_item('uploads_path') . $folder . '/' . $name;
            if (file_exists($path)) {
                unlink($path);
            }
        }
    }

    /**
     * checks user table for the valid email
     * @return boolean
     */
    function checkEmailExists($email) {
        $prev_email = $this->db->get_where('users', array('email' => $email));
        $num = $prev_email->num_rows();
        if (($num) >= 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /*
     * pdf generate through mpdf library
     */

    public function generate_pdf($html, $company, $title, $paper = 'A4') {
//        define('_MPDF_PATH','../');
        $this->load->library('mpdf60/mpdf');
//        $mpdf = new mPDF('c', $paper, '', '', 20, 15, 48, 25, 10, 10);
        $mpdf = new mPDF('c', $paper, '', '', 5, 5, 48, 25, 10, 10);
        // LOAD a stylesheet
        $stylesheet = file_get_contents(base_url('assets/admin_css/pdf.css'));
        $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and nobody/html/text

        $mpdf->SetTitle("$company->company_name. - " . $title);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output('CompanyInvoice.pdf', 'I');
    }

    public function getRecommededProducts() {
        $query = $this->db->select('*')
                ->from('products')
                ->where('status', '1')
                ->where('type', 1)
                ->order_by('id', 'random')
                ->limit(5)
                ->get();
        return $query->result();
    }

}

/* End of file General_model.php
 * Location: ./application/modules/admin/models/General_model.php */