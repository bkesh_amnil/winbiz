<?php

class Admin_enterprise_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $enterprise_count = 'enterprise_count';
    var $enterprise = 'enterprises';
    var $enterprise_category_list = 'enterprise_category_list';
    var $enterprise_category = 'enterprise_categories';

    function display_name($user_id) {
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (count($row) == 1) {
            if ($row->first_name != '') {
                return $row->first_name . ' ' . $row->last_name;
            } else {
                return $row->user_name;
            }
        } else {
            return '';
        }
    }

    function get_user_detail($user_id) {
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $data = $this->db->get($this->admin_user)->row();
        if (count($data) == 0) {
            redirect('login/logout');
        }
        return $data;
    }

    function get_enterprises($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $row = $this->get_user_detail(current_admin_id());  //user of sub site will have access to their users only.
        if (!$row->main_site) {
            $this->db->where($this->enterprise . '.site_id', $row->site_id);
        }

        if ($this->input->get('search')) {
            $this->db->like('enterprise_name', $this->input->get('search'));
        }
        if ($this->input->get('site')) {
            $this->db->like($this->site . '.id', $this->input->get('site'));
        }

        $this->db->select($this->enterprise . '.*, ' . $this->site . '.site_title,' . $this->enterprise_count . '.count');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->enterprise . '.id', $order);

        $this->db->join($this->site, $this->enterprise . '.site_id = ' . $this->site . '.id');

        $this->db->join($this->enterprise_category_list, $this->enterprise . '.id = ' . $this->enterprise_category_list . '.enterprise_id');

        $this->db->join($this->enterprise_category, $this->enterprise_category . '.id = ' . $this->enterprise_category_list . '.enterprise_category_id');

        $this->db->join($this->enterprise_count, $this->enterprise_count . '.enterprise_id = ' . $this->enterprise . '.id');
        $this->db->group_by($this->enterprise . '.id');
        if ($id == 0) {
            if ($row->enterprise_related == '1') {
                $this->db->where($this->enterprise . '.id', $row->enterprise_id);
            }
            $result['enterprise'] = $this->db->get($this->enterprise)->result();
            $query = "SELECT ec.enterprise_category_name, ec.id AS enterprise_category_id, ecl.enterprise_id FROM tbl_enterprise_categories ec JOIN tbl_enterprise_category_list ecl ON ec.id = ecl.enterprise_category_id ";
            $result['category'] = $this->db->query($query)->result();
        } else {
            $this->db->where($this->enterprise . ".id", $id);
            $result = $this->db->get($this->enterprise)->row();
//            dumparray($result);
            /* $query = "SELECT ec.enterprise_category_name, ec.id AS enterprise_category_id, ecl.enterprise_id FROM tbl_enterprise_categories ec JOIN tbl_enterprise_category_list ecl ON ec.id = ecl.enterprise_category_id WHERE ecl.enterprise_id = $id";
              $result['category']=$this->db->query($query); */
        }

        return $result;
    }

    function get_all_enterprises($id) {
        $this->db->select($this->enterprise . '.id, enterprise_name');
        if (!empty($id)) {
            $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id=' . $this->enterprise . '.id');
            $this->db->where($this->enterprise_category_list . '.enterprise_category_id', $id);
        }
        $this->db->where($this->enterprise . '.status', 1);
        $result = $this->db->get($this->enterprise)->result();

        if (isset($result) && !empty($result) && is_array($result)) {
            return $result;
        }

        return array();
    }

    function get_enterprise_id_or_name($ent_name = '', $ent_id = NULL) {
        if ($ent_id != 0 || !empty($ent_name)) {
            if ($ent_id != 0) {
                $this->db->select('enterprise_name as res');
                $this->db->where('id', $ent_id);
            }

            if ($ent_name != '') {
                $this->db->select('id as res');
                $this->db->where('enterprise_name', $ent_name);
            }

            $row = $this->db->get($this->enterprise)->row();

            if (isset($row) && !empty($row)) {
                return $row->res;
            }
        }

        return '';
    }

    /* function get_enterprise_id_by_name($ent_name) {
      $this->db->select('id');
      $this->db->where('enterprise_name', $ent_name);
      $row = $this->db->get($this->enterprise)->row();

      if(isset($row) && !empty($row)) {
      return $row->id;
      }

      return '';
      }

      function get_enterprise_name_by_id($ent_id) {
      $this->db->select('enterprise_name')
      } */
}

?>