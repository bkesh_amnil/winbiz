<?php

class Admin_user_model extends CI_Model {

    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';

    function display_name($user_id) {
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (count($row) == 1) {
            if ($row->first_name != '') {
                return $row->first_name . ' ' . $row->last_name;
            } else {
                return $row->user_name;
            }
        } else {
            return '';
        }
    }

    function username_password() {
        $user_id = current_admin_id();
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();
        if (md5($row->user_name) == $row->password)
            return TRUE;
        else
            return FALSE;
    }

    function get_user_detail($user_id) {
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        //$this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->admin_user . '.enterprise_id');
        $data = $this->db->get($this->admin_user)->row();
        if (count($data) == 0) {
            redirect('login/logout');
        }

        return $data;
    }

    function get_users($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {

        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.
        if (!$row->main_site) {
            $this->db->where('site_id', $row->site_id);
        }
        if ($this->input->get('site')) {
            $this->db->where('site_id', $this->input->get('site'));
        }
        if ($this->input->get('search')) {
            $this->db->like('user_name', $this->input->get('search'));
        }

        $this->db->select($this->admin_user . '.*, profile_name, site_title');
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by('site_title', $order);
        $this->db->order_by('profile_name', $order);
        $this->db->order_by($order_by, $order);
        if ($id == 0) {
            $this->db->where($this->profile . '.id', 1);
            $result = $this->db->get($this->admin_user)->result();
        } else {
            $this->db->where($this->profile . '.id', 1);
            $this->db->where($this->admin_user . ".id", $id);
            $result = $this->db->get($this->admin_user)->row();
        }

        return $result;
    }

    function get_other_users($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {

        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.

        /* if( ! $row->main_site)
          {
          $this->db->where('site_id', $row->site_id);
          } */
        if ($this->input->get('site')) {
            $this->db->where('site_id', $this->input->get('site'));
        }
        if ($this->input->get('search')) {
            $this->db->like('user_name', $this->input->get('search'), 'both');
        }

        $this->db->select($this->admin_user . '.*, profile_name, site_title');
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');

        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by('site_title', $order);
        $this->db->order_by('profile_name', $order);
        $this->db->order_by($order_by, $order);
        $this->db->where_not_in($this->profile . '.id', 1);
        if ($id == 0) {
            if ($row->enterprise_related == '1') {
                $this->db->where($this->admin_user . '.enterprise_id', $row->enterprise_id);
                //$this->db->where($this->site . '.enterprise_id', $row->enterprise_id);
            }
            $result = $this->db->get($this->admin_user)->result();
        } else {
            $this->db->where($this->admin_user . ".id", $id);
            $result = $this->db->get($this->admin_user)->row();
        }

        return $result;
    }

    function get_profiles($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc') {
        $row = $this->get_user_detail(current_admin_id()); //user of sub site will have access to their users only.

        if (!$row->main_site) {
            $this->db->where('site_id', $row->id);
        }
        if ($this->input->get('site')) {
            $this->db->where('site_id', $this->input->get('site'));
        }

        if ($this->input->get('search')) {
            $this->db->like('profile_name', $this->input->get('search'));
        }

        $this->db->select($this->profile . '.*, site_title');
        if ($paginate) {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by('site_title', $order);
        $this->db->order_by($order_by, $order);

        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $this->db->where_not_in($this->profile . ".id", 1);
        if ($id == 0) {
            if ($row->enterprise_related == '1') {
                $this->db->where_not_in($this->profile . ".id", 2);
            }
            $result = $this->db->get($this->profile)->result();
        } else {
            $this->db->where($this->profile . ".id", $id);
            $result = $this->db->get($this->profile)->row();
        }

        return $result;
    }

    //module settings
    function get_credentials($module_name) {
        $credentials['view'] = $credentials['add'] = $credentials['edit'] = $credentials['delete'] = 0;

        $row = $this->get_user_detail(current_admin_id());
        $profile_id = $row->profile_id;

        $this->db->where('module_name', $module_name);
        $module = $this->db->get($this->module)->row();

        if (count($module) == 0) {
            return $credentials;
        }

        if ($row->super_admin == 'True' && ($row->main_site || $module->global_module)) {
            $credentials['view'] = $credentials['add'] = $credentials['edit'] = $credentials['delete'] = 1;
        } else {
            if ($this->input->get('site') && $row->site_id != $this->input->get('site')) {
                $credentials['view'] = $credentials['add'] = $credentials['edit'] = $credentials['delete'] = 0;
            } else {
                $module_id = $module->id;
                $this->db->where('profile_id', $profile_id);
                $this->db->where('module_id', $module_id);
                $row = $this->db->get($this->profile_detail)->row();

                if (count($row) > 0) {
                    $credentials['view'] = $row->view_module;
                    $credentials['add'] = $row->add_module;
                    $credentials['edit'] = $row->edit_module;
                    $credentials['delete'] = $row->delete_module;
                }
            }
        }
        return $credentials;
    }

    function access_module($module_name, $check, $id = 0) {
        $data = $this->get_credentials($module_name);
        $authority = true;

        switch ($check) {
            case 'add/edit':
                if ((!$data['add'] && $id == 0) || (!$data['edit'] && $id != 0))
                    $authority = false;
                break;

            case 'delete':
                if (!$data['delete'])
                    $authority = false;
                break;

            case 'view':
            default:
                if (!$data['view'])
                    $authority = false;
        }

        if (!$authority) {
            $session['class'] = 'error';
            $session['msg'] = "You don't have authority to access this page.";
            $this->session->set_flashdata($session);
            redirect('dashboard');
        }
        return $data;
    }

    function get_page_settings($profile_id, $module_id) {
        $this->db->where('profile_id', $profile_id);
        $this->db->where('module_id', $module_id);
        $row = $this->db->get($this->profile_detail)->row();
        if (count($row) == 0) {
            $credentials['view'] = $credentials['add'] = $credentials['edit'] = $credentials['delete'] = $credentials['export'] = '';
        } else {
            $credentials['view'] = ($row->view_module == 1) ? 'checked' : '';
            $credentials['add'] = ($row->add_module == 1) ? 'checked' : '';
            $credentials['edit'] = ($row->edit_module == 1) ? 'checked' : '';
            $credentials['delete'] = ($row->delete_module == 1) ? 'checked' : '';
        }
        return $credentials;
    }

    function getdashboardmenu() {
        $this->db->select('id, name, controller');
        $this->db->where('status', 'yes');
        $this->db->where('show_dashboard', 1);
        $this->db->order_by('parent_id', 'ASC');
        $result = $this->db->get('cms_menu')->result();
        return $result;
    }

    function get_user_enterprise_info($user_id) {
        $this->db->select('enterprise_id');
        $this->db->where('id', $user_id);
        $row = $this->db->get($this->admin_user)->row();

        if (isset($row) && !empty($row) && $row->enterprise_id != '0') {
            $this->db->select('enterprises.id as ent_id, enterprise_categories.id as ent_cat_id, enterprise_name');
            $this->db->join('enterprises', 'enterprises.id = admin_user.enterprise_id');
            $this->db->join('enterprise_categories', 'enterprise_categories.id = enterprises.enterprise_category_id');
            $this->db->where('enterprise_id', $row->enterprise_id);
            $res = $this->db->get($this->admin_user)->row();


            if (isset($res) && !empty($res)) {
                return $res;
            }
        }

        return '';
    }

}

?>