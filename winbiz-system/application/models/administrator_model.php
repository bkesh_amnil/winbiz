<?php
	class Administrator_model extends CI_Model {
		
		function get_data($table_name, $id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
		{ 
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
			}
			
			$this->db->order_by($order_by, $order);
			
			if($id == 0 ) 
			{
				$result = $this->db->get($table_name)->result();	
			} 	
			else  
			{
				$this->db->where("{$table_name}.id", $id);
				$result = $this->db->get($table_name)->row();
			}

			return $result;	
		}
		
		function get_modules($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
		{
			$row = $this->admin_user_model->get_user_detail(current_admin_id());			
			if( ! $row->main_site)
			{
				$this->db->where('global_module', '1');
			}
			if($this->input->get('search'))
			{
				$this->db->like('module_title', $this->input->get('search'));
			}
			
			if($paginate) 
			{
				$per_page = $this->custom_pagination->per_page();
				//$this->db->limit($per_page, $uri);
			}			
						
			$result = $this->get_data('module', $id, $paginate, $uri, $order_by, $order);
		
			return $result;
		}
		
		function get_sites($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
		{
			$row = $this->admin_user_model->get_user_detail(current_admin_id());			
			
			if( ! $row->main_site)
			{
				//$this->db->where('id', $row->site_id);
				$this->db->where('id', $row->id);
			}

			if(current_admin_id() != '1' && $row->profile_id != '1' && $row->profile_id != '2') {
				$this->db->where('enterprise_id', $row->enterprise_id);
			}


			$result = $this->db->get('site')->result();
			
			//$result = $this->get_data('site', $id, $paginate, $uri, $order_by, $order);
			return $result;
		}
		
		function get_menu_types($site_id = '')
		{
			$this->db->where('site_id', $site_id);
			$lists = $this->db->get('site_menus')->result();			
			return $lists;
		}
		
		function get_categories_dropdown()
		{
			$this->db->where('status', 1);
			$result = $this->db->get('categories')->result();
			$list["all"] = "All";
			if(!empty($result) && is_array($result)){
				foreach($result as $ind=>$val){
					$list[$val->category_alias] = $val->category_name;
				}
			}
			if(!empty($list) && is_array($list)){
				return $list;	
			}
			return array();
		}
		
		function getCategoryAlias($id){
			$this->db->where('id', $id);
			$result = $this->db->get('categories')->row();
			if(isset($result->category_alias) && !empty($result->category_alias)){
				return $result->category_alias;	
			}else{
				return false;	
			}
			
		}
		function get_module_contents($siteId, $moduleId, $position, $selected = '', $category = '', $selectAll = TRUE)
		{
			if(empty($siteId) || empty($moduleId))
			{
				return false;	
			}
			
			$selected = explode(',', $selected);
			
			$moduleName = $this->global_model->get_single_data('module', 'module_name', $moduleId, 'id');
			$fld = "title";
			$category_id = 0;
			if(!empty($category) && $category != "all"){
				$this->db->where('category_alias', $category);
				$result = $this->db->get('categories')->row();
				if(isset($result->id) && !empty($result->id )){
					$category_id = $result->id;	
				}
			}
                        
			switch($moduleName){
				case "menu":
					$tblName = "menu";
					$fld = "menu_title";
					break;
				case "content":
					$tblName = "content";
					break;
				case "gallery":
					$tblName = "gallery";
					break;
				case "banner":
					$tblName = "banner";
					break;
				case "news":
					$tblName = "news";
					break;
				case "advertisement":
					$tblName = "advertisement";
					break;
				case "dynamic_form":
					$tblName = "form";
					$fld = "form_title";
					break;
				case "material":
					$tblName = "material";
					break;
				case "form_fields":
					$tblName = "form";
					$fld = "form_title";
					break;
                                case "video":
                                        $tblName = "video";
                                        break;
                                case "newsletter":
                                        $tblName = "newsletter";
                                        break;
				default:
					$tblName = "";
			}
			
			if($tblName == ""){
				return false;	
			}
			
			$this->db->where('site_id', $siteId);
			if($tblName == "form")
			{					
				$this->db->where('form_status', 'Published');
			} 
			else if($tblName == "menu")
			{
				$this->db->select('menu.*');
				$this->db->where('menu_parent', '0');
				$this->db->join('site_menus', "site_menus.id = menu.menu_type_id");
				$this->db->where('menu.status', '1');
			}
			else
			{
				$this->db->where('status', '1');
			}
			
			if(!empty($category_id)){
				if($category == "uncategorized"){
					$this->db->where("(category_id = '".$category_id."' OR category_id = '')");
				}else{
					$this->db->where('category_id', $category_id);
				}
			}
			$data = $this->db->get($tblName)->result();
			$result = '';
			
			if(is_array($data) && !empty($data)){
				$result = '<ul class="contentListModules">';
				if(!empty($category) && $category != "all"){
					if($selectAll == TRUE){
						$allchecked = 'checked="checked"';
						$allselClass = 'class="selectedRow"';
					}else{
						$allchecked = 'checked="checked"';
						$allselClass = 'class="selectedRow"';
					}
					$result .= '<li '.$allselClass.' style="width:865px"><input type="checkbox" name="allSelectedModuleContent['.$position.']" value="true" class="selectAllWithinCategory selectAllModuleContent_'.$position.'"' . $allchecked .'  />All Within Category</li>';	
				}else{
					$selectAll = FALSE;	
				}
				foreach($data as $ind=>$val){
					if(in_array($val->id, $selected))
					{
						$checked = 'checked="checked"';
						$selClass = 'class="selectedRow"';
					}else{
						if(!empty($category_id)){
							$checked = 'checked="checked"';
							$selClass = 'class="selectedRow"';
						}else{
							$checked = '';
							$selClass = '';
						}
					}
					if($selectAll == TRUE){
						$hidden = ' style="display:none"';	
					}else{
						$hidden = '';	
					}
					$result .= '<li '.$selClass.' '.$hidden.' title="'. $val->id . '"><input type="checkbox" name="selectedModuleContent['.$position.'][]" value="'.$val->id.'" class="selectedModuleContent_'.$position.'"' . $checked .'  /> '.$val->$fld."</li>";
				}
				$result .= "</ul>";
			}
			if(empty($result)){
				$result = "No Content Available Within This Module";	
			}
			echo $result;
			//echo $siteId." === ".$moduleId." === ".$moduleName." === ".$tblName;
		}
		
		function get_menu($menuTypeId, $selecteds = "")
		{
			$this->load->model('cms/admin_menu_model', 'cms');
			$result = $this->cms->getMenuByType($menuTypeId);
			$html = '';
			if(!empty($result) && is_array($result)){
				$html = '<ul class="menuList">';
				$html .= $this->generateMenuList($result, $selecteds);
				$html .= '</ul>';
			}
			echo $html;
		}
		function get_menu_list($menuTypeId, $selecteds = "", $for="")
		{
			$this->load->model('cms/admin_menu_model', 'cms');
			$result = $this->cms->getMenuByType($menuTypeId);
			$html = '';
			if(!empty($result) && is_array($result)){
				$html = '<ul class="menuList">';
				$html .= $this->generateMenuList($result, $selecteds, $for);
				$html .= '</ul>';
			}
			return $html;
		}
		function generateMenuList($result, $selecteds = "", $for = "")
		{
			$html = '';
			if($this->input->post("for")){
                            $for = $this->input->post("for");
                        } 
			$ids = explode(',',$selecteds);
			foreach($result as $ind=>$val){
				if(in_array($val['id'], $ids)){
					$checked = 'checked="checked"';
					$selClass = ' selectedRow';
				}else{
					$checked = '';
					$selClass = '';
				}
				$childrens = count($val['childs']);
				if($childrens > 0){
					$childsText = '&nbsp;&nbsp;<b>('.$childrens.')</b>';	
				}else{
					$childsText = '';
				}
				$html .= '<li><div class="menuListRow'.$selClass.'"><input type="checkBox" name="menuList_'.$for.'[]" value="'.$val['id'].'" '.$checked.' /><span>'.$val['name'].$childsText.'</span></div>';
				if(!empty($val['childs']) && is_array($val['childs'])){
					$html .= '<ul>';
					$html .= $this->generateMenuList($val['childs'], $selecteds);
					$html .= '</ul>';
				}
				$html .= '</li>';
			}
			return $html;
		}
		function getAvailableMenus($menuType, $menu_parent='', $level = 0){
			if(empty($menuType)){
				$options = "";
			}else{
				$this->db->where("id", $menuType);
				$settingData = $this->db->get('site_menus')->row();
				if($settingData->submenus == "yes"){
					if($settingData->depth == 0){
						$depth = 999;	
					}else{
						$depth = $settingData->depth;	
					}
					$options = $this->getMenus($settingData->id, $depth);
				}else{
					$options = "";	
				}
			}
			$html = '<option value="">Parent</option>';
			if(!empty($options) && is_array($options)){
				$html .= $this->generateHtml($options, $menu_parent);
				
			}
			return $html;
		}
		//function to get menu list inorder to generate dropdown options
		function getMenus($typeId, $depth = 0, $parent = 0){
			if($depth == 0){
				return false;	
			}
			$this->db->where('menu_type_id', $typeId);
			$this->db->where('status', "1");
			$this->db->where('menu_parent', $parent);
			$menuData = $this->db->get('menu')->result();
			if(!empty($menuData) && is_array($menuData)){
				foreach($menuData as $ind=>$val){
					$data[] = array("id"=>$val->id, "name"=>$val->menu_title, "depth"=>$depth, "childs"=>$this->getMenus($typeId, ($depth - 1), $val->id));
				}
			}
			if(!empty($data) && is_array($data)){
				return $data;	
			}
			return false;
		}
		//function to generate options list for available menus
		function generateHtml($options, $menu_parent, $maxDepth = 0){
			$html = "";
			foreach($options as $ind=>$val){
				$depth = $val['depth'];	
				if($maxDepth == 0){
					$maxDepth = $depth;	
				}
				$actDepth = $maxDepth - $depth;
				$spaces = "";
				for($i = 0; $i < $actDepth; $i++){
					$spaces .= '&nbsp;';	
				}
				if($val['id'] == $menu_parent){
					$selected = 'selected="selected"';	
				}else{
					$selected = '';	
				}
				$html .= '<option value="'.$val['id'].'" '.$selected.'>'.$spaces.$val['name'].'</option>';
				if(!empty($val['childs']) && is_array($val['childs'])){
					$html .= $this->generateHtml($val['childs'], $menu_parent, $maxDepth);
				}
			}
			return $html;
		}
		
		function get_form_categories_dropdown()
		{
			$this->db->where('form_status', 'Published');
			$result = $this->db->get('form')->result();
			$list["all"] = "All";
			if(!empty($result) && is_array($result)){
				foreach($result as $ind=>$val){
					$list[$val->form_unique_name] = $val->form_name;
				}
			}
			if(!empty($list) && is_array($list)){
				return $list;	
			}
			return array();
		}
	}
	