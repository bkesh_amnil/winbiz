<?php
	class Login_model extends CI_Model {
		
		var $table_name = 'admin_user';
		
		function check_login($user_name, $password)
		{
			$user_id = 0;
			
			$this->db->where('user_name', $user_name);
			$this->db->where('password', $password);
			$this->db->where('status', 'Active');
			
			$query = $this->db->get($this->table_name);
			
			if ($query->num_rows() == 1)
			{
				$row = $query->row();
				$user_id = $row->id;
			}
			return $user_id;
		}		
	}
?>
