<?php
class Admin_enterprise_category_list_model extends CI_Model {
    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $module = 'module';
    var $site = 'site';
    var $enterprise_category = 'enterprise_categories';
    var $enterprise_category_list = 'enterprise_category_list';
    var $product_category = 'product_categories';

    function get_user_detail($user_id){
        $this->db->where($this->admin_user . '.id', $user_id);
        $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
        $this->db->join($this->site, $this->profile . '.site_id = ' . $this->site . '.id');
        $data = $this->db->get($this->admin_user)->row();
        if(count($data) == 0){
            redirect('login/logout');
        }
        return $data;
    }

    function get_enterprise_category($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc'){
        $row = $this->get_user_detail(current_admin_id());  //user of sub site will have access to their users only.
        if( ! $row->main_site)
        {
            $this->db->where('site_id', $row->site_id);
        }

        if($this->input->get('search'))
        {
            $this->db->like('enterprise_category_name', $this->input->get('search'));
        }
        if($this->input->get('site'))
        {
            $this->db->like($this->site . '.id', $this->input->get('site'));
        }

        $this->db->select($this->enterprise_category . '.*, '.$this->site.'.site_title');

        if($paginate)
        {
            $per_page = $this->custom_pagination->per_page();
            //$this->db->limit($per_page, $uri);
        }

        $this->db->order_by($this->enterprise_category.'.id', $order);

        $this->db->join($this->site, $this->enterprise_category . '.site_id = ' . $this->site . '.id');

        if($id == 0 ) {
            $result = $this->db->get($this->enterprise_category)->result();
        }   else  {
            $this->db->where($this->enterprise_category . ".id", $id);
            $result = $this->db->get($this->enterprise_category)->row();

        }
        return $result;
    }

    function get_all_enterprise_categories() {
        $this->db->select('id, enterprise_category_name');
        $this->db->where('status', 1);
        $this->db->order_by('enterprise_category_name');
        $result = $this->db->get($this->enterprise_category)->result();

        if(isset($result) && !empty($result) && is_array($result)) {
            return $result;
        }

        return array();
    }

    function get($id) {
        $this->db->select('enterprise_category_id');
        $this->db->where('enterprise_id', $id);
        $result = $this->db->get($this->enterprise_category_list)->result();

        if(isset($result) && !empty($result) && is_array($result)) {
            return $result;
        }

        return array();
    }

    function get_product_cateogry_names($id) {
        $this->db->select('product_category_name');
        $this->db->where('enterprise_category_id', $id);
        $result = $this->db->get($this->product_category)->result();

        if(isset($result) && !empty($result) && is_array($result)) {
            $array = array();
            foreach($result as $res) {
                array_push($array, $res->product_category_name);
            }

            return $array;
        }

        return array();
    }

    function get_all_enterprise_product_categories($ent_id) {
        $this->db->select('id, product_category_name, product_category_alias');
        $this->db->where('enterprise_category_id', $ent_id);
        $result = $this->db->get($this->product_category)->result();

        if(isset($result) && !empty($result) && is_array($result)) {
            return $result;
        }

        return array();
    }

}
?>