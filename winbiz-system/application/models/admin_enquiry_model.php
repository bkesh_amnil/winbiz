<?php
	class Admin_enquiry_model extends CI_Model {
        var $admin_user = 'admin_user';
        var $profile = 'profile';
        var $profile_detail = 'profile_detail';
        var $site = 'site';
		var $product_enquiry = 'product_enquiry';
        var $products = 'products';
        var $enterprises = 'enterprises';
		
		function get_user_detail($user_id) {
            $this->db->where($this->admin_user . '.id', $user_id);
            $this->db->join($this->profile, $this->admin_user . '.profile_id = ' . $this->profile . '.id');
            $data = $this->db->get($this->admin_user)->row();
            if(count($data) == 0)
            {
                redirect('login/logout');
            }
            return $data;
        }

        function get_enquiry($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc'){
            $row = $this->get_user_detail(current_admin_id());

            if($this->input->get('site'))
            {
                if($this->input->get('site') != '1') {
                    $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->product_enquiry . '.enterprise_id');
                    $this->db->like($this->site . '.id', $this->input->get('site'));
                }
            }

            $this->db->select($this->product_enquiry . '.*, '.$this->products.'.product_name, '.$this->enterprises.'.enterprise_name');
            
            if($paginate) 
            {
                $per_page = $this->custom_pagination->per_page();
                //$this->db->limit($per_page, $uri);
            }
            
            $this->db->order_by($this->product_enquiry.'.id', 'DESC');
            
            $this->db->join($this->products, $this->product_enquiry . '.product_id = ' . $this->products . '.id');

            $this->db->join($this->enterprises, $this->enterprises.'.id = ' . $this->product_enquiry.'.enterprise_id');
            
            if($id == 0 ) {
                if($row->enterprise_related == '1') {
                    $this->db->where($this->product_enquiry . '.enterprise_id ', $row->enterprise_id);
                }
                $result = $this->db->get($this->product_enquiry)->result();   
            }   else  {
                $this->db->where($this->product_enquiry . ".id", $id);
                $result = $this->db->get($this->product_enquiry)->row();

            }
            return $result;
        }
		
	}
?>