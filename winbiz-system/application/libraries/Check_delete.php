<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Check_delete  {
		
		function check($module, $id)
		{		
			$delete = true;
			
			$CI = &get_instance();
			
			$CI->db->where('module_name', $module);
			$row = $CI->db->get('module')->row();
			
			if($row)
				$module_id = $row->id;
			else
				$delete = false;
			
			$CI->db->where('module_id', $module_id);
			$CI->db->where('content_ids', "{$id}");
			if($CI->db->count_all_results('menu_page_settings'))
				$delete = false;
			
			$CI->db->where('module_id', $module_id);
			$CI->db->like('content_ids', ",{$id}", "before");
			if($CI->db->count_all_results('menu_page_settings'))
				$delete = false;			
			
			$CI->db->where('module_id', $module_id);
			$CI->db->like('content_ids', "{$id},", "after");
			if($CI->db->count_all_results('menu_page_settings'))
				$delete = false;
			
			$CI->db->where('module_id', $module_id);
			$CI->db->like('content_ids', ",{$id},");
			if($CI->db->count_all_results('menu_page_settings'))
				$delete = false;
			
			return $delete;
		}
	
	}