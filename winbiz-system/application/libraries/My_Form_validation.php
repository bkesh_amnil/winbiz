<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	//protected $CI;
 
	function __construct()
	{
		parent::__construct();
 
		$this->CI =& get_instance();

	}

	public function unique($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('unique',	'The %s is already being used.');

		list($table, $field, $cur_id) = explode(".", $params, 3);
	
		$cur_id = (int)$cur_id;
		if($cur_id){
			$CI->db->where('id != ', $cur_id);
		}
			
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();

		if ($query->row()) {
			return false;
		} else {
			return true;
		}
	}
        
        

	public function unique_ad($value, $params) {
		$CI =& get_instance();

		if($value != '1') {
			$CI->form_validation->set_message('unique_ad',	'The %s can have only one value.');

			list($table, $field, $cur_id) = explode(".", $params, 3);
		
			$cur_id = (int)$cur_id;
			if($cur_id){
				$CI->db->where('id != ', $cur_id);
			}
				
			$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();
                        printQuery();

			if ($query->row()) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	

	function unique_product($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('unique_product',	'The %s is already being used.');

		list($table, $field, $enterprise_id, $prod_id) = explode(".", $params, 4); 
	
		$enterprise_id = (int)$enterprise_id; 
		$prod_id = (int)$prod_id;
		if($prod_id != 0){
			$CI->db->where('enterprise_id ', $enterprise_id);
			$CI->db->where('id !=', $prod_id);
		}
		else{
			$CI->db->where('enterprise_id', $enterprise_id);
		}

		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();
		if ($query->row()) {
			return false;
		} else {
			return true;
		}
	}
	
	function existsId($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('exists',	'The %s does not exist.');

		list($table, $field) = explode(".", $params, 2);
	
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();
	
		if ( ! $query->row()) {
			return false;
		} else {
			return true;
		}
	}
	
	//calling:: fixed_values[Active,Inactive]
	function fixed_values($value, $valid_values = '')
	{
		$this->CI->form_validation->set_message('fixed_values',	'The %s is invalid.');
		
		if($valid_values == '') {		
			$array = array('Yes', 'No');
		} else {
			$array = explode(',', $valid_values);
		}
		
		return (in_array(trim($value), $array)) ? TRUE : FALSE;
	}
	
	/**
     * Validate URL
     *
     * @access    public
     * @param    string
     * @return    string
     */
    function valid_url($url)
    {
        $pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
        if (!preg_match($pattern, $url))
        {
            return FALSE;
        }

        return TRUE;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Real URL
     *
     * @access    public
     * @param    string
     * @return    string
     */
    function real_url($url)
    {
        return @fsockopen("$url", 80, $errno, $errstr, 30);
    } 
}
?>