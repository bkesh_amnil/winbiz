<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_pagination  {
	
	function per_page ()
	{
		return 20;
	}
	
	function admin_configuration()
	{
		$CI =& get_instance();
		
		$CI->load->library('pagination');
	
		$config['num_links'] = 10;
		
		$config['per_page'] = $this->per_page(); 
		$config['uri_segment'] = 3;		//may be need to change here
		
		$config['full_tag_open'] = ' <div class="pagination">';
		$config['full_tag_close'] = '</div>';
		
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '';
		$config['next_tag_close'] = '';
		
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '';
		$config['prev_tag_close'] = '';
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '';
		$config['first_tag_close'] = '';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '';
		$config['last_tag_close'] = '';
		
		$config['cur_tag_open'] = '<span>';
		$config['cur_tag_close'] = '</span>';
		
		$config['num_tag_open'] = '';
		$config['num_tag_close'] = '';
		
		 return $config;
	}
	
	function public_per_page ()
	{
		return 12;
	}
	
	function public_configuration($per_page = 0)
	{
		$CI =& get_instance();
		
		$CI->load->library('pagination');
		
		if( ! $per_page )
			$config['per_page'] = $this->public_per_page(); 
		else 
			$config['per_page'] = $per_page;
		
		$config['num_links'] = 15;
		
		$config['full_tag_open'] = ' <ul class="nav_page">';
		$config['full_tag_close'] = '</ul>';
		
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '';
		$config['next_tag_close'] = '';
		
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '';
		$config['prev_tag_close'] = '';
		
		$config['first_link'] = '';
		$config['first_tag_open'] = '';
		$config['first_tag_close'] = '';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '';
		$config['last_tag_close'] = '';
		
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		 return $config;
	}
	
}