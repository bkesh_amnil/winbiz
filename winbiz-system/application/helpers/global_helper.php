<?php

function get_today() {
    return date('Y-m-d');
}

function get_now() {
    return date('Y-m-d H:i:s');
}

function date_difference($day2, $day1 = '') {
    if ($day1 == '') {
        $day1 = get_now();
    }

    if ($day2 == '0000-00-00') {
        return '0';
    }
    $difference = strtotime($day2) - strtotime($day1);
    $difference = abs($difference) / 60 / 60 / 24;
    $difference = floor($difference);
    return $difference;
}

function printr($data, $exit = FALSE) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit) {
        exit;
    }
}

function convert_to_dropdown($object, $value, $index = 'id', $default_text = 'Select', $display_text = TRUE) {
    if ($display_text || count($object) > 1)
        $tmp[''] = $default_text;
    else
        $tmp = array();
    if (!empty($object)) {
        foreach ($object as $row) {
            $tmp[$row->$index] = $row->$value;
        }
    }
    return $tmp;
}

function dropdown_data($result, $index, $value, $rels = '') {
    $tmp = array();
    //$tmp['']  = 'Please select';
    foreach ($result as $row) {
        if ($rels) {
            $rel = '" ' . $rels . '="' . $row->$rels;
        } else {
            $rel = "";
        }
        $tmp[$row->$index . $rel] = $row->$value;
    }
    return $tmp;
}

function get_url() {
    $C = &get_instance();
    $get = $C->input->get();
    $url = '';
    if (is_array($get)) {
        $i = 1;
        foreach ($get as $index => $value) {
            if ($index == "database") {
                continue;
            }
            if (!$url) {
                $url = '?';
            }

            $url .= $index . "=" . $value;
            if ($i < count($get))
                $url .= '&';
            $i++;
        }
    }
    return ($url);
}

function get_category_dropdown($list, $valueFld = "id", $captFld = false, $selectattrs = false, $optattrs = false, $selected = false, $selected_values = FALSE) {
    $sattrs = ' ';
    $oattrs = ' ';
    $select = false;
    if ($selectattrs && is_array($selectattrs)) {
        foreach ($selectattrs as $ind => $val) {
            $sattrs .= $ind . '="' . $val . '" ';
        }
    }
    if ($optattrs && is_array($optattrs)) {
        foreach ($optattrs as $ind => $val) {
            $oattrs .= $ind . '="' . $val . '" ';
        }
    }
    $select .= '<select' . $sattrs . '>';
    if ($selected) {
        $selectd = explode(".", $selected);
        if (isset($selectd[0]) && $selectd[0] == "new") {
            $select .= '<option value="">' . (isset($selectd[1]) ? $selectd[1] : "select") . '</option>';
            if (isset($selectd[2])) {
                $selected = $selectd[2];
            } else {
                $selected = false;
            }
        } else {
            if (isset($selectd[1])) {
                $selected = $selectd[1];
            } else {
                $selected = false;
            }
        }
    }
    $select .= get_options($list, $valueFld, $captFld, $oattrs, 0, false, $selected, $selected_values);
    $select .= '</select>';
    return $select;
}

function get_options($list = false, $valueFld = "id", $captFld = false, $oattrs = false, $level = 0, $child = false, $selected = false, $selected_values = FALSE) {
    $optns = '';
    $space = '';
    for ($lev = 0; $lev < $level; $lev++) {
        $space .= "&shy;-&shy;";
    }
    if (!empty($list) && is_array($list)) {
        foreach ($list as $ind => $val) {
            if ($child == false) {
                $level = 0;
            }
            if (!isset($val->$valueFld) || !isset($val->$captFld)) {
                return false;
                break;
            }
            if ($selected_values) {
                $selecte = explode(',', $selected_values);
                foreach ($selecte as $sel) {
                    if (in_array($val->$valueFld, $selecte)) {
                        $isselected = ' selected="selected"';
                        break;
                    } else {
                        $isselected = '';
                    }
                }
            } else {
                if ($selected && $val->$valueFld == $selected) {
                    $isselected = ' selected="selected"';
                } else {
                    $isselected = '';
                }
            }
            $optns .= '<option value="' . $val->$valueFld . '"' . $oattrs . $isselected . '>' . $space . " " . $val->$captFld . '</option>';
            if (isset($val->childs) && !empty($val->childs)) {
                $optns .= get_options($val->childs, $valueFld, $captFld, $oattrs, ++$level, true, $selected, $selected_values);
            }
        }
        return $optns;
    } else {
        return false;
    }
}

/**
 * displays the array in preformatted form
 * @param type $array
 */
function dumparray($array, $exit = TRUE) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
    if ($exit) {
        exit;
    }
}

/**
 * prints the last executed query
 */
function printQuery($exit = TRUE) {
    $CI = &get_instance();
    echo $CI->db->last_query();
    if ($exit) {
        exit;
    }
}

/**
 * returns the money formatted value
 * @param type $money
 * @return type
 */
function show_money($money) {
    return config_item('currency') . ' ' . number_format($money, 2);
}

/**
 * returns the result set as per the parameters provided
 * @param type $table
 * @param type $where
 * @param type $result
 * @param type $order
 * @param type $limit
 * @param type $start
 * @return boolean
 */
function getWhere($table, $where, $result = FALSE, $order = '', $limit = '', $start = 0) {
    $ci = & get_instance();
    if ($where != '') {
        $ci->db->where($where);
    }
    if ($order != '') {
        $ci->db->order_by($order);
    }
    if ($limit != '') {
        $ci->db->limit($limit, $start);
    }
    $query = $ci->db->get($table);
    if ($query->num_rows() == 0) {
        return FALSE;
    } else {
        if ($result == TRUE) {
            return $query->result();
        } else {
            return $query->row();
        }
    }
}

?>
