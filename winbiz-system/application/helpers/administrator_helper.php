<?php

function current_admin_id() {
    $CI = & get_instance();
    return (int) $CI->session->userdata('admin_id');
}

function is_already_logged_in() {
    if (!current_admin_id()) {
        $data['login'] = 'You must login to access this page!';
        $data['class'] = 'error';
        $CI = & get_instance();
        $CI->session->set_flashdata($data);
        $current_url = current_url();
        $get = $CI->input->get();
        $getvals = "";
        //printr($get, true);
        if ($get && is_array($get)) {
            foreach ($get as $gind => $gval) {
                $getvals[] = $gind . "=" . $gval;
            }
        }
        if (isset($getvals) && is_array($getvals)) {
            $getvals = implode("&", $getvals);
        }
        $url = 'login?redirect=' . $current_url;
        if (!empty($getvals)) {
            $url = $url . "?" . $getvals;
        }
        redirect($url);
    }
}

function site_alias($str) {
    $str = strtolower(trim($str));
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);        //removing all the special characters                
    $str = preg_replace('/[\s]+/', '-', $str);                      //converting white space into hypen			
    $str = preg_replace('/-{2,}/', '-', $str);                      //removes more than 2 consecutive hypens 

    return $str;
}

function flash_redirect($page_name, $id) {
    $get = get_url();
    $page_name .= $get;

    if ((int) $id != 0) {
        $CI = & get_instance();

        if (!$get)
            $page_name .= '?';
        else
            $page_name .= '&';

        $page_name .= 'database=' . $id;
    }
    redirect($page_name);
}

function get_cms_menus() {
    $CI = & get_instance();
    $CI->db->where("status", "yes");
    $CI->db->order_by("order");
    $CI->db->where("parent_id", 0);
    $menus = $CI->db->get("cms_menu")->result();
    $menu_list = false;
    if (!empty($menus) && is_array($menus)) {
        foreach ($menus as $ind => $val) {
            $data = $CI->admin_user_model->get_credentials($val->controller);
            if (!$data['view']) {
                $authority = false;
            } else {
                $childs = get_cms_child_menus($val->id);
                $menu_list[] = array("name" => $val->name, "controller" => $val->controller, "childs" => $childs);
            }
        }
    }
    return $menu_list;
}

function get_cms_child_menus($id = false) {
    $CI = & get_instance();
    if ($id) {
        $CI->db->where("status", "yes");
        $CI->db->where("parent_id", $id);
        $CI->db->order_by("order");
        $menus = $CI->db->get("cms_menu")->result();
        if (!empty($menus) && is_array($menus)) {
            foreach ($menus as $ind => $val) {
                $data = $CI->admin_user_model->get_credentials($val->controller);
                if (!$data['view']) {
                    $authority = false;
                } else {
                    $menu_list[] = array("name" => $val->name, "controller" => $val->controller, "childs" => "");
                }
            }
            if (isset($menu_list) && is_array($menu_list)) {
                return $menu_list;
            }
        }
    }
    return false;
    //$this->db
}

function get_class_parent($class = false) {
    $CI = & get_instance();
    if ($class) {
        $CI->db->where("controller", $class);
        $CI->db->where("parent_id > 0");
        $CI->db->select("parent_id");
        $rowchild = $CI->db->get("cms_menu")->row();
        if ($rowchild && isset($rowchild->parent_id) && $rowchild->parent_id > 0) {
            $parent_id = $rowchild->parent_id;
            $CI->db->where("id", $parent_id);
            $CI->db->select("controller");
            $rowchild = $CI->db->get("cms_menu")->row();
            if ($rowchild && isset($rowchild->controller) && !empty($rowchild->controller)) {
                return $rowchild->controller;
            }
        }
        return false;
    }
}

//bkesh>>

/**
 * returns the count of total post made and array of the currently running arrays
 * @param type $id
 * @return type
 */
function get_ad_history($id, $field = 'ad_id',$site_id='') {
    $ci = & get_instance();
    $now = get_now();
    if($site_id!=''){
        $ci->db->where('site_id',$site_id);
    }
    $count_total = $ci->db->select('*')
            ->from('advertisement_session')
            ->where($field, $id)
            ->count_all_results();
    if($site_id!=''){
        $ci->db->where('site_id',$site_id);
    }
    $running = $ci->db->select('*')
            ->from('advertisement_session')
            ->where($field, $id)
            ->where('start_date <=', $now)
            ->where('end_date >=', $now)
            ->count_all_results();

    return array('total' => $count_total, 'running' => $running);
}

/**
 * 
 * @param type $provider_id
 * @return type
 */
function get_ad_payment_report($provider_id) {
    $ci = & get_instance();
    $q = $ci->db->select_sum('est_price')
                    ->select_sum('paid_price')
                    ->from('advertisement_session')
                    ->where('provider_id', $provider_id)
                    ->get()->row();
    if ($q) {
        $a = array('est' => $q->est_price, 'paid' => $q->paid_price);
    } else {
        $a = array('est' => '0.00', 'paid' => '0.00');
    }
    return $a;
}

function get_ymd($date) {
    return date('Y-m-d', strtotime($date));
}

function db_format($date) {
    return date('Y-m-d H:i:s', strtotime($date));
}

function user_format($date) {
    return date('dS F, Y', strtotime($date));
}
