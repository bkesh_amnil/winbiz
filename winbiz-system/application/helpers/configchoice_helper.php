<?php 
	
	/**
	*Takes the category and returns the array from the ConfigChoice table
	*@access  public
	*@param   string
	*@return  array
	*/
	 function getConfigChoices($category)
	{
		$CI =& get_instance();
		return	$CI->db->select('CC.ConfigChoiceID,CC.ConfigChoiceCode')
						->from('ConfigChoice CC')
						->join('ConfigChoiceCategory CCC','CCC.ConfigChoiceCategoryID = CC.ConfigChoiceCategoryID','inner')
						->where('CCC.Category',$category)
						->get()
						->result();
	}

	/**
	 * Takes the category and value to return the ChoiceID from ConfigChoice table
	 * @access public
	 * @param string
	 * @param string
	 * @return int 
	 */

	function getConfigChoiceID($category,$propertytype)
	{
		$CI =& get_instance();
		$data =  $CI->db->select('CC.ConfigChoiceID')
						->from('ConfigChoice CC')
						->join('ConfigChoiceProperty CCP','CCP.ConfigChoiceID = CC.ConfigChoiceID','inner')
						->join('ConfigChoicePropertyType CCPT','CCPT.ConfigChoicePropertyTypeID = CCP.ConfigChoicePropertyTypeID','inner')
						->join('ConfigChoiceCategory CCC','CCC.ConfigChoiceCategoryID = CCPT.ConfigChoiceCategoryID','inner')
						->where(array('CCC.Category'=>$category,'CCPT.Property'=>$propertytype,'CCP.`Value`'=>1), FALSE)
						->get()
						->row();
						//echo $CI->db->last_query(); die;
		if($data) {
			return $data->ConfigChoiceID;
		} else {
			return 0;
		}
	}
 ?>