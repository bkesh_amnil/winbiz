<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement_provider extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $provider = 'advertisement_provider';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Advertisement Provider";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue", "facebox");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['advertisement_provider'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('admin_advertisement_model', 'adv');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->adv->get_advertisement_providers());
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $data['rows'] = $this->adv->get_advertisement_providers(0, TRUE, $start, 'id');
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/view_advertisement_providers', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $this->load->helper('ckeditor');
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
//        $siteId = intval($this->input->get('site'));
//        if ($id != 0) {
//            $row = $this->adv->get_advertisements($id);
//            $siteId = $row->site_id;
//        } else if (empty($siteId)) {
//            $this->session->set_flashdata('class', 'error');
//            $this->session->set_flashdata('msg', "Please Select Site To Add Advertisement For.");
//            redirect('advertisement/');
//        }
        $this->header['con_title'] = "Add Advertisement Provider";
        $id = (int) $id;
        $error_mess = '';
        $this->load->library('form_validation');
//        dumparray($_POST);
        if ($this->input->post('save') == "true") {
            $this->form_validation->set_rules('name', 'Name', "trim|required|xss_clean");
            $this->form_validation->set_rules('email', 'email', "trim|required|xss_clean");
            $this->form_validation->set_rules('contact', 'contact', "trim|required|xss_clean");
            $this->form_validation->set_rules('address', 'address', "trim|required|xss_clean");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $not = array('id', 'add', 'save');
                $data = $this->mylibrary->get_post_array($not);
//                dumparray($data);
                if ($id == 0) {
                    $data['created_by'] = current_admin_id();
                    if ($this->db->insert($this->provider, $data)) {
                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'New data added successfully.');
                    } else {
                        $this->session->set_flashdata('class', 'error');
                        $this->session->set_flashdata('msg', 'Error in adding data.');
                    }
                } else {
                    $data['updated_by'] = current_admin_id();
                    $data['updated_at'] = get_now();

                    $this->db->where('id', $id);
                    if ($this->db->update($this->provider, $data)) {
                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'Data updated successfully.');
                    } else {
                        $this->session->set_flashdata('class', 'error');
                        $this->session->set_flashdata('msg', 'Error in updating data.');
                    }
                }
                flash_redirect($this->header['page_name'], $id);
            }
        }
        $data = $this->_format_data($id);
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/add_edit_advertisement_providers', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post('save') == "true") {
            $data['id'] = set_value('id');
            $data['name'] = set_value('name');
            $data['company'] = set_value('company');
            $data['email'] = set_value('email');
            $data['gender'] = set_value('gender');
            $data['contact'] = $this->input->post('contact');
            $data['address'] = set_value('address');
            $data['status'] = set_value('status');
        } else if ($id != 0) {
            $row = $this->adv->get_advertisement_providers($id);
            $data['id'] = $row->id;
            $data['name'] = $row->name;
            $data['company'] = $row->company;
            $data['email'] = $row->email;
            $data['gender'] = $row->gender;
            $data['contact'] = $row->contact;
            $data['address'] = $row->address;
            $data['status'] = $row->status;
            $data['created_by'] = $row->created_by;
            $data['created_date'] = $row->created_at;
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = $row->updated_at;
        } else {
            $data['id'] = '';
            $data['site_id'] = '1';
            $data['name'] = '';
            $data['company'] = '';
            $data['email'] = '';
            $data['gender'] = '';
            $data['contact'] = '';
            $data['address'] = '';
            $data['status'] = '';
        }

        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
//        dumparray($_POST);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->provider, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->provider, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        //check whether it is used in other tables or not	
        $this->load->library('restrict_delete');
        $params = "";  //change it later
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selectd_id) {
                    $this->db->where('id', $selectd_id);
                    $this->db->delete($this->provider);
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $this->db->where('id', $id);
                $this->db->delete($this->provider);
                if ($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system.';
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function autocomplete() {
        $q = strtolower($this->input->get("q"));
        $this->db->where("LOWER(category_name) LIKE '" . $q . "%'");
        $result = $this->db->get($this->category)->result();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $key => $value) {
                echo $value->category_name . "|" . $value->category_name . "\n";
            }
        }
    }

}
