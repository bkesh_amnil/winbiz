<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Sites extends CI_Controller{
        var $header = array();
        var $table_name = 'site';
        var $enterprises = 'enterprises';
		
        function __Construct()
        {
            parent::__Construct();

            $this->load->helper('administrator');
            is_already_logged_in();

            $this->header['title']			= "Manage Sites";
            
            $this->header['page_name']		= $this->router->fetch_class();

            $this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
           // $this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
            $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
            $this->data['backend'] = TRUE;
            $this->header['sites'] = TRUE;
            $this->load->library('custom_pagination');
        }
		
        function index()
        {	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');

            $start = $this->uri->segment(3);
            $total_rows = count($this->administrator_model->get_data($this->table_name));				//change here

            $config = $this->custom_pagination->admin_configuration();
            $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
            $config['total_rows'] = $total_rows;

            $data['start'] = $start;
            $data['rows'] = $this->administrator_model->get_data($this->table_name, 0, TRUE, $start, 'site_title');		//change here

            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);						
            $this->load->view('sites/view', $data);
            $this->load->view('action');                    
            $this->load->view('footer');
        }
		
        function form($id = NULL)
        { 
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            $site_count = $this->db->count_all('site');
            $row = $this->db->get('globalsetting')->row();
            $site_limit = $row->site_limits;
            if(! $id && $site_count >= $site_limit)
            {
                $this->session->set_flashdata('class', 'error');
                $this->session->set_flashdata('msg', 'You have reached limit of adding new sites.');	
                redirect($this->header['page_name']);
            }
            $error_mess = '';
            $this->load->library('form_validation');
            if ($this->input->post())
            { 
                $enterprise_id = $this->input->post('hidden_enteprise_id');
                $site_name = site_alias($this->input->post('site_title'));
                $this->form_validation->set_rules('site_title', 'site title', "trim|required|unique[site.site_title.{$id}]|ucwords");
                $this->form_validation->set_rules('sub_domain', 'sub domain', "trim|alpha|unique[site.sub_domain.{$id}]");
                $this->form_validation->set_rules('site_offline', 'site offline', 'trim|fixed_values[yes,no]');
                $this->form_validation->set_rules('site_offline_msg', 'site offline message', 'trim');
                $this->form_validation->set_rules('site_from_email', 'email', 'trim|valid_email');
                $this->form_validation->set_rules('site_description', 'site description', 'trim');
                $this->form_validation->set_rules('site_keyword', 'site keyword', 'trim');
                $this->form_validation->set_rules('google_analytics_code', 'google analytics code', 'trim');
                $this->form_validation->set_rules('no_of_menus', 'No. of site menus', 'trim|integer|greater_than[0]');						
                $this->form_validation->set_rules('numberofnewsTypes', 'No. of news type', 'trim|integer');
                $this->form_validation->set_rules('enableAllModules', 'enable All Modules', 'trim');
                $this->form_validation->set_rules('modulesEnabled', 'modules Enabled', '');
                $this->form_validation->set_rules('siteBanner', 'site banner', 'trim');
                $this->form_validation->set_rules('imageSize', 'image size', 'trim');
                $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
                if ($this->form_validation->run() == TRUE)
                {
                    /*social media*/
                    /*$media_name_array = $this->input->post('social_media_name');
                    $media_link_array = $this->input->post('social_media_url');
                    $emptyMediaLinkIndexes = array_keys($media_link_array, '');
                    if(!empty($emptyMediaLinkIndexes)){
                        foreach($emptyMediaLinkIndexes as $key){
                          unset($media_name_array[$key]);
                          unset($media_link_array[$key]);
                        }
                    }
                    ksort($media_link_array);
                    $media_link_array = array_values($media_link_array);
                    ksort($media_name_array);
                    $media_name_array = array_values($media_name_array);*/
                    /* social media*/
                    //for site_logo
                    if(isset($_FILES['site_logo']))
                    {
                        if(!empty($_FILES['site_logo']['name']) && $_FILES['site_logo']['error'] == 0){
                                    //upload image
                            $this->load->library('upload');

                            $config['upload_path'] 	 = './uploaded_files/site_logo/';
                            $config['allowed_types'] = 'png|jpeg|jpg|gif';
                            $config['max_size']		 =2048;
                            $config['max_width'] 	 = '1024';
                            $config['max_height'] 	 = '768';
                            $config['encrypt_name']  = TRUE;

                            $this->upload->initialize($config);					
                            if($this->upload->do_upload('site_logo'))
                            {
                                $data_image = $this->upload->data();
                                $insert_enterprise['enterprise_logo'] = $insert_data['site_logo'] = $data_image['file_name'];
                                $logoid = $id;
                                $this->db->where('id', $logoid);
                                $row = $this->db->get("site")->row();
                                /* if logo is updated in enterprise related site , same logo is to be updated in related enterprise */
                                if(file_exists('uploaded_files/site_logo/'.$data_image['file_name']) && !is_dir('uploaded_files/site_logo/'.$data_image['file_name'])){
                                    chmod("uploaded_files/site_logo/".$data_image['file_name'], 0777);
                                    if(copy("./uploaded_files/site_logo/".$data_image['file_name'], "./uploaded_files/enterprise/".$data_image['file_name'])){
                                        chmod('uploaded_files/enterprise/'.$data_image['file_name'], 0777);
                                    }
                                }
                                /* if logo is updated in enterprise related site , same logo is to be updated in related enterprise */
                                if(file_exists("./uploaded_files/site_logo/" .$row->site_logo) && !is_dir("./uploaded_files/site_logo/" .$row->site_logo))
                                {
                                    unlink ("./uploaded_files/site_logo/" .$row->site_logo);
                                }
                            }else{
                                $error = $this->upload->display_errors('', '');
                            }
                        }else{
                              /*$error = "Select logo For Site";*/
                            $insert_enterprise['enterprise_logo'] ="";
                        }
                    }
					// end og site logo
 if(isset($_FILES['share_image']))
                    {
                        if(!empty($_FILES['share_image']['name']) && $_FILES['share_image']['error'] == 0){
                                    //upload image
                            $this->load->library('upload');

                            $config['upload_path'] 	 = './uploaded_files/site_logo/';
                            $config['allowed_types'] = 'png|jpeg|jpg|gif';
                            $config['max_size']		 =2048;
                            $config['max_width'] 	 = '1024';
                            $config['max_height'] 	 = '768';
                            $config['encrypt_name']  = TRUE;

                            $this->upload->initialize($config);					
                            if($this->upload->do_upload('share_image'))
                            {
                                $data_image = $this->upload->data();
                                $insert_data['share_image'] = $insert_data['share_image'] = $data_image['file_name'];
                          
                               
                            }else{
                                $error = $this->upload->display_errors('', '');
                            }
                        }else{
                              /*$error = "Select logo For Site";*/
                            $insert_data['share_image'] ="";
                        }
                    }
                    if(empty($error)){
                        $insert_enterprise['enterprise_name'] = $insert_data['site_title']              = $this->input->post('site_title');
                        $insert_enterprise['enterprise_alias'] = $insert_data['site_name'] = $site_name;
                        $insert_enterprise['domain_name'] = $insert_data['sub_domain'] = $this->input->post('sub_domain');
                        $insert_data['site_offline']            = $this->input->post('site_offline');
                        $insert_data['site_offline_msg']        = $this->input->post('site_offline_msg');
                        $insert_data['site_from_email']         = $this->input->post('site_from_email');
                        $insert_data['site_description']        = $this->input->post('site_description');
                        $insert_data['site_keyword']            = $this->input->post('site_keyword');
                        $insert_data['google_analytics_code']   = $this->input->post('google_analytics_code');

                        if($insert_data['sub_domain'] != '') {
                            $insert_data['modules_enabled']     = '15,51,9,11,49,18,17,47,40,54,45,43,7,4,10,50,46,55,48,41,6';
                        } else {
                            $insert_data['modules_enabled']         = (is_array($this->input->post('modulesEnabled')) ? implode(",",$this->input->post('modulesEnabled')) : "");
                        }
                        $insert_data['siteBanner']              = strip_tags(addslashes(trim($this->input->post('siteBanner'))));
                        $insert_data['imageSize']               = strip_tags(addslashes(trim($this->input->post('imageSize'))));
                        $insert_data_mt['name']                 = $this->input->post('menuName');
                        $insert_data_mt['submenus']             = $this->input->post('submenus');
                        $insert_data_mt['depth']                = $this->input->post('depth');
                        $insert_data_mt['status']               = "yes";
                        $insert_data_nt['name']                 = $this->input->post('newsName');
                        $insert_enterprise['enterprise_facebook_link'] = $insert_data['facebook_url'] = $this->input->post('facebook_url');
                        $insert_enterprise['enterprise_twitter_link'] = $insert_data['twitter_url'] = $this->input->post('twitter_url');
                        $insert_enterprise['enterprise_youtube_link'] = $insert_data['youtube_url'] = $this->input->post('youtube_url');
                        $insert_enterprise['enterprise_linkedin_link'] = $insert_data['linkedin_url'] = $this->input->post('linkedin_url');
                        $insert_data['instagram_url']           = $this->input->post('instagram_url');
$insert_data['share_title']   = $this->input->post('share_title');
$insert_data['share_link']   = $this->input->post('share_link');
$insert_data['share_description']   = $this->input->post('share_description'); 
                        if($id == 0){	//insert
                            $insert_data['created_by']          = current_admin_id(); 
                            $insert_data['created_date']        = get_now();
                            $this->db->insert($this->table_name, $insert_data);
                            $site_id = $this->db->insert_id();
                            if(is_array($insert_data_mt['name']) && !empty($insert_data_mt['name'])){
                                for($i = 0; $i < count($insert_data_mt['name']); $i++){
                                    unset($menu_data);
                                    $menu_data['site_id'] = $site_id;
                                    $menu_data['name'] = trim(strip_tags($insert_data_mt['name'][$i]));
                                    $menu_data['submenus'] = (($insert_data_mt['submenus'][$i] == "yes") ? "yes" : "no");
                                    $menu_data['depth'] = ((intval($insert_data_mt['depth'][$i]) >0) ? intval($insert_data_mt['depth'][$i]) : 0);
                                    $menu_data['status'] = "yes";	
                                    if(!empty($menu_data['name'])){
                                        $this->db->where("site_menus.name", $menu_data['name']);
                                        $this->db->where("site_menus.site_id", $menu_data['site_id']);
                                        $result = $this->db->get("site_menus")->row();
                                        if($result->id > 0){
                                            $this->db->where('id', $result->id);
                                            $this->db->update("site_menus", $menu_data);
                                            $ids[] = $result->id;
                                        }else{
                                            $this->db->insert("site_menus", $menu_data);
                                            $ids[] = $this->db->insert_id();
                                        }
                                    }
                                }
                                if(is_array($ids) && !empty($ids)){
                                    $ids = implode(",",$ids);
                                    $this->db->query("DELETE FROM {PREFIX}site_menus WHERE id NOT IN (".$ids.") AND site_id = '".$site_id."'");
                                }else{
                                    $this->db->query("DELETE FROM {PREFIX}site_menus WHERE site_id = '".$site_id."'");
                                }
                            }
                            if(is_array($insert_data_nt['name']) && !empty($insert_data_nt['name'])){
                                for($i = 0; $i < count($insert_data_nt['name']); $i++){
                                    unset($news_data);
                                    $news_data['site_id'] = $site_id;
                                    $news_data['news_type'] = trim(strip_tags($insert_data_nt['name'][$i]));
                                    $news_data['status'] = "yes";
                                    if(!empty($news_data['news_type'])){
                                        $this->db->where("site_news.news_type", $news_data['news_type']);
                                        $this->db->where("site_news.site_id", $news_data['site_id']);
                                        $result = $this->db->get("site_news")->row();
                                        if($result->id > 0){
                                            $this->db->where('id', $result->id);
                                            $this->db->update("site_news", $news_data);
                                            $nids[] = $result->id;
                                        }else{
                                            $this->db->insert("site_news", $news_data);
                                            $nids[] = $this->db->insert_id();
                                        }
                                    }
                                }
                                if(is_array($nids) && !empty($nids)){
                                    $nids = implode(",",$nids);
                                    $this->db->query("DELETE FROM {PREFIX}site_news WHERE id NOT IN (".$nids.") AND site_id = '".$site_id."'");
                                }else{
                                    $this->db->query("DELETE FROM {PREFIX}site_news WHERE site_id = '".$site_id."'");
                                }
                            }
                            /* insert social media data */
                            /*if(!empty($media_link_array) && is_array($media_link_array)) {
                                foreach($media_link_array as $ind=>$val) {
                                    $insert_data_social['social_media_name'] = $media_name_array[$ind];
                                    $insert_data_social['social_media_url'] = $val;
                                    $this->db->insert('tbl_social_media', $insert_data_social);
                                }
                            }*/
                            /* insert social media data */
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        }else{
                            $insert_data['modified_by']		= current_admin_id(); 
                            $insert_data['modified_date']	= get_now(); 

                            $site_id = $id;
                            $this->db->where('id', $id);
                            $this->db->update($this->table_name, $insert_data);
                            if(is_array($insert_data_mt['name']) && !empty($insert_data_mt['name'])){
                                for($i = 0; $i < count($insert_data_mt['name']); $i++){
                                    unset($menu_data);
                                    $menu_data['site_id'] = $site_id;
                                    $menu_data['name'] = trim(strip_tags($insert_data_mt['name'][$i]));
                                    $menu_data['submenus'] = (($insert_data_mt['submenus'][$i] == "yes") ? "yes" : "no");
                                    $menu_data['depth'] = ((intval($insert_data_mt['depth'][$i]) >0) ? intval($insert_data_mt['depth'][$i]) : 0);
                                    $menu_data['status'] = "yes";	
                                    if(!empty($menu_data['name'])){
                                        $this->db->where("site_menus.name", $menu_data['name']);
                                        $this->db->where("site_menus.site_id", $menu_data['site_id']);
                                        $result = $this->db->get("site_menus")->row();
                                        if($result->id > 0){
                                            $this->db->where('id', $result->id);
                                            $this->db->update("site_menus", $menu_data);
                                            $ids[] = $result->id;
                                        }else{
                                            $this->db->insert("site_menus", $menu_data);
                                            $ids[] = $this->db->insert_id();
                                        }
                                    }
                                }
                                if(is_array($ids) && !empty($ids)){
                                    $ids = implode(",",$ids);
                                    $this->db->query("DELETE FROM {PREFIX}site_menus WHERE id NOT IN (".$ids.") AND site_id = '".$site_id."'");
                                }else{
                                    $this->db->query("DELETE FROM {PREFIX}site_menus WHERE site_id = '".$site_id."'");
                                }
                            }
                            if(is_array($insert_data_nt['name']) && !empty($insert_data_nt['name'])){
                                for($i = 0; $i < count($insert_data_nt['name']); $i++){
                                    unset($news_data);
                                    $news_data['site_id'] = $site_id;
                                    $news_data['news_type'] = trim(strip_tags($insert_data_nt['name'][$i]));
                                    $news_data['status'] = "yes";
                                    if(!empty($news_data['news_type'])){
                                        $this->db->where("site_news.news_type", $news_data['news_type']);
                                        $this->db->where("site_news.site_id", $news_data['site_id']);
                                        $result = $this->db->get("site_news")->row();
                                        if($result->id > 0){
                                            $this->db->where('id', $result->id);
                                            $this->db->update("site_news", $news_data);
                                            $nids[] = $result->id;
                                        }else{
                                            $this->db->insert("site_news", $news_data);
                                            $nids[] = $this->db->insert_id();
                                        }
                                    }
                                }
                                if(is_array($nids) && !empty($nids)){
                                    $nids = implode(",",$nids);
                                    $this->db->query("DELETE FROM {PREFIX}site_news WHERE id NOT IN (".$nids.") AND site_id = '".$site_id."'");
                                }else{
                                    $this->db->query("DELETE FROM {PREFIX}site_news WHERE site_id = '".$site_id."'");
                                }
                            }

                            if($enterprise_id != '0') {
                                $this->db->where('id', $enterprise_id);
                                $this->db->update($this->enterprises, $insert_enterprise);
                            }

                            //$this->db->where('id', $)

                            /*if(!empty($media_link_array) && is_array($media_link_array)) {
                                $this->db->query("DELETE FROM tbl_social_media");
                                foreach($media_link_array as $ind=>$val) {
                                    $insert_data_social['social_media_name'] = $media_name_array[$ind];
                                    $insert_data_social['social_media_url'] = $val;
                                    $this->db->insert('tbl_social_media', $insert_data_social);
                                }
                            }*/
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');						
                        }
                        $this->_createDynamicRouting();
                        redirect($this->header['page_name']);
                    }else{
                        $error_mess = $error;
                    }
                }
            }
            $data = $this->_format_data($id);
            $data['error_mess'] = $error_mess;							
            $this->load->view('header', $this->header);
            $this->load->view('backend/sub_menu');
            $this->load->view('menu', $this->data);
            $this->load->view('sites/form', $data);
            $this->load->view('footer');
        }
		
		function _createDynamicRouting(){
			$siteSetting = $this->db->get("globalsetting")->row();
			//printr($siteSetting);
			$structure = $siteSetting->site_structure;
			if($siteSetting->multiple_sites == "yes"){
				if($structure == "folder"){
					$my_file = 'application/config/dynamicrouting.route';
					chmod($my_file, 0777);
					$handle = fopen($my_file, 'w');
					$phpTagOpen = '<?php'."\n";
					fwrite($handle, $phpTagOpen);
					$this->db->where('main_site', 0);
					$sites = $this->db->get('site')->result();
					if(!empty($sites) && is_array($sites)){
						foreach($sites as $ind=>$site){
							if(!empty($site->site_name)){
								$data = '//Routing Rule For '.$site->site_title."\n";
								fwrite($handle, $data);
								$data = '$route[\''.$site->site_name.'\'] = \'home\';'."\n";
								fwrite($handle, $data);
								$data = '$route[\''.$site->site_name.'/(.*)\'] = "$1";'."\n\n\n";
								fwrite($handle, $data);
							}
						}					
					}
					$phpTagClose = "\n".'?>';
					fwrite($handle, $phpTagClose);
					fclose($handle);
					/*
					 * $route['cms2'] = "home";
					   $route['cms2/(.*)'] = "$1";
					*/
				}else{
					$my_file = 'application/config/dynamicrouting.route';
					$handle = fopen($my_file, 'w');
					$phpTagOpen = '<?php'."\n".'?>';
					fwrite($handle, $phpTagOpen);
					fclose($handle);
				}
			}else{
				$my_file = 'application/config/dynamicrouting.route';
				$handle = fopen($my_file, 'w');
				$phpTagOpen = '<?php'."\n".'?>';
				fwrite($handle, $phpTagOpen);
				fclose($handle);
			}
		}

		function _format_data($id)
		{
			$data['menuName']				= array();
			$data['submenus']				= array();
			$data['depth']					= array();
			$data['newsName']				= array();
			if ($this->input->post())
			{	
				$data['site_title']				= set_value('site_title');
				$data['sub_domain']				= set_value('sub_domain');
                $data['enterprise_id']          = '';
				$data['site_offline']			= set_value('site_offline');
				$data['site_offline_msg']		= set_value('site_offline_msg');
				$data['site_from_email']		= set_value('site_from_email');
				$data['site_description']		= set_value('site_description');
				$data['site_keyword']			= set_value('site_keyword');
				$data['google_analytics_code']	= set_value('google_analytics_code');
				$data['modules_enabled']		= set_value('modulesEnabled');
				$data['siteBanner']				= set_value('siteBanner');
				$data['imageSize']				= set_value('imageSize');
				$data['menuName']				= $this->input->post('menuName');
				$data['submenus']				= $this->input->post('submenus');
				$data['depth']					= $this->input->post('depth');
				$data['no_of_menus']			= set_value('no_of_menus');
				$data['numberofnewsTypes']		= set_value('numberofnewsTypes');				
				$data['newsName']				= $this->input->post('newsName');
				$data['enableAllModules']		= set_value('enableAllModules');
				$data['modulesEnabled']			= $this->input->post('modulesEnabled');
				$data['site_logo']				= '';
				$data['status']					= "yes";
                $data['facebook_url']           = '';
                $data['twitter_url']            = '';
                $data['youtube_url']            = '';
                $data['linkedin_url']           = '';
                $data['instagram_url']          = '';
$data['share_title'] = '';
$data['share_image'] = '';
$data['share_link'] = '';
$data['share_description']='';
			} 
			else if($id != 0) 
			{
				$row = $this->administrator_model->get_data($this->table_name, $id);
                /*$this->db->select('social_media_name, social_media_url');
                $res = $this->db->get('tbl_social_media')->result();*/
                if($row)
				{
					$data['site_title']				= $row->site_title;
					$data['sub_domain']				= $row->sub_domain;
                    $data['enterprise_id']          = $row->enterprise_id;
					$data['site_offline']			= $row->site_offline;
					$data['site_offline_msg']		= $row->site_offline_msg;
					$data['site_from_email']		= $row->site_from_email;
					$data['site_description']		= $row->site_description;
					$data['site_keyword']			= $row->site_keyword;
					$data['google_analytics_code']	= $row->google_analytics_code;
					$data['enableAllModules']		= '';
					$data['modules_enabled']		= $row->modules_enabled;
					$data['siteBanner']				= $row->siteBanner;
					$data['imageSize']				= $row->imageSize;
					$data['site_logo']				= $row->site_logo;
                    $data['facebook_url']           = $row->facebook_url;
                    $data['twitter_url']            = $row->twitter_url;
                    $data['youtube_url']            = $row->youtube_url;
                    $data['linkedin_url']           = $row->linkedin_url;
                    $data['instagram_url']          = $row->instagram_url;
$data['share_title'] = $row->share_title;
$data['share_image'] = $row->share_image;
$data['share_link'] = $row->share_link;
$data['share_description'] = $row->share_description;
					$this->db->where('site_id', $row->id);
					$menusType = $this->administrator_model->get_data("site_menus");
					$data['no_of_menus'] 			= count($menusType);
					if(!empty($menusType) && is_array($menusType)){
						foreach($menusType as $ind=>$val){
							$data['menuName'][] = $val->name;
							$data['submenus'][] = $val->submenus;
							$data['depth'][] 	= $val->depth;
						}
					} 
					$this->db->where('site_id', $row->id);
					$newsType = $row = $this->administrator_model->get_data("site_news");
					$data['numberofnewsTypes'] 		= count($newsType);
					if(!empty($newsType) && is_array($newsType)){
						foreach($newsType as $nind=>$nval){
							$data['newsName'][] 	= $nval->news_type;
						}
					}
                    /*if(isset($res) && !empty($res) && is_array($res)) {
                        $data['facebook_url'] = $data['twitter_url'] = $data['linkedin_url'] = $data['youtube_url'] = $data['instagram_url'] = '';
                        foreach($res as $val) {
                            if($val->social_media_name == 'facebook') {
                                $data['facebook_url'] = $val->social_media_url;
                            }

                            if($val->social_media_name == 'twitter') {
                                $data['twitter_url'] = $val->social_media_url;
                            }

                            if($val->social_media_name == 'linkedin') {
                                $data['linkedin_url'] = $val->social_media_url;
                            }

                            if($val->social_media_name == 'youtube') {
                                $data['youtube_url'] = $val->social_media_url;
                            }

                            if($val->social_media_name == 'instagram') {
                                $data['instagram_url'] = $val->social_media_url;
                            }
                        }
                    }*/
				}
				else 
				{
					$action['class'] = 'error';
					$action['msg'] = 'Invalid Request!';
					$this->session->set_flashdata($action);
					redirect($this->header['page_name']);
				}
			} 
			else 
			{
				$data['site_title']				= '';
				$data['sub_domain']				= '';
                $data['enterprise_id']          = '';
				$data['site_offline']			= 'yes';
				$data['site_offline_msg']		= '';
				$data['site_from_email']		= '';
				$data['site_description']		= '';
				$data['site_keyword']			= '';
				$data['google_analytics_code']	= '';
				$data['enableAllModules']		= '';
				$data['modules_enabled']		= '';
				$data['siteBanner']				= '';
				$data['imageSize']				= '';
				$data['newsName']				= '';
				$data['site_logo']				= '';				
				$data['no_of_menus']			= '';
				$data['numberofnewsTypes']		= '';
                $data['facebook_url']           = '';
                $data['twitter_url']            = '';
                $data['youtube_url']            = '';
                $data['linkedin_url']           = '';
                $data['instagram_url']          = '';
$data['share_title'] = '';
$data['share_image'] = '';
$data['share_link'] = '';
$data['share_description'] = '';
			}
		
			$data['site_offlines'] = array('No', 'Yes');
								
			return $data;			
		}
		
		function delete($id = NULL)
		{
                    $id = (int)$id;
                    $this->admin_user_model->access_module($this->header['page_name'], 'delete');
                    $row = $this->administrator_model->get_data($this->table_name, $id);
                    if(count($row) == 0 ){
                        $action['class'] = 'error';
                        $action['msg'] = 'Invalid Request!';
                    }else{
                        //need to delete all data related to this site 
                        if($row->main_site == 1){
                            $action['class'] = 'error';
                            $action['msg'] = 'Site you are trying to delete is <b>Main Site</b>. Main site is not  deletable!';
                            $this->session->set_flashdata($action);
                            redirect($this->header['page_name']);
                        }else{
                            $deleteConfirm = $this->input->post("confirmDeleteSite");
                            if($deleteConfirm != "sure"){
                                $tables = $this->db->list_tables();
                                $tableContainingCategory = array();
                                if(!empty($tables)){
                                    foreach($tables as $tablename){
                                        $fields = $this->db->list_fields($tablename);
                                        foreach ($fields as $field)
                                        {
                                            if($field == "site_id"){
                                                $this->db->where("site_id", $id);
                                                $records = $this->db->get($tablename)->result();
                                                $tableContainingCategory[] = array("tableName"=>$tablename, "rows"=>count($records));   
                                            }
                                        }
                                    }
                                }
                                $data["siteInfo"] = $row;
                                if(!empty($tableContainingCategory)){
                                    $data["tableStatus"] = $tableContainingCategory;
                                }else{
                                    $data["tableStatus"] = "";
                                }
                                $this->header['title']  = "Manage Sites : <span style=\"color:red\">Confirm Delete</span>";
                                $this->load->view('header', $this->header);
                                $this->load->view('backend/sub_menu');	
                                $this->load->view('menu', $this->data);						
                                $this->load->view('sites/delete', $data);
                                $this->load->view('footer');
                            }else{
                                $tables = $this->db->list_tables();
                                $tableContainingCategory = array();
                                if(!empty($tables)){
                                    foreach($tables as $tablename){
                                        $fields = $this->db->list_fields($tablename);
                                        foreach ($fields as $field){
                                            if($field == "site_id"){
                                            $this->db->where("site_id", $id);
                                            $this->db->delete($tablename);
                                            $res[] = "Delete ".$tablename." where site_id = ".$id;
                                           }
                                        }
                                    }
                                }
                                $this->db->where('id', $row->id);
                                $this->db->delete($this->table_name);
                                if($this->db->affected_rows() > 0){
                                    $this->_createDynamicRouting();
                                    $action['class'] = 'success';
                                    $action['msg'] = 'Data deleted successfully!';
                                }else{
                                    $action['class'] = 'error';
                                    $action['msg'] = 'Error in deleting data!';
                                }
                                $this->session->set_flashdata($action);
                                redirect($this->header['page_name']);
                            }
                        }
                    }
		}
    }