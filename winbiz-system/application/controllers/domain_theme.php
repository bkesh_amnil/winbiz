<?php
    class Domain_theme extends CI_Controller {
        public $header = array();
        var $module = 'module';
        var $profile = 'profile';
        var $profile_detail = 'profile_detail';
        var $domain_theme = 'domain_theme';
	
        function __Construct(){
            parent::__Construct();
            $this->load->helper('administrator');
            is_already_logged_in();
            $this->header['title']		= "Domain Theme Management";
            $this->header['page_name']	= $this->router->fetch_class();
            $this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
            $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
            $this->data['menu_cms']         = TRUE;
            $this->header['domain_theme']  = TRUE;
            $this->load->library('custom_pagination');
            $this->load->model('manager/admin_domain_theme_model', 'dom_theme');
        }
	
        function index(){	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
            $start = $this->uri->segment(3);
            $total_rows = count($this->dom_theme->get_domain_themes());
            $config = $this->custom_pagination->admin_configuration();
            $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
            $config['total_rows'] = $total_rows;
            $data['start'] = $start;
            $data['rows'] = $this->dom_theme->get_domain_themes(0, TRUE, $start, 'id');
            $this->load->view('header', $this->header);
            $this->load->view('cms/sub_menu');
            $this->load->view('menu', $this->data);
            $this->load->view('manager/view_domain_theme', $data);
            $this->load->view('action');			
            $this->load->view('footer'); 
        }
	
        function form($id = NULL){
            $this->load->helper('ckeditor');
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id); 
            $siteId = intval($this->input->get('site'));
            if( $id != 0 ){
                $row = $this->dom_theme->get_domain_themes($id);
            }
            $this->header['con_title'] = "Add Domain_theme For ".strtoupper($this->global_model->get_single_data("site", "site_name", $siteId)); 
            $id = (int)$id;			
            $error_mess = '';
            $this->load->library('form_validation');
            if($this->input->post()){
                $this->form_validation->set_rules('domain_theme_name', 'Domain Theme Name', "trim|required|xss_clean|unique[".$this->domain_theme.".domain_theme_name.$id]");
                $this->form_validation->set_rules('domain_theme_code', 'Domain Theme Code', "trim|required|xss_clean|unique[".$this->domain_theme.".domain_theme_code.$id]");
                $this->form_validation->set_rules('status', 'Status', "trim|required|fixed_values[1,0]");			
                $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');				
                if ($this->form_validation->run() == TRUE){
                    $insert_domain_theme['domain_theme_name']			 = $this->input->post('domain_theme_name');
                    $insert_domain_theme['domain_theme_code']            = $this->input->post('domain_theme_code');
                    $insert_domain_theme['status']			             = $this->input->post('status');
                        
                    if($id == 0){
                        $insert_domain_theme['created_by'] = current_admin_id(); 
                        $insert_domain_theme['created_date'] = get_now();

                        if($this->db->insert($this->domain_theme, $insert_domain_theme)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }
                    }
                    else{
                        $insert_domain_theme['updated_by']		= current_admin_id(); 
                        $insert_domain_theme['updated_date']	= get_now();
                        
                        $this->db->where('id', $id);
                        if($this->db->update($this->domain_theme, $insert_domain_theme)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }
                    }
                    flash_redirect($this->header['page_name'], $id);
                }
            }
            $data = $this->_format_data($id);			
            $data['siteId'] = $siteId;	
            $data['error_mess'] = $error_mess;
            $this->load->view('header', $this->header);
            $this->load->view('cms/sub_menu');
            $this->load->view('menu', $this->data);
            $this->load->view('manager/add_edit_domain_theme', $data);
            $this->load->view('footer');
        }
	
        function _format_data($id){
            if($this->input->post('save') == "true"){
                $data['id']                           = set_value('id');
                $data['domain_theme_name']			  = set_value('domain_theme_name');
                $data['domain_theme_code']		       = set_value('domain_theme_code');
                $data['status']			              = set_value('status');
            }else if($id != 0){
                $row = $this->dom_theme->get_domain_themes($id);
                $data['id']	                          = $row->id;
                $data['domain_theme_name']           = $row->domain_theme_name;
                $data['domain_theme_code']		  = $row->domain_theme_code;
                $data['status']                       = $row->status;
                $data['created_by']                   = $row->created_by;
                $data['created_date']                 = $row->created_date;
                $data['updated_by']                   = $row->updated_by;
                $data['updated_date']                 = $row->updated_date;
            }else{
                $data['id']                           = '';
                $data['domain_theme_name']			  = '';
                $data['domain_theme_code']		  = '';
                $data['status']	                      = '';
            }
            return $data;	
        }

        function change_status($status = '', $id = NULL){
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            if($this->input->post('selected')){
                $data = array('status' => $status);
                $selected_ids = $this->input->post('selected');
                $changed = 0;
                foreach($selected_ids as $selectd_id){
                    $this->db->where('id', $selectd_id);
                    $this->db->update($this->domain_theme, $data);
                    if($this->db->affected_rows() > 0) {
                        $changed++; 
                        $id = $selectd_id;      
                    }
                }
                if($changed){
                    $action['class'] = 'success';
                    $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }               
            }else {
                $data = array('status' => $status);
                $this->db->where('id', $id);
                $this->db->update($this->domain_theme, $data);
                if($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Status changed successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name'], $id);
        }
	
        function delete($id = NULL){
			$this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
			$id = (int)$id;
			//check whether it is used in other tables or not	
			$this->load->library('restrict_delete');
			$params = "";		//change it later
			if($this->restrict_delete->check_for_delete($params, $id)){
                if($this->input->post('selected')){
                    $selected_ids = $this->input->post('selected');
                    $deleted = 0;
                    foreach($selected_ids as $selectd_id){
                        $this->db->where('id', $selectd_id);
                        $this->db->delete($this->domain_theme);
                        if($this->db->affected_rows() > 0) {
                            $deleted++;						
                        }
                    }
                    if($deleted){
                        $action['class'] = 'success';
                        $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                    }else {
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }else{
                    $this->db->where('id', $id);
                    $this->db->delete($this->domain_theme);
                    if($this->db->affected_rows() > 0) {
                        $action['class'] = 'success';
                        $action['msg'] = 'Data deleted successfully!';
                    }else {
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }
			}else {
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
			}
			$this->session->set_flashdata($action);
			flash_redirect($this->header['page_name'],$id);
        }
    
    }
?>