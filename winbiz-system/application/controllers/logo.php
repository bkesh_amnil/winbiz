<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Logo extends CI_Controller {
		public $header = array();
		var $module = 'module';
		
		function __Construct() {
			parent::__Construct();
			
			$this->load->helper('administrator');
			is_already_logged_in();
			
			$this->header['title']			= "Manage Logos";
			$this->header['page_name']		= $this->router->fetch_class();
			
			$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
			//$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js","plugins/quicksearch.js");
			$this->data['users'] = TRUE;
			$this->header['profile'] = TRUE;
			
			$this->load->library('custom_pagination');
			$this->load->model('logo_model', 'logo');
			$this->load->model('module_model');
		}
		
		
		
		function index($id = NULL)
		{	
			$id = (int)$id;			
			$this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
			if($this->input->post()){
			$insert_data['first_title'] = $_POST['first_title'];
			$insert_data['second_title'] = $_POST['second_title'];
			$insert_data['third_title'] = $_POST['third_title'];
			$insert_data['first_link'] = $_POST['first_link'];
			$insert_data['second_link'] = $_POST['second_link'];
			$insert_data['third_link'] = $_POST['third_link'];
			if ($_FILES)
			{foreach($_FILES as $key=>$files)
			{
			if(!empty($files['name']) && $files['error'] == 0){
			//upload image										
                        $this->load->library('upload');
                        $config['upload_path'] 	 = './uploaded_files/logos/';
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size']		 =2048;
                        $config['encrypt_name']  = TRUE;
                        $this->upload->initialize($config);					
                        if($this->upload->do_upload($key)){
                            $data_image 		= $this->upload->data();
                            $insert_data[$key] = $data_image['file_name'];
			}else{							
                            $error = $this->upload->display_errors('', '');							
                        }	
			}
			}
						$insert_data['updated_date'] = time();
						$this->db->where('id', '0');
						$actRes = $this->db->update('tbl_logo', $insert_data);
						
						$this->session->set_flashdata('class', 'success');
						$this->session->set_flashdata('msg', "Data updated successfully.");				
					redirect($this->header['page_name']);
			}
			}
		
			$data = $this->_format_data($id);	
			$data['con_title']='Logo Management';
			$this->load->view('header', $this->header);
			$this->load->view('menu', $this->data);		
			$this->load->view('logo', $data);
			$this->load->view('footer');
		}
		
		
		
		function _format_data($id)
		{
			$data['readonly']		= '';
			if($this->input->post()){
				$data['first_title']			= set_value('first_title');
				$data['second_title']		= set_value('second_title');
				$data['third_title']		= set_value('third_title');
				$data['first_link']			= set_value('first_link');
				$data['second_link']		= set_value('second_link');
				$data['third_link']		= set_value('third_link');
			}
			if ($_FILES)
			{
				$data['first_logo']			= set_value('first_logo');
				$data['second_logo']		= set_value('second_logo');
				$data['third_logo']		= set_value('third_logo');
			
			}else{
				$row = $this->logo->get_logo();
				if($row)
				{
					$data['id']			= $row->id;
					$data['first_logo']		= $row->first_logo;
					$data['second_logo']		= $row->second_logo;
					$data['third_logo']			= $row->third_logo;
					$data['first_title']			= $row->first_title;
				$data['second_title']		= $row->second_title;
				$data['third_title']		= $row->third_title;
				$data['first_link']			= $row->first_link;
				$data['second_link']		= $row->second_link;
				$data['third_link']		= $row->third_link;
					
				}else{
					$data['id']			= '';
					$data['first_logo']		= '';
					$data['second_logo']		= '';
					$data['third_logo']			= '';
					$data['first_title']			= '';
				$data['second_title']		= '';
				$data['third_title']		= '';
				$data['first_link']			= '';
				$data['second_link']		= '';
				$data['third_link']		= '';
				}
			}					
			return $data;			
		}
		
		function delete($id = NULL)
		{
			$this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
		
			$id = (int)$id;
			$row = $this->admin_user_model->get_profiles($id);
			if(count($row) == 0 )
			{
				$action['class'] = 'error';
				$action['msg'] = 'Invalid Request!';
			}
			else
			{
				//check whether it is used in other tables or not	
				$this->load->library('restrict_delete');
				$params = "admin_user.profile_id";
				
				if($this->restrict_delete->check_for_delete($params, $id))
				{
					$this->db->where('id', $id);
					$this->db->delete($this->profile);
					
					if($this->db->affected_rows() > 0) {
						$this->db->where('profile_id', $id);
						$this->db->delete($this->profile_detail);
						
						$action['class'] = 'success';
						$action['msg'] = 'Data deleted successfully!';
					} else {
						$action['class'] = 'error';
						$action['msg'] = 'Error in deleting data!';
					}
				}
				else 
				{
					$action['class'] = 'error';
					$action['msg'] = 'This data cannot be deleted. It is being used in system.';
				}
			}
			$this->session->set_flashdata($action);
			redirect($this->header['page_name']);
		}
	}