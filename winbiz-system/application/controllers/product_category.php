<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_category extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $product_category = 'product_categories';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Product Category Management";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue", "facebox");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js", "scripts/form-validation.js", "plugins/jquery-validation/dist/jquery.validate.min.js", "plugins/jquery-validation/dist/additional-methods.min.js");
        $this->data['menu_cms'] = TRUE;  //menu
        $this->header['product_category'] = TRUE;  //submenu
        $this->load->library('custom_pagination');
        $this->load->model('admin_product_category_model', 'prod_category');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $data['start'] = $start;
        $data['rows'] = $this->prod_category->get_product_category(0, TRUE, $start, 'id');
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('products/view_product_category', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $id = (int) $id;
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $siteId = intval($this->input->get('site'));
        if ($id != 0) {
            $row = $this->prod_category->get_product_category($id);
            $siteId = $row->site_id;
        }
        $this->header['title'] = "Add / Edit Product Category";
        $id = (int) $id;
        $error_mess = '';
        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->form_validation->set_rules('product_category_name', 'Product Category Name', "trim|required|xss_clean|unique[" . $this->product_category . ".product_category_name.$id]");
            $this->form_validation->set_rules('product_category_alias', 'Product Category Alias', "trim|required|xss_clean|unique[" . $this->product_category . ".product_category_alias.$id]");
            $this->form_validation->set_rules('status', 'Status', 'trim|required|fixed_value[1,0]');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                if (empty($error)) {
                    $insert_data['site_id'] = $this->input->post('site_id');
                    $insert_data['product_category_name'] = $this->input->post('product_category_name');
                    $insert_data['product_category_alias'] = $this->input->post('product_category_alias');
                    $insert_data['status'] = $this->input->post('status');

                    if ($id == 0) {
                        $insert_data['created_by'] = current_admin_id();
                        $insert_data['created_date'] = get_now();
                        if ($this->db->insert($this->product_category, $insert_data)) {
                            $product_category_id = $this->db->insert_id();

                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added Successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }
                    } else {
                        $insert_data['updated_by'] = current_admin_id();
                        $insert_data['updated_date'] = get_now();
                        $this->db->where('id', $id);
                        if ($this->db->update($this->product_category, $insert_data)) {

                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data Updated Successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }
                    }
                    flash_redirect('' . $this->header['page_name'], $id);
                }
            }
        }
        $data = $this->_format_data($id);
        $data['con_title'] = $this->header['title'];
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('menu', $this->data);
        $this->load->view('products/add_edit_product_category', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post()) {
            $data['id'] = set_value('id');
            $data['site_id'] = set_value('site_id');
            $data['product_category_name'] = set_value('product_category_name');
            $data['product_category_alias'] = set_value('product_category_alias');
            $data['status'] = set_value('status');
        } elseif ($id != 0) {
            $row = $this->prod_category->get_product_category($id);
            $data['id'] = $row->id;
            $data['site_id'] = $row->site_id;
            $data['product_category_name'] = $row->product_category_name;
            $data['product_category_alias'] = $row->product_category_alias;
            $data['status'] = $row->status;
            $data['created_by'] = $row->created_by;
            $data['created_date'] = $row->created_date;
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = $row->updated_date;
        } else {
            $data['id'] = '';
            $data['site_id'] = '';
            $data['product_category_name'] = '';
            $data['product_category_alias'] = '';
            $data['status'] = '';
        }
        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->product_category, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->product_category, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        $this->load->library('restrict_delete');
        $params = "tbl_product_categories.product_category_id|tbl_enterprises.product_category_id";
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selected_id) {
                    $this->db->where('id', $selectd_id);
                    if ($this->db->delete($this->product_category))
                        $deleted++;
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in Deleting Data';
                }
            } else {
                $this->db->where('id', $id);
                if ($this->db->delete($this->product_category)) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Data Deleted Successfully';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in Deleting Data';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system for product categories or enterprise.';
        }
        $this->session->set_flashdata($action);
        flash_redirect('' . $this->header['page_name'], $id);
    }

    function autocomplete() {
        $q = strtolower($this->input->get("q"));
        $this->db->select('id, product_category_name');
        $this->db->where("LOWER(product_category_name) LIKE '" . $q . "%'");
        $result = $this->db->get($this->product_category)->result();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $key => $value) {
                echo $value->product_category_name . "|" . $value->product_category_name . "\n";
            }
        }
    }

}

?>