<?php

//Add following code in routes.php
//$route['image_show/(:any)'] = "image_show/index/$1";

class Image_show extends CI_Controller {

    function __Construct() {
        parent::__Construct();
    }

    function index($type = NULL, $width = 0, $height = 0, $file = NULL) {
        if (empty($width)) {
            $width = 100;
        }
        if (empty($height)) {
            $height = 100;
        }
        if (!empty($file)) {
            switch ($type) {
                case "logo":
                    $path = "./uploaded_files/site_logo/";
                    break;
                case "advertisement":
                    $path = "./uploaded_files/advertisement/";
                    break;
                case "content":
                    $path = "./uploaded_files/content/";
                    break;
                case "enterprise":
                    $path = "./uploaded_files/enterprise/";
                    break;
                case "products":
                    $path = "./uploaded_files/products/";
                    break;
                case "product_image":
                    $path = "./uploaded_files/product_images/";
                    break;
                case "temp":
                    $path = "./uploaded_files/temp/";
                    break;
                default:
                    $path = "./images/";
            }
            $imagePath = $path . $file;
            if (file_exists($imagePath)) {
                $this->output->set_content_type('JPEG');
                require_once './phpthumb/ThumbLib.inc.php';
                $thumb = PhpThumbFactory::create($path . $file);
                $thumb->adaptiveResize($width, $height);
                $thumb->show();
            }
        }
    }

}

?>
