<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

        class Cms extends CI_Controller {
            public $header = array();
		
            function __Construct(){
                parent::__Construct();
                $this->load->helper('administrator');
                is_already_logged_in();
                $this->header['title']			= "Manage Site Content";
                $this->header['page_name']		= $this->router->fetch_class();
                $this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
                //$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
                $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
                $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
                $this->data['menu_cms'] = TRUE;
                // to highlight the submenu
                //$this->header['profile'] = TRUE;

                $this->load->library('custom_pagination');
            }
		
            function index(){
                $data = $this->admin_user_model->get_credentials($this->header['page_name']);
                if( ! $data['view']){
                    $this->session->set_flashdata('class', 'error');
                    $this->session->set_flashdata('msg', "You don't have authority to view this page.");
                    redirect('dashboard');
                }

                $this->load->view('header', $this->header);
                $this->load->view('cms/sub_menu');
                $this->load->view('menu', $this->data);
                $this->load->view('users/users_view', $data);
                $this->load->view('footer');
            }
	}