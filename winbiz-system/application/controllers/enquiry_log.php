<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Enquiry_log extends CI_Controller {
        public $header = array();
	
        function __Construct(){
            parent::__Construct();
            $this->load->helper('administrator');
            is_already_logged_in();
            $this->header['title']		= "Enquiry Log";
            $this->header['page_name']	= $this->router->fetch_class();
            $this->header['stylesheets'] 	= array("960", "reset", "text", "blue","facebox");
            $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js");
            $this->data['menu_cms']         = TRUE;
            $this->header['enquiry_log']       = TRUE;
            $this->load->library('custom_pagination');
            $this->load->model('admin_enquiry_model', 'enquiry');
        }
        
        function index(){	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
            $start = $this->uri->segment(3);
            $data['start'] = $start;
            $data['rows'] = $this->enquiry->get_enquiry(0, "all");
            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);						
            $this->load->view('enquiry/view_enquiry_log', $data);	
            $this->load->view('action');		
            $this->load->view('footer');
        }
        
        function form($id = NULL){
            $id = (int)$id;	
	        $this->header['title'] = "Enquiry Log View"; 
            $data = $this->_format_data($id);	
            $data['con_title'] = $this->header['title']; 
            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);	
            $this->load->view('enquiry/enquiry_log_data', $data);
            $this->load->view('footer');
        }
	
        function _format_data($id){
            $row = $this->enquiry->get_enquiry($id);
            $data['id']			        = $row->id;
            $data['enterprise_name']    = $row->enterprise_name;
            $data['product_name']       = $row->product_name;
            $data['customer_name']	    = $row->customer_name;
            $data['email']	            = $row->email;
            $data['contact_number']     = $row->contact_number;
            $data['message']		    = $row->message;
            $data['enquiry_date']       = $row->enquiry_date;
            return $data;
        }
        
}
?>