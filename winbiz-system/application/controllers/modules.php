<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modules extends CI_Controller {

    public $header = array();
    var $table_name = 'module';
    var $profile_detail = 'profile_detail';

    function __Construct() {
        parent::__Construct();

        $this->load->helper('administrator');
        is_already_logged_in();

        $this->header['title'] = "Manage Modules";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        //$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['backend'] = TRUE;
        $this->header['modules'] = TRUE;

        $this->load->library('custom_pagination');  //add this
        $this->load->model('module_model', 'cmsmodules');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->administrator_model->get_modules());    //change here
        $search = $this->input->post('search');
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';   //change here
        $config['total_rows'] = $total_rows;
        $config['num_links'] = 4;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="first">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="last">';
        $config['next_link'] = 'Next → ';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '← Prev';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        //$this->pagination->initialize($config);

        $data['start'] = $start;
        // $data['rows'] = $this->administrator_model->get_modules(0, TRUE, $start, 'module_title');				//change here
        $data['rows'] = $this->cmsmodules->getParentModule($search);
        $this->load->view('header', $this->header);
        $this->load->view('menu', $this->data);
        $this->load->view('modules/view', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $id = (int) $id;

        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);

        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->form_validation->set_rules('module_title', 'module title', 'trim|required|ucwords');
            $this->form_validation->set_rules('module_name', 'module name', 'trim|required|strtolower|unique[' . $this->table_name . ".module_name.$id]");
            $this->form_validation->set_rules('parent_module', 'parent module', 'trim');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $insert_data['module_title'] = $this->input->post('module_title');
                $insert_data['module_name'] = $this->input->post('module_name');
                $parent_module = $this->input->post('parent_module');
                if (!empty($parent_module)) {
                    $insert_data['parent_module'] = $parent_module;
                } else {
                    $insert_data['parent_module'] = 0;
                }
                if ($id == 0) { //insert
                    $insert_data['created_by'] = current_admin_id();
                    $insert_data['created_date'] = get_now();
                    $this->db->insert($this->table_name, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'New data added successfully.');
                } else {
                    $insert_data['modified_by'] = current_admin_id();
                    $insert_data['modified_date'] = get_now();

                    $this->db->where('id', $id);
                    $this->db->update($this->table_name, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'Data updated successfully.');
                }
                redirect($this->header['page_name']);
            }
        }

        $data = $this->_format_data($id);

        $this->load->view('header', $this->header);
        $this->load->view('backend/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('modules/form', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post()) {
            $modules = $this->cmsmodules->getModuleName($id);
            $data['modules'] = convert_to_dropdown($modules, 'module_title', 'id');
            $data['module_title'] = set_value('module_title');
            $data['module_name'] = set_value('module_name');
            $data['parent_module'] = set_value('parent_module');
        } else if ($id != 0) {
            $row = $this->administrator_model->get_modules($id);
            if ($row) {
                $data['module_title'] = $row->module_title;
                $data['module_name'] = $row->module_name;
                $data['parent_module'] = $row->parent_module;
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Invalid Request!';
                $this->session->set_flashdata($action);
                redirect($this->header['page_name']);
            }
        } else {
            $data['module_title'] = '';
            $data['module_name'] = '';
            $data['parent_module'] = '';
            // $data['global_module']	= '1';
        }
        $modules = $this->cmsmodules->getModuleName($id);
        $data['modules'] = convert_to_dropdown($modules, 'module_title', 'id');
        // $data['global_modules'] = array('No', 'Yes');
        return $data;
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete');

        $row = $this->administrator_model->get_data($this->table_name, $id);
        if (count($row) == 0) {
            $action['class'] = 'error';
            $action['msg'] = 'Invalid Request!';
        } else if (!$row->deletable) {
            $action['class'] = 'error';
            $action['msg'] = 'This module cannot be deleted!';
        } else {
            $this->db->where('id', $row->id);
            $this->db->delete($this->table_name);

            if ($this->db->affected_rows() > 0) {
                $this->db->where('module_id', $id);
                $this->db->delete($this->profile_detail);

                $action['class'] = 'success';
                $action['msg'] = 'Data deleted successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in deleting data!';
            }
        }
        $this->session->set_flashdata($action);
        redirect($this->header['page_name']);
    }

    function sort_modules($parent_id = 0, $menu_type_id = '') {
        if ($this->input->post()) {
            $ids = explode(',', $this->input->post('sort_order'));
            for ($i = 0; $i < count($ids); $i++) {
                $data['module_position'] = $i;
                $this->db->where('id', $ids[$i]);
                $this->db->update($this->table_name, $data);
            }
        } else {
            $data['modules'] = $this->cmsmodules->get_module_for_sort($parent_id, $menu_type_id);
            $this->load->view('modules/sort', $data);
        }
    }

}
