<?php

/* add following code in routes.php
$route['form_controller/(:any)'] = "form_controller/index/$1";
*/

class Form_controller extends CI_Controller {
               
	function __construct()
	{
 		parent::__construct();
		$this->load->helper('administrator');
		is_already_logged_in();		
		$this->header['page_name']		= $this->router->fetch_class();
		
		$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
		//$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects", "jquery.animate-colors-min","swfupload/swfupload", "swfupload/swfupload.queue", "swfupload/fileprogress", "swfupload/handlers");			
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
		$this->data['menu_cms'] = TRUE;		//menu
		$this->header['dynamic_form'] = TRUE;		//submenu
		$this->load->model('dynamic_form/form_model');
	}	
	
	function index($form_unique_name = '', $action = '', $id = '')
	{
		if($form_unique_name == "deleteImages_temp"){
			$this->deleteImages_temp();
		}else{
			$form = $this->db->where('form_unique_name', $form_unique_name)->get('form')->row();
			if( ! $form){
				redirect('dynamic_form');
			}
			
			$this->header['title'] = $form->form_title;
			$form->page_name = $this->header['page_name'] . '/' .$form->form_unique_name;
			if($action == 'remove_file')
			{
				$this->_remove_file();
			}
			else if($this->input->post())
			{
				$this->_post($form, $action, $id);
			}
			else if($action == 'form')
			{
				$this->_form($form, $action, $id);
			}
			else if($action == 'delete')
			{
				$this->_delete($form, $action, $id);
			}
			else
			{
				$this->_grid($form);
			}
		}
	}
	
	function _grid($form)
	{
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 	
		
		$form->add = $data['add'];
		$form->edit = $data['edit'];
		$form->delete = $data['delete'];
		$form->fields = $this->form_model->form_fields($form->id, 1);
		
		$submitted_forms = $this->db->where('form_id', $form->id)->get('form_submission')->result();
		
		$array = array();		
		$search_in = (int)$this->input->get('search_in');
		$search = $this->input->get('search');
		$i = 0;
		foreach($submitted_forms as $submitted_form)
		{
			if($search_in)
			{
				$result = $this->form_model->get_search_submitted_data($submitted_form->id, $search_in, $search);
				if( ! $result){
					continue;
				}
			}
			foreach($form->fields as $field) 
			{
				$array[$submitted_form->id][$field->id] = $this->form_model->get_submitted_data($submitted_form->id, $field->id, $search_in, $search);
			}
			$i++;
		}

		$this->load->library('custom_pagination');		//add this
		
		$start = (int)$this->uri->segment(3);
		$total_rows = count($array);				//change here
		
		$config = $this->custom_pagination->admin_configuration();
		$config['base_url'] = site_url() . '' . $form->page_name;			
		$config['total_rows'] = $total_rows;
		//$this->pagination->initialize($config);

		$form->start = $start;
		$form->submitted_forms =  array_slice($array, $start, $config['per_page'], true);
			
		$this->load->view('header', $this->header);
		$this->load->view('menu', $this->data);			
		$this->load->view('dynamic_form/form_grid', $form);
		$this->load->view('action');
		$this->load->view('footer');	
	}
	
	function _form($form, $action, $submitted_form_id)
	{ 
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $submitted_form_id);
		
		$form->fields = $this->form_model->form_fields($form->id);
		$form->submitted_form_id = $submitted_form_id;
		
		if($submitted_form_id)
		{
			$i = 0; 
			foreach($form->fields as $field)
			{
				$form->fields[$i]->default_value = $this->form_model->get_submitted_data($submitted_form_id, $field->id);
				$i++;
				
			}
		}
		
		$this->load->view('header', $this->header);
		$this->load->view('cms/sub_menu');	
		$this->load->view('menu', $this->data);			
		$this->load->view('dynamic_form/generate_form', $form);
		$this->load->view('footer');	
	}
	
	function _post($form, $action, $form_submission_id)
	{
		
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $form_submission_id);
		
		$fields = $this->input->post('field_name');
		$result = $this->form_model->form_fields($form->id);
				
		foreach($result as $row)
		{
			if($row->field_type != 'upload' && isset($fields[$row->id]))
			{
				$_POST['field_' . $row->id] = $fields[$row->id];
				$this->form_validation->set_rules('field_' . $row->id, $row->field_label, $row->validation_rule);			
			}
		}
			
		$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
	
		$data['action'] = 'success';	
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{			
			$data['action'] = 'error';
			$data['msg'] = validation_errors();
		}
		else if(! $this->input->is_ajax_request()) // passed validation proceed to post success logic
		{
			$submission['form_id'] = $form->id;
			
			$action = array();
			if($form_submission_id)
			{
				foreach($fields as $index=>$value)
				{
					unset($submission_fields);
					$submission_fields['form_submission_id'] = $form_submission_id;
					$submission_fields['form_field_id'] = $index;
					
					$row = $this->db->where($submission_fields)->get('form_submission_fields')->row();
					$submission_fields['form_field_value'] = $value;
					if($row)
					{						
						$this->db->where('id', $row->id);
						$this->db->update('form_submission_fields', $submission_fields);
					}
					else
					{
						$this->db->insert('form_submission_fields', $submission_fields);
					}
				}
				
				$this->_single_upload($form_submission_id);	
				
				$data['msg'] = 'update';
				
				$action['msg'] = 'Data updated successfully.';
			}
			else
			{
				if($form->submit_action == 'email' || $form->submit_action == 'both')
				{
					if($form->admin_email)
					{
						$this->load->library('email');
						
						$this->email->from('info@amniltech.com', 'Amnil Technologies');
						$this->email->to($form->admin_email); 
						
						$this->email->subject($form->form_title);
						$this->email->message($form->admin_email_msg); 
						
						$this->email->send();
					}					
				}
				if( ! ($form->submit_action == 'email' ))
				{
					$this->db->insert('form_submission', $submission);
				
					$submission_fields['form_submission_id'] = $this->db->insert_id();
					foreach($fields as $index=>$value)
					{
						$submission_fields['form_field_id'] = $index;
						$submission_fields['form_field_value'] = $value;
						
						$this->db->insert('form_submission_fields', $submission_fields);
					}
					
					$this->_single_upload($submission_fields['form_submission_id']);	
					$this->_add_multi_upload($submission_fields['form_submission_id']);
					
					$action['msg'] = 'New data added successfully.';
				}					
			}				
			
			if($form->success_msg)
				$action['msg'] = $form->success_msg;
			
			$action['class'] = 'success';
			$this->session->set_flashdata($action);
						
			redirect('' . $form->page_name . '/' . $this->input->get('cur_page')) ;
		}		
		echo json_encode($data);
	}
	
	function _delete($form, $action, $form_submission_id)
	{
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'delete', 1);
		
		$this->db->where('id', $form_submission_id);
		$this->db->delete('form_submission');
		
		if($this->db->affected_rows())
		{
			//need to delete files if field is upload
			
			$this->db->where('form_submission_id', $form_submission_id);
			$this->db->delete('form_submission_fields');
		}
		
		redirect('' . $form->page_name . '/' . $this->input->get('cur_page')) ;
	}
	
	//ajax remove file
	function _remove_file()
	{
		if($this->input->is_ajax_request())
		{
			$this->db->where('form_submission_id', $this->input->post('submitted'));
			$this->db->where('form_field_id', $this->input->post('field_id'));
			$row = $this->db->get('form_submission_fields')->row();
			
			$action = 'fail';
			if($row)
			{
				$file = './dynamic_uploads/' . $row->form_field_value;
				
				if(is_file($file) && file_exists($file))
					unlink($file);
				
				if($this->input->post('multi') == "true")
				{
					$action = 'multi_upload';
				}
				else
				{
					$action = 'success';
				}
				$this->db->where('id', $row->id);
				//$update['form_field_value'] = '';
				$this->db->delete('form_submission_fields');
			}
			echo $action;
		}
		else
		{
			redirect('');
		}
	}
	
	//single file uploads multiple times
	function _single_upload($form_submission_id)
	{
		$table_name = 'form_submission_fields';
		
		//uploading files
		$this->load->library('upload');
		
		$config['upload_path'] 	= './dynamic_uploads/';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		
		if( ! isset($_FILES['field_name'])){
			return;
		}
				
		foreach($_FILES['field_name']['name'] as $i=>$value)
		{
			$_FILES['userfile']['name'] 	= $_FILES['field_name']['name'][$i];
			$_FILES['userfile']['type']		= $_FILES['field_name']['type'][$i];
			$_FILES['userfile']['tmp_name']	= $_FILES['field_name']['tmp_name'][$i];
			$_FILES['userfile']['error'] 	= $_FILES['field_name']['error'][$i];
			$_FILES['userfile']['size'] 	= $_FILES['field_name']['size'][$i];
						
			if($this->upload->do_upload())
			{
				$data_image = $this->upload->data();
				
				unset($insert_data);
				$insert_data['form_submission_id'] = $form_submission_id;
				$insert_data['form_field_id']	 = $i;
				
				if($this->db->where($insert_data)->count_all_results($table_name))
				{
					$this->db->where($insert_data);
					$insert_data['form_field_value']  = $data_image['file_name'];
					$insert_data['form_display_text'] = $data_image['orig_name'];
					$this->db->update($table_name, $insert_data);
				}
				else
				{
					$insert_data['form_field_value'] = $data_image['file_name'];
					$insert_data['form_display_text'] = $data_image['orig_name'];
					$this->db->insert($table_name, $insert_data);
				}
			}
			else
			{
				//echo $this->upload->display_errors('', '');
			}
		}
	}
	
	function deleteImages_temp()
	{
		
		if($this->input->get('file')){
				$file = "./uploaded_files/temp/".$this->input->get('file');
				
				if(file_exists($file)){
					//printr($file);
					unlink($file);
				}
				$msg = "Deleted Successfully";
				echo $msg;
		}
	}
	
	function _add_multi_upload($form_submission_id)
	{
			//echo $galleryId."<hr>";
			//printr($_POST);
			$table_name = 'form_submission_fields';
			$post = $this->input->post();
			$imageProperties = (isset($post['imageProp']) ? $post['imageProp'] : "");	
                        
			
			if(!empty($imageProperties) && is_array($imageProperties)){
				foreach($imageProperties as $ind=>$val){
					
					if(file_exists('uploaded_files/temp/'.$val['file'])){
						chmod("uploaded_files/temp/".$val['file'], 0777);
						if(copy("./uploaded_files/temp/".$val['file'], "./dynamic_uploads/".$val['file'])){
							unlink("./uploaded_files/temp/".$val['file']);
							chmod('dynamic_uploads/'.$val['file'], 0777);
							
						$insert_data['form_submission_id'] = $form_submission_id;
						$insert_data['form_field_id']	 = $_POST['form_field_id'];
						
					$insert_data['form_field_value'] = $val['file'];
					$insert_data['form_display_text'] = $val['file'];
					$this->db->insert($table_name, $insert_data);
							
							
						}
					}
				}
			}
			
			//$this->session->set_flashdata('class', 'success');
			//$this->session->set_flashdata('msg', 'Images Description Updated.');
			//redirect($this->header['page_name'] . "/images?id=".$galleryId);
		
	}
}
