<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class My_profile extends CI_Controller {
		public $header = array();
		
		function __Construct()
		{
			parent::__Construct();
			
			$this->load->helper('administrator');
			is_already_logged_in();
			
			$this->header['title']			= "My Profile";
			$this->header['page_name']		= $this->router->fetch_class();
			
			$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
			//$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
			$this->data['my_profile'] = TRUE;			//menu
			$this->header['my'] = TRUE;		//submenu
			
			$this->load->library('custom_pagination');		//add this
		}

		function index()
		{
			$this->_menus('my_profile');
			
			$data = $this->admin_user_model->get_user_detail(current_admin_id());			

			$this->load->view('header', $this->header);
			$this->load->view('menu', $this->data);	
		   	$this->load->view('my_profile/view_profile', $data);					
			$this->load->view('footer');
		}
		
		function change_password()
		{
			$data = array();	
			$this->load->library('form_validation');
			if ($this->input->post())
			{				
				$this->form_validation->set_rules('old_password', 'Old Password', 'required|md5');
				$this->form_validation->set_rules('new_password', 'New Password', 'required|min_length[6]|matches[confirm_password]|md5');
				$this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required');
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
				
				if ($this->form_validation->run() == TRUE)
				{	
					//need to check old password
					$old_password = $this->global_model->get_single_data('admin_user', 'password', current_admin_id());
					if($old_password != $this->input->post('old_password'))
					{
						$this->session->set_flashdata('class', 'error');
						$this->session->set_flashdata('msg', 'Please enter your valid old password.');
						redirect('my_profile/change_password');
					}
					else
					{
						$update_data['password'] = $this->input->post('new_password');
						$this->db->where('id', current_admin_id());
						$this->db->update('admin_user', $update_data);
					
						$this->session->set_flashdata('class', 'success');
						$this->session->set_flashdata('msg', 'Your password has been changed successfully.');
						redirect('my_profile');
					}
				}
			}
			
			$this->_menus('password');
			$this->load->view('header', $this->header);
			$this->load->view('menu', $this->data);		
			$this->load->view('my_profile/change_password');
			$this->load->view('footer');
		}
		
		function _format_users()
		{
			$id = current_admin_id();
			
			$row = $this->admin_user->get_user_detail(current_admin_id());
		
			if ($this->input->post())
			{
				$data['first_name']		= set_value('first_name');
				$data['email']			= set_value('email');
				$data['contact_num']	= set_value('contact_num');
				$data['address']		= set_value('address');
			} 
			else if($id != 0) 
			{	
				$data['display_name']	= $row->display_name;
				$data['email']			= $row->email;
				$data['contact_num']	= $row->contact_num;
				$data['address']		= $row->address;
			} 				
			return $data;
		}		
		
		//private methods 
		function _menus($current)
		{
			$profile = $password = $edit_info = '';
			if($current == 'my_profile') {
				$profile = 'class="current"';
			} else if($current == 'password')  {
				$password = 'class="current"';
			} else if($current == 'edit_info')  {
				$edit_info = 'class="current"';
			}
			
			$this->data['sub_menus'][] = anchor('my_profile/', '<span>My Profile</span>', $profile);
			/*$this->data['sub_menus'][] = anchor('my_profile/update', '<span>Update Profile</span>', $edit_info);*/
			$this->data['sub_menus'][] = anchor('my_profile/change_password', '<span>Change Password</span>', $password);			
		}
	}