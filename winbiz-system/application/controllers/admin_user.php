<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_user extends CI_Controller {

    public $header = array();
    var $admin_user = 'admin_user';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';

    function __Construct() {
        parent::__Construct();

        $this->load->helper('administrator');
        is_already_logged_in();

        $this->header['title'] = "Manage Super Administrator";
        $this->header['page_name'] = $this->router->fetch_class();

        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['users'] = TRUE;
        $this->header['admin_user'] = TRUE;

        $this->load->library('custom_pagination');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');

        $start = $this->uri->segment(3);
        $total_rows = count($this->admin_user_model->get_users());

        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;

        $data['start'] = $start;
        $data['rows'] = $this->admin_user_model->get_users(0, TRUE, $start, 'user_name');
        $this->load->view('header', $this->header);
        $this->load->view('menu', $this->data);
        $this->load->view('users/view_admin', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);

        $this->load->library('form_validation');
        if ($this->input->post()) {
            $profile_id = $this->input->post('profile_id');

            $this->form_validation->set_rules('profile_id', 'profile name', "trim|required|integer|exists[profile.id]");
            $this->form_validation->set_rules('user_name', 'user name', "trim|required|alpha_numeric|unique[admin_user.user_name.{$id}]");
            $this->form_validation->set_rules('first_name', 'first name', "trim|ucwords|required");
            $this->form_validation->set_rules('last_name', 'last name', "trim|ucwords");
            $this->form_validation->set_rules('email', 'email', "trim|valid_email|required");
            $this->form_validation->set_rules('gender', 'gender', "trim|ucwords");
            $this->form_validation->set_rules('mobile_num', 'mobile number', "trim");
            $this->form_validation->set_rules('address', 'address', "trim");

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');

            if ($this->form_validation->run() == TRUE) {
                $insert_data['profile_id'] = $this->input->post('profile_id');
                $insert_data['first_name'] = $this->input->post('first_name');
                $insert_data['last_name'] = $this->input->post('last_name');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['gender'] = $this->input->post('gender');
                $insert_data['mobile_num'] = $this->input->post('mobile_num');
                $insert_data['address'] = $this->input->post('address');
                $insert_data['status'] = $this->input->post('status');

                if ($id == 0) {
                    $insert_data['user_name'] = $this->input->post('user_name');
                    $insert_data['password'] = md5($this->input->post('user_name'));
                    $insert_data['created_by'] = current_admin_id();
                    $insert_data['created_date'] = get_now();
                    $this->db->insert($this->admin_user, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'New data added successfully.');
                } else {
                    $insert_data['modified_by'] = current_admin_id();
                    $insert_data['modified_date'] = get_now();
                    $this->db->where('id', $id);
                    $this->db->update($this->admin_user, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'Data updated successfully.');
                }
                redirect($this->header['page_name']);
            }
        }

        $data = $this->_format_data($id);

        $this->load->view('header', $this->header);
        $this->load->view('users/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('users/form_admin', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        $data['readonly'] = '';
        if ($this->input->post()) {
            $data['profile_id'] = set_value('profile_id');
            $data['user_name'] = set_value('user_name');
            $data['first_name'] = set_value('first_name');
            $data['last_name'] = set_value('last_name');
            $data['email'] = set_value('email');
            $data['gender'] = set_value('gender');
            $data['mobile_num'] = set_value('mobile_num');
            $data['address'] = set_value('address');
            $data['status'] = set_value('status');
        } else if ($id != 0) {
            $row = $this->admin_user_model->get_users($id);
            if ($row) {
                $data['profile_id'] = $row->profile_id;
                $data['user_name'] = $row->user_name;
                $data['first_name'] = $row->first_name;
                $data['last_name'] = $row->last_name;
                $data['email'] = $row->email;
                $data['gender'] = $row->gender;
                $data['mobile_num'] = $row->mobile_num;
                $data['address'] = $row->address;
                $data['status'] = $row->status;
                $data['readonly'] = 'readonly="readonly"';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Invalid Request!';
                $this->session->set_flashdata($action);
                redirect($this->header['page_name']);
            }
        } else {
            $data['profile_id'] = '';
            $data['user_name'] = '';
            $data['first_name'] = '';
            $data['last_name'] = '';
            $data['gender'] = 'Male';
            $data['mobile_num'] = '';
            $data['address'] = '';
            $data['email'] = '';
            $data['status'] = 'Active';
        }

        //get_profiles start here
        $result = $this->admin_user_model->get_profiles();
        $tmp[''] = 'Select Profile';
        foreach ($result as $row) {
            $tmp[$row->id] = $row->profile_name;
        }
        $data['profiles'] = $tmp;
        //get_profiles end here

        $data['genders'] = array(
            'Male' => 'Male',
            'Female' => 'Female');
        $data['statuses'] = array(
            'Active' => 'Active',
            'Inactive' => 'Inactive');
        return $data;
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);

        $id = (int) $id;
        if ($this->db->where('id', $id)->count_all_results($this->admin_user) == 0) {
            $action['class'] = 'error';
            $action['msg'] = 'Invalid Request!';
        } else {
            //check whether it is used in other tables or not	
            $this->load->library('restrict_delete');
            $params = "admin_user.created_by | admin_user.modified_by | 
						advertisement.created_by | advertisement.updated_by |
						banner.created_by | banner.updated_by |
						content.created_by | content.updated_by |
						form.created_by | form.updated_by |
						gallery.created_by | gallery.updated_by |
						images.created_by | images.updated_by |
						material.created_by | material.updated_by |
						menu.created_by | menu.updated_by |
						module.created_by | module.modified_by | 
						news.created_by | news.updated_by |
						profile.created_by | profile.modified_by |
						setting.created_by | setting.modified_by |
						site.created_by | site.modified_by";

            if ($this->restrict_delete->check_for_delete($params, $id)) {
                $this->db->where('id', $id);
                $this->db->delete($this->admin_user);

                if ($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
            }
        }
        $this->session->set_flashdata($action);
        redirect($this->header['page_name']);
    }

    //manage credentials
    function manage_credentials($profile_id = 0) {
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $profile_id);

        $profile_id = (int) $profile_id;
        $row = $this->admin_user_model->get_profiles($profile_id);

        if (count($row) == 0 || (int) $profile_id == 0) {
            $action['class'] = 'error';
            $action['msg'] = 'Invalid Request!';
            $this->session->set_flashdata($action);
            redirect('' . $this->header['page_name']);
        }

        if ($this->input->post('module_ids')) {
            $module_ids = $this->input->post('module_ids');
            $view = $this->input->post('view');
            $add = $this->input->post('add');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');

            foreach ($module_ids as $module_id) {
                unset($insert_data);
                $insert_data['view_module'] = (isset($view[$module_id])) ? 1 : 0;
                $insert_data['add_module'] = (isset($add[$module_id])) ? 1 : 0;
                $insert_data['edit_module'] = (isset($edit[$module_id])) ? 1 : 0;
                $insert_data['delete_module'] = (isset($delete[$module_id])) ? 1 : 0;

                $this->db->where('profile_id', $profile_id);
                $this->db->where('module_id', $module_id);
                if ($this->db->count_all_results($this->profile_detail) == 0) {
                    $insert_data['profile_id'] = $profile_id;
                    $insert_data['module_id'] = $module_id;
                    $this->db->insert($this->profile_detail, $insert_data);
                } else {
                    $this->db->where('profile_id', $profile_id);
                    $this->db->where('module_id', $module_id);
                    $this->db->update($this->profile_detail, $insert_data);
                }
            }
            $action['class'] = 'success';
            $action['msg'] = 'Credentials changed successfully!';
            $this->session->set_flashdata($action);
            redirect('profile');
        }

        $this->db->order_by('module_title');
        $data['modules'] = $this->db->get($this->module)->result();
        $data['profile_id'] = $profile_id;
        $this->header['title'] = "Manage Credentials of Profile '" . $row->profile_name . "'";

        $this->load->view('header', $this->header);
        $this->load->view('users/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('users/manage_credentials', $data);
        $this->load->view('footer');
    }

}