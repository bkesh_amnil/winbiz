<?php

class Advertisement extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $advertisement = 'advertisement';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Advertisement Management";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['advertisement'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('admin_advertisement_model', 'adv');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->adv->get_advertisements());
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $data['rows'] = $this->adv->get_advertisements(0, TRUE, $start, 'id');
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/view_advertisement', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $this->load->helper('ckeditor');
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $siteId = intval($this->input->get('site'));
        if ($id != 0) {
            $row = $this->adv->get_advertisements($id);
            $siteId = $row->site_id;
        } else if (empty($siteId)) {
            $this->session->set_flashdata('class', 'error');
            $this->session->set_flashdata('msg', "Please Select Site To Add Advertisement For.");
            redirect('advertisement/');
        }
        $this->header['con_title'] = "Add Advertisement For " . strtoupper($this->global_model->get_single_data("site", "site_name", $siteId));
        $id = (int) $id;
        $error_mess = '';
        $this->load->library('form_validation');
        if ($this->input->post('save') == "true") {
            $this->form_validation->set_rules('site_id', 'Site Id', "trim|required|integer");
            $this->form_validation->set_rules('advertisement_name', 'Advertisement Name', "trim|required|xss_clean");
            $this->form_validation->set_rules('advertisement_position_id', 'Advertisement Position', "trim|required|xss_clean|integer|unique_ad[advertisement.advertisement_position_id.$id]");
            $this->form_validation->set_rules('advertisement_link_type[]', 'Advertisement Link Type', "trim|required|xss_clean|fixed_values[Product,Internal Content,External URL,None]");
            $menuId = 0;
            $contentId = 0;
            $shopId = 0;
            if ($this->input->post('advertisement_link_type') == "Internal Content") {
                $contentId = $this->input->post('content_id');
                $this->form_validation->set_rules('content_id', 'Content', 'trim|required|exists[content.id]');
                $this->form_validation->set_rules('advertisement_link_opens', 'Advertisement Link Opens', "trim|required|fixed_values[same,new]");
            } else if ($this->input->post('advertisement_link_type') == "External URL") {
                $this->form_validation->set_rules('external_url', 'Link Url', 'trim|required|prep_url');
                $this->form_validation->set_rules('advertisement_link_opens', 'Advertisement Link Opens', "trim|required|fixed_values[same,new]");
            } else if ($this->input->post('advertisement_link_type') == "Product") {
                $shopId = $this->input->post('product_id');
                $this->form_validation->set_rules('product_id', 'Product', 'trim|required|exists[products.id]');
                $this->form_validation->set_rules('advertisement_link_opens', 'Advertisement Link Opens', "trim|required|fixed_values[same,new]");
            }
            $this->form_validation->set_rules('publish_date', 'Publish Date', "required");
            $this->form_validation->set_rules('unpublish_date', 'Unpublish Date', "required");
            $this->form_validation->set_rules('status', 'Status', "trim|required|fixed_values[1,0]");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $imgsize = $this->input->post('advertisement_image_size');
                $imgsize = explode('X', $imgsize);
                $width = $imgsize[0];
                $height = $imgsize[1];
                $imgUpload = false;
                $oldAdvertisement = $this->input->post('uploaded_advertisement_old');
                if (/* $id == 0 || */!empty($_FILES['advertisement_image']['name'])) {
                    if (!empty($_FILES['advertisement_image']['name']) && $_FILES['advertisement_image']['error'] == 0) {
                        $filename = $_FILES['advertisement_image']['name'];
                        $filesize_image = $_FILES['advertisement_image']['size'];
                        $file = $_FILES['advertisement_image']['tmp_name'];
                        $file_size = getimagesize($file);
                        $file_width = $file_size[0];
                        $file_height = $file_size[1];
                        $allowed_ext = "jpg,jpeg,gif,png,bmp";
                        $imgInfo = getimagesize($file);
                        $file_type = image_type_to_mime_type(exif_imagetype($file));
                        $allowed_ext = preg_split("/\,/", $allowed_ext);
                        $maxSize = 1024 * 8;
                        $minW = $width;
                        $minH = $height;
                        $for = "bigyapan";
                        $uploadPath = "./uploaded_files/bigyapan/";

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath);
                            chmod($uploadPath, 0777);
                        }
                        /* if($filesize_image > ($maxSize * 1024)){
                          $error = "Image size is too large to accept. Image Size should be less than ".$maxSize." KB";
                          } else if($file_width < $width || $file_height < $height) {
                          $error = "Image size is small. Image Size should be greater than or equal to ".$width."X".$height." for the position.";
                          }else{ */
                        switch ($file_type) {
                            case 'image/gif':
                                $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".gif";
                                $upltype = "gif";
                                break;
                            case 'image/jpeg':
                                $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                $upltype = "jpg";
                                break;
                            case 'image/jpg':
                                $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                $upltype = "jpg";
                                break;
                            case 'image/png':
                                $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".png";
                                $upltype = "png";
                                break;
                            default:
                                $fileUploadName = "";
                                break;
                        }
                        if (empty($fileUploadName)) {
                            $error = "Unrecognized File Type";
                        } else {
                            $options = array('jpegQuality' => 100);
                            require_once './phpthumb/ThumbLib.inc.php';
                            $thumb = PhpThumbFactory::create($file, $options);
                            $thumb->adaptiveResize($file_width, $file_height);
                            $saved = $thumb->save($uploadPath . "/" . $fileUploadName, $upltype);
                            if ($saved) {
                                $imgUpload = true;
                                $image = $fileUploadName;
                            }
                        }
                        /* } */
                        if (!$imgUpload) {
                            $uplErr = "";
                            if (!empty($error)) {
                                $uplErr = $error;
                            }
                            $error = "Image was not uploaded.";
                            $error = $error . " " . $uplErr;
                        }
                    } else {
                        $error = "Select Image For Advertisement";
                    }
                } else {
                    if ($id == 0)
                        $error = 'Advertisement Image is Compulsory';
                    $fileUploadName = $oldAdvertisement;
                }

                if (!empty($oldAdvertisement) && !empty($imgUpload)) {
                    if (file_exists("./uploaded_files/bigyapan/" . $oldAdvertisement) && !is_dir("./uploaded_files/bigyapan/" . $oldAdvertisement)) {
                        unlink("./uploaded_files/bigyapan/" . $oldAdvertisement);
                    }
                } else if (!empty($oldAdvertisement)) {
                    $image = $oldAdvertisement;
                }

                if (empty($error)) {
                    $insert_advertisement['site_id'] = $this->input->post('site_id');
                    $insert_advertisement['advertisement_position_id'] = $this->input->post('advertisement_position_id');
                    $insert_advertisement['advertisement_name'] = $this->input->post('advertisement_name');
                    $insert_advertisement['advertisement_image'] = $image;
                    $insert_advertisement['advertisement_link_type'] = $this->input->post('advertisement_link_type');
                    $insert_advertisement['content_id'] = $this->input->post('content_id');
                    $insert_advertisement['product_id'] = $this->input->post('product_id');
                    $insert_advertisement['external_url'] = $this->input->post('external_url');
                    $insert_advertisement['advertisement_link_opens'] = $this->input->post('advertisement_link_opens');
                    $insert_advertisement['advertisement_description'] = $this->input->post('advertisement_description');
                    $insert_advertisement['publish_date'] = $this->input->post('publish_date');
                    $insert_advertisement['unpublish_date'] = $this->input->post('unpublish_date');
                    $insert_advertisement['status'] = $this->input->post('status');

                    if ($id == 0) {
                        $insert_advertisement['created_by'] = current_admin_id();
                        $insert_advertisement['created_date'] = get_now();

                        if ($this->db->insert($this->advertisement, $insert_advertisement)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }
                    } else {
                        $insert_advertisement['updated_by'] = current_admin_id();
                        $insert_advertisement['updated_date'] = get_now();

                        $this->db->where('id', $id);
                        if ($this->db->update($this->advertisement, $insert_advertisement)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }
                    }
                    flash_redirect($this->header['page_name'], $id);
                } else {
                    $error_mess = $error;
                }
            }
        }
        $data = $this->_format_data($id);
        if (strtoupper($this->global_model->get_single_data("site", "site_name", $siteId)) == "WINBIZ") {
            $data['advertisement_positions'] = $this->adv->get_advertisement_position();
        } else {
            $positions = $this->adv->get_advertisement_position();
            foreach ($positions as $position) {
                if ($position->advertisement_position_value != 'right_top_position' && $position->advertisement_position_value != 'right_center_position' && $position->advertisement_position_value != 'right_bottom_position') {
                    $data['advertisement_positions'][] = $position;
                }
            }
        }

        $data['siteId'] = $siteId;
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/add_edit_advertisement', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post('save') == "true") {
            $data['id'] = set_value('id');
            $data['site_id'] = set_value('site_id');
            $data['advertisement_position_id'] = set_value('advertisement_position_id');
            $data['advertisement_name'] = set_value('advertisement_name');
            $data['advertisement_image'] = set_value('advertisement_image');
            $data['advertisement_image_size'] = $this->input->post('advertisement_image_size');
            $data['advertisement_link_type'] = set_value('advertisement_link_type');
            $data['content_id'] = set_value('content_id');
            $data['product_id'] = set_value('product_id');
            $data['external_url'] = set_value('external_url');
            $data['advertisement_link_opens'] = set_value('advertisement_link_opens');
            $data['advertisement_description'] = html_entity_decode(set_value('advertisement_description'));
            $data['publish_date'] = $this->input->post('publish_date');
            $data['unpublish_date'] = $this->input->post('unpublish_date');
            $data['status'] = set_value('status');
        } else if ($id != 0) {
            $row = $this->adv->get_advertisements($id);
            $data['id'] = $row->id;
            $data['site_id'] = $row->site_id;
            $data['ent_id'] = $row->enterprise_id;
            $data['advertisement_position_id'] = $row->advertisement_position_id;
            $data['advertisement_name'] = $row->advertisement_name;
            $data['advertisement_image'] = $row->advertisement_image;
            $data['advertisement_image_size'] = $row->advertisement_size;
            $data['advertisement_link_type'] = $row->advertisement_link_type;
            $data['content_id'] = $row->content_id;
            $data['product_id'] = $row->product_id;
            $data['external_url'] = $row->external_url;
            $data['advertisement_link_opens'] = $row->advertisement_link_opens;
            $data['advertisement_description'] = $row->advertisement_description;
            $data['publish_date'] = $row->publish_date;
            $data['unpublish_date'] = $row->unpublish_date;
            $data['status'] = $row->status;
            $data['created_by'] = $row->created_by;
            $data['created_date'] = $row->created_date;
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = $row->updated_date;
        } else {
            $data['id'] = '';
            $data['site_id'] = '1';
            $data['advertisement_position_id'] = '';
            $data['advertisement_name'] = '';
            $data['advertisement_image'] = '';
            $data['advertisement_image_size'] = '';
            $data['advertisement_link_type'] = '';
            $data['content_id'] = '';
            $data['product_id'] = '';
            $data['external_url'] = '';
            $data['advertisement_link_opens'] = '';
            $data['advertisement_description'] = '';
            $data['publish_date'] = '';
            $data['unpublish_date'] = '';
            $data['status'] = '';
        }

        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->advertisement, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->advertisement, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        //check whether it is used in other tables or not	
        $this->load->library('restrict_delete');
        $params = "";  //change it later
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selectd_id) {
                    $file = $this->global_model->get_single_data_ad($this->advertisement, 'advertisement_image', $selectd_id);
                    $file = './uploaded_files/bigyapan/' . $file;
                    $this->db->where('id', $selectd_id);
                    $this->db->delete($this->advertisement);
                    if ($this->db->affected_rows() > 0) {
                        $deleted++;
                        if (file_exists($file))
                            unlink($file);
                    }
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $file = $this->global_model->get_single_data_ad($this->advertisement, 'advertisement_image', $id);
                $file = './uploaded_files/bigyapan/' . $file;
                $this->db->where('id', $id);
                $this->db->delete($this->advertisement);
                if ($this->db->affected_rows() > 0) {
                    if (file_exists($file))
                        unlink($file);
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system.';
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

}

?>