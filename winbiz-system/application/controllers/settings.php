<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Settings extends CI_Controller {
		public $header = array();
		var $table_name = 'globalsetting';
		
		function __Construct()
		{
			parent::__Construct();
			
			$this->load->helper('administrator');
			is_already_logged_in();
			
			$this->header['title']			= "Global Settings";
			$this->header['page_name']		= $this->router->fetch_class();
			
			$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
			$this->header['head_scripts'] = array("js/jquery-1.8.2.min.js");
            //$this->header['scripts']      = array("bootstrap/js/bootstrap.min.js", "js/jquery.blockui.js","data-tables/jquery.dataTables.js","data-tables/DT_bootstrap.js","uniform/jquery.uniform.min.js","fancybox/source/jquery.fancybox.pack.js","js/app.js");		
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
			$this->data['settings'] = TRUE;			//menu
			
			$this->load->library('custom_pagination');		//add this
		}
		
		function index($id = NULL)
		{	
			$id = (int)$id;			
			$this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
			$this->load->library('form_validation');
			if ($this->input->post())
			{
				$this->form_validation->set_rules('multipleSites', 'Multiple Site Enable/Disable', "trim|required|fixed_values[yes,no]");
				if($this->input->post('multipleSites') == "yes")
				{
					$this->form_validation->set_rules('siteLimits', 'Site Limit', "trim|required|numeric");
					$this->form_validation->set_rules('siteStructure', 'Site Structure', "trim|required|fixed_values[sub_domain,folder]");
				}				
				$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
				
				if ($this->form_validation->run() == TRUE)
				{
					$row = $this->administrator_model->get_data('globalsetting', 1);
					if($row)
					{
						$id = $row->id;
					}
					else
					{
						$id = 0;
					}
					
					$no_multiple = '';
					$site_count = $this->db->count_all('site');
					if( $site_count > 1 && $this->input->post('multipleSites') == "no")
					{
						$insert_data['multiple_sites']	= 'yes';
						$no_multiple = 'Please delete other sites to change the status of multiple sites.';
					}
					else
					{
						$insert_data['multiple_sites']	= $this->input->post('multipleSites');		
					}					
					
					if($this->input->post('siteLimits') < $site_count)
					{
						$insert_data['site_limits']		= $site_count;
					}
					else
					{
						$insert_data['site_limits']		= $this->input->post('siteLimits');
					}
									
					$insert_data['site_structure']	= $this->input->post('siteStructure');					
					$insert_data['user_restriction']= $this->input->post('userRestriction');
										
					if($id == 0)	//insert
					{
						$actRes = $this->db->insert($this->table_name, $insert_data);
						
						$this->session->set_flashdata('class', 'success');
						$this->session->set_flashdata('msg', "New data added successfully. $no_multiple");
					}
					else
					{	
						$this->db->where('id', $id);
						$actRes = $this->db->update($this->table_name, $insert_data);
						
						$this->session->set_flashdata('class', 'success');
						$this->session->set_flashdata('msg', "Data updated successfully. $no_multiple");						
					}
					$this->_createDynamicRouting($insert_data['site_structure'], $insert_data['multiple_sites']);
					redirect($this->header['page_name']);
				}
			}
			$data = $this->_format_data($id);	
			$this->load->view('header', $this->header);
			$this->load->view('menu', $this->data);		
			$this->load->view('settings/form', $data);
			$this->load->view('footer');
		}
		
		function _createDynamicRouting($structure = "", $multipleSites)
		{
			if($structure == "folder" && $multipleSites == "yes")
			{
				$my_file = 'application/config/dynamicrouting.route';
				chmod($my_file, 0777);
				$handle = fopen($my_file, 'w');
				$phpTagOpen = '<?php'."\n";
				fwrite($handle, $phpTagOpen);
				$this->db->where('main_site', 0);
				$sites = $this->db->get('site')->result();
				if(!empty($sites) && is_array($sites)){
					foreach($sites as $ind=>$site){
						if(!empty($site->site_name)){
							$data = '//Routing Rule For '.$site->site_title."\n";
							fwrite($handle, $data);
							$data = '$route[\''.$site->site_name.'\'] = \'home\';'."\n";
							fwrite($handle, $data);
							$data = '$route[\''.$site->site_name.'/(.*)\'] = "$1";'."\n\n\n";
							fwrite($handle, $data);
						}
					}					
				}
				$phpTagClose = "\n".'?>';
				fwrite($handle, $phpTagClose);
				fclose($handle);
				/*
				 * $route['cms2'] = "home";
				   $route['cms2/(.*)'] = "$1";
				*/
			}else{
				$my_file = 'application/config/dynamicrouting.route';
				$handle = fopen($my_file, 'w');
				$phpTagOpen = '<?php'."\n".'?>';
				fwrite($handle, $phpTagOpen);
				fclose($handle);
			}
		}
		
		function _format_data($id)
		{
			$data['readonly']		= '';
			if ($this->input->post())
			{
				$data['settingId']			= set_value('settingId');
				$data['multipleSites']		= set_value('multipleSites');
				$data['siteStructure']		= set_value('siteStructure');
				$data['siteLimits']			= set_value('siteLimits');
				$data['userRestriction']	= set_value('userRestriction');
				$data['saEmail']			= set_value('saEmail');
				$data['saUsernme']			= set_value('saUsernme');
			}else{
				$row = $this->administrator_model->get_data('globalsetting', 1);
				if($row)
				{
					$data['settingId']			= $row->id;
					$data['multipleSites']		= $row->multiple_sites;
					$data['siteStructure']		= $row->site_structure;
					$data['siteLimits']			= $row->site_limits;
					$data['userRestriction']	= $row->user_restriction;
					$data['saEmail']			= '';
					$data['saUsernme']			= '';
				}else{
					$data['settingId']			= '';
					$data['multipleSites']		= 'no';
					$data['siteStructure']		= 'sub_domain';
					$data['siteLimits']			= '1';
					$data['userRestriction']	= 'yes';
					$data['saEmail']			= '';
					$data['saUsernme']			= '';
				}
			}					
			return $data;			
		}
		
		function delete($id = NULL)
		{
			$this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
		
			$id = (int)$id;
			$row = $this->admin_user_model->get_profiles($id);
			if(count($row) == 0 )
			{
				$action['class'] = 'error';
				$action['msg'] = 'Invalid Request!';
			}
			else
			{
				//check whether it is used in other tables or not	
				$this->load->library('restrict_delete');
				$params = "admin_user.profile_id";
				
				if($this->restrict_delete->check_for_delete($params, $id))
				{
					$this->db->where('id', $id);
					$this->db->delete($this->profile);
					
					if($this->db->affected_rows() > 0) {
						$this->db->where('profile_id', $id);
						$this->db->delete($this->profile_detail);
						
						$action['class'] = 'success';
						$action['msg'] = 'Data deleted successfully!';
					} else {
						$action['class'] = 'error';
						$action['msg'] = 'Error in deleting data!';
					}
				}
				else 
				{
					$action['class'] = 'error';
					$action['msg'] = 'This data cannot be deleted. It is being used in system.';
				}
			}
			$this->session->set_flashdata($action);
			redirect($this->header['page_name']);
		}
	}
