<?php

//check_loggedin_user();
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $users = 'users';

    public function __construct() {
        parent::__construct();

        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Frontend Users";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue", "facebox");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['users'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('user_model');
        $this->load->model('general_model', 'general');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->user_model->get_user());
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $data['rows'] = $this->user_model->get_user(0, TRUE, $start, 'id');
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('ecommerce/view_users_list', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
//        dumparray($_POST);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->users, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->users, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id) {
        set_message('User Deleted sucessfully', 'sucess');
        delete('users', $id);
        delete('users_profile', $id);
        redirect('admin/user');
    }

    function show($id) {
        $data['title'] = 'User Details';
        $data['breadcrumb'] = array('Dashboard' => 'dashboard', $this->title => $this->module, 'User info' => '');
        $record = $this->user_model->get_user_detail($id);

        $coupons = $this->coupon->get_coupon_by_user($id);

//     dumparray($coupons);
        $data['button'] = 'Go Back! ';
        $data['datas'] = $record;




        $data['coupons'] = $coupons;
        $this->template->load('template', 'user_info', $data);
    }

}
