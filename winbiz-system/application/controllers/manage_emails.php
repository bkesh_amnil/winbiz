<?php
    class Manage_emails extends CI_Controller {
        public $header = array();
        var $module = 'module';
        var $profile = 'profile';
        var $profile_detail = 'profile_detail';
        var $email_management = 'email_management';
        var $enterprise = 'enterprises';
	
        function __Construct(){
            parent::__Construct();
            $this->load->helper('administrator');
            is_already_logged_in();
            $this->header['title']		= "Email Management";
            $this->header['page_name']	= $this->router->fetch_class();
            $this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
            $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
            $this->data['menu_cms']         = TRUE;
            $this->header['email_management']  = TRUE;
            $this->load->library('custom_pagination');
            $this->load->model('admin_email_management_model', 'emanage');
            $this->load->model('admin_enterprise_model', 'ent');
        }
	
        function index(){	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
            $start = $this->uri->segment(3);
            $total_rows = count($this->emanage->get_email_managements());
            $config = $this->custom_pagination->admin_configuration();
            $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
            $config['total_rows'] = $total_rows;
            $data['start'] = $start;
            $data['rows'] = $this->emanage->get_email_managements(0, TRUE, $start, 'id');
            $this->load->view('header', $this->header);
            $this->load->view('cms/sub_menu');
            $this->load->view('menu', $this->data);						
            $this->load->view('cms/view_email_management', $data);
            $this->load->view('action');			
            $this->load->view('footer'); 
        }
	
        function form($id = NULL){
            $this->load->helper('ckeditor');
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id); 
            $siteId = intval($this->input->get('site'));
            if( $id != 0 ){
                $row = $this->emanage->get_email_managements($id);
                $siteId = $row->site_id;
            }else if(empty($siteId)){
                $this->session->set_flashdata('class', 'error');
                $this->session->set_flashdata('msg', "Please Select Site To Add Email Management For.");
                redirect('manage_emails/');
            }

            $this->header['con_title'] = "Add Email Management For ".strtoupper($this->global_model->get_single_data("site", "site_name", $siteId)); 
            $id = (int)$id;			
            $error_mess = '';
            $this->load->library('form_validation');
            if($this->input->post('save') == "true"){
                $this->form_validation->set_rules('site_id', 'Site Id', "trim|required|integer");
                $this->form_validation->set_rules('enterprise_name', 'Enterprise Name', "trim|required|xss_clean");
                $this->form_validation->set_rules('emails', 'Emails', "trim|required|xss_clean|valid_emails");
                $this->form_validation->set_rules('status', 'Status', "trim|required|fixed_values[1,0]");			
                $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');				
                if ($this->form_validation->run() == TRUE){
                    $insert_email_management['site_id']			 = $this->input->post('site_id');
                    $insert_email_management['enterprise_id']	 = $this->ent->get_enterprise_id_or_name($this->input->post('enterprise_name'), 0);
                    $insert_email_management['emails']			 = $this->input->post('emails');
                    $insert_email_management['status']			 = $this->input->post('status');
                    if($id == 0){
                        $insert_email_management['created_by'] = current_admin_id(); 
                        $insert_email_management['created_date'] = get_now();

                        if($this->db->insert($this->email_management, $insert_email_management)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }
                    } else{
                        $insert_email_management['updated_by']		= current_admin_id(); 
                        $insert_email_management['updated_date']	= get_now();
                        
                        $this->db->where('id', $id);
                        if($this->db->update($this->email_management, $insert_email_management)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }
                    }
                    flash_redirect($this->header['page_name'], $id);
                }
            }
            $data = $this->_format_data($id);			
            $data['siteId'] = $siteId;	
            $data['error_mess'] = $error_mess;
            $data['profile_enterprise_name'] = $this->global_model->get_single_data("site", "site_title", $siteId);
            $this->load->view('header', $this->header);
            $this->load->view('cms/sub_menu');
            $this->load->view('menu', $this->data);		
            $this->load->view('cms/add_edit_email_management', $data);
            $this->load->view('footer');
        }
	
        function _format_data($id){
            if($this->input->post('save') == "true"){
                $data['id']               = set_value('id');
                $data['site_id']          = set_value('site_id');
                $data['enterprise_name']  = set_value('enterprise_id');
                $data['emails']			  = set_value('emails');
                $data['status']			  = set_value('status');
            }else if($id != 0){
                $row = $this->emanage->get_email_managements($id);
                $ent_name = $this->ent->get_enterprise_id_or_name('', $row->enterprise_id) ;
                $data['id']	              = $row->id;
                $data['site_id']		  = $row->site_id;
                $data['enterprise_name']   = $ent_name;
                $data['emails']           = $row->emails;
                $data['status']           = $row->status;
                $data['created_by']       = $row->created_by;
                $data['created_date']     = $row->created_date;
                $data['updated_by']       = $row->updated_by;
                $data['updated_date']     = $row->updated_date;
            }else{
                $data['id']               = '';
                $data['site_id']		  = '1';
                $data['enterprise_name']  = '';
                $data['emails']			  = '';
                $data['status']		      = '';
                $data['created_by']       = '';
                $data['created_date']	  = 'None';
                $data['updated_by']       = '';
                $data['updated_date']	  = '';
            }
            $ent_info = $this->admin_user_model->get_user_enterprise_info(current_admin_id());
            if(!empty($ent_info)) {
                $data['profile_enterprise_name'] = $ent_info->enterprise_name;
            }
            return $data;	
        }

        function change_status($status = '', $id = NULL){
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            if($this->input->post('selected')){
                $data = array('status' => $status);
                $selected_ids = $this->input->post('selected');
                $changed = 0;
                foreach($selected_ids as $selectd_id){
                    $this->db->where('id', $selectd_id);
                    $this->db->update($this->email_management, $data);
                    if($this->db->affected_rows() > 0) {
                        $changed++; 
                        $id = $selectd_id;      
                    }
                }
                if($changed){
                    $action['class'] = 'success';
                    $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }               
            }else {
                $data = array('status' => $status);
                $this->db->where('id', $id);
                $this->db->update($this->email_management, $data);
                if($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Status changed successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name'], $id);
        }
	
        function delete($id = NULL){
			$this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
			$id = (int)$id;
			//check whether it is used in other tables or not	
			$this->load->library('restrict_delete');
			$params = "";		//change it later
			if($this->restrict_delete->check_for_delete($params, $id)){
                if($this->input->post('selected')){
                    $selected_ids = $this->input->post('selected');
                    $deleted = 0;
                    foreach($selected_ids as $selectd_id){
                        $file = $this->global_model->get_single_data_ad($this->email_management, 'email_management_image', $selectd_id);
                        $file = './uploaded_files/email_management/' . $file;
                        $this->db->where('id', $selectd_id);
                        $this->db->delete($this->email_management);
                        if($this->db->affected_rows() > 0) {
                            $deleted++;						
                            if(file_exists($file))
                                unlink($file);
                        }
                    }
                    if($deleted){
                        $action['class'] = 'success';
                        $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                    }else {
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }else{
                    $file = $this->global_model->get_single_data_ad($this->email_management, 'email_management_image', $id);
                    $file = './uploaded_files/email_management/' . $file;
                    $this->db->where('id', $id);
                    $this->db->delete($this->email_management);
                    if($this->db->affected_rows() > 0) {
                        if(file_exists($file))
                            unlink($file);
                        $action['class'] = 'success';
                        $action['msg'] = 'Data deleted successfully!';
                    }else {
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }
			}else {
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
			}
			$this->session->set_flashdata($action);
			flash_redirect($this->header['page_name']);
        }

    }
?>