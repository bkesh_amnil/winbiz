<?php

class Enterprise extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $enterprise = 'enterprises';
    var $enterprise_count = 'enterprise_count';
    var $enterprise_category_list = 'enterprise_category_list';
    var $site = 'site';
    var $site_menus = 'site_menus';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Enterprise Management";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js", "scripts/jquery.textarea-counter.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['enterprise'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('admin_enterprise_model', 'ent');
        $this->load->model('admin_enterprise_category_model', 'ent_category');
        $this->load->model('manager/admin_domain_theme_model', 'dom_theme');
        $this->load->model('admin_enterprise_category_list_model', 'ent_cat_list');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->ent->get_enterprises());
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $rows = $this->ent->get_enterprises(0, TRUE, $start, 'id');
        foreach ($rows['enterprise'] as $key => $row) {
            $data['rows'][$key] = $row;
            foreach ($rows['category'] as $category) {
                if ($row->id == $category->enterprise_id) {
                    $name[] = $category->enterprise_category_name;
                    $id[] = $category->enterprise_category_id;
                }
            }
            $data['rows'][$key]->enterprise_category_name = implode(',', $name);
            $data['rows'][$key]->enterprise_category_id = implode(',', $id);
            unset($name);
            unset($id);
        }
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('enterprise/view_enterprise', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $this->load->helper('ckeditor');
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $siteId = intval($this->input->get('site'));
        if ($id != 0) {
            $row = $this->ent->get_enterprises($id);
            $siteId = $row->site_id;
        }/* else if(empty($siteId)){
          $this->session->set_flashdata('class', 'error');
          $this->session->set_flashdata('msg', "Please Select Site To Add Enterprise For.");
          redirect('enterprise/');
          } */
        $this->header['con_title'] = "Add Enterprise For " . strtoupper($this->global_model->get_single_data("site", "site_name", $siteId));
        $id = (int) $id;
        $error_mess = '';
        $this->load->library('form_validation');
        if ($this->input->post('save') == "true") {
            $this->form_validation->set_rules('site_id', 'Site Id', "trim|required|integer");
            $this->form_validation->set_rules('enterprise_category_id', 'Enterprise Category', "required");
            $this->form_validation->set_rules('enterprise_name', 'Enterprise Name', "trim|required|xss_clean|unique[" . $this->enterprise . ".enterprise_name.$id]");
            $this->form_validation->set_rules('enterprise_alias', 'Enterprise Alias', "trim|required|xss_clean|unique[" . $this->enterprise . ".enterprise_alias.$id]");
            $this->form_validation->set_rules('status', 'Status', "trim|required|fixed_values[1,0]");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $width = 180;
                $height = 105;
                $imgUpload = false;
                $oldEnterprise = $this->input->post('uploaded_enterprise_old');
                if (/* $id == 0 || */
                        !empty($_FILES['enterprise_logo']['name'])
                ) {
                    if (!empty($_FILES['enterprise_logo']['name']) && $_FILES['enterprise_logo']['error'] == 0) {
                        $filename = $_FILES['enterprise_logo']['name'];
                        $filesize_image = $_FILES['enterprise_logo']['size'];
                        $file = $_FILES['enterprise_logo']['tmp_name'];
                        $file_size = getimagesize($file);
                        $file_width = $file_size[0];
                        $file_height = $file_size[1];
                        $allowed_ext = "jpg,jpeg,gif,png,bmp";
                        $imgInfo = getimagesize($file);
                        $file_type = image_type_to_mime_type(exif_imagetype($file));
                        $allowed_ext = preg_split("/\,/", $allowed_ext);
                        $maxSize = 1024 * 8;
                        $minW = $width;
                        $minH = $height;
                        $for = "enterprise";
                        $uploadPath = "./uploaded_files/enterprise/";

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath);
                            chmod($uploadPath, 0777);
                        }
                        if ($filesize_image > ($maxSize * 1024)) {
                            $error = "Image size is too large to accept. Image Size should be less than " . $maxSize . " KB";
                        } else if ($file_width < $width || $file_height < $height) {
                            $error = "Image size is small. Image Size should be greater than or equal to " . $width . "X" . $height . " for the position.";
                        } else {
                            switch ($file_type) {
                                case 'image/gif':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".gif";
                                    $upltype = "gif";
                                    break;
                                case 'image/jpeg':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                    $upltype = "jpg";
                                    break;
                                case 'image/jpg':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                    $upltype = "jpg";
                                    break;
                                case 'image/png':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".png";
                                    $upltype = "png";
                                    break;
                                default:
                                    $fileUploadName = "";
                                    break;
                            }
                            if (empty($fileUploadName)) {
                                $error = "Unrecognized File Type";
                            } else {
                                require_once './phpthumb/ThumbLib.inc.php';
                                $thumb = PhpThumbFactory::create($file);
                                $thumb->adaptiveResize($minW, $minH);
                                $saved = $thumb->save($uploadPath . "/" . $fileUploadName, $upltype);
                                if ($saved) {
                                    $imgUpload = true;
                                    $image = $fileUploadName;
                                }
                            }
                        }
                        if (!$imgUpload) {
                            $uplErr = "";
                            if (!empty($error)) {
                                $uplErr = $error;
                            }
                            $error = "Image was not uploaded.";
                            $error = $error . " " . $uplErr;
                        }
                    } else {
                        $error = "Select Image For Enterprise";
                    }
                } else {
                    /* if($id == 0)
                      $error_mess = 'Enterprise Image is Compulsory'; */
                    $fileUploadName = $oldEnterprise;
                }

                if (!empty($oldEnterprise) && !empty($imgUpload)) {
                    if (file_exists("./uploaded_files/enterprise/" . $oldEnterprise) && !is_dir("./uploaded_files/enterprise/" . $oldEnterprise)) {
                        unlink("./uploaded_files/enterprise/" . $oldEnterprise);
                    }
                } else if (!empty($oldEnterprise)) {
                    $image = $oldEnterprise;
                }

                if (empty($error)) {
                    $insert_enterprise['site_id'] = $this->input->post('site_id');
                    $insert_site['site_title'] = $insert_enterprise['enterprise_name'] = $this->input->post('enterprise_name');
                    $insert_site['site_name'] = $insert_enterprise['enterprise_alias'] = $this->input->post('enterprise_alias');
                    $insert_enterprise['enterprise_logo'] = !empty($image) ? $image : '';
                    $insert_enterprise['enterprise_website'] = $this->input->post('enterprise_website');
                    $insert_enterprise['enterprise_short_description'] = $this->input->post('enterprise_short_description');
                    $insert_enterprise['enterprise_description'] = $this->input->post('enterprise_description');
                    $insert_site['facebook_url'] = $insert_enterprise['enterprise_facebook_link'] = $this->input->post('enterprise_facebook_link');
                    $insert_site['twitter_url'] = $insert_enterprise['enterprise_twitter_link'] = $this->input->post('enterprise_twitter_link');
                    $insert_site['linkedin_url'] = $insert_enterprise['enterprise_linkedin_link'] = $this->input->post('enterprise_linkedin_link');
                    $insert_site['youtube_url'] = $insert_enterprise['enterprise_youtube_link'] = $this->input->post('enterprise_youtube_link');
                    $insert_enterprise['status'] = $this->input->post('status');

                    if ($id == 0) {
                        $insert_enterprise['created_by'] = current_admin_id();
                        $insert_enterprise['created_date'] = get_now();

                        if ($this->db->insert($this->enterprise, $insert_enterprise)) {
                            $alias = $insert_enterprise['enterprise_alias'];
                            $query = "SELECT id FROM tbl_enterprises WHERE enterprise_alias ='$alias'";
                            $result = $this->db->query($query)->row();
                            $insert_count['site_id'] = $this->input->post('site_id');
                            $insert_count['enterprise_id'] = $result->id;
                            $insert_count['count'] = '0';
                            $this->db->insert($this->enterprise_count, $insert_count);

                            foreach ($this->input->post('enterprise_category_id') as $category_id) {
                                $insert_enterprise_category['enterprise_category_id'] = $category_id;
                                $insert_enterprise_category['enterprise_id'] = $result->id;
                                $this->db->insert($this->enterprise_category_list, $insert_enterprise_category);
                            }

                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }

                        flash_redirect($this->header['page_name'] . '/form?site=' . $siteId, '');
                    } else {
                        $insert_enterprise['updated_by'] = current_admin_id();
                        $insert_enterprise['updated_date'] = get_now();

                        $this->db->where('id', $id);
                        if ($this->db->update($this->enterprise, $insert_enterprise)) {

                            if (isset($_FILES['enterprise_logo']['name']) && !empty($_FILES['enterprise_logo']['name'])) {
                                if (file_exists('uploaded_files/enterprise/' . $image)) {
                                    chmod("uploaded_files/enterprise/" . $image, 0777);
                                    if (copy("./uploaded_files/enterprise/" . $image, "./uploaded_files/site_logo/" . $image)) {
                                        chmod('uploaded_files/site_logo/' . $image, 0777);
                                        unlink("./uploaded_files/site_logo/" . $oldEnterprise);
                                    }
                                }
                            }

                            $insert_site['site_logo'] = !empty($image) ? $image : '';

                            $this->db->where('enterprise_id', $id);

                            $this->db->delete($this->enterprise_category_list, ['enterprise_id' => $id]);
                            foreach ($this->input->post('enterprise_category_id') as $category_id) {
                                $insert_enterprise_category['enterprise_category_id'] = $category_id;
                                $insert_enterprise_category['enterprise_id'] = $id;
                                $this->db->insert($this->enterprise_category_list, $insert_enterprise_category);
                            }


                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }

                        flash_redirect($this->header['page_name'] . '/form/' . $id, '');
                    }
                    //flash_redirect($this->header['page_name'], $id);
                } else {
                    $error_mess = $error;
                }
            }
        }
        $data = $this->_format_data($id);
        $data['siteId'] = $siteId;
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('enterprise/add_edit_enterprise', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post('save') == "true") {
            $data['id'] = set_value('id');
            $data['site_id'] = set_value('site_id');
            $data['enterprise_category_id'] = set_value('enterprise_category_id');
            $data['enterprise_name'] = set_value('enterprise_name');
            $data['enterprise_alias'] = set_value('enterprise_alias');
            $data['enterprise_logo'] = set_value('enterprise_logo');
            $data['enterprise_website'] = set_value('enterprise_website');
            $data['enterprise_short_description'] = html_entity_decode(set_value('enterprise_short_description'));
            $data['enterprise_description'] = html_entity_decode(set_value('enter'));
            $data['enterprise_facebook_link'] = set_value('enterprise_facebook_link');
            $data['enterprise_twitter_link'] = set_value('enterprise_twitter_link');
            $data['enterprise_linkedin_link'] = set_value('enterprise_linkedin_link');
            $data['enterprise_youtube_link'] = set_value('enterprise_youtube_link');
            $data['status'] = set_value('status');
            $data['domain_theme_id'] = set_value('domain_theme_id');
            $data['domain_name'] = set_value('domain_name');
        } else if ($id != 0) {
            $row = $this->ent->get_enterprises($id);
            $enterprise_category = $this->ent_cat_list->get($id);
            $data['id'] = $row->id;
            $data['site_id'] = $row->site_id;
            $data['enterprise_category_id'] = $enterprise_category;
            $data['enterprise_name'] = $row->enterprise_name;
            $data['enterprise_alias'] = $row->enterprise_alias;
            $data['enterprise_logo'] = $row->enterprise_logo;
            $data['enterprise_website'] = $row->enterprise_website;
            $data['enterprise_short_description'] = $row->enterprise_short_description;
            $data['enterprise_description'] = $row->enterprise_description;
            $data['enterprise_facebook_link'] = $row->enterprise_facebook_link;
            $data['enterprise_twitter_link'] = $row->enterprise_twitter_link;
            $data['enterprise_linkedin_link'] = $row->enterprise_linkedin_link;
            $data['enterprise_youtube_link'] = $row->enterprise_youtube_link;
            $data['status'] = $row->status;
            $data['created_by'] = $row->created_by;
            $data['created_date'] = $row->created_date;
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = $row->updated_date;
            $data['domain_theme_id'] = $row->domain_theme_id;
            $data['domain_name'] = $row->domain_name;
        } else {
            $data['id'] = '';
            $data['site_id'] = '1';
            $data['enterprise_category_id'] = '';
            $data['enterprise_name'] = '';
            $data['enterprise_alias'] = '';
            $data['enterprise_alias'] = '';
            $data['enterprise_logo'] = '';
            $data['enterprise_website'] = '';
            $data['enterprise_short_description'] = '';
            $data['enterprise_description'] = '';
            $data['enterprise_facebook_link'] = '';
            $data['enterprise_twitter_link'] = '';
            $data['enterprise_linkedin_link'] = '';
            $data['enterprise_youtube_link'] = '';
            $data['status'] = '';
            $data['domain_theme_id'] = '';
            $data['domain_name'] = '';
        }
        $data['enterprise_categories'] = $this->ent_category->get_all_enterprise_categories();
        $ent_info = $this->admin_user_model->get_user_enterprise_info(current_admin_id());
        if (!empty($ent_info)) {
            $data['profile_enterprise_cat_id'] = $ent_info->ent_cat_id;
        }
        $data['domain_themes'] = $this->dom_theme->get_all_domain_themes();
        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->enterprise, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->enterprise, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        //check whether it is used in other tables or not
        $this->load->library('restrict_delete');
        $params = "";        //change it later
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selectd_id) {
                    $file = $this->global_model->get_single_data_ad($this->enterprise, 'enterprise_logo', $selectd_id);
                    $file = './uploaded_files/enterprise/' . $file;
                    $this->db->where('id', $selectd_id);
                    $this->db->delete($this->enterprise);
                    if ($this->db->affected_rows() > 0) {
                        $deleted++;
                        if (file_exists($file))
                            unlink($file);
                    }
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $file = $this->global_model->get_single_data_ad($this->enterprise, 'enterprise_logo', $id);
                $this->db->where('id', $id);
                $this->db->delete($this->enterprise);
                if ($this->db->affected_rows() > 0) {
                    if (file_exists($file))
                        $file = './uploaded_files/enterprise/' . $file;
                    unlink($file);
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system.';
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function autocomplete() {
        $q = strtolower($this->input->get("q"));
        $this->db->select('id, enterprise_name');
        $this->db->where("LOWER(enterprise_name) LIKE '" . $q . "%'");
        $result = $this->db->get($this->enterprise)->result();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $key => $value) {
                echo $value->enterprise_name . "|" . $value->enterprise_name . "\n";
            }
        }
    }

    function apply_theme() {
        $post = $_POST;
        $enterprise_id = $post['enterprise_id'];
        $theme_id = $post['theme_id'];

        $data['action'] = 'error';
        $data['msg'] = 'Error occured';

        $insert_data['domain_theme_id'] = $theme_id;
        $this->db->where('id', $enterprise_id);
        $this->db->update($this->enterprise, $insert_data);

        if ($this->db->affected_rows() > 0) {
            $data['action'] = 'success';
            $data['msg'] = 'Theme applied successfully';
        }

        echo json_encode($data);
    }

    function save_domain() {
        $post = $_POST;
        $enterprise_id = $post['enterprise_id'];
        $domain_name = $post['domain_name'];

        $data['action'] = 'error';
        $data['msg'] = 'Error occured';
        $insert = '1';

        $this->db->trans_begin();

        $this->db->select('domain_name');
        $this->db->where('id', $enterprise_id);
        $domain_row = $this->db->get($this->enterprise)->row();

        if (isset($domain_row) && empty($domain_row->domain_name)) {
            $insert = '0';
        }

        $insert_data['domain_name'] = $domain_name;
        $this->db->where('id', $enterprise_id);
        $this->db->update($this->enterprise, $insert_data);

        if ($this->db->affected_rows() > 0) {
            $row = $this->ent->get_enterprises($enterprise_id);
            $insert_site['site_title'] = $row->enterprise_name;
            $insert_site['site_name'] = $row->enterprise_alias;
            $insert_site['main_site'] = '0';
            $insert_site['enterprise_id'] = $enterprise_id;
            $insert_site['sub_domain'] = $domain_name;
            $insert_site['site_offline'] = 'no';
            $insert_site['site_offline_msg'] = 'The site is currently underconstruction. We will be back soon.';
            $insert_site['site_logo'] = $row->enterprise_logo;
            $insert_site['facebook_url'] = $row->enterprise_facebook_link;
            $insert_site['twitter_url'] = $row->enterprise_twitter_link;
            $insert_site['youtube_url'] = $row->enterprise_youtube_link;
            $insert_site['linkedin_url'] = $row->enterprise_linkedin_link;
            $insert_site['instagram_url'] = '';

            if ($insert != '0') {
                $this->db->where('enterprise_id', $enterprise_id);
                $this->db->update($this->site, $insert_site);
            } else {
                $insert_site['modules_enabled'] = '15,51,9,11,49,18,17,47,40,54,45,43,7,4,10,50,46,55,48,41,6';

                $this->db->insert($this->site, $insert_site);

                if ($this->db->affected_rows() > 0) {
                    $site_id = $this->db->insert_id();
                    $insert_site_menus['site_id'] = $site_id;
                    $insert_site_menus['name'] = 'Top Menu';
                    $insert_site_menus['submenus'] = 'no';
                    $insert_site_menus['depth'] = '0';
                    $insert_site_menus['main_menu'] = 'yes';
                    $insert_site_menus['status'] = 'yes';

                    $this->db->insert($this->site_menus, $insert_site_menus);
                }
                if (file_exists('uploaded_files/enterprise/' . $row->enterprise_logo) && !is_dir('uploaded_files/enterprise/' . $row->enterprise_logo)) {
                    chmod("uploaded_files/enterprise/" . $row->enterprise_logo, 0777);
                    if (copy("./uploaded_files/enterprise/" . $row->enterprise_logo, "./uploaded_files/site_logo/" . $row->enterprise_logo)) {
                        chmod('uploaded_files/site_logo/' . $row->enterprise_logo, 0777);
                    }
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $data['action'] = 'success';
            $data['msg'] = 'Domain saved successfully';
        }

        echo json_encode($data);
    }

}

?>