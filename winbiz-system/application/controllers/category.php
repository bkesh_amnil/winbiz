<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

        class Category extends CI_Controller {
            public $header = array();
            var $module = 'module';
            var $profile = 'profile';
            var $profile_detail = 'profile_detail';
            var $category = 'categories';
		
            function __Construct(){
                parent::__Construct();
                $this->load->helper('administrator');
                is_already_logged_in();
                $this->header['title']		= "CMS Category Management";
                $this->header['page_name']	= $this->router->fetch_class();
                $this->header['stylesheets'] 	= array("960", "reset", "text", "blue","facebox");
                $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
                $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
                $this->data['menu_cms']         = TRUE;
                $this->header['category']       = TRUE;
                $this->load->library('custom_pagination');
                $this->load->model('cms/admin_cmscategory_model', 'cmscategory');
            }
            
            function index(){	
                $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
                $start = $this->uri->segment(3);
                $data['start'] = $start;
                $data['rows'] = $this->cmscategory->get_categories_list(0, "all");
                $this->load->view('header', $this->header);
                $this->load->view('menu', $this->data);						
                $this->load->view('cms/view_category', $data);	
                $this->load->view('action');		
                $this->load->view('footer');
            }
            
            function form($id = NULL){
                $id = (int)$id;	
                $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
                $siteId = intval($this->input->get('site'));
                if( $id != 0 ){
                    $row = $this->cmscategory->get_category($id);
                    //$siteId = $row->site_id;
                }
	            $this->header['title'] = "Add / Edit CMS Category"; 
                $id = (int)$id;	
                $error_mess = '';		
                $this->load->library('form_validation');
                if ($this->input->post()){
                    $this->form_validation->set_rules('parent_category', 'parent_category', "trim|integer");
                    $this->form_validation->set_rules('category_name', 'category_name', "trim|required|xss_clean");
                    $this->form_validation->set_rules('category_alias', 'category_alias', "trim|required|xss_clean|url_title|strtolower|unique[categories.category_alias.$id]");
                    $this->form_validation->set_rules('status', 'status', 'trim|required|fixed_values[1,0]');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
                    if ($this->form_validation->run() == TRUE){
                        $insert_data['category_name'] = $this->input->post('category_name');
                        $insert_data['category_alias'] = $this->input->post('category_alias');
                        $parent_cat = $this->input->post('parent_category');
                        if(!empty($parent_cat)){
                            $this->db->select('category_alias');
                            $this->db->where('id', $parent_cat);
                            $row = $this->db->get('categories')->row();
                            if($row->category_alias == 'uncategorized'){
                                $this->session->set_flashdata('class', 'error');
                                $this->session->set_flashdata('msg', 'Not Allowed to Add Category to Uncategorized');
                                flash_redirect(''.$this->header['page_name'], $id);
                            }else{
                                $insert_data['parent_category'] = $parent_cat;
                            }
                        }else
                            $insert_data['parent_category'] = 0;
                        $insert_data['status'] = $this->input->post('status');
                        if($id == 0){
                            $insert_data['created_by'] = current_admin_id();
                            $insert_data['created_date'] = get_now();
                            $this->db->insert($this->category, $insert_data);
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added Successfully');
                        }else{
                            $insert_data['updated_by'] = current_admin_id();
                            $insert_data['updated_date'] = get_now();
                            $this->db->where('id', $id);
                            $this->db->update($this->category, $insert_data);
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data Updated Successfully');
                        }
                        flash_redirect(''.$this->header['page_name'], $id);
                    }
                }
                $data = $this->_format_data($id);	
                $data['con_title'] = $this->header['title']; 
                $data['error_mess'] = $error_mess;
                $this->load->view('header', $this->header);
                $this->load->view('menu', $this->data);	
                $this->load->view('cms/add_edit_category', $data);
                $this->load->view('footer');
            }
		
            function _format_data($id){
                if($this->input->post()){
                    $data['id']			= set_value('id');
                    $data['category_name']	= set_value('category_name');
                    $data['category_alias']	= set_value('category_alias');
                    $data['parent_category']    = set_value('parent_category');
                    $data['status']		= set_value('status');
                    $data['created_by']         = set_value('created_bye');
                    $data['created_date']       = set_value('created_date');
                    $data['updated_by']         = set_value('updated_by');
                    $data['updated_date']       = set_value('updated_date');
                }elseif($id != 0){
                    $row = $this->cmscategory->get_categoryedit($id);
                    $data['id']			= $row->id;
                    $data['category_name']	= $row->category_name;
                    $data['category_alias']	= $row->category_alias;
                    $data['parent_category']    = $row->parent_category;
                    $data['status']		= $row->status;
                    $data['created_by']         = $row->created_by;
                    $data['created_date']       = $row->created_date;
                    $data['updated_by']         = $row->updated_by;
                    $data['updated_date']       = $row->updated_date;
                }else{
                    $data['id']                 = '';
                    $data['category_name']      = '';
                    $data['category_alias']     = '';
                    $data['parent_category']    = '';
                    $data['status']             = '';
                    $data['created_by']         = '';
                    $data['created_date']       = '';
                    $data['updated_by']         = '';
                    $data['updated_date']       = '';
                }
                $data['rows'] = $this->cmscategory->get_categories_list(0, "all");
                return $data;
            }
        
            function change_status($status = '', $id = NULL){ 
                $id = (int)$id;
                $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', 1);
                $data['status'] = ($status == '1') ? '1':'0'; 
                if($this->input->post('selected')){
                    $selected_ids = $this->input->post('selected');
                    $changed = 0;
                    foreach($selected_ids as $selected_id){
                        $this->db->where('id', $selected_id);
                        $this->db->update($this->category, $data);
                        if($this->db->affected_rows()>0){
                            $changed++;
                        }
                    }
                    if($changed){
                        $action['class'] = 'success';
                        $action['msg'] = $changed.' out of '. count($selected_ids).' status changed successfully!';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in changing Status';
                    }
                }else{
                    $this->db->where('id', $id);
                    $this->db->update($this->category, $data);
                    if($this->db->affected_rows()>0){
                        $action['class'] = 'success';
                        $action['msg'] = 'Status Changed Successfully';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in changing Status';
                    }
                }
                $this->session->set_flashdata($action);
                flash_redirect(''.$this->header['page_name'], $id);
            }
            
            function delete($id = NULL){
                $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
                $id  = (int)$id;
                $this->load->library('restrict_delete');
                $params = "";
                if($this->restrict_delete->check_for_delete($params, $id)){
                    if($this->input->post('selected')){
                       $selected_ids = $this->input->post('selected') ;
                       $deleted = 0;
                       foreach($selected_ids as $selected_id){
                           if($this->_delete_category($selected_id))
                               $deleted++;
                       }
                       if($deleted){
                           $action['class'] = 'success';
                           $action['msg'] = $deleted.' out of '.count($selected_ids).' data deleted successfully';
                       }else{
                           $action['class'] = 'error';
                           $action['msg'] = 'Error in Deleting Data';
                       }
                    }else{
                        if($this->_delete_category($id)){
                            $action['class'] = 'success';
                            $action['msg'] = 'Data Deleted Successfully';
                        }else{
                            $action['class'] = 'error';
                            $action['msg'] = 'Error in Deleting Data';
                        }
                    }
                }else{
                    $action['class'] = 'error';
                    $action['msg'] = 'This data cannot be deleted. It is being used in system.';
                }
                $this->session->set_flashdata($action);
                flash_redirect(''.$this->header['page_name'], $id);
            }
            
            function _delete_category($id){
                $this->db->where('id', $id);
                $this->db->delete($this->category);
                return true;
            }
       
            function autocomplete(){
                $q = strtolower($this->input->get("q"));
                $this->db->where("LOWER(category_name) LIKE '".$q."%'");
                $result = $this->db->get($this->category)->result();
                if(!empty($result) && is_array($result)){
                    foreach ($result as $key=>$value) {
                            echo $value->category_name."|".$value->category_name."\n";
                    }
                }
            }
            
	}