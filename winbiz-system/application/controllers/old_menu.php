﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Menu extends CI_Controller{
        public $header = array();
        var $module = 'module';
        var $profile = 'profile';
        var $profile_detail = 'profile_detail';
        var $menu = 'menu';
        
        function __Construct(){
            parent::__Construct();
            $this->load->helper('administrator');
            is_already_logged_in();
            $this->header['title']          = "Menu Management";
            $this->header['page_name']      = $this->router->fetch_class();
            $this->header['page_parent']    = get_class_parent($this->header['page_name']);
            $this->header['stylesheets']    = array("960", "reset", "text", "blue", "facebox");
            $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
            $this->data['menu_cms']         = TRUE;     //menu
            $this->header['menu']           = TRUE;     //submenu
            $this->load->library('custom_pagination');      //add this
            $this->load->model('cms/admin_menu_model', 'cms');
        }
        
        function index(){       
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
            $start = $this->uri->segment(3);
            $total_rows = count($this->cms->get_menu());                //change here
            $config = $this->custom_pagination->admin_configuration();
            $data['start'] = $start;
            $menuList = $this->cms->get_menu(0, TRUE, $start, 'menu_position'); 
            $data['rows'] = $this->cms->generateHtml($menuList, $this->header['page_name']);    //change here
            $sites  = $this->administrator_model->get_sites();
            $data['site_names'] = convert_to_dropdown($sites, 'site_title', 'id', 'All Sites', FALSE);
            $data['site_id']    = $this->input->get('site');
            if(count($data['site_names']) == 1){
                foreach($data['site_names'] as $index => $value)
                    $data['site_id'] = $index;
            }
            $menu_types = $this->administrator_model->get_menu_types($data['site_id']);         
            $data['menu_types'] = convert_to_dropdown($menu_types, 'name', 'id', 'All Menus', FALSE);           
            $data['menu_type']  = $this->input->get('menutype');
            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);                     
            $this->load->view('cms/view_menu', $data);
            $this->load->view('action');            
            $this->load->view('footer');
        }
        
        function form($id = NULL){
            $id = (int)$id;
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            $siteId = intval($this->input->get('site'));
            if( $id != 0 ){
                $row = $this->cms->get_menu($id);
                $siteId = $row->site_id;
            }
            $this->header['title'] = "Add Menu For ";
            $this->load->library('form_validation');
            if ($this->input->post('add')){ 
                $siteId = $this->input->post('site_id');
                $this->form_validation->set_rules('site_id', 'site', "required|exists[site.id]");
                $this->form_validation->set_rules('menu_type_id', 'menu type', "required|exists[site_menus.id]");
                $this->form_validation->set_rules('menu_title', 'menu title', "trim|required");
                $this->form_validation->set_rules('page_title', 'page title', "trim|required");
                $this->form_validation->set_rules('menu_heading', 'menu heading', "trim|required");
                $this->form_validation->set_rules('menu_alias', 'menu alias', "trim|strtolower|url_title");
                $this->form_validation->set_rules('menu_keywords', 'menu keywords', "trim");
                $this->form_validation->set_rules('menu_description', 'menu description', "trim");
                $this->form_validation->set_rules('menu_parent', 'menu parent', "trim");
                $this->form_validation->set_rules('status', 'status', "trim|required|fixed_values[1,0]");
                $this->form_validation->set_rules('menu_link_type', 'menu link type', "trim|required");
                $this->form_validation->set_rules('menu_open', 'menu open', "trim|required|fixed_values[same,new]");
               // $this->form_validation->set_rules('selectedModuleContent[][]','selected module content','required');
                $menu_link_type = $this->input->post('menu_link_type');
                switch($menu_link_type){
                    case "url":
                        $this->form_validation->set_rules('menu_url', 'menu url', "required|trim|prep_url");
                        break;
                    case "page":
                        //$this->form_validation->set_rules('side_menu_id', 'side menu id', "numeric");
                        //$this->form_validation->set_rules('footer_menu_id', 'footer menu id', "numeric");
                        //$this->form_validation->set_rules('banner', 'banner', "required|numeric|fixed_values[0,1]");
                       // $this->form_validation->set_rules('selectedModulesIds[][]', 'selected module content', "trim");
                        break;
                    case "site":
                        $this->form_validation->set_rules('menu_site', 'site link', "required|numeric|exists[site.id]");
                        break;
                }               
                $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
                if ($this->form_validation->run() == TRUE){ //echo '<pre>'; print_r($_POST); die;
                    /*$sideId2=$this->input->post('menuList_side');
                    if(!empty($sideId2) && is_array($sideId2)){
                        $sideId_cas=implode(",", $sideId2);
                    }else{
                        $sideId_cas = "";   
                    }
                    $footerId=$this->input->post('menuList_footer');
                    if(!empty($footerId) && is_array($footerId)){
                            $footerId_cas=implode(",", $footerId);
                    }else{
                            $footerId_cas = "";
                    } */
                    $insert_data['menu_type_id']    = $this->input->post('menu_type_id');
                    $insert_data['menu_title']      = $this->input->post('menu_title');
                    $insert_data['menu_alias']      = $this->input->post('menu_alias');
                    if($id == $this->input->post('menu_parent'))
                        $insert_data['menu_parent'] = 0;
                    else
                        $insert_data['menu_parent'] = $this->input->post('menu_parent');
                    $insert_data['page_title']          = $this->input->post('page_title');
                    $insert_data['menu_keywords']       = $this->input->post('menu_keywords');
                    $insert_data['menu_heading']        = $this->input->post('menu_heading');
                    $insert_data['menu_description']    = $this->input->post('menu_description');
                    $insert_data['status']              = $this->input->post('status');
                    //Menu Page Settings
                    $insert_data['menu_link_type']  = $this->input->post('menu_link_type');
                    $insert_data['menu_opens']      = $this->input->post('menu_open');
                    $insert_data['menu_url']        = $this->input->post('menu_url');
                    /*$insert_data['side_menu_id']    = $this->input->post('side_menu_id');
                    $insert_data['side_menu_title'] = $this->input->post('side_title');
                    $insert_data['side_menus']      = $sideId_cas;
                    $insert_data['footer_menu_id']  = $this->input->post('footer_menu_id');
                    $insert_data['footer_menu_title']   = $this->input->post('footer_title');
                    $insert_data['footer_menu_title']   = $this->input->post('footer_title');
                    $insert_data['footer_menus']    = $footerId_cas;
                    $insert_data['banner']      = $this->input->post('banner'); 
                    $insert_data['selected_banners']    = $this->input->post('selectedBanners');*/    
                    $insert_data['menu_site']       = $this->input->post('menu_site');  
                    //  $data['menu_position']      = set_value('menu_position');
                    if($id == 0){//insert
                        $insert_data['created_by']      = current_admin_id(); 
                        $insert_data['created_date']    = get_now();
                        $this->db->insert($this->menu, $insert_data);
                        $id = $this->db->insert_id();                       
                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'New data added successfully.');
                    }else{                      
                        $insert_data['updated_by']      = current_admin_id(); 
                        $insert_data['updated_date']    = get_now();
                        $this->db->where('id',$id);
                        $this->db->update($this->menu, $insert_data);
                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'Data updated successfully.');                     
                    }
                    if($this->input->post('menu_link_type')=="page"){
                        $modulesIdAdded = $this->input->post('module_id');
                        $selectedModuleContent = $this->input->post('selectedModulesIds');
                        $module_title = $this->input->post('module_title');
                        $allSelectedModuleContent = $this->input->post('allSelectedModuleContent');
                        $categorysName = $this->input->post('category_id');
                        if(!empty($modulesIdAdded) && is_array($modulesIdAdded)){
                            $count = 1;
                            $this->db->where("menu_id", $id);
                            $this->db->delete('menu_page_settings');
                            foreach($modulesIdAdded as $modInd=>$modVal){
                            unset($insert_data_menupage);
                            $category_id = 0;
                            if(isset($categorysName[$modInd]) && !empty($categorysName[$modInd])){
                                $category = $categorysName[$modInd];
                            }else{
                                $category = $categorysName[$modInd];
                            }
                            if(!empty($category) && $category != "all"){
                                $this->db->where('category_alias', $category);
                                $result = $this->db->get('categories')->row();
                                if(isset($result->id) && !empty($result->id )){
                                    $category_id = $result->id; 
                                }
                            }
                            $insert_data_menupage['menu_id']    = $id;
                            $insert_data_menupage['module_id']  = $modVal;
                            $insert_data_menupage['content_ids']    = $selectedModuleContent[$modInd];
                            $insert_data_menupage['position']   = $modInd;
                            $insert_data_menupage['module_title']   =  $module_title[$modInd];
                            $insert_data_menupage['category_id']    =  $category_id;
                            if(isset($allSelectedModuleContent[$modInd]) && $allSelectedModuleContent[$modInd] == "true"){
                                $insert_data_menupage['all_within_category'] =  'yes';
                            }else{
                                $insert_data_menupage['all_within_category'] =  'no';
                            }
                            $this->db->insert("menu_page_settings",$insert_data_menupage);
                        }
                        }
                    }   
                    flash_redirect($this->header['page_name'],$id);
                }
            }
            $data = $this->_format_data($id);
            $data['siteId']             = $siteId;  
            $data['modules']            = $this->cms->get_enabled_module($siteId);
            $data['menu_types']         = $this->cms->get_menu_types_dropdown($siteId);
            //$data['availabeBanners']    = $this->_getBanners($siteId, $data['selected_banners']);
            $this->load->view('header',$this->header);
            $this->load->view('menu',$this->data);     
            $this->load->view('cms/add_edit_menu',$data);
            $this->load->view('footer');
            
        }
        
        function _format_data($id){
            $data['module_id']          = array();
            $data['selectedModuleContent']  = array();
            $data['selectedModulesIds']         = array();
            if($this->input->post()){
                $data['id']         = $this->input->post('id');
                $data['site_id']        = $this->input->post('site_id');
                $data['menu_type_id']       = $this->input->post('menu_type_id');
                $data['menu_title']     = $this->input->post('menu_title');
                $data['menu_alias']     = $this->input->post('menu_alias');
                $data['page_title']     = $this->input->post('page_title');
                $data['menu_keywords']      = $this->input->post('menu_keywords');
                $data['menu_description']   = $this->input->post('menu_description');
                $data['menu_parent']        = $this->input->post('menu_parent');
                $data['menu_heading']       = $this->input->post('menu_heading');
                //Menu Page Settings
                $data['menu_link_type']     = $this->input->post('menu_link_type');
                $data['menu_open']      = $this->input->post('menu_open');
                $data['menu_url']       = $this->input->post('menu_url');
                $data['side_menu_id']       = $this->input->post('side_menu_id');
                $data['footer_menu_id']     = $this->input->post('footer_menu_id');
                $data['side_menus']         = $this->input->post('side_menus');
                $data['side_menu_title']    = $this->input->post('side_title');
                $data['footer_menus']       = $this->input->post('footer_menus');
                $data['footer_menu_title']  = $this->input->post('footer_title');
                $data['banner']         = $this->input->post('banner');
                $data['selected_banners']   = $this->input->post('selectedBanners');
                $data['selectedModulesIds'] = $this->input->post('selectedModulesIds');
                $data['selectedModuleContent']  = $this->input->post('selectedModuleContent');  
                $data['menu_site']      = $this->input->post('menu_site');
                $data['status']         = $this->input->post('status');
                $data['created_by']     = $this->input->post('created_by');
                $data['created_date']       = $this->input->post('created_date');
                $data['updated_by']     = $this->input->post('updated_by');
                $data['updated_date']       = $this->input->post('updated_date');
                $data['hits']           = $this->input->post('hits');
                $data['status']         = $this->input->post('status');
                $sideMenus                      = $this->input->post('sideMenuIds');
                $footerMenus                    = $this->input->post('footerMenuIds');
                $data['sideMenus']      = $this->administrator_model->get_menu_list($data['side_menu_id'], $sideMenus, 'side');
                $data['footerMenus']        = $this->administrator_model->get_menu_list($data['footer_menu_id'], $footerMenus, 'footer');
                $data['settings']       = $this->getSettings();
                if($id != 0){
                    $row = $this->cms->get_menu($id);
                    if($row){
                        $data['site_id']    = $row->site_id;    
                    }
                }
            }else if($id != 0){
                $row = $this->cms->get_menu($id);
                if( ! $row){
                    $this->session->set_flashdata('class', 'error');
                    $this->session->set_flashdata('msg', "Invalid Request!");
                    redirect($this->header['page_name']);
                }
                $data['id']         = $row->id;
                $data['site_id']        = $row->site_id;
                $data['menu_title']     = $row->menu_title;
                $data['menu_alias']     = $row->menu_alias;
                $data['page_title']     = $row->page_title;
                $data['menu_keywords']      = $row->menu_keywords;
                $data['menu_description']   = $row->menu_description;
                $data['menu_parent']        = $row->menu_parent;
                $data['menu_heading']       = $row->menu_heading;
                $data['menu_position']      = $row->menu_position;
                $data['menu_type_id']       = $row->menu_type_id;
                //Menu Page Settings
                $data['menu_link_type']     = $row->menu_link_type;
                $data['menu_open']      = $row->menu_opens;
                $data['menu_url']       = $row->menu_url;
                $data['side_menu_id']       = $row->side_menu_id;
                $data['footer_menu_id']     = $row->footer_menu_id;
                $data['side_menus']         = $row->side_menus;
                $data['side_menu_title']    = $row->side_menu_title;
                $data['footer_menus']       = $row->footer_menus;
                $data['footer_menu_title']  = $row->footer_menu_title;
                $data['banner']         = $row->banner;
                $data['selected_banners']   = $row->selected_banners;               
                $data['menu_site']      = $row->menu_site;
                $data['status']         = $row->status;
                $data['created_by']     = $row->created_by;
                $data['created_date']       = $row->created_date;
                $data['updated_by']     = $row->updated_by;
                $data['updated_date']       = $row->updated_date;
                $data['hits']           = $row->hits;
                $data['status']         = $row->status;
                $data['sideMenus']      = $this->administrator_model->get_menu_list($row->side_menu_id, $row->side_menus, 'side');
                $data['footerMenus']        = $this->administrator_model->get_menu_list($row->footer_menu_id, $row->footer_menus, 'footer');
                $data['settings']       = $this->cms->get_menu_page_settings($row->id);
            }else{
                $data['id']         = '';
                $data['site_id']        = $this->input->get('site');
                $data['menu_type_id']       = '';
                $data['menu_title']     = '';
                $data['menu_alias']     = '';
                $data['page_title']     = '';
                $data['menu_keywords']      = '';
                $data['menu_description']   = '';
                $data['menu_parent']        = '';
                $data['menu_heading']       = '';
                //Menu Page Settings
                $data['menu_link_type']     = 'none';
                $data['menu_open']      = 'same';
                $data['menu_url']       = '';
                $data['side_menu_id']       = '';
                $data['footer_menu_id']     = '';
                $data['side_menus']         = '';
                $data['side_menu_title']    = '';
                $data['footer_menus']       = '';
                $data['footer_menu_title']  = '';
                $data['banner']         = '';
                $data['selected_banners']   = '';           
                $data['menu_site']      = '';
                $data['status']         = '';
                $data['created_by']     = '';
                $data['created_date']       = '';
                $data['updated_by']     = '';
                $data['updated_date']       = '';
                $data['hits']           = '';
                $data['status']         = '';
                $data['sideMenus']      = '';
                $data['footerMenus']        = '';
                $data['settings']       = $this->getSettings();
                $data['status']         = '1';
            }
            $data['menu_opens']  =  array('same' => 'Same Window/Tab', 'new' => 'New Window/Tab');
            $data['statuses']    =  array('1' => 'Publish', '0' => 'Unpublish');
            //$data['banners']     =  array('No', 'Yes');
            $data['categories']  =  $this->administrator_model->get_categories_dropdown(/*$data['site_id']*/);
            $sites  =   $this->administrator_model->get_sites();
            $data['site_names'] =   convert_to_dropdown($sites, 'site_title', 'id', 'Select');
            $data['form_categories']    =   $this->administrator_model->get_form_categories_dropdown(); 
            return $data;           
        }
        
        function getSettings(){
            $module_title = $this->input->post('module_title');
            $module_id = $this->input->post('module_id');
            $category_id = $this->input->post('category_id');
            $selectedModulesIds = $this->input->post('selectedModulesIds');
            $settings = array();
            if(!empty($module_title) && is_array($module_title) && !empty($module_id) && is_array($module_id)){
                foreach($module_title as $ind=>$val){
                    $list = array("id" => "", "module_title"=>$module_title[$ind], "menu_id"=>"", "module_id"=>$module_id[$ind], "content_ids"=>$selectedModulesIds[$ind], "category_id"=>$category_id[$ind], "position"=>$ind);
                    array_push($settings, $list);
                }
            }
            return $settings;
        }
        
        function _getBanners($siteId, $selecteds){
            $html = '<div class="bannerListings"><ul class="bannerList">';
            $selecteds = explode(',', $selecteds);
            $banners   = $selecteds; 
            $this->db->select('id');
            $this->db->where('site_id', $siteId);
            $this->db->where_not_in('id', $selecteds);
            $result = $this->db->get('banner')->result_array();
            foreach($result as $row){
                array_push($banners, $row["id"]);   
            }
            if($siteId){
                $this->db->where('id', $siteId);
                $siteDetails = $this->db->get("site")->row();           
                $bannerSize = $siteDetails->siteBanner;
                $bannerSize = explode(',', $bannerSize);    
                $width = intval($bannerSize[0]);
                $height = intval($bannerSize[1]);
            }else{
                $bannerSize = "960,300";
                $bannerSize = explode(',', $bannerSize);
                $width = intval($bannerSize[0]);
                $height = intval($bannerSize[1]);       
            }
            $thmHeight = 10 / 100 * $height;
            $thmWidth = 10 / 100 * $width;
            foreach($banners as $selectedBanner){
                $this->db->where('site_id', $siteId);
                $this->db->where('id', $selectedBanner);
                $banner = $this->db->get('banner')->row_array();
                if($banner){    
                    $liClass=(in_array($banner['id'], $selecteds))?'class="selectedRow"':'';                    
                    if(file_exists('./uploaded_files/banner/'.$banner['file'])){
                        list($bannerWidth, $bannerHeight) = getimagesize(site_url('uploaded_files/banner/'.$banner['file']));
                        if($width <= $bannerWidth && $height <= $bannerHeight){
                            $html .= '<li rel="'.$banner['id'].'" '.$liClass.' title="' . $banner['id'] . '"><img src="' . site_url("image_show/banner/$thmWidth/$thmHeight/" .$banner['file']). '""/></li>';
                        }
                    }
                }
            }
            $html .= '</ul><div style="clear:both"></div></div>';
            return $html;
        }
        
        function delete($id = NULL){
            $this->admin_user_model->access_module($this->header['page_name'], 'delete');
            $id = (int)$id;
            //check whether it is used in other tables or not   
            $this->load->library('restrict_delete');
            $params = ""; //change it later
            if($this->restrict_delete->check_for_delete($params, $id)){
                if($this->input->post('selected')){
                    $selected_ids = $this->input->post('selected');
                    $deleted = 0;
                    foreach($selected_ids as $selectd_id){
                        if($this->_delete_menu($selectd_id))
                            $deleted++; 
                    }
                    if($deleted){
                        $action['class'] = 'success';
                        $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }else{
                    if($this->_delete_menu($id)){
                        $action['class'] = 'success';
                        $action['msg'] = 'Data deleted successfully!';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }
            }else{
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name']);
        }
        
        function _delete_menu($id){
            $this->db->where('id', $id);
            $this->db->delete($this->menu);
            if($this->db->affected_rows() > 0){             
                $this->db->where('menu_id', $id);
                $this->db->delete('menu_page_settings');    
                $this->db->where('menu_parent', $id);
                $result = $this->db->get($this->menu)->result();
                foreach($result as $row){
                    $this->_delete_menu($row->id);
                }               
                return true;
            }else{
                return false;
            }
        }
        
        function change_status($status = '', $id = NULL){
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', 1);
            $data['status'] = ($status=='1')?"1":"0";
            if($this->input->post('selected')){
                $selected_ids = $this->input->post('selected');
                $changed = 0;
                foreach($selected_ids as $selectd_id){
                    $this->db->where('id', $selectd_id);
                    $this->db->update($this->menu, $data);
                    if($this->db->affected_rows() > 0){
                        $changed++;                     
                    }
                }
                if($changed){
                    $action['class'] = 'success';
                    $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' status changed successfully!';
                }else{
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }else{
                $this->db->where('id', $id);
                $this->db->update($this->menu, $data);
                if($this->db->affected_rows() > 0){
                    $action['class'] = 'success';
                    $action['msg'] = 'Status changed successfully!';
                }else{
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name'], $id);
        }
        
        function sort_menu($parent_id = 0, $menu_type_id = ''){
            if($this->input->post()){
                $ids = explode(',', $this->input->post('sort_order'));
                for($i = 0; $i < count($ids); $i++){
                    $data['menu_position'] = $i;
                    $this->db->where('id', $ids[$i]);
                    $this->db->update('menu', $data);
                }           
            }else{
                $data['menus'] = $this->cms->get_menu_for_sort($parent_id, $menu_type_id);              
                $this->load->view('cms/sort_menu', $data);
            }
        }
        
    }