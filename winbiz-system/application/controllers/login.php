<?php
	class Login extends CI_Controller  {
		
		function __Construct()
		{
			parent::__Construct();		
			
			$this->header['title']			= "";
			$this->load->model('login_model');
			$this->load->helper('administrator');	
		}
		
		function index()
		{
			$url = $this->uri->segment_array();
            if(empty($url) && !is_array($url)){
                show_404();
            }
			if(current_admin_id() != 0) 
			{
				redirect('dashboard');
			}
			if (!($this->input->post()))
			{
				$this->load->view('login');
				$this->load->library('user_agent');
			}
			else
			{
				$user_name = $this->input->post('user_name');
				$password = $this->input->post('pass_word');
				$redirect = $this->input->post('redirect');
				$user_id = $this->login_model->check_login($user_name, md5($password));
				if ($user_id != 0)
				{
					//set session and redirect to dashboard
					$this->session->set_userdata('admin_id', $user_id);
					if($redirect){
						redirect($redirect);
					}else{
						redirect(base_url("dashboard"));
					}
				}
				else
				{
					$data['login'] = 'Invalid Username/Password!';
					$data['class'] = 'error';
					$this->session->set_flashdata($data);
					$redirect_url = "";
					if($redirect){
						$redirect_url = "?redirect=".$redirect;
					}
					redirect('login'.$redirect_url);
				}
			}				
		}
		
		function logout()
		{
			$this->session->unset_userdata('admin_id');
			
			$data['login'] = 'Log out successfully!';
			$data['class'] = 'success';
			$this->session->set_flashdata($data);
			
			redirect('login');
		}
		
		function forget()
		{
			if(current_admin_id() != 0) 
			{
				redirect('dashboard');
			}
			
			if ( ! ($this->input->post()))
			{
				$this->load->view('header', $this->header);
				$this->load->view('forget_password');
				$this->load->view('footer');
			}
			else
			{
				$this->db->where('user_name', $this->input->post('user_name'));
				//$this->db->where('email', $this->input->post('email'));
				$admin = $this->db->get('admin_user')->row();
				
				if (count($admin) > 0) 
				{
					$pass = random_string('alnum', 10);
										
					//send email										
					$this->load->library('email');				
					$this->email->set_newline("\r\n");
					$sender_info = $this->db->select('site_from_email, site_title')->get('site')->row();
					$sender_email = $sender_info->site_from_email;
					$sender_title = $sender_info->site_title;
					/*$sender_email = $this->global_model->setting('site_email');
					if(empty($sender_email))
						$sender_email = 'site_email@localhost.com';
						
					$sender_title = $this->global_model->setting('site_title');*/
					
					if(empty($site_title))
						$site_title = 'site_title';	
					
					$this->email->from($sender_email, $sender_title);
					$this->email->to($admin->email);
								
					$this->email->subject('Reset Password');
					$this->email->message("You have requested for password change!
Your new password is : $pass

Note: Please change this password!
"
			);
			
					$this->email->send();
					
					$this->db->where('user_name', $admin->user_name);
					$this->db->where('email', $admin->email);
					$database['password'] = md5($pass);
					$this->db->update('admin_user', $database);
			
					$data['login'] = 'E-mail sent to ' . $admin->email . '!';
					$data['class'] = 'success';
					$this->session->set_flashdata($data);
				}
				else
				{
					$data['login'] = 'Username and e-mail address not matched!';
					$data['class'] = 'error';
					$this->session->set_flashdata($data);
					redirect('login/forget');
				}
				redirect('login');
			}
			
		}
	}

	
