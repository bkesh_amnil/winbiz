<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Install extends CI_Controller {

    function index() {
        

        if (file_exists('application/config/route_extend.php')) {
            redirect();
        }

        $data = array();
        if ($this->input->post()) {
            //may need form validation code here				
            $error = array();

            //database.php
            $server_name = $this->input->post('server_name');
            $db_name = $this->input->post('db_name');
            $db_user_name = $this->input->post('db_user_name');
            $db_pass_word = $this->input->post('db_pass_word');
            $db_prefix = $this->input->post('db_prefix');

            $con = @mysqli_connect($server_name, $db_user_name, $db_pass_word, $db_name);
            // Check connection
            if (mysqli_connect_errno($con)) {
                $error[] = "Please check db server name, db name, db username and db password.";
            }

            $query = file_get_contents("database.sql");
            $query = str_replace('{PREFIX}', $db_prefix, $query);

            /* execute multi query */
            if (!mysqli_multi_query($con, $query)) {
                $error[] = "Couldn't execute database file.";
            }

            /* need this code to know the end of the query execution */
            do {
                if ($result = mysqli_store_result($con)) {
                    mysqli_free_result($result);
                }
            } while (mysqli_next_result($con));

            if (count($error) != 0) {
                $data['error'] = $error;
            } else {
                $my_file = "application/config/database_extend.php";
                $fh = fopen($my_file, 'w') or die("can't open file");
                $string_data = '<?php 
	$db["default"]["hostname"] = "' . $server_name . '";
	$db["default"]["username"] = "' . $db_user_name . '";
	$db["default"]["password"] = "' . $db_pass_word . '";
	$db["default"]["database"] = "' . $db_name . '";
	$db["default"]["dbprefix"] = "' . $db_prefix . '";
	$db["default"]["swap_pre"] = "{PREFIX}";							
?>';
                fwrite($fh, $string_data);
                fclose($fh);

                $session['server_name'] = $this->input->post('server_name');
                $session['db_name'] = $this->input->post('db_name');
                $session['db_user_name'] = $this->input->post('db_user_name');
                $session['db_pass_word'] = $this->input->post('db_pass_word');
                $session['db_prefix'] = $this->input->post('db_prefix');

                $session['site_name'] = $this->input->post('site_name');
                $session['site_offline_msg'] = $this->input->post('site_offline_msg');
                $session['site_from_email'] = $this->input->post('site_email');
                $session['site_description'] = $this->input->post('site_description');
                $session['site_keyword'] = $this->input->post('site_keyword');
                $session['google_analytics_code'] = $this->input->post('analytics_code');

                $session['user_name'] = $this->input->post('user_name');
                $session['password'] = $this->input->post('pass_word');
                $session['first_name'] = $this->input->post('first_name');
                $session['last_name'] = $this->input->post('last_name');
                $session['email'] = $this->input->post('email');
                $session['gender'] = $this->input->post('gender');
                $session['mobile_num'] = $this->input->post('mobile_number');
                $session['address'] = $this->input->post('address');

                $this->session->set_flashdata($session);

                redirect('installer/install/installing');
            }
        }
        $this->load->view('installer/install', $data);
    }

    function installing() {
        if (!$this->session->flashdata('user_name')) {
            redirect();
        }

        $server_name = $this->session->flashdata('server_name');
        $db_user_name = $this->session->flashdata('db_user_name');
        $db_pass_word = $this->session->flashdata('db_pass_word');
        $db_name = $this->session->flashdata('db_name');
        $db_prefix = $this->session->flashdata('db_prefix');

        //site
        $insert_site['site_title'] = $this->session->flashdata('site_name');
        $insert_site['site_name'] = url_title(strtolower($insert_site['site_title']));
        $insert_site['main_site'] = 1;
        $insert_site['site_offline_msg'] = $this->session->flashdata('site_offline_msg');
        $insert_site['site_from_email'] = $this->session->flashdata('site_email');
        $insert_site['site_description'] = $this->session->flashdata('site_description');
        $insert_site['site_keyword'] = $this->session->flashdata('site_keyword');
        $insert_site['google_analytics_code'] = $this->session->flashdata('analytics_code');
        $this->db->insert('site', $insert_site);
        $site_id = $this->db->insert_id();

        //profile
        $insert_profile['site_id'] = $site_id;
        $insert_profile['profile_name'] = 'Super Administrator';
        $insert_profile['super_admin'] = 'TRUE';
        $this->db->insert('profile', $insert_profile);
        $profile_id = $this->db->insert_id();

        //administrator account
        $insert_user['profile_id'] = $profile_id;
        $insert_user['user_name'] = $this->session->flashdata('user_name');
        $insert_user['password'] = md5($this->session->flashdata('password'));
        $insert_user['first_name'] = $this->session->flashdata('first_name');
        $insert_user['last_name'] = $this->session->flashdata('last_name');
        $insert_user['email'] = $this->session->flashdata('email');
        $insert_user['gender'] = $this->session->flashdata('gender');
        $insert_user['mobile_num'] = $this->session->flashdata('mobile_number');
        $insert_user['address'] = $this->session->flashdata('address');
        $this->db->insert('admin_user', $insert_user);

        //now change route
        $my_file = "application/config/route_extend.php";
        $fh = fopen($my_file, 'w') or die("can't open file");
        $string_data = '<?php 
	$route["default_controller"] = "login";					
?>';
        fwrite($fh, $string_data);
        fclose($fh);

        redirect();
    }

}
