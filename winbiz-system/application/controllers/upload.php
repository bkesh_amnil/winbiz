<?php
	class Upload extends CI_Controller {

		public $header = array();

		function __Construct() {
			parent::__Construct();
		}
		
		function index() {	
		}

		function uploadImages($site_id = NULL, $for = "gallery", $temp = true){
			
			if($site_id){
				$msg = "";
				$result = "";
				if($temp == true){
					$uploaddir = './uploaded_files/temp/';
				}else{
					$uploaddir = './uploaded_files/'.$for;
				}
				if(!is_dir($uploaddir)){
					if(mkdir($uploaddir)){
						chmod($uploaddir, 0777);
					}else{
						$result = "response:error, msg:Invalid Upload Target";
						echo $result;
						die();
					}
				} 
				$insertFileInf = false;
				if(!empty($_FILES['Filedata']['name']) && $_FILES['Filedata']['error'] == 0){
						$filename 				= $_FILES['Filedata']['name'];
						$filesize_image 		= $_FILES['Filedata']['size'];
						$file 					= $_FILES['Filedata']['tmp_name'];
						$allowed_ext = "jpg,jpeg,gif,png,bmp";
						$imgInfo =  getimagesize($file);
						$file_type = image_type_to_mime_type(exif_imagetype($file));
						$allowed_ext = preg_split("/\,/",$allowed_ext);
						$maxSize 				= 1024 * 5;
						$maxW 					= 1024;
						$maxH 					= 768;
						$minW 					= 200;
						$minH 					= 200;
						$uploadPath 			= $uploaddir;
						if($filesize_image > ($maxSize * 1024)){
							$msg="Image size is too large to accept. Image Size should be less than ".$maxSize." KB";
							$result = "response:error, msg:".$msg;
						}else{
							switch($file_type){
								case 'image/gif':
									$fileUploadName = $for."_".rand(1,999)."_".time().".gif";
									$upltype = "gif";
									break;
								case 'image/jpeg':
									$fileUploadName = $for."_".rand(1,999)."_".time().".jpg";
									$upltype = "jpg";
									break;
								case 'image/jpg':
									$fileUploadName = $for."_".rand(1,999)."_".time().".jpg";
									$upltype = "jpg";
									break;
								case 'image/png':
									$fileUploadName = $for."_".rand(1,999)."_".time().".png";
									$upltype = "png";
									break;
								default:
									$fileUploadName = "";
									break;
							}
							if(empty($fileUploadName)){
								$msg="Unrecognized File Type";
								$result = "response:error, msg:".$msg;
							}else{
								require_once './phpthumb/ThumbLib.inc.php';
								$thumb = PhpThumbFactory::create($file);
								$thumb->adaptiveResize($maxW, $maxH);
								$saved = $thumb->save($uploadPath."/".$fileUploadName, $upltype);
								if($saved){
									$imgUpload = true;
									$image = $fileUploadName;
								}
							}
						}
						if($imgUpload)
						{
						  list($width, $height, $type, $attr) = getimagesize($uploadPath."/".$fileUploadName);
						  if($width >= $minW && $height >= $minH){
								$insertFileInf = true;
								$msg = "File Uploaded Successfully".
								$result = "response:success, msg:File Uploaded Successfully, file_name:".$fileUploadName;
						  }else{
							 unlink($uploadPath."/".$image);
							 $msg = "Image Size Should be of ".$minW."px Width and ".$minH."px Height.";
							 $result = "response:error, msg:".$msg;
						  }
						}else{
							if(empty($msg) || $msg == "Unspecified error"){
								$msg = "Image was not uploaded.";
								$result = "response:error, msg:".$msg;
							}
						}
				}else{
					$msg = "Select Image For Banner";
					$result = "response:error, msg:".$msg;
				}
				echo $result;
			}
		}

		function showImage(){
			$file = $this->input->get('file');
			$type = $this->input->get('type');
			$width = $this->input->get('width');
			$height = $this->input->get('height');
			if(empty($width)){
				$width = 100;
			}
			if(empty($height)){
				$height = 100;
			}
			if(!empty($file)){
				switch($type){
					case "logo":
						$path = "./uploaded_files/site_logo/";
						break;
					case "advertisement":
						$path = "./uploaded_files/advertisement/";
						break;
					case "content":
						$path = "./uploaded_files/content/";
						break;
					case "enterprise":
						$path = "./uploaded_files/enterprise/";
						break;
					case "products":
						$path = "./uploaded_files/products/";
						break;
					case "product_image":
						$path = "./uploaded_files/product_images/";
						break;
					case "temp":
						$path = "./uploaded_files/temp/";
						break;
					default:
						$path = "./images/";
						break;
				}
				$imagePath = $path.$file;
				if(file_exists($imagePath)){
					require_once './phpthumb/ThumbLib.inc.php';
					$thumb = PhpThumbFactory::create($path.$file);
					$thumb->adaptiveResize($width,$height);
					$thumb->show();
				}
			}
		}
	}
?>