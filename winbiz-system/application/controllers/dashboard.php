<?php

class Dashboard extends CI_Controller {

    function __Construct() {
        parent::__Construct();

        $this->load->helper('administrator');
        is_already_logged_in();

        $header['title'] = "Administrator Dashboard";
        //$header['stylesheets'] 	= array("960", "reset", "text", "blue");
        $header['scripts'] = array("plugins/jquery-1.8.3.min.js", "plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/jqvmap/jqvmap/jquery.vmap.js", "plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js", "plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js", "plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js", "plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js", "plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js", "plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js", "plugins/flot/jquery.flot.js", "plugins/flot/jquery.flot.resize.js", "plugins/jquery.peity.min.js", "plugins/jquery-knob/js/jquery.knob.js", "plugins/jquery.pulsate.min.js", "plugins/bootstrap-daterangepicker/date.js", "plugins/bootstrap-daterangepicker/daterangepicker.js", "plugins/gritter/js/jquery.gritter.js", "plugins/fullcalendar/fullcalendar/fullcalendar.min.js", "scripts/app.js", "scripts/index.js");
        $this->load->model('admin_user_model');
        $this->load->view('header', $header);
    }

    function index() {
        $this->load->helper('html');  //for nbs(); in get_organisation_projects();
        $data['dashboard'] = TRUE;
        $data['same_password'] = $this->admin_user_model->username_password();
        $data['dashboardmenus'] = $this->admin_user_model->getdashboardmenu();
        $data['page_name'] = "Dashboard";
        $this->load->view('menu', $data);
        $this->load->view('dashboard');
        $this->load->view('footer');
    }

}

?>