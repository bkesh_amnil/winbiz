<?php

class Form_fields extends CI_Controller {
               
	function __construct()
	{
 		parent::__construct();
		
		$this->load->helper('administrator');
		is_already_logged_in();
		
		$this->header['title']			= "Form Field Management";
		$this->header['page_name']		= $this->router->fetch_class();
		$this->header['stylesheets'] 	= array("960", "reset", "text", "blue", "facebox");
		//$this->header['scripts'] = array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects", "jquery.animate-colors-min", "dynamic_forms/form_fields", "sortFacebox", "jquery.form", "jquery-ui", "admin/sort", "dynamic_forms/jquery.tablednd", "dynamic_forms/jquery.tablednd.0.7.min");			
		$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","cms/sort.js", "cms/jquery.tablednd.js", "cms/jquery.tablednd.0.7.min.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js", "cms/public_script.js", "cms/form_fields.js");
		$this->data['menu_cms'] = TRUE;		//menu
		$this->header['dynamic_form'] = TRUE;		//submenu
		
		$this->load->model('dynamic_form/dynamic_form_model');
		$this->load->model('dynamic_form/form_model');
	}	
	
	function index($id = NULL)
	{	
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
                
		$row = $this->db->where('id', $id)->get('form')->row_array();
		if( ! $row)
		{
        	redirect('dynamic_form', 'refresh', 404);
		}	
		
		if($this->input->post())
		{
			$this->_post($id);
			redirect('' . $this->header['page_name'] . '/index/' . $id);
		}	
		$data = array_merge($data, $this->_format($id)); 
		$this->load->view('header', $this->header);
		$this->load->view('cms/sub_menu');	
		$this->load->view('menu', $this->data);	
		$this->load->view('dynamic_form/add_edit_fields', $data);	
		$this->load->view('footer');	
	}
	
	function _format($id)
	{           
		$data = $this->db->where('id', $id)->get('form')->row_array();
		$data['fields'] = $this->form_model->form_fields($id, 0, $this->input->get('search'));
		
		$field_types = $this->db->get('field_type')->result();
		$data['field_types'] = dropdown_data($field_types, 'field_type', 'display_text', 'multiple_values');
		
		$data['validations'] = $this->db->get('validation_rule')->result();
		$data['show_in_grids'] = array('No', 'Yes');
		$data['front_displays'] = array('No', 'Yes');
		return $data;
	}
	
	function _post($form_id)
	{
		$field_labels = $this->input->post('field_label');
		$field_types = $this->input->post('field_type');
		
		$field_name = $this->input->post('field_name');
		
		$display_texts = $this->input->post('display_text');
		
		$default_values = $this->input->post('default_value');
		$field_classes = $this->input->post('field_class');
		
		$field_attributes = $this->input->post('field_attribute');
		$field_orders = $this->input->post('field_order');
		$front_displays = $this->input->post('front_display');
		$show_in_grids = $this->input->post('show_in_grid');
		$validation_rules = $this->input->post('validation_rule');
		//printr($this->input->post());		
		
		if($field_labels)
		{
			$data = array(
						'form_id'		=> $form_id,
						'field_label' 	=> $field_labels,
						'field_name' 	=> $field_name,
						'field_type'	=> $field_types,
						'default_value' => $default_values,
						'field_class' 	=> $field_classes,
						'field_attribute' => '',
						'field_order'	 => $field_orders,
						'front_display' => $front_displays,
						'show_in_grid' 	=> $show_in_grids,
						'validation_rule' => is_array($validation_rules)?implode('|', $validation_rules):"",
			);
			
			$this->db->insert('form_field', $data);
			
			$insert_values['form_field_id'] = $this->db->insert_id(); 
					   
			for($j = 0; $j < count($display_texts); $j++)
			{
				if(trim($display_texts[$j]) != '')
				{
					$insert_values['display_text'] = $display_texts[$j]; 
					$this->db->insert('form_field_value', $insert_values);										   
				}
			}
		}	
	}
	
	function remove_field()
	{
		if($this->input->is_ajax_request())
		{
			$fields = $this->input->post('fields');

			foreach($fields as $field)
			{
				$field_id =  $field["value"];				
				$this->db->where('id', $field_id);
				$row = $this->db->get('form_field')->row();
				
				$action = 'fail';
				if($row)
				{
					$field_type = $row->field_type;
					
					$this->db->where('id', $field_id);
					$this->db->delete('form_field');
				
					if($this->db->affected_rows())
					{
						$action = 'success';
						
						//delete the multiple values
						$this->db->where('form_field_id', $field_id);
						$this->db->delete('form_field_value');
						
						//unlink the uploaded files
						if($field_type == 'upload')
						{
							$result = $this->db->where('form_field_id', $field_id)->get('form_submission_fields')->result();
							foreach($result as $row)
							{
								$file = './dynamic_uploads/' . $row->form_field_value;
								if($row->form_field_value && file_exists($file))
									unlink($file);
							}
						}
						
						$this->db->where('form_field_id', $field_id);
						$this->db->delete('form_submission_fields');				
					}
				}	
			}
			echo $action;
		}
		else
		{
			redirect('dynamic_form', 'refresh', 404);
		}
	}
	
	function save_field()
	{		
		if($this->input->post())
		{      
			$form_field_id = $this->input->post('form_field_id');
			$form_id = $this->input->post('form_id');
			
			$this->form_validation->set_rules("field_label",'Field Label',"trim|xss_clean|required");
			$this->form_validation->set_error_delimiters('', '');
			
			if($this->form_validation->run() == FALSE)
			{
				echo validation_errors();
			}
			else
			{
				$display_texts = $this->input->post('display_text');	//one dimensional array
				$field_labels = $this->input->post('field_label');
				$field_name = $this->input->post('field_name');
				$default_values = $this->input->post('default_value'); 
				$field_classes = $this->input->post('field_class');				
				$field_attributes = $this->input->post('field_attribute');
				$field_orders = $this->input->post('field_order');
				$show_in_grids = $this->input->post('show_in_grid'); 
				$front_display = $this->input->post('front_display');
				$validation_rules = $this->input->post('validation_rule'); 
                                
				if($field_labels)
				{
					$this->form_validation->set_rules("field_labels",'',"trim|xss_clean|required");
					
					$data = array(
							'field_label' 	=> $field_labels,
							'field_name'	=> $field_name,
							'default_value' => $default_values,
							'field_class' 	=> $field_classes,
							'field_attribute' => $field_attributes,
							'field_order'	 => $field_orders,
							'show_in_grid' 	=> $show_in_grids,
							'front_display' => $front_display,
							'validation_rule' => is_array($validation_rules)?implode('|', $validation_rules):"",
						);
								
				                                
					$this->db->where('id', $form_field_id);
					$this->db->update('form_field', $data);
                
					if(is_array($display_texts))
					{
						$insert_values['form_field_id'] = $form_field_id;
						foreach($display_texts as $display_text)
						{
							if(trim($display_text) != '')
							{
								$insert_values['display_text'] = $display_text;
								$this->db->insert('form_field_value', $insert_values);
							}
						}	
					}
				}
				//echo $this->db->last_query();
               redirect('form_fields/index/'.$form_id, 'refresh');

			}		
		}
	}
	
	function remove_cur_value()
	{
		if($this->input->is_ajax_request())
		{
			$this->db->where('id', $this->input->post('id'));
			$this->db->delete('form_field_value');
			if($this->db->affected_rows())
				echo "success";
			else
				echo "failed";
		}
		else
		{
			redirect('dynamic_form', 'refresh', 404);
		}
	}
        
        function add_field($id=NULl)
        {
                $data = $this->_format($id);
                $this->load->view('cms/add_fields', $data);
        }
        function sort_fields()
        {
            if($this->input->post())
            {
                $ids = explode(' ', $this->input->post('debugStr')); 
                for($i = 1; $i<count($ids)-1; $i++)
                { 
                    $data['field_order'] = $i;
                    $this->db->where('id', $ids[$i]);
                    $this->db->update('form_field', $data);
                }               
            }
        }
        
        function edit($form_id='', $id='')
        {
            $data['field_label'] = $this->input->post('field_label[]');
            $this->db->where('id', $id, 'form_id', $form_id);
            $this->db->update('form_field', $data);
            
        }
}
?>