<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Dynamic_form_ajax extends CI_Controller {
		function __Construct()
		{
			parent::__Construct();
			
			$this->load->helper('administrator');
			is_already_logged_in();
		}
		
		function add_edit_fields($form_id = '', $field_id = '')
		{
			$this->load->model('dynamic_form/form_model');
			
			$data = $this->db->where('id', $form_id)->get('form')->row_array();
			$data['fields'] = $this->form_model->form_fields($form_id);
			
			$field_types = $this->db->get('field_type')->result();
			$data['field_types'] = dropdown_data($field_types, 'field_type', 'display_text', 'multiple_values');
			
			$data['validations'] = $this->db->get('validation_rule')->result();
                        $data['show_in_grids'] = array('No', 'Yes');
			$data['front_displays'] = array('No', 'Yes');
			$this->load->view("dynamic_form/add_fields", $data);
         }
		 
		function edit_fields($form_id = '', $id = '')
		{
			$this->load->model('dynamic_form/form_model');
			$data = $this->db->where('id', $form_id)->get('form')->row_array();
			
			$field_types = $this->db->get('field_type')->result();
			$data['field_types'] = dropdown_data($field_types, 'field_type', 'display_text' , 'multiple_values');
			$data['validations'] = $this->db->get('validation_rule')->result();
			$data['show_in_grids'] = array('No', 'Yes');
			$data['front_displays'] = array('No', 'Yes');
			$data['valueArr'] = $this->db->where('id', $id)->where('form_id', $form_id)->get('form_field')->row_array();
			
			$row = $this->db->where('field_type', $data['valueArr']['field_type'])->get('field_type')->row();
			$data['valueArr']['field_type'] .= '" multiple_values="' . $row->multiple_values;
			//echo $this->db->last_query();
			//printr($data);
			$this->load->view("dynamic_form/edit_fields", $data);				   
		}
		
		function remove_option($form_field_id = '')
		{
			if($this->input->is_ajax_request())
			{
				$option_id = $this->input->post('option_id');
				$this->db->where('id', $option_id);
				$this->db->where('form_field_id', $form_field_id);
				$this->db->delete('form_field_value');
				
				if($this->db->affected_rows())
					echo "success";
				else
					echo "fail";
			}
		}
	}