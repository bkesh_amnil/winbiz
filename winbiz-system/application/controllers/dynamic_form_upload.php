<?php
//Flash can't use the cookie and session of php. so, it cannot check the logged in members
//so don't use is_already_logged_in(); function

class Dynamic_form_upload extends CI_Controller {

		function __Construct()
		{
			parent::__Construct();
		}
		
		function multiple_uploads()
		{
			if($this->input->post('form_submission_id') && $this->input->post('form_field_id'))
			{
				$table_name = 'form_submission_fields';
				//sleep(5);
				//echo "20"; exit;
				//uploading files
				$this->load->library('upload');
				
				$config['upload_path'] 	= './dynamic_uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
		
				$this->upload->initialize($config);
				
				$_FILES['userfile']['name'] 	= $_FILES['Filedata']['name'];
				$_FILES['userfile']['type']		= $_FILES['Filedata']['type'];
				$_FILES['userfile']['tmp_name']	= $_FILES['Filedata']['tmp_name'];
				$_FILES['userfile']['error'] 	= $_FILES['Filedata']['error'];
				$_FILES['userfile']['size'] 	= $_FILES['Filedata']['size'];
							
				if($this->upload->do_upload())
				{
					$data_image = $this->upload->data();
					
					$insert_data['form_submission_id']	= $this->input->post('form_submission_id');
					$insert_data['form_field_id']		= $this->input->post('form_field_id');				
					$insert_data['form_field_value'] 	= $data_image['file_name'];
					$insert_data['form_display_text'] 	= $data_image['orig_name'];
					$this->db->insert($table_name, $insert_data);
					
					echo '<div class="row">&raquo; ' . $data_image['orig_name'] . ' &nbsp; &nbsp; ' . anchor("downloads/files/" . $insert_data['form_submission_id'] . '/' . $insert_data['form_field_id'] . '/' . $this->db->insert_id(), 'Download') . ' &nbsp; ' . anchor('#', 'Remove', 'rel="'. $insert_data['form_field_id'] . '" class="remove_file" multi_upload="true"') . '</div>';
				}
				else
				{
					echo $this->upload->display_errors('', '');
				}
			}
			else
			{
				redirect('dashboard');
			}
		}
}
?>
