<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __Construct() {
        parent::__Construct();

        $this->load->helper('administrator');
        is_already_logged_in();
    }

    function index() {
    }

    function address_labels() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('country_model');
            $result = $this->country_model->get_address_settings($this->input->post('country_id'));
            echo json_encode($result);
        }
    }

    function getMenu($menuTypeId = '') {
        if ($this->input->is_ajax_request()) {
            if ($menuTypeId) {
                $for = $this->input->post('for');
                echo '<div style="width:500px;"><span style="float:left;">Menu Title : </span>   <input type="text" name="' . $for . '_title" style="float:left; margin-left:10px;" class="span12" /></div><div style="clear:both"></div>';
                $this->administrator_model->get_menu($menuTypeId);
            }
        }
    }

    function getModuleContents() {
        if ($this->input->is_ajax_request()) {
            //printr($_POST);
            $siteId = $this->input->post('siteId');
            $moduleId = $this->input->post('moduleId');
            $position = $this->input->post('position');
            $category = $this->input->post('category_id');
            $selected = $this->input->post('selecteds');
            $this->administrator_model->get_module_contents($siteId, $moduleId, $position, $selected, $category);
        }
    }

    function getAvailableMenus($level = 0) {
        $siteId = $this->input->post('siteId');
        $menuType = $this->input->post('menuType');

        if (empty($menuType)) {
            $options = "";
        } else {
            $this->db->where("id", $menuType);
            $settingData = $this->db->get('site_menus')->row();
            if ($settingData->submenus == "yes") {
                if ($settingData->depth == 0) {
                    $depth = 999;
                } else {
                    $depth = $settingData->depth;
                }
                $options = $this->getMenus($settingData->id, $depth);
            } else {
                $options = "";
            }
        }
        $html = '<option value="">Parent</option>';
        if (!empty($options) && is_array($options)) {
            $html .= $this->generateHtml($options);
        }
        echo $html;
    }

    function getMenus($typeId, $depth = 0, $parent = 0) {
        if ($depth == 0) {
            return false;
        }
        $this->db->where('menu_type_id', $typeId);
        $this->db->where('status', "1");
        $this->db->where('menu_parent', $parent);
        $menuData = $this->db->get('menu')->result();
        
        if (!empty($menuData) && is_array($menuData)) {
            foreach ($menuData as $ind => $val) {
                $data[] = array("id" => $val->id, "name" => $val->menu_title, "depth" => $depth, "childs" => $this->getMenus($typeId, ($depth - 1), $val->id));
            }
        }
        if (!empty($data) && is_array($data)) {
            return $data;
        }
        return false;
    }

    function generateHtml($options, $maxDepth = 0) {
        $html = "";
        foreach ($options as $ind => $val) {
            $depth = $val['depth'];
            if ($maxDepth == 0) {
                $maxDepth = $depth;
            }
            $actDepth = $maxDepth - $depth;
            $spaces = "";
            for ($i = 0; $i < $actDepth; $i++) {
                $spaces .= '&nbsp;';
            }
            $html .= '<option value="' . $val['id'] . '">' . $spaces . $val['name'] . '</option>';
            if (!empty($val['childs']) && is_array($val['childs'])) {
                $html .= $this->generateHtml($val['childs'], $maxDepth);
            }
        }
        return $html;
    }

    function getBanner($siteId) {
        if ($this->input->is_ajax_request()) {
            if ($siteId) {
                $this->load->model("cms/admin_banner_model", "bannercms");
                $result = $this->bannercms->getBanners($siteId, FALSE, 0, 'position', 'asc');
                $html = '';
                if (!empty($result) && is_array($result)) {
                    $html = '<ul class="bannerList">';
                    foreach ($result as $ind => $banner) {
                        $this->db->where('id', $banner["site_id"]);
                        $siteDetails = $this->db->get("site")->row();
                        $bannerSize = $siteDetails->siteBanner;
                        $bannerSize = explode(',', $bannerSize);
                        $width = intval($bannerSize[0]);
                        $height = intval($bannerSize[1]);
                        $thmHeight = 10 / 100 * $height;
                        $thmWidth = 10 / 100 * $width;
                        if (file_exists('./uploaded_files/banner/' . $banner['file'])) {
                            list($bannerWidth, $bannerHeight) = getimagesize(site_url('uploaded_files/banner/' . $banner['file']));
                            if ($width <= $bannerWidth && $height <= $bannerHeight) {
                                $html .= '<li rel="' . $banner['id'] . '"><img src="' . site_url('upload/showImage?file=' . $banner['file'] . '&type=banner&width=' . $thmWidth . '&height=' . $thmHeight) . '" width="' . $thmWidth . '" height="' . $thmHeight . '" /></li>';
                            } else {
                                //echo $width." >= ".$bannerWidth.", ".$height." >=  ".$bannerHeight." -- Size mismatched <br><br>";	
                            }
                        } else {
                            //echo "File Not Found <br><br>";	
                        }
                    }
                    //die();
                    $html .= '</ul>';
                }
                echo $html;
            }
        }
    }

    function getSiteMenuTypes() {
        $site_id = intval($this->input->post('siteId'));
        if ($site_id > 0) {
            $menuTypes = $this->administrator_model->get_menu_types($site_id);
        } else {
            $menuTypes = '';
        }
        $htm = '';
        if (count($menuTypes) != 1) {
            $htm .= '<option value="0">Select</option>';
        }
        foreach ($menuTypes as $ind => $val) {
            $htm .= '<option value="' . $val->id . '">' . $val->name . '</option>';
        }
        echo $htm;
    }

    function get_modules() {
        if ($this->input->is_ajax_request()) {
            $site_id = $this->input->post('site_id');
            $this->load->model('cms/admin_menu_model', 'menu');

            if ($this->input->post('output') == 'select') {
                $result = $this->menu->get_enabled_module($site_id);
                $output = '';
                foreach ($result as $index => $value) {
                    $output .= '<option value="' . $index . '">' . $value . '</option>';
                }
                echo $output;
            }
        }
    }

    function getSearchCategoryList() {

        $this->db->select('*')
                ->from('tbl_categories')
                ->like('category_name', $this->input->get('term'), 'after');

        $query = $this->db->get();
        $search_result = array();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $search_result[] = array('id' => $result->category_id, 'label' => $result->category_name, 'value' => $result->category_name);
            }
            echo json_encode($search_result);
            exit;
        } else {
            var_dump("contents");
        }
        exit;
    }

}