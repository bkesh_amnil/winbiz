<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Content extends CI_Controller {
        public $header = array();
        var $module = "module";
        var $profile = 'profile';
        var $profile_detail = 'profile_detail';
        var $content = 'content';

        function __Construct(){
                parent::__Construct();
                $this->load->helper('administrator');
                is_already_logged_in();
                $this->header['title']		= "Content Management";
                $this->header['page_name']	= $this->router->fetch_class();
                $this->header['page_parent']	= get_class_parent($this->header['page_name']);
                $this->header['stylesheets'] 	= array("960", "reset", "text", "blue", "facebox");
                $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
                $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js","scripts/form-validation.js","plugins/jquery-validation/dist/jquery.validate.min.js","plugins/jquery-validation/dist/additional-methods.min.js");
                $this->data['menu_cms']         = TRUE;		//menu
                $this->header['content']        = TRUE;		//submenu
                $this->load->library('custom_pagination');		//add this
                $this->load->model('cms/admin_content_model', 'cms');
                $this->load->model('cms/admin_cmscategory_model', 'cmscategory');
        }

        function index(){	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
            $start = $this->uri->segment(3);
            $total_rows = count($this->cms->get_content());
            $config = $this->custom_pagination->admin_configuration();
            $data['start'] = $start;
            $data['rows'] = $this->cms->get_content(0, TRUE, $start, 'title');
            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);						
            $this->load->view('cms/view_content', $data);	
            $this->load->view('action');		
            $this->load->view('footer');
        }

        function form($id = NULL){
            $id = (int)$id;	
            $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            $siteId = intval($this->input->get('site'));
            if( $id != 0 ){
                $row = $this->cms->get_content($id);
                $siteId = $row->site_id;
            }
            $this->header['title'] = "Add / Edit Content"; 
            $id = (int)$id;	
            $error_mess = '';
            $this->load->library('form_validation');
            if ($this->input->post()){	
                $this->form_validation->set_rules('site_id', 'site title', "trim|required|integer");
                $this->form_validation->set_rules('alias', 'alias', "trim|required|xss_clean|url_title|strtolower|unique[content.alias.$id]");
                $this->form_validation->set_rules('title', 'title', "trim|required|xss_clean");
                $this->form_validation->set_rules('category', 'category', "trim|xss_clean");
                $this->form_validation->set_rules('description', 'description', "trim|required");
                $conLink = $this->input->post('content_link');
                if(!empty($conLink)){
                    $this->form_validation->set_rules('content_link', 'link of content', "required|trim|prep_url");
                }
                $this->form_validation->set_rules('keywords', 'keywords', "trim");
                $this->form_validation->set_rules('metadescription', 'meta description', "trim");
                $this->form_validation->set_rules('status', 'status', "trim|required|xss_clean");
                $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
                if ($this->form_validation->run() == TRUE){
                    if(!empty($_FILES['title_image']['name']) && $_FILES['title_image']['error'] == 0){
                        //upload image										
                        $this->load->library('upload');
                        $config['upload_path'] 	 = './uploaded_files/content/';
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size']		 =2048;
                        $config['encrypt_name']  = TRUE;
                        $this->upload->initialize($config);					
                        if($this->upload->do_upload('title_image')){
                            $data_image 		= $this->upload->data();
                            $insert_data['title_image'] = $data_image['file_name'];
                            if(isset($row) && file_exists("./uploaded_files/content/" .$row->title_image) && !is_dir("./uploaded_files/content/" .$row->title_image)){
                                unlink ("./uploaded_files/content/" .$row->title_image);
                            }
                            //for cropping
                            $config['image_library']    = 'gd2';
                            $config['source_image']     = $config['upload_path'] . $data_image['file_name'];
                            $config['create_thumb']     = FALSE;
                            $config['maintain_ratio']   = TRUE;
                            $config['width'] 		= 1024;
                            $config['height'] 		= 768;
                            $this->load->library('image_lib', $config); 
                            $this->image_lib->resize();
                        }else{							
                            $error = $this->upload->display_errors('', '');							
                        }						
                    }
                    if(empty($error)){
                        $insert_data['site_id']		 = $this->input->post('site_id');
                        $insert_data['title']	 	 = $this->input->post('title'); 
                        $insert_data['alias'] 		 = $this->input->post('alias');
                        $insert_data['category_id']	 = $this->input->post('category_id');
                        $insert_data['description']	 = $this->input->post('description'); 
                        $insert_data['content_link']	 = $this->input->post('content_link'); 
                        $insert_data['keywords'] 	 = $this->input->post('keywords');										
                        $insert_data['metadescription']	 = $this->input->post('metadescription'); 
                        $insert_data['status'] 		 = (($this->input->post('status') == "1") ? "1" : "0");	
                        $insert_data['image_status'] 	 = (($this->input->post('imagestatus') == "yes") ? "yes" : "no");
                        if($id == 0){
                            $insert_data['created_by']	 = current_admin_id(); 
                            $insert_data['created_date'] = get_now();
                            $this->db->insert($this->content, $insert_data);
                            $id = $this->db->insert_id();
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        }else{
                            $insert_data['updated_by']	 = current_admin_id(); 
                            $insert_data['updated_date'] = get_now();
                            $this->db->where('id', $id);
                            $this->db->update($this->content, $insert_data);
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        }
                        flash_redirect($this->header['page_name'], $id);
                    }else{
                        $error_mess = $error;
                    }
                }
            }
            $data = $this->_format_data($id);	
            $data['siteId'] = $siteId;
            $data['con_title'] = $this->header['title']; 
            $data['error_mess'] = $error_mess;
            $this->load->view('header', $this->header);
            $this->load->view('menu', $this->data);		
            $this->load->view('cms/add_edit_content', $data);
            $this->load->view('footer');
        }

        function _format_data($id){
            if($this->input->post()){
                $data['id']			= set_value('id');
                $data['site_id']		= set_value('site_id');
                $data['alias']			= set_value('alias');
                $data['title']			= set_value('title');
                $data['description']		= html_entity_decode(set_value('description'));
                $data['content_link']		= set_value('content_link');
                $data['keywords']		= set_value('keywords');
                $data['metadescription'] 	= set_value('metadescription');
                $data['status']			= set_value('status');
                $data['category']		= set_value('category');
                $data['created_by']		= '';
                $data['created_date']		= '';
                $data['updated_by']		= '';
                $data['updated_date']		= '';
                $data['hits']			= 0;
                $data['title_image']		= "";
                $data['imagestatus']		= set_value('imagestatus');
                if($id){
                    $row = $this->cms->get_content($id);				
                    $data['title_image']		= $row->title_image;
                }
            }else if($id != 0){
                $row = $this->cms->get_content($id);
                $this->db->where('id', $row->category_id);
                $result=$this->db->get('categories')->row();
                if( ! $row){
                    $this->session->set_flashdata('class', 'error');
                    $this->session->set_flashdata('msg', "Invalid Request!");
                    flash_redirect($this->header['page_name']);
                }					
                $data['id']			= $row->id;
                $data['site_id']		= $row->site_id;
                $data['alias']			= $row->alias;
                $data['title']			= $row->title;
                $data['description']		= $row->description;
                $data['content_link']		= $row->content_link;
                $data['keywords']		= $row->keywords;
                $data['metadescription']	= $row->metadescription;
                $data['status']			= $row->status;
                $data['category']		= $row->category_id;
                $data['created_by']		= $row->created_by;
                $data['created_date']		= $row->created_date;
                $data['updated_by']		= $row->updated_by;
                $data['updated_date']		= $row->updated_date;
                $data['hits']			= $row->hits;
                $data['title_image']		= $row->title_image;
                $data['imagestatus']            = $row->image_status;
            }else{
                $data['id']			= '';
                $data['site_id']		= $this->input->get('site');
                $data['alias']			= '';
                $data['title']			= '';
                $data['description']		= '';
                $data['content_link']		= '';
                $data['keywords']		= '';
                $data['metadescription']	= '';
                $data['status']			= '';
                $data['category']		= '';
                $data['created_by']		= '';
                $data['created_date']		= '';
                $data['updated_by']		= '';
                $data['updated_date']		= '';
                $data['hits']			= 0;
                $data['title_image']		= '';
                $data['imagestatus']		= '';
            }
            $sites 				= 	$this->administrator_model->get_sites();
            $data['site_names']  =convert_to_dropdown($sites, 'site_title', 'id', 'Select');
            $data['categories'] = $this->cmscategory->get_all_categories();
            
            return $data;			
        }

        function delete($id = NULL){
            $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
            $id = (int)$id;
            //check whether it is used in other tables or not	
            $this->load->library('restrict_delete');
            $params = "";
            if($this->restrict_delete->check_for_delete($params, $id)){
                if($this->input->post('selected')){
                    $selected_ids = $this->input->post('selected');
                    $deleted = 0;
                    foreach($selected_ids as $selectd_id){    
                        $file = $this->global_model->get_single_data($this->content, 'title_image', $selectd_id);
                        $file = './uploaded_files/content/' . $file;
                        $this->db->where('id', $selectd_id);
                        $this->db->delete($this->content);
                        if($this->db->affected_rows() > 0) {
                            $deleted++;	
                            if(file_exists($file))
                                unlink($file);					
                        }
                    }
                    if($deleted){
                        $action['class'] = 'success';
                        $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }else{
                    $file = $this->global_model->get_single_data($this->content, 'title_image', $id);
                    $file = './uploaded_files/content/' . $file;
                    $this->db->where('id', $id);
                    $this->db->delete($this->content);
                    if($this->db->affected_rows() > 0) {   
                        if(file_exists($file))
                            unlink($file);
                        $action['class'] = 'success';
                        $action['msg'] = 'Data deleted successfully!';
                    }else{
                        $action['class'] = 'error';
                        $action['msg'] = 'Error in deleting data!';
                    }
                }
            }else{
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name']);
        }

        function change_status($status = '', $id = NULL){
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', 1);
            $data['status'] = ($status=='1')?"1":"0";
            if($this->input->post('selected')){
                $selected_ids = $this->input->post('selected');
                $changed = 0;
                foreach($selected_ids as $selectd_id){
                    $this->db->where('id', $selectd_id);
                    $this->db->update($this->content, $data);
                    if($this->db->affected_rows() > 0) {
                        $changed++;						
                    }
                }
                if($changed){
                    $action['class'] = 'success';
                    $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' status changed successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }else{
                $this->db->where('id', $id);
                $this->db->update($this->content, $data);
                if($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Status changed successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name'], $id);
        }

    }