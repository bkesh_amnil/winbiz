<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Enterprise_category extends CI_Controller {
      public $header = array();
      var $module = 'module';
      var $profile = 'profile';
      var $profile_detail = 'profile_detail';
      var $enterprise_category = 'enterprise_categories';

      function __Construct(){
          parent::__Construct();
          $this->load->helper('administrator');
          is_already_logged_in();
          $this->header['title']		= "Enterprise Category Management";
          $this->header['page_name']	= $this->router->fetch_class();
          $this->header['stylesheets'] 	= array("960", "reset", "text", "blue","facebox");
          $this->header['head_scripts']   = array("plugins/jquery-1.8.3.min.js");
          $this->header['scripts']        = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js","scripts/form-validation.js","plugins/jquery-validation/dist/jquery.validate.min.js","plugins/jquery-validation/dist/additional-methods.min.js", "cms/enterprise-category.js");
          $this->data['menu_cms']         = TRUE;		//menu
          $this->header['enterprise_category']       = TRUE;		//submenu
          $this->load->library('custom_pagination');
          $this->load->model('admin_enterprise_category_model', 'ent_category');
      }
      
      function index(){
          $data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 
          $start = $this->uri->segment(3);
          $data['start'] = $start;
          $data['rows'] = $this->ent_category->get_enterprise_category(0, TRUE, $start, 'id');
          $this->load->view('header', $this->header);
          $this->load->view('cms/sub_menu');	
          $this->load->view('menu', $this->data);						
          $this->load->view('enterprise/view_enterprise_category', $data);	
          $this->load->view('action');		
          $this->load->view('footer');
      }
      
      function form($id = NULL){
          $id = (int)$id;	
          $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
          $siteId = intval($this->input->get('site'));
          if( $id != 0 ){
              $row = $this->ent_category->get_enterprise_category($id);
              $siteId = $row->site_id;
          }
          $this->header['title'] = "Add / Edit Enterprise Category"; 
          $id = (int)$id;	
          $error_mess = '';		
          $this->load->library('form_validation');
          if ($this->input->post()){
              $this->form_validation->set_rules('enterprise_category_name', 'Enterprise Category Name', "trim|required|xss_clean|unique[".$this->enterprise_category.".enterprise_category_name.$id]");
              $this->form_validation->set_rules('enterprise_category_alias', 'Enterprise Category Alias', "trim|required|xss_clean|unique[".$this->enterprise_category.".enterprise_category_alias.$id]");
              $this->form_validation->set_rules('status', 'Status', 'trim|required|fixed_value[1,0]');
              $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
              if ($this->form_validation->run() == TRUE){
                if(empty($error)){
                  $insert_data['site_id'] = $this->input->post('site_id');
                  $insert_data['enterprise_category_name'] = $this->input->post('enterprise_category_name');
                  $insert_data['enterprise_category_alias'] = $this->input->post('enterprise_category_alias');
                  $insert_data['status'] = $this->input->post('status');

                  if($id == 0){
                      $insert_data['created_by'] = current_admin_id();
                      $insert_data['created_date'] = get_now();
                      if($this->db->insert($this->enterprise_category, $insert_data)) {
                        $enterprise_category_id = $this->db->insert_id();

                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'New data added Successfully.');
                      } else {
                        $this->session->set_flashdata('class', 'error');
                        $this->session->set_flashdata('msg', 'Error in adding data.');
                      }
                  }else{
                      $insert_data['updated_by'] = current_admin_id();
                      $insert_data['updated_date'] = get_now();
                      $this->db->where('id', $id);
                      if($this->db->update($this->enterprise_category, $insert_data)) {

                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'Data Updated Successfully.');
                      } else {
                        $this->session->set_flashdata('class', 'error');
                        $this->session->set_flashdata('msg', 'Error in updating data.');
                      }
                  }
                  flash_redirect(''.$this->header['page_name'], $id);
                }
              }
          }
          $data = $this->_format_data($id);	
          $data['con_title'] = $this->header['title']; 
          $data['error_mess'] = $error_mess;
          $this->load->view('header', $this->header);
          $this->load->view('menu', $this->data);	
          $this->load->view('enterprise/add_edit_enterprise_category', $data);
          $this->load->view('footer');
      }

      function _format_data($id){
        if($this->input->post()){
            $data['id']                               = set_value('id');
            $data['site_id']                          = set_value('site_id');
            $data['enterprise_category_name']         = set_value('enterprise_category_name');
            $data['enterprise_category_alias']	      = set_value('enterprise_category_alias');
            $data['status']                           = set_value('status');
        }elseif($id != 0){
            $row = $this->ent_category->get_enterprise_category($id);
            /*$data['prod_rows'] = $this->ent_category->get_product_cateogry($id);*/
            $data['id']			                          = $row->id;
            $data['site_id']	                        = $row->site_id;
            $data['enterprise_category_name']	        = $row->enterprise_category_name;
            $data['enterprise_category_alias']        = $row->enterprise_category_alias;
            $data['status']		                        = $row->status;
            $data['created_by']                       = $row->created_by;
            $data['created_date']                     = $row->created_date;
            $data['updated_by']                       = $row->updated_by;
            $data['updated_date']                     = $row->updated_date;
        }else{
            $data['id']                               = '';
            $data['site_id']                          = '';
            $data['enterprise_category_name']         = '';
            $data['enterprise_category_alias']        = '';
            $data['status']                           = '';
        }
        return $data;
      }

      function change_status($status = '', $id = NULL){
            $id = (int)$id;
            $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
            if($this->input->post('selected')){
                $data = array('status' => $status);
                $selected_ids = $this->input->post('selected');
                $changed = 0;
                foreach($selected_ids as $selectd_id){
                    $this->db->where('id', $selectd_id);
                    $this->db->update($this->enterprise_category, $data);
                    if($this->db->affected_rows() > 0) {
                        $changed++; 
                        $id = $selectd_id;      
                    }
                }
                if($changed){
                    $action['class'] = 'success';
                    $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }               
            }else {
                $data = array('status' => $status);
                $this->db->where('id', $id);
                $this->db->update($this->enterprise_category, $data);
                if($this->db->affected_rows() > 0) {
                    $action['class'] = 'success';
                    $action['msg'] = 'Status changed successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in changing status!';
                }
            }
            $this->session->set_flashdata($action);
            flash_redirect($this->header['page_name'], $id);
      }
      
      function delete($id = NULL){
          $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
          $id  = (int)$id;
          $this->load->library('restrict_delete');
          $params = "tbl_enterprises.enterprise_category_id";
          if($this->restrict_delete->check_for_delete($params, $id)){
              if($this->input->post('selected')){
                 $selected_ids = $this->input->post('selected') ;
                 $deleted = 0;
                 foreach($selected_ids as $selected_id){
                    $this->db->where('id', $selectd_id);
                    if($this->db->delete($this->enterprise_category))
                      $deleted++;
                 }
                 if($deleted){
                     $action['class'] = 'success';
                     $action['msg'] = $deleted.' out of '.count($selected_ids).' data deleted successfully';
                 }else{
                     $action['class'] = 'error';
                     $action['msg'] = 'Error in Deleting Data';
                 }
              }else{ 
                  $this->db->where('id', $id);
                  if($this->db->delete($this->enterprise_category)) {
                      $action['class'] = 'success';
                      $action['msg'] = 'Data Deleted Successfully';
                  }else{
                      $action['class'] = 'error';
                      $action['msg'] = 'Error in Deleting Data';
                  }
              }
          }else{
              $action['class'] = 'error';
              $action['msg'] = 'This data cannot be deleted. It is being used in system for product categories or enterprise.';
          }
          $this->session->set_flashdata($action);
          flash_redirect(''.$this->header['page_name'], $id);
      }

      /*function view_enterprise_product_categories() {
        $post = $_POST;
        $ent_id = $post['ent_id'];

        $html = '';

        $result = $this->ent_category->get_all_enterprise_product_categories($ent_id);
        if(isset($result) && !empty($result) && is_array($result)) {
          $html .= '<table id="product-multi-table" class="table table-bordered" width="100%">';
          $html .= '<thead><tr>';
          $html .= '<th>Package Name</th>';
          $html .= '<th>Package Alias</th>';
          $html .= '<th>Action</th>';
          $html .= '</tr></thead>';
          $html .= '<tbody>';
          foreach($result as $prod_row) {
            $html .= '<tr class="new-multiple">';
            $html .= '<td>';
            $html .= '<input type="hidden" name="product_name_multiple[]" value="'.$prod_row->product_category_name.'" class="hidden_product_name" />';
            $html .= $prod_row->product_category_name;
            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="hidden" name="product_alias_multiple[]" value="'.$prod_row->product_category_alias.'" class="hidden_product_alias" />';
            $html .= $prod_row->product_category_alias;
            $html .= '</td>';
            $html .= '<td><span class="edit-multiple">[ Edit ]</span><span class="remove-multiple"> [ Remove ]</span></td>';
            $html .= '</tr>';
          }
          $html .= '</tbody></table>';
        }

        echo json_encode($html);
      }*/

      /*function remove_image() {
        $post = $_POST;
        $img = $post['img'];

        if(file_exists("./uploaded_files/enterprise_category/".$img) && !is_dir("./uploaded_files/enterprise_category/".$img)){
            unlink("./uploaded_files/enterprise_category/".$img);
        }
      }*/
  }
?>