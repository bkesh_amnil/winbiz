<?php

class Advertisement_session extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';
    var $advertisement = 'advertisement';
    var $advertisement_session = 'advertisement_session';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Advertisement Session Management";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['advertisement'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('admin_advertisement_model', 'adv');
    }

    function index($ad_id) {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');

        if (!isset($ad_id) || !is_numeric($ad_id)) {
            redirect(base_url('advertisement'));
        }
        $start = $this->uri->segment(4);
        $total_rows = count($this->adv->get_advertisement_session($ad_id));
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;

        $data['ad_data'] = $this->adv->get_advertisements($ad_id);
        $data['rows'] = $this->adv->get_advertisement_session($ad_id, 0, TRUE, $start, 'id');
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/view_advertisement_session', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($ad_id, $id = NULL) {
        $this->load->helper('ckeditor');
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $this->header['con_title'] = "Start Advertisement Session";
        $id = (int) $id;
        $error_mess = '';
        $this->load->library('form_validation');
        if ($this->input->post('save') == "true") {
//            dumparray($_POST);
//            $this->form_validation->set_rules('site_id', 'Site', "trim|required|xss_clean");
            $this->form_validation->set_rules('advertisement_position_id', 'Advertisement Position', "trim|required|xss_clean");
            $this->form_validation->set_rules('publish_date', 'Publish Date', "required");
            $this->form_validation->set_rules('unpublish_date', 'Unpublish Date', "required");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $insert_ad_post['ad_id'] = $ad_id;
                $insert_ad_post['provider_id'] = $this->input->post('provider_id');
                $insert_ad_post['est_price'] = $this->input->post('est_price');
                $insert_ad_post['paid_price'] = $this->input->post('paid_price');
                $insert_ad_post['start_date'] = $this->input->post('publish_date');
                $insert_ad_post['end_date'] = $this->input->post('unpublish_date');
//                $insert_ad_post['est_price'] = $this->adv->calculate_ad_price($insert_ad_post['site_id'], $insert_ad_post['position_id'], $insert_ad_post['start_date'], $insert_ad_post['end_date']);
                if ($id == 0) {
                    $insert_ad_post['site_id'] = $this->input->post('site_id');
                    $insert_ad_post['position_id'] = $this->input->post('advertisement_position_id');
                    $insert_ad_post['created_by'] = current_admin_id();
                    $insert_ad_post['created_date'] = get_now();
                    $this->db->insert($this->advertisement_session, $insert_ad_post);
//                    printQuery();
                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'New data added successfully.');
                } else {
                    $insert_ad_post['updated_by'] = current_admin_id();
                    $insert_ad_post['updated_date'] = get_now();
                    $this->db->where('id', $id);
                    if ($this->db->update($this->advertisement_session, $insert_ad_post)) {
                        $this->session->set_flashdata('class', 'success');
                        $this->session->set_flashdata('msg', 'Data updated successfully.');
                    } else {
                        $this->session->set_flashdata('class', 'error');
                        $this->session->set_flashdata('msg', 'Error in updating data.');
                    }
                }
                redirect(site_url($this->header['page_name'] . "/index/" . $ad_id));
            }
        }
        $data = $this->_format_data($ad_id, $id);
        $data['isEdit'] = FALSE;
        if ($id != '') {
            $data['isEdit'] = TRUE;
        }
        $data['ad_data'] = $this->adv->get_advertisements($ad_id);
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('advertisement/add_edit_advertisement_session', $data);
        $this->load->view('footer');
    }

    function _format_data($ad_id, $id) {
        if ($this->input->post('save') == "true") {
            $data['id'] = set_value('id');
            $data['site_id'] = set_value('site_id');
            $data['advertisement_position_id'] = set_value('advertisement_position_id');
            $data['publish_date'] = $this->input->post('publish_date');
            $data['unpublish_date'] = $this->input->post('unpublish_date');
            $data['status'] = set_value('status');
            $data['est_price'] = $this->input->post('est_price');
            $data['paid_price'] = $this->input->post('paid_price');
        } else if ($id != 0) {
            $row = $this->adv->get_advertisement_session($ad_id, $id);
            $data['id'] = $row->id;
            $data['site_id'] = $row->site_id;
            $data['advertisement_position_id'] = $row->position_id;
            $data['publish_date'] = get_ymd($row->start_date);
            $data['unpublish_date'] = get_ymd($row->end_date);
            $data['created_by'] = $row->created_by;
            $data['created_date'] = ($row->created_date);
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = ($row->updated_date);
            $data['est_price'] = $row->est_price;
            $data['paid_price'] = $row->paid_price;
        } else {
            $data['id'] = '';
            $data['site_id'] = '1';
            $data['advertisement_position_id'] = '';
            $data['publish_date'] = '';
            $data['unpublish_date'] = '';
            $data['est_price'] = '';
            $data['paid_price'] = '';
        }
        $siteNames = $this->administrator_model->get_sites();
        $data['sites'] = convert_to_dropdown($siteNames, 'site_title');

//        $providers = $this->adv->get_advertisement_providers();
//        $data['providers'] = convert_to_dropdown($providers, 'name');

        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->advertisement, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->advertisement, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        //check whether it is used in other tables or not	
        $this->load->library('restrict_delete');
        $params = "";  //change it later
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selectd_id) {
                    $file = $this->global_model->get_single_data_ad($this->advertisement, 'advertisement_image', $selectd_id);
                    $file = './uploaded_files/bigyapan/' . $file;
                    $this->db->where('id', $selectd_id);
                    $this->db->delete($this->advertisement);
                    if ($this->db->affected_rows() > 0) {
                        $deleted++;
                        if (file_exists($file))
                            unlink($file);
                    }
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $file = $this->global_model->get_single_data_ad($this->advertisement, 'advertisement_image', $id);
                $file = './uploaded_files/bigyapan/' . $file;
                $this->db->where('id', $id);
                $this->db->delete($this->advertisement);
                if ($this->db->affected_rows() > 0) {
                    if (file_exists($file))
                        unlink($file);
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                }else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system.';
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function get_ad_price() {
        $site_id = $this->input->post('site_id');
        $publish_date = $this->input->post('publish_date');
        $unpublish_date = $this->input->post('unpublish_date');
        $advertisement_position_id = $this->input->post('advertisement_position_id');
        echo json_encode($this->adv->calculate_ad_price($site_id, $advertisement_position_id, $publish_date, $unpublish_date));
    }

    function check_ad_space() {
        $site_id = $this->input->post('site_id');
        $advertisement_position_id = $this->input->post('advertisement_position_id');
        $now = get_now();
        $query = $this->db->get_where($this->advertisement_session, array('site_id' => $site_id, 'position_id' => $advertisement_position_id, 'start_date <=' => $now, 'end_date >=' => $now));
        if ($query->num_rows == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}

?>