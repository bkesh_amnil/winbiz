<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Setup extends CI_Controller {
	
		function __Construct()
		{
			parent::__Construct();
			
			$this->load->helper('administrator');
			is_already_logged_in();
			
			$this->header['title']			= "CMS Setup";
			$this->header['page_name']		= $this->router->fetch_class();
			
			$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
			//$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
			$this->header['settings'] = TRUE;			//menu
		}
		
		function index($id = NULL)
		{	
			$id = (int)$id;			
			$this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
			
			$data = $this->_format_data();	
			
			$this->load->view('header', $this->header);
			$this->load->view('menu');		
			$this->load->view('settings/cms_setup', $data);
			$this->load->view('footer');
		}
		
		function _format_data()
		{
			$data['folders'] = array('dynamic_uploads', 'uploaded_files',
									'uploaded_files/advertisement', 'uploaded_files/banner', 
									'uploaded_files/content', 'uploaded_files/gallery',
									'uploaded_files/gallery_cover',
									'uploaded_files/material',
									'uploaded_files/news', 'uploaded_files/site_logo', 'uploaded_files/temp'
								);
			$data['tables_except']  = array('globalsetting', 'module', 'validation_rule');
			
			$tables = $this->db->list_tables();
			if ($this->input->post())
			{
				foreach ($tables as $table)
				{
					if( ! in_array($table, $data['tables_except']))
					{	
						//truncate
				  		$this->db->truncate($table);
					}				   
				}
				
				//Create temporary site
				$site['site_title'] = 'Site Title';
				$site['site_name']	= 'site_name';
				$site['main_site']	= '1';
				$this->db->insert('site', $site);
				
				$site_id = $this->db->insert_id();
				
				//Add profile and superadmin username password
				$profile['site_id']			= $site_id;
				$profile['profile_name'] 	= 'Super Administrator';						
				$profile['super_admin']		= 'True';
				$this->db->insert('profile', $profile);						
			
				$admin_user['profile_id'] = $this->db->insert_id(); 
				$admin_user['user_name']  = 'superadmin';
				$admin_user['password']   = md5($admin_user['user_name']);
				$this->db->insert('admin_user', $admin_user);	
				
				//delete files
				foreach ($data['folders'] as $folder)
				{
					if ($handle = opendir($folder)) 
					{
						while (false !== ($entry = readdir($handle))) 
						{
							$file = './' . $folder . '/' . $entry;
							if (is_file($file)) 
							{								
								unlink($file);								
							}
						}
						fopen( './' . $folder .'/index.html', 'w'); //creats index.html
						closedir($handle);
					}
				}	
				
				$session['class'] = 'success';
				$session['msg']   = 'Database has been cleaned up. New superadministrator username and password is ' . $admin_user['user_name'];
				$this->session->set_flashdata($session);
				
				redirect('setup');
			}		
			
			return $data;
		}
	}
