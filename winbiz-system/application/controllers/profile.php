<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $profile_detail = 'profile_detail';

    function __Construct() {
        parent::__Construct();

        $this->load->helper('administrator');
        is_already_logged_in();

        $this->header['title'] = "Manage Profiles";
        $this->header['page_name'] = $this->router->fetch_class();

        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        //$this->header['scripts'] 		= array("jquery-1.7.1.min", "blend/jquery.blend", "ui.core", "ui.sortable", "ui.dialog", "effects");			
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js", "plugins/quicksearch.js");
        $this->data['users'] = TRUE;
        $this->header['profile'] = TRUE;

        $this->load->library('custom_pagination');
        $this->load->model('module_model');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->admin_user_model->get_profiles());

        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $data['rows'] = $this->admin_user_model->get_profiles(0, TRUE, $start, 'profile_name');
        $data['profile_name'] = $this->admin_user_model->get_user_detail(current_admin_id());
        $this->load->view('header', $this->header);
        $this->load->view('menu', $this->data);
        $this->load->view('users/view_profile', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);

        $siteId = intval($this->input->get('site'));
        if ($id != 0) {
            $row = $this->admin_user_model->get_profiles($id);
            $siteId = $row->site_id;
        } else if (empty($siteId)) {
            $this->session->set_flashdata('class', 'error');
            $this->session->set_flashdata('msg', "Please Select Site To Add Profile For.");
            redirect('profile/');
        }
        $this->header['con_title'] = "Add Profile For " . strtoupper($this->global_model->get_single_data("site", "site_name", $siteId));

        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->form_validation->set_rules('site_id', 'site title', "trim|required|integer");
            $this->form_validation->set_rules('profile_name', 'profile name', "trim|required|ucwords");
            $this->form_validation->set_rules('super_admin', 'super administrator', "trim|required|ucwords");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $insert_data['site_id'] = $this->input->post('site_id');
                $insert_data['profile_name'] = $this->input->post('profile_name');
                $insert_data['super_admin'] = $this->input->post('super_admin');
                $insert_data['enterprise_related'] = $this->input->post('enterprise_related');
                if ($id == 0) { //insert
                    $insert_data['created_by'] = current_admin_id();
                    $insert_data['created_date'] = get_now();
                    $this->db->insert($this->profile, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'New data added successfully.');
                } else {
                    $insert_data['modified_by'] = current_admin_id();
                    $insert_data['modified_date'] = get_now();
                    $this->db->where('id', $id);
                    $this->db->update($this->profile, $insert_data);

                    $this->session->set_flashdata('class', 'success');
                    $this->session->set_flashdata('msg', 'Data updated successfully.');
                }
                redirect($this->header['page_name']);
            }
        }

        $data = $this->_format_data($id);
        $data['siteId'] = $siteId;
        $data['user_id'] = current_admin_id();

        $this->load->view('header', $this->header);
        $this->load->view('users/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('users/form_profile', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post()) {
            $data['id'] = set_value('id');
            $data['profile_name'] = set_value('profile_name');
            $data['site_id'] = set_value('site_id');
            $data['super_admin'] = set_value('super_admin');
            $data['enterprise_related'] = set_value('enterprise_related');
            $data['created_by'] = set_value('created_by');
            $data['created_date'] = set_value('created_date');
            $data['updated_by'] = set_value('modified_by');
            $data['updated_date'] = set_value('modified_date');
        } else if ($id != 0) {
            $row = $this->admin_user_model->get_profiles($id);
            if ($row) {
                $data['id'] = $row->id;
                $data['profile_name'] = $row->profile_name;
                $data['site_id'] = $row->site_id;
                $data['super_admin'] = $row->super_admin;
                $data['enterprise_related'] = $row->enterprise_related;
                $data['created_by'] = $row->created_by;
                $data['created_date'] = $row->created_date;
                $data['updated_by'] = $row->modified_by;
                $data['updated_date'] = $row->modified_date;
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Invalid Request!';
                $this->session->set_flashdata($action);
                redirect($this->header['page_name']);
            }
        } else {
            $data['id'] = '';
            $data['profile_name'] = '';
            $data['site_id'] = '';
            $data['super_admin'] = 'False';
            $data['enterprise_related'] = '0';
            $data['created_by'] = '';
            $data['created_date'] = '';
            $data['updated_by'] = '';
            $data['updated_date'] = '';
        }

        //sites start here
        $sites = $this->administrator_model->get_sites();
        $tmp_sites[''] = 'Select Site';
        foreach ($sites as $site) {
            $tmp_sites[$site->id] = $site->site_title;
        }
        $data['site_ids'] = $tmp_sites;
        //sites end here

        $data['super_admins'] = array(
            'False' => 'False',
            'True' => 'True');
        $data['enterprise_relations'] = array(
            '0' => 'No',
            '1' => 'Yes');
        return $data;
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);

        $id = (int) $id;
        $row = $this->admin_user_model->get_profiles($id);
        if (count($row) == 0) {
            $action['class'] = 'error';
            $action['msg'] = 'Invalid Request!';
        } else {
            //check whether it is used in other tables or not	
            $this->load->library('restrict_delete');
            $params = "admin_user.profile_id";

            if ($this->restrict_delete->check_for_delete($params, $id)) {
                $this->db->where('id', $id);
                $this->db->delete($this->profile);

                if ($this->db->affected_rows() > 0) {
                    $this->db->where('profile_id', $id);
                    $this->db->delete($this->profile_detail);

                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'This data cannot be deleted. It is being used in system.';
            }
        }
        $this->session->set_flashdata($action);
        redirect($this->header['page_name']);
    }

    //manage credentials
    function manage_credentials($profile_id = 0) {
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $profile_id);
        $profile_id = (int) $profile_id;
        $row = $this->admin_user_model->get_profiles($profile_id);
        $site_id = $row->site_id;
        if (count($row) == 0 || (int) $profile_id == 0) {
            $action['class'] = 'error';
            $action['msg'] = 'Invalid Request!';
            $this->session->set_flashdata($action);
            redirect('' . $this->header['page_name']);
        }

        if ($this->input->post('module_ids')) {
            $module_ids = $this->input->post('module_ids');
            $view = $this->input->post('view');
            $add = $this->input->post('add_mod');
            $edit = $this->input->post('edit');
            $delete = $this->input->post('delete');
            foreach ($module_ids as $module_id) {
                unset($insert_data);
                unset($profileData);
                $insert_data['view_module'] = (isset($view[$module_id])) ? 1 : 0;
                $insert_data['add_module'] = (isset($add[$module_id])) ? 1 : 0;
                $insert_data['edit_module'] = (isset($edit[$module_id])) ? 1 : 0;
                $insert_data['delete_module'] = (isset($delete[$module_id])) ? 1 : 0;

                $this->db->where('profile_id', $profile_id);
                $this->db->where('module_id', $module_id);
                $profileData = $this->db->get($this->profile_detail)->row();
                if (!isset($profileData) || empty($profileData)) {
                    $insert_data['profile_id'] = $profile_id;
                    $insert_data['module_id'] = $module_id;
                    $this->db->insert($this->profile_detail, $insert_data);
                    $ids[] = $this->db->insert_id();
                } else {
                    $this->db->where('id', $profileData->id);
                    $this->db->update($this->profile_detail, $insert_data);
                    $ids[] = $profileData->id;
                }
            }
            if (isset($ids) && is_array($ids)) {
                $reset_data['view_module'] = 0;
                $reset_data['add_module'] = 0;
                $reset_data['edit_module'] = 0;
                $reset_data['delete_module'] = 0;
                $ids = implode(",", $ids);
                $this->db->where('id NOT IN (' . $ids . ')');
                $this->db->where('profile_id', $profile_id);
                $this->db->update($this->profile_detail, $reset_data);
            }
            $action['class'] = 'success';
            $action['msg'] = 'Credentials changed successfully!';
            $this->session->set_flashdata($action);
            redirect('profile');
        }
        // $enabledModules = $this->global_model->get_single_data("site", "modules_enabled", $site_id);
        $this->db->order_by('module_title');
        $data['modules'] =$c= $this->module_model->getAllParents();
        $data['profile_id'] = $profile_id;
        $data['site_id'] = $site_id;
        $this->header['title'] = "Manage Credentials of Profile '" . $row->profile_name . "'";

        $this->load->view('header', $this->header);
        $this->load->view('users/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('users/manage_credentials', $data);
        $this->load->view('footer');
    }

}
