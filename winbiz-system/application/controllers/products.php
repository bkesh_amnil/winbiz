<?php

class Products extends CI_Controller {

    public $header = array();
    var $module = 'module';
    var $profile = 'profile';
    var $site = 'site';
    var $profile_detail = 'profile_detail';
    var $products = 'products';
    var $product_images = 'product_images';
    var $product_count = 'product_count';

    function __Construct() {
        parent::__Construct();
        $this->load->helper('administrator');
        is_already_logged_in();
        $this->header['title'] = "Product Management";
        $this->header['page_name'] = $this->router->fetch_class();
        $this->header['stylesheets'] = array("960", "reset", "text", "blue");
        $this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
        $this->header['scripts'] = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js", "plugins/breakpoints/breakpoints.js", "plugins/jquery-slimscroll/jquery.slimscroll.min.js", "plugins/jquery.blockui.js", "plugins/jquery.cookie.js", "plugins/uniform/jquery.uniform.min.js", "plugins/data-tables/jquery.dataTables.js", "plugins/data-tables/DT_bootstrap.js", "plugins/fancybox/source/jquery.fancybox.pack.js", "plugins/uniform/jquery.uniform.min.js", "scripts/app.js", "cms/jquery.form.js", "plugins/select2/select2.min.js", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", "plugins/jquery-inputmask/jquery.inputmask.bundle.min.js", "plugins/jquery.input-ip-address-control-1.0.min.js", "scripts/form-components.js", "cms/sortFacebox.js", "cms/jquery.autocomplete.js", "cms/cms_menu.js", "cms/products.js", "swfupload/swfupload.js", "swfupload/swfupload.queue.js", "swfupload/fileprogress.js", "swfupload/handlers.js");
        $this->data['menu_cms'] = TRUE;
        $this->header['products'] = TRUE;
        $this->load->library('custom_pagination');
        $this->load->model('admin_products_model', 'prod');
        $this->load->model('admin_enterprise_category_model', 'ent_category');
        $this->load->model('admin_enterprise_model', 'ent');
        $this->load->model('admin_product_category_model', 'prod_category');
    }

    function index() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'view');
        $start = $this->uri->segment(3);
        $total_rows = count($this->prod->get_products());
        $config = $this->custom_pagination->admin_configuration();
        $config['base_url'] = site_url() . $this->header['page_name'] . '/index';
        $config['total_rows'] = $total_rows;
        $data['start'] = $start;
        $profile_enterprise_id = '0';
        $ent_info = $this->admin_user_model->get_user_enterprise_info(current_admin_id());
        if (!empty($ent_info)) {
            $profile_enterprise_id = $ent_info->ent_id;
        }
        $data['rows'] = $this->prod->get_products(0, TRUE, $start, 'id', $profile_enterprise_id);
        
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('products/view_products', $data);
        $this->load->view('action');
        $this->load->view('footer');
    }

    function form($id = NULL) {
        $this->load->helper('ckeditor');
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $siteId = intval($this->input->get('site'));
        if ($id != 0) {
            $row = $this->prod->get_products($id);
            $siteId = $row->site_id;
        } else if (empty($siteId)) {
            $this->session->set_flashdata('class', 'error');
            $this->session->set_flashdata('msg', "Please Select Site To Add Product For.");
            redirect('products/');
        }
        $this->header['con_title'] = "Add Product For " . strtoupper($this->global_model->get_single_data("site", "site_name", $siteId));
        $id = (int) $id;
        $error_mess = '';

        $this->load->library('form_validation');
        if ($this->input->post('save') == "true") {
            $enterprise_id = $this->input->post('enterprise_id');
            $this->form_validation->set_rules('site_id', 'Site Id', "trim|required|integer");
            $this->form_validation->set_rules('enterprise_id', 'Enterprise', "trim|required|integer");
            $this->form_validation->set_rules('product_category_name', 'Product Category', "trim|required");
            $this->form_validation->set_rules('product_name', 'Product Name', "trim|required|xss_clean|unique_product[" . $this->products . ".product_name.$enterprise_id.$id]");
            $this->form_validation->set_rules('product_alias', 'Product Alias', "trim|required|xss_clean|unique_product[" . $this->products . ".product_alias.$enterprise_id.$id]");
            $this->form_validation->set_rules('show_product_price', 'Show Product Price', "trim|required|fixed_values[1,0]");
            if ($this->input->post('show_product_price') == 1) {
                $this->form_validation->set_rules('product_price', 'Product Price', 'trim|required');
            }
            $this->form_validation->set_rules('status', 'Status', "trim|required|fixed_values[1,0]");
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
            if ($this->form_validation->run() == TRUE) {
                $width = 600;
                $height = 600;
                $imgUpload = false;
                $oldProduct = $this->input->post('uploaded_product_old');
                if (!empty($_FILES['product_cover_image']['name'])) {
                    if (!empty($_FILES['product_cover_image']['name']) && $_FILES['product_cover_image']['error'] == 0) {
                        $filename = $_FILES['product_cover_image']['name'];
                        $filesize_image = $_FILES['product_cover_image']['size'];
                        $file = $_FILES['product_cover_image']['tmp_name'];
                        $file_size = getimagesize($file);
                        $file_width = $file_size[0];
                        $file_height = $file_size[1];
                        $allowed_ext = "jpg,jpeg,gif,png,bmp";
                        $imgInfo = getimagesize($file);
                        $file_type = image_type_to_mime_type(exif_imagetype($file));
                        $allowed_ext = preg_split("/\,/", $allowed_ext);
                        $maxSize = 1024 * 8;
                        $minW = $width;
                        $minH = $height;
                        $for = "products";
                        $uploadPath = "./uploaded_files/products/";

                        if (!is_dir($uploadPath)) {
                            mkdir($uploadPath);
                            chmod($uploadPath, 0777);
                        }
                        if ($filesize_image > ($maxSize * 1024)) {
                            $error = "Image size is too large to accept. Image Size should be less than " . $maxSize . " KB";
                        } else if ($file_width != $width || $file_height != $height) {
                            $error = "Image Size should be equal to " . $width . "X" . $height . " for the position.";
                        } else {
                            switch ($file_type) {
                                case 'image/gif':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".gif";
                                    $upltype = "gif";
                                    break;
                                case 'image/jpeg':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                    $upltype = "jpg";
                                    break;
                                case 'image/jpg':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".jpg";
                                    $upltype = "jpg";
                                    break;
                                case 'image/png':
                                    $fileUploadName = $for . "_" . rand(1, 999) . "_" . time() . ".png";
                                    $upltype = "png";
                                    break;
                                default:
                                    $fileUploadName = "";
                                    break;
                            }
                            if (empty($fileUploadName)) {
                                $error = "Unrecognized File Type";
                            } else {
                                $options = array('jpegQuality' => 100);
                                require_once './phpthumb/ThumbLib.inc.php';
                                $thumb = PhpThumbFactory::create($file, $options);
                                //$thumb->adaptiveResize($file_width, $file_height);
                                $saved = $thumb->save($uploadPath . "/" . $fileUploadName, $upltype);
                                if ($saved) {
                                    $imgUpload = true;
                                    $image = $fileUploadName;
                                }
                            }
                        }
                        if (!$imgUpload) {
                            $uplErr = "";
                            if (!empty($error)) {
                                $uplErr = $error;
                            }
                            $error = "Image was not uploaded.";
                            $error = $error . " " . $uplErr;
                        }
                    } else {
                        $error = "Select Image For Product";
                    }
                } else {
                    if ($id == 0)
                        $error_mess = $error = 'Product Image is Compulsory';
                    $fileUploadName = $oldProduct;
                }

                if (!empty($oldProduct) && !empty($imgUpload)) {
                    if (file_exists("./uploaded_files/products/" . $oldProduct) && !is_dir("./uploaded_files/products/" . $oldProduct)) {
                        unlink("./uploaded_files/products/" . $oldProduct);
                    }
                } else if (!empty($oldProduct)) {
                    $image = $oldProduct;
                }

                if (empty($error)) {
                    $insert_products['site_id'] = $this->input->post('site_id');
                    $insert_products['enterprise_category_id'] = $this->input->post('enterprise_category_id');
                    $insert_products['enterprise_id'] = $this->input->post('enterprise_id');
                    $insert_products['product_category_id'] = $this->prod_category->get_productcategory_id_or_name($this->input->post('product_category_name'), 0);
                    $insert_products['product_name'] = $this->input->post('product_name');
                    $insert_products['product_alias'] = $this->input->post('product_alias');
                    $insert_products['show_product_price'] = $this->input->post('show_product_price');
                    $insert_products['product_price'] = $this->input->post('product_price');
                    $insert_products['product_short_description'] = $this->input->post('product_short_description');
                    $insert_products['product_description'] = $this->input->post('product_description');
                    $insert_products['product_cover_image'] = !empty($image) ? $image : '';
                    $insert_products['status'] = $this->input->post('status');

                    if ($id == 0) {
                        $insert_products['created_by'] = current_admin_id();
                        $insert_products['created_date'] = get_now();

                        if ($this->db->insert($this->products, $insert_products)) {
                            $product_id = $this->db->insert_id();

                            $insert_product_count['product_id'] = $product_id;
                            $insert_product_count['view_count'] = '0';
                            $this->db->insert($this->product_count, $insert_product_count);

                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'New data added successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in adding data.');
                        }
                    } else {
                        $insert_products['updated_by'] = current_admin_id();
                        $insert_products['updated_date'] = get_now();

                        $this->db->where('id', $id);
                        if ($this->db->update($this->products, $insert_products)) {
                            $this->session->set_flashdata('class', 'success');
                            $this->session->set_flashdata('msg', 'Data updated successfully.');
                        } else {
                            $this->session->set_flashdata('class', 'error');
                            $this->session->set_flashdata('msg', 'Error in updating data.');
                        }
                    }
                    flash_redirect($this->header['page_name'], $id);
                } else {
                    $error_mess = $error;
                }
            }
        }
        $data = $this->_format_data($id);

        if ($siteId == 1) {
            $data['enterprise_categories'] = $this->ent_category->get_all_enterprise_categories();
            $data['enterprise'] = $this->ent->get_all_enterprises('');
        } else {
            $ent_id = $this->ent_category->get_site_ent_id($siteId);
            $data['enterprise_categories'] = $this->ent_category->get_all_enterprise_categories_By_id($ent_id->enterprise_id);
            $data['enterprise'] = $ent_id;
        }
        $data['siteId'] = $siteId;
        $data['error_mess'] = $error_mess;
        $this->load->view('header', $this->header);
        $this->load->view('cms/sub_menu');
        $this->load->view('menu', $this->data);
        $this->load->view('products/add_edit_products', $data);
        $this->load->view('footer');
    }

    function _format_data($id) {
        if ($this->input->post('save') == "true") {
            $data['id'] = set_value('id');
            $data['site_id'] = set_value('site_id');
            $data['product_category_name'] = $this->input->post('product_category_name');
            $data['enterprise_category_id'] = $this->input->post('enterprise_category_id');
            $data['enterprise_id'] = $this->input->post('enterprise_id');
            $data['product_category_id'] = $this->input->post('product_category_id');
            $data['product_name'] = $this->input->post('product_name');
            $data['product_alias'] = $this->input->post('product_alias');
            $data['show_product_price'] = set_value('show_product_price');
            $data['product_price'] = set_value('product_price');
            $data['product_short_description'] = html_entity_decode(set_value('product_short_description'));
            $data['product_description'] = html_entity_decode(set_value('product_description'));
            $data['product_cover_image'] = set_value('product_cover_image');
            $data['status'] = set_value('status');
        } else if ($id != 0) {
            $row = $this->prod->get_products($id);
            /* $data['enterprise_category_id'] = $this->prod->get_enterprise_category($id); */
            /* $data['enterprises'] = $this->ent->get_all_enterprises($data['enterprise_category_id']); */
            $prod_cat_name = $this->prod_category->get_productcategory_id_or_name('', $row->product_category_id);
            $data['id'] = $row->id;
            $data['site_id'] = $row->site_id;
            $data['product_category_name'] = $prod_cat_name;
            $data['enterprise_id'] = $row->enterprise_id;
            $data['product_category_id'] = $row->product_category_id;
            $data['enterprise_category_id'] = $row->enterprise_category_id;
            $data['product_name'] = $row->product_name;
            $data['product_alias'] = $row->product_alias;
            $data['show_product_price'] = $row->show_product_price;
            $data['product_price'] = $row->product_price;
            $data['product_short_description'] = $row->product_short_description;
            $data['product_description'] = $row->product_description;
            $data['product_cover_image'] = $row->product_cover_image;
            $data['status'] = $row->status;
            $data['created_by'] = $row->created_by;
            $data['created_date'] = $row->created_date;
            $data['updated_by'] = $row->updated_by;
            $data['updated_date'] = $row->updated_date;
        } else {
            $data['id'] = '';
            $data['site_id'] = '1';
            $data['product_category_name'] = '';
            $data['enterprise_category_id'] = '';
            $data['enterprise_id'] = '';
            $data['product_category_id'] = '';
            $data['product_name'] = '';
            $data['product_alias'] = '';
            $data['show_product_price'] = '';
            $data['product_price'] = '';
            $data['product_short_description'] = '';
            $data['product_description'] = '';
            $data['product_cover_image'] = '';
            $data['status'] = '';
        }
        $data['profile_enterprise_cat_id'] = '';
        $data['profile_enterprise_id'] = '';
        $ent_info = $this->admin_user_model->get_user_enterprise_info(current_admin_id());
        if (!empty($ent_info)) {
            $data['profile_enterprise_cat_id'] = $ent_info->ent_cat_id;
            $data['profile_enterprise_id'] = $ent_info->ent_id;
            $data['enterprises'] = $this->ent->get_all_enterprises($ent_info->ent_cat_id);
        }
        return $data;
    }

    function change_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        if ($this->input->post('selected')) {
            $data = array('status' => $status);
            $selected_ids = $this->input->post('selected');
            $changed = 0;
            foreach ($selected_ids as $selectd_id) {
                $this->db->where('id', $selectd_id);
                $this->db->update($this->products, $data);
                if ($this->db->affected_rows() > 0) {
                    $changed++;
                    $id = $selectd_id;
                }
            }
            if ($changed) {
                $action['class'] = 'success';
                $action['msg'] = $changed . ' out of ' . count($selected_ids) . ' Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        } else {
            $data = array('status' => $status);
            $this->db->where('id', $id);
            $this->db->update($this->products, $data);
            if ($this->db->affected_rows() > 0) {
                $action['class'] = 'success';
                $action['msg'] = 'Status changed successfully!';
            } else {
                $action['class'] = 'error';
                $action['msg'] = 'Error in changing status!';
            }
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function delete($id = NULL) {
        $this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
        $id = (int) $id;
        //check whether it is used in other tables or not   
        $this->load->library('restrict_delete');
        $params = "";       //change it later
        if ($this->restrict_delete->check_for_delete($params, $id)) {
            if ($this->input->post('selected')) {
                $selected_ids = $this->input->post('selected');
                $deleted = 0;
                foreach ($selected_ids as $selectd_id) {
                    $file = $this->global_model->get_single_data_ad($this->products, 'product_cover_image', $selectd_id);
                    $file = './uploaded_files/products/' . $file;
                    $this->db->where('id', $selectd_id);
                    $this->db->delete($this->products);
                    if ($this->db->affected_rows() > 0) {
                        $this->db->where('product_id', $selectd_id);
                        $this->db->delete($this->product_count);

                        $this->db->select('id');
                        $this->db->where('products_id', $selectd_id);
                        $res = $this->db->get($this->product_images)->result();
                        if (isset($res) && !empty($res) && is_array($res)) {
                            foreach ($res as $val) {
                                $file1 = $this->global_model->get_single_data_ad($this->product_images, 'product_image', $val->id);
                                $file1 = './uploaded_files/product_images/' . $file1;
                                $this->db->where('products_id', $selectd_id);
                                $this->db->delete($this->product_images);
                                if ($this->db->affected_rows() > 0) {
                                    if (file_exists($file1)) {
                                        unlink($file1);
                                    }
                                }
                            }
                        }
                        $deleted++;
                        if (file_exists($file))
                            unlink($file);
                    }
                }
                if ($deleted) {
                    $action['class'] = 'success';
                    $action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            } else {
                $file = $this->global_model->get_single_data_ad($this->products, 'product_cover_image', $id);
                $file = './uploaded_files/products/' . $file;
                $this->db->where('id', $id);
                $this->db->delete($this->products);
                if ($this->db->affected_rows() > 0) {
                    if (file_exists($file))
                        unlink($file);

                    $this->db->where('product_id', $id);
                    $this->db->delete($this->product_count);

                    $this->db->select('id');
                    $this->db->where('products_id', $id);
                    $res1 = $this->db->get($this->product_images)->result();
                    if (isset($res1) && !empty($res1) && is_array($res1)) {
                        foreach ($res1 as $val1) {
                            $file2 = $this->global_model->get_single_data_ad($this->product_images, 'product_image', $val1->id);
                            $file2 = './uploaded_files/product_images/' . $file2;
                            $this->db->where('products_id', $id);
                            $this->db->delete($this->product_images);
                            if ($this->db->affected_rows() > 0) {
                                if (file_exists($file2)) {
                                    unlink($file2);
                                }
                            }
                        }
                    }
                    $action['class'] = 'success';
                    $action['msg'] = 'Data deleted successfully!';
                } else {
                    $action['class'] = 'error';
                    $action['msg'] = 'Error in deleting data!';
                }
            }
        } else {
            $action['class'] = 'error';
            $action['msg'] = 'This data cannot be deleted. It is being used in system.';
        }
        $this->session->set_flashdata($action);
        flash_redirect($this->header['page_name'], $id);
    }

    function get_dropdown_values() {
        $post = $_POST;
        $html = '';

        if ($post['type'] == 'enterprise') {
            $result = $this->ent->get_all_enterprises($post['id']);

            if (isset($result) && !empty($result) && is_array($result)) {
                foreach ($result as $res) {
                    /* $selected = '';
                      if(isset($post['option_id']) && $post['option_id'] != '') {
                      if($res->id == $post['option_id']) {
                      $selected = ' selected="selected"';
                      }
                      } */
                    $html .= '<option value="' . $res->id . '">' . $res->enterprise_name . '</option>';
                }
            }
        } else {
            $result1 = $this->ent_category->get_all_enterprise_product_categories($post['id']);

            if (isset($result1) && !empty($result1) && is_array($result1)) {
                foreach ($result1 as $res1) {
                    /* $selected = '';
                      if(isset($post['option_id']) && $post['option_id'] != '') {
                      if($res1->id == $post['option_id']) {
                      $selected = ' selected="selected"';
                      }
                      } */
                    $html .= '<option value="' . $res1->id . '">' . $res1->product_category_name . '</option>';
                }
            }
        }

        echo $html;
    }

    function remove_image() {
        $post = $_POST;
        $img = $post['img'];

        if (file_exists("./uploaded_files/products/" . $img) && !is_dir("./uploaded_files/products/" . $img)) {
            unlink("./uploaded_files/products/" . $img);
        }
    }

    /* funciton for flash multi upload */

    function images($id = NULL) {
        $getId = intval($this->input->get('id'));
        if ($getId > 0) {
            $id = $getId;
        }
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
        $this->header['con_title'] = "Upload Images For " . $this->global_model->get_single_data("products", "product_name", $id);
        $data['images'] = $this->prod->get_available_product_images($id);
        $data['product_id'] = $id;
        $this->load->view('header', $this->header);
        $this->load->view('menu', $this->data);
        $this->load->view('products/uploadImages', $data);
        $this->load->view('footer');
    }

    function uploadImages($productId) {
        $post = $this->input->post();
        $imageProperties = (isset($post['imageProp']) ? $post['imageProp'] : "");
        if (!empty($imageProperties) && is_array($imageProperties)) {
            foreach ($imageProperties as $ind => $val) {
                if (file_exists('uploaded_files/temp/' . $val['file'])) {
                    chmod("uploaded_files/temp/" . $val['file'], 0777);
                    if (copy("./uploaded_files/temp/" . $val['file'], "./uploaded_files/product_images/" . $val['file'])) {
                        unlink("./uploaded_files/temp/" . $val['file']);
                        chmod('uploaded_files/product_images/' . $val['file'], 0777);
                        unset($insert_data);
                        $insert_data['products_id'] = $productId;
                        $insert_data['product_image'] = $val['file'];
                        $insert_data['product_image_title'] = $val['title'];
                        $insert_data['product_image_description'] = $val['alt'];
                        $insert_data['created_by'] = current_admin_id();
                        $insert_data['created_date'] = get_now();
                        if (!$this->db->insert($this->product_images, $insert_data)) {
                            unlink('uploaded_files/product_images/' . $val['file']);
                        }
                    }
                }
            }
        }
        $imagePropertiesEdit = (isset($post['editProp']) ? $post['editProp'] : "");
        if (!empty($imagePropertiesEdit) && is_array($imagePropertiesEdit)) {
            foreach ($imagePropertiesEdit as $ind => $val) {
                unset($insert_data);
                $insert_data['product_image_title'] = $val['title'];
                $insert_data['product_image_description'] = $val['alt'];
                $insert_data['updated_by'] = current_admin_id();
                $insert_data['updated_date'] = get_now();
                $this->db->where('id', $ind);
                $this->db->update($this->product_images, $insert_data);
            }
        }
        $this->session->set_flashdata('class', 'success');
        $this->session->set_flashdata('msg', 'Images Description Updated.');
        redirect($this->header['page_name'] . "/images?id=" . $productId);
    }

    function deleteImages() {
        $data = $this->admin_user_model->access_module($this->header['page_name'], 'delete');
        if ($this->input->get('file')) {
            $file = "./uploaded_files/temp/" . $this->input->get('file');
            if (file_exists($file)) {
                unlink($file);
            }
            $msg = "Deleted Successfully";
        } else if ($this->input->get('id')) {
            $id = $this->input->get('id');
            $this->db->where('id', $id);
            $row = $this->db->get($this->product_images)->row();
            $image = "./uploaded_files/product_images/" . $row->product_image;
            if (!is_dir($image) && file_exists($image)) {
                unlink($image);
            }
            $this->db->where('id', $id);
            $this->db->delete($this->product_images);
            $msg = "Deleted Successfully";
        } else {
            $msg = "No Image To Delete";
        }
        echo $msg;
    }

    function change_image_status($status = '', $id = NULL) {
        $id = (int) $id;
        $this->admin_user_model->access_module($this->header['page_name'], 'add/edit', 1);
        $data['status'] = ($status == '1') ? "1" : "0";
        $this->db->where('id', $id);
        $this->db->update($this->product_images, $data);
        if ($this->db->affected_rows() > 0) {
            echo 'success';
        } else {
            echo 'error';
        }
    }

}

?>