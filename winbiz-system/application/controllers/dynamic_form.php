<?php

class Dynamic_form extends CI_Controller {
               
	function __construct()
	{
 		parent::__construct();
		
		$this->load->helper('administrator');
		is_already_logged_in();
		
		$this->header['title']			= "Dynamic Form Management";
		$this->header['page_name']		= $this->router->fetch_class();
		
		$this->header['stylesheets'] 	= array("960", "reset", "text", "blue");
			$this->header['head_scripts'] = array("plugins/jquery-1.8.3.min.js");
            $this->header['scripts']      = array("plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js", "plugins/bootstrap/js/bootstrap.min.js","plugins/breakpoints/breakpoints.js","plugins/jquery-slimscroll/jquery.slimscroll.min.js","plugins/jquery.blockui.js","plugins/jquery.cookie.js","plugins/uniform/jquery.uniform.min.js","plugins/data-tables/jquery.dataTables.js","plugins/data-tables/DT_bootstrap.js","plugins/fancybox/source/jquery.fancybox.pack.js","plugins/uniform/jquery.uniform.min.js","scripts/app.js","cms/jquery.form.js","plugins/select2/select2.min.js","plugins/bootstrap-datepicker/js/bootstrap-datepicker.js","plugins/jquery-inputmask/jquery.inputmask.bundle.min.js","plugins/jquery.input-ip-address-control-1.0.min.js","scripts/form-components.js","cms/sortFacebox.js","cms/jquery.autocomplete.js", "cms/cms_menu.js");
		$this->data['menu_cms'] = TRUE;		//menu
		$this->header['dynamic_form'] = TRUE;		//submenu
		
		$this->load->model('dynamic_form/dynamic_form_model');
	}	
	
	function index()
	{
		$data = $this->admin_user_model->access_module($this->header['page_name'], 'view'); 		
		
		$this->load->library('custom_pagination');		//add this
		
		$start = $this->uri->segment(3);
		$total_rows = count($this->dynamic_form_model->get_forms());				//change here
	
		$config = $this->custom_pagination->admin_configuration();
		$config['base_url'] = site_url() . '' . $this->header['page_name'] . '/index';			
		$config['total_rows'] = $total_rows;
		//$this->pagination->initialize($config);
		
		$data['start'] = $start;
		$data['result'] = $this->dynamic_form_model->get_forms(0, TRUE, $start, 'form_title');	//change here
		$data['page_name'] = $this->header['page_name'];
		
		$this->load->view('header', $this->header);
		$this->load->view('cms/sub_menu');	
		$this->load->view('menu', $this->data);	
		$this->load->view('dynamic_form/dynamic_form_grid', $data);	
		$this->load->view('action');			
		$this->load->view('footer');	
	}
	
	function form($id = NULL)
	{		
		$id = (int)$id;
		
		$this->admin_user_model->access_module($this->header['page_name'], 'add/edit', $id);
		
		$this->form_validation->set_rules('site_id', 'Site', 'required|integer|exists[site.id]');			
		$this->form_validation->set_rules('form_title', 'Form Title', 'required|trim|xss_clean|max_length[255]');			
		$this->form_validation->set_rules('form_name', 'Form Name', 'required|trim|xss_clean|max_length[50]|url_title|alpha_dash');			
		$this->form_validation->set_rules('form_related', 'Form Relation', 'trim|integer|xss_clean');			
		$this->form_validation->set_rules('form_relation_link', 'Relation Link Text', 'trim|xss_clean');			
		$this->form_validation->set_rules('form_description', 'Form Description', 'trim');			
		$this->form_validation->set_rules('form_class', 'Form Class', 'trim|xss_clean|max_length[255]');			
		$this->form_validation->set_rules('form_attribute', 'Attributes', 'trim|xss_clean|max_length[255]');			
		$this->form_validation->set_rules('form_status', 'Form Status', "required|trim|fixed_values[Published,Unpublished]");			
		$this->form_validation->set_rules('submit_action', 'Submit Action', "required|fixed_values[email,database,both]");			
		$this->form_validation->set_rules('success_msg', 'Success Message', 'trim');			
		$this->form_validation->set_rules('admin_email', 'Admin Email', 'trim|valid_email|max_length[255]');			
		$this->form_validation->set_rules('admin_email_msg', 'Admin Email Message', '');			
		$this->form_validation->set_rules('email_to_user', 'Email to user', 'is_numeric|fixed_values[0,1]');			
		$this->form_validation->set_rules('user_email_msg', 'User Email Message', '');
			
		$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a data-dismiss="alert" class="alert-close close">×</a><span class="info_inner">', '</span></div>');
	
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{			
			$data = $this->_format($id);
			$data['availableForms'] = $this->dynamic_form_model->get_available_forms($id, $data['site_id']);	//change here
			$this->load->view('header', $this->header);
			$this->load->view('cms/sub_menu');	
			$this->load->view('menu', $this->data);				
			$this->load->view('dynamic_form/add_edit_form', $data);
			$this->load->view('footer');
		}
		else // passed validation proceed to post success logic
		{
		 	// build array for the model			
			$form_data = array(
							'site_id'	=> set_value('site_id'),
					       	'form_title' => set_value('form_title'),
					       	'form_name' => set_value('form_name'),
					       	'form_related' => set_value('form_related'),
					       	'form_relation_link' => set_value('form_relation_link'),
					       	'form_description' => html_entity_decode(set_value('form_description')),
					       	'form_class' => set_value('form_class'),
					       	'form_attribute' => set_value('form_attribute'),
					       	'form_status' => set_value('form_status'),
					       	'submit_action' => set_value('submit_action'),
					       	'success_msg' => set_value('success_msg'),
					       	'admin_email' => set_value('admin_email'),
					       	'admin_email_msg' => set_value('admin_email_msg'),
					       	'email_to_user' => set_value('email_to_user'),
					       	'user_email_msg' => set_value('user_email_msg')
						);
					
			// run insert model to write data to db
		
			if ($this->dynamic_form_model->SaveForm($form_data, $id) == TRUE) 
			{
				flash_redirect(''.$this->header['page_name'], $id);
			}
			else
			{
				redirect(''.$this->header['page_name']);
			}
		}
	}
	
	function _format($id)
	{
		if($this->input->post())
		{
			$data['site_id']		= set_value('site_id');
			$data['form_title']		= set_value('form_title');
			$data['form_name']		= set_value('form_name');
			$data['form_related']	= set_value('form_related');
			$data['form_relation_link']	= set_value('form_relation_link');
			$data['form_description']	= set_value('form_description');
			$data['form_class'] 	= set_value('form_class');
			$data['form_attribute'] = set_value('form_attribute');
			$data['form_status'] 	= set_value('form_status');
			$data['submit_action'] 	= set_value('submit_action');
			$data['success_msg'] 	= set_value('success_msg');
			$data['admin_email'] 	= set_value('admin_email');
			$data['admin_email_msg']= set_value('admin_email_msg');
			$data['email_to_user'] 	= set_value('email_to_user');	
			$data['user_email_msg'] = set_value('user_email_msg');	
		}
		else if($id != 0)
		{
			$row = $this->db->where('id', $id)->get('form')->row();
			if($row)
			{
				$data['site_id']		= $row->site_id;
				$data['form_title']		= $row->form_title;
				$data['form_name']		= $row->form_name;
				$data['form_related']	= $row->form_related;
				$data['form_relation_link']	= $row->form_relation_link;
				$data['form_description']	= $row->form_description;
				$data['form_class'] 	= $row->form_class;
				$data['form_attribute'] = $row->form_attribute;
				$data['form_status'] 	= $row->form_status;
				$data['submit_action'] 	= $row->submit_action;
				$data['success_msg'] 	= $row->success_msg;
				$data['admin_email'] 	= $row->admin_email;
				$data['admin_email_msg']= $row->admin_email_msg;
				$data['email_to_user'] 	= $row->email_to_user;	
				$data['user_email_msg'] = $row->user_email_msg;
			}
			else 
			{
				$action['class'] = 'error';
				$action['msg'] = 'Invalid Request!';
				$this->session->set_flashdata($action);
				redirect(''.$this->header['page_name']);
			}
		}
		else
		{
			$data['site_id']		= $this->input->get('site');
			$data['form_title']		= '';
			$data['form_name']		= '';
			$data['form_related']	= '';
			$data['form_relation_link']	= '';
			$data['form_description']	= '';
			$data['form_class'] 	= '';
			$data['form_attribute'] = '';
			$data['form_status'] 	= '';
			$data['submit_action'] 	= '';
			$data['success_msg'] 	= '';
			$data['admin_email'] 	= '';
			$data['admin_email_msg']= '';
			$data['email_to_user'] 	= '';	
			$data['user_email_msg'] = '';	
		}

		$data['statuses'] = array('Published' => 'Published', 'Unpublished' => 'Unpublished');
		$data['submit_actions'] = array('database' => 'Save to database', 'email' => 'Send Email', 'both' => 'Save to database and send email');
		$data['email_to_users'] = array('No', 'Yes');
		$sites  = $this->administrator_model->get_sites();
		$data['sites'] = dropdown_data($sites, 'id', 'site_title');		
		return $data;
	}
	
	function delete($id = NULL)
	{		
		$this->admin_user_model->access_module($this->header['page_name'], 'delete', $id);
	
		$id = (int)$id;
		
		//check whether it is used in other tables or not	
		$this->load->library('restrict_delete');
		$params = "";
		
		if($this->input->post('selected'))
		{
			$selected_ids = $this->input->post('selected');
			
			$deleted = 0;
			foreach($selected_ids as $selectd_id)
			{   
				if($this->restrict_delete->check_for_delete($params, $selectd_id))
				{
					$this->db->where('id', $selectd_id);
					$this->db->delete('form');
				
					if($this->db->affected_rows()) 
					{
						$deleted++;	
						$fields = $this->db->where('form_id', $id)->get('form_field')->result();
						foreach($fields as $field)
						{
							$this->db->where('form_field_id', $field->id);
							$this->db->delete('form_field_value');						
						}
						$this->db->where('form_id', $id);
						$this->db->delete('form_field');			
					}
				}
			}
			if($deleted)
			{
				$action['class'] = 'success';
				$action['msg'] = $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully!';
			} 
			else 
			{
				$action['class'] = 'error';
				$action['msg'] = 'Error in deleting data!';
			}
		}
		else
		{			
			if($this->restrict_delete->check_for_delete($params, $id))
			{	
				$this->db->where('id', $id);
				$this->db->delete('form');
				
				if($this->db->affected_rows())
				{
					$fields = $this->db->where('form_id', $id)->get('form_field')->result();
					foreach($fields as $field)
					{
						$this->db->where('form_field_id', $field->id);
						$this->db->delete('form_field_value');						
					}
					$this->db->where('form_id', $id);
					$this->db->delete('form_field');
				
					$action['class'] = 'success';
					$action['msg'] = 'Data deleted successfully!';
				}			
				else 
				{
					$action['class'] = 'error';
					$action['msg'] = 'Error in deleting data!';
				}
			}
			else 
			{
				$action['class'] = 'error';
				$action['msg'] = 'This data cannot be deleted. It is being used in system.';
			}
		}
				
		$this->session->set_flashdata($action);
		redirect(''.$this->header['page_name']);	
	}
	
	function change_status($status = '', $id = NULL)
	{
		$id = (int)$id;
		$this->admin_user_model->access_module($this->header['page_name'], 'add/edit', 1);
		
		$data['form_status'] = ($status=='1')?"1":"0";
		
		if($this->input->post('selected'))
		{
			$selected_ids = $this->input->post('selected');
			
			$changed = 0;
			foreach($selected_ids as $selectd_id)
			{
				$this->db->where('id', $selectd_id);
				$this->db->update('form', $data);
				
				if($this->db->affected_rows() > 0) 
				{
					$changed++;						
				}
			}
			if($changed)
			{
				$action['class'] = 'success';
				$action['msg'] = $changed . ' out of ' . count($selected_ids) . ' status changed successfully!';
			} 
			else 
			{
				$action['class'] = 'error';
				$action['msg'] = 'Error in changing status!';
			}
		}
		else 
		{
			$this->db->where('id', $id);
			$this->db->update('form', $data);
			
			if($this->db->affected_rows() > 0) {
				$action['class'] = 'success';
				$action['msg'] = 'Status changed successfully!';
			} else {
				$action['class'] = 'error';
				$action['msg'] = 'Error in changing status!';
			}
		}
		$this->session->set_flashdata($action);
		flash_redirect(''.$this->header['page_name'], $id);
	}
	
}
?>