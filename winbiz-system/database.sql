-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 13, 2013 at 03:50 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_cms_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}admin_user`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}admin_user` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `profile_id` int(100) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` enum('Female','Male') NOT NULL DEFAULT 'Male',
  `mobile_num` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}advertisement`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `alias` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `file` varchar(800) NOT NULL,
  `link_type` enum('menu','content','url','none') NOT NULL DEFAULT 'none',
  `link_id` int(11) NOT NULL,
  `link_url` varchar(800) NOT NULL,
  `opens` enum('same','new') NOT NULL DEFAULT 'same',
  `position` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `{PREFIX}categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(800) NOT NULL,
  `category_alias` varchar(800) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `is_default` enum('yes','no') NOT NULL DEFAULT 'no',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `{PREFIX}cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `show_dashboard` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `{PREFIX}cms_menu`
--

INSERT INTO `{PREFIX}cms_menu` (`id`, `name`, `controller`, `parent_id`, `show_dashboard`, `status`) VALUES
(1, 'Dashboard', 'dashboard', 0, '0', 'yes'),
(2, 'Users', 'user', 0, '0', 'yes'),
(3, 'Manage Profiles', 'profile', 2, '0', 'yes'),
(4, 'Manage Admins', 'admin_user', 2, '0', 'yes'),
(5, 'Backend Settings', 'backend', 0, '0', 'yes'),
(6, 'Manage Sites', 'sites', 5, '0', 'yes'),
(7, 'Manage Modules', 'modules', 5, '0', 'yes'),
(8, 'Settings', 'settings', 0, '0', 'yes'),
(9, 'CMS', 'cms', 0, '0', 'yes'),
(10, 'Menu', 'menu', 9, '1', 'yes'),
(11, 'Content', 'content', 9, '1', 'yes'),
(12, 'News', 'news', 9, '1', 'yes'),
(13, 'Gallery', 'gallery', 9, '1', 'yes'),
(14, 'Video', 'video', 9, '0', 'yes'),
(15, 'Banner', 'banner', 9, '1', 'yes'),
(16, 'Advertisement', 'advertisement', 9, '1', 'yes'),
(17, 'Material', 'material', 9, '1', 'yes'),
(24, 'My Profile', 'my_profile', 0, '0', 'yes'),
(25, 'My Profile', 'my_profile', 24, '0', 'yes'),
(26, 'Forms', 'dynamic_form', 9, '1', 'yes'),
(27, 'Change Password', 'my_profile/change_password', 24, '1', 'yes'),
(28, 'Form Fields', 'form_fields', 9, '1', 'no'),
(29, 'Category', 'category', 9, '1', 'yes'),
(30, 'Newsletter', 'newsletter', 9, '0', 'yes');

CREATE TABLE IF NOT EXISTS `{PREFIX}banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `alias` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `file` varchar(800) NOT NULL,
  `link_type` enum('menu','content','url','none') NOT NULL DEFAULT 'none',
  `link_id` int(11) NOT NULL,
  `link_url` varchar(800) NOT NULL,
  `opens` enum('same','new') NOT NULL DEFAULT 'same',
  `position` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}categories`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(800) NOT NULL,
  `category_alias` varchar(800) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}content`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `alias` varchar(800) NOT NULL,
  `title` varchar(800) NOT NULL,
  `description` text NOT NULL,
  `content_link` varchar(800) NOT NULL,
  `category_id` int(11) NOT NULL,
  `keywords` varchar(800) NOT NULL,
  `metadescription` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `hits` int(11) NOT NULL,
  `title_image` varchar(255) NOT NULL,
  `image_status` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}countries`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `printable_name` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `{PREFIX}countries`
--

INSERT INTO `{PREFIX}countries` (`id`, `iso`, `name`, `printable_name`, `iso3`, `numcode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152),
(44, 'CN', 'CHINA', 'China', 'CHN', 156),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188),
(53, 'CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352),
(99, 'IN', 'INDIA', 'India', 'IND', 356),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417),
(116, 'LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600),
(168, 'PE', 'PERU', 'Peru', 'PER', 604),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716);

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}field_type`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}field_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type` varchar(50) CHARACTER SET latin1 NOT NULL,
  `display_text` varchar(100) CHARACTER SET latin1 NOT NULL,
  `multiple_values` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `{PREFIX}field_type`
--

INSERT INTO `{PREFIX}field_type` (`id`, `field_type`, `display_text`, `multiple_values`) VALUES
(1, 'input', 'Text box', 0),
(2, 'password', 'Password', 0),
(3, 'radio', 'Radio Button', 1),
(4, 'select', 'Dropdown Menu', 1),
(5, 'upload', 'Upload Files', 0),
(6, 'textarea', 'Textarea', 0),
(7, 'checkbox', 'Checkbox', 1),
(8, 'multiple_upload', 'Multiple Uploads', 0);

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(100) NOT NULL,
  `form_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `form_related` varchar(255) NOT NULL,
  `form_relation_link` varchar(255) NOT NULL,
  `form_description` text NOT NULL,
  `form_unique_name` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'Format = form_name-id',
  `form_title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `form_class` varchar(255) CHARACTER SET latin1 NOT NULL,
  `form_attribute` varchar(255) CHARACTER SET latin1 NOT NULL,
  `form_status` enum('Published','Unpublished') CHARACTER SET latin1 NOT NULL,
  `category_id` int(11) NOT NULL,
  `submit_action` enum('email','database','both') CHARACTER SET latin1 NOT NULL DEFAULT 'database',
  `success_msg` tinytext CHARACTER SET latin1 NOT NULL,
  `admin_email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `admin_email_msg` text CHARACTER SET latin1 NOT NULL,
  `email_to_user` tinyint(1) NOT NULL,
  `user_email_msg` text CHARACTER SET latin1 NOT NULL,
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(100) NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form_field`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `default_value` text NOT NULL,
  `field_attribute` varchar(255) NOT NULL,
  `field_class` varchar(100) NOT NULL,
  `field_order` int(5) NOT NULL,
  `show_in_grid` tinyint(1) NOT NULL,
  `front_display` tinyint(1) NOT NULL,
  `validation_rule` varchar(255) NOT NULL COMMENT 'separated by |',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form_field_value`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form_field_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_field_id` int(11) NOT NULL,
  `display_text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form_group`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_parent` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `status` enum('published','unpublished') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form_submission`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form_submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}form_submission_fields`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}form_submission_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_submission_id` int(11) NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_field_value` text NOT NULL,
  `form_display_text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}gallery`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `date` varchar(255) NOT NULL,
  `alias` varchar(500) NOT NULL,
  `cover_image` varchar(800) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `resize` tinyint(20) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}globalsetting`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}globalsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `multiple_sites` enum('yes','no') NOT NULL,
  `site_structure` enum('sub_domain','folder') NOT NULL,
  `site_limits` int(11) NOT NULL,
  `user_restriction` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `{PREFIX}globalsetting`
--

INSERT INTO `{PREFIX}globalsetting` (`id`, `multiple_sites`, `site_structure`, `site_limits`, `user_restriction`) VALUES
(1, 'no', 'sub_domain', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}images`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gal_id` int(11) NOT NULL,
  `image_name` varchar(800) NOT NULL,
  `image_alt` varchar(800) NOT NULL,
  `image_title` varchar(800) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}material`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `alias` varchar(500) NOT NULL,
  `file` varchar(800) NOT NULL,
  `material_image` varchar(255) NOT NULL,
  `original_filename` varchar(800) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}menu`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_type_id` int(11) NOT NULL COMMENT '{PREFIX}site_menus.id',
  `menu_title` varchar(255) NOT NULL,
  `menu_alias` varchar(255) NOT NULL,
  `menu_keywords` tinytext NOT NULL,
  `menu_description` text NOT NULL,
  `menu_image` varchar(255) NOT NULL,
  `product_img` varchar(255) NOT NULL,
  `menu_heading` varchar(800) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `menu_position` int(11) NOT NULL,
  `menu_link_type` enum('url','page','site','none') NOT NULL DEFAULT 'none',
  `menu_opens` enum('new','same') NOT NULL DEFAULT 'same',
  `menu_url` varchar(800) NOT NULL,
  `menu_site` int(11) NOT NULL,
  `banner` tinyint(1) NOT NULL,
  `selected_banners` varchar(800) NOT NULL,
  `side_menu_id` int(11) NOT NULL COMMENT '{PREFIX}site_menus.id',
  `footer_menu_id` int(11) NOT NULL COMMENT '{PREFIX}site_menus.id',
  `side_menus` varchar(800) NOT NULL,
  `side_menu_title` varchar(800) NOT NULL,
  `footer_menus` varchar(800) NOT NULL,
  `footer_menu_title` varchar(800) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  `page_title` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}menu_page_settings`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}menu_page_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_title` varchar(800) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `content_ids` varchar(800) NOT NULL,
  `category_id` int(11) NOT NULL,
  `all_within_category` enum('yes','no') NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}module`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}module` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `module_title` varchar(255) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `global_module` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 if it can be accessed only by super super admin',
  `public_module` enum('yes','no') NOT NULL DEFAULT 'no' COMMENT 'these modules content are seen in front',
  `deletable` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `{PREFIX}module`
--

INSERT INTO `{PREFIX}module` (`id`, `module_title`, `module_name`, `global_module`, `public_module`, `deletable`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 'Manage Modules', 'modules', 0, 'no', 0, 0, '0000-00-00 00:00:00', 2, '2012-06-26 15:53:29'),
(2, 'Settings', 'settings', 1, 'no', 1, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(4, 'Manage Sites', 'sites', 0, 'no', 1, 0, '0000-00-00 00:00:00', 1, '2012-06-15 16:40:13'),
(5, 'Backend Settings', 'backend', 0, 'no', 1, 0, '0000-00-00 00:00:00', 1, '2012-06-26 15:54:02'),
(6, 'Users', 'users', 1, 'no', 1, 0, '0000-00-00 00:00:00', 1, '2012-09-06 15:10:46'),
(7, 'Manage Profile', 'profile', 1, 'no', 1, 2, '2012-06-13 15:51:44', 0, '0000-00-00 00:00:00'),
(8, 'Administrator Users', 'admin_user', 1, 'no', 1, 2, '2012-06-14 12:21:08', 1, '2012-12-14 15:23:03'),
(9, 'CMS', 'cms', 1, 'no', 1, 1, '2012-06-26 15:00:07', 0, '0000-00-00 00:00:00'),
(10, 'Menu', 'menu', 1, 'no', 1, 1, '2012-06-26 15:19:50', 0, '0000-00-00 00:00:00'),
(11, 'Content', 'content', 1, 'yes', 1, 1, '2012-06-26 15:20:05', 0, '0000-00-00 00:00:00'),
(12, 'Gallery', 'gallery', 1, 'yes', 1, 1, '2012-06-26 15:20:19', 0, '0000-00-00 00:00:00'),
(13, 'Banner', 'banner', 1, 'yes', 1, 1, '2012-06-26 15:20:27', 1, '2012-12-14 15:15:21'),
(14, 'News', 'news', 0, 'yes', 1, 1, '2012-06-26 15:20:35', 1, '2012-12-14 15:25:24'),
(15, 'Advertisement', 'advertisement', 1, 'yes', 1, 1, '2012-06-26 15:20:57', 0, '0000-00-00 00:00:00'),
(16, 'Material', 'material', 1, 'yes', 1, 1, '2012-08-01 14:49:47', 0, '0000-00-00 00:00:00'),
(17, 'Dynamic Forms', 'dynamic_form', 1, 'yes', 1, 1, '2012-08-23 15:02:36', 0, '0000-00-00 00:00:00'),
(18, 'Dynamic Form Data', 'form_fields', 1, 'yes', 1, 1, '2012-08-24 14:04:49', 1, '2012-09-03 18:16:00'),
(19, 'Form Controller', 'form_controller', 1, 'no', 1, 1, '2012-08-24 15:29:35', 0, '0000-00-00 00:00:00'),
(20, 'Database Clean Up', 'clean_database', 1, 'no', 1, 1, '2012-10-19 12:56:32', 0, '0000-00-00 00:00:00'),
(21, 'Manage Video', 'video', 1, 'yes', 1, 1, '2012-10-29 12:46:29', 1, '2013-01-22 10:42:28'),
(22, 'Manage Newsletter', 'newsletter', 1, 'yes', 1, 1, '2012-10-29 12:46:47', 0, '0000-00-00 00:00:00'),
(23, 'CMS Setup', 'setup', 1, 'no', 1, 1, '2012-10-29 12:47:31', 0, '0000-00-00 00:00:00'),
(24, 'Car Product', 'car_product', 1, 'no', 1, 1, '2013-04-30 15:00:17', 0, '0000-00-00 00:00:00'),
(25, 'Category', 'category', 1, 'no', 1, 1, '2013-04-30 15:10:17', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}news`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `alias` varchar(800) NOT NULL,
  `title` varchar(800) NOT NULL,
  `newsType` int(11) NOT NULL,
  `news_link` varchar(800) NOT NULL,
  `title_image` varchar(255) NOT NULL,
  `image_status` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `keywords` varchar(800) NOT NULL,
  `metadescription` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `published_date` date NOT NULL,
  `published_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}newsletter`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `published_date` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(500) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}profile`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}profile` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `site_id` int(100) NOT NULL,
  `profile_name` varchar(50) NOT NULL,
  `super_admin` enum('False','True') NOT NULL DEFAULT 'False',
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}profile_detail`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}profile_detail` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `profile_id` int(100) NOT NULL,
  `module_id` int(100) NOT NULL,
  `add_module` tinyint(1) NOT NULL,
  `edit_module` tinyint(1) NOT NULL,
  `view_module` tinyint(1) NOT NULL,
  `delete_module` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}setting`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_title` varchar(255) NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}site`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}site` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `main_site` tinyint(1) NOT NULL DEFAULT '0',
  `sub_domain` varchar(255) NOT NULL,
  `site_offline` enum('yes','no') NOT NULL DEFAULT 'yes',
  `site_offline_msg` text NOT NULL,
  `site_from_email` varchar(255) NOT NULL,
  `site_description` text NOT NULL,
  `site_keyword` text NOT NULL,
  `google_analytics_code` varchar(255) NOT NULL,
  `modules_enabled` varchar(500) NOT NULL,
  `siteBanner` varchar(800) NOT NULL,
  `imageSize` varchar(800) NOT NULL,
  `created_by` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime NOT NULL,
  `site_logo` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}site_menus`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}site_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `submenus` enum('yes','no') NOT NULL DEFAULT 'no',
  `depth` int(11) NOT NULL,
  `main_menu` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}site_news`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}site_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `news_type` varchar(500) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}subscription`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(40) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}validation_rule`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}validation_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `validation_rule` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` tinytext CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_validation_rule`
--

INSERT INTO `tbl_validation_rule` (`id`, `validation_rule`, `description`) VALUES
(1, 'required', 'Required'),
(2, 'trim', 'Triming the space'),
(3, 'alpha', 'Alphabetical characters.'),
(4, 'alpha_numeric', 'Alpha-numeric characters'),
(5, 'numeric', 'Numeric characters'),
(6, 'valid_email', 'Valid email address'),
(7, 'integer', 'Only Integer');
-- --------------------------------------------------------

--
-- Table structure for table `{PREFIX}video`
--

CREATE TABLE IF NOT EXISTS `{PREFIX}video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) DEFAULT NULL,
  `title` varchar(500) NOT NULL,
  `alias` varchar(500) NOT NULL,
  `video_id` varchar(500) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` enum('yes','no') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
