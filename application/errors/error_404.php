<?php 
$CI = &get_instance();
$CI->load->library('user_agent');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/error404.css') ?>">
<title>404 Page Not Found</title>
</head>
<body>
    <div id="container">
        <div class="backlinks">
            <?php  
            $referrer = $CI->agent->referrer();
            $current_url = current_url();
            ?>
            <span class="home"><a href="<?php echo site_url() ?>">Home</a></span>
            <?php if(!empty($referrer) && $referrer != $current_url){ ?>
             <span class="back"><a href="<?php echo $CI->agent->referrer() ?>">Go Where You Came From</a></span>   
             <?php } ?>
        </div>
        <img src="<?php echo base_url('images/error_CDES.jpg') ?>" width="960" height="532"/>
    </div>
</body>
</html>