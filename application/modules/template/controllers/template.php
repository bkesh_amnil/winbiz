<?php
if(!defined('BASEPATH'))
	exit('No direct script access is allowed');
	
class Template extends Admin_Controller{
    function index(){
        $this->load->view('backend/auth');
    }
    function frontend($data){
       
        $this->load->view('frontend/index', $data);
    }
    function admin($data){
        $this->load->view('backend/index', $data);
    }
	
    function two_col($data){
        $this->load->view('two_col', $data);
    }
    function product($data){
        $this->load->view('product/index',$data);
    }
    function moreinfo($data){
        $this->load->view('moreinfo/index',$data);
    }
	
	
}