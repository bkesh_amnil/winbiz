<!-- Footer starts here-->
<div class="footer">
    <div class="topfooter">
    <div class="container">
    <div class="row-fluid">
        <div class="span6">
            <ul class="leftfooter">
                <li><a href="<?php echo site_url('parent_about_us'); ?>">About Us</a></li>
                <li class="border"><a href="<?php echo site_url("parent_customer_care");?>">Customer Care</a></li>
                <li class="border"><a href="<?php echo site_url("parent_media");?>">Media</a></li>
                <li class="border"><a href="<?php echo site_url('parent_contact_us'); ?>">Contact Us</a></li>
            </ul>
        </div>
    	<div class="span6" style="width:45%" >
            <ul class="social">
            	<li class="followus">Follow Us</li>
            	<!--<li><a href="htpps://www.twitter.com/justcallnepal" class="twitter">Twitter</a></li>-->
                <li><a target="_blank" href="https://www.facebook.com/justcallnepal" class="facebook">Facebook</a></li>
                <!--<li><a class="earth">Earth</a></li>-->
                <!--<li><a class="g">G</a></li>-->
                <!--<li><a class="linkedin">LinkedIn</a></li>-->
                <!--<li><a class="p">P</a></li>-->
            </ul>
        </div>
    </div>
    </div>
    </div>
    <div class="bottomfooter">
    	<div class="container">
        	<div class="row-fluid">
            	<div class="span12">
                    <!--<ul class="city">
                        <li><a href="#">Birganj</a></li>
                        <li class="border"><a href="#">Dhangadi</a></li>
                        <li class="border"><a href="#">Biratnagar</a></li>
                        <li class="border"><a href="#">Dharan</a></li>
                        <li class="border"><a href="#">Pokhara</a></li>
                        <li class="border"><a href="#">Hetauda</a></li>
                        <li class="border"><a href="#">Mahendranagar</a></li>
                        <li class="border"><a href="#">Chitwan</a></li>
                        <li class="border"><a href="#">Jumla</a></li>
                        <li class="border"><a href="#">Lamjung</a></li>
                        <li class="border"><a href="#">Illam</a></li>
                    </ul>-->
                    <p class="copy"> &copy; Copyrights 2013, Just Call Nepal, All rights reserved.<br>
                    	<a href="mailto:info@justcallnepal.com">info@justcallnepal.com</a>
                    </p>
                </div>
            </div>
        </div>    
    </div>         
</div>
<!--Footer ends here-->
</body>
</html>