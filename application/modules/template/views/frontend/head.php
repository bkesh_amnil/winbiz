<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>JustCallNepal</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/frontend/assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/frontend/assets/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/assets/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/assets/css/scroller_styles.css" type="text/css"> 

<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/assets/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/assets/js/predictive_search.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>themes/frontend/assets/js/links.js"></script>
<meta name="keywords" content="justcallnepal, just call nepal, justcall nepal, justcall, hotels, 5 star hotels, 5 star hotel, 5 star hotels in nepal, 5 star hotel in nepal, hotel in nepal, hotel, hotels, trekking, restaurants, restaurant, budget, budget hotels, budget hotel, apartment, rent, carpenters, carpenter, electrician, electricians" />
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/frontend/assets/css/predictive_search.css" type="text/css">
<script type="text/javascript">
$(function(){
	var change_city = function(protocol){
		var city = $("#city option:selected").text();
		$('#txt_city').html(city);
	}
	change_city($("#city option:selected").text());
	$('#city').on('change', function(){
		change_city($("#city option:selected").text());
	});
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $(".tab_content").hide();
    $(".tab_content:first").show();
    $("ul.tabs li").click(function() {

            $("ul.tabs li").removeClass("active");

            $(this).addClass("active");

            $(".tab_content").hide();

            var activeTab = $(this).attr("rel"); 

            $("#"+activeTab).fadeIn(); 

    });
});
</script>
</head>