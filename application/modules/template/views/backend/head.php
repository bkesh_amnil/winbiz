<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<!-- Viewport metatags -->
	<meta name="HandheldFriendly" content="true" />
	<meta name="MobileOptimized" content="320" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- iOS webapp metatags -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<title><?php echo $title;?></title>

	<!-- iOS webapp icons -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.html" />
	<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.html" />
	<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-retina.html" />

	<?php 
		echo css_tag(array(	
	 		$theme_path . 'css/reset.css',
			$theme_path . 'css/fluid.css',
			$theme_path . 'css/dandelion.theme.css',
			$theme_path . 'css/dandelion.css',
			$theme_path . 'jui/css/jquery.ui.all.css',
			$theme_path . 'css/demo.css',
			$theme_path . 'plugins/fullcalendar/fullcalendar.css',
			$theme_path . 'plugins/fullcalendar/fullcalendar.print.css',
			$theme_path . 'plugins/tipsy/tipsy.css',
			$theme_path . 'plugins/jgrowl/jquery.jgrowl.css',
                        $theme_path . 'css/style.css'
		));

		echo js_tag(array_merge(array(	
	 		$theme_path . 'js/jquery-1.7.2.min.js',
			$theme_path . 'jui/js/jquery-ui-1.8.20.min.js', 
			$theme_path . 'jui/js/jquery.ui.timepicker.min.js',
			$theme_path . 'jui/js/jquery.ui.touch-punch.min.js',
			$theme_path . 'jui/js/jquery.ui.timepicker.min.js',
			$theme_path . 'js/jquery.fileinput.js',
			$theme_path . 'js/jquery.placeholder.js',
			$theme_path . 'js/jquery.mousewheel.min.js',
			$theme_path . 'js/jquery.tinyscrollbar.min.js',
			$theme_path . 'js/jquery.ui.accordion.js',

			$theme_path . 'plugins/validate/jquery.validate.min.js',
			$theme_path . 'js/demo/demo.validation.js',


			$theme_path . 'plugins/tipsy/jquery.tipsy-min.js',
			$theme_path . 'plugins/validate/jquery.validate.js',
			$theme_path . 'js/jquery.metadata.js',
			$theme_path . 'js/core/plugins/dandelion.circularstat.min.js',
			$theme_path . 'js/core/plugins/dandelion.wizard.min.js',
			$theme_path . 'plugins/fullcalendar/fullcalendar.min.js',
			$theme_path . 'plugins/fullcalendar/gcal.js',
			$theme_path . 'plugins/jgrowl/jquery.jgrowl.min.js',
			$theme_path . 'js/jquery.debouncedresize.js',
			$theme_path . 'js/core/dandelion.core.js',
			$theme_path . 'js/core/dandelion.customizer.js',
			$theme_path . 'js/demo/demo.ui.js',
			$theme_path . 'js/links.js',
			$theme_path . 'js/general.js'
		), $javascripts));
	?>

    <base href="<?php echo base_url();?>"><!--[if lte IE 6]></base><![endif]-->
    <script type="text/javascript">
        var theme_path = '<?php echo $theme_path;?>';
        var baseUrl = '<?php echo base_url();?>';
        $(function(){
            <?php //if ($feedback_information) echo $feedback_information;?>
        });
	</script>

</head>