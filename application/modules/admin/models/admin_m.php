<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends MY_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function getUser($username,$password)
	{
		return $this->db->select("P.PersonID, (CASE WHEN P.MiddleName <> '' THEN CONCAT(P.FirstName, ' ', P.MiddleName, ' ', P.LastName)
						ELSE CONCAT(P.FirstName, ' ', P.LastName) END) AS UserName", FALSE)
						->from('tbl_Person P')
						->join('tbl_PersonLogin PL','PL.PersonID = P.PersonID')
						->where(array('PL.username'=>$username,'PL.password'=>md5($password)))
						->get()
						->row();
		
	}
}

/* End of file admin_m.php */
/* Location: ./application/modules/admin/models/admin_m.php */