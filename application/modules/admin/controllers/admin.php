<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('admin_m');

	}
	public function index()
	{
		
		if($this->input->post()){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$result = $this->admin_m->getUser($username,$password);
           	if($result)
           	{
           		$session_data = array(
                                'current_user_id' => $result->PersonID,
                                'current_user' => $result->UserName,
                                'logged_in' => TRUE
                                );
                $this->session->set_userdata($session_data);
			}
			redirect('category');
        } else {
        	echo Modules::run('template');
        }
	}

	public function logout()
	{
		$session_data = array('current_user' => '', 'logged_in' => '');
		$this->session->unset_userdata($session_data);
		redirect('admin');
	}
}
/* End of file admin.php */
/* Location: ./application/modules/admin/controllers/admin.php */