<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_m extends MY_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getAll($ActiveStatus, $DeletedStatus)
	{
		return $this->db->query('SELECT 
								  child.CategoryID,
								  child.CategoryName,
								  parent.CategoryName ParentName 
								FROM
								  (`tbl_Category` child) 
								  LEFT JOIN `tbl_Category` parent 
								  ON `parent`.`CategoryID` = `child`.`ParentID` 
								  WHERE `child`.`ConfigChoice_CategoryStatusID` = '.$ActiveStatus.' AND `child`.`ParentID` NOT IN (SELECT CategoryID FROM tbl_Category WHERE `ConfigChoice_CategoryStatusID` = '.$DeletedStatus.')
								  ORDER BY `CategoryID` ',FALSE)
						->result();
	}

	public function getCategoryName($is_array = FALSE, $id = 0, $ActiveStatus, $DeleteStatus)
	{
		$query = 'SELECT 
				  child.CategoryID,
				  child.CategoryName 
				FROM
				  (`tbl_Category` child) 
				  LEFT JOIN `tbl_Category` parent 
				  ON `parent`.`CategoryID` = `child`.`ParentID` 
				  WHERE `child`.CategoryID <> '.$id.' AND `child`.ParentID <> '.$id.' AND `child`.`ConfigChoice_CategoryStatusID` = '.$ActiveStatus.' AND `child`.`ParentID` NOT IN (SELECT CategoryID FROM tbl_Category WHERE `ConfigChoice_CategoryStatusID` = '.$DeleteStatus.')
				  ORDER BY `CategoryID` ';	
		
		if($is_array) {
			return $this->db->query($query)->result_array();
		} 
		else {
			return $this->db->query($query)->result();
		}
						
	}

	public function getCategory($id){
		return $this->db->where(array('CategoryID'=>$id))
						->get('tbl_Category')
						->row();
	}

	public function insert($db_array){
		$toreturn = array();
		$toreturn['success'] =  $this->db->insert('tbl_Category',$db_array);
		$toreturn['CategoryID'] = $this->db->insert_id();
		return $toreturn;
	}

	public function update($db_array,$id){
		$toreturn = array();
		$this->db->where('CategoryID',$id);
		$toreturn['success'] = $this->db->update('Category',$db_array);
		$toreturn['CategoryID'] = $id;
		return $toreturn;
	}

	public function getCategoryOptions()
	{
		return $this->db->select('CategoryID as value, CategoryName as text')
						->get('tbl_Category')
						->result();
	}

	public function delete($id)
	{
		$resultset = $this->db->get('tbl_Category');
		$data = array("Status"=>0);
		$this->db->where('CategoryID',$id);
		$this->db->update('tbl_Category',$data);
	}

}

/* End of file category_m.php */
/* Location: ./application/modules/category/models/category_m.php */