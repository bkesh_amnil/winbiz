$(document).ready(function(){
	if ($('#data-table').size() > 0){
		$('#data-table').dataTable({
			"sDom": "<'row'<'col-lg-5'l><'col-lg-5'f>r>t<'row-fluid'<'col-lg-5'i><'col-lg-5'p>>",
			"oLanguage": {
				"sLengthMenu": "_MENU_ records per page"
			},
            'bProcessing': true,
            'bPaginate': true,
            'bSort': true,
			"aoColumnDefs": [
			                 { 
			                   "bSortable": false,
			                   "sWidth": '20%',
			                   "aTargets": [ -1 ], // <-- gets last column and turns off sorting
			                  }, 
			                  { 
			                   "sWidth": '5%',
			                   "aTargets": [ 0 ], // <-- gets last column and turns off sorting
			                  } 
			              ],
		});
	}
});
