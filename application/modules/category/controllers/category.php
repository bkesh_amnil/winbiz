<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('category_m');

		$this->load->helper('my_form_helper');

		if(!$this->session->userdata('logged_in'))
             redirect('admin');
	}

	public function index()
	{
		$ActiveStatus = getConfigChoiceID('Product Category Status', 'IsActive');
		$DeleteStatus = getConfigChoiceID('Product Category Status', 'IsDeleted');
		$categories = $this->category_m->getAll($ActiveStatus,$DeleteStatus);
		$this->data['view_file'] = 'category_view';
        $this->data['categories'] = $categories;
		echo Modules::run('template/admin',$this->data);

	}

	public function form($id=0)
	{ 
		if ($this->input->post())
		{
			$this->validation_rules = array(
						array(
							'field' => 'CategoryName',
							'rules' => 'trim|required|is_unique[Category.CategoryName]'
							),
						array(
							'field' => 'ParentID',
							'rules' => 'trim|xss_clean'
							),
						array(
							'field' => 'Status',
							'rules'	=> 'trim|required'
							),
				);
			$this->form_validation->set_rules($this->validation_rules);

			$db_array=array(
				'CategoryName' => $this->input->post('CategoryName'),
				'ParentID' => $this->input->post('ParentID'),
				'ConfigChoice_CategoryStatusID' => $this->input->post('Status') 
				);

			if($this->form_validation->run() != false)
			{
				if($id)
				{
					$db_array['ModifiedBy'] = $this->session->userdata('current_user_id');
					$db_array['ModifiedDate'] = date('Y-m-d H:i:s');
					$result = $this->category_m->update($db_array,$id);
					if(isset($result['error'])&& count($result['error']>0))
					{
						foreach($result['error'] as $error)
						{	
							$this->session->set_flashdata('error',$error);
						}
					}
					if($result['success'])
					{
						$action['class'] = 'success';
						$action['msg'] = 'Data added successfully.';
						$this->session->set_flashdata('success',$action);
					}
					else
					{
						$action['class'] = 'error';
						$action['msg'] = 'Invalid values to the form!';
						$this->session->set_flashdata('error',$action);
					}
				}
				else
				{
					$db_array['CreatedBy'] = $this->session->userdata('current_user_id');
					$db_array['CreatedDate'] = 'NOW()';
					$result = $this->category_m->insert($db_array);
					if(isset($result['error']) && count($result['error']>0))
					{
						foreach($result['error'] as $error)
						{
							$this->session->set_flashdata('error',$error);
						}
					}
					if($result['success'])
					{
						$action['class'] = 'success';
						$action['msg'] = 'Data added successfully.';
						$this->session->set_flashdata('sucess',$action);
					}
					else
					{
						$action['class'] = 'error';
						$action['msg'] = 'Invalid values to the form!';
						$this->session->set_flashdata('error',$action);
					}
				}
				if(isset($this->input->post['frmSubmit']))
				{
					redirect('category');
				}
				else
				{
					redirect('category/index');
				}

			} 
		}
		if($this->input->post()) 
		{
			
			$categories = $this->category_m->getCategoryName(true);
			$this->data['categories'] = convert_to_dropdown($categories,'CategoryName','CategoryID','Select Parent Category'); 
			$this->data['CategoryName'] = set_value('CategoryName');
			$this->data['ParentID'] = set_value('ParentID');
			$this->data['Status']	= set_value('Status');
		}
		else if($id)
		{
			$category = $this->category_m->getCategory($id);
			$this->data['CategoryName'] = $category->CategoryName;
			$this->data['ParentID'] = $category->ParentID;
			$this->data['Status'] = $category->ConfigChoice_CategoryStatusID;
		}
		else
		{ 
			$this->data['CategoryName'] = '';
			$this->data['ParentID'] = '';
				/*foreach($this->validation_rules as $rule){
				//$data['category']->{$rule['field']} = set_value($rule['field']);
				}*/
		}
		$Statuses = getConfigChoices('Product Category Status');
		$this->data['Statuses'] = $Statuses;
		$DefaultStatusID = getConfigChoiceID('Product Category Status', 'IsDefault');
		$this->data['DefaultStatusID'] = $DefaultStatusID;
		$ActiveStatus = getConfigChoiceID('Product Category Status', 'IsActive');
		$DeleteStatus = getConfigChoiceID('Product Category Status', 'IsDeleted');
		$categories = $this->category_m->getCategoryName(true, $id, $ActiveStatus,$DeleteStatus);
		$this->data['categories'] = convert_to_dropdown($categories,'CategoryName','CategoryID','Select Parent Category'); 
		$this->load->view('form',$this->data);
		}

	public function delete($id = NULL)
	{
		$id = (int)$id;
		$this->category_m->delete($id);
		redirect('category/index');
	}

}

/* End of file category.php */
/* Location: ./application/modules/category/controllers/category.php */