    <?php echo anchor(site_url('category/form'),'<span class="label label-success">Add New Category</span>'); ?>
    <table>
        <thead>
            <tr>
                <th>Category Name</th>
                <th>Parent Category</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($categories as $list): ?>
        <tr>
            <td><?php echo $list->CategoryName; ?></td>
            <td><?php echo $list->ParentName ?></td>
            <td>
            <?php echo anchor(site_url('category/form/' .$list->CategoryID),'<span class="label label-success">Edit</span>'); ?>
            <?php echo anchor(site_url('category/delete/' . $list->CategoryID),'<span class="label label-danger delete">Delete</span>'); ?></td>
        </tr>
        <?php endforeach ?>
        </tbody>
    </table>