<div class="grid_4">
    <div class="da-panel">
        <div class="da-panel-header">
                <span class="da-panel-title">
                <img alt="" src="<?php echo site_url().$theme_path; ?>images/icons/black/16/pencil.png">
                Add Category
            </span>
        </div>
        <div class="da-panel-content">
                <?php 
                    $attributes = array('name'=>'CategoryForm', 'id' => 'CategoryForm');
                    echo form_open('',$attributes);
                ?>
                <div class="da-form-row">
                       <?php echo form_label("Category Name",'CategoryNameLabel'); ?> 
                    <div class="da-form-item small">
                    <?php 
                        $data = array(
                              'name'        => 'CategoryName',
                              'id'          => 'CategoryName',
                              'placeholder' => 'Category Name',
                              'value'       => $CategoryName
                              );
                        echo form_input($data);
                    ?>
                    </div>
                </div>

                <div class="da-form-row">
                       <?php echo form_label("Parent Category",'ParentCategoryLabel'); ?> 
                    <div class="da-form-item small">
                    <?php echo form_dropdown('ParentID',$categories, $ParentID); ?>   
                    </div>
                </div>

                <div class="da-form-row">
                       <?php echo form_label("Status:",'CategoryStatusLabel'); ?> 
                    <div class="da-form-item small">
                    <?php 
                        foreach($Statuses as $Status) {
                            $checked = FALSE;
                            if($Status->ConfigChoiceID == $DefaultStatusID) {
                                $checked = TRUE;
                            }
                            $data = array(
                                    'name'=>'Status',
                                    'id'=>'statusid',
                                    'value'=> $Status->ConfigChoiceID,
                                    'checked'=> $checked
                                );
                            ?>
                        <label class="radio"><?php echo form_radio($data); ?><?php echo $Status->ConfigChoiceCode; ?></label>
                    <?php } ?>   
                    </div>
                </div>

                <div class="da-button-row">
                        <input type="submit" class="da-button green" value="Add Category">
                        <input type="reset" getlink="<?php echo site_url('category'); ?>" class="da-button gray left ims_link" value="Cancel">
                </div>
        </div>
    </div>
</div>

 