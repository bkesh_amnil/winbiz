<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Dynamic_form extends CI_Controller {

        var $product_enquiry = 'tbl_product_enquiry';
        var $product_review = 'tbl_product_review';
        var $product_rate = 'tbl_product_rate';
    
        function __Construct(){
            parent::__Construct();
            $this->load->model('dynamic_form/public_form_model');
            $this->load->model('dynamic_form/form_model');
            $this->load->helper('file');
        }   
    
        function product_enquiry_submit(){
            if($this->input->is_ajax_request()) {
                $post = $_POST;
                $fullname = $post['full_name'];
                $email = $post['email'];
                $contact = $post['contact'];
                $message = $post['message'];
                $enterprise_id = $post['enterprise_id'];
                $product_id = $post['product_id'];
                $product_name = $post['product_name'];

                $insert_data_enquiry['enterprise_id'] = $enterprise_id;
                $insert_data_enquiry['product_id'] =$product_id;
                $insert_data_enquiry['customer_name'] = $fullname;
                $insert_data_enquiry['email'] = $email;
                $insert_data_enquiry['contact_number'] = $contact;
                $insert_data_enquiry['message'] = $message;
                $insert_data_enquiry['enquiry_date'] = get_now();

                if($this->db->insert($this->product_enquiry, $insert_data_enquiry)) {
                    $to_emails = $this->public_model->getEnterpriseEmail($enterprise_id);
                    if(isset($to_emails) && !empty($to_emails)) {
                        $site_url = site_url();
                        $site = $this->public_model->getSiteDetails($enterprise_id);
                        
                        $to_list = explode(',', $to_emails);

                        require_once ('swiftmailer/swift_required.php');
                        $subject = 'Product Enquiry';
                        $message1 = 'Respected Sir/Madam<br/><br/>';
                        $message1 .= 'A new Product Enquiry has been made for ' . $product_name.'.';
                        $message1 .= '<br/>Here are the Details :';
                        $message1 .= '<br/>Custome Name : ' . ucfirst($fullname);
                        $message1 .= '<br/>Email : ' . $email;
                        $message1 .= '<br/>Enquiry : ' . $message;
                        $message1 .= '<br/><br/>Regards<br/><a href="'.$site_url.'" target="_blank">FWEAN</a>';
                        $body = $message1;

                        foreach($to_list as $to_email) {
                            $to = $to_email;
                            try {
                                if($_SERVER['SERVER_NAME'] === 'localhost') {
                                    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                                        ->setUsername('rojeena.localmail@gmail.com')
                                        ->setPassword('neversaydie');
                                } else {
                                    $transport = Swift_SendmailTransport::newInstance();
                                }

                                $mailer = Swift_Mailer::newInstance($transport);

                                $message = Swift_Message::newInstance($subject)
                                    ->setFrom(array($site->site_from_email => $site->site_title))
                                    ->setTo($to)
                                    ->setBody($body, 'text/html');
                                $result = $mailer->send($message);
                                
                                $data['class'] = 'success';
                                $data['msg'] = 'Enquiry sent successfully.';
                            } catch(Exception $e) {
                                echo '<pre>'; 
                                $data['class'] = 'error';
                                $data['msg'] = $e;
                            }
                        }

                        /*$this->load->library('email');
                        $config['mailtype'] = 'html';
                        $config['validate'] = TRUE;
                        $this->email->initialize($config);
                        $this->email->from($site->site_from_email, $site->site_title);
                        $this->email->to($to_list); 
                        $this->email->subject('Product Enquiry');
                        $message1 = 'Respected Sir/Madam<br/><br/>';
                        $message1 .= 'A new Product Enquiry has been made for ' . $product_name.'.';
                        $message1 .= '<br/>Here are the Details :';
                        $message1 .= '<br/>Custome Name : ' . ucfirst($fullname);
                        $message1 .= '<br/>Email : ' . $email;
                        $message1 .= '<br/>Enquiry : ' . $message;
                        $message1 .= '<br/><br/>Regards<br/><a href="'.$site_url.'" target="_blank">FWEAN</a>';
                        $this->email->message($message1);
                        if($this->email->send()) {
                            $data['class'] = 'success';
                            $data['msg'] = 'Enquiry sent successfully.';
                        } else {
                            $data['class'] = 'error';
                            $data['msg'] = $this->email->print_debugger();
                        }*/
                    } else {
                        $data['class'] = 'error';
                        $data['msg'] = 'Email not set yet.';
                    }
                } else {
                    $data['class'] = 'error';
                    $data['msg'] = 'Error saving data.';
                }
                
                echo json_encode($data);
            }
        }

        function product_review_submit() {
            if($this->input->is_ajax_request()) {
                $action = '';
                $success = true;
                $review = $rate = 0;

                $client_ip = $_SERVER['REMOTE_ADDR'];
                $post = $_POST;
                $fullname = $post['full_name'];
                $email = $post['email'];
                $review = $post['review'];
                $score = $post['score'];
                $enterprise_id = $post['enterprise_id'];
                $product_id = $post['product_id'];
                $product_name = $post['product_name'];

                $unique = $this->public_model->checkUniqueness($client_ip, $email, $enterprise_id, $product_id);

                if($unique != 'rated and reviewed') {
                    $insert_data_rate['ip_address'] = $insert_date_review['ip_address'] = $client_ip;
                    $insert_data_rate['enterprise_id'] = $insert_date_review['enterprise_id'] = $enterprise_id;
                    $insert_data_rate['product_id'] = $insert_date_review['product_id'] = $product_id;
                    $insert_data_rate['email'] = $insert_date_review['email'] = $email;

                    $insert_data_rate['rate'] = $score;
                    $insert_data_rate['rate_date'] = get_now();

                    $insert_date_review['customer_name'] = $fullname;
                    $insert_date_review['review'] = $review;
                    $insert_date_review['review_date'] = get_now();

                    if($unique == 'not rated') {
                        $this->db->insert($this->product_rate, $insert_data_rate);
                        if($this->db->affected_rows() > 0) {
                            $action = 'rate';
                            $data['class'] = 'success';
                            $data['msg'] = 'Product Rated Successfully.';
                        } else {
                            $success = false;
                        }
                    } elseif($unique == 'not reviewed') {
                        $this->db->insert($this->product_review, $insert_date_review);
                        if($this->db->affected_rows() > 0) {
                            $action = 'review';
                            $data['class'] = 'success';
                            $data['msg'] = 'Product Reviewed Successfully.';
                        }  else {
                            $success = false;
                        }
                    } else {
                        $this->db->insert($this->product_review, $insert_date_review);
                        if($this->db->affected_rows() > 0) {
                            $this->db->insert($this->product_rate, $insert_data_rate);
                            if($this->db->affected_rows() > 0) {
                                $action = 'both';
                                $data['class'] = 'success';
                                $data['msg'] = 'Product Reviewed & Rated Successfully.';
                            }
                        }  else {
                            $success = false;
                        }
                    }
                } else {
                    $data['class'] = "error";
                    $data['msg'] = "You can't review & rate twice.";
                }

                /*if($success == true) && !empty($action)) {
                    $to_emails = $this->public_model->getEnterpriseEmail($enterprise_id);
                    switch($action) {
                        case 'rate':
                            $subject = 'Product Rate';
                            $msg = 'Rate : ' . $score;
                            break;
                        case 'review':
                            $subject = 'Product Review';
                            $msg = 'Review : ' . $review;
                            break;
                        default:
                            $subject = 'Product Review & Rate';
                            $msg = 'Review : ' . $review;
                            $msg .= 'Rate : ' . $score;
                            break;
                    }
                    if(isset($to_emails) && !empty($to_emails)) {
                        $to_list = array($to_emails);
                        $site = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
                        $this->load->library('email');
                        $config['mailtype'] = 'html';
                        $config['validate'] = TRUE;
                        $this->email->initialize($config);
                        $this->email->from($site->site_from_email, $site->site_title);
                        $this->email->to($to_list); 
                        $this->email->subject($subject);
                        $message = 'Respected Sir/Madam';
                        $message .= 'A new Product Review/Rate has been made for ' . $product_name;
                        $message .= '<br/>Here are the Details :';
                        $message .= '<br/>Custome Name : ' . ucfirst($fullname);
                        $message .= '<br/>Email : ' . $email . '<br/>';
                        $message .= $msg;
                        $message .= '<br/><br/>Regards<br/><a href="'.$site_url.'">FWEAN</a>';
                        if($this->email->send()) {
                            $data['class'] = 'success';
                            $data['msg'] = $subject . ' sent successfully.';
                        } else {
                            $data['class'] = 'error';
                            $data['msg'] = $this->email->print_debugger();
                        }
                    }

                    $data['class'] = 'success';
                    $data['msg'] = 'Review saved successfully.';
                } else {
                    $data['class'] = 'error';
                    $data['msg'] = 'Error saving data.';
                }*/

                echo json_encode($data);
            }
        }

        function form_submit() {
            if($this->input->is_ajax_request()) {
                $form_id = $this->input->post('form_id');
                $dynamic_fields = $this->input->post('field_name');

                $form = $this->public_form_model->get_forms($form_id);
                $site = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
                $form_fields = $this->form_model->getFormFields($form_id);

                if(isset($form_fields) && !empty($form_fields)) {
                    foreach($form_fields as $field) {
                        $field_arr[$field->field_name] = $field->id;
                    }
                }
                
                $fields = $this->input->post('field_name');
                $result = $this->form_model->form_fields($form_id);
                $html ='';
                 
                $submission['form_id'] = $form->id;
                if($form->submit_action == 'email' || $form->submit_action == 'both'){ 
                    if($form->admin_email){
                        $this->load->library('email');
                        $config['mailtype'] = 'html';
                        $config['validate'] = TRUE;
                        $this->email->initialize($config);
                        $this->email->from($site->site_from_email, $site->site_title);
                        $this->email->to($form->admin_email); 
                        $this->email->subject($form->form_title);
                        switch ($form->id) {
                            case '1' :
                                $link_arr = array(
                                    '{name}' => $dynamic_fields[$field_arr['name']],
                                    '{email}' => $dynamic_fields[$field_arr['email']],
                                    '{message}' => $dynamic_fields[$field_arr['message']],
                                    '{site_name}' => $site->site_title
                                );
                                break;
                            default:
                                break;
                        }
                        $body = strtr($form->admin_email_msg, $link_arr);
                        $this->email->message($body);
                        $this->email->send();
                    }                  
                }
                if( ! ($form->submit_action == 'email' )){
                    $this->db->insert('tbl_form_submission', $submission);
                    $submission_fields['form_submission_id'] = $this->db->insert_id();
                    foreach($fields as $index=>$value){
                        $submission_fields['form_field_id'] = $index;
                        $submission_fields['form_field_value'] = $value;
                        $this->db->insert('tbl_form_submission_fields', $submission_fields);
                    }
                }                   
                $action = array();
                $data['action'] = 'success';
                
                echo json_encode($data);
            }
        }
    }
?>