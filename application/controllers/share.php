<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Share extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $url = $this->uri->segment_array();
        if(empty($url) && !is_array($url)){
            show_404();
        }

        $pid = $this->uri->segment(2);
        $data['product_info'] = $this->public_model->getProductInfoFb($pid);

        $this->load->view('share', $data);
    }

}
?>