<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users Controller
 * @package Controller
 * @subpackage Controller
 * Date created:july 19 2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class Users extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    public function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'general');
        $this->load->model('users_model');

        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);

        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];


        if ($hostName == 'localhost') { // for local
            $siteName = $url[2];
        } else { // for live
            $siteName = $host[0]; //change it to  1 byb. only to test in localhost virtual host i made it 0
        }
        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();

            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where('enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
                die($content);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);
    }

    function index() {
        if (!$this->session->userdata('user_profile')) {
            redirect(base_url('users/login'), 'refresh');
        } else {
            redirect(base_url('dashboard'));
        }
    }

    /**
     * Display login page
     */
    public function login($checkout = '') {

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['logos'] = $this->public_model->get_logo();
        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }

        $this->template['content'] = $this->load->view('users/login_view', $data, TRUE);
    }

    /**
     * checks for valid username and password for user login
     */
    public function loginProcess($ref = '') {
//        dumparray($_POST);
        if ($this->input->post('submitUserLogin')) {
            $this->form_validation->set_rules('userEmail', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('userPassword', 'Password', 'trim|required');
            if (FALSE == $this->form_validation->run()) {
                $this->session->set_flashdata('message', 'Invalid/blank Email or password');
                $this->get_redirect($ref);
            }
            $email = $this->input->post('userEmail');
            $password = md5($this->input->post('userPassword'));
            $result = $this->users_model->authenticate('tbl_users', array('email' => $email, 'password' => $password));
            if ($result) {
                if ($result->status == '1') {
                    $this->session->set_userdata('user_profile', $result);
//                    dumparray($this->session->userdata('user_profile'));
                    $this->getDestination($ref);
                } else {
                    $this->session->set_flashdata('message', 'Your Account is not activated.');
                    $this->get_redirect($ref);
                }
            } else {
                $this->session->set_flashdata('message', 'Invalid Email or Password.');
                $this->get_redirect($ref);
            }
        } else {
            show_404();
        }
    }

    function getDestination($ref = '') {
        $coupon = $this->input->post('coupon');
        $reward_points = $this->input->post('reward_point');
        $credit_slip = $this->input->post('credit_slip');
        if ($ref == 'checkout') {
            redirect(base_url('checkout/'));
        } else {
            redirect(base_url('dashboard'));
        }
    }

    function get_redirect($ref = '') {
        if ($ref == '') {
            redirect(base_url('users/login'));
        } else if ($ref == 'checkout') {
            redirect(base_url('checkout'));
        }
    }

    /**
     * destroys all session and logs out the user 
     */
    function logOut($ref = '') {
        $this->session->unset_userdata('user_profile');
        $this->get_redirect($ref);
    }

    function register() {
        $siteId = $this->template['site_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];

        $this->template['content'] = $this->load->view('users/register_view', $data, TRUE);
    }

    function registerProcess() {
        $this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha_numeric');
        //$this->form_validation->set_rules('middle_name', 'Middle Name', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('last_name', 'Last name', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_users.email]', array(
            'required' => 'You have not provided %s.',
            'is_unique' => 'This %s has already been resistered.'
        ));
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('country', 'Country', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('mobile', 'Mobile No.', 'trim|required|alpha_numeric');
//        $this->form_validation->set_rules('company', 'Company.', 'trim|required|alpha_numeric_spaces');
//        $this->form_validation->set_rules('state', 'State / Teritory.', 'trim|required|alpha_numeric');
//        $this->form_validation->set_rules('town', 'Suburban / Town', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('postcode', 'Post code', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('address_line1', 'Address', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', validation_errors());
            $this->session->set_flashdata('first_name', $this->input->post('first_name'));
            $this->session->set_flashdata('middle_name', $this->input->post('middle_name'));
            $this->session->set_flashdata('last_name', $this->input->post('last_name'));

            $this->session->set_flashdata('phone', $this->input->post('phone'));
            $this->session->set_flashdata('mobile', $this->input->post('mobile'));
            $this->session->set_flashdata('company', $this->input->post('company'));
            $this->session->set_flashdata('state', $this->input->post('state'));
            $this->session->set_flashdata('town', $this->input->post('town'));
            $this->session->set_flashdata('email', $this->input->post('email'));
            $this->session->set_flashdata('postcode', $this->input->post('postcode'));
            $this->session->set_flashdata('address_line1', $this->input->post('address_line1'));

            redirect('users/register');
        } else {

            $user_id = $this->users_model->register_user();
            $user_profile = $this->general->getById('tbl_users', 'id', $user_id);
            $this->session->set_userdata('user_profile', $user_profile);
//        dumparray($this->session->userdata('user_profile'));
            //==SEND MAIL TO ADMIN
            $date = date('Y-m-d h:i:s');
            $ip_address = $_SERVER["REMOTE_ADDR"];
            $userEmail = $_POST['email'];
            $_POST['ip_address'] = $ip_address;
            $boundary = str_replace(' ', '', $this->config->item('site_name')) . date("ymd");
            $message = "<br/><h4><u>A new member has been registered with following details on " . date('j M Y') . " </u></h4><br/>\r\n";
            foreach ($_POST as $key => $value) {
                if ($key == "submit" || $key == "password" || $key == "passconf") {
                    continue;
                }
                if (!empty($value)) {
                    $message.= "<b>" . ucfirst(str_replace("_", " ", $key)) . ": </b> " . $value . "<br/>";
                }
            }
            $message.='<br/>Login to your site admin and make sure of it, <a href="' . site_url() . '/admin" target="_blank">' . site_url() . '/admin</a>';
            $message.="<br/><br/>--" . $boundary . "--\r\n";
            //===============================================================
            $member_message = "<br/><h4><u>Thankyou for registering in our site " . config_item('site_name') . " on  " . date('j M Y') . " </u></h4><br/>\r\n";
            foreach ($_POST as $key => $value) {
                if ($key == "submitUserLogin" || $key == "password" || $key == "conf_password") {
                    continue;
                }
                if (!empty($value)) {
                    $member_message.= "<b>" . ucfirst(str_replace("_", " ", $key)) . ": </b> " . $value . "<br/>";
                }
            }
            $member_message.='<br/>Visit our site for regular updates about new products and trends, <a href="' . config_item('site_link') . '" target="_blank">' . config_item('site_link') . '</a>';
            $member_message.="<br/><br/>--" . $boundary . "--\r\n";

            //echo $message; exit;
            $subject = config_item('site_name') . ": Member registration Details";
            $site_email = admin_email();
            $header = config_item('site_name');
            $member_email = $userEmail;
            //send_email($to, $subject, $message, $header , $from ,$attatch=NULL,$atach=NULL)
            $this->mylibrary->send_email($site_email, $subject, $message, $header, $member_email);
            $message.= "<b>Password: </b> " . $_POST['password'] . "<br/>";
            $this->mylibrary->send_email($member_email, $subject, $member_message, $header, $site_email);
            redirect(base_url('dashboard'));
        }
    }

    function resetPassword() {
        $email = $this->input->post('Email_Address');
        if (isset($email) && $email != '') {
            $this->form_validation->set_rules('Email_Address', ' Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('message', 'Invalid Email.');
                redirect(current_url());
            }
            $user_info = $this->users_model->authenticate('tbl_users', array('email' => $email));
            if (FALSE == ($user_info)) {
                $this->session->set_flashdata('message', 'Sorry, there is no account associated with this email. Please check your email address and submit again!!!');
                redirect(base_url('users/resetPassword'));
            } else {
                if ($user_info->status != '1') {
                    $this->session->set_flashdata('message', 'Your Account is not activated.Please contact the site administrator.');
                    redirect(current_url());
                } else {
                    $code = random_string('alnum', 32);
                    $this->users_model->saveCode($code, $email); //reset code save garan
                    //==SEND MAIL TO ADMIN
                    $Name = $user_info->first_name;
                    $date = date('Y-m-d h:i:s');
                    $ip_address = $_SERVER["REMOTE_ADDR"];
                    $_POST['ip_address'] = $ip_address;
                    $boundary = str_replace(' ', '', $this->config->item('site_name')) . date("ymd");
                    $message = '<strong>Dear ' . $Name . ',</strong><br><br>';
                    $message.='<p>We want to help you to reset your password .Please <strong><a href="' . base_url() . 'users/reset_password_form/' . $code . '">Click Here</a></strong> to reset your password</p>';
                    $message.= 'Your password has been reset, as per your request.<br>';
                    // $message.= "Please use following details to log in to your account.<br><br>";
                    // $message.= 'Your Email: ' . $Email . '<br>';
                    // $message.= 'New Password: ' . $newPassword . '<br><br>';
                    $message.= "Thank You, <br>Best Regards, <br><strong>" . config_item('site_name') . "</strong>";
                    $message.="<br/><br/>--" . $boundary . "--\r\n";

//                    echo $message; exit;
                    $subject = config_item('site_name') . ": Password Reset Information.";
                    $site_email = admin_email();
                    $header = config_item('site_name');
                    //send_email($to, $subject, $message, $header , $from ,$attatch=NULL,$atach=NULL)
                    $sent = $this->mylibrary->send_email($email, $subject, $message, $header, $site_email);
                    if ($sent) {
                        $this->session->set_flashdata('success', 'Your password is reset and has been sent to your email. Please check your email.');
                    } else {
                        $this->session->set_flashdata('message', 'Error Occured, The password cannot be reset, Please try again.');
                    }
                    redirect(base_url('users/resetPassword'));
                }
            }
        }



        $data['page_title'] = 'Forgot Password';
        $data['current'] = $this->uri->segment(1);
        $siteId = $this->template['site_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];

        $this->template['content'] = $this->load->view('users/resetPassword_view', $data, TRUE);
    }

    //varifying the email code

    public function reset_password_form($email_code) {

        if (isset($email_code)) {

            $data['page_title'] = 'Reset Password';
            $data['reset_code'] = $email_code;

            $data['page_title'] = 'Forgot Password';
            $data['current'] = $this->uri->segment(1);
            $siteId = $this->template['site_id'];

            $data['active_menu'] = 'home';
            $data['special_offer'] = 1;
            $data['site_id'] = $siteId;
            $data['default_host'] = $this->template['default_host_name'];
            $data['main_host'] = $this->template['main_host_name'];

            $this->template['content'] = $this->load->view('users/update_password_view', $data, TRUE);
        }
    }

    function updatePassword() {
        $varified = $this->users_model->varify_reset_password_code($this->input->post('rcode'));
        if ($varified) {
            //updating the password
            $password = md5($this->input->post('new_pass'));

            if ($this->users_model->updatePassword($varified->email, $password)) {
                $this->session->set_flashdata('success', 'Your password is reset successfully! Please login ');
                $this->users_model->removeCode($varified->email);

                redirect('users/login');
            }
        } else {
            $this->session->set_flashdata('message', 'The verification code is not valid');
            redirect('users/login');
        }
    }

    //#for ajax call check user login for coupon code form
    function checkLogin() {
        if ($this->session->userdata('user_profile')) {
            echo 1;
        } else {
            echo 0;
        }
    }

}

/* End of file users.php
 * Location: ./application/modules/users/controllers/users.php */