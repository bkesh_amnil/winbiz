<?php

class Checkout extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'general');
        $this->load->model('users_model', 'users');
        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);

        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];

        if ($hostName == 'localhost') { // for local
            $siteName = $url[1];
        } else { // for live
            $siteName = $host[0]; //change it to  1 byb. only to test in localhost virtual host i made it 0
        }
        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();

            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where('enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
                die($content);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['sub_domain'] = $siteDetails->sub_domain;
        $this->template['sub_domain'] = $siteDetails->sub_domain;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);
    }

    function index() {
        if (FALSE == $this->_get_cart()) {
            redirect('carts');
        } else if (!is_logged_in()) {
            $this->_options();
        } else {
            $this->_form();
        }
    }

    function _get_cart() {
        $return = FALSE;
        $carts = $this->template['carts'];
        if (!empty($carts)) {
            $siteId = $this->template['site_id'];
            $site_cart = $carts[$siteId];
            if (!empty($site_cart)) {
                $return = $site_cart;
            }
        }
        return $return;
    }

    function _options() {
        $data['current'] = $this->uri->segment(1);
        $data['page_title'] = 'Checkout Page';
        $data['current'] = $this->uri->segment(2);
        $data['page_title'] = 'Shopping Cart';
//        $user_id = $this->session->userdata('user_profile')->id;
//        $data['users'] = $this->users_model->get_user_detail($user_id);
//        $this->session->userdata('user_profile')->id;
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }
        $this->template['content'] = $this->load->view('carts/checkout_option', $data, TRUE);
    }

    function _form() {
        $data['current'] = $this->uri->segment(1);
        $data['page_title'] = 'Checkout Page';
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['breadcrumb'] = array('Checkout' => '');
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];
        $data['carts'] = $this->_get_cart();

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }

        $data['user'] = $this->_getUserData();
        $this->template['content'] = $this->load->view('carts/checkout_form', $data, TRUE);
    }

    function _getUserData() {
        $sess = $this->session->userdata('user_profile');
        $user = ($sess) ? $this->users->get_user_detail($sess->id) : FALSE;
        $data = array(
            'first_name' => ($user) ? $user->first_name : '',
            'middle_name' => ($user) ? $user->middle_name : '',
            'last_name' => ($user) ? $user->last_name : '',
            'email' => ($user) ? $sess->email : '',
            'company' => ($user) ? $user->company : '',
            'phone' => ($user) ? $user->phone : '',
            'address_line1' => ($user) ? $user->address_line1 : '',
            'town' => ($user) ? $user->town : '',
            'state' => ($user) ? $user->state : '',
            'postcode' => ($user) ? $user->postcode : '',
            'country' => ($user) ? $user->country : '',
        );
        return $data;
    }

    function guest() {
        if ($this->cart->total_items() == 0) {
            redirect('carts');
        } else {
            $this->_form();
        }
    }

    function process() {
//        dumparray($_POST);
        //#FORM VALIDATION STARTS>>
        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|trim|xss_clean||min_length[6]');
        $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
        $this->form_validation->set_rules('country', 'Country', 'required|trim|xss_clean');

        //#shipping 
        $same_shipping_address = $this->input->post('same_address');
        if (!isset($same_shipping_address) && $same_shipping_address != '') {
            $this->form_validation->set_rules('s_first_name', 'Shipping First Name', 'required|trim|xss_clean|callback_valid_text');
            $this->form_validation->set_rules('s_last_name', 'Shipping Last Name', 'required|trim|xss_clean|callback_valid_text');
            $this->form_validation->set_rules('s_email', 'Shipping Email', 'required|trim|xss_clean|valid_email');
            $this->form_validation->set_rules('s_phone', 'Shipping Phone Number', 'required|trim|xss_clean||min_length[6]');
            $this->form_validation->set_rules('s_address', 'Shipping Address', 'required|trim|xss_clean');
            $this->form_validation->set_rules('s_country', 'Shipping Country', 'required|trim|xss_clean');
        }

        $this->form_validation->set_rules('comments', 'Comments', 'trim|xss_clean');
//        $this->form_validation->set_rules('shipping_method', 'Shipping Method', 'required|trim|xss_clean');
        $this->form_validation->set_rules('payment_method', 'Payment Method', 'required|trim|xss_clean');
        $this->form_validation->set_error_delimiters('<li>', '</li>');
        //#FORM VALIDATION ENDS <<
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $billing_data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
                'country' => $this->input->post('country'),
            );

            // dumparray($billing_data);
            //#checkout data starts >>
            $checkout_data = $billing_data;
            $checkout_data['same_address'] = filter_checkbox($same_shipping_address);



            if (isset($same_shipping_address) && $same_shipping_address == '') {
                $checkout_data['s_first_name'] = $this->input->post('s_first_name');
                $checkout_data['s_middle_name'] = $this->input->post('s_middle_name');
                $checkout_data['s_last_name'] = $this->input->post('s_last_name');
                $checkout_data['s_email'] = $this->input->post('s_email');
                $checkout_data['s_company'] = $this->input->post('s_company');
                $checkout_data['s_address'] = $this->input->post('s_address');
                $checkout_data['s_phone'] = $this->input->post('s_phone');
                $checkout_data['s_country'] = $this->input->post('s_country');
            }



            //#payment method
            $payment_method = $this->input->post('payment_method');
            $checkout_data['payment_method'] = $payment_method;

            $ip = $_SERVER['REMOTE_ADDR'];
            $sess = $this->session->userdata('user_profile');
            $checkout_data['user_id'] = $user_id = ($sess) ? $sess->id : '';
            $checkout_data['comments'] = $this->input->post('comments');
            $checkout_data['new_entry'] = 1;
            $checkout_data['status'] = 'PENDING';
            $checkout_data['ip_address'] = $ip;

            //# orders processer
            //# tax
            $tp = config_item('tax');
            $all_carts = $this->template['carts'];
            $siteId = $this->template['site_id'];
            $ent_id = $this->template['enterprise_id'];
            $checkout_data['site_id'] = $siteId;
            $checkout_data['ent_id'] = $ent_id;

            $carts = $all_carts[$siteId];
//            dumparray($carts);
            $cart_total = $this->cart->get_cart_total($carts);
//            dumparray($cart_total);
            $checkout_data['tax'] = $tax = (($cart_total * ($tp / 100)));
            $checkout_data['order_sub_total_price'] = ($cart_total);
            $checkout_data['order_total_price'] = $grand_total = ($cart_total + $tax );
            $grand_total = number_format($grand_total, 2);

//            dumparray($checkout_data, FALSE);
            $checkout_id = $this->general->insert('checkout', $checkout_data);
//            printQuery();
//            
            //# process cart
            $this->_processCart($carts, $checkout_id, $user_id);
            $order = getWhere('checkout', array('id' => $checkout_id));
            if ($payment_method == 'paypal') {
                //#send email first>>
                $message = $this->getEmailContent('sending-paypal', $order);
//                echo $message;exit;
                $subject_customer = config_item('site_name') . ": Order Details from " . config_item('site_name');
                $subject_owner = config_item('site_name') . ": Customer Filled Order Details " . config_item('site_name');
                $site_email = admin_email();
                $header = config_item('site_name');
                $customer_email = $order->email;
                //#customer message
                $cm = '<p>Thankyou for ordering products from our site. We have received your order and will soon process your order. Here are the details of order you just made.</p>' . $message;
                $this->mylibrary->send_email($customer_email, $subject_customer, $cm, $header, $site_email);
                $this->mylibrary->send_email($site_email, $subject_owner, $message, $header, $customer_email);
                //#redirect to payment
                $this->session->set_userdata('grand_total', $grand_total);
                $this->session->set_userdata('checkout_id', $checkout_id);
                redirect('payment');
            } else if ($payment_method == 'esewa') {
                $message = $this->getEmailContent('eway', $order);
//                echo $message;
//                exit;
                $subject_customer = config_item('site_name') . ": Order Details from " . config_item('site_name');
                $subject_owner = config_item('site_name') . ": Order Details " . config_item('site_name');
                $site_email = admin_email();
                $header = config_item('site_name');
                $customer_email = $order->email;
                //#customer message
                $cm = '<p>Thankyou for ordering products from our site. We have received your order and will soon process your order. Here are the details of order you just made.</p>' . $message;
                //#commment as for now due to server mail error
                $this->mylibrary->send_email($customer_email, $subject_customer, $cm, $header, $site_email);
                $this->mylibrary->send_email($site_email, $subject_owner, $message, $header, $customer_email);
                //#redirect to payment
                $this->session->set_userdata('grand_total', $grand_total);
                $this->session->set_userdata('checkout_id', $checkout_id);
                redirect('payment/eway');
            } else if ($payment_method == 'cod') {
                $message = $this->getEmailContent('cod', $order);
//                echo $message;
//                exit;
                $subject_customer = config_item('site_name') . ": Order Details from " . config_item('site_name');
                $subject_owner = config_item('site_name') . ": Order Details " . config_item('site_name');
                $site_email = admin_email();
                $header = config_item('site_name');
                $customer_email = $order->email;
                //#customer message
                $cm = '<p>Thankyou for ordering products from our site. We have received your order and will soon process your order. Here are the details of order you just made.</p>' . $message;
                //#commment as for now due to server mail error
                $this->mylibrary->send_email($customer_email, $subject_customer, $cm, $header, $site_email);
                $this->mylibrary->send_email($site_email, $subject_owner, $message, $header, $customer_email);
                //#destroy session
                $this->cart->destroy();

                //#redirect to thankyou page
                $this->session->set_flashdata('thankyou', 'success');
                $this->session->set_flashdata('success', 'The order has been successfully received. Thankyou.');
                redirect('payment/thankyou');
            }
        }
    }

    function getEmailContent($type, $order = '') {
        $data['type'] = $type;
        $data['order'] = $order;
        $data['products'] = getWhere('order_product', array('checkout_id' => $order->id), TRUE);
        $content = $this->load->view('payment/email_view', $data, TRUE);
        return $content;
    }

    function _processCart($carts, $checkout_id) {
        foreach ($carts as $rows):
            $product_id = $rows['id'];
            $product_data[] = array(
                'checkout_id' => $checkout_id,
                'product_id' => $product_id,
                'product_name' => $rows['name'],
                'price' => $rows['price'],
                'qty' => $rows['qty'],
                'site_id' => $rows['option']['site'], #
                'ent_id' => $rows['option']['ent'], #
            ); //#productwise site and ent id required coz:: to idetify the products sold from main site which dont have site domain
        endforeach;
        $this->db->insert_batch('order_product', $product_data);
        return TRUE;
    }


    function thankyou() {
//        dumparray($_REQUEST);
//        $thanks = $this->session->flashdata('thankyou');
//        if ($thanks && $thanks == 'success') {
        $data['current'] = $this->uri->segment(2);
        $data['page_title'] = 'Thankyou';
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = array('Thank you' => '');
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }



        $this->template['content'] = $this->load->view('payment/thankyou', $data, TRUE);
//        } else {
//            redirect(base_url());
//        }
    }

    function cancellation() {
        redirect('home');
    }

}

//class closes


    