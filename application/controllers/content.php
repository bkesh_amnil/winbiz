<?php

Class Content extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    function __construct() {
        parent::__construct();

        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);
        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];

        if ($hostName == 'localhost') { // for local
            $siteName = $url[1];
        } else { // for live
            $siteName = $host[0];
        }

        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();


            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where($this->enterprise_category_list . '.enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
                die($content);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
//        dumparray($this->template['menus'] );
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);

        /* $row = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
          $this->template['site_title'] = $row->site_title;
          $this->template['site_logo'] = $row->site_logo;
          $this->template['site_keywords'] = $row->site_keyword;
          $this->template['site_description'] = $row->site_description;

          $this->template['menus'] = $this->public_model->getMainMenu(1);
          $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
          $this->template['footer_menus'] = $this->public_model->getFooterMenus();
          $this->template['social_medias'] = $this->public_model->getSocialMedia(); */
    }

    function index() {
        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $alias = $this->uri->segment(1);
//        echo $alias;exit;

        $data['active_menu'] = $this->public_model->getActiveMenu($this->uri->segment(1), $siteId);
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['logos'] = $this->public_model->get_logo();
//        $data['top_right_bigyapan'] = $this->public_model->getAdvertisement('right_top', $siteId);

        $data['top_right_bigyapan'] = $r = $this->public_model->get_advertisement_session('right_top', $siteId);
        $data['center_right_bigyapan'] = $this->public_model->getAdvertisement('right_center', $siteId);
        $data['bottom_right_bigyapan'] = $this->public_model->getAdvertisement('right_bottom', $siteId);
        $data['bottom_right_bigyapan'] = $this->public_model->getAdvertisement('right_bottom', $siteId);

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(4, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(4, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }

        $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);

        $menuid = $this->public_model->getMenuId($alias, $siteId);
        $modules = $this->public_model->getModules($menuid->id);

        if ($modules[0]['module'] == 'content') {
            $data['content'] = $modules[0]['data'][0];

            $this->template['content'] = $this->load->view('content', $data, TRUE);
        } else {
            if (empty($modules)) {
                $content = $this->load->view('404', $data, true);
                die($content);
            } else {
                $data['modules'] = $modules;
                $this->template['content'] = $this->load->view('contact', $data, TRUE);
            }
        }
    }

}

?>