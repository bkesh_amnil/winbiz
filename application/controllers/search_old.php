<?php 
    
    Class Search extends BASE_Controller{
        
        function __construct() {
            parent::__construct();

            $row = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
            $this->template['site_title'] = $row->site_title;
            $this->template['site_logo'] = $row->site_logo;
            $this->template['site_keywords'] = $row->site_keyword;
            $this->template['site_description'] = $row->site_description;
$this->template['share_image'] = $siteDetails->share_image;
$this->template['share_title'] = $siteDetails->share_title;
$this->template['share_description'] = $siteDetails->share_description;
$this->template['share_link'] = $siteDetails->share_link;

            $this->template['menus'] = $this->public_model->getMainMenu(1);
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
            $this->template['footer_menus'] = $this->public_model->getFooterMenus();
            $this->template['social_medias'] = $this->public_model->getSocialMedia();
        }

        function index() {
        	$url = $this->uri->segment_array();
            if(empty($url) && !is_array($url)){
                show_404();
            }
$data['logos'] = $this->public_model->get_logo();
            $data['active_menu'] = $this->uri->segment(1);
        	$data['center_bigyapans'] = $this->public_model->getAdvertisement('center');
            $data['breadcrumb'] = $this->public_model->getBreadCrumb($url);
$data['logos'] = $this->public_model->get_logo();
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(2);

        	$post = $_POST;
        	$data['search_text'] = $post['search_text'];
        	$data['search_result'] = $this->public_model->getSearchResult($post['search_text']);

        	$this->template['content'] = $this->load->view('search', $data, TRUE);
        }

    }
?>