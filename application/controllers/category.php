<?php

Class Category extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    function __construct() {
        parent::__construct();

        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);

        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];

        if ($hostName == 'localhost') { // for local
            $siteName = $url[2];
        } else { // for live
            $siteName = $host[0];
        }

        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();

            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where('enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);

        /* $row = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
          $this->template['site_title'] = $row->site_title;
          $this->template['site_logo'] = $row->site_logo;
          $this->template['site_keywords'] = $row->site_keyword;
          $this->template['site_description'] = $row->site_description;

          $this->template['menus'] = $this->public_model->getMainMenu(1);
          $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
          $this->template['footer_menus'] = $this->public_model->getFooterMenus();
          $this->template['social_medias'] = $this->public_model->getSocialMedia(); */
    }

    function index() {
        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $url = $this->uri->segment_array();
        if (empty($url) && !is_array($url)) {
            show_404();
        }

        $count = count($url);
        $data['count'] = $count;
        $data['active_menu'] = $this->uri->segment(1);
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['logos'] = $this->public_model->get_logo();
        if ($siteId == '1') {
            $ent_cat_id = $data['enterprise_category_id'] = $this->uri->segment(2);
            $ent_id = $data['enteprise_id'] = $this->uri->segment(3);
        }

        $ent_alias = $this->uri->segment(4);
        $this->public_model->increaseEnterpriseCount($ent_id, $siteId);
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);

        if ($count > 3) {
            $data['enterprise_info'] = $this->public_model->getEnterpriseInfo($ent_cat_id, $ent_id, $ent_alias);
            $data['enterprise_products'] = $this->public_model->getEnterpriseProduct($ent_cat_id, $ent_id);
            //$data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($ent_cat_id, $ent_id);
            $data['enterprises'] = $this->public_model->getEnterprises($ent_cat_id);
        } else {
            if ($siteId != '1') {
                $prod_cat_id = $this->uri->segment(2);
                $data['products'] = $this->public_model->getProductsByProductCategory($prod_cat_id, $ent_id);
                $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
            } else {
                $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
                $enterprise_category = explode(',', $ent_cat_id);
                if (count($enterprise_category) > 1) {
                    $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
                } else {
                    if (is_array($enterprise_category)) {
                        $enterprise_category = $enterprise_category[0];
                    }
                    $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
                }
            }
        }

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
        } else {
            /* $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($ent_cat_id, $ent_id); */
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }

        $data['center_bigyapans'] = $this->public_model->getAdvertisement('center', $siteId);
        $data['best_selling_products'] = $this->public_model->getBestSellingProducts(2, $siteId);

        $data['sub_domain_url'] = 'http://' . $data['enterprise_info']->sub_domain . '.' . $data['default_host'];

        if ($count > 3) {
            $this->template['content'] = $this->load->view('enterprise/information', $data, TRUE);
        } else {
            if ($siteId != '1') {
                $this->template['content'] = $this->load->view('products/products_by_category', $data, TRUE);
            } else {
                $data['enterprises'] = $this->public_model->getEnterprises($ent_cat_id);
                $this->template['content'] = $this->load->view('enterprise/all_enterprises', $data, TRUE);
            }
        }
    }

}

?>