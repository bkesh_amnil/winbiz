<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Controller
 * @package Controller
 * @subpackage Controller
 * Date created:July 19 2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class Dashboard extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    public function __construct() {
        parent::__construct();
        $this->checkUserlogin();
        $this->load->model('general_model', 'general');
        $this->load->model('users_model');
        $this->load->model('order_model', 'order');

        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);

        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];


        if ($hostName == 'localhost') { // for local
            $siteName = $url[1];
        } else { // for live
            $siteName = $host[0]; //change it to  1 byb. only to test in localhost virtual host i made it 0
        }
        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();

            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where('enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
                die($content);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);
    }

    /**
     * check whether user is loggin or not. If not redirect to the login page.
     */
    public function checkUserlogin() {
//        dumparray($this->session->userdata('user_profile'));
        if (!$this->session->userdata('user_profile')) {
            redirect(base_url('users/login'), 'refresh');
        }
    }

    function index() {
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $this->session->userdata('user_profile')->id;
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }
        $data['orders'] = $this->order->list_orders(0, TRUE, $user_id);

        $this->template['content'] = $this->load->view('users/dashboard_view', $data, TRUE);
    }

    function orderList() {
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $this->session->userdata('user_profile')->id;
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }
        $data['orders'] = $this->order->list_orders(0, TRUE, $user_id);

        $this->template['content'] = $this->load->view('users/orders_list', $data, TRUE);
    }

    function order($id) {
        if (!isset($id)) {
            redirect(base_url());
        }
        if (!is_numeric($id)) {
            redirect(base_url());
        }
        $orders = $this->order->getOrder($id);
        $data['order'] = $orders['checkout_info'];
        $data['products'] = $orders['product_info'];
        if (FALSE == $data['order']) {
            redirect(base_url());
        }
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $data['page_title'] = 'Order Detail';

        $data['logos'] = $this->public_model->get_logo();
        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['site_id'] = $siteId;
        $data['default_host'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }

        $this->template['content'] = $this->load->view('users/order_view', $data, TRUE);
    }

    /**
     * checks user table for the valid email
     * @return boolean
     */
    public function checkEmailExists($email) {
        if ($email != '') {
            $prev_email = $this->general->countTotal('tbl_users', array('email' => $email));
            if (($prev_email) >= 1) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    function process() {
        $email = $this->session->userdata('user_profile')->email;
        $user_id = $this->session->userdata('user_profile')->id;
        $query = $this->general->getById('users', 'email', $email, TRUE);

        $firstname = $this->input->post('first_name');
        $middlename = $this->input->post('middle_name');
        $lastname = $this->input->post('last_name');
        $country = $this->input->post('country');
        $phone = $this->input->post('phone');
        $mobile = $this->input->post('mobile');
        $address_line1 = $this->input->post('address_line1');
        $postcode = $this->input->post('postcode');
        $company = $this->input->post('company');
        $town = $this->input->post('town');
        $state = $this->input->post('state');


        $new_email = $this->input->post('email');
        $oldPass = $this->input->post('oldPassword');
        $newPass = $this->input->post('newPassword');
        $confPass = $this->input->post('confPassword');

        $login_data = array(
            'email' => $new_email,
        );
        $profile_data = array(
            'first_name' => $firstname,
            'last_name' => $lastname,
            'middle_name' => $middlename,
            'country' => $country,
            'phone' => $phone,
            'mobile' => $mobile,
            'address_line1' => $address_line1,
            'company' => $company,
            'town' => $town,
            'state' => $state,
            'postcode' => $postcode,
        );

        //dumparray($data);
        if ($email != $new_email && $this->checkEmailExists($new_email) == FALSE) {
            $this->session->set_flashdata('message', "This email address has already been registered");
            redirect(base_url() . 'dashboard');
        }

        if ($oldPass != '' && md5($oldPass) == $query['password']) {
            if ($newPass == '' or $confPass == '') {
                $this->session->set_flashdata('message', 'Password Fields Cannot be blank!');
                redirect(base_url('dashboard'), 'refresh');
                return FALSE;
            } elseif ($newPass != $confPass) {
                $this->session->set_flashdata('message', 'The conformation password not matched');
                redirect(base_url('dashboard'), 'refresh');
                return FALSE;
            } else {
                $login_data['password'] = md5($newPass);
                $this->general->update('users', $login_data, array('id' => $user_id));
                $this->general->update('users_profile', $profile_data, array('user_id' => $user_id));
                //reset user profile session data
                $user_profile = $this->general->getById('users', 'id', $user_id);
                $this->session->set_userdata('user_profile', $user_profile);
                $this->session->set_flashdata('success', 'The account information has been Updated.');
                redirect(base_url('dashboard'));
            }
        } elseif ($oldPass != '' && md5($oldPass) != $query['password']) {
            $this->session->set_flashdata('message', 'Current password Invalid.');
            redirect(base_url('dashboard'), 'refresh');
            return FALSE;
        } elseif ($oldPass == '' && ($newPass != '' or $confPass != '')) {
            $this->session->set_flashdata('message', 'Please Enter Current Password ');
            redirect(base_url('dashboard'), 'refresh');
            return FALSE;
        } else {
            $this->general->update('users', $login_data, array('id' => $user_id));
            $this->general->update('users_profile', $profile_data, array('user_id' => $user_id));
            // $this->general->update('country', $profile_data);
            //reset user profile session data
            $user_profile = $this->general->getById('users', 'id', $user_id);
            $this->session->set_userdata('user_profile', $user_profile);
            $this->session->set_flashdata('success', 'The account information has been Updated.');
            redirect(base_url('dashboard'));
        }
    }

}
