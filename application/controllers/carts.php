<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carts extends BASE_Controller {

    var $site = 'tbl_site';
    var $enterprise = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $domain_theme = 'tbl_domain_theme';

    function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'general');
        $this->load->model('users_model');
        $mainSite = false;
        $siteId = '1';
        $theme = 'default.css';
        $enterprise_category_id = $enterprise_id = '';

        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);

        $host = $_SERVER['HTTP_HOST'];
        $this->template['default_host_name'] = $host;
        $this->template['main_host_name'] = 'http://www.winbiz.com.np/';

        $host = explode('.', $host);

        $hostName = $host[0];





        if ($hostName == 'localhost') { // for local
            $siteName = $url[1];
        } else { // for live
            $siteName = $host[0]; //change it to  1 byb. only to test in localhost virtual host i made it 0
        }
        if (empty($siteName) || $siteName == 'winbiz' || $siteName == 'amniltech') { // main site home page
            $this->db->where('main_site', 1);
            $siteDetails = $this->db->get($this->site)->row();
            $mainSite = true;
        } else { // sub site home page
            $this->db->select($this->site . '.*, ' . $this->domain_theme . '.domain_theme_code, ' . $this->enterprise . '.id as ent_id, enterprise_category_id');
            $this->db->where('main_site !=', 1);
            $this->db->where('sub_domain', $siteName);
            $this->db->where('site_offline', 'no');
            $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->site . '.enterprise_id');
            $this->db->join($this->domain_theme, $this->domain_theme . '.id = ' . $this->enterprise . '.domain_theme_id', 'LEFT');
            $siteDetails = $this->db->get($this->site)->row();

            if (isset($siteDetails) && !empty($siteDetails)) {
                $siteId = $siteDetails->id;
                $theme = (!empty($siteDetails->domain_theme_code)) ? $siteDetails->domain_theme_code . '.css' : 'default.css';
                $enterprise_id = $siteDetails->ent_id;

                $this->db->select($this->enterprise_category_list . '.*');
                $this->db->where('enterprise_id =', $siteDetails->ent_id);
                $categoryDetails = $this->db->get($this->enterprise_category_list)->result();
                foreach ($categoryDetails as $details) {
                    $catDetails[] = $details->enterprise_category_id;
                }
                $siteDetails->enterprise_category_id = implode(',', $catDetails);
                $enterprise_category_id = $siteDetails->enterprise_category_id;
            } else {
                $data['default_host_name'] = $this->template['main_host_name'];
                $content = $this->load->view('404', $data, true);
                die($content);
            }
        }

        $mainMenuId = $this->public_model->getMainMenuId($siteId);
        $this->db->where('main_site', 1);
        $mainSiteLogo = $this->db->get($this->site)->row();

        $this->template['site_id'] = $siteId;
        $this->template['sub_domain'] = $siteDetails->sub_domain;
        $this->template['sub_domain'] = $siteDetails->sub_domain;
        $this->template['site_title'] = $siteDetails->site_title;
        $this->template['site_logo'] = $siteDetails->site_logo;
        $this->template['main_site_logo'] = $mainSiteLogo->site_logo;
        $this->template['share_image'] = $siteDetails->share_image;
        $this->template['share_title'] = $siteDetails->share_title;
        $this->template['share_description'] = $siteDetails->share_description;
        $this->template['share_link'] = $siteDetails->share_link;
        $this->template['site_keywords'] = $siteDetails->site_keyword;
        $this->template['site_description'] = $siteDetails->site_description;
        $this->template['domain_theme_css'] = $theme;
        $this->template['enterprise_category_id'] = $enterprise_category_id;
        $this->template['enterprise_id'] = $enterprise_id;

        $this->template['menus'] = $this->public_model->getMainMenu($mainMenuId, $siteId);
        if ($siteId == '1') {
            $this->template['footer_contents'] = $this->public_model->getFooterContent(2);
        }
        $this->template['footer_menus'] = $this->public_model->getFooterMenus();
        $this->template['social_medias'] = $this->public_model->getSocialMedia($siteId);
    }

    function index() {
        $data['current'] = $this->uri->segment(2);
        $data['page_title'] = 'Shopping Cart';
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }



        $this->template['content'] = $this->load->view('carts/cart_view', $data, TRUE);
    }

    
    function checkit() {
        $data['current'] = $this->uri->segment(2);
        $data['page_title'] = 'Shopping Cart';
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }



        $this->template['content'] = $this->load->view('carts/checkit_view', $data, TRUE);
    }
    
    function myAccount() {
        $data['current'] = $this->uri->segment(2);
        $data['page_title'] = 'Shopping Cart';
        $user_id = $this->session->userdata('user_profile')->id;
        $data['users'] = $this->users_model->get_user_detail($user_id);
        $data['logos'] = $this->public_model->get_logo();

        $siteId = $this->template['site_id'];
        $ent_cat_id = $this->template['enterprise_category_id'];
        $ent_id = $this->template['enterprise_id'];

        $data['active_menu'] = 'home';
        $data['special_offer'] = 1;
        $data['active_menu'] = $this->uri->segment(1);
        $url = $this->uri->segment_array();
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url, $siteId);
        $data['site_id'] = $siteId;
        $data['default_host_name'] = $this->template['default_host_name'];
        $data['main_host'] = $this->template['main_host_name'];
        $data['carts'] = $this->template['carts'];
        $data['sub_domain'] = $this->template['sub_domain'];

        if ($siteId == '1') {
            $data['enterprise_categories'] = $this->public_model->getAllEnterpriseProductCategories();
            $data['best_selling_products'] = $this->public_model->getBestSellingProducts(9, $siteId);
        } else {
            $data['best_selling_products'] = $this->public_model->getBestSellingProductsByEnterprise(9, $ent_id);
            $enterprise_category = explode(',', $ent_cat_id);
            if (count($enterprise_category) > 1) {
                $data['enterprise_product_category'] = $this->public_model->getEnterpriseProductCategories($ent_id);
            } else {
                if (is_array($enterprise_category)) {
                    $enterprise_category = $enterprise_category[0];
                }
                $data['enterprise_product_category'] = $this->public_model->getEnterpirseProductCategory($enterprise_category, $ent_id);
            }
        }



        $this->template['content'] = $this->load->view('carts/myaccount_view', $data, TRUE);
    }
    function dump() {

        dumparray($this->cart->contents());
    }

    function cart_checkout() {

        //echo 'a';exit;

        if (isset($_POST['terms_condition']) AND ( $_POST['terms_condition'] = 'ok')) {

            redirect(checkout);
        } else {

            $this->session->set_flashdata('info_err', 'You must accept the terms and conditions First!');

            redirect($_SERVER['HTTP_REFERER']);

            return false;
        }
    }

    function getshippings() {

        $id = $this->input->post('id');

        if (!isset($id)) {

            echo 0;
        } else {
            //for tax
            if ($id == 3) {
                $tp = config_item('tax');
            } else {
                $tp = 100;
            }
            $this->session->set_userdata('shipping_tax', $tp);

            $ship = $this->general->getById('shipping_country', 'sid', $id);

            $country = $ship->country;

            $price = $ship->rates;
            if ($id == 3 && $this->cart->total() >= 100) {
                $price = 1000;
            }

            $this->session->set_userdata('shipping_price', $price);

            $this->session->set_userdata('shipping_cid', $id);
            $this->session->set_userdata('shipping_country', $country);
            $tpm = $tp == 100 ? 0 : $tp;
            $pricem = $price == 1000 ? 0 : $price;
            $tax = $this->cart->total() * ($tpm / 100);
            $total = $this->cart->total() + $tax + $pricem;

            $return = array('ship' => show_money($pricem), 'tax' => show_money($tax), 'total' => show_money($total));

            echo json_encode($return);
        }
    }

    function add() {
        $product_id = $this->input->post('product_id');
        $ent_id = $this->input->post('ent_id');
        $qty = $this->input->post('qty');
        $site_id = get_siteId_by_entId($ent_id);
        $product = $this->general->getById('products', 'id', $product_id);
        if (isset($qty)) {
            $pReplace = array('-', '&', '!', '%', '\/', '\\', ',', ';', ':', '`');
            $name = str_replace($pReplace, ' ', $product->product_name);
            $cart_data = array(
                'id' => $product->id,
                'qty' => $qty,
                'price' => $product->product_price,
                'name' => $name,
                'option' => array(
                    'image' => ($product->product_cover_image),
                    'ent' => $ent_id,
                    'site' => $site_id
                )
            );
            $this->cart->insert($cart_data);
//            echo "<pre>";
//            print_r($this->cart->contents());
//            die();
            foreach ($this->cart->contents() as $rows):
                if ($site_id != 1) {
                    $msg = $site_id != 1 ? 'To view the full details of the cart, please click the link above to the respective site link' : '';
                }
                $this->session->set_flashdata('success', 'The ' . $rows['name'] . ' has been successfully added to the cart.' . $msg);
            endforeach;
            redirect(base_url('carts'));
        } else {
            $this->session->set_flashdata('message', 'No items to add');
            redirect(base_url('carts'));
        }
    }

    function remove($rowid) {
        $this->cart->update(array(
            'rowid' => $rowid,
            'qty' => 0
        ));
        $this->session->set_flashdata('message', 'The cart item has been successfully removed.');
        redirect('carts');
    }

    function clear() {

        $this->cart->destroy();


        $this->session->unset_userdata('shipping_price');

        $this->session->unset_userdata('shipping_country');
        $this->session->unset_userdata('shipping_cid');
        $this->session->unset_userdata('shipping_tax');

        $this->session->set_flashdata('success_message', 'The all items has been successfully removed.');

        //$this->load->view('add');

        redirect('carts');
    }

    function update() {
        $i = 0;
        dumparray($this->cart->contents(), false);
        dumparray($_POST, FALSE);
        $qnty = $this->input->post('qty');
        $row_ids = $this->input->post('rowid');
        $data = array();
        foreach ($row_ids as $rowid) {
            $qty = ($qnty[$i] != '') ? $qnty[$i] : 0;
            $data[] = array(
                'rowid' => $rowid,
                'qty' => $qty
            );
            $i++;
        }
//        dumparray($data);
        if (!empty($data)) {
            $this->cart->update($data);
        }
        $this->session->set_flashdata('success', 'The cart item has been successfully updated.');
        redirect('carts');
    }

    function move_to_carts($id) {

        //echo $id; exit;

        if (isset($id)) {

            $product = $this->product_model->get($id);

            $product1 = $this->product_model->get($id)->row_array();

            //echo "<pre>";	print_r($product->result()); die();

            $qty = 0;

            if ($cart = $this->cart->contents()) {

                //echo 'yes'; exit;

                foreach ($cart as $item) {

                    if ($item['id'] == $id) {

                        if (isset($item['option'])) {

                            if ($product1['home_image'] != NULL) {

                                if ($product1['home_image'] == $item['option']['src']) {

                                    $qty = $item['qty'];
                                }
                            } else {

                                $qty = 0;
                            }
                        } else {

                            $qty = $item['qty'];
                        }
                    }
                }
            }//echo 'no'; exit;

            foreach ($product->result() as $products) {

                $replaced_data = str_replace('<p class="item-desc">', '', $products->describe);

                $final_data = str_replace('</p>', '', $replaced_data);

                $data = trim($final_data);

                //print_r(trim($final_data)); die();

                $data_1 = array(
                    'id' => $id,
                    'qty' => 1 + $qty,
                    'price' => $products->price,
                    'name' => $products->name,
                    'option' => array('src' => $products->home_image, 'contents' => $data)
                );



                $this->cart->insert($data_1);
            }

            //echo "<pre>";	print_r($this->session->all_userdata()); die();
        }

        $mem_id = $this->session->userdata('member_login_id');

        $query = $this->db->delete('wishlist', array('pro_id' => $id, 'mem_id' => $mem_id));

        $this->session->set_flashdata('info_scs', 'Product successfully moved to the cart.');

        redirect('cart');
    }

}

//class closes

