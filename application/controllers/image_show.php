<?php 
class Image_show extends CI_Controller {
		
    function __Construct(){
        parent::__Construct();
    }
		
    function index($type = NULL, $width = 0, $height = 0, $file = NULL){
        if(!empty($file)){
            switch($type){
                case "logo":
                    $path = "./winbiz-system/uploaded_files/site_logo/";
                    break;
                case "bigyapan":
                    $path = "./winbiz-system/uploaded_files/bigyapan/";
                    break;
                case "product_cover_image":
                    $path = "./winbiz-system/uploaded_files/products/";
                    break;
                case "product_image":
                    $path = "./winbiz-system/uploaded_files/product_images/";
                    break;
                case "footer_content":
                    $path = "./winbiz-system/uploaded_files/content/";
                    break;
                case "enterprise":
                    $path = "./winbiz-system/uploaded_files/enterprise/";
                    break;
                case "temp":
                    $path = "./winbiz-system/uploaded_files/temp/";
                    break;
                case "content";
                    $path = "./winbiz-system/uploaded_files/content/";
                    break;
                case "default":
                    $path = "images/";
                    break;
                default:
                    //$path = "./winbiz-system/images/";
            }
            
            /*if(empty($width)){
                $width = 100;
            }
            $ratio = 1; 
            if(empty($height)){
                if(file_exists($path.$file) && !is_dir($path.$file)){
                    list($origwidth, $origheight) = getimagesize($path.$file);
                    if($origheight && $origwidth){
                        $ratio = $origwidth/$origheight;
                    }
                }
                $height = intval($width / $ratio);
            }*/
            $imagePath = $path.$file;
            if(file_exists($imagePath)){
                if(!$width && !$height) {
                    list($origwidth, $origheight) = getimagesize($imagePath);
		    $width = $origwidth;
                    $height = $origheight;
		}
                $this->output->set_content_type('JPEG');
                require_once './winbiz-system/phpthumb/ThumbLib.inc.php';
                $thumb = PhpThumbFactory::create($path.$file);
                $thumb->adaptiveResize($width,$height);
                $thumb->show();
            }
            
        }
    }
}