<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  | example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  | http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  | $route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  | $route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "home";
$route['image/(:any)'] = "image_show/index/$1";
$route['cat/(:any)'] = "category/index/$1";
$route['detail/(:any)'] = "product/detail/$1";
$route['dynamic_form/product_enquiry_submit'] = "dynamic_form/product_enquiry_submit/$1";
$route['dynamic_form/product_review_submit'] = "dynamic_form/product_review_submit/$1";
$route['search'] = "search/index/$1";
$route['search/on_winbiz'] = "search/index/$1";
$route['product/refresh_review'] = "product/refresh_review/$1";
$route['product/refresh_rate'] = "product/refresh_rate/$1";

//users routes >>
$route['carts'] = "carts";
$route['delete/(:any)'] = "carts/remove/$1";
$route['carts/(:any)'] = "carts/$1";
$route['dashboard'] = "dashboard";
$route['dashboard/(:any)'] = "dashboard/$1";
$route['checkout'] = "checkout";
$route['checkout/(:any)'] = "checkout/$1";
$route['payment'] = "payment";
$route['payment/(:any)'] = "payment/$1";
$route['users'] = "users";
$route['users/(:any)'] = "users/$1";
$route['thankyou'] = "checkout/thankyou";
$route['cancellation'] = "checkout/cancellation";
$route['ipn'] = "payment/ipn";
//users routes <<

$route['share/(:any)'] = "share/index/$1";
$route['dynamic_form/(:any)'] = "dynamic_form/form_submit/$1";
$route['(:any)'] = "content/index/$1";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */