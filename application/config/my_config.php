<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$CI = &get_instance();
$CI->config->load();
define('ROW_PER_PAGE', 30);
$config['site_name'] = 'winbiz';
$config['site_email'] = 'limited.sky710@gmail.com';

$config['site_url'] = '';
$config['uploads_url'] = $config['site_url'] . 'uploads/';
$config['product_uploads_url'] = $config['site_url'] . 'uploads/products/';

$config['site_root'] = $_SERVER['DOCUMENT_ROOT'] . '/winbiz/';
$config['uploads_path'] = $config['site_root'] . 'uploads/';
$config['product_uploads_path'] = $config['site_root'] . 'uploads/products/';

$config['currency'] = 'Rs.';
$config['tax'] = 10;
$config['per_page'] = 50;
