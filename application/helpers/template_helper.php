<?php

function css_tag($src) {
    if (is_array($src) and !empty($src)) {
        $css_tags = '';
        foreach($src as $s) {
            $href = strpos($s, 'http') === 0 ? $s : base_url($s);
            $attr = array('href' => $href, 'rel' => 'stylesheet');
            $css_tags .= "\n" . html_tag('link', $attr);
        }
        return $css_tags;
    } else {
        $href = strpos($src, 'http') === 0 ? $src : base_url($src);
        $attr = array('href' => $href, 'rel' => 'stylesheet');
        return html_tag('link', $attr);
    }
}

function js_tag($src, $content = '') {
    if (is_array($src) and !empty($src)) {
        $js_tags = '';
        foreach($src as $s) {
            $href = strpos($s, 'http') === 0 ? $s : base_url($s);
            $attr = array('src' => $href, 'type' => 'text/javascript');
            $js_tags .= "\n" . html_tag('script', $attr, '');
        }
        return $js_tags;
    } else {
        $href = strpos($src, 'http') === 0 ? $src : base_url($src);
        $attr = array('src' => $href, 'type' => 'text/javascript');
        return html_tag('script', $attr, $content);
    }
}

function html_tag($tag, $attr = array(), $content = false) {
    $has_content = (bool) ($content !== false and $content !== null);
    $html = '<' . $tag;

    if (empty($attr)) {
        $html .= '';
    } else {
        if (is_array($attr)) {
            foreach ($attr as $att => $val)
                $html .= ' ' . $att . '="' . $val . '"';
        } else {
            $html .= $attr;
        }
    }
    $html .= $has_content ? '>' : ' />';
    $html .= $has_content ? $content . '</' . $tag . '>' : '';

    return $html;
}

function short_description($description, $charnum) {
    if(strlen($description) <= $charnum + 4)
        return $description;

    return substr($description, 0, $charnum) . '....';
}

function show_status($status){
    if($status == 1){
        $row = '<td style="color:green"> Active </td>';
    }else{
        $row = '<td style="color:red"> Inactive </td>';
    }
    return $row;
}

function file_path($path, $filename, $default) {
    if($filename != '' and file_exists($path.$filename))
        return base_url() . $path . $filename;
    else
        return base_url() . $path . $default;
}

function is_admin($role) {        
    if($role == 'Center Admin' || $role == 'admin')
        return true;
    else
        return false;
}
function get_header($method = 'index'){
	echo modules::run("general/header/".$method);
}

function get_footer($method = 'index'){
	echo modules::run("general/footer/".$method);
}

function get_menu($method = 'index'){
	echo modules::run("general/menu/".$method);
}

function is_logged_in(){
	return modules::run("users/_is_logged_in");	
}

function dumper(){
    return modules::run("carts/dump");	
}

?>