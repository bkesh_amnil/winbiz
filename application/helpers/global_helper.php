<?php

function get_today() {
    return date('Y-m-d');
}

function get_now() {
    return date('Y-m-d H:i:s');
}

function date_difference($day2, $day1 = '') {
    if ($day1 == '') {
        $day1 = get_now();
    }

    if ($day2 == '0000-00-00') {
        return '0';
    }
    $difference = strtotime($day2) - strtotime($day1);
    $difference = abs($difference) / 60 / 60 / 24;
    $difference = floor($difference);
    return $difference;
}

function printr($data, $exit = false) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit)
        exit;
}

function convert_to_dropdown($object, $value, $index = 'id', $default_text = 'Select', $display_text = TRUE) {
    if ($display_text || count($object) > 1)
        $tmp[''] = $default_text;
    else
        $tmp = array();
    foreach ($object as $row) {
        $tmp[$row->$index] = $row->$value;
    }
    return $tmp;
}

function dropdown_data($result, $index, $value, $rels = '') {
    $tmp = array();
    //$tmp['']  = 'Please select';
    foreach ($result as $row) {
        if ($rels) {
            $rel = '" ' . $rels . '="' . $row->$rels;
        } else {
            $rel = "";
        }
        $tmp[$row->$index . $rel] = $row->$value;
    }
    return $tmp;
}

function get_url() {
    $C = &get_instance();
    $get = $C->input->get();
    $url = '';
    if (is_array($get)) {
        $i = 1;
        foreach ($get as $index => $value) {
            if (!$url) {
                $url = '?';
            }

            $url .= $index . "=" . $value;
            if ($i < count($get))
                $url .= '&';
            $i++;
        }
    }
    return ($url);
}

/**
 * displays the array in preformatted form
 * @param type $array
 */
function dumparray($array, $exit = TRUE) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
    if ($exit) {
        exit;
    }
}

/**
 * prints the last executed query
 */
function printQuery($exit = TRUE) {
    $CI = &get_instance();
    echo $CI->db->last_query();
    if ($exit) {
        exit;
    }
}

/**
 * returns the admin email for mailing purpose
 * @return type
 */
function admin_email() {
    $email = config_item('site_email');
    $setting = getWhere('tbl_admin_user', array('id' => 1));
    if (FALSE != $setting) {
        $email = $setting->email;
    }
    return $email;
}

/**
 * returns the result set as per the parameters provided
 * @param type $table
 * @param type $where
 * @param type $result
 * @param type $order
 * @param type $limit
 * @param type $start
 * @return boolean
 */
function getWhere($table, $where, $result = FALSE, $order = '', $limit = '', $start = 0) {
    $ci = & get_instance();
    if ($where != '') {
        $ci->db->where($where);
    }
    if ($order != '') {
        $ci->db->order_by($order);
    }
    if ($limit != '') {
        $ci->db->limit($limit, $start);
    }
    $query = $ci->db->get($table);
    if ($query->num_rows() == 0) {
        return FALSE;
    } else {
        if ($result == TRUE) {
            return $query->result();
        } else {
            return $query->row();
        }
    }
}

/**
 * checks if the user is logged in and redirects to login if not logged in
 */
function is_logged_in() {
    $ci = &get_instance();
    if ($ci->session->userdata('user_profile')) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * returns the money formatted value
 * @param type $money
 * @return type
 */
function show_money($money) {
    return config_item('currency') . ' ' . number_format($money, 2);
}

/**
 * returns sub domain name 
 * @param type $site_id
 * @return type
 */
function get_sub_domain($site_id) {
    return getwhere('site', array('id' => $site_id))->sub_domain;
}

/**
 * 
 * @param type $ent_id
 * @return intreturns site id on basis of ent id
 */
function get_siteId_by_entId($ent_id) {
    $row = getwhere('site', array('enterprise_id' => $ent_id));
    if ($row) {
        return $row->id;
    } else {
        return 1;
    }
}

/**
 * returns the breadcrumb in html format
 * @param type $breadcrumb
 * @return string
 */
function display_breadcrumb($breadcrumb) {
    $bread = '<ol class="breadcrumb">';
    $bread .= '<li><a href="' . base_url() . '">Home</a></li>';
    foreach ($breadcrumb as $key => $value):
        if ($value == ''):
            $bread .= '<li class="active">' . ucfirst($key) . '</li>';
        else:
            $bread .= '<li><a href="' . base_url() . 'admin/' . $value . '">' . ucfirst($key) . '</a></li>';
        endif;
    endforeach;
    $bread .= '</ol>';
    return $bread;
}

/**
 * returns the image html format
 * @param type $image
 * @param type $title
 * @param type $folder
 * @param type $w
 * @param type $h
 * @param type $class
 * @param type $alt
 * @param type $sample
 */
function show_image($image, $title, $folder = 'banners', $w = 'auto', $h = 'auto', $class = 'img-responsive', $alt = FALSE, $sample = 'logo.png') {
    $return = '';
    if ($image != '') {
        $return = '<img src="' . base_url() . $folder . '/' . $image . '" title="' . $title . '" alt="' . $title . '"  height="' . $h . '"  width="' . $w . '" class="' . $class . '"/>';
    } else {
        if ($alt) {
            $return = '<img src="' . base_url() . $sample . '" title="' . $title . '"  alt="' . $title . '"  height="' . $h . '"  width="' . $w . '" alt="' . $title . '" class="' . $class . '"/>';
        }
    }
    echo $return;
}

/**
 * checks if the values is set if not sets 0 to it especially for checkbox
 * @param type $input
 * @return type
 */
function filter_checkbox($input) {
    $return = !isset($input) ? '0' : $input;
    return $return;
}
/**
 * date format to send to database
 * @param type $date
 * @return type
 */
function db_format($date) {
    return date('Y-m-d', strtotime($date));
}


/**
 * date format to present to users
 * @param type $date
 * @return string
 */
function user_format($date) {
    $time = strtotime($date);
    if ($time == 0) {
        return ' - ';
    } else {
        return date('M d Y', strtotime($date));
    }
}

?>
