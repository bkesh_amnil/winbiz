<?php
function getTableName($moduleName = ""){
	switch($moduleName){
		case "menu":
			$tblName = "tbl_menu";
			$fld = "menu_title";
			break;
		case "content":
			$tblName = "tbl_content";
			break;
		case "gallery":
			$tblName = "tbl_gallery";
			break;
		case "banner":
			$tblName = "tbl_banner";
			break;
		case "news":
			$tblName = "tbl_news";
			break;
		case "advertisement":
			$tblName = "tbl_advertisement";
			break;
		case "material":
			$tblName = "tbl_material";
			break;
		default:
			$tblName = "";
	}
	return $tblName;	
}
function getViewFromModule($moduleName, $type = ''){
	switch($moduleName){
		case "menu":
			if($type == 'thumb'){
				$viewFile = "modules/menu_thumb";
			}else{
				$viewFile = "";
			}
			break;
		case "sub_menu":
			if($type == 'thumb'){
				$viewFile = "modules/sub_menu_thumb";
			}else{
				$viewFile = "";
			}
			break;
		case "content":
			if($type == 'list'){
				$viewFile = "modules/view_content_list";
			}else if($type == 'thumb'){
				$viewFile = "modules/view_content_thumb";
			}else if($type == 'thumb_1'){
				$viewFile = "modules/view_content_thumb_1";
			}else if($type == 'full'){
				$viewFile = "modules/view_content_full";
			}else if($type == 'full_home'){
				$viewFile = "modules/view_home_content_full";
			}else{
				$viewFile = "";
			}
			break;
		case "gallery":
			if($type == 'list'){
				$viewFile = "modules/view_gallery_list";
			}else if($type == 'thumb'){
				$viewFile = "modules/view_gallery_thumb";
			}else if($type == 'full'){
				$viewFile = "modules/view_gallery_full";
			}
			else if($type == 'side_list'){
				$viewFile = "modules/view_gallery_sidelist";
			}else{
				$viewFile = "modules/view_gallery_scroll";
			}
			break;
		case "news":
			if($type == 'list'){
				$viewFile = "modules/view_news_list";
			}else if($type == 'thumb'){
				$viewFile = "modules/view_news_sidelist";
			}else if($type == 'full'){
				$viewFile = "modules/view_news_full";
			}else if($type == 'schemes_list'){
				$viewFile = "schemes/view_schemes_list";	
			}
			else if($type == 'side_list'){
				$viewFile = "modules/view_news_sidelist";
			}
			else if($type == 'detail'){
				$viewFile = "public/modules/news_detail_full";
			}else{
				$viewFile = "public/modules/view_news_scroll";
			}
			break;
		case "advertisement":
			if($type == 'list'){
				$viewFile = "modules/view_advertisement_list";
			}else if($type == 'thumb'){
				$viewFile = "modules/view_advertisement_thumb";
			}else if($type =='img'){
				$viewFile = "modules/view_advertisement_titleimg";
			}
			else if($type =='full'){
				$viewFile = "modules/view_add_full";
			}
			else{
				$viewFile = "public/modules/view_advertisement_scroll";
			}
			break;
		case "material":
			if($type == 'grid' || $type == 'list'){
				$viewFile = "public/modules/view_material_list";
			}else if($type == 'thumb'){
				$viewFile = "modules/view_material_thumb";
			}else if($type == 'full'){
				$viewFile = "modules/view_material_full";
			}else{
				$viewFile = "public/modules/view_material_scroll";
			}
			
			break;
		case "dynamic_form":
			if($type == 'list' || $type == 'full') {
				$viewFile = "modules/view_form_single";
			} else {
				$viewFile = "modules/view_form_single";	
			}
			break;
		case "form_fields":
			///$CI = & get_instance();
			//$CI->load->model('dynamic_form/form_model');
			if($type == 'full') {
				$viewFile = "modules/form_fields";	
			} 
			elseif($type == 'thumb') {
				$viewFile = "modules/form_fields_thumb";	
			} 
			elseif($type == 'product_full') {
				$viewFile = "modules/product_full";	
			}
			else {
				//$viewFile = "public/modules/view_single_list";
				$viewFile = "";	
			}
			break;
		default:
			$viewFile = "";
	}
	return $viewFile;
}
function getBreadCrumb($current_menu = "", $urls = array(), $alias = false){
	
	$CI = & get_instance();
	$CI->load->model('public/public_model');
	$bcoms = '<li><a href="'.site_url().'">Home</a></li>';
	$link = site_url()."page";
	if(is_array($urls)){
		//printr($urls);
		foreach($urls as $ind=>$val){
			if(empty($val)){
				continue;	
			}
			$menuDet = 	$CI->public_model->get_menu($val);
			//printr($menuDet);
			if($menuDet){
			$linkType = $menuDet['menu_link_type'];
			if($linkType == "page"){
				$link .= "/".$val;
				$menuLink = $link.".html";
			}else if($linkType == "url"){
				$menuLink = $menuDet['menu_url'];	
			}else{
				$menuLink = "#";	
			}
			
			
			if($menuDet['menu_opens'] == "new"){
				$target = 'target="_blank"';	
			}else{
				$target = '';
			}
			//$link .= "/".$val;
			if($val == $current_menu && $alias == false){
				$bcoms .= '  <li class="unavailable"><span class="pl-10">'.$menuDet['menu_title'].'</span></li>';
			}else{
				
				$bcoms .= '<li> <a href="'.$menuLink.'"  '.$target.(($menuLink == "#")? 'onclick="javascript: return false;"' : '').'>'.$menuDet['menu_title'].'</a></li>';
			}
		}
		}
	}
	if($alias)
		$bcoms .= '  <li class="unavailable"><span class="pl-10">'.$alias.'</span></li>';
	return $bcoms;
}
function getArticlePart($desc, $return = "part"){
	$desc = splitString($desc);
	$part = $desc[0];
	$partFound =  false;	
	if(count($desc) > 1){
		$partFound =  true;	
	}
	unset($desc[0]);
	if(is_array($desc)){
		$fullstring = $part." ".implode("<hr />", $desc); 
	}else{
		$fullstring = $part;	
	}
	if($return == "part"){
		if($partFound == true){
			return $part;	
		}else{
			return limitPartWord($part, 200);	
		}
	}else{
		return $fullstring;	
	}
}
function splitString($str){
	$desc = explode("<hr />", $str);
	if(count($desc) < 2){
		$desc = explode("<hr/>", $str);
		if(count($desc) < 2){
			$desc = explode("<hr >", $str);
			if(count($desc) < 2){
				$desc = explode("<hr>", $str);
			}
		}
	}
	return $desc;
}
function limitPartWord($part, $limit = 100){
	$part = explode(" ", strip_tags($part));
	$returnStr = "<p>";
	$limStr = "";
	$count = 0;
	foreach($part as $ind=>$val){
		if($count > $limit){
			break;	
		}
		$count = $count + strlen($val);
		$limStr = $limStr . " ". $val;
	}
	$returnStr = $returnStr.$limStr." ...</p>";
	return $returnStr;
}
?>
