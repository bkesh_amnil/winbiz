<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MyLibrary {

    function MyLibrary() {
        $this->CI = & get_instance();
    }

    function get_post_array($not) {
        $array = array();
        foreach ($_POST as $key => $value) {
            $match = false;
            foreach ($not as $vals) {
                if ($key == $vals) {
                    $match = true;
                }
            }
            if ($match == false) {
                $array[$key] = $this->CI->input->post($key);
            }
        }
        return $array;
    }

    function send_email($to, $subject, $message, $header, $from, $attatch = NULL, $atach = NULL) {
        //echo $header; exit;
        $CI = & get_instance();
        $CI->load->library('email');

        $CI->email->clear(true);

        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'mailtrap.io',
        'smtp_port' => 2525,
        'smtp_user' => 'afb0f752f7b546',
        'smtp_pass' => '116b413a1b0ea8',
        'crlf' => "\r\n",
        'newline' => "\r\n",

        'mailtype' => 'html',
        'charset' => 'utf-8',
        'priority' => '1',
        'wordwrap' => TRUE
        );

        $CI->email->initialize($config);
        if (!empty($attatch))
            $CI->email->attach($attatch);
        if (!empty($atach))
            $CI->email->attach($atach);
        $CI->email->from($from, $header);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if ($CI->email->send())
            return TRUE;
        else
            return FALSE;
    }

}

//class closes