<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class BASE_Cart extends CI_Cart {

    function __construct() {
        parent::__construct();
        $this->product_name_rules = '\d\D';
    }

    /**
     * filter the carts as per the site domain
     * @return type
     */
    public function filter_carts() {
        $carts = array();
        if (!empty($this->contents())) {
            foreach ($this->contents() as $row) {
                $carts[$row['option']['site']][] = $row;
            }
        }
//        dumparray($carts);
        return $carts;
    }

    /**
     * returns the total of the cart 
     * @param type $cart
     * @return type
     */
    function get_cart_total($cart) {
        $total = 0;
        foreach ($cart as $row) {
            $total += $row['qty']*$row['price'];
        }
        return $total;
    }

}

/* End of file MY_Cart.php */
/* Location: ./system/application/libraries/MY_Cart.php */