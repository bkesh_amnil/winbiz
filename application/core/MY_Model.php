<?php
if(!defined('BASEPATH'))
    exit('No direct script access is allowed');

class MY_Model extends CI_Model{
    var $table_name = '';
    protected $primary_key = NULL;
    public function __construct($table_name = NULL, $primary_key = NULL){
        parent::__construct();
        $this->table_name = $table_name;
        $this->primary_key = $primary_key;
    }
    
    public function insertRow($data, $table_name = '', $insert_id = true){
        if($table_name == '')
            $table_name = $this->table_name;
        if(!$this->db->insert($table_name, $data))
            return false;
        return $insert_id ? $this->db->insert_id() : true;
    }
    
    public function getFromId($id, $table_name = ''){
        if($table_name == '')
            $table_name = $this->table_name;
        return $this->db->get_where($table_name, array('id' => $id))->row();
    }
    
    public function updateRow($id, $data, $primary_key = '', $table_name = ''){
        if($table_name == '')
            $table_name = $this->table_name;
        if($primary_key == '')
            $primary_key = $this->primary_key;
        $this->db->where($primary_key, $id);
        return $this->db->update($table_name, $data);
    }
    
    public function deleteRow($id, $primary_key = '', $table_name = ''){
        if($table_name == '')
            $table_name = $this->table_name;
        if($primary_key == '')
            $primary_key = $this->primary_key;
        return $this->db->delete($table_name, array($primary_key => $id));
    }
    
    public function insertRows($data, $table_name = '') {
        if ($table_name == '')
            $table_name = $this->table_name;
        return $this->db->insert_batch($table_name, $data);
    }
}
