<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_Controller extends Public_Controller {

	var $data = array();
	
	public function __construct(){
		parent::__construct();
        $this->data['theme_path'] = 'themes/frontend';
        $this->load->model('city/City_model');
        $cities = $this->City_model->getCity();
        $this->data['cities'] = $cities;
        $this->breadcrumb->append_crumb('Kathmandu', base_url());
	}

	protected function get_per_page(){
		$perpage = $this->uri->segment('5');
		if($perpage != 0 && (!empty($perpage)))
			$this->session->set_userdata('parent_perpage', $perpage);
		if($this->session->userdata('parent_perpage'))
			return $this->session->userdata('parent_perpage');
		else
			return $this->config->item('perpage');		
	}

    public function is_ajax_request()
    {
        return ($this->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest');
    }

	//for pagination 
    protected function paginate($perpage, $total, $base_url, $uri_segment) {
        if ($total > $perpage) {
            $this->load->library('pagination');

            $config['base_url'] = site_url($base_url);
            $config['total_rows'] = $total;
            $config['per_page'] = $perpage;
            $config['uri_segment'] = $uri_segment;

            $config['first_link'] = '&lsaquo; First';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['last_link'] = 'Last &rsaquo;';

            $config['full_tag_open'] = '<ul class="dataTables_paginate paging_full_numbers" id="da-pagination" style="margin-bottom: 0px;">';
            $config['full_tag_close'] = '</ul>';

            $config['first_tag_open'] = '<li class="first paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="last paginate_button">';
            $config['last_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="paginate_active">';
            $config['cur_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li class="next paginate_button">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="previous paginate_button">';
            $config['prev_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();

        }
        
        if($total > 0) {
            $offset = $this->uri->segment($uri_segment);
            $from = $offset + 1;
            $to = ($offset + $perpage > $total) ? $total : $offset + $perpage;
            $this->data['pagination_details'] = $to == 1 ? 'Showing 1 entry' : 'Showing ' . $from . ' to ' . $to . ' of ' . $total . ' entries';
        }
    }
}

/* End of file Front_Controller.php */
/* Location: ./application/core/Front_Controller.php */