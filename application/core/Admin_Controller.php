<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	var $data = array();
    public function __construct(){
       
        parent::__construct();
        
        $this->data['javascripts'] = array();
        $this->data['theme_path'] = 'themes/backend/';
        $this->data['title'] = 'JustCallNepal::Admin';
        $this->form_validation->set_error_delimiters('<label for="name" generated="true" class="error">', '</label>');
        
    
        if($this->uri->segment(1))
            $this->data['current_controller'] = $this->uri->segment(1);
        else
            $this->data['current_controller'] = '';
	 }
}

/* End of file Admin_Controller.php */
/* Location: ./application/core/Admin_Controller.php */