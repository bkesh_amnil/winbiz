<?php

class BASE_Controller extends CI_Controller {

    protected $template = array();
    protected $base_template = "layout/layout";
    protected $current_user = array();

    function __construct() {
        parent::__construct();
        //$this->load->library('session');
        //track the IP if unique store in the database.
        $newip = $this->input->ip_address();
        // //check if the IP is unique

        $this->db->select('id')
                ->from('tbl_ip_track')
                ->where('ip_address', $newip)
                ->limit(1);
        $query = $this->db->get();



        if ($query->num_rows() < 1) {
            $this->db->insert('tbl_ip_track', array('ip_address' => $newip));
        }

        // Assign current_user to the instance so controllers can use it
//        $this->current_user = $this->check_login()? Model_User::find(Arr::get(Auth::get_user_id(), 1)) : null;
        if ($this->check_login()) {
            $this->template['current_user'] = $this->session->all_userdata();
            $this->current_user = $this->session->all_userdata();
        } else {
            $this->template['current_user'] = null;
            $this->current_user = null;
        }

        $this->template['carts'] = $this->cart->filter_carts();

//        // Set a global variable so views can use it
//        View::set_global('current_user', $this->current_user);
        // $this->load_view();
//        // Check authentication
//        $no_redirect = array(
//            'users/login');
//        if ($this->ion_auth->logged_in() == false && ! in_array(uri_string(), $no_redirect)) {
//            redirect('users/login');
//        }
//        
//        $this->parser = new Textile\Parser();
//        
//        $this->load->library('zf_cache', array('lifetime' => 900));
//        $this->zf_cache = $this->zf_cache->get_instance();
//        
//        $this->output->enable_profiler(ENVIRONMENT == 'development');
    }

    public function check_login() {
        return ($this->session->userdata('logged_in'));
    }

//    public function user_login() {
//        $newdata = array(
//            'username' => 'johndoe',
//            'email' => 'johndoe@some-site.com',
//            'logged_in' => TRUE
//        );
//
//        $this->session->set_userdata($newdata);
//    }
//    public function user_logout() {
//        $this->session->sess_destroy();
//    }

    public function _output($output) {

        echo $this->load->view($this->base_template, $this->template, TRUE);
    }

}
