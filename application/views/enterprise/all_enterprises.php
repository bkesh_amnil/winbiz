<div class="container">
    <div class="row">
         <div class="col-sm-12">
            <?php
            if(isset($breadcrumb) && !empty($breadcrumb)) {
                $this->load->view('common/breadcrumb', $breadcrumb);
            }
            ?>
        </div>
        <div class="col-sm-9">
            <?php if(isset($enterprises) && !empty($enterprises) && is_array($enterprises)) { ?>
            <div class="row">
                <div class="col-xs-fixing">
                    <?php foreach($enterprises as $enterprise) { ?>
                        <div class="col-sm-4 col-xs-6">
                            <a href="<?php echo site_url('cat/'.$enterprise_category_id.'/'.$enterprise->id.'/'.$enterprise->enterprise_alias) ?>" class="image-wrapper company-logo ">
                                <?php if(!empty($enterprise->enterprise_logo)) { ?>
                                    <img class="img-responsive" src="<?php echo base_url('image/enterprise/100/100/'.$enterprise->enterprise_logo) ?>" alt="<?php echo $enterprise->enterprise_name ?>">
                                <?php } else { ?>
                                    <img class="img-responsive" src="<?php echo base_url('images/company-logo.png') ?>" alt="<?php echo $enterprise->enterprise_name ?>">
                                <?php } ?>
                                <span><?php echo $enterprise->enterprise_name ?></span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-sm-3">
            <aside>
                <?php
                if($count > 3) {
                    $this->load->view('category/enterprise_product_categories', $enterprise_product_category);
                } else {
                    //$this->load->view('category/enterprise_categories', $enterprise_categories);
                }
                ?>
            </aside>
            <?php $this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) ?>
            <h2 class="content-header">Top Viewed <span>products</span></h2>
            <div class="row">
                <?php $this->load->view('products/inner_best_selling_products', $best_selling_products); ?>
            </div>
        </div>
    </div>
</div>