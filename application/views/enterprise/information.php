<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php
            if (isset($breadcrumb) && !empty($breadcrumb)) {
                $this->load->view('common/breadcrumb', $breadcrumb);
            }
            ?>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-md-5 col-sm-7">
                    <?php if (isset($enterprise_products) && !empty($enterprise_products[0]->product_cover_image) && is_array($enterprise_products)) { ?>
                        <div id="sync1" class="owl-carousel">
                            <?php foreach ($enterprise_products as $product_cover_image) { ?>
                                <a href="<?php echo base_url('image/product_cover_image/0/0/' . $product_cover_image->product_cover_image) ?>" class="product-popup" rel="product_images"><img class="img-responsive" src="<?php echo base_url('image/product_cover_image/600/600/' . $product_cover_image->product_cover_image) ?>" alt="<?php echo $product_cover_image->product_name ?>"></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($enterprise_products) && !empty($enterprise_products[0]->product_cover_image) && is_array($enterprise_products)) { ?>
                        <div id="sync2" class="owl-carousel">
                            <?php foreach ($enterprise_products as $product_cover_image) { ?>
                                <img class="img-responsive" src="<?php echo base_url('image/product_cover_image/350/350/' . $product_cover_image->product_cover_image) ?>" alt="<?php echo $product_cover_image->product_name ?>">
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-7 col-sm-12">
                    <div class="product-detail">
                        <h2>
                            <?php echo $enterprise_info->enterprise_name ?>
                            <?php if (!empty($enterprise_info->site_offline) && !empty($enterprise_info->sub_domain) && $enterprise_info->site_offline == 'no') { ?>
                                <?php
                                $p = explode('.', $sub_domain_url);
                                $a = explode('//', $p[0]);
                                $sub_domain_url = $a[0] . '//' . $p[1] . '.' . $a[1] . '.' . $p[2] . '.' . $p[3] . '.' . $p[4];
                                ?><a href="javascript:void(0);" url="<?php echo $sub_domain_url ?>" class="sub-domain_link"><?php echo $sub_domain_url ?></a>
                            <?php } ?>
                            <?php if (!empty($enterprise_info->enterprise_website)) { ?>
                                <a href="<?php echo $enterprise_info->enterprise_website ?>" target="_blank"><?php echo $enterprise_info->enterprise_website ?></a>
                        <?php } ?>
                        </h2>
<?php if (!empty($enterprise_info->enterprise_short_description)) { ?>
                            <div class="ent_short_desc" style="display:block;">
                                <p><?php echo $enterprise_info->enterprise_short_description ?></p>
                            </div>
                        <?php } ?>
                        <?php if (!empty($enterprise_info->enterprise_description)) { ?>
                            <a class="read-more" href="javascript:void(0);">Show More</a>
                            <?php } ?>
                        <div class="social-link">
                            <?php if (!empty($enterprise_info->enterprise_facebook_link)) { ?><a class="fb" href="<?php echo $enterprise_info->enterprise_facebook_link ?>" target="_blank"></a><?php } ?>
                            <?php if (!empty($enterprise_info->enterprise_twitter_link)) { ?><a class="twitter" href="<?php echo $enterprise_info->enterprise_twitter_link ?>" target="_blank"></a><?php } ?>
<?php if (!empty($enterprise_info->enterprise_linkedin_link)) { ?><a class="instagram"href="<?php echo $enterprise_info->enterprise_linkedin_link ?>" target="_blank"></a><?php } ?>
<?php if (!empty($enterprise_info->enterprise_youtube_link)) { ?><a class="linked-in" href="<?php echo $enterprise_info->enterprise_youtube_link ?>" target="_blank"></a><?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                        <?php if (!empty($enterprise_info->enterprise_description)) { ?>
                        <div class="ent_long_desc" style="display:none;">
    <?php echo $enterprise_info->enterprise_description ?>
                            <a class="read-more-inside" href="javascript:void(0);">Show Less</a>
                        </div>

<?php } ?>
                </div>
            </div>
            <h3 class="may-like content-header"><span>Our Products</span></h3>
                <?php if (isset($enterprise_products) && !empty($enterprise_products) && is_array($enterprise_products)) { ?>
                <div class="row">
                <?php $this->load->view('products/related_products', $enterprise_products); ?>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-3">
                <?php $this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) ?>
            <h2 class="content-header"><span>Top Viewed products</span></h2>
            <div class="row">
<?php $this->load->view('products/inner_best_selling_products', $best_selling_products); ?>
            </div>
        </div>
    </div>
</div>