<div class="container">
    <div class="row">
        
        <div class="col-sm-9 col-sm-push-3">
           <aside class="hidden-lg hidden-md">
                <?php $this->load->view('category/enterprise_product_categories', $enterprise_product_category); ?>
            </aside>
            <?php 
            if(isset($breadcrumb) && !empty($breadcrumb)) {
                $this->load->view('common/breadcrumb', $breadcrumb);
            }
            ?>
            <h2 class="content-header">Search <span>results</span> for <?php echo '<u>'.$search_text.'</u>' ?></h2>
            <?php
            $array = array();
            if(isset($search_result) && !empty($search_result) && is_array($search_result)) {
                foreach($search_result as $ind => $search_res) {
                    $aClass = 'image-wrapper';
                    if($ind == 'Enterprise') {
                        $aClass = 'image-wrapper company-logo';
                    }
                ?>
                <div class="row">
                    <div class=" col-xs-fixing">
                    <div class="col-lg-12">
                        <h2 class="content-header"><?php echo $ind ?></h2></div>
                        <?php 
                        foreach($search_res as $val) {
                            if($ind != 'Enterprise') {
                                $rating = $this->public_model->getProductRating($val['id']);
                            }
                            if(!in_array($val['data'], $array)) {
                                array_push($array, $val['data']);
                        ?>
                            <div class="col-sm-4 col-xs-6">
                                <a href="<?php echo $val['url'] ?>" class="<?php echo $aClass ?>">
                                    <img class="img-responsive" src="<?php echo $val['img'] ?>" alt="<?php echo $val['data'] ?>">
                                    <span><?php echo $val['data'] ?></span>
                                    <?php if($ind != 'Enterprise') { ?>
                                    <div class="individual-star" data-score="<?php echo $rating['rate'] ?>"></div>
                                    <?php } ?>
                                </a>
                            </div>
                        <?php }} ?>
                    </div>
                </div>
                <?php
                }
            }
             else { ?>
                <p><b> No search Results Found</b></p>
           <?php }
       
            ?>
        </div>
        <div class="col-sm-3 col-sm-pull-9">
            <aside class="hidden-xs hidden-sm">
				<?php $this->load->view('category/enterprise_product_categories', $enterprise_product_category); ?>
            </aside>
            <?php $this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) ?>
            <h2 class="content-header">Top View <span>products</span></h2>
            <div class="row">
                <?php $this->load->view('products/inner_best_selling_products', $best_selling_products); ?>
            </div>
		</div>
    </div>
</div>