<form id="cartForm" method="post" name="cartForm" action="carts/add">
    <input type="hidden" name="product_id" id="" value="<?php echo $product_info->id ?>" />
    <input type="hidden" name="ent_id" id="" value="<?php echo $ent_id ?>" />
    <div class="addcart">
        <ul class="clearfix">
            <li>Quantity</li>
            <li>
                <select name="qty">
                    <?php
                    for ($i = 1; $i <= 10; $i++) {
                        echo '<option>' . $i . '</option>';
                    }
                    ?>
                </select>
            </li>
            <li><button type="submit">Add To Cart <i class="fa fa-cart-arrow-down"></i></button></li>
        </ul>
    </div>
</form>