<div class="container">
    <div class="row">
		<div class="col-sm-12">
        <?php 
            if(isset($breadcrumb) && !empty($breadcrumb)) {
                $this->load->view('common/breadcrumb', $breadcrumb);
            }
        ?>
        </div>
		<div class="col-sm-9">
            <div class="row">
                <div class="col-md-5 col-sm-7">
                    <div id="sync1" class="owl-carousel">
                    	<?php if(isset($product_info->product_cover_image) && !empty($product_info->product_cover_image)) {?>
                    	<a href="<?php echo base_url('image/product_cover_image/0/0/'.$product_info->product_cover_image) ?>" class="product-popup"><img class="img-responsive" src="<?php echo base_url('image/product_cover_image/350/350/'.$product_info->product_cover_image) ?>" alt="<?php echo $product_info->product_name ?>"></a>
                    	<?php } ?>
                        <?php 
                        if(isset($product_images) && !empty($product_images) && is_array($product_images)) {
                            foreach($product_images as $product_image) {
                        ?>
                            <img class="img-responsive" src="<?php echo base_url('image/product_image/350/350/' .$product_image->product_image) ?>" alt="<?php echo $product_image->product_image_title ?>">
                        <?php
                            }
                        }
                        ?>
                    </div>
                    <div id="sync2" class="owl-carousel">
                    	<?php if(isset($product_info->product_cover_image) && !empty($product_info->product_cover_image)) { ?>
                    	<img class="img-responsive" src="<?php echo base_url('image/product_cover_image/350/350/'.$product_info->product_cover_image) ?>">
                    	<?php } ?>
                        <?php 
                        if(isset($product_images) && !empty($product_images) && is_array($product_images)) {
                            foreach($product_images as $product_image) {
                        ?>
                            <img class="img-responsive" src="<?php echo base_url('image/product_image/350/350/' .$product_image->product_image) ?>" alt="<?php echo $product_image->product_image_title ?>">
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12">
                    <div class="product-detail">
                        <div class="off-canvas">
                            <?php $this->load->view('form/product_enquiry_form') ?>
                        </div>
                        <div class="off-canvas-review">
                            <?php $this->load->view('form/product_review_form') ?>
                        </div>
                        <div id="saved-star"><span class="votes"><?php echo !empty($product_rate['count']) ? '(' .$product_rate['count'] . ' votes )' : 'Not Rated yet.' ?> <?php echo $product_rate['rate'] ?></span></div> 
                        <h2><?php echo $product_info->product_name ?></h2>
                        <span class="price">
                        	<?php
                        	if($product_info->show_product_price) {
                        		echo $product_info->product_price;
                        	} else {
                        		echo 'Price on Request';
                        	}
                        	?>
                    	</span>
                        <?php echo !empty($product_info->product_short_description) ? $product_info->product_short_description : '<p>No Product Description Added as of Yet.</p>' ?>
                        <a href="javascript:void(0);" class="btn-buy">Enquiry</a><a href="javascript:void(0);" class="btn-review">Review & Rating</a><!--
                        --><div class="social-link enquiry-social-link">
                            <a class="fb" id="share_button" href="http://www.facebook.com/sharer.php?u=<?php echo base_url('share/'.$product_info->id) ?>" target="_blank"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul id="tab" class="nav nav-tabs responsive-tab">
                        <li class="active"><a href="#description" data-toggle="tab">Product Description</a></li>
                        <li><a href="#review" data-toggle="tab">Product Review</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="description">
                            <?php echo (!empty($product_info->product_description) ? $product_info->product_description : '<p>No Description added as of Yet.</p>') ?>
                        </div>
                        <div class="tab-pane fade in" id="review">
                            <?php
                            if(isset($product_reviews) && !empty($product_reviews) && is_array($product_reviews)) {
                                foreach($product_reviews as $product_review) {
                                ?>
                                <div class="review-detail">
                                    <span>
                                        <?php echo $product_review->customer_name . '(' . $product_review->review_date . ')' ?>
                                        <div class="individual-star" data-score="<?php echo $product_review->rate ?>"></div>
                                    </span>
                                    <p><?php echo $product_review->review; ?></p>
                                </div>
                                <?php
                                }
                            } else {
                                echo '<p>No Review made as of Yet.</p>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="may-like content-header"><span>You may also like</span></h3>
            <?php if(isset($enterprise_products) && !empty($enterprise_products) && is_array($enterprise_products)) { ?>
                <div class="row">
                    <?php $this->load->view('products/related_products', $enterprise_products) ?>
                </div>
            <?php } ?>
		</div>
		<div class="col-sm-3">
            <?php $this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) ?>
            <h2 class="content-header"><span>Top Viewed products</span></h2>
            <div class="row">
                <?php $this->load->view('products/inner_best_selling_products', $best_selling_products); ?>
            </div>
        </div>
	</div>
</div>
<input type="hidden" id="hidden_total_rate" value="<?php echo $product_rate['rate'] ?>" />