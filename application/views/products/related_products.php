<div class="col-xs-fixing" id="products_all">
    <div class="col-lg-12">
        <section class="sub-filter">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-lg-4">
                    <select class="form-control">
                        <option>All</option>
                        <option>Clothes</option>
                        <option>Mens</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4">
                    <select class="form-control">
                        <option>Price</option>
                        <option>1000</option>
                        <option>5000</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4">
                    <select class="form-control">
                        <option>Popularity</option>
                        <option>Latest</option>
                    </select>
                </div>
            </div>
        </section>
        
        <span class="products-found">(31 products found) - from anta in Nepal</span>
    </div>
	<?php 
	if(!empty($enterprise_products[0]->product_name)) {
    	foreach($enterprise_products as $enterprise_product) {
	    	$rating = $this->public_model->getProductRating($enterprise_product->id);
		?>
	    <div class="col-sm-4 col-xs-6 product_category_<?php echo $enterprise_product->product_category_id ?>">
	        <div class="image-wrapper">
	            <img class="img-responsive" data-echo="<?php echo base_url('image/product_cover_image/600/600/'.$enterprise_product->product_cover_image) ?>" src="<?php echo base_url('images/blank.gif'); ?>" alt="<?php echo $enterprise_product->product_name ?>">
                    <span class="tag-discount">-23%</span>
	            <div class="individual-star" data-score="<?php echo $rating['rate'] ?>"></div>
	            <span><?php echo $enterprise_product->product_name ?></span>
                    <span class="uploader"><i class="fa fa-user"></i> Anta</span>
                    <div class="overlay">
                        <a href="<?php echo site_url('detail/'.$enterprise_category_id.'/'.$enteprise_id.'/'.$enterprise_product->product_category_id.'/'.$enterprise_product->id.'/'.$enterprise_product->product_alias) ?>" class="btn-detail">ADD</a>
                        <div class="like-share">
                            <a href=""><i class="fa fa-share-alt"></i></a>
                        </div>
                    </div>
	        </div>
	    </div>
    <?php 
		}
    } else { ?>
	<div class="col-sm-4 col-xs-6"><span><?php echo 'No Products added as of yet.' ?> </span></div>
	<?php } ?>
</div>