<?php if(isset($best_selling_products) && !empty($best_selling_products) && is_array($best_selling_products)) { ?>
<div class="container">
            <h2 class="content-header"><span>top viewed products</span></h2>
            <div class="row">
                <div id="top-view-slider" class="owl-carousel">
                    <?php 
                    foreach($best_selling_products as $ind => $best_selling_product) { 
                        $rating = $this->public_model->getProductRating($best_selling_product->prod_id);
                        $i = $ind + 1;
                    ?>
                    <div class="item">
                        <a href="<?php echo site_url('detail/'.$best_selling_product->ent_cat_id.'/'.$best_selling_product->ent_id.'/'.$best_selling_product->prod_cat_id.'/'.$best_selling_product->prod_id.'/'.$best_selling_product->product_alias) ?>"  class="image-wrapper">
                            <?php if(!empty($best_selling_product->product_cover_image)) { ?>
                                <img class="img-responsive" data-echo="<?php echo base_url('image/product_cover_image/600/600/'.$best_selling_product->product_cover_image) ?>" src="<?php echo base_url('images/blank.gif'); ?>" alt="<?php echo $best_selling_product->product_name ?>">
                            <?php } ?>
                            <div class="individual-star" data-score="<?php echo $rating['rate'] ?>"></div>
                            <span><?php echo $best_selling_product->product_name ?></span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <?php if(!empty($bottom_right_bigyapan)) { ?>
           <!--  <div class="col-sm-3">
                <?php $this->load->view('bigyapan/bottom_bigyapan', $bottom_right_bigyapan); ?>
            </div> -->
        <?php } ?>
</div>
<?php } ?> 