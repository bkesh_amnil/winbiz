<?php if(isset($best_selling_products) && !empty($best_selling_products)) { ?>
<div class="col-xs-fixing">
    <?php foreach($best_selling_products as $best_selling_product) { ?>
    <div class="col-sm-12 col-xs-6">
        <a href="<?php echo site_url('detail/'.$best_selling_product->ent_cat_id.'/'.$best_selling_product->ent_id.'/'.$best_selling_product->prod_cat_id.'/'.$best_selling_product->prod_id.'/'.$best_selling_product->product_alias) ?>" id="fade-it-<?php echo $i ?>" class="image-wrapper">
        	<?php if(!empty($best_selling_product->product_cover_image)) { ?>
            	<img class="img-responsive" data-echo="<?php echo base_url('image/product_cover_image/340/203/'.$best_selling_product->product_cover_image) ?>" src="<?php echo base_url('images/blank.gif'); ?>" alt="<?php echo $best_selling_product->product_name ?>">
        	<?php } ?>
            <span><?php echo $best_selling_product->product_name ?></span>
        </a>
    </div>
    <?php } ?>
</div>
<?php } ?>