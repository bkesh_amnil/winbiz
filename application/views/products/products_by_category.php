<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php 
                if(isset($breadcrumb) && !empty($breadcrumb)) {
                    $this->load->view('common/breadcrumb', $breadcrumb);
                }
            ?>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="product-detail">
                        <?php if(isset($products) && !empty($products)) { ?>
                        <div class="row">
                         <div class="col-lg-12">
                            <h2 class="content-header"><span>Products</span></h2>
                            </div>
                            </div>
                        <div class="row">
                        	<div class="col-xs-fixing" id="products_all">
                        		<?php 
								if(!empty($products[0]->product_name)) {
							    	foreach($products as $enterprise_product) {
								    	$rating = $this->public_model->getProductRating($enterprise_product->id);
									?>
								    <div class="col-sm-4 col-xs-6 product_category_<?php echo $enterprise_product->product_category_id ?>">
								        <a href="<?php echo site_url('detail/'.$enterprise_product->enterprise_category_id.'/'.$enterprise_product->enterprise_id.'/'.$enterprise_product->product_category_id.'/'.$enterprise_product->id.'/'.$enterprise_product->product_alias) ?>" class="image-wrapper">
								            <img class="img-responsive" data-echo="<?php echo base_url('image/product_cover_image/600/600/'.$enterprise_product->product_cover_image) ?>" src="<?php echo base_url('images/blank.gif'); ?>" alt="<?php echo $enterprise_product->product_name ?>">
								            <span><?php echo $enterprise_product->product_name ?></span>
								            <div class="individual-star" data-score="<?php echo $rating['rate'] ?>"></div>
								        </a>
								    </div>
							    <?php 
									}
							    } else { ?>
								<div class="col-sm-4 col-xs-6"><span><?php echo 'No Products added as of yet.' ?> </span></div>
								<?php } ?>
                        	</div>
                        </div>
                    	<?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php if(!empty($enterprise_info->enterprise_description)) { ?>
                        <div class="ent_long_desc" style="display:none;">
                            <?php echo $enterprise_info->enterprise_description ?>
                            <a class="read-more-inside" href="javascript:void(0);">Show Less</a>
                        </div>

                    <?php } ?>
                </div>
            </div>
            <!-- <h3 class="may-like content-header"><span>Our Products</span></h3> -->
            <?php /*if(isset($enterprise_products) && !empty($enterprise_products) && is_array($enterprise_products)) { ?>
                <div class="row">
                    <?php $this->load->view('products/related_products', $enterprise_products); ?>
                </div>
            <?php }*/ ?>
        </div>
        <div class="col-sm-3">
            <?php $this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) ?>
            <h2 class="content-header"><span>Top Viewed products</span></h2>
            <div class="row">
                <?php $this->load->view('products/inner_best_selling_products', $best_selling_products); ?>
            </div>
        </div>
    </div>
</div>