<?php if (isset($best_selling_products) && !empty($best_selling_products) && is_array($best_selling_products)) { ?>
        <div class="col-sm-12">
            <div class="inner-products-list">
                <?php foreach ($best_selling_products as $best_selling_product) { ?>
                    <div class="media">
                        <a href="<?php echo site_url('detail/' . $best_selling_product->ent_cat_id . '/' . $best_selling_product->ent_id . '/' . $best_selling_product->prod_cat_id . '/' . $best_selling_product->prod_id . '/' . $best_selling_product->product_alias) ?>"
                           class="media-left">
                            <?php if (!empty($best_selling_product->product_cover_image)) { ?>
                                <img class="media-object"
                                     data-echo="<?php echo base_url('image/product_cover_image/600/600/' . $best_selling_product->product_cover_image) ?>"
                                     src="<?php echo base_url('images/blank.gif'); ?>"
                                     alt="<?php echo $best_selling_product->product_name ?>">
                            <?php } ?>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $best_selling_product->product_name ?></h4>
                                <?php echo substr(strip_tags($best_selling_product->product_short_description), 0, 42);
                                echo('...'); ?>
                            </div>
                    </div>
                <?php } ?>
            </div>
        </div>
<?php } ?>