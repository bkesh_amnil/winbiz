<p>Hi, <?php echo $user_info->first_name. ''.$user_info->last_name;?></p>


<h1>
    Your Account has been successfully created. 
    In order to  complete your registration.
</h1>

<p>
    Click the following link to activate your account.
</p>

<h3>
    <?php echo base_url('users/activation/' . $user_info->activation_code); ?>
</h3>

<p>
    This activation code is only valid until 24 hours.
    And is one time Use. Expires after one time use.
</p>

