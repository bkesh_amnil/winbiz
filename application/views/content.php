<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<?php 
                if(isset($breadcrumb) && !empty($breadcrumb)) {
                    $this->load->view('common/breadcrumb', $breadcrumb);
                }
            ?>
            <div class="inner-content">
                <?php if(isset($content) && !empty($content)){ ?>
                    <h2 class="content-header"><span><?php echo $content->title ?></span></h2>
                    <?php echo $content->description ?>
                <?php } else { ?>
                    <p>No Content Added as of Yet.</p>
                <?php } ?>
            </div>
		</div>
		<div class="col-sm-3">
            <div class="row">
                <div class="col-xs-fixing">
                    <?php if(!empty($top_right_bigyapan)) { ?>
                    <div class="col-sm-12 col-xs-6">
                        <?php $this->load->view('bigyapan/top_bigyapan', $top_right_bigyapan); ?>
                    </div>
                    <?php } ?>
                    <?php if(!empty($center_right_bigyapan)) { ?>
                        <!-- <div class="col-sm-12 col-xs-6">
                            <?php $this->load->view('bigyapan/left_bigyapan', $center_right_bigyapan); ?>
                        </div> -->
                    <?php } ?>
                </div>
            </div>
        </div>
	</div>
</div>
<section class="best-selling-section">
    <?php $this->load->view('products/best_selling_products', $best_selling_products); ?>
</section>