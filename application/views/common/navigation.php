<body class="body-overflow">
    <div class="loader-wrap">
        <div class="loader">
            <img src="<?php echo base_url('images/loader.gif') ?>">
        </div>
    </div>
    <?php if ($special_offer) { ?>
        <section class="special-offer" style="display: none;">
            <div class="container">
                <div class="collapse in">
                    <button type="button" class="btn btn-close">Close</button>
                    <h2>Special Offer</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
        </section>
    <?php
    }
    if ($site_id == '1') {
        $header_class = "site-header";
    } else {
        $header_class = "enterprise-template-body";
    }
    if ($site_id != '1') {
        ?>
    
        <ul class="header-top-section">
                <div class="container">
                <?php
                if (is_logged_in()) {
                    ?>
                    <!--<li><a  href="dashboard/orderList" >My Orders</a></li>-->
                    <li class="has-dropdown">
                        <a  href="javascript:void(0)" class="user-loggedin" ><i class="fa fa-user"></i>John Doe <span class="drop-arrow"></span></a>
                        <div class="dropdown-top">
                            <a href="">My Account</a>
                            <a href="">Logout</a>
                        </div>
                    </li>
                    <li class="has-dropdown">
                        <a  href="javascript:void(0)"><i class="fa fa-shopping-cart"></i>Cart <span class="drop-arrow"></span></a>
                        <div class="dropdown-top">
                            <h5>Items in your cart</h5>
                            <div class="nav-cart-list">
                                <img src="http://bee.winbiz.dev/image/bigyapan/870/432/bigyapan_353_1459335805.jpg" /><!--
                                --><div class="short-list">
                                    <h6>Honey 1000g</h6>
                                    <span>Qty<span>2</span></span>
                                    <span>Price(Rs.)<span>2000</span></span>
                                </div>
                            </div>
                            <div class="nav-cart-list">
                                <img src="http://bee.winbiz.dev/image/bigyapan/870/432/bigyapan_353_1459335805.jpg" /><!--
                                --><div class="short-list">
                                    <h6>Honey 1000g</h6>
                                    <span>Qty<span>2</span></span>
                                    <span>Price(Rs.)<span>2000</span></span>
                                </div>
                            </div>
                            <a href="carts" class="btn-viewall">View All</a>
                        </div>
                    </li>
                    <li><a href="users/logout" ><i class="fa fa-sign-out"></i>Logout</a></li>
                <?php } else { ?>
                    <li><a href="users/login"><i class="fa fa-sign-in"></i>Login/Register</a></li>
                    <!--<li><a  href="users/register">Register</a></li>-->

                <?php } ?>
                </div>
            </ul>
    
        <div class="top-hover">
            <nav class="navbar enterprise-template" role="navigation">
                <section class="header-top">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo $main_host; ?>">
                            <img class="img-responsive" src="<?php echo base_url('winbiz-system/uploaded_files/site_logo/' . $main_site_logo) ?>">
                        </a>
                        <div class="header-top-content">
                            <div class="search-wrapper">
                                <form style="margin:0;" method="post" id="form-search" action="<?php echo base_url('search') ?>">
                                    <div class="dropdown search-option-dropdown">
                                        <button data-toggle="dropdown" type="button" class="btn btn-primary dropdown-toggle search-site" aria-expanded="false">On Site
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li class='winbiz-search'><a href="<?php echo base_url('search'); ?>">On Site</a></li>
                                            <li class='winbiz-search'><a href="<?php echo $main_host . 'search'; ?>">On Winbiz</a></li>
                                        </ul>
                                    </div>
                                    <div class="search">
                                        <input type="text" id="search_text" name="search_text" placeholder="<?php echo (isset($search_text) && !empty($search_text) ? $search_text : '') ?>" style="outline:none">
                                        <input type='hidden' id='base_url' value='<?php echo base_url(); ?>'>
                                        <input type='hidden' id='main_host' value='<?php echo $main_host; ?>'>
                                        <button type="submit" id="search-form" class="btn btn-search">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </nav>
        <?php }else{ ?>
            <ul class="header-top-section">
                <div class="container">
                <?php
                if (is_logged_in()) {
                    ?>
                    <!--<li><a  href="dashboard/orderList" >My Orders</a></li>-->
                    <li class="has-dropdown">
                        <a  href="javascript:void(0)" class="user-loggedin" ><i class="fa fa-user"></i>John Doe <span class="drop-arrow"></span></a>
                        <div class="dropdown-top">
                            <a href="">My Account</a>
                            <a href="">Logout</a>
                        </div>
                    </li>
                    <li class="has-dropdown">
                        <a  href="javascript:void(0)"><i class="fa fa-shopping-cart"></i>Cart <span class="drop-arrow"></span></a>
                        <div class="dropdown-top">
                            <h5>Items in your cart</h5>
                            <div class="nav-cart-list">
                                <img src="http://bee.winbiz.dev/image/bigyapan/870/432/bigyapan_353_1459335805.jpg" /><!--
                                --><div class="short-list">
                                    <h6>Honey 1000g</h6>
                                    <span>Qty<span>2</span></span>
                                    <span>Price(Rs.)<span>2000</span></span>
                                </div>
                            </div>
                            <div class="nav-cart-list">
                                <img src="http://bee.winbiz.dev/image/bigyapan/870/432/bigyapan_353_1459335805.jpg" /><!--
                                --><div class="short-list">
                                    <h6>Honey 1000g</h6>
                                    <span>Qty<span>2</span></span>
                                    <span>Price(Rs.)<span>2000</span></span>
                                </div>
                            </div>
                            <a href="carts" class="btn-viewall">View All</a>
                        </div>
                    </li>
                    <li><a href="users/logout" ><i class="fa fa-sign-out"></i>Logout</a></li>
                <?php } else { ?>
                    <li><a href="users/login"><i class="fa fa-sign-in"></i>Login/Register</a></li>
                    <!--<li><a  href="users/register">Register</a></li>-->

                <?php } ?>
                </div>
            </ul>
        <?php }?>
        <header class="<?php echo $header_class ?>">
            <nav class="navbar" role="navigation">
                <!-- social icons starts -->
                <?php if ($site_id == '1') {
                    if (!empty($social_medias[0]->facebook_url) || !empty($social_medias[0]->twitter_url) || !empty($social_medias[0]->instagram_url) || !empty($social_medias[0]->linkedin_url)) {
                        ?>
                        <section class="header-social">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="social-link">
        <?php if (!empty($social_medias[0]->facebook_url)) { ?>
                                                <a href="<?php echo(isset($social_medias[0]->facebook_url) ? $social_medias[0]->facebook_url : '') ?>"
                                                   target="_blank" class="fb"></a>
                                            <?php } ?>

        <?php if (!empty($social_medias->twitter_url)) { ?>
                                                <a href="<?php echo(isset($social_medias[0]->twitter_url) ? $social_medias[0]->twitter_url : '') ?>"
                                                   target="_blank" class="twitter"></a>
                                            <?php } ?>

        <?php if (!empty($social_medias->instagram_url)) { ?>
                                                <a href="<?php echo(isset($social_medias[0]->instagram_url) ? $social_medias[0]->instagram_url : '') ?>"
                                                   target="_blank" class="instagram"></a>
                                            <?php } ?>

        <?php if (!empty($social_medias->linkedin_url)) { ?>
                                                <a href="<?php echo(isset($social_medias[0]->linkedin_url) ? $social_medias[0]->linkedin_url : '') ?>"
                                                   target="_blank" class="linked-in"></a>
        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php }
                }
                ?>
                <!-- social icons ends -->
                <section class="header-top">
                    <div class="container">
<?php if (isset($site_logo) && !empty($site_logo)) { ?>
                            <a class="navbar-brand" href="<?php echo site_url() ?>">
                                <img class="img-responsive" src="<?php echo base_url('winbiz-system/uploaded_files/site_logo/' . $site_logo) ?>" alt="<?php echo $site_title ?>">
                            </a>
                            <?php } else { ?>
                            <a class="navbar-name navbar-brand" href="<?php echo site_url() ?>">
                            <?php echo $site_title ?>
                            </a>
<?php } ?>
                                <?php if ($site_id == '1') { ?>
                            <div class="header-top-content">
                                <div class="search-wrapper">
    <?php $this->load->view('common/search') ?>
                                </div>
                                <div class="logo">
                                    <h2>Supported by:</h2>
                                    <a href="<?php echo $logos->first_link ?>"> 
                                        <img src="<?php echo base_url('winbiz-system/uploaded_files/logos/' . $logos->first_logo) ?>" alt="<?php echo $logos->first_title ?>" title="<?php echo $logos->first_title ?>">
                                    </a>
                                    <a href="<?php echo $logos->second_link ?>">
                                        <img src="<?php echo base_url('winbiz-system/uploaded_files/logos/' . $logos->second_logo) ?>" alt="<?php echo $logos->second_title ?>" title="<?php echo $logos->second_title ?>">
                                    </a>
                                    <a href="<?php echo $logos->third_link ?>">
                                        <img src="<?php echo base_url('winbiz-system/uploaded_files/logos/' . $logos->third_logo) ?>" alt="<?php echo $logos->third_title ?>" title="<?php echo $logos->third_title ?>" >
                                    </a>
                                </div>
                            </div>
<?php } ?>
                    </div>
                </section>
                <section class="header-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <button class="navbar-toggle" data-target="#main-navbar-collapse" data-toggle="collapse" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-sm-9 col-sm-push-3">
                                <div id="main-navbar-collapse" class="collapse navbar-collapse">
                                        <?php if (isset($menus) && !empty($menus) && is_array($menus)) { ?>
                                        <ul class="nav navbar-nav">
                                            <?php
                                            foreach ($menus as $menu) {
                                                $url = site_url($menu['menu_alias']);
                                                $liClass = "";
                                                if ($active_menu == $menu['menu_alias']) {
                                                    $liClass = " active";
                                                }

                                                if ($menu['menu_alias'] == 'home') {
                                                    $url = site_url();
                                                }

                                                if (isset($menu['childs']) && !empty($menu['childs'])) {
                                                    ?>
                                                    <li class="dropdown<?php echo $liClass ?>">
                                                        <a href="<?php echo $url ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <?php echo $menu['menu_title'] ?>
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                                    <?php foreach ($menu['childs'] as $mChild) { ?>
                                                                <li>
                                                                    <a href="<?php echo site_url($mChild['menu_alias']) ?>">
                                                                <?php echo $mChild['menu_title'] ?>
                                                                    </a>
                                                                </li>
                                                    <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li class="<?php echo $liClass ?>">
                                                        <a href="<?php echo $url ?>">
                                                    <?php echo $menu['menu_title'] ?>
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
<?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-3 col-sm-pull-9">
                                <aside>
                                    <?php
                                    if ($site_id == '1') {
                                        $this->load->view('category/enterprise_categories', $enterprise_category);
                                    } else {
                                        
                                        $this->load->view('category/enterprise_product_categories', $enterprise_product_category);
                                    }
                                    ?>
                                </aside>
                            </div>
                        </div>
                    </div>
                </section>
            </nav>
        </header>
<?php if ($site_id != '1') { ?>
        </div>
        <?php } ?>