

<?php
if (!empty($carts)) {
    if ($site_id != 1) {
        $site_cart = $carts[$site_id];
        $i = 1;
        if (!empty($site_cart)) {
            foreach ($site_cart as $cp) {
                echo $i++ . '. ' . $cp['id'] . ' / ' . $cp['name'] . ' / ' . show_money($cp['price']) . ' / ' . $cp['qty'] . '<br>';
            }
            echo "<a href='checkout'>Checkout</a>";
        }
    } else {
        foreach ($carts as $site => $cart) {
            echo '<strong>' . getWhere('site', array('id' => $site))->site_title . '</strong><br>';
            $i = 1;
            foreach ($cart as $cp) {
                echo $i++ . '. ' . $cp['id'] . ' / ' . $cp['name'] . ' / ' . show_money($cp['price']) . ' / ' . $cp['qty'] . '<br>';
            }
            $sub_domain = get_sub_domain($site);
//            echo 'okay boys'.$sub_domain.'--';

            $sub_url = ($sub_domain) ? "http://$sub_domain.$default_host_name" : "http://$default_host_name";
            $url_target = ($sub_domain) ? "_blank" : "";
            echo "<a href='$sub_url/checkout' target='$url_target'>Checkout</a> / ";
            echo "<a href='$sub_url/carts' target='$url_target'>Carts</a>";
            echo '<br><br>';
        }
    }
}
?>

<hr>