<body>
    <div class="loader-wrap">
        <div class="loader">
            <img src="<?php echo base_url('images/loader.gif') ?>">
        </div>
    </div>
    <?php if($special_offer) { ?>
    <section class="special-offer" style="display: none;">
        <div class="container">
            <div class="collapse in">
                <button type="button" class="btn btn-close">Close</button>
                <h2>Special Offer</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
    </section>
    <?php } ?>
    <header class="">
        <nav class="navbar" role="navigation">
            <!-- social icons starts -->
            <section class="header-social">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="social-link">
                                <a href="<?php echo (isset($social_medias['facebook']) ? $social_medias['facebook'] : '') ?>" target="_blank" class="fb"></a>
                    <!-- 
                     -->
                    <a href="<?php echo (isset($social_medias['twitter']) ? $social_medias['twitter'] : '') ?>" target="_blank" class="twitter"></a>
                    <!--
                    -->
                    <a href="<?php echo (isset($social_medias['instagram']) ? $social_medias['instagram'] : '') ?>" target="_blank" class="instagram"></a>
                    <!--
                    -->
                    <a href="<?php echo (isset($social_medias['linkedin']) ? $social_medias['linkedin'] : '') ?>" target="_blank" class="linked-in"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- social icons ends -->
            <section class="header-top">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo site_url() ?>">
                        <img class="img-responsive" src="<?php echo base_url('winbiz-system/uploaded_files/site_logo/'.$site_logo) ?>">
                    </a>
                    <div class="header-top-content">
                        <div class="search-wrapper">
                            <?php $this->load->view('common/search') ?>
                        </div>
                        <div class="logo">
                            <img src="<?php echo base_url('images/logo-1.jpg') ?>">
                            <img src="<?php echo base_url('images/logo-2.png') ?>">
                            <img src="<?php echo base_url('images/logo-3.png') ?>">
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-target="#main-navbar-collapse" data-toggle="collapse" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <div id="main-navbar-collapse" class="collapse navbar-collapse">
                            <?php if(isset($menus) && !empty($menus) && is_array($menus)) { ?>
                            <ul class="nav navbar-nav">
                                <?php
                                foreach($menus as $menu) {
                                    $url = site_url($menu['menu_alias']);
                                    $liClass = "";
                                    if($active_menu == $menu['menu_alias']) {
                                        $liClass = " active";
                                    }
                                    
                                    if($menu['menu_alias'] == 'home') {
                                        $url = site_url();
                                    }
                                    
                                    if(isset($menu['childs']) && !empty($menu['childs'])) {
                                    ?>
                                    <li class="dropdown<?php echo $liClass ?>">
                                        <a href="<?php echo $url ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <?php echo $menu['menu_title'] ?>
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php foreach($menu['childs'] as $mChild) { ?>
                                            <li>
                                                <a href="<?php echo site_url($mChild['menu_alias']) ?>">
                                                    <?php echo $mChild['menu_title'] ?>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php
                                    } else {
                                    ?>
                                    <li class="<?php echo $liClass ?>">
                                        <a href="<?php echo $url ?>">
                                            <?php echo $menu['menu_title'] ?>
                                        </a>
                                    </li>
                                    <?php    
                                    }
                                }
                                ?>
                            </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>