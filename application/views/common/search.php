<form style="margin:0;" id="form-search" method="post" action="<?php echo base_url('search') ?>">
    <div class="search">
        <input type="text" id="search_text" name="search_text" placeholder="<?php echo (isset($search_text) && !empty($search_text) ? $search_text: "I'm looking for...") ?>" style="outline:none">
        <button type="submit" id="search-form" class="btn btn-search">Search</button>
    </div>
</form>