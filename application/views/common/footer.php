<input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url() ?>">
<input type="hidden" id="searchUrl" value="<?php echo base_url('ajax/getEnterpriseorProduct') ?>" ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                <img class="footer-logo" src="<?php echo base_url('images/winbiz-logo.png') ?>">
                <p class="footer-text"><?php echo $site_description ?></p>
            </div>
            <?php 
            if(isset($footer_menus) && !empty($footer_menus) && is_array($footer_menus)) { 
                foreach($footer_menus as $ind => $footer_menu) {
                ?>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                    <span class="footer-header"><?php echo $ind ?></span>
                    <?php if(is_array($footer_menu)) { ?>
                        <div class="footer-links">
                            <?php foreach($footer_menu as $val) { ?>
                                <a href="#"><?php echo $val->menu_title ?></a>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <p class="footer-text"><?php echo $footer_menu->description ?></p>
                    <?php } ?>
                </div>
                <?php
                }
            ?>
            <?php
            }
            ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <span class="copyright">Copyright © <?php echo date('Y') .' '. $site_title ?></span>
            </div>
            <div class="col-sm-6">
                <div class="social-link">
                    <a href="<?php echo (isset($social_medias['facebook']) ? $social_medias['facebook'] : '') ?>" target="_blank" class="fb"></a>
                    <!-- 
                     -->
                    <a href="<?php echo (isset($social_medias['twitter']) ? $social_medias['twitter'] : '') ?>" target="_blank" class="twitter"></a>
                    <!--
                    -->
                    <a href="<?php echo (isset($social_medias['instagram']) ? $social_medias['instagram'] : '') ?>" target="_blank" class="instagram"></a>
                    <!--
                    -->
                    <a href="<?php echo (isset($social_medias['linkedin']) ? $social_medias['linkedin'] : '') ?>" target="_blank" class="linked-in"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<a href="#0" class="cd-top">Top</a>
<!-- Script -->
<script type="text/javascript" src="<?php echo base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('js/bootstrap-tabcollapse.js') ?>" type="text/javascript"></script>
<!-- SmartMenus jQuery plugin -->
<script type="text/javascript" src="<?php echo base_url('js/vendor/smartmenus-0.9.7/jquery.smartmenus.min.js') ?>"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo base_url('js/vendor/smartmenus-0.9.7/jquery.smartmenus.bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('js/custom-tab-collapse.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('js/custom-dropdown.js') ?>" type="text/javascript"></script>
<!-- Owl Carousel plugin -->
<script src="<?php echo base_url('js/vendor/owl-carousel/owl.carousel.js') ?>"></script>
<script type="text/javascript">
    setTimeout(function() {
        $(".loader-wrap").fadeOut("slow");
        $("body").removeClass("body-overflow");
    }, 5000);
</script>
<script>
 $(document).ready(function() {
    			$('.winbiz-search').on('click',function(e){
    			e.preventDefault();
    			var text=$(this).find('a').text();
    			text+='<span class="caret"></span>';
    			$('.search-site').html(text);
    			var url = $(this).find('a').attr('href');
    			$('#form-search').attr('action',url);

    			});
    		});

</script>
<?php if($active_menu == 'home') { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#banner-slider").owlCarousel({
                autoPlay: 3000,
                navigation: false,
                slideSpeed: 300,
                pagination: true,
                singleItem: true
            });
        });
    </script>
<?php } ?>
<script type="text/javascript">
        $(document).ready(function() {
            $("#top-view-slider").owlCarousel({
                // autoPlay: 3000,
                navigation: true,
                slideSpeed: 300,
                pagination: false,

                items : 5,
                itemsCustom : false,
                itemsDesktop : [1199,5],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false
            });
        });
    </script>
<?php if($active_menu == 'cat') { ?>
    <script type="text/javascript">
        $(document).on("click", ".read-more", function() {
            var btn = $(this);
            if($(this).hasClass("more")) {
                $('html, body').animate({
                    scrollTop: 42
                }, "slow"); 
                $(".ent_long_desc").slideToggle( "slow" );
                btn.removeClass("more");
                btn.text("Show More");
            } else {
                $('html, body').animate({
                    scrollTop: 371
                }, "slow"); 
                $(".ent_long_desc").slideToggle( "slow" );
                btn.addClass("more");
                btn.text("Show Less");
            }
        })
        $(document).on("click", ".read-more-inside", function() {
            $('html, body').animate({
                scrollTop: 42
            }, "slow");
            $(".ent_long_desc").slideToggle( "slow" );
            $(".read-more").removeClass("more");
            $(".read-more").text("Show More");
        })
        $("a.sub-domain_link").on("click", function() {
            var url = $(this).attr('url');
            window.location.href = url;
        })
    </script>
<?php } ?>
<?php  if($active_menu == 'detail' || $active_menu == 'cat') { ?>
    <script src='<?php echo base_url('vendor/fancybox/jquery.fancybox.pack.js') ?>'></script>
    <script>
    	(function() {
    		$(document).ready(function() {
    			$('.product-popup').fancybox();
    		});
    	})();
    </script>
<?php } ?>

<?php if($active_menu == 'detail' || $active_menu == 'cat' || $active_menu == 'search') { ?>
    <script type="text/javascript ">
        $(document).ready(function() {
            /* description image slider */
            var sync1 = $("#sync1");
            var sync2 = $("#sync2");

            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 1000,
                navigation: true,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
            });

            sync2.owlCarousel({
                items: 5,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 4],
                itemsTablet: [768, 6],
                itemsMobile: [479, 4],
                pagination: false,
                responsiveRefreshRate: 100,
                margin: 5,
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync2")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#sync2").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync2").on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }
            }
            /* description image slider */
            /* featured products slider */
            $("#one-slider").owlCarousel({
                autoPlay: 3000,
                navigation: false,
                slideSpeed: 300,
                pagination: false,
                singleItem: true
            });
            $("#two-slider").owlCarousel({
                autoPlay: 3000,
                navigation: false,
                slideSpeed: 300,
                pagination: false,
                singleItem: true
            });
            /* featured products slider */
        });

        /*$(document).on("click", ".product-category_id", function() {
            $('html, body').animate({
                scrollTop: $('div.col-md-7').offset().top
            }, 700);
            var prod_cat_id = $(this).attr("rel");
            $('.category-list li').removeClass("active");    
            var parents = $(this).parent().addClass("active");
            $("div.product_category_" + prod_cat_id).css("display", "block");;
            $('div#products_all div:not(.product_category_'+prod_cat_id+')').css("display", "none");
            return false;
        })*/
    </script>
<?php } ?>
<!-- Parallax -->
<script type="text/javascript" src="<?php echo base_url('js/vendor/parallax/TweenMax.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/vendor/parallax/jquery.superscrollorama.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/vendor/parallax/jquery.lettering-0.6.1.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var windowWidth = $(window).width();
            if (windowWidth >= 768) {
                var controller = $.superscrollorama();
                // individual element tween examples
                controller.addTween('.services-section', TweenMax.from($('#translate-bottom-1'), .5, {
                    css: {
                        top: '800px'
                    },
                    ease: Quad.easeInOut
                }), 0, -250);
                controller.addTween('.services-section', TweenMax.from($('#translate-bottom-2'), 1, {
                    css: {
                        top: '800px'
                    },
                    ease: Quad.easeInOut
                }), 0, -250);
                controller.addTween('.services-section', TweenMax.from($('#translate-bottom-3'), 1.5, {
                    css: {
                        top: '800px'
                    },
                    ease: Quad.easeInOut
                }), 0, -250);
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-1'), .3, {
                    css: {
                        opacity: 0
                    }
                }));
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-2'), .4, {
                    css: {
                        opacity: 0
                    }
                }));
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-3'), .5, {
                    css: {
                        opacity: 0
                    }
                }));
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-4'), .6, {
                    css: {
                        opacity: 0
                    }
                }));
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-5'), .7, {
                    css: {
                        opacity: 0
                    }
                }));
                controller.addTween('.best-selling-section', TweenMax.from($('#fade-it-6'), .8, {
                    css: {
                        opacity: 0
                    }
                }));
            }
    });
</script>
    <!-- Fixed Header -->
<script type="text/javascript">
    /*$(function() {
        $(window).scroll(function() {
            var windowWidth = $(window).width();
            if (windowWidth < 1024) {
                return false;
            }
            var navbarTopHeight = 45;
            var scrolled = $(document).scrollTop();
            if (scrolled < navbarTopHeight) {
                $(".site-header .navbar").removeAttr("style");
                $(".site-header .navbar").removeClass("navbar-fixed-top");
                $("body").css("padding-top", 0);
            }
            if (scrolled >= navbarTopHeight) {
               // $(".navbar").css(", "tr ease anslateY(-45px)");
                //$(".navbar").css("-webkit-, "tr ease anslateY(-45px)");
                $(".site-header .navbar").addClass("navbar-fixed-top");
                setTimeout(function() {
                    $(".site-header .navbar").css("transition", "800ms ease ");
                }, 200);
                $("body").css("padding-top", $(".site-header .navbar").height());
            }
        });
    });*/
</script>
<script type="text/javascript">
    /*$(function() {
        $(window).scroll(function() {
            var windowWidth = $(window).width();
            if (windowWidth > 1024) {
            
                $(".enterprise-template.navbar").addClass("navbar-fixed-top");
                $(".enterprise-template-body").css("margin-top", $(".enterprise-template.navbar").height());
                
	            var navbarTopHeight = 62;
	            var scrolled = $(document).scrollTop();
	            if (scrolled < navbarTopHeight) {
	                $(".enterprise-template-body .navbar").removeAttr("style");
	                $(".enterprise-template-body .navbar").removeClass("navbar-fixed-top");
	                $("body").css("padding-top", 0);
	                $(".enterprise-template-body .header-top").css("margin", "10px 0 15px");
	                $(".enterprise-template-body .header-nav").css("margin-top", "0");

                    // $(".enterprise-template-body .navbar-brand").css("opacity", "1");
	            }
	            if (scrolled >= navbarTopHeight) {
	                $(".enterprise-template-body .navbar").addClass("navbar-fixed-top");
	                $(".enterprise-template-body .navbar").css("transition", "550ms ease ");
	                $(".enterprise-template-body .header-top").css("margin", "0");
	                $(".enterprise-template-body .navbar-fixed-top").css("top", $(".enterprise-template").height());
	                $(".enterprise-template").css("z-index", "99999");
	                $(".enterprise-template-body .header-nav").css("margin-top", "-20px");

                    // $(".enterprise-template-body .navbar-brand").css({"opacity" : "0","transition" : "opacity 800ms"});
	            }
            }
        });
    });*/
</script>
<?php if($active_menu == 'detail' || $active_menu == 'contact-us') { ?>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.validate.min.js') ?>"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo base_url('js/vendor/raty/jquery.raty.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap-tabcollapse.js') ?>"></script>
    <script type="text/javascript">
        //$(document).ready(function() {
            $('#saved-star').raty({
                path: $("#baseUrl").val() + 'js/vendor/raty/images',
                readOnly: true,
                half: true,
                hints: [['0.5', '1'], ['1.5', '2'], ['2.5', '3'], ['3.5', '4'], ['4.5', '5']],
                precision: true,
                score: $('#hidden_total_rate').val(),
            })
            $('.individual-star').raty({
                path: $("#baseUrl").val() + 'js/vendor/raty/images',
                readOnly: true,
                score: function() {
                    return $(this).attr('data-score');
                }
            })
        //})
        $('#tab').tabCollapse();
        $('#star').raty({
            path: $("#baseUrl").val() + 'js/vendor/raty/images',
            half: true,
            hints: [['0.5', '1'], ['1.5', '2'], ['2.5', '3'], ['3.5', '4'], ['4.5', '5']],
            precision: false
        });
        </script>
<?php if($active_menu == 'detail' || $active_menu == 'cat' || $active_menu == 'search' || $active_menu == 'home' || $active_menu == 'success-story' || $active_menu == 'about-us'){ ?>
    <script type="text/javascript">
        
        /* form open close */
        $(document).ready(function(){
            $(".btn-buy").click(function(){
                $(".product-detail").addClass("show");
                $(".off-canvas").addClass('show');
                $('.form-component').show();
                $('div.message-holder').css('display','none');
                $('.btn-send').show();
            });
            $(".btn-back").click(function(){
                $(".product-detail").removeClass("show");
                $(".off-canvas").removeClass('show');
            });
            $(".btn-review").click(function(){
                $(".product-detail").addClass("show");
                $(".off-canvas-review").addClass('show');
                $('.form-component-review').show();
                $('div.message-holder-review-success').html('');
                $('div.message-holder-review-error').html('');
                $('.btn-send-review').show();
            });
            $(".btn-back-review").click(function(){
                $(".product-detail").removeClass("show");
                $(".off-canvas-review").removeClass('show');
            });
        });
        /* form open close */
        /* form submit */
        $(document).off('click', '.btn-send');
        $(document).on('click', '.btn-send', function (e) {
           e.stopPropagation();
            e.preventDefault();
            var $this = $(this);
            var form_name = $(this).parents('form').attr('name');
            var form = $(this).parents('form');
            var data = form.serialize();
            var action = form.attr('action');

            form.validate();
            if(form.valid()) {
                $('.processing').css('display','block');
                $.ajax({
                    url: action,
                    data: data,
                    type: 'post',
                    dataType: 'JSON',
                    success: function (data) {console.log(data);
                        $('.processing').css('display','none');
                        if(data.class != 'error') {
                            $('.message-holder').css('display','block');
                            $('.message-holder').html('<div class="alert alert-' + data.class + '">' +
                                '<a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>' +
                                data.msg + '</div>');
                            $this.parents('form')[0].reset();
                            $('.form-component').hide();
                            $('.btn-send').hide();
                        } else {
                            $('.message-holder').css('display','block');
                            $('.message-holder').html('<div class="alert alert-' + data.class + '">' +
                                '<a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>' +
                                data.msg + '</div>');
                        }
                   }
                });
            }
        });
        $(document).off('click', '.btn-send-review');
        $(document).on('click', '.btn-send-review', function (e) {
           e.stopPropagation();
            e.preventDefault();
            var $this = $(this);
            var form_name = $(this).parents('form').attr('name');
            var form = $(this).parents('form');
            var data = form.serialize();
            var action = form.attr('action');

            form.validate();
            if(form.valid()) {
                $('.processing-review').css('display','block');
                $.ajax({
                    url: action,
                    data: data,
                    type: 'post',
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.class != 'error') {
                            console.log(data);
                            $('.processing-review').css('display','none');
                            $('.message-holder-review-success').html('<div class="alert alert-success"><a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>'+data.msg+'</div>');
                            $this.parents('form')[0].reset();
                            $('.form-component-review').hide();
                            $('.btn-send-review').hide();
                            $.ajax({
                                url : $("#baseUrl").val() + 'product/refresh_review/',
                                type : "POST",
                                data : {product_id : $("input[name=product_id]").val()},
                                success : function(data) {
                                    $('div#review').html('');
                                    $('div#review').html(data);
                                    $.ajax({
                                        url : $("#baseUrl").val() + 'product/refresh_rate/',
                                        type : "POST",
                                        data : {product_id : $("input[name=product_id]").val()},
                                        dataType : "JSON",
                                        success : function(data) {
                                            $("#hidden_total_rate").val(data.rate);
                                            $("span.votes").html('');
                                            $("span.votes").html('(' + data.count + ' votes ) ' + data.rate);
                                            return false;
                                        },
                                        error : function() {
                                            alert('Error');
                                            return false;
                                        }
                                    })
                                    return false;
                                },
                                error : function() {
                                    alert('Error');
                                    return false;
                                }
                            })
                        } else {
                            $('.processing-review').css('display','none');
                            $('.message-holder-review-error').html('<div class="alert alert-danger"><a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>'+data.msg+'</div>');
                            $this.parents('form')[0].reset();
                            $('.form-component-review').hide();
                            $('.btn-send-review').hide();
                        }
                   }
                });
            }
        });
        /* form submit */
    </script>
<?php } ?>
<!-- lazy load image -->
<script src="<?php echo base_url('vendor/lazyload/echo.min.js') ?>"></script>
<script>
echo.init({
	offset: 100,
	throttle: 250,
	unload: false,
	callback: function (element, op) {
	  // console.log(element, 'has been', op + 'ed')
	}
});
</script>
<!-- Google map -->
<?php if($active_menu == 'contact-us') { ?>
    <script type="text/javascript">
        $(document).off('click', '.contact-btn-send');
        $(document).on('click', '.contact-btn-send', function (e) {
           e.stopPropagation();
            e.preventDefault();
            var $this = $(this);
            var form_name = $(this).parents('form').attr('name');
            var form = $(this).parents('form');
            var data = form.serialize();
            var action = form.attr('action');

            form.validate();
            if(form.valid()) {
                $('.processing').css('display','block');
                $.ajax({
                    url: action,
                    data: data,
                    type: 'post',
                    dataType: 'JSON',
                    success: function (data) {
                        $('.processing').css('display','none');
                        $('.message-holder').css('display','block');
                        $this.parents('form')[0].reset();
                   }
                });
            }
        });
    </script>


<script>
jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

    function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
            center: new google.maps.LatLng(27.701108,85.321795),
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);
var marker = new google.maps.Marker({
    position: new google.maps.LatLng(27.701108,85.321795),
    title:"WINBIZ"
});

var contentString = "<h3>WINBIZ</h3><p>Women in Business</p>";
var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
// To add the marker to the map, call setMap();
marker.setMap(map);
    }
</script>
<?php } ?>
<!-- Category Collapse -->
<script type="text/javascript ">
        $(document).ready(function() {
            if($(window).width() > 1024) {
            $('.aside-header').hover(function(){
                $('#category-collapse').toggleClass('active');
            });
            $('#category-collapse').hover(function(){
                $('#category-collapse').toggleClass('active');
            });
            } else {
            $('.aside-header').click(function(){
                $('#category-collapse').toggleClass('active');
            });
            $('.category-collaps').click(function(){
                $('.category-collaps').toggleClass('active');
            });
            }
        });
</script>
<script type="text/javascript ">
        $(document).ready(function() {

        });
</script>

<!--<script type="text/javascript ">
$(document).ready(function() {
var windowHeight =$(window).height();
var navHeight = $('nav').outerHeight(true);
var totaldropdownHeight = windowHeight - navHeight;
$('.category-list').css('height', totaldropdownHeight);
  $('.category-list').on('mousemove', function(e) {
    var leftOffset = $(this).offset().top;
    $('.category-list-wrap').css('top', -e.clientY + leftOffset);

    console.log($(this).outerHeight() + ' - ' + $(this)[0].scrollHeight);
  });
});
</script>-->
<script>
    $('body').on('click','#search-form',function (e)
    {
        var that = $(this);
        var search = $('#search_text');
        var valid = search.data('valid');
        if (valid === 0) { // Is valid, let's continue
            return;
        } else {
            e.preventDefault(); //prevent default form submit
                if(search.val() == false){
                        $('.search').addClass('search-error');
                        $('.search-error').css("border","1px solid rgba(255, 0, 0, 0.3)");
                        $('.search-error').css("box-shadow","0 0 6px -1px rgba(255, 0, 0, 0.6)");
                    } else {
                        search.data('valid', 0);
                        $('#form-search').submit();
                    }
                }
            });
</script>
<!-- To-Top -->
<script type="text/javascript" src="<?php echo base_url('js/vendor/to-top/modernizr.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/vendor/to-top/main.js') ?>"></script>

</body>
</html>