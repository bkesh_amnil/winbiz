<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" lang="en" xml:lang="en">
    <head>
        <title><?php echo (!empty($site_title) ? $site_title : 'WINBIZ') ?></title>
        <base href="<?php echo site_url() ?>" />
        <meta charset="utf-8">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
        <meta name="keywords" content="<?php echo $site_keywords ?>">
        <meta name="description" content="<?php echo $site_description ?>">
        <!--social share og tags -->
        <meta property="og:url" content="<?php echo (!empty($share_link) ? $share_link : base_url()) ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo (!empty($share_title) ? $share_title : $site_title) ?> " />
        <meta property="og:description" content="<?php echo (!empty($share_description) ? $share_description : $site_description) ?>" />
        <meta property="og:image" content="<?php echo (!empty($share_image) ? base_url('winbiz-system/uploaded_files/site_logo/' . $share_image) : base_url('winbiz-system/uploaded_files/site_logo/' . $site_logo)) ?>">
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- Fav-icon -->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>images/icons/favicon.png">
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.css') ?>">
        <!--  Owl stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url('css/vendor/owl-carousel/owl.carousel.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/vendor/owl-carousel/owl.theme.css') ?>">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/main.css') ?>">
        <link href="<?php echo base_url('css/login.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('css/register.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- Rating -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('js/vendor/raty/jquery.raty.css') ?>">
        <?php if ($active_menu == 'detail' || $active_menu == 'cat') { ?>  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('vendor/fancybox/jquery.fancybox.css') ?>" />
        <?php } ?>
        <!-- Theme Css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/enterprise-' . $domain_theme_css) ?>">
        <script type="text/javascript" src="<?php echo base_url('js/jquery-1.11.1.js') ?>"></script>
    </head>