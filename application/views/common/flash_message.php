<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-message" onclick="javascript:$(this).fadeOut(1000)">
        <p><strong><?php echo $this->session->flashdata('message'); ?></strong> <i class="fa fa-close"></i></p>
    </div>
<?php } else if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success" onclick="javascript:$(this).fadeOut(1000)">
        <p><strong><?php echo $this->session->flashdata('success'); ?></strong>  <i class="fa fa-close"></i></p>
    </div>
    <?php
}?>