<?php if(isset($footer_contents) && !empty($footer_contents)) { ?>
    <div class="container">
        <div class="row">
            <?php 
            foreach($footer_contents as $ind => $footer_content) {
                $i = $ind + 1;
            ?>
            <div class="col-sm-4">
                <div id="translate-bottom-<?php echo $i ?>" class="services-wrapper">
                    <?php if($footer_content->image_status == 'yes' && !empty($footer_content->title_image)) { ?>
                        <img src="<?php echo base_url('image/footer_content/130/82/'.$footer_content->title_image) ?>" alt="<?php echo $footer_content->title ?>">
                    <?php } ?>
                    <h2><?php echo $footer_content->title ?></h2>
                    <?php echo $footer_content->description ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
<?php } ?> 