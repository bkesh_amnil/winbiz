<?php
$url = base_url('detail/'.$product_info->enterprise_category_alias.'/'.$product_info->enterprise_alias.'/'.$product_info->product_category_alias.'/'.$product_info->product_alias);

if(strpos($_SERVER['HTTP_REFERER'],"facebook.com")>-1){
 header("Location:".$url);
 exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="user-scaleable=false, width=device-width, initial-scale=1">
        <meta property="og:title" content="<?php echo $product_info->product_name ?>" />
            <meta property="og:image" content="<?php echo base_url('image/product_cover_image/200/200/'.$product_info->product_cover_image) ?>" />
           <meta property="og:description" content="<?php echo $product_info->product_short_description ?>" />
      </head>
     <body>
        <?php echo $product_info->product_short_description; ?>
        <img src="<?php echo base_url('image/product_cover_image/350/350/'.$product_info->product_cover_image) ?>" />
     </body>
</html>