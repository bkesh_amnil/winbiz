<div style="display: none;" class="processing-review">
    <div class="alert alert-info">
        <a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>
        Processing ....
    </div>
</div>
<div class="message-holder-review-success">
</div>
<div class="message-holder-review-error">
</div>
<form action="<?php echo base_url('dynamic_form/product_review_submit') ?>" accept-charset="utf-8" name="product_review" method="post" class="buy-form" autocomplete="Off">
    <div class="row">
        <div class="col-sm-3 col-xs-4 form-component-review">
            <label>Full name *</label>
        </div>
        <div class="col-sm-9 col-xs-8 form-component-review">
            <input type="text" class="form-control required" name="full_name" value="<?php echo set_value('full_name'); ?>">
        </div>
        <div class="col-sm-3 col-xs-4 form-component-review">
            <label>Email *</label>
        </div>
        <div class="col-sm-9 col-xs-8 form-component-review">
            <input type="text" class="form-control required email" name="email">
        </div>
        <div class="col-sm-3 col-xs-4 form-component-review">
            <label>Review *</label>
        </div>
        <div class="col-sm-9 col-xs-8 form-component-review">
            <textarea name="review" class="required"></textarea>
        </div>
        <div class="col-sm-3 col-xs-4 form-component-review">
            <label>Rating *</label>
        </div>
        <div class="col-sm-9 col-xs-8 form-component-review">
            <div id="star"></div>
        </div>
        <div class="buy-btn-group">
            <a href="javascript:void(0);" class="btn-back-review">Back</a>
            <!-- 
             -->
            <input id="submit" class="btn-send-review" type="submit" value="Send">
            <input type="hidden" name="enterprise_id" value="<?php echo $product_info->ent_id ?>" />
            <input type="hidden" name="product_id" value="<?php echo $product_info->id ?>" />
            <input type="hidden" name="product_name" value="<?php echo $product_info->product_name ?>" />
        </div>
    </div>
</form>