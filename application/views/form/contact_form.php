<div style="display: none;" class="processing">
    <div class="alert alert-info">
        <a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>
        Processing ....
    </div>
</div>
<div class="message-holder" style="display: none;">
    <div class="alert alert-success">
        <a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>
        <?php echo (!empty($form_data->success_msg) ? $form_data->success_msg : 'Thank you for your valuable feedback.')?>
    </div>
</div>
<?php echo '<pre>'; print_r($form_data); die;
$attributes = array('name' => $form_data->form_name, 'method' => 'post', 'class' => 'contact-form', "autocomplete"=>'Off');
echo form_open_multipart('dynamic_form/form_submit',$attributes);
$where = array('form_id' => $form_data->id);
$fields = $this->db->order_by('field_order', 'ASC')->where($where)->get('tbl_form_field')->result();
foreach($fields as $field){
    $validation = explode('|' , $field->validation_rule);
    $validation_class = implode(' ', $validation);
    $divClass = 'col-lg-6';
    if($field->field_type == 'textarea') {
        $divClass = 'col-lg-12';
    }
    ?>
    <div class="<?php echo $divClass ?>">
        <?php 
        $field_name = 'field_name[' . $field->id . ']';
        switch($field->field_type){
            case "textarea":
                $attribute = 'placeholder="'.$field->default_value.'" class="'.$validation_class.'"';
                ?>
                <textarea name="<?php echo $field_name ?>" placeholder="<?php echo $field->default_value ?>" class="<?php echo $validation_class ?>"></textarea>
                <?php
                //echo form_textarea($field_name, $field->default_value, $attribute);
                break;
            case "upload":
                if( $field->default_value && file_exists('./dynamic_uploads/' . $field->default_value))
                    echo '<span>'. anchor("administrator/downloads/files/{$submitted_form_id}/{$field->id}", 'Download') . ' &nbsp; ' . anchor('#', 'Remove', 'rel="'. $field->id . '" class="remove_file"') . '</span>';
                else
                    echo form_upload($field_name, '', $attribute);
                break;
            case "password":
                echo form_password($field_name, $field->default_value, $attribute);
                break;
            case "hidden":
                echo form_hidden($field_name, $field->default_value, $attribute);
                break;
            case "select"   :
                $attribute = 'id="cmbservice" class="textBox3" class="form-control"';
                $result = $this->db->where('form_field_id', $field->id)->order_by('display_text')->get('form_field_value')->result();
                $dropdowns = dropdown_data($result, 'display_text', 'display_text');
                echo form_dropdown($field_name, $dropdowns, $field->default_value, $attribute);
                break;
            case "radio":
                $result = $this->db->where('form_field_id', $field->id)->order_by('display_text')->get('form_field_value')->result();
                foreach($result as $row){
                    $checked = ($row->display_text == $field->default_value)?TRUE:FALSE;
                    echo form_radio($field_name, $row->display_text, $checked, $attribute) . $row->display_text . ' &nbsp; ';
                }
                break;
            case "checkbox" :
                $result = $this->db->where('form_field_id', $field->id)->order_by('display_text')->get('form_field_value')->result();
                foreach($result as $row){
                    echo "<div id='chckbox'>".form_checkbox($field_name,$row->display_text, $field->default_value).$row->display_text."</div>";
                }
                break;          
            default:
                $attribute = 'type="text" name="'.$field->field_label.'" placeholder="'.$field->default_value.'" class="'.$validation_class.'"';
                echo form_input($field_name, '', $attribute);
        }
        ?>
    </div>
    <?php
}
?>
<div class="col-lg-2 col-sm-4">
    <input type="hidden" name="form_id" value="<?php echo $form_data->id ?>" />
    <input type="submit" class="contact-btn-send" value="Send">
</div>
<?php echo '</form>'; ?>
<input type="hidden" name="currentUrl" id="currentUrl" value="<?php echo current_url(); ?>" />