 <!DOCTYPE html>
<html>
    <head>
        <title>404</title>
        <meta charset="UTF-8">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="description" content="Diet Nepal">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scaleable=false" >
        <link href='http://fonts.googleapis.com/css?family=Roboto:700,300' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('css/bootstrap.css') ?>" media="all" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('css/404-page.css') ?>" media="all" rel="stylesheet" type="text/css">
    </head>
    <body>
        <section class="errorpage-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-lg-4 col-lg-offset-1">
                        <img class="img-responsive" src="<?php echo base_url();?>images/icon-404.png" />
                    </div>
                    <div class="col-xs-12 col-sm-8 col-lg-7">
                        <h1>Error 404</h1>
                        <span class="oops">Oops, Page not found!</span>
                        <span class="lost">We know you are lost. <a class="btn-back-home hvr-sweep-to-right-error" href="<?php echo $default_host_name;?>">Back to home</a></span>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>

    