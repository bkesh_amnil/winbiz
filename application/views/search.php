<div class="container">
    <div class="row">

        <div class="col-sm-9 ">
            <?php
            if (isset($breadcrumb) && !empty($breadcrumb)) {
                $this->load->view('common/breadcrumb', $breadcrumb);
            }
            ?>
            <h3>Search results for <span class="search-header"><?php echo $search_text; ?></span></h3>
            <?php
            $array = array();
            if (isset($search_result) && !empty($search_result) && is_array($search_result)) {
                foreach ($search_result as $ind => $search_res) {
                    if ($ind == 'Contents') { ?>
                        <div class="row">
                        <div class=" col-xs-fixing">
                        <div class="col-lg-12">
                            <h2 class="content-header"><span><?php echo $ind ?></span></h2>
                        <?php foreach ($search_res as $val) { ?>
                            <a href="<?php echo $val['url'] ?>" class="image-wrapper">
                                <span><?php echo $val['menu_heading'] ?></span>
                            </a>
                            <p><?php echo substr(strip_tags($val['description']), 0, 500);
                                echo ('.....') ?></p>
                        <?php }?>
                        </div>
                        </div>
                        </div>
                   <?php } else {
                        $aClass = 'image-wrapper';
                        if ($ind == 'Enterprise') {
                            $aClass = 'image-wrapper company-logo';
                        }
                        ?>

                        <div class="row">
                            <div class=" col-xs-fixing">
                                <div class="col-lg-12">
                                    <h2 class="content-header"><span><?php echo $ind ?></span></h2></div>
                                <?php
                                foreach ($search_res as $val) {
                                    if ($ind != 'Enterprise') {
                                        $rating = $this->public_model->getProductRating($val['id']);
                                    }
                                    if (!in_array($val['data'], $array)) {
                                        array_push($array, $val['data']);
                                        ?>
                                        <div class="col-sm-4 col-xs-6">
                                            <?php
                                            if ($ind == 'Enterprise') {
                                                $a = explode('/', $val['img']);
                                                if (empty($a[7])) { ?>
                                                    <a href="<?php echo $val['url'] ?>" class="image-wrapper">
                                                        <span><?php echo $val['data'] ?></span>
                                                    </a>
                                                <?php } else {
                                                    ?>
                                                    <a href="<?php echo $val['url'] ?>" class="<?php echo $aClass ?>">
                                                        <img class="img-responsive" src="<?php echo $val['img'] ?>"
                                                             alt="<?php echo $val['data'] ?>">
                                                             <span><?php echo $val['data'] ?></span>
                                                    </a>
                                                <?php }
                                            } else { ?>
                                                <a href="<?php echo $val['url'] ?>" class="<?php echo $aClass ?>">
                                                    <img class="img-responsive" src="<?php echo $val['img'] ?>"
                                                         alt="<?php echo $val['data'] ?>">

                                                    <div class="individual-star"
                                                         data-score="<?php echo $rating['rate'] ?>"></div>
                                                    <span><?php echo $val['data'] ?></span>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                        <?php
                    }
                }
            } else { ?>
                <p><b> No search Results Found</b></p>
            <?php }

            ?>
        </div>
        <div class="col-sm-3">
            <?php /*$this->load->view('bigyapan/inner_center_bigyapan', $center_bigyapans) */ ?><!--
            <h2 class="content-header">Top View <span>products</span></h2>
            <div class="row">
                <?php /*$this->load->view('products/inner_best_selling_products', $best_selling_products); */ ?>
            </div>-->
            <div class="row">
                <div class="col-xs-fixing">
                    <?php if (!empty($top_right_bigyapan)) { ?>
                        <div class="col-sm-12 col-xs-6">
                            <?php $this->load->view('bigyapan/top_bigyapan', $top_right_bigyapan); ?>
                        </div>
                    <?php } ?>
                    <?php if (!empty($center_right_bigyapan)) { ?>
                        <!-- <div class="col-sm-12 col-xs-6">
                            <?php $this->load->view('bigyapan/left_bigyapan', $center_right_bigyapan); ?>
                        </div> -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="best-selling-section">
    <?php $this->load->view('products/best_selling_products', $best_selling_products); ?>
</section>