<?php if(isset($center_bigyapans) && !empty($center_bigyapans)) { ?>
<h2 class="content-header"><span>featured products</span></h2>
    <div class="row">
        <div class="col-sm-12">
            <div class="inner-products-list">
               
                	<?php 
                	foreach($center_bigyapans as $ind => $center_bigyapan) {
                		if($ind > 2) { break; }
                        if($center_bigyapan['advinfo']->advertisement_link_type != 'None') {
                            $url = $center_bigyapan['advurl'];                     
                        } else {
                            $url = 'javascript:void(0);';
                        }
                        if($center_bigyapan['advinfo']->advertisement_link_opens == 'new'){
                        $target = '_blank';
                        }else{
                        $target = '_top';
                        }
		                ?>
                         <div class="media">
                        <a href="<?php echo $url ?>" class="media-left" target="<?php echo $target ?>">
                             <img class="media-object" src="<?php echo base_url('image/bigyapan/600/600/' . $center_bigyapan['advinfo']->advertisement_image) ?>" alt="<?php echo $center_bigyapan['advinfo']->advertisement_name ?>">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $center_bigyapan['advinfo']->advertisement_name ?></h4>
                            <?php echo substr(strip_tags($center_bigyapan['advinfo']->advertisement_description),0,42);echo('...'); ?>
                        </div>
                         </div>
                    <?php } ?>
               
            </div>
            <!-- <div class="col-sm-12 col-xs-6">
                <div id="two-slider" class="owl-carousel">
                    <?php 
                    foreach($center_bigyapans as $ind => $center_bigyapan) {
                        if($ind < 3) { continue; }
                        if($center_bigyapan['advinfo']->advertisement_link_type != 'None') {
                            $url = $center_bigyapan['advurl'];                     
                        } else {
                            $url = 'javascript:void(0);';
                        }
if($center_bigyapan['advinfo']->advertisement_link_opens == 'new'){
$target = '_blank';
}else{
$target = '_top';
}
                        ?>
                        <a href="<?php echo $url ?>" class="image-wrapper featured" target="<?php echo $target ?>">
                             <img class="img-responsive" src="<?php echo base_url('image/bigyapan/0/0/' . $center_bigyapan['advinfo']->advertisement_image) ?>" alt="<?php echo $center_bigyapan['advinfo']->advertisement_name ?>">
                            <span><?php echo $center_bigyapan['advinfo']->advertisement_name ?></span>
                        </a>
                    <?php } ?>
                </div>
            </div> -->
        </div>
    </div>
<?php } ?>