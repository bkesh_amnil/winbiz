<?php  if (isset($center_bigyapans) && !empty($center_bigyapans) && is_array($center_bigyapans)) { ?>

    <h2 class="content-header"><span>featured products  </span></h2>
    <div class="row">
        <div class="col-xs-fixing">
            <?php
            foreach ($center_bigyapans as $center_adv) {
                $rating = $this->public_model->getProductRating($center_adv['advinfo']->product_id);
                ?>
                <div class="col-sm-3 col-xs-6">
                    <?php
                    if ($center_adv['advinfo']->advertisement_link_type != 'None') {
                        $url = $center_adv['advurl'];
                    } else {
                        $url = 'javascript:void(0);';
                    }
                    if ($center_adv['advinfo']->advertisement_link_opens == 'new') {
                        $target = '_blank';
                    } else {
                        $target = '_top';
                    }
                    ?>
                    <div class="product_category_<?php echo $center_adv['advinfo']->product_category_id ?>">
                        <a href="<?php echo $url ?>" class="image-wrapper" target=<?php echo $target ?>>
                            <img class="img-responsive" data-echo="<?php echo base_url('image/bigyapan/600/600/' . $center_adv['advinfo']->advertisement_image) ?>" src="<?php echo base_url('images/blank.gif'); ?>" alt="<?php echo $center_adv['advinfo']->advertisement_name ?>">
                            <div class="individual-star" data-score="<?php echo $rating['rate'] ?>"></div>
                            <span><?php echo $center_adv['advinfo']->advertisement_name ?></span>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>