<?php 
foreach ($top_right_bigyapan as $top_adv) { ?>
    <div class="bigyapan">
        <?php
        if ($top_adv['advinfo']->advertisement_link_type != 'None') {
            $url = $top_adv['advurl'];
        } else {
            $url = 'javascript:void(0);';
        }
        if ($top_adv['advinfo']->advertisement_link_opens == 'new') {
            $target = '_blank';
        } else {
            $target = '_top';
        }
        ?>
        <a href="<?php echo $url ?>" target="<?php echo $target ?>">
            <img class="img-responsive" src="<?php echo base_url('image/bigyapan/270/432/' . $top_adv['advinfo']->advertisement_image) ?>" alt="<?php echo $top_adv['advinfo']->advertisement_name ?>">
        </a>
    </a>
    </div>
<?php } ?>