<?php if(isset($banners) && !empty($banners)) { ?>
<div id="banner-slider" class="owl-carousel">
	<?php foreach($banners as $banner) { ?>
    <div class="banner-slider-wrapper">
        <img class="img-responsive" src="<?php echo base_url('image/bigyapan/870/432/' . $banner['advinfo']->advertisement_image) ?>" alt="<?php echo $banner['advinfo']->advertisement_name ?>">
        </a>
        <div class="banner-slider-text">
        	<?php if(!empty($banner['advinfo']->advertisement_description)) { ?>
            <h2><?php echo $banner['advinfo']->advertisement_name?></h2>
            <?php echo $banner['advinfo']->advertisement_description; ?>
            <?php } ?>
            <?php 
            if($banner['advinfo']->advertisement_link_type != 'None') { ?>
            	<a href="<?php echo $banner['advurl']; ?>" class="hav-look-link" target="<?php echo $banner['advinfo']->advertisement_link_opens ?>">Have a Look</a>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>
<?php } ?>