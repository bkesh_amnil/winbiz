<?php foreach ($center_right_bigyapan as $cent_adv) { ?>
    <div class="bigyapan">
        <?php
        if ($cent_adv['advinfo']->advertisement_link_type != 'None') {
            $url = $cent_adv['advurl'];
        } else {
            $url = 'javascript:void(0);';
        }
        if ($cent_adv['advinfo']->advertisement_link_opens == 'new') {
            $target = '_blank';
        } else {
            $target = '_top';
        }
        ?>
        <a href="<?php echo $url ?>" target="<?php echo $target ?>">
            <img class="img-responsive" src="<?php echo base_url('image/bigyapan/770/243/' . $cent_adv['advinfo']->advertisement_image) ?>" alt="<?php echo $cent_adv['advinfo']->advertisement_name ?>">
        </a>
    </a>
    </div>
<?php } ?>