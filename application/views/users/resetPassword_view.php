<div class="login-page-section">
    <div class="container">
        <div class="login-page-indicator">
            <p><?php echo $page_title ?></p>
        </div>
        <?php $this->load->view('common/flash_message') ?>
        <div class="login-page">
            <div class="login-right">
                <div class="login-wrapper">
                    <span>Registered Customers</span>
                    <p>
                        If you have forgotten your password, please send in you email address to us and we will be sending you new password to your email.
                    </p>
                    <form action="users/resetPassword" method="post" class="user_form_password">
                        <div class="form-group">
                            <label for="email"><p>*</p>E-mail</label>
                            <input type="text" id="email" name="Email_Address" class="required">
                        </div>
                        <div class="login-account">
                            <div class="form-group">
                                <span>*Required Fields</span>
                            </div>
                            <span><a href='users/login'>Login Here</a></span>
                            <div class="login-button">
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
