<div class="login-page-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-lg-6">
                <div class="login-wrapper">
                    <div class="login-form">
                        <h2>Registered Customers</h2>
                        <span class="signin-desc">If you have an account with us, please log in.</span>
                        <form action="users/loginProcess" method="post" class="user_login_form">
                            <div class="form-group clearfix">
                                <input type="text" id="email" name="userEmail" placeholder="Email" class="form-control required email">
                                <label class="fa fa-envelope" for="email"></label>
                            </div>
                            <div class="form-group clearfix">
                                <input type="password" id="pwd" name="userPassword" placeholder="Password" required  class="form-control password">
                                <label for="password" class="fa fa-unlock-alt"></label>
                            </div>
                            <a href='users/resetPassword'><i class="fa fa-refresh"></i> <span>Forgot password</span></a>
                            <button class="btn-login" type="submit" name="submitUserLogin" value="submitUserLogin">Login</button>
                        </form>
                        <span class="seprator"><span>or</span></span>
                    </div>
                    <?php  $this->load->view('common/flash_message') ?>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-4 col-lg-6">
                <section class="other-login">
                        <div class="signup-option">
                            <h2 class="title-header">New Customer <a href="users/register" >Create account</a></h2>
                            <a class="signin-facebook" href="#">
                            <i class="fa fa-facebook"></i>
                            <span>Sign In with Facebook</span>
                            </a>
                            <a class="signin-google" href="#">
                            <i class="fa fa-google-plus"></i>
                            <span>Sign In with Google</span>
                            </a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>