<div class="container">
    <?php $this->load->view('common/flash_message') ?>
    <div class="content-detail-main-wrap">
        <div class="left-main-section">
            <div class="dashboard-tab-content-wrap">
                <div class="dashboard-content active" id="profile">
                    <form name="updateaccount" class="updateaccount" method="post" action="dashboard/process">
                        <ul class="left-list">
                            <li>
                                <div class="client-detail"> <span>Username</span> <span class="name"><?php echo ucfirst($users->first_name . ' ' . $users->last_name) ?></span> </div>
                            </li>
                            <li>
                                <label>First Name : </label>
                                <input placeholder="First Name" name="first_name" type="text" value="<?= ucfirst($users->first_name); ?>">
                            </li>
                            <li>
                                <label>Middle Name : </label>
                                <input placeholder="Mirst Name" name="middle_name" type="text" value="<?= ucfirst($users->middle_name); ?>">
                            </li>
                            <li>
                                <label>Last Name : </label>
                                <input placeholder="Last Name" name="last_name" type="text" value="<?= ucfirst($users->last_name); ?>">
                            </li>
                            <li>
                                <label>Phone : </label>
                                <input placeholder="Phone" name="phone" type="tel" value="<?= $users->phone; ?>">
                            </li>
                            <li>
                                <label>Mobile : </label>
                                <input placeholder="Phone" name="mobile" type="tel" value="<?= $users->mobile; ?>">
                            </li>
                            <li>
                                <label>Email : </label>
                                <input placeholder="Email" name="email" type="email" value="<?= $users->email; ?>">
                            </li>
                            <li>
                                <label>Post Code : </label>
                                <input placeholder="Post Code" name="postcode" type="text" value="<?= $users->postcode; ?>">
                            </li>
                            <li>
                                <label>Company: </label>
                                <input placeholder="Company" name="company" type="text" value="<?= $users->company; ?>">
                            </li>

                        </ul>
                        <ul class="right-list">


                            <li>
                                <div class="country_block1">
                                    <label>Country:</label> <input placeholder="country" name="country" type="text" value="<?= $users->country; ?>">
                                </div>
                            </li>
                            <li>
                                <label>Address : </label>
                                <input placeholder="Existing addres" type="text" name="address_line1" value="<?php echo $users->address_line1 ?>">
                            </li>



                            <li>
                                <label>Old password : </label>
                                <input placeholder="old password" type="password" name="oldPassword">
                            </li>
                            <li>
                                <label>New password : </label>
                                <input placeholder="New password" type="password" name="newPassword">
                            </li>
                            <li>
                                <label>Conforim password : </label>
                                <input placeholder="Confirm password" type="password" name="confPassword">
                            </li>
                        </ul>
                        <div class="clear"></div>
                        <div class="update-wrap"> 
                            <input type="submit" value="Update Account" id="" name="">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

