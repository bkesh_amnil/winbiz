<div class="container">
    <div class="register-user-wrap">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-lg-6">
                <div class="register-user">
                    <form action="users/registerProcess" method="post" class='user_form'>
                        <h2>Register</h2>
                        <div class="form-group">
                            <input class="form-control required" type="text" name="first_name" id="first-name" placeholder="Fullname" value="<?php if ($this->session->flashdata('first_name')) echo $this->session->flashdata('first_name') ?>" >
                            <label for="first-name"><i class="fa fa-user"></i></label>
                        </div>

                        <div class="form-group country_block">
                            <input type="text" name="country" placeholder="Country" class="form-control required" value="<?php if ($this->session->flashdata('country')) echo $this->session->flashdata('country') ?>">
                            <label><i class="fa fa-globe"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" placeholder="Mobile No." id="" class="form-control required" value="<?php if ($this->session->flashdata('mobile')) echo $this->session->flashdata('mobile') ?>">
                            <label><i class="fa fa-phone"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="address_line1" id="" placeholder="Address" class="form-control required" value="<?php if ($this->session->flashdata('address_line1')) echo $this->session->flashdata('address_line1') ?>">
                            <label><i class="fa fa-map-marker"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="company" placeholder="Company" class="form-control" value="<?php if ($this->session->flashdata('company')) echo $this->session->flashdata('company') ?>">
                            <label><i class="fa fa-building"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="E-mail" class="form-control required" value="<?php if ($this->session->flashdata('email')) echo $this->session->flashdata('email') ?>">
                            <label><i class="fa fa-envelope"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" class="form-control" id="password">
                            <label><i class="fa fa-unlock-alt"></i></label>
                        </div>
                        <div class="form-group">
                            <input type="password" name="passconf" placeholder="Confim Password" class="form-control" id="confirm-password">
                            <label><i class="fa fa-unlock-alt"></i></label>
                        </div>
                        <div class="captcha-wrap">
                            This is captcha
                        </div>
                        <a class="btn-resend-activation" href="#">
                            <i class="fa fa-refresh"></i>Re-Send activation email?
                        </a>
                        <input type="submit" name="submitUserLogin" value="Register">
                    </form>
                    <?php $this->load->view('common/flash_message') ?>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-4 col-lg-5 col-lg-offset-1">
                <img class="img-responsive" src="http://winbiz.dev/image/bigyapan/600/600/bigyapan_713_1449058077.jpg" alt=""/>
            </div>
        </div>
    </div>
</div>