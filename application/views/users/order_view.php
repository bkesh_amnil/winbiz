<div class="dashboard-tab-wrap">
    <div class="container">
        <ul class="dashboard-tab">
            <li class="active"><a data-id="#profile" href="javascript:void(0)">Order Detail</a></li>
        </ul>
    </div>
</div>

<div class="user-order-detail">
    <div class="container">

        <p> order Date:  <?php echo user_format($order->created_at) ?></p>
        <p> order Status:  <?php echo ($order->status) ?></p>

        <table class="description cart-decorations" cellpadding="" style="width:100%">
            <tr bgcolor="" class="cart-hdr">
                        <th> S.no</th>
                        <th> Product Name </th>
                        <th> Quantity </th>
                        <th> Unit Price </th>
                        <th> Amount </th>
                    </tr>
                    <?php
                    $i = 1;
                    $sub_total = 0;
                    foreach ($products as $res):
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><a href="<?php //echo base_url('product/' . $res->product_id . '/' . get_slug($res->product_name, '-'))  ?>" target="_blank"><?php echo $res->product_name ?></a></td>
                            <td><?php echo $res->qty ?></td>
                            <td><?php echo show_money($res->price) ?></td>
                            <td><?php
                                $total_amt = $res->qty * $res->price;
                                echo show_money($total_amt)
                                ?></td>        
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    <tr>
                        <td align="right" colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">subtotal:</td>
                        <td><?php echo show_money($order->order_sub_total_price) ?></td>
                    </tr>
                   
                    <tr>
                        <td align="right" colspan="4">Tax Applicable:</td>
                        <td><?php
                            $tax = $order->tax;
                            echo show_money($tax);
                            ?></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">Estimated Shipping Charge:<br>(<?php echo $order->shipping_method ?>) </td>
                        <td><?php
                            $ship = $order->shipping_cost;
                            echo show_money($ship)
                            ?></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">Grand Total: </td>
                        <td> <?php
                            $Gtotal = $order->order_total_price;
                            echo show_money($Gtotal);
                            ?></td>
                    </tr>
        </table>
        <div class="billing-shipping-address">
            <div class="tabs">
                <ul>
                    <li class="selected"><a href="javascript:void(0)" data-id="#billing-shipping">Billing & Shipping Information</a></li>
                    <li><a href="javascript:void(0)" data-id="#payment">Payment Method</a></li>
                </ul>
            </div>
            <div class="detail">
                <div class="billing-shipping selected" id="billing-shipping">
                    <h3>Billing Information</h3>
                    <ul class="left">
                        <li><span>First Name</span><?= $order->first_name . " " . " " . $order->middle_name . " " . $order->last_name; ?></li>
                        <li><span>Last Name</span>  <?= $order->email; ?></li>
                        <li class="email"><span>E-mail</span>  <?= $order->email; ?></li>
                        <li><span>Company</span> <?= $order->company; ?></li>
                        <li class="telephone"><span>Telephone</span>  <?= $order->phone; ?></li>
                    </ul>
                    <ul class="center">
                        <li><span class="address">Address</span><span class="address-name"> <?= $order->address; ?></span></li>
                       
                    </ul>
                    <ul class="right">
                        <li><span>Country</span><?= $order->country; ?>

                        </li>
                    </ul>



                    <h3>Shipping Information</h3>
                    <?php if ($order->same_address == 0) { ?>
                        <ul class="left">
                            <li><span>First Name</span><?php echo $order->s_first_name ?></li>
                            <li><span>Last Name</span>  <?php echo $order->s_last_name ?></li>
                            <li class="email"><span>E-mail</span> <?php echo $order->s_email ?></li>
                            <li><span>Company</span><?php echo $order->s_company ?></li>
                            <li class="telephone"><span>Telephone</span> <?php echo $order->s_phone ?></li>
                        </ul>
                        <ul class="center">
                            <li><span class="address">Address</span><span class="address-name"><?php echo $order->s_address ?></span></li>
                        </ul>
                        <ul class="right">
                            <li><span>Country</span> <?php echo $order->s_country ?></li>
                        </ul>

                    <?php } else {
                        ?>


                        <strong>Shipping Address: Ship To Billing Address</strong>
                        <br/>


                    <?php }
                    ?>
                </div>
                <div class="payment" id="payment">
                    <?php if ($order->payment_method == 'esewa') { ?>
                        <ul>
                            <li>1. esewa Transaction Number - <?php echo $order->txn_id ?></li>
                        </ul>
                    <?php } else if ($order->payment_method == 'paypal') {
                        ?>
                        <ul>
                            <li>1. Paypal Transaction Number - <?php echo $order->txn_id ?></li>
                        </ul>
                    <?php } else {
                        ?>
                        <ul>
                            <li>Cash on Delivery</li>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>