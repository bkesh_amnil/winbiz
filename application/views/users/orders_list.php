<div class="dashboard-content" id="orders">
    <?php
    $spent = 0;
    if (FALSE != $orders) {
        ?><ul class="orders-list"><?php
        if ($site_id != 1) {
            $site_orders = $orders[$site_id];
            $i = 1;
            if (!empty($site_orders)) {
                foreach ($site_orders as $value) {
                    ?>
                        <li>
                            <div class="order-list-wrap">
                                <!--<div class="image-preview"> <img src="front/images/belt.jpg"> </div>-->
                                <div class="preview-content"> 
                                    <span class="title-preview"><?php echo $i . ')  ' . user_format($value->created_at); ?></span> 
                                    <span>order status:<strong><?php echo $value->status ?></strong> </span><br/>



                                </div>
                            </div>
                            <div class="product-price"> <span><?php echo show_money($value->order_total_price); ?></span> <a href="dashboard/order/<?php echo $value->id ?>"><small>View Order Details</small></a> </div>
                        </li>
                        <?php
                        $spent+=$value->order_total_price;
                        $i++;
                    }
                }
            } else {
                foreach ($orders as $site => $order) {
                    echo '<strong>' . getWhere('site', array('id' => $site))->site_title . '</strong><br>';
                    $i = 1;
                    $per_site_spent = 0;
                    foreach ($order as $value) {
                        ?>
                        <li>
                            <div class="order-list-wrap">
                                <!--<div class="image-preview"> <img src="front/images/belt.jpg"> </div>-->
                                <div class="preview-content"> 
                                    <span class="title-preview"><?php echo $i . ')  ' . user_format($value->created_at); ?></span> 
                                    <span>order status:<strong><?php echo $value->status ?></strong> </span><br/>
                                </div>
                            </div>
                            <div class="product-price"> <span><?php echo show_money($value->order_total_price); ?></span> <a href="dashboard/order/<?php echo $value->id ?>"><small>View Order Details</small></a> </div>
                        </li>
                        <?php
                        $per_site_spent+=$value->order_total_price;
                        $spent+=$value->order_total_price;
                        $i++;
                        echo '<hr>';
                    }
                    echo 'Total spent in this site: ' . show_money($per_site_spent) . '<br><br>';
                    echo '<hr>';
                }
                ?>
                <?php
            }
            ?>
        </ul>

        <div class="clear"></div>
        <div class="total-spent-amount"> <span class="amount-title">Total amount spent <i class="fa fa-angle-right"></i></span> <span class="amont-total">
                <?php echo show_money($spent) ?>
            </span>
        </div>      
        <?php
    } else {
        echo "No item listed";
    }
    ?>
</div>