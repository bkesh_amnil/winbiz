<table width="100%" border="1"  cellpadding="0" bgcolor="#f9f9f9" bordercolor="#ccc">
    <tr>
        <td>
            <table width="100%" bgcolor="#f9f9f9" cellspacing="0" border="0" cellpadding="0" bordercolor="#ccc">
                <tr bgcolor="#f3f3f3">
                    <td height="35" colspan="5" align="left"  ><h3 style="color: #1E90FF;    font-size: 12px;  padding-top:5px;    padding-left: 20px; font-weight: bold;    line-height: 18px;  margin-bottom: 10px;">&raquo; Billing Address</h3></td>
                </tr>
                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td height="35"><strong>First Name: </strong></td>
                    <td height="35"><?php echo $order->first_name ?></td>

                    <td width="24%" height="35"><strong>&nbsp;Last Name:</strong></td>
                    <td width="25%" height="35"><?php echo $order->last_name ?></td>
                </tr>

                <tr>
                    <td width="2%">&nbsp;</td>
                    <td width="24%" height="35">&nbsp;<strong>Email Address:</strong></td>
                    <td width="25%" height="35"><?php echo $order->email ?></td>
                    <td>&nbsp;<strong> Company: </strong></td>
                    <td><?php echo $order->company ?></td>
                </tr>
                <tr>
                    <td width="2%">&nbsp;</td>
                    <td width="18%"><strong>&nbsp;Address: </strong></td>
                    <td width="31%"><?php echo $order->address ?></td>
                    <td width="18%">&nbsp;<strong>Phone:</strong></td>
                    <td width="31%" height="35"><?php echo $order->phone ?></td>
                </tr>
                <tr>
                    <td width="2%">&nbsp;</td>
                    <td width="18%">&nbsp;<strong>Country:</strong></td>
                    <td width="31%"><?php echo $order->country ?></td>
                    <td width="24%" height="35">&nbsp;<strong>Post Code:  </strong></td>
                    <td width="25%" height="35"><?php echo $order->postcode ?></td>
                </tr>

                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>
                <?php if ($order->same_address == 0) { ?>
                    <tr bgcolor="#f3f3f3">
                        <td height="35" colspan="5" align="left"  ><h3 style="color: #1E90FF;    font-size: 12px;  padding-top:5px;    padding-left: 20px; font-weight: bold;    line-height: 18px;  margin-bottom: 10px;">&raquo; Shipping Address</h3></td>
                    </tr>
                    <tr>
                        <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td height="35"><strong>First Name: </strong></td>
                        <td height="35"><?php echo $order->s_first_name ?></td>

                        <td width="24%" height="35"><strong>&nbsp;Last Name:</strong></td>
                        <td width="25%" height="35"><?php echo $order->s_last_name ?></td>
                    </tr>

                    <tr>
                        <td width="2%">&nbsp;</td>
                        <td width="24%" height="35">&nbsp;<strong>Email Address:</strong></td>
                        <td width="25%" height="35"><?php echo $order->s_email ?></td>
                        <td>&nbsp;<strong> Company: </strong></td>
                        <td><?php echo $order->s_company ?></td>
                    </tr>
                    <tr>
                        <td width="2%">&nbsp;</td>
                        <td width="18%"><strong>&nbsp;Address: </strong></td>
                        <td width="31%"><?php echo $order->s_address_line1 ?></td>
                        <td width="18%">&nbsp;<strong>Phone:</strong></td>
                        <td width="31%" height="35"><?php echo $order->s_phone ?></td>
                    </tr>
                    <tr>
                        <td width="2%">&nbsp;</td>
                        <td width="18%">&nbsp;<strong>Country:</strong></td>
                        <td width="31%"><?php echo $order->s_country ?></td>
                        <td width="24%" height="35">&nbsp;<strong>Post Code:  </strong></td>
                        <td width="25%" height="35"><?php echo $order->s_postcode ?></td>
                    </tr>
                    <tr>
                        <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                    </tr>
                <?php } else {
                    ?>
                    <tr>
                        <td width="2%">&nbsp;</td>
                        <td width="18%">&nbsp;<strong>Shipping Address: Ship To Billing Address</strong></td>
                        <td width="31%"><?php echo $order->s_country ?></td>
                        <td width="24%" height="35">&nbsp;<strong> </strong></td>
                        <td width="25%" height="35"> </td>
                    </tr>
                <?php }
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td height="35"><strong>IP Address: </strong></td>
                    <td height="35" colspan="3"><?php echo $order->ip_address ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td height="35"><strong>Comment: </strong></td>
                    <td height="35" colspan="3"><?php echo $order->comments ?></td>
                </tr>
                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>
                <tr bgcolor="#f3f3f3">
                    <td height="35" colspan="5" align="left" ><h3 style="color: #1E90FF; font-size: 12px; padding-top:5px;    padding-left: 20px; font-weight: bold;    line-height: 18px;  margin-bottom: 10px; font-weight: bold;    line-height: 18px;  margin-bottom: 10px;">&raquo; Payment Method</h3></td>
                </tr>
                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>
                <?php $this->load->view('payment/' . $type) ?>
                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>
                <tr bgcolor="#f3f3f3">
                    <td height="35" colspan="5" align="left" ><h3 style="color: #1E90FF;    font-size: 12px;   padding-top:5px;  padding-left: 20px; font-weight: bold;    line-height: 18px;  margin-bottom: 10px;">&raquo; Item Details</h3></td>

                </tr>
                <tr>
                    <td height="1" colspan="5" align="left" bgcolor="#c6c6c6" > </td>
                </tr>

            </table>
        </td>
    </tr>
</table>';