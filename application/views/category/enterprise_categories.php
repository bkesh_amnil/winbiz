<div class="navbar-header">
    <button class="navbar-toggle category-collaps" data-target="#category-collapse" data-toggle="collapse" type="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<h2 class="aside-header">Categories</h2>
<div id="category-collapse" class="collapse navbar-collapse">
    <?php if(isset($enterprise_categories) && !empty($enterprise_categories)) { ?>
    <div class="category-list">
    <ul class="category-list-wrap nav navbar-nav">
        <?php 
        foreach($enterprise_categories as $ind => $enterprise_category) { 
            if(!empty($enterprise_category)) {
            $count = count($enterprise_category);
                        if($count>1){
                            $class='';
                        }else{
                            $class='single-list';
                        }
            ?>
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <?php echo $ind ?>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu <?php echo $class?>" role="menu">
                    <li>
                        <h2> <?php echo $ind ?> </h2>
                        <div class="listings">
                            <?php /*foreach($enterprise_category as $ent_cat){ ?><a href="<?php echo base_url('cat/'.$ent_cat->enterprise_category_alias.'/'.$ent_cat->enterprise_alias) ?>"><?php echo ucfirst($ent_cat->enterprise_name) ?></a><?php }*/ ?>
                            <?php foreach($enterprise_category as $ent_cat){ $url = site_url('cat/'.$ent_cat->entcatId.'/'.$ent_cat->entId.'/'.$ent_cat->enterprise_alias); ?><a href="<?php echo $url ?>"><?php echo ucfirst($ent_cat->enterprise_name) ?></a><?php } ?>
                        </div>
                    </li>
                </ul>
            </li>
            <?php
            } else {
            ?>
            <li>
                <a href="javascript:void(0);"><?php echo $ind; ?></a>
            </li>
            <?php
            }
        }
        ?>
    </ul>
    </div>
    <?php } ?>
</div>