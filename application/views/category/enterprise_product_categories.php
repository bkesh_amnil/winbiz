<div class="navbar-header">
    <button class="navbar-toggle category-collaps" data-target="#category-collapse" data-toggle="collapse"
            type="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<h2 class="aside-header">Categories </h2>
<div id="category-collapse" class="collapse navbar-collapse">
    <?php if (isset($enterprise_product_category) && !empty($enterprise_product_category)) { ?>
    <div class="category-list">
        <ul class="category-list-wrap nav navbar-nav">
            <?php foreach ($enterprise_product_category as $ind => $enterprise_category) {
                if (!empty($enterprise_category->product_category)) { ?>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            <?php echo $ind ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <h2> <?php echo $ind ?> </h2>

                                <div class="listings">
                                    <?php /*foreach($enterprise_category as $ent_cat){ ?><a href="<?php echo base_url('cat/'.$ent_cat->enterprise_category_alias.'/'.$ent_cat->enterprise_alias) ?>"><?php echo ucfirst($ent_cat->enterprise_name) ?></a><?php }*/ ?>
                                    <?php foreach ($enterprise_category->product_category as $ent_cat) {
                                        $url = site_url('cat/' . $ent_cat->id . '/' . $ent_cat->product_category_alias ); ?>
                                        <a
                                        href="<?php echo $url ?>"><?php echo ucfirst($ent_cat->product_category_name) ?></a><?php } ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php } else { 
                $url = site_url('cat/' . $enterprise_category->id  . '/' . $enterprise_category->product_category_alias);?>
                    <li id="product_category_<?php echo $enterprise_category->id ?>" class="">
                        <a href="<?php echo $url ?>"
                           class="product-category_id"
                           rel="<?php echo $enterprise_category->id ?>"><?php echo $enterprise_category->product_category_name; ?></a>
                    </li>
                <?php } ?>

            <?php } ?>
        </ul>
        </div>
    <?php } ?>
</div>