<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-9">
                    banners
                    <?php $this->load->view('bigyapan/banner', $banners) ?>
                </div>
                <div class="col-xs-fixing clearfix">
                            top right
                    <?php if(!empty($top_right_bigyapan)) { ?>
                        <div class="col-sm-3 col-xs-12">
                            <?php $this->load->view('bigyapan/top_bigyapan', $top_right_bigyapan); ?>
                        </div>
                    <?php } ?>
                    
                </div>
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-12">
                        featured  
                      <?php $this->load->view('bigyapan/center_bigyapan', $center_bigyapans); ?>
                    </div>
                    <?php if(!empty($center_right_bigyapan)) { ?>
                    <div class="col-sm-8">
                        middle left
                      <?php $this->load->view('bigyapan/left_bigyapan', $center_right_bigyapan); ?>
                    </div>
                    <?php } ?>
                    <?php if(!empty($bottom_right_bigyapan)) { ?>
                    <div class="col-sm-4">
                        middle right
                      <?php $this->load->view('bigyapan/bottom_bigyapan', $bottom_right_bigyapan); ?>
                    </div>
                    <?php } ?>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="best-selling-section">
    top viewed
    <?php $this->load->view('products/best_selling_products', $best_selling_products); ?>
</section>