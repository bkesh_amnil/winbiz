
<?php foreach ($forms['user_registration'] as $form_detail): ?>	

    <?php if (($form_detail->field_type == "input") || ($form_detail->field_type == "password")) { ?>                
        <label><?php echo $form_detail->field_label; ?></label>
        <input name="<?php echo $form_detail->field_name; ?>"  value="<?php echo set_value($form_detail->field_name); ?>" class="form-control"  type="<?php echo $form_detail->field_type; ?>"> <br/>
        <?php
    } else if ($form_detail->field_type == "radio") {
        echo '<label>' . $form_detail->field_label . '</label>';
        $radio_options = explode(',', $form_detail->display_text);
        echo '</br>';
        foreach ($radio_options as $option_value):
            ?>
            <input type="radio" name="<?php echo $form_detail->field_name; ?>" value="<?php echo $option_value; ?>" <?php echo set_radio($form_detail->field_name, $option_value, TRUE); ?>><label><?php echo $option_value; ?></label>

        <?php endforeach;
        echo '</br>';
        ?>

        <?php
        echo '</br>';
    };
endforeach;
?>