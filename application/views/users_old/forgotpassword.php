<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                <li>/</li>
                                                <li><a href="#">Kathmandu</a></li>
                                                <li>/</li>					
                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="">Forgot Password<li>
                        <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>
<div class="container"> 
    <div class="row">
        <div class="col-md-6">	
            <div id="msg-privacy"></div>

            <?php
            if (isset($error)) {
                echo "<div class='alert  alert-danger'>" . $error . "</div>";
            };
            ?>
            <?php
            if (isset($success)) {
                echo "<div class='alert  alert-success'>" . $success . "</div>";
            }
            ?>

            <form id="password-form" action="" method="Post">
                <label>User Name </label>
                <input name="username" value="<?php echo set_value('username'); ?>" class="form-control" type="text"> <br>
                <label>Email </label>
                <input name="email" value="<?php echo set_value('email'); ?>" class="form-control" type="email"> <br>
                <input type="submit" value='submit' class="btn btn-primary"/>
            </form>
        </div>
    </div>
</div>
