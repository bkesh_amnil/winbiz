
<link href="<?php echo base_url(); ?>administrator/assets/css/chosen.css" rel="stylesheet" />

<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                <li>/</li>
                                                <li><a href="#">Kathmandu</a></li>
                                                <li>/</li>					
                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="">User Dashboard</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>




<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-8 pagecontainer2 offset-0">
            <div class="padding30 grey">  <div class="tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active"><a data-toggle="tab" role="tab" href="#profile">Edit profile</a></li>

                        <?php if ($user_info->profile_id == 4): ?>
                            <li class=""><a data-toggle="tab" role="tab" href="#add-user">Add User</a></li>
                            <li class=""><a data-toggle="tab" role="tab" href="#package">Request for Packages</a></li>

                            <li class=""><a data-toggle="tab" role="tab" href="#edit-product">Add/Edit Product</a></li>
                            <li class=""><a data-toggle="tab" role="tab" href="#edit-category">Add/Edit Category</a></li>
                            <li class=""><a data-toggle="tab" role="tab" href="#edit-shop">Edit Shop</a></li>
                        <?php endif; ?>

                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div id="profile" class="tab-pane fade active in">

                            <div>
                                MY Profile <br/> 

                                UserName : <?php echo $user_info->user_name; ?> <br/>
                                First Name:<?php echo $user_info->first_name; ?> <br/>
                                Last Name:<?php echo $user_info->last_name; ?> <br/>
                                Email :<?php echo $user_info->email; ?> <br/>
                                Gender:<?php echo $user_info->gender; ?> <br/>
                                Mobile:<?php echo $user_info->mobile_num; ?> <br/>
                                Address:<?php echo $user_info->address; ?> <br/>


                                <div id="editprofile"></div>
                                <?php //var_dump($user_info);?>
                                <br/>
                                <button class="edit-profile">Edit Profile</button> 
                            </div>

                        </div>


                        <div id="add-user" class="tab-pane fade">

                            New User Add form.
                            <div id="user-add-msg"></div>
                            <form action="shop" method="post" id="add-new-user">
                                <input type="hidden" name="shop_id" value="<?php echo $shop_info->id; ?>">
                                <input type="hidden" name="created_by" value="<?php echo $user_info->id; ?>">
                                <?php echo $this->load->view('users/user_registration'); ?>
                                <input type="submit" value="Register" class="bluebtn margtop20"/>
                            </form>





                        </div>
                        <div id="package" class="tab-pane fade">

                            <div id ="req-package" >
                                <h3> Package Info.</h3> <br/>
                                Your shop is currently packaged under </br>
                                <div>
                                    <?php
                                    foreach ($shop_package_list as $key => $value):
                                        if ($value->id == $shop_info->package_id)
                                            echo '<h4>' . $value->package_name . '</h4>';
                                        ?>						
                                    <?php endforeach; ?>


                                    <?php //var_dump( $shop_package_list);  ?>
                                    <br/></br>

                                    Do you want to request following package.
                                    <div id='package_req'>
                                        <?php
                                        foreach ($shop_package_list as $key => $value):
                                            if ($value->id > $shop_info->package_id) {
                                                ?>							
                                                <input type="radio" name="shop-register-package" value="<?php echo $value->id; ?>" >	
                                                <label><?php echo $value->package_name; ?></label>

                                            <?php } endforeach; ?>

                                    </div>
                                    <a href='' id="package-choose"> YES </a>

                                </div>

                            </div>





                        </div>



                        <div id="edit-product" class="tab-pane fade">

                            <script>
                                $.get("<?php echo base_url('product/product_list/' . $shop_info->id) ?>", function(data) {
                                    $("#edit-product").html(data);
                                });
                            </script>


                        </div>

                        <div id="edit-category" class="tab-pane fade">
                            Add/Edit Category.

                            <script>

                                $.get("<?php echo base_url('shop_category/product_category_list/' . $shop_info->id) ?>", function(data) {
                                    $("#edit-category").html(data);
                                });
                            </script>

                        </div>

                        <div id="edit-shop" class="tab-pane fade">                                
                            <?php echo $this->load->view('shop/shop_edit', $shop_detail_info); ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    $(document).on('click', '.edit-profile', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: '<?php echo base_url("users/profile_edit") ?>',
            dataType: 'json',
            success: function(data) {

                if (data.success)
                {
                    // alert(data.editHtml);
                    $('#editprofile').html(data.editHtml);

                }
                else
                {
                    alert(data.error);
                }
            },
            error: function() {
                alert('error occured');
            }
        })
    });


    $(document).on('click', '.edit-profile-submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        // element = $();
        var formData = $('#edit-profile-form').serialize();

        $.ajax({
            url: '<?php echo base_url('users/profile_edit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                $('#editprofile').html(data.editHtml);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

    //add-new-user

    $('#add-new-user').on('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        // element = $();
        var formData = $('#add-new-user').serialize();


        $.ajax({
            url: '<?php echo base_url('users/register'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                $('#user-add-msg').html(data.msg);
                $('#user-add-msg').append(data.error);



            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });


    function toggleDisplay($cssid)
    {
        $('#' + $cssid).toggle();
    }


    $('#package-choose').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var package_id = $('#package_req').find(':input:checked').val();
        var shop_id = <?php echo (isset($shop_info->id)) ? $shop_info->id : '0'; ?>

        $.ajax({
            url: '<?php echo base_url("users/package_req") ?>',
            data: {'shop_id': shop_id, 'package_id': package_id},
            type: 'POST',
            dataType: 'json',
            success: function(data) {

                if (data.success)
                {
                    // alert(data.editHtml);
                    $('#package-choose').html(data.msg);

                }
                else
                {
                    $('#package-choose').append(data.msg);
                }
            },
            error: function() {
                alert('error occured');
            }
        });


    });

</script>