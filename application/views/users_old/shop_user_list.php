<table class="table table-bordered">
    <?php if (!empty($users) && is_array($users)) { ?>

        <tr><th> First Name </th> <th>Last Name</th> <th> Email </th><th>Mobile No.</th> <th> Status</th> 
            <?php
            foreach ($users as $user):
                ?>
            <tr>
                <td> <?php echo $user->first_name; ?> </td>
                <td><?php echo $user->last_name; ?> </td>
                <td><?php echo $user->email; ?> </td>
                <td><?php echo $user->mobile_num; ?> </td>
                <td>
                    <?php if ($user->status == 'Active') { ?>
                        <button class="btn btn-danger change-status" data-id="<?php echo $user->id; ?>" data-status="2"  > Active </button>
                    <?php } else { ?>
                        <button class="btn btn-success change-status" data-id="<?php echo $user->id; ?>" data-status="1"  > Inactive </button>
                    <?php } ?>
            </tr>

            <?php
        endforeach;
    } else {
        echo "No other user assigned to your shop.";
    }
    ?>
</table>


<script>
    $(document).on('click', '.change-status', function(e) {
        e.preventDefault();
        e.stopPropagation();

        user_id = $(this).attr('data-id');
        status = $(this).attr('data-status');

        element = $(this);
//        alert("this is the change click");
//        return false;

        $.ajax({
            url: '<?php echo base_url("users/change_status") ?>',
            data: {'user_id': user_id, 'status': status},
            type: 'POST',
            dataType: 'json',
            success: function(data) {

                if (data.success)
                {                    
                    if (status == 1) {
                        element.html('Active').removeClass('btn-success').addClass('btn-danger').attr('data-status',2);
                        
                    }else{
                        element.html('Inactive').removeClass('btn-danger').addClass('btn-success').attr('data-status',1);
                    }
                }
                else
                {
                    alert("User has not yet confirmed the email activation.")
                }
            },
            error: function() {
                 alert('error occured');
            }
        });



    });



</script>