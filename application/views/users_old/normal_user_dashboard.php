<link href="<?php echo base_url(); ?>css/chosen.css" rel="stylesheet" />
<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="">User Dashboard</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>

<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-12 pagecontainer2 offset-0">
            <div class="col-md-1 offset-0">
                <ul class="nav profile-tabs" role="tablist"  id="myTab">
                    <li class="active"><a data-toggle="tab" role="tab" href="#profile"><span class="profile-icon"></span>My Profile</a></li>
                    <li class=""><a data-toggle="tab" role="tab" href="#req-shop"><span class="shop-icon"></span>Request for Shop</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- END OF FILTERS --> 
            <!-- LIST CONTENT-->
            <div class="col-md-11 offset-0"> 
                <!-- Here Goes shop list -->
                <div class="tab-content5" id="myTabContent">
                    <div id="profile" class="tab-pane padding20 fade active in">
                        <div class="relative">
                            <?php if ($user_info->profile_id == 4): ?>
                                <div class="alert alert-warning"> Even though you have been register as shop user.<br/>
                                    until and unless your shop is registered, you are a normal user.<br/>
                                    Please do keep waiting for our call for your shop confirmation. </div>
                            <?php endif; ?>
                            <ul class="nav nav-tabs myTab2pos" role="tablist">
                                <li class="active"><a href="#profile-info" role="tab" data-toggle="tab">Profile Info</a></li>
                                <li><a href="#edit-profile" role="tab" data-toggle="tab">Edit Profile</a></li>
                                <li><a href="#privacy" role="tab" data-toggle="tab">Privacy</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content6">
                                <div class="tab-pane active" id="profile-info"> <span class="size18">My Profile</span>
                                    <ul class="settingsList">
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> UserName :</h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->user_name; ?></span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> First Name:</h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->first_name; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Last Name: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->last_name; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Email : </h3>
                                            <span class="settingsListContent col-md-4"> <?php echo $user_info->email; ?> </span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Gender: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->gender; ?> </span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Mobile: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->mobile_num; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Address: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->address; ?></span> </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="tab-pane" id="edit-profile">
                                    <div id="msg-profile"></div>
                                    <div id="edit-profile-form-div"></div>
                                    <script>
                                        $.get("<?php echo base_url('users/profile_edit') ?>", function(data) {
                                            $("#edit-profile-form-div").html(data);
                                        });
                                    </script> 
                                </div>
                                <div class="tab-pane" id="privacy">
                                    <div id="msg-privacy"></div>
                                    <form id="password-form" method="POST">
                                        New Password
                                        <input name="password" value="" class="form-control" type="password">
                                        <br>
                                        Confirm Password
                                        <input name="re-password" value="" class="form-control" type="password">
                                        <br>
                                        <input type="submit" value='submit' id ="privacy-form-submit" class="bluebtn"/>
                                    </form>
                                </div>
                            </div>
                            <div id="editprofile"></div>
                            <?php //var_dump($user_info);?>
                            <br/>
                        </div>
                    </div>
                    <div id="req-shop" class="tab-pane fade padding20">
                        <h3 class="settingsContentTitle">Request for the shop</h3>
                        <div class="line4"></div>
                        <div class="clearfix"></div>
                        <div id="error-req"></div>
                        <form method="POST" action="" id="shop-register-request-form">
                            <div id="shop-registration-form">
                                <?php foreach ($shop_registration_form as $form_detail): ?>
                                    <div class="control-group"> <?php echo $form_detail->field_label; ?>
                                        <div class="controls">
                                            <input name="<?php echo $form_detail->field_name; ?>" value="<?php echo set_value($form_detail->field_name); ?>" class="form-control" type="<?php echo $form_detail->field_type; ?>">
                                        </div>
                                    </div>
                                    <br/>
                                <?php endforeach; ?>

                                Choose one of the packages. 
                                <br/>


                                <?php foreach ($shop_package_list as $packagelist): ?>
                                    <div class="radio">
                                        </label>
                                        <input type="radio" name="shop-register-package" value="<?php echo $packagelist->id; ?>" <?php echo set_radio('shop-register-package', $packagelist->id, TRUE); ?>>
                                        <?php echo $packagelist->package_name; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                                <div class="control-group "> Category : *
                                    <div class="controls">






                                        <input id="autocomplete-multiple" type="text" data-id="<?php //echo $selected_multiple_categories_id;       ?>" value="<?php //echo $selected_multiple_categories_name;                  ?>" name="CategoryID" class="form-control" />





                                        <?php /*
                                          <select name="CategoryID[]" class="testcls" rel="testclsrel" multiple="multiple">
                                          <?php
                                          foreach ($categoryList as $row) {
                                          $selected = '';
                                          if (in_array($row['category_id'], $selected_multiple_categories))
                                          $selected = 'selected="selected"';
                                          ?>
                                          <option value="<?php echo $row['category_id'] ?>" <?php echo $selected ?>>
                                          <?php
                                          echo $row['category_name'];
                                          echo (!empty($row['ParentName']) ? '(' . $row['ParentName'] . ')' : '')
                                          ?>
                                          </option>
                                          <?php } ?>
                                          </select>
                                         */ ?>

                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls"> <br/>
                                        <input type="submit" value="Submit" id="shop-register-request" class="bluebtn"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End of offset1-->
            <div class="clearfix"></div>
        </div>
        <!-- END OF LIST CONTENT--> 
    </div>
</div>
<script>

    $(document).on('click', '#shop-register-request', function(e) {
        e.preventDefault();
        e.stopPropagation();

        // element = $();
        formData = new FormData($('#shop-register-request-form')[0]);
        var categoryArray = [];

        $(".ui-icon-close").each(function (index) {
            categoryArray[index] = $(this).attr('data-id')
            //console.log(index + ": " + $(this).attr('data-id'));
        });

        formData.append('CategoryID', categoryArray.join(','));


        $.ajax({
            url: '<?php echo base_url('users/shop_request_submit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR)
            {
                if (data.success) {
                    $('#error-req').html(data.msg).removeClass().addClass('alert alert-success');
                    $('#shop-register-request-form')[0].reset();
                }
                else {
                    $('#error-req').html(data.msg).removeClass().addClass('alert alert-danger');
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

    $(document).on('click', '.edit-profile', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: '<?php echo base_url("users/profile_edit") ?>',
            dataType: 'json',
            type: 'POST',
            success: function (data) {

                if (data.success)
                {
                    // alert(data.editHtml);
                    $('#editprofile').html(data.editHtml);

                }
                else
                {
                    alert(data.error);
                }
            },
            error: function() {
                alert('error occured');
            }
        })
    });


    $(document).on('submit', '#edit-profile-form', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = $('#edit-profile-form').serialize();

        $.ajax({
            url: '<?php echo base_url('users/profile_edit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                if (data.error) {
                    $('#msg-profile').html(data.error).removeClass().addClass('alert alert-danger');
                }
                if (data.msg) {
                    $('#msg-profile').html(data.msg).removeClass().addClass('alert alert-success');
                    window.location.href = "<?php echo base_url('users/dashboard'); ?>";
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

    $(document).on('submit', '#password-form', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var formData = $(this).serialize();
        $.ajax({
            url: '<?php echo base_url('users/password_edit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                if (data.error) {
                    $('#msg-privacy').html(data.error).removeClass().addClass('alert alert-danger');
                }
                if (data.msg) {
                    $('#msg-privacy').html(data.msg).removeClass().addClass('alert alert-success');
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

</script> 
<script type="text/javascript" src="<?php echo base_url('administrator/assets/scripts/chosen.jquery.js') ?>"></script> 
<script type="text/javascript">
    $('.testcls').chosen({width: "100%"});
</script> 
