
<link href="<?php echo base_url(); ?>administrator/assets/css/chosen.css" rel="stylesheet" />
<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                <li>/</li>
                                                <li><a href="#">Kathmandu</a></li>
                                                <li>/</li>					
                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="#">User Register</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>


<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-8 pagecontainer2 offset-0">
            <div class="padding30 grey"> <span class="size16 bold">Registration Form</span>
                <div class="line2"></div>			

                <div id="msg"></div>
                <div id="error"></div>
                <?php if (validation_errors() || ((isset($errors)) && !(empty($errors)))) { ?>
                    <div class="alert  alert-danger"> 
                        <?php
                        echo validation_errors();
                        if (isset($errors)) {
                            foreach ($errors as $error) {
                                echo $error . '<br/>';
                            };
                        };
                        ?>
                    </div>
                    <?php
                } elseif (isset($success_msg)) {
                    echo '<div class="alert alert-success">' . $success_msg . '</div>';
                    ?>

                    <script>

                        window.onload = function () {

                            $("#registration-form")[0].reset();
                            $("#registration-form").hide();
                        }
                    </script>

                    <?php
                }
                ?>
                <?php if (!(isset($success_msg))): ?>


                    <form id="registration-form" action="<?php //echo base_url('uses/register');                ?>" method="post">                        


                        <?php echo $this->load->view('users/user_registration', array('forms' => $forms)); ?>

                        <h4> Do you want to register Shop? </h4>			
                        <div id="shop-registration-choose">	
                            <input type="radio" name="shop-register" value="1" <?php echo set_radio('shop-register', '1', TRUE); ?> ><label>Yes</label>
                            <input type="radio" name="shop-register" value="0" <?php echo set_radio('shop-register', '0', TRUE); ?>><label>No</label>

                        </div>
                        <br/>

                        <div id="shop-registration-form" style="display: none;">
                            <?php
                            foreach ($forms['shop_registration'] as $form_detail):
                                if ($form_detail->field_name != 'address'):
                                    ?>					
                                    <div class="control-group">
                                        <label class="control-label"><?php echo $form_detail->field_label; ?></label>
                                        <div class="controls"> <input name="<?php echo $form_detail->field_name; ?>" value="<?php echo set_value($form_detail->field_name); ?>" class="form-control" type="<?php echo $form_detail->field_type; ?>"> </div>
                                    </div><br/>
                                    <?php
                                endif;
                            endforeach;
                            ?>

                            <label>Choose one of the packages. </label><br/>
                            <?php foreach ($packages as $packagelist): ?>
                                <input type="radio" name="shop-register-package" value="<?php echo $packagelist->id; ?>" <?php echo set_radio('shop-register-package', $packagelist->id, TRUE); ?>><label><?php echo $packagelist->package_name; ?></label>
                            <?php endforeach; ?>




                            <div class="control-group ">
                                <label class="control-label">Category : *</label>                                                        
                                <?php
                                if (isset($selected_multiple_categories)) {
                                    $categoryInfo = $selected_multiple_categories;
                                    foreach ($categoryInfo as $category) {
                                        ?>
                                        <div class="ui-autocomplete-multiselect-item"><?php echo $category->category_name; ?><span class="ui-icon ui-icon-close" data-id="<?php echo $category->category_id; ?>"></span></div>
                                        <?php
                                    }
                                };
                                ?>


                                <input id="autocomplete-multiple" type="text" data-id="<?php //echo $selected_multiple_categories_id;    ?>" value="<?php //echo $selected_multiple_categories_name;               ?>" name="Category" class="form-control" />


                                <!--                                <div class="controls">
                                                                    <select name="CategoryID[]" class="testcls" rel="testclsrel" multiple="multiple">
                                <?php /*
                                  foreach ($categoryList as $row) {
                                  $selected = '';
                                  if (in_array($row['category_id'], $selected_multiple_categories))
                                  $selected = 'selected="selected"';
                                  ?>
                                  <option value="<?php echo $row['category_id'] ?>" <?php echo $selected ?>><?php
                                  echo $row['category_name'];
                                  echo (!empty($row['ParentName']) ? '(' . $row['ParentName'] . ')' : '')
                                  ?></option>
                                  <?php } */ ?>
                                                                    </select>
                                                                </div>-->



                            </div>
                        </div>

                        <input type="submit" value="Register" class="bluebtn margtop20"/>
                    </form>


                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#registration-form').on('submit', function (e) {
        e.preventDefault();
        formData = new FormData($('#registration-form')[0]);

        //var eachSpanCategory = ($('.ui-icon-close'));
        var categoryArray = [];

        $(".ui-icon-close").each(function (index) {
            categoryArray[index] = $(this).attr('data-id')
            //console.log(index + ": " + $(this).attr('data-id'));
        });

        formData.append('CategoryID[]', categoryArray.join(','));

        $.ajax({
            data: formData,
            dataType: 'json',
            type: 'POST',
            url: '<?php echo base_url('users/register'); ?>',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR)
            {

                if (data.error) {

                    $("#error").html(data.error).removeClass().addClass('alert alert-danger');
                    $("#msg").html('').removeClass();
                    //window.location.href = "<?php //echo base_url();                                                                                     ?>";

                } else if (data.msg)
                {
                    $("#error").html('').removeClass();
                    $("#msg").html(data.msg).removeClass().addClass('alert alert-success');
                    $("#registration-form")[0].reset();
                    $("#registration-form").hide();

                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }

        })

    })

</script>





