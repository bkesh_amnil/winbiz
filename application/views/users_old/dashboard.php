
<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                <li>/</li>
                                                <li><a href="#">Kathmandu</a></li>
                                                <li>/</li>					
                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="http://192.168.25.25:81/justcallnepal/">Home</a></li>
                <li>/</li>
                <li><a href="http://192.168.25.25:81/justcallnepal/cat/1/14.html">Apparels</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>


<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-12 pagecontainer2 offset-0">
            <div class="padding30 grey">


                <a class="btn" href="">Edit profile</a>
                <a class="btn" onclick="return toggleDisplay('shop-info');" href="#">Edit Shop Info</a>
                <a class="btn" onclick="return toggleDisplay('add-user');" href="#">Add User</a>
                <a class="btn" href="">Add Products</a>
				<a class="btn" onclick="return toggleDisplay('req-package');">Req for Package</a>

                <div id="profile-edit">               

                    MY Profile <br/> 

                    UserName : <?php echo $user_info->user_name;?> <br/>
                    First Name:<?php echo $user_info->first_name;?> <br/>
                    Last Name:<?php echo $user_info->last_name;?> <br/>
                    Email :<?php echo $user_info->email;?> <br/>
                    Gender:<?php echo $user_info->gender;?> <br/>
                    Mobile:<?php echo $user_info->mobile_num;?> <br/>
                    Address:<?php echo $user_info->address;?> <br/>


                    <div id="editprofile"></div>
					<?php //var_dump($user_info);?>
                    <br/>
                    <button class="edit-profile">Edit Profile</button>


                    <!--                    <form action="" method="post">
					<?php //echo $this->load->view('users/profile_change', $user_info_data); ?>
                                            <input type="submit" value="Edit" class="bluebtn margtop20"/>
                                        </form>-->

                </div>



				<div  style="display:none" id="shop-info">
					Change Shop Detail
					<?php var_dump($shop_info);?>
				</div>



				<div id="add-user" style="display:none">
					New User Add form.
					<div id="user-add-msg"></div>
					<form action="shop" method="post" id="add-new-user">
						<input type="hidden" name="shop_id" value="<?php echo $shop_info->id;?>">
						<input type="hidden" name="created_by" value="<?php echo $user_info->id;?>">
						<?php echo $this->load->view('users/user_registration');?>
						<input type="submit" value="Register" class="bluebtn margtop20"/>
					</form>

				</div>

				<div>
					Add Products And Deletes
				</div>

				<div id ="req-package" style="display:none">
					Package Info.
					Your shop is currently packaged under ..
					<div>
						<?php
						foreach($shop_package_list as $key=> $value):
							if($value->id==$shop_info->package_id) echo $value->package_name;
							?>						
						<?php endforeach;?>


						<?php //var_dump( $shop_package_list);?>
						Do you want to req for
						<div id='package_req'>
							<?php
							foreach($shop_package_list as $key=> $value):
								if($value->id>$shop_info->package_id){
									?>							
									<input type="radio" name="shop-register-package" value="<?php echo $value->id;?>" >	
									<label><?php echo $value->package_name;?></label>

								<?php } endforeach;?>

						</div>
						<a href='' id="package-choose"> YES </a>

					</div>

				</div>
			</div>
		</div>
	</div>


	<script>




					$(document).on('click', '.edit-profile', function(e) {
						e.preventDefault();
						e.stopPropagation();

						$.ajax({
							url: '<?php echo base_url("users/profile_edit")?>',
							dataType: 'json',
							success: function(data) {

								if (data.success)
								{
									// alert(data.editHtml);
									$('#editprofile').html(data.editHtml);

								}
								else
								{
									alert(data.error);
								}
							},
							error: function() {
								alert('error occured');
							}
						})
					});


					$(document).on('click', '.edit-profile-submit', function(e) {
						e.preventDefault();
						e.stopPropagation();
						// element = $();
						var formData = $('#edit-profile-form').serialize();

						$.ajax({
							url: '<?php echo base_url('users/profile_edit');?>',
							type: 'POST',
							data: formData,
							dataType: 'json',
							success: function(data, textStatus, jqXHR)
							{
								$('#editprofile').html(data.editHtml);
							},
							error: function(jqXHR, textStatus, errorThrown)
							{
								// Handle errors here
								console.log(textStatus);
								// STOP LOADING SPINNER
							}
						});
					});

					//add-new-user

					$('#add-new-user').on('submit', function(e) {
						e.preventDefault();
						e.stopPropagation();
						// element = $();
						var formData = $('#add-new-user').serialize();


						$.ajax({
							url: '<?php echo base_url('users/register');?>',
							type: 'POST',
							data: formData,
							dataType: 'json',
							success: function(data, textStatus, jqXHR)
							{
								$('#user-add-msg').html(data.msg);
								$('#user-add-msg').append(data.error);



							},
							error: function(jqXHR, textStatus, errorThrown)
							{
								// Handle errors here
								console.log(textStatus);
								// STOP LOADING SPINNER
							}
						});
					});


					function toggleDisplay($cssid)
					{
						$('#' + $cssid).toggle();
					}

					$('#package-choose').on('click', function(e) {
						e.preventDefault();
						e.stopPropagation();

						var package_id = $('#package_req').find(':input:checked').val();
						var shop_id = <?php echo ($shop_info->id)?$shop_info->id:'0';?>

						$.ajax({
							url: '<?php echo base_url("users/package_req")?>',
							data: {'shop_id': shop_id, 'package_id': package_id},
							type: 'POST',
							dataType: 'json',
							success: function(data) {

								if (data.success)
								{
									// alert(data.editHtml);
									$('#package-choose').html(data.msg);

								}
								else
								{
									$('#package-choose').append(data.msg);
								}
							},
							error: function() {
								alert('error occured');
							}
						});


					});

	</script>

