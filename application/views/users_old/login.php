<div class="container"> 
    <div class="row">
        <div class="col-md-12">			
            <h1>Login</h1>
            <div id="error">
                <?php
                echo validation_errors();
                if (isset($error)) {
                    foreach ($error as $errors) {
                        echo $errors . "<br/>";
                    }
                }
                ?>
            </div>
            <form id ="login-form" action="<?php // echo base_url('users/login');      ?>" method="post">                           
                <?php foreach ($forms['login_form'] as $form_detail): ?>	

                    <label><?php echo $form_detail->field_label; ?></label>
                    <input name="<?php echo $form_detail->field_name; ?>"  value="<?php echo set_value($form_detail->field_name); ?>" class="form-control" type="<?php echo $form_detail->field_type; ?>"><br/>
                <?php endforeach; ?>
                <div class="checkbox">
                    <input type="checkbox" name="remember-me" value="1" />Remember Me 
                </div>           
                <input type="submit" value="Login" class="bluebtn fr"/>
                <a href="<?php echo base_url('users/forgotpassword');?>" class="forget-link">Forgot Password ?</a> <br/>
            </form>
        </div>
    </div>
</div>

<script>
    $('#login-form').on('submit', function(e) {

        e.preventDefault();
        formData = $('#login-form').serialize();

        $.ajax({
            data: formData,
            dataType: 'json',
            type: 'POST',
            url: '<?php echo base_url('users/login'); ?>',
            success: function(data, textStatus, jqXHR)
            {

                if (data.success) {
                    $('#error').html();
					$('#facebox .close').click();
					window.location.reload();
                   

                }
                else
                {
                    if (data.error) {
                        $('#error').html(data.error).addClass('alert alert-danger');
                    } else {
                        $('#error').html('');
                    }
                }

            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }

        })

    })

</script>
