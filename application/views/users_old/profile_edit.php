<?php if(isset($completed)){
        echo $completed;
};?>
<?php echo validation_errors();?>

<form action="" method="post" id="edit-profile-form">
  First Name
  <input name="first_name" value="<?php echo $user_info->first_name;?>" class="form-control" type="input">
  <br>
  Last Name
  <input name="last_name" value="<?php echo $user_info->last_name;?>" class="form-control" type="input">
  <br>
  User Name
  <input name="user_name" value="<?php echo $user_info->user_name;?>" class="form-control" type="input">
  <br>
  Email
  <input name="email" value="<?php echo $user_info->email;?>" class="form-control" type="input">
  <br>
  <?php /*
    <label>Password</label>
    <input name="password" value="" class="form-control" type="password"> <br>



    <label>Confirm Password</label>
    <input name="re-password" value="" class="form-control" type="password"> <br>
*/?>
  Gender<br>
  <div class="radio">
    <label>
      <input type="radio" name="gender" value="Male" <?php echo ($user_info->gender == 'Male') ? 'checked':''; ?>  >
      Male</label>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="gender" value="Female" <?php echo ($user_info->gender == 'Female') ? 'checked':'';?> >
      Female</label>
  </div>
  
  Mobile Number
  <input name="mobile_number" value="<?php echo $user_info->mobile_num;?>" class="form-control" type="input">
  <br>
  Address
  <input name="address" value="<?php echo $user_info->address;?>" class="form-control" type="input">
  <input type="submit"  value="Edit" class="edit-profile-submit bluebtn margtop20">
</form>
