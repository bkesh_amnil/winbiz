
<link href="<?php echo base_url(); ?>administrator/assets/css/chosen.css" rel="stylesheet" />
<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                                <li>/</li>
                                                                <li><a href="#">Kathmandu</a></li>
                                                                <li>/</li>					
                                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="">User Dashboard</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>

<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-12 pagecontainer2 offset-0">
            <div class="col-md-1 offset-0">
                <ul class="nav profile-tabs" role="tablist"  id="myTab">
                    <li class="active"><a data-toggle="tab" role="tab" href="#profile"><span class="profile-icon"></span>My Profile</a></li>
                    <li class=""><a id="shop-user-ajax" data-toggle="tab" role="tab" href="#add-user"><span class="shopUser-icon"></span>Shop User</a></li>
                    <li class=""><a id="shop-edit-ajax" data-toggle="tab" role="tab" href="#edit-shop"><span class="edit-icon"></span>Shop Edit</a></li>            
                    <li class=""><a data-toggle="tab" role="tab" href="#edit-category"><span class="productCategory-icon"></span>Product Category</a></li>
                    <li class=""><a data-toggle="tab" role="tab" href="#edit-product"><span class="product-icon"></span>Products</a></li>
                    <li class=""><a data-toggle="tab" role="tab" href="#package"><span class="package-icon"></span>Request for packages</a></li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- END OF FILTERS --> 
            <!-- LIST CONTENT-->
            <div class="col-md-11 offset-0"> 
                <!-- Here Goes shop list -->
                <div class="tab-content5" id="myTabContent">
                    <div class="padding20">
                        <h3 class="settingsContentTitleTop">
                            <?php echo $shop_info->shop_title; ?>

                        </h3>
                    </div>
                    <div id="profile" class="tab-pane fade active in padding20">
                        <div class="relative">
                            <ul class="nav nav-tabs myTab2pos" role="tablist">
                                <li class="active"><a href="#profile-info" role="tab" data-toggle="tab">Profile Info</a></li>
                                <li><a href="#edit-profile" role="tab" data-toggle="tab">Edit Profile</a></li>
                                <li><a href="#privacy" role="tab" data-toggle="tab">Privacy</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content6">
                                <div class="tab-pane active" id="profile-info"> <span class="size18">My Profile</span>
                                    <ul class="settingsList">
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> UserName :</h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->user_name; ?></span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> First Name:</h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->first_name; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Last Name: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->last_name; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Email : </h3>
                                            <span class="settingsListConten col-md-4"> <?php echo $user_info->email; ?> </span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Gender: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->gender; ?> </span></li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Mobile: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->mobile_num; ?></span> </li>
                                        <li>
                                            <h3 class="settingsListLabel col-md-2"> Address: </h3>
                                            <span class="settingsListContent col-md-4"><?php echo $user_info->address; ?></span> </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <?php /* ?> <div class="tab-pane active" id="profile-info">

                                  MY Profile <br/>

                                  UserName : <?php echo $user_info->user_name; ?> <br/>
                                  First Name:<?php echo $user_info->first_name; ?> <br/>
                                  Last Name:<?php echo $user_info->last_name; ?> <br/>
                                  Email :<?php echo $user_info->email; ?> <br/>
                                  Gender:<?php echo $user_info->gender; ?> <br/>
                                  Mobile:<?php echo $user_info->mobile_num; ?> <br/>
                                  Address:<?php echo $user_info->address; ?> <br/>


                                  </div><?php */ ?>
                                <div class="tab-pane" id="edit-profile">
                                    <div id="msg-profile"></div>
                                    <div id="edit-profile-form-div"></div>

                                    <script>
                                        $.get("<?php echo base_url('users/profile_edit') ?>", function(data) {
                                            $("#edit-profile-form-div").html(data);
                                        });
                                    </script>

                                </div>
                                <div class="tab-pane" id="privacy">

                                    <div id="msg-privacy"></div>
                                    <form id="password-form">
                                        <label>New Password</label>
                                        <input name="password" value="" class="form-control" type="password"> <br>

                                        <label>Confirm Password</label>
                                        <input name="re-password" value="" class="form-control" type="password"> <br>

                                        <input type="submit" value='submit' class="bluebtn"/>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="add-user" class="tab-pane fade padding20">
                        <div class="relative">

                            <ul class="nav nav-tabs myTab2pos" role="tablist">
                                <li class="active"><a href="#user-list" role="tab" data-toggle="tab">Users List</a></li>
                                <li><a href="#add-shop-user" role="tab" data-toggle="tab">Add Shop User</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content6">
                                <div class="tab-pane active" id="user-list">

                                    <div id="msg-user-list"></div>
                                    <div id="view-user-list"></div>

                                    <script>


                                        $(document).on('click', '#shop-user-ajax', function() {
                                            $.get("<?php echo base_url('users/get_shop_user') ?>", function(data) {
                                                $("#view-user-list").html(data);
                                            });
                                        });



                                    </script>



                                    <?php /*
                                      MY Profile <br/>

                                      UserName : <?php echo $user_info->user_name; ?> <br/>
                                      First Name:<?php echo $user_info->first_name; ?> <br/>
                                      Last Name:<?php echo $user_info->last_name; ?> <br/>
                                      Email :<?php echo $user_info->email; ?> <br/>
                                      Gender:<?php echo $user_info->gender; ?> <br/>
                                      Mobile:<?php echo $user_info->mobile_num; ?> <br/>
                                      Address:<?php echo $user_info->address; ?> <br/>

                                     */ ?>
                                </div>
                                <div class="tab-pane" id="add-shop-user">

                                    <div id="user-add-msg"></div>
                                    <form action="shop" method="post" id="add-new-user">
                                        <input type="hidden" name="shop_id" value="<?php echo $shop_info->id; ?>">
                                        <input type="hidden" name="created_by" value="<?php echo $user_info->id; ?>">

                                        <?php echo $this->load->view('users/user_registration'); ?>
                                        <input type="submit" value="Register" class="bluebtn margtop20"/>
                                    </form>

                                </div>


                            </div>
                        </div>
                    </div>

                    <div id="edit-product" class="tab-pane fade padding20">

                        <script>
                            $.get("<?php echo base_url('product/product_list/' . $shop_info->id) ?>", function(data) {
                                $("#edit-product").html(data);
                            });
                        </script>


                    </div>

                    <div id="edit-category" class="tab-pane fade padding20">
                        Add/Edit Category.

                        <script>

                            $.get("<?php echo base_url('shop_category/product_category_list/' . $shop_info->id) ?>", function(data) {
                                $("#edit-category").html(data);
                            });
                        </script>

                    </div>

                    <div id="edit-shop" class="tab-pane fade padding20">  

                        <script>
                            $(document).on('click', '#shop-edit-ajax', function() {
                                $.get("<?php echo base_url('users/shop_edit_form') ?>", function(data) {
                                    $("#edit-shop").html(data);
                                });
                            });
                        </script>

                    </div>

                    <div id="package" class="tab-pane fade padding20">

                        <div id ="req-package" >
                            <h3 class="settingsContentTitle">Package Info.</h3>
                            <div class="alert alert-info">
                                Your shop is currently packaged under  <?php
                                foreach ($shop_package_list as $key => $value):
                                    if ($value->id == $shop_info->package_id)
                                        echo '<span style="font-size:18px;">' . $value->package_name . '</span>';
                                    ?>						
                                <?php endforeach; ?></div>

                            <div>

                                <?php if ($shop_info->package_id < 2): ?>
                                    Do you want to request for following package.
                                    <div id='package_req' class="radio">
                                        <?php
                                        foreach ($shop_package_list as $key => $value):
                                            if ($value->id > $shop_info->package_id) {
                                                ?>							
                                                <input type="radio" name="shop-register-package" value="<?php echo $value->id; ?>" >	
                                                <?php echo $value->package_name; ?>

                                            <?php } endforeach; ?>

                                    </div>

                                    <a href='#' id="package-choose" class="btn-yes"> YES </a>


                                <?php endif; ?>

                            </div>

                        </div>





                    </div>





                </div>
            </div>
            <!-- End of offset1-->
            <div class="clearfix"></div>
        </div>
        <!-- END OF LIST CONTENT--> 
    </div>
</div>

<script>
    $(document).on('click', '.edit-profile', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: '<?php echo base_url("users/profile_edit") ?>',
            dataType: 'json',
            success: function(data) {

                if (data.success)
                {
                    // alert(data.editHtml);
                    $('#editprofile').html(data.editHtml);

                }
                else
                {
                    alert(data.error);
                }
            },
            error: function() {
                alert('error occured');
            }
        })
    });


    $(document).on('submit', '#edit-profile-form', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = $('#edit-profile-form').serialize();

        $.ajax({
            url: '<?php echo base_url('users/profile_edit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {


                if (data.error) {
                    $('#msg-profile').html(data.error).removeClass().addClass('alert alert-danger');
                }
                if (data.msg) {
                    $('#msg-profile').html(data.msg).removeClass().addClass('alert alert-success');
                    window.location.href = "<?php echo base_url('users/dashboard'); ?>";
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });

    //add-new-user

    $('#add-new-user').on('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        // element = $();
        var formData = $('#add-new-user').serialize();


        $.ajax({
            url: '<?php echo base_url('users/register'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                // alert('data');
                if (data.success)
                {
                    $('#user-add-msg').html(data.msg).removeClass('alert-danger').addClass('alert alert-success');
                    $('#add-new-user')[0].reset();
                    window.location.href = "<?php echo base_url('users/dashboard'); ?>";


                } else {
                    $('#user-add-msg').html(data.msg).removeClass('alert-success').addClass('alert alert-danger');
                    $('#user-add-msg').append(data.error);
                }





            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });


    function toggleDisplay($cssid)
    {
        $('#' + $cssid).toggle();
    }


    $('#package-choose').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var package_id = $('#package_req').find(':input:checked').val();
        var shop_id = <?php echo (isset($shop_info->id)) ? $shop_info->id : '0'; ?>

        $.ajax({
            url: '<?php echo base_url("users/package_req") ?>',
            data: {'shop_id': shop_id, 'package_id': package_id},
            type: 'POST',
            dataType: 'json',
            success: function(data) {

                if (data.success)
                {
                    // alert(data.editHtml);
                    $('#package-choose').html(data.msg);

                }
                else
                {
                    $('#package-choose').append(data.msg);
                }
            },
            error: function() {
                alert('error occured');
            }
        });


    });


    $(document).on('submit', '#password-form', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var formData = $(this).serialize();
        $.ajax({
            url: '<?php echo base_url('users/password_edit'); ?>',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            {
                if (data.error) {
                    $('#msg-privacy').html(data.error).removeClass().addClass('alert alert-danger');
                }
                if (data.msg) {
                    $('#msg-privacy').html(data.msg).removeClass().addClass('alert alert-success');
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log(textStatus);
                // STOP LOADING SPINNER
            }
        });
    });


</script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.autocomplete.multiselect.js'); ?>"></script>
<script>
    $(function() {
        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        $("#autocomplete-multiple")
// don't navigate away from the field on tab when selecting an item
                .bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }

        }).autocomplete({
            source: function(request, response) {
                $.getJSON("<?php echo base_url('ajax/getSearchCategoryList'); ?>", {
                    term: extractLast(request.term)
                }, response);
            },
            search: function() {
// custom minLength
                var term = extractLast(this.value);
                if (term.length < 2) {
                    return false;
                }
            },
            focus: function() {
// prevent value inserted on focus
                return false;
            },
            select: function(event, ui) {

                $("<div></div>")
                        .addClass("ui-autocomplete-multiselect-item")
                        .text(ui.item.label)
                        .append(
                        $("<span></span>")
                        .addClass("ui-icon ui-icon-close")
                        .attr('data-id', ui.item.id)
                        .click(function() {
                    var item = $(this).parent();
                    var removelist = ',' + ($(this).attr('data-id'));
                    var categorylists = $('#autocomplete-multiple').attr('data-id');
                    item.remove();
                    // console.log(categorylists.replace(item.attr('data-id'));

                    $('#autocomplete-multiple').attr('data-id', categorylists.replace(removelist, ''));
                })
                        )
                        .insertBefore(this);

                var terms = split(this.value);
// remove the current input
                terms.pop();
// add the selected item
                terms.push(ui.item.value);
// add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                this.value = '';

                var idterms = split($('#autocomplete-multiple').attr('data-id'));

                if (idterms.length > 0) {
                    idterms += (",") + (ui.item.id);
                }

                $("#autocomplete-multiple").attr('data-id', idterms);
                return false;
            }
        });
    });
</script>

