<div class="container breadcrub">
    <div> <a href="#" class="homebtn left"></a>
        <div class="left">
            <ul class="bcrumbs">
                <li>/</li>
                <!-- <li><a href="#">Home</a></li>
                                                <li>/</li>
                                                <li><a href="#">Kathmandu</a></li>
                                                <li>/</li>					
                                                <li><a href="#" class="active">Area</a></li> -->
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li>/</li>
                <li><a href="#">User Activation</a></li>
                <li> </li>
            </ul>
        </div>
        <a href="#" class="backbtn right"></a> </div>
    <div class="clearfix"></div>
    <div class="brlines"></div>
</div>


<div class="container">
    <div class="container mt25 offset-0">
        <div class="col-md-8 pagecontainer2 offset-0">
            <div class="padding30 grey"> <span class="size16 bold">Activation message.</span>
                <div class="line2"></div>


                <?php
                if ($msg && $msg['status']) {
                    echo '<p class="alert alert-success">' . $msg['msg'] . '</p>';
                    ?> 

                    <p>Please use your credentials to "Sign In". </p>

                    <?php
                } else {
                    echo '<p class="alert alert-danger">' . $msg['msg'] . '</p>';
                    ?>
                    <a href="<?php echo base_url(''); ?>"> Back to Home </a> 
                <?php }; ?> 

            </div>
        </div>
    </div>
</div>
