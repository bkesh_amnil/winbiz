<?php $this->load->view('common/header') ?>
<!-- <body> -->
<?php $this->load->view('common/navigation') ?>
<?php $this->load->view('common/carts_info_bar') ?>
	<!-- main content -->
	<?php echo $content;?>
<?php if(isset($footer_contents) && !empty($footer_contents)) { ?>
	<section class="services-section">
	    <?php $this->load->view('services', $footer_contents); ?>
	</section>
<?php } ?>
<!-- footer -->
<?php echo $this->load->view('common/footer') ?>
