<div class="container">
    <section class="cart-section">
        <h2><span>Summary</span> <img src="../../../images/icons/icon-line.png" alt=""/></h2>
        <div class="wizard-wrap">
            <div class="table-responsive">
            <table class="table">
            <thead>
               <th>Product</th>
               <th>Product Summary</th>
               <th>Quantity</th>
               <th>Unit Price(RS.)</th>
               <th>Total Price (RS.)</th>
               <th>Delete Item</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="my-items">
                            <div class="popular-products"><img class="img-responsive" src="../../../images/company-logo.png" alt=""/></div>
                        </div>
                    </td>
                    <td>
                        <span class="product-whey">
                            Gold standard 100% Whey
                            <span class="my-item-detail">Chocolate, 2lbs</span>
                        </span>
                    </td>
                    <td>
                        <input class="quantity" value="1"/>
                    </td>

                    <td>
                        <span>5000</span>
                    </td>

                    <td>
                        <span>5000</span>
                    </td>
                    <td><button class="btn-close"></button></td>
                </tr>
                
                <tr>
                    <td>
                        <div class="my-items">
                            <div class="popular-products"><img class="img-responsive" src="../../../images/company-logo.png" alt=""/></div>
                        </div>
                    </td>
                    <td>
                        <span class="product-whey">
                            Gold standard 100% Whey
                            <span class="my-item-detail">Chocolate, 2lbs</span>
                        </span>
                    </td>
                    <td>
                        <input class="quantity" value="1"/>
                    </td>

                    <td>
                        <span>5000</span>
                    </td>

                    <td>
                        <span>5000</span>
                    </td>
                    <td><button class="btn-close"></button></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Sub total (RS.)</td>
                    <td><span class="shipping-cost"> <span>6000</span></span></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Taxes(%)</td>
                    <td><span class="shipping-cost"><span>115</span></span></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Your Total (RS.)</td>
                    <td><span class="total-shipping"><span>6115</span></span></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        </div>
        </div>
        <button type="button" class="btn btn-cart-options btn-continue">Continue Shopping</button>
        <button type="button" class="btn btn-cart-options btn-checkout">Checkout</button>
    </section>
</div>


<script>

    function sure()

    {

        if (!confirm('Are you sure you want to remove this item from cart???'))

        {

            return false;

        }

    }

</script>