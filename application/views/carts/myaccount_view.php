<div class="container">
    <section class="cart-section">
        <h2><span>MY Account</span> <img src="../../../images/icons/icon-line.png" alt=""/></h2>
        <div class="row">
            <div class="col-lg-2">
                <ul id="profileTab" class="nav nav-tabs">
                    <li class="active"><a href="#myProfile" data-toggle="tab">My Profile</a></li>
                    <li><a href="#myOrderHistory" data-toggle="tab">My Order History</a></li>
                </ul>
            </div>
            <div class="col-lg-10">
                <div id="tabContent" class="tab-content" >
                    <div class="tab-pane fade in active" id="myProfile">
                        <form>
                            <div class="custom-group">
                                <h3 class="sub-title">Basic Information</h3>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Middle Name</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Contact No.</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-group">
                                <h3 class="sub-title">Security Information</h3>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Current Password</label>
                                            <input class="form-control" type="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input class="form-control" type="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <label>Re-type new password</label>
                                            <input class="form-control" type="password" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-cart-options btn-checkout" type="button">Save</button>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="myOrderHistory">
                        dfdf
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>