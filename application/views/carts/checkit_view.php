<div class="container">
    <section class="cart-section">
        <h2><span>Shipping</span> <img src="../../../images/icons/icon-line.png" alt=""/></h2>
        <div class="wizard-wrap">
            <form>
                <div class="custom-group">
                    <h3 class="sub-title">Billing Address Information</h3>
                    <span>Your billing address is your current address</span>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Contact No.</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Country</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="custom-group">
                    <h3 class="sub-title">Delivery Address Information</h3>
                        <span>Your delivery address is the destination you want the products to be delivered at.</span>
                        <div class="control-group">
                        <label class="control control--radio">
                            Deliver to the billing address
                            <input type="radio" name="prefer_time" checked="checked"/>
                            <div class="control__indicator"></div>
                        </label>
                        <label class="control control--radio">
                            Deliver to a different address.
                            <input type="radio" name="prefer_time"/>
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div>
                <div class="custom-group">
                    <h3 class="sub-title">New Delivery Address</h3>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Contact No.</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label>Country</label>
                                <input class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="custom-group">
                    <h3 class="sub-title">Payment Options</h3>
                    <div class="control-group">
                        <label class="control control--radio">
                            Cash on delivery
                            <input type="radio" name="prefer_time" checked="checked"/>
                            <div class="control__indicator"></div>
                        </label>
                        <label class="control control--radio">
                            Paypal
                            <input type="radio" name="prefer_time"/>
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div>
                <div class="custom-group">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit </p>
                    <p><input type="checkbox" /> I agree <a href="">terms and condition</a></p>
                </div>
                </form>
            </div>
        <button type="button" class="btn btn-cart-options btn-continue">Continue Shopping</button>
        <button type="button" class="btn btn-cart-options btn-checkout">Checkout</button>
    </section>
</div>