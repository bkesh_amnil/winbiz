<?php

class Form_model extends CI_Model {
	
	function form_fields($form_id, $show_in_grid = 0){
            if($show_in_grid)
            $this->db->where('show_in_grid', 1);
            $this->db->order_by('field_order', 'ASC');
            return $this->db->where('form_id', $form_id)->get('tbl_form_field')->result();
	}
	
	function get_submitted_data($form_submission_id, $form_field_id){
            $this->db->where('form_submission_id', $form_submission_id);
            $this->db->where('form_field_id', $form_field_id);
            $row = $this->db->get('tbl_form_submission_fields')->row();
            if($row){	
                return $row->form_field_value;
            } else { 
                return '';
            }
	}
	
	function public_form_fields($form_id, $front_display = 0){
            if($front_display)
                $this->db->where('front_display', 1);
            $this->db->order_by('field_order', 'ASC');
            return $this->db->where('form_id', $form_id)->get('tbl_form_field')->result();
	}

    function getFormFields($form_id, $show_in_grid = 0) {
        $query = "SELECT * FROM  tbl_form_field WHERE form_id = '".$form_id."'";
        if($show_in_grid) {
            $query .= " AND show_in_grid = '1' ";
        }
        $query .= " ORDER BY field_order ASC";
        $result = $this->db->query($query)->result();

        
        if(isset($result) && !empty($result)) {
            return $result;
        }

        return array();
    }
	
}