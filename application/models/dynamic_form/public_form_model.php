<?php
class Public_form_model extends CI_Model {
	
	function get_forms($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc'){
            $table_name = 'tbl_form';
            if($paginate) {
                $per_page = $this->custom_pagination->per_page();
                $this->db->limit($per_page, $uri);
            }
            $this->db->order_by($order_by, $order);
            if($this->input->get('search')){
                $this->db->like('form_title', $this->input->get('search'));
            }
            if($this->input->get('site')){
                $this->db->like('site_id', $this->input->get('site'));
            }
            if($id == 0 ) {
                $result = $this->db->get($table_name)->result();	
            }else{
                $this->db->where("{$table_name}.id", $id);
                $result = $this->db->get($table_name)->row();
            }
            return $result;	
	}
        
        function get_jobdetail($id){
            $this->db->select('tbl_form_submission_fields.form_field_value, tbl_form_field.field_name');
            $this->db->where('form_submission_id', $id);
            $this->db->join('tbl_form_field', 'tbl_form_field.id = tbl_form_submission_fields.form_field_id');
            $res = $this->db->get('tbl_form_submission_fields')->result();
            return $res;
        }
	
}