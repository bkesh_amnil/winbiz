<?php

class Public_model extends CI_Model {

    var $site_menus = 'tbl_site_menus';
    var $profile = 'tbl_profile';
    var $profile_detail = 'tbl_profile_detail';
    var $module = 'tbl_module';
    var $site = 'tbl_site';
    var $menu = 'tbl_menu';
    var $menu_page_setting = 'tbl_menu_page_setting';
    var $advertisement_position = 'tbl_advertisement_position';
    var $advertisement = 'tbl_advertisement';
    var $enterprise_categories = 'tbl_enterprise_categories';
    var $product_categories = 'tbl_product_categories';
    var $enterprises = 'tbl_enterprises';
    var $enterprise_category_list = 'tbl_enterprise_category_list';
    var $products = 'tbl_products';
    var $content = 'tbl_content';
    var $menu_page_settings = 'tbl_menu_page_settings';
    var $product_images = 'tbl_product_images';
    var $product_count = 'tbl_product_count';
    var $enterprise_count = 'tbl_enterprise_count';
    var $product_review = 'tbl_product_review';
    var $email_management = 'tbl_email_management';
    var $product_rate = 'tbl_product_rate';
    var $social_media = 'tbl_social_media';

    function getSiteDetails($enterprise_id) {
        $this->db->where('enterprise_id', $enterprise_id);
        $row = $this->db->get($this->site)->row();

        if (isset($row) && empty($row)) {
            $this->db->where('enterprise_id', '0');
            $row = $this->db->get($this->site)->row();
        }

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getMainMenuId($siteId) {
        $this->db->select('id');
        $this->db->where('site_id', $siteId);
        $this->db->where('main_menu', 'yes');
        $row = $this->db->get($this->site_menus)->row();

        return (isset($row) && !empty($row)) ? $row->id : '';
    }

    function getMainMenu($id, $siteId, $parent = 0) {
        $this->db->where($this->site_menus . '.id', $id);
        $this->db->where($this->menu . '.menu_parent', $parent);
        $this->db->where($this->menu . '.status', 1);
        $this->db->where($this->site_menus . '.site_id', $siteId);
        $this->db->order_by($this->menu . '.menu_position', 'ASC');
        $this->db->select($this->menu . ' .id, menu_title, menu_alias');
        $this->db->from($this->menu);
        $this->db->join($this->site_menus, $this->site_menus . '.id = ' . $this->menu . '.menu_type_id');
        $result = $this->db->get()->result();
//dumparray($result);
        $list = "";

        if (isset($result) && !empty($result)) {
            foreach ($result as $ind => $menu) {
                $menuListArr = "";
                foreach ($menu as $mind => $mval) {
                    $menuListArr[$mind] = $mval;
                }
                $menuListArr['childs'] = $this->getMainMenu($id, $siteId, $menu->id);
                $list[] = $menuListArr;
            }
        }

        return (isset($list) && !empty($list)) ? $list : array();
    }

    function getActiveMenu($menu_alias, $siteId) {
        $this->db->select('menu_parent, menu_alias');
        $this->db->join($this->site_menus, $this->site_menus . '.id = ' . $this->menu . '.menu_type_id');
        $this->db->where('menu_alias', $menu_alias);
        $this->db->where('site_id', $siteId);
        $row = $this->db->get($this->menu)->row();

        if (isset($row) && $row->menu_parent != '0') {
            $this->db->select('menu_alias');
            $this->db->where('id', $row->menu_parent);
            $row1 = $this->db->get($this->menu)->row();

            return $row1->menu_alias;
        } else {
            return $row->menu_alias;
        }

        return '';
    }

    function getEnterprises($ent_cat_id) {
        $this->db->select($this->enterprise . '.id, enterprise_name, enterprise_alias, enterprise_logo');
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . ' .enterprise_id =  ' . $this->enterprises . ' .id', 'left');
        $this->db->where($this->enterprises . '.status', 1);
        $this->db->where($this->enterprise_category_list . '.enterprise_category_id', $ent_cat_id);
        $this->db->order_by($this->enterprise . '.enterprise_name', 'ASC');
        $result = $this->db->get($this->enterprises)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getAllEnterpriseProductCategories() {
        $this->db->select($this->enterprise_categories . '.id as entcatId, enterprise_category_name, enterprise_category_alias, enterprise_name, enterprise_alias, ' . $this->enterprises . '.id as entId, ' . $this->site . '.sub_domain, site_offline');
        $this->db->from($this->enterprises);
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id = ' . $this->enterprises . '.id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->enterprise_category_list . '.enterprise_category_id');
        $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->enterprises . '.id', 'LEFT');
        $this->db->where($this->enterprises . '.status', '1');
        $this->db->order_by($this->enterprises . '.enterprise_name', 'ASC');
        $result = $this->db->get()->result();

        /* $query = "SELECT ec.id As entcatId, ec.enterprise_category_name, ec.enterprise_category_alias, e.enterprise_name, e.enterprise_alias ,e.id as entId,s.sub_domain,s.site_offline FROM tbl_enterprises e JOIN tbl_enterprise_category_list ecl ON e.id = ecl.enterprise_id JOIN tbl_enterprise_categories ec ON ec.id=ecl.enterprise_category_id LEFT JOIN tbl_site s ON s.enterprise_id=e.id WHERE e.status = '1' ORDER BY CAST(SUBSTRING(e.enterprise_name,LOCATE(' ',e.enterprise_name)+1) AS UNSIGNED)";
          $result = $this->db->query($query)->result(); */
        if (isset($result) && !empty($result)) {
            foreach ($result as $res) {
                $array[$res->enterprise_category_name][] = $res;
            }
        }
        return (isset($array) && !empty($array)) ? $array : array();
    }

    function getEnterpriseProductCategories($ent_id) {
        $this->db->select($this->enterprise_categories . '.id as entcatId, enterprise_category_name, enterprise_category_alias, enterprise_name, enterprise_alias, ' . $this->enterprises . '.id as entId, ' . $this->site . '.sub_domain, site_offline');
        $this->db->from($this->enterprise_category_list);
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->enterprise_category_list . '.enterprise_category_id');
        $this->db->join($this->enterprise, $this->enterprise . '.id = ' . $this->enterprise_category_list . '.enterprise_id');
        $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->enterprises . '.id', 'LEFT');
        $this->db->where($this->enterprise_category_list . '.enterprise_id', $ent_id);
        $this->db->order_by($this->enterprises . '.enterprise_name', 'ASC');
        $result = $this->db->get()->result();

        if (isset($result) && !empty($result)) {
            foreach ($result as $res) {
                $array[$res->enterprise_category_name] = $res;
                $array[$res->enterprise_category_name]->product_category = $this->public_model->getEnterpirseProductCategory($res->entcatId, $ent_id);
            }
        }

        return (isset($array) && !empty($array)) ? $array : array();
    }

    /**
     * original function, to retreive ad. infos from db
     * @param type $position_val
     * @param type $siteId
     * @return string
     */
    function getAdvertisement($position_val, $siteId) {
        $current_date = date("Y-m-d");
        if ($position_val == 'center') {
            $query = "SELECT `advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`,`tbl_products`.`product_category_id`
                    FROM (`tbl_advertisement`)
                    JOIN `tbl_advertisement_position`
                    ON `tbl_advertisement_position`.`id` = `tbl_advertisement`.`advertisement_position_id`
                    JOIN `tbl_products` 
                    ON `tbl_products`.`id` = `tbl_advertisement`.`product_id`
                    JOIN `tbl_site`
                    ON `tbl_site`.`id` = `tbl_advertisement`.`site_id`
                    WHERE `tbl_advertisement_position`.`status` = 1
                    AND `tbl_advertisement`.`status` = 1
                    AND `tbl_advertisement_position`.`position` = '" . $position_val . "'
                    AND `tbl_advertisement`.`site_id` = '" . $siteId . "'
                    AND CURDATE( ) BETWEEN  `publish_date` AND  `unpublish_date`
                    ORDER BY `tbl_advertisement_position`.`id`,`tbl_advertisement`.`advertisement_name` ASC";
        } else {
            $query = "SELECT `advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`
                    FROM (`tbl_advertisement`)
                    JOIN `tbl_advertisement_position`
                    ON `tbl_advertisement_position`.`id` = `tbl_advertisement`.`advertisement_position_id`";

            if ($position_val != 'right_top' && $position_val != 'right_center' && $position_val != 'right_bottom') {
                $query .= " JOIN `tbl_site`
                    ON `tbl_site`.`id` = `tbl_advertisement`.`site_id`";
            }

            $query .= " WHERE `tbl_advertisement_position`.`status` = 1
                    AND `tbl_advertisement`.`status` = 1
                    AND `tbl_advertisement_position`.`position` = '" . $position_val . "'";

            if ($position_val != 'right_top' && $position_val != 'right_center' && $position_val != 'right_bottom') {
                $query .= " AND `tbl_advertisement`.`site_id` = '" . $siteId . "'
                    AND CURDATE( ) BETWEEN  `publish_date` AND  `unpublish_date` ";
            }

            $query .= " ORDER BY `tbl_advertisement_position`.`id`,`tbl_advertisement`.`advertisement_name` ASC";
        }
        if ($position_val != 'center' && $position_val != 'banner') {
            $query .= " LIMIT 1";
            $row = $this->db->query($query)->row();

            $list = "";
            if (isset($row) && !empty($row)) {
                $advArr['advinfo'] = $row;
                switch ($row->advertisement_link_type) {
                    case "External URL":
                        $advArr['advurl'] = $row->external_url;
                        break;
                    case "Product":
                        $advArr['advurl'] = $this->getProductAlias($row->product_id);
                        break;
                    case "Internal Content":
                        $advArr['advurl'] = $this->getContentAlias($row->content_id);
                        break;
                    default:
                        $advArr['advurl'] = '';
                        break;
                }
                $list[] = $advArr;

                return $list;
            }

            return '';
        } else {
            $result = $this->db->query($query)->result();
//             printQuery();
            $list = "";
            if (isset($result) && !empty($result)) {
                foreach ($result as $ind => $res) {
                    $advArr['advinfo'] = $res;
                    switch ($res->advertisement_link_type) {
                        case "External URL":
                            $advArr['advurl'] = $res->external_url;
                            break;
                        case "Product":
                            $advArr['advurl'] = $this->getProductAlias($res->product_id);
                            break;
                        case "Internal Content":
                            $advArr['advurl'] = $this->getContentAlias($res->content_id);
                            break;
                        default:
                            $advArr['advurl'] = '';
                            break;
                    }
                    $list[] = $advArr;
                }
                return $list;
            }

            return array();
        }
    }

    /**
     * bkesh: implementation as per new changes in frontend
     * @param type $position_val
     * @param type $siteId
     * @return string
     */
    function get_advertisement_session($position_val, $siteId) {
        $current_date = date("Y-m-d");
        if ($position_val == 'center') {
            $this->db->select('`advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`,`p`.`product_category_id`')
                    ->from('tbl_advertisement as a')
                    ->join('tbl_advertisement_session as ases', 'ases.ad_id=a.id')
                    ->join('tbl_products as p', 'p.id=a.product_id')
                    ->join('tbl_advertisement_position as app', 'app.id=ases.position_id')
                    ->where('app.position', $position_val)
                    ->where('ases.site_id', $siteId)
                    ->where('a.status', 1)
                    ->where('ases.start_date >=', 'CURDATE( )')
                    ->where('ases.end_date <=', 'CURDATE( )');
//                    ->get();
//            $query = "SELECT `advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`,`tbl_products`.`product_category_id`
//                    FROM (`tbl_advertisement`)
//                    JOIN `tbl_advertisement_position`
//                    ON `tbl_advertisement_position`.`id` = `tbl_advertisement`.`advertisement_position_id`
//                    JOIN `tbl_products` 
//                    ON `tbl_products`.`id` = `tbl_advertisement`.`product_id`
//                    JOIN `tbl_site`
//                    ON `tbl_site`.`id` = `tbl_advertisement`.`site_id`
//                    WHERE `tbl_advertisement_position`.`status` = 1
//                    AND `tbl_advertisement`.`status` = 1
//                    AND `tbl_advertisement_position`.`position` = '" . $position_val . "'
//                    AND `tbl_advertisement`.`site_id` = '" . $siteId . "'
//                    AND CURDATE( ) BETWEEN  `publish_date` AND  `unpublish_date`
//                    ORDER BY `tbl_advertisement_position`.`id`,`tbl_advertisement`.`advertisement_name` ASC";
        } else {
            $this->db->select('`advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`')
                    ->from('tbl_advertisement as a')
                    ->join('tbl_advertisement_session as ases', 'ases.ad_id=a.id')
                    ->join('tbl_advertisement_position as app', 'app.id=ases.position_id')
                    ->where('app.position', $position_val)
                    ->where('ases.site_id', $siteId)
                    ->where('a.status', 1)
                    ->where('app.status', 1)
                    ->where('ases.start_date <=',$current_date)
                    ->where('ases.end_date >=', $current_date);
//                    ->get();
//            $query = "SELECT `advertisement_position_value`, `advertisement_name`, `advertisement_image`, `advertisement_link_type`, `advertisement_link_opens`, `advertisement_description`, `content_id`, `product_id`, `external_url`
//                    FROM (`tbl_advertisement`)
//                    JOIN `tbl_advertisement_position`
//                    ON `tbl_advertisement_position`.`id` = `tbl_advertisement`.`advertisement_position_id`";
//
//            if ($position_val != 'right_top' && $position_val != 'right_center' && $position_val != 'right_bottom') {
//                $query .= " JOIN `tbl_site`
//                    ON `tbl_site`.`id` = `tbl_advertisement`.`site_id`";
//            }
//
//            $query .= " WHERE `tbl_advertisement_position`.`status` = 1
//                    AND `tbl_advertisement`.`status` = 1
//                    AND `tbl_advertisement_position`.`position` = '" . $position_val . "'";
//
//            if ($position_val != 'right_top' && $position_val != 'right_center' && $position_val != 'right_bottom') {
//                $query .= " AND `tbl_advertisement`.`site_id` = '" . $siteId . "'
//                    AND CURDATE( ) BETWEEN  `publish_date` AND  `unpublish_date` ";
//            }
//
//            $query .= " ORDER BY `tbl_advertisement_position`.`id`,`tbl_advertisement`.`advertisement_name` ASC";
        }
        if ($position_val != 'center' && $position_val != 'banner') {
            $row = $this->db->limit(1)->get()->row();
//            $row = $this->db->query($query)->row();
//            printQuery();

            $list = "";
            if (isset($row) && !empty($row)) {
                $advArr['advinfo'] = $row;
                switch ($row->advertisement_link_type) {
                    case "External URL":
                        $advArr['advurl'] = $row->external_url;
                        break;
                    case "Product":
                        
                        $advArr['advurl'] = $this->getProductAlias($row->product_id);
                        break;
                    case "Internal Content":
                        $advArr['advurl'] = $this->getContentAlias($row->content_id);
                        break;
                    default:
                        $advArr['advurl'] = '';
                        break;
                }
                $list[] = $advArr;

                return $list;
            }

            return '';
        } else {
            $result = $this->db->get()->result();
//            $result = $this->db->query($query)->result();

            $list = "";
            if (isset($result) && !empty($result)) {
                foreach ($result as $ind => $res) {
                    $advArr['advinfo'] = $res;
                    switch ($res->advertisement_link_type) {
                        case "External URL":
                            $advArr['advurl'] = $res->external_url;
                            break;
                        case "Product":
                            $advArr['advurl'] = $this->getProductAlias($res->product_id);
                            break;
                        case "Internal Content":
                            $advArr['advurl'] = $this->getContentAlias($res->content_id);
                            break;
                        default:
                            $advArr['advurl'] = '';
                            break;
                    }
                    $list[] = $advArr;
                }
                return $list;
            }

            return array();
        }
    }

    function getProductAlias($product_id) {
        if (!empty($product_id)) {
//            echo $product_id;exit;
            $this->db->select($this->enterprise_categories . '.id as ent_cat_id, ' . $this->enterprises . '.id as ent_id, ' . $this->product_categories . '.id as prod_cat_id,' . $this->products . '.id as prod_id, product_alias');
            /* $this->db->select('product_alias, product_category_alias, enterprise_alias, enterprise_category_alias'); */
            $this->db->from($this->products);
            $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
            $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
            $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
            $this->db->where($this->products . '.id', $product_id);
            $row = $this->db->get()->row();

            if (isset($row) && !empty($row)) {
                return site_url('detail/' . $row->ent_cat_id . '/' . $row->ent_id . '/' . $row->prod_cat_id . '/' . $row->prod_id . '/' . $row->product_alias);
            }
//            echo 'ow';exit;

            return '';
        }

        return '';
    }

    function getContentAlias($content_id) {
        if (!empty($content_id)) {
            $this->db->select('alias');
            $this->db->where('id', $content_id);
            $row = $this->db->get($this->content)->row();

            if (isset($row) && !empty($row)) {
                return site_url($row->alias);
            }

            return '';
        }

        return '';
    }

    /* advertisement */
    /* enterprise */

    function getEnterpriseInfo($ent_cat_id, $ent_id, $ent_alias) {
        $this->db->select('enterprise_name, enterprise_logo, enterprise_website, enterprise_short_description, enterprise_description, enterprise_facebook_link, enterprise_twitter_link, enterprise_linkedin_link, enterprise_youtube_link, sub_domain, site_offline');
        $this->db->from($this->enterprises);
        $this->db->join($this->site, $this->site . '.enterprise_id = ' . $this->enterprises . '.id', 'LEFT');
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id = ' . $this->enterprises . '.id', 'LEFT');
        $this->db->where($this->enterprises . '.id', $ent_id);
        $this->db->where($this->enterprise_category_list . '.enterprise_category_id  ', $ent_cat_id);
        $this->db->where($this->enterprises . '.enterprise_alias', $ent_alias);
        $result = $this->db->get()->row();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getEnterpriseProduct($ent_cat_id, $ent_id, $ent_alias) {
        $this->db->select($this->products . '.id, product_name, product_alias, product_cover_image, product_category_id');
        $this->db->from($this->enterprises);
        $this->db->join($this->products, $this->products . ' .enterprise_id =  ' . $this->enterprises . ' .id', 'left');
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . ' .enterprise_id =  ' . $this->enterprises . ' .id', 'left');
        $this->db->where($this->enterprises . '.id', $ent_id);
        $this->db->where($this->enterprise_category_list . '.enterprise_category_id  ', $ent_cat_id);
        $this->db->where($this->products . '.enterprise_category_id', $ent_cat_id);
        $this->db->where($this->products . '.status', 1);
        $this->db->where($this->enterprises . '.status', 1);
        $this->db->order_by($this->products . '.product_name', 'ASC');
        if (!empty($ent_alias)) {
            $this->db->where($this->enterprises . '.enterprise_alias', $ent_alias);
        }
        $result = $this->db->get()->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getEnterpirseProductCategory($ent_cat_id, $ent_id, $ent_alias) {
        $this->db->select($this->product_categories . ' .id,  product_category_name, product_category_alias');
        $this->db->from($this->products);
        $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->where($this->enterprises . '.id', $ent_id);
        $this->db->where($this->products . '.enterprise_category_id  ', $ent_cat_id);
        $this->db->where($this->products . '.status ', '1');
        $this->db->order_by($this->product_categories . '.product_category_name', 'ASC');
        if (!empty($ent_alias)) {
            $this->db->where($this->enterprises . '.enterprise_alias', $ent_alias);
        }
        $this->db->group_by($this->products . '.product_category_id');
        $result = $this->db->get()->result();

        return isset($result) && !empty($result) ? $result : array();
    }

    function getEnterpriseEmail($ent_id) {
        $this->db->select('emails');
        $this->db->where('enterprise_id', $ent_id);
        $this->db->where('status', 1);
        $row = $this->db->get($this->email_management)->row();

        return (isset($row) && !empty($row)) ? $row->emails : '';
    }

    /* enterprise */

    function getBreadCrumb($url, $siteId) {
        $count = count($url);
        $current_url = current_url();
        $html = '';
        $crumb = '';

        if ($count > 1) {
            $type = $this->uri->segment(1);
            if ($type == 'cat') {
                if ($count > 3) {
                    $liClass = '';
                } else {
                    $liClass = ' class="active"';
                }
                $enterprise_category = $this->getNameAliasforBreadCrumbById($this->uri->segment(2), 'enterprise_category');
                $enterprise_category_url = site_url('cat/' . $this->uri->segment(2) . '/' . $enterprise_category['alias']);
                if ($count > 3) {
                    $html .= '<li' . $liClass . '><a href="' . $enterprise_category_url . '">' . ucwords($enterprise_category['name']) . '</a></li>';
                    $enterprise = $this->getNameAliasforBreadCrumbById($this->uri->segment($count - 1), 'enterprise');
                    $html .= '<li class="active">' . ucwords($enterprise['name']) . '</li>';
                } else {
                    $html .= '<li class="active">' . ucwords($this->uri->segment(3)) . '</li>';
                }
            } else {
                $enterprise_category_url = $enterprise_url = "javascript:void(0);";
                $product = $this->getNameAliasforBreadCrumbById($this->uri->segment($count - 1), 'product');
                $product_category = $this->getNameAliasforBreadCrumbById($this->uri->segment($count - 2), 'product_category');
                $enterprise = $this->getNameAliasforBreadCrumbById($this->uri->segment($count - 3), 'enterprise');
                $enterprise_category = $this->getNameAliasforBreadCrumbById($this->uri->segment($count - 4), 'enterprise_category');

                if ($siteId == '1') {
                    $enterprise_category_url = site_url('cat/' . $this->uri->segment($count - 4) . '/' . $enterprise_category['alias']);
                    $enterprise_url = site_url('cat/' . $this->uri->segment($count - 4) . '/' . $this->uri->segment($count - 3) . '/' . $enterprise['alias']);
                }
                $html .= '<li><a href="' . $enterprise_category_url . '">' . ucwords($enterprise_category['name']) . '</a></li>';
                $html .= '<li><a href="' . $enterprise_url . '">' . ucwords($enterprise['name']) . '</a></li>';
                $html .= '<li>' . ucwords($product_category['name']) . '</li>';
                $html .= '<li class="active">' . ucwords($product['name']) . '</li>';
            }
        } else {
            $url = site_url($this->uri->segment(1));
            $menu_name = $this->getNameforBreadCrumb($this->uri->segment(1), 'content', $siteId);
            if (isset($menu_name) && !empty($menu_name)) {
                $html .= $menu_name;
            } else {
                $html .= '<li class="active">' . ucwords($this->uri->segment(1)) . '</li>';
            }
        }

        return $html;
    }

    function getNameforBreadCrumb($alias, $type, $siteId) {
        if ($type == 'content') {
            $this->db->select('menu_parent, menu_title');
            $this->db->join($this->site_menus, $this->site_menus . '.id = ' . $this->menu . '.menu_type_id');
            $this->db->where('menu_alias', $alias);
            $this->db->where('site_id', $siteId);
            $row = $this->db->get($this->menu)->row();

            if (isset($row) && !empty($row)) {
                if ($row->menu_parent != '0') {
                    $this->db->select('menu_title');
                    $this->db->where('id', $row->menu_parent);
                    $row1 = $this->db->get($this->menu)->row();

                    return '<li>' . ucwords($row1->menu_title) . '</li><li class="active"><a href="' . base_url($alias) . '">' . ucwords($row->menu_title) . '</a></li>';
                } else {
                    return '<li class="active"><a href="' . base_url($alias) . '">' . ucwords($row->menu_title) . '</a></li>';
                }
            }
        } elseif ($type == 'product_categories') {
            $this->db->select('product_category_name');
            $this->db->where('product_category_alias', $alias);
            $row = $this->db->get($this->product_categories)->row();

            if (isset($row) && !empty($row)) {
                return $row->product_category_name;
            }
        } else {
            $this->db->select($type . '_name as name');
            $this->db->where($type . '_alias', $alias);
            if ($type != 'enterprise_category') {
                $row = $this->db->get('tbl_' . $type . 's')->row();
            } else {
                $row = $this->db->get($this->enterprise_categories)->row();
            }

            if (isset($row) && !empty($row)) {
                return $row->name;
            }
        }

        return NULL;
    }

    function getNameAliasforBreadCrumbById($id, $type) {
        $this->db->select($type . '_name as name, ' . $type . '_alias as alias');
        $this->db->where('id', $id);
        if ($type == 'enterprise_category') {
            $row = $this->db->get($this->enterprise_categories)->row();
        } elseif ($type == 'product_category') {
            $row = $this->db->get($this->product_categories)->row();
        } else {
            $row = $this->db->get('tbl_' . $type . 's')->row();
        }

        if (isset($row) && !empty($row)) {
            $data['name'] = $row->name;
            $data['alias'] = $row->alias;
            return $data;
        }
    }

    /*

      function getNameforBreadCrumbById($id, $type) {
      $this->db->select($type . '_name as name');
      $this->db->where('id', $id);
      if($type == 'enterprise_category') {
      $row = $this->db->get($this->enterprise_categories)->row();
      } elseif($type == 'product_category') {
      $row = $this->db->get($this->product_categories)->row();
      } else {
      $row = $this->db->get('tbl_'.$type.'s')->row();
      }

      if(isset($row) && !empty($row)) {
      return $row->name;
      }
      }

      function getAliasforBreadCrumbById($id, $type) {
      $this->db->select($type . '_alias as alias');
      $this->db->where('id', $id);
      if($type == 'enterprise_category') {
      $row = $this->db->get($this->enterprise_categories)->row();
      } elseif($type == 'product_category') {
      $row = $this->db->get($this->product_categories)->row();
      } else {
      $row = $this->db->get('tbl_'.$type.'s')->row();
      }

      if(isset($row) && !empty($row)) {
      return $row->alias;
      }
      } */

    /* cms */

    function getMenuId($menu_alias, $siteId) {
        $this->db->select($this->menu . '.id');
        $this->db->join($this->site_menus, $this->site_menus . '.id = ' . $this->menu . '.menu_type_id');
        $this->db->where('menu_alias', $menu_alias);
        $this->db->where($this->menu . '.status', 1);
        $this->db->where('site_id', $siteId);
        $row = $this->db->get($this->menu)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getModules($menuId, $contentAlias = false, $paginate = false, $start = 0, $per_page = 10) {
        $this->db->select($this->menu_page_settings . '.content_ids, ' . $this->menu_page_settings . '.module_title, ' . $this->module . '.module_name');
        $this->db->where($this->menu_page_settings . '.menu_id', $menuId);
        $this->db->order_by("position", "asc");
        $this->db->from($this->menu_page_settings);
        $this->db->join($this->module, $this->module . '.id = ' . $this->menu_page_settings . '.module_id');
        $result = $this->db->get()->result();

        $modules = array();
        if (isset($result) && !empty($result)) {
            foreach ($result as $ind => $data) {
                $tblName = $this->getTableName($data->module_name);
                if (!empty($tblName)) {
                    $modules[$ind] = array("module" => $data->module_name, "module_title" => $data->module_title, "data" => $this->getData($tblName, $data->content_ids, $paginate, $start, $per_page));
                }
            }
        }

        return $modules;
    }

    function getTableName($moduleName = "") {
        switch ($moduleName) {
            case "menu":
                $tblName = "tbl_menu";
                $fld = "menu_title";
                break;
            case "content":
                $tblName = "tbl_content";
                break;
            case "advertisement":
                $tblName = "tbl_advertisement";
                break;
            case "dynamic_form":
                $tblName = "tbl_form";
                break;
            case "form_fields":
                $tblName = "tbl_form";
                break;
            default:
                $tblName = "";
        }
        return $tblName;
    }

    function getData($tblName, $ids, $paginate = false, $start = 0, $per_page = 10) {
        $ids = explode(',', $ids);
        $this->db->where_in("id", $ids);
        if ($paginate)
            $this->db->limit($per_page, $start);
        $data = $this->db->get($tblName)->result();

        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getFooterContent($category_id) {
        $this->db->select('title, description, title_image, image_status');
        $this->db->where('status', 1);
        $this->db->where('category_id', $category_id);
        $result = $this->db->get($this->content)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getFooterMenus() {
        $this->db->select('menu_type_id, menu_title, menu_alias');
        $this->db->where('status', 1);
        $this->db->where('menu_type_id', 2);
        $this->db->or_where('menu_type_id', 3);
        $result = $this->db->get($this->menu)->result();

        $this->db->select('description');
        $this->db->where('status', 1);
        $this->db->where('id', 10);
        $row = $this->db->get($this->content)->row();

        $list = "";
        $arr = "";

        if (isset($result) && !empty($result)) {
            foreach ($result as $res) {
                if ($res->menu_type_id == '2') {
                    $type = 'information';
                } else {
                    $type = 'our services';
                }
                $arr[$type][] = $res;
            }
        }

        if (isset($row) && !empty($row)) {
            $arr['quick contact'] = $row;
        }

        $list = $arr;
        return $list;
    }

    function getSocialMedia($siteId) {
        $this->db->select('facebook_url, twitter_url, youtube_url, linkedin_url, youtube_url');
        $this->db->where('id', $siteId);
        $result = $this->db->get($this->site)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    /* cms */
    /* product */

    function getProductInfo($ent_cat_id, $ent_id, $prod_cat_id, $prod_id) {
        $this->db->select($this->products . '.id, product_name, show_product_price, product_price, product_short_description, product_description, product_cover_image, ' . $this->enterprises . '.id as ent_id');
        $this->db->from($this->products);
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id = ' . $this->products . '.enterprise_id');
        $this->db->where($this->enterprise_category_list . '.enterprise_category_id', $ent_cat_id);
        $this->db->where($this->products . '.enterprise_id', $ent_id);
        $this->db->where($this->products . '.product_category_id', $prod_cat_id);
        $this->db->where($this->products . '.id', $prod_id);
        $result = $this->db->get()->row();

        $list = "";

        if (isset($result) && !empty($result)) {
            $prodArr["product_info"] = $result;
            $prodArr["product_images"] = $this->getProductImages($result->id);
            $prodArr["product_review"] = $this->getProductReview($result->id);
            $prodArr["product_rating"] = $this->getProductRating($result->id);

            $list[] = $prodArr;
            return $list;
        }

        return array();
    }

    function getProductImages($product_id) {
        $this->db->select('product_image, product_image_title, product_image_description');
        $this->db->where('products_id', $product_id);
        $result = $this->db->get($this->product_images)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductReview($product_id) {
        $query = "SELECT Re.customer_name, Re.review, Re.review_date, Ra.rate
                    FROM tbl_product_review AS Re
                    LEFT JOIN tbl_product_rate Ra
                    ON Re.email = Ra.email
                    AND Re.ip_address = Ra.ip_address
                    AND Re.enterprise_id = Ra.enterprise_id
                    AND Re.product_id = Ra.product_id
                    WHERE Re.product_id = " . $product_id . "
                    ORDER BY Re.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductRating($product_id) {
        $this->db->select('rate');
        $this->db->where('product_id', $product_id);
        $result = $this->db->get($this->product_rate)->result();

        if (isset($result) && !empty($result)) {
            $count = count($result);
            $total_votes = 0;
            foreach ($result as $res) {
                $total_votes += $res->rate;
            }
            $avg = $total_votes / $count;
            $data['rate'] = $avg;
            $data['count'] = $count;

            return $data;
        }

        return 0;
    }

    function increaseProductCount($product_id) {
        $this->db->where('product_id', $product_id)
                ->set('view_count', 'view_count + 1', FALSE)
                ->update($this->product_count);
    }

    function increaseEnterpriseCount($ent_id, $siteId) {
        $this->db->where('enterprise_id', $ent_id);
        $this->db->where('site_id', $siteId);
        $result = $this->db->get($this->enterprise_count)->result();
        if (isset($result) && !empty($result)) {
            $this->db->where('enterprise_id', $ent_id);
            $this->db->where('site_id', $siteId);
            $this->db->set('count', 'count + 1', FALSE);
            $this->db->update($this->enterprise_count);
        } else {
            $data = array(
                'site_id' => $siteId,
                'enterprise_id' => $ent_id,
                'count' => 1
            );
            $this->db->insert($this->enterprise_count, $data);
        }
    }

    function getBestSellingProducts($limit, $siteId) {
        $this->db->select($this->products . '.id as prod_id, product_name, product_alias, product_cover_image, product_short_description, ' . $this->product_categories . '.id as prod_cat_id, ' . $this->enterprises . '.id as ent_id, ' . $this->enterprise_categories . '.id as ent_cat_id');
        $this->db->from($this->products);
        $this->db->join($this->product_count, $this->product_count . '.product_id = ' . $this->products . '.id');
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
        if ($siteId != '1') {
            $this->db->join($this->site, $this->site . '.id = ' . $this->products . '.site_id');
            $this->db->where($this->products . '.site_id', $siteId);
        }
        $this->db->limit($limit);
        $this->db->order_by($this->product_count . '.product_id', 'DESC');
        $result = $this->db->get()->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBestSellingProductsByEnterprise($limit, $ent_id) {
        $this->db->select($this->products . '.id as prod_id, product_name, product_alias, product_cover_image, product_short_description, ' . $this->product_categories . '.id as prod_cat_id, ' . $this->enterprises . '.id as ent_id, ' . $this->enterprise_categories . '.id as ent_cat_id');
        $this->db->from($this->products);
        $this->db->join($this->product_count, $this->product_count . '.product_id = ' . $this->products . '.id');
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
        $this->db->where($this->enterprises . '.id', $ent_id);
        $this->db->where($this->products . '.status', '1');

        $this->db->limit($limit);
        $this->db->order_by($this->product_count . '.product_id', 'DESC');
        $result = $this->db->get()->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductInfoFb($product_id) {
        $this->db->select($this->products . '.id, product_name, product_alias, product_short_description, product_cover_image, enterprise_category_alias, enterprise_alias, product_category_alias');
        $this->db->from($this->products);
        $this->db->join($this->product_categories, $this->product_categories . '.id =' . $this->products . '.product_category_id');
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->enterprises . '.enterprise_category_id');
        $this->db->where($this->products . '.id', $product_id);
        $row = $this->db->get()->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getProductsByProductCategory_old($product_category_id, $ent_id) {
        $this->db->select($this->products . '.id, product_name, product_alias, product_category_id, product_cover_image, enterprise_category_id, ' . $this->enterprises . '.id as enterprise_id');
        $this->db->from($this->products);
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->where($this->products . '.product_category_id', $product_category_id);
        $this->db->where($this->products . '.enterprise_id', $ent_id);
        $this->db->where($this->products . '.status', 1);

        $result = $this->db->get()->result();
        die($this->db->last_query());
        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductsByProductCategory($product_category_id, $ent_id) {
        $query = "SELECT p.id, p.product_name, p.product_alias, p.product_category_id, p.product_cover_image,p.enterprise_category_id,e.id as enterprise_id FROM tbl_products p LEFT JOIN tbl_enterprises e ON e.id = p.enterprise_id WHERE p.product_category_id=$product_category_id AND p.enterprise_id = $ent_id AND p.status = '1' ORDER BY CAST(SUBSTRING(p.product_name,LOCATE(' ',p.product_name)+1) AS UNSIGNED)";
        $result = $this->db->query($query)->result();
        return (isset($result) && !empty($result)) ? $result : array();
    }

    /* product */
    /* search */

    function getSearchResult($search_val) {
        $list = "";

        $this->db->select($this->enterprises . '.id as ent_id, enterprise_name as name, enterprise_logo as image, enterprise_alias, ' . $this->enterprise_category_list . '.enterprise_category_id as ent_cat_id');
        $this->db->from($this->enterprises);
        $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id = ' . $this->enterprises . '.id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->enterprise_category_list . '.enterprise_category_id');
        $this->db->like('enterprise_name', $search_val, 'both');
        $this->db->or_like('enterprise_description', $search_val, 'both');
        $result1 = $this->db->get()->result();

        if (isset($result1) && !empty($result1)) {
            foreach ($result1 as $res) {
                $searchArr['id'] = $res->ent_id;
                $searchArr['data'] = $res->name;
                $searchArr['img'] = base_url('image/enterprise/100/100/' . $res->image);
                $searchArr['url'] = site_url('cat/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->enterprise_alias);
                $list['Enterprise'][] = $searchArr;
            }
        }

        $this->db->select('id');
        $this->db->where('status', 1);
        $this->db->like('enterprise_category_name', $search_val, 'both');
        $row = $this->db->get($this->enterprise_categories)->result();

        if (isset($row) && !empty($row)) {
            foreach ($row as $val) {
                /* products under enterprise category */
                /* enterprise under enterprise category */
                $this->db->select($this->enterprises . '.id as ent_id, enterprise_name as name, enterprise_logo as image, enterprise_alias,' . $this->enterprise_category_list . '.enterprise_category_id as ent_cat_id');
                $this->db->from($this->enterprises);
                $this->db->join($this->enterprise_category_list, $this->enterprise_category_list . '.enterprise_id=' . $this->enterprise . '.id');
                $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->enterprise_category_list . '.enterprise_category_id');
                $this->db->where($this->enterprise_categories . '.id', $val->id);
                $result1 = $this->db->get()->result();

                if (isset($result1) && !empty($result1)) {
                    foreach ($result1 as $res) {
                        $searchArr['id'] = $res->ent_id;
                        $searchArr['data'] = $res->name;
                        $searchArr['img'] = base_url('image/enterprise/100/100/' . $res->image);
                        $searchArr['url'] = site_url('cat/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->enterprise_alias);
                        $list['Enterprise'][] = $searchArr;
                    }
                }
                /* products under enterprise category */
                $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
                $this->db->from($this->products);
                $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
                $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
                $this->db->order_by($this->products . '.id', DESC);
                $this->db->where($this->products . '.status', 1);
                $this->db->where($this->enterprises . '.status', 1);
                $this->db->where($this->enterprise_categories . '.id', $val->id);
                $result = $this->db->get()->result();

                if (isset($result) && !empty($result)) {
                    foreach ($result as $res) {
                        $searchArr['id'] = $res->prod_id;
                        $searchArr['data'] = $res->name;
                        $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                        $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                        $list['Products'][] = $searchArr;
                    }
                }
                /* enterprise under enterprise category */
            }
        }

        $this->db->select('id');
        $this->db->where('status', 1);
        $this->db->like('product_category_name', $search_val, 'both');
        $row1 = $this->db->get($this->product_categories)->result();

        if (isset($row1)) {
            foreach ($row1 as $val1) {
                /* products under product category */
                $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
                $this->db->from($this->products);
                $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
                $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
                $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
                $this->db->order_by($this->products . '.id', DESC);
                $this->db->where($this->products . '.status', 1);
                $this->db->where($this->enterprises . '.status', 1);
                $this->db->where($this->product_categories . '.id', $val1->id);
                $result = $this->db->get()->result();

                if (isset($result) && !empty($result)) {
                    foreach ($result as $res) {
                        $searchArr['id'] = $res->prod_id;
                        $searchArr['data'] = $res->name;
                        $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                        $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                        $list['Products'][] = $searchArr;
                    }
                }
                /* products under product category */
            }
        }

        $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
        $this->db->from($this->products);
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
        $this->db->order_by($this->products . '.id', DESC);
        $this->db->like('product_name', $search_val, 'both');
        $this->db->or_like('product_description', $search_val, 'both');
        $result = $this->db->get()->result();

        if (isset($result) && !empty($result)) {
            foreach ($result as $res) {
                $searchArr['id'] = $res->prod_id;
                $searchArr['data'] = $res->name;
                $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                $list['Products'][] = $searchArr;
            }
        }
        $this->db->select('id as content_id,description');
        $this->db->where('status', 1);
        $this->db->where('site_id', $site_id);
        $this->db->like('description', $search_val, 'both');
        $content_ids = $this->db->get($this->content)->result();
        if (isset($content_ids) && !empty($content_ids)) {
            foreach ($content_ids as $id) {
                $this->db->select('menu_id');
                $this->db->where('content_ids', $id->content_id);
                $this->db->where('module_id', '11');
                $menu = $this->db->get($this->menu_page_settings)->result();
                foreach ($menu as $key => $menu_id) {
                    $ids[$key] = $menu_id;
                    $ids[$key]->description = $id->description;
                }
            }
            foreach ($ids as $id) {
                if ($id->menu_id != '2') {
                    $this->db->select('menu_alias , menu_heading');
                    $this->db->where('id', $id->menu_id);
                    $alias = $this->db->get($this->menu)->row();
                    $searchArr['url'] = site_url($alias->menu_alias);
                    $searchArr['menu_heading'] = $alias->menu_heading;
                    $searchArr['description'] = $id->description;
                    $list['Contents'][] = $searchArr;
                }
            }
        }
        return $list;
    }

    function getSearchResultByEnterprise($search_val, $ent_id, $site_id) {
        $list = "";

        $this->db->select('id');
        $this->db->where('status', 1);
        $this->db->like('enterprise_category_name', $search_val, 'both');
        $row = $this->db->get($this->enterprise_categories)->result();

        if (isset($row) && !empty($row)) {
            foreach ($row as $val) {
                /* products under enterprise category */
                $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
                $this->db->from($this->products);
                $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
                $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
                $this->db->order_by($this->products . '.id', DESC);
                $this->db->where($this->products . '.status', 1);
                $this->db->where($this->enterprises . '.status', 1);
                $this->db->where($this->enterprises . '.id', $ent_id);
                $this->db->where($this->enterprise_categories . '.id', $val->id);
                $result = $this->db->get()->result();

                if (isset($result) && !empty($result)) {
                    foreach ($result as $res) {
                        $searchArr['id'] = $res->prod_id;
                        $searchArr['data'] = $res->name;
                        $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                        $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                        $list['Products'][] = $searchArr;
                    }
                }
                /* products under enterprise category */
                /* enterprise under enterprise category */
            }
        }

        $this->db->select('id');
        $this->db->where('status', 1);
        $this->db->like('product_category_name', $search_val, 'both');
        $row1 = $this->db->get($this->product_categories)->result();

        if (isset($row1)) {
            foreach ($row1 as $val1) {
                /* products under product category */
                $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
                $this->db->from($this->products);
                $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
                $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
                $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
                $this->db->order_by($this->products . '.id', DESC);
                $this->db->where($this->products . '.status', 1);
                $this->db->where($this->enterprises . '.status', 1);
                $this->db->where($this->enterprises . '.id', $ent_id);
                $this->db->where($this->product_categories . '.id', $val1->id);
                $result = $this->db->get()->result();

                if (isset($result) && !empty($result)) {
                    foreach ($result as $res) {
                        $searchArr['id'] = $res->prod_id;
                        $searchArr['data'] = $res->name;
                        $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                        $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                        $list['Products'][] = $searchArr;
                    }
                }
                /* products under product category */
            }
        }

        $this->db->select($this->products . '.id as prod_id, product_name as name, product_alias, product_cover_image as image, product_category_id as prod_cat_id, enterprise_id as ent_id,' . $this->enterprise_categories . '.id as ent_cat_id');
        $this->db->from($this->products);
        $this->db->join($this->enterprises, $this->enterprises . '.id = ' . $this->products . '.enterprise_id');
        $this->db->join($this->product_categories, $this->product_categories . '.id = ' . $this->products . '.product_category_id');
        $this->db->join($this->enterprise_categories, $this->enterprise_categories . '.id = ' . $this->products . '.enterprise_category_id');
        $this->db->order_by($this->products . '.id', DESC);
        $this->db->like('product_name', $search_val, 'both');
        $result = $this->db->get()->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $res) {
                $searchArr['id'] = $res->prod_id;
                $searchArr['data'] = $res->name;
                $searchArr['img'] = base_url('image/product_cover_image/600/600/' . $res->image);
                $searchArr['url'] = site_url('detail/' . $res->ent_cat_id . '/' . $res->ent_id . '/' . $res->prod_cat_id . '/' . $res->prod_id . '/' . $res->product_alias);
                $list['Products'][] = $searchArr;
            }
        }
        $this->db->select('id as content_id,description');
        $this->db->where('status', 1);
        $this->db->where('site_id', $site_id);
        $this->db->like('description', $search_val, 'both');
        $content_ids = $this->db->get($this->content)->result();
        if (isset($content_ids) && !empty($content_ids)) {
            foreach ($content_ids as $id) {
                $this->db->select('menu_id');
                $this->db->where('content_ids', $id->content_id);
                $this->db->where('module_id', '11');
                $menu = $this->db->get($this->menu_page_settings)->result();
                foreach ($menu as $key => $menu_id) {
                    $ids[$key] = $menu_id;
                    $ids[$key]->description = $id->description;
                }
            }
            foreach ($ids as $id) {
                if ($id->menu_id != '2') {
                    $this->db->select('menu_alias , menu_heading');
                    $this->db->where('id', $id->menu_id);
                    $alias = $this->db->get($this->menu)->row();
                    $searchArr['url'] = site_url($alias->menu_alias);
                    $searchArr['menu_heading'] = $alias->menu_heading;
                    $searchArr['description'] = $id->description;
                    $list['Contents'][] = $searchArr;
                }
            }
        }
        return $list;
    }

    function get_logo() {
        $query = "SELECT * FROM tbl_logo";
        $result = $this->db->query($query)->row();
        return $result;
    }

    /* search */
    /* review & rate */

    function checkUniqueness($client_ip, $client_email, $enterprise_id, $product_id) {
        $review = $rate = 0;

        $this->db->select('id');
        $this->db->where('ip_address', $client_ip);
        $this->db->where('email', $client_email);
        $this->db->where('enterprise_id', $enterprise_id);
        $this->db->where('product_id', $product_id);
        $row = $this->db->get($this->product_review)->row();

        if (isset($row) && !empty($row)) {
            $review = 1;
        }

        $this->db->select('id');
        $this->db->where('ip_address', $client_ip);
        $this->db->where('email', $client_email);
        $this->db->where('enterprise_id', $enterprise_id);
        $this->db->where('product_id', $product_id);
        $row1 = $this->db->get($this->product_rate)->row();

        if (isset($row1) && !empty($row1)) {
            $rate = 1;
        }

        if ($review) {
            if ($rate) {
                return 'rated and reviewed';
            } else {
                return 'not rated';
            }
        } else {
            if ($rate) {
                return 'not reviewed';
            } else {
                return 'not rated and not reviewed';
            }
        }
    }

    /* review & rate */
}
