<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users Dashboard_Model Model
 * @package Model
 * @subpackage Model
 * Date created:Feb 22 2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        
    }

    /**
     * Checking for the rows in tableby its email and password
     * @param type $table
     * @param type $condition
     * @return rows from table
     */
    public function authenticate($table, $condition) {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->row();
        }
    }

    public function get_user_detail($uid) {
        $this->db->select("*");
        $this->db->from('tbl_users_profile as up');
        $this->db->join('tbl_users as u', 'u.id = up.user_id');
        $this->db->where('u.id', $uid);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->row();
        }
    }

    function register_user() {
        $login_data = array(
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'ip_address' => $_SERVER['REMOTE_SERVER'],
            'status' => '1',
        );
        $this->db->insert('tbl_users', $login_data);
        $user_id = $this->db->insert_id();
        $not = array('submitUserLogin', 'email', 'password', 'passconf');
        $data = $this->mylibrary->get_post_array($not);
        $data['user_id'] = $user_id;
        $this->db->insert('tbl_users_profile', $data);
        return $user_id;
    }

    function register_checkout_user($billing_data, $password, $ip) {
        $salt = random_string('alnum', 4);
        $login_data = array(
            'email' => $billing_data['email'],
            'salt' => $salt,
            'password' => md5($password),
            'ip_address' => $ip,
            'status' => '1',
        );
        $this->db->insert('tbl_users', $login_data);
        $user_id = $this->db->insert_id();
        unset($billing_data['email']);
        $data = $billing_data;
        $data['user_id'] = $user_id;
        $this->db->insert('tbl_users_profile', $data);
        return $user_id;
    }

    function update_user() {
        $login_data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'salt' => random_string('alnum', 4),
            'ip_address' => $_SERVER['REMOTE_SERVER'],
            'status' => '1',
        );
        $this->db->insert('tbl_users', $login_data);
        $user_id = $this->db->insert_id();
        $not = array('submitUserLogin', 'email', 'password', 'passconf');
        $data = $this->mylibrary->get_post_array($not);
        $data['user_id'] = $user_id;
        $this->db->insert('tbl_users_profile', $data);
        return $user_id;
    }



    function saveCode($code,$email)
    {
        $data['forgotten_password_code']=$code;
        $this->db->where('email',$email);
        $query=$this->db->update('tbl_users',$data);

    }

    function varify_reset_password_code($email_code)
    {
        $this->db->where('forgotten_password_code',$email_code);
        $query=$this->db->get('tbl_users');
        if($query->num_rows()==1)
        {
            return $query->row();
            
        }
    }


function updatePassword($email,$password)
{
    $data['password']=$password;
    $this->db->where('email',$email);
    $query=$this->db->update('tbl_users',$data);
    return true;

}

function removeCode($email)
{
    $data['forgotten_password_code']='';
    $this->db->where('email',$email);
    $query=$this->db->update('tbl_users',$data);

}



}

/* End of file Users_model.php
     * Location: ./application/modules/users/models/users_model.php */    