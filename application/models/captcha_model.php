<?php
    class Captcha_model extends CI_Model {
	function __Construct(){
            parent::__Construct();
            $this->load->helper('captcha');
            $this->load->helper('string');
	}
	
	function create_captcha($txt_class = ''){
            $image_folder = 'captcha_images'; 		
            $random = random_string('alnum', 6);
            $vals = array(
                'word'		 => $random,
                'img_path'	 => "./{$image_folder}/",
                'img_url'	 => base_url() . "{$image_folder}/",
                'font_path'	 => './system/fonts/Adventure Subtitles.ttf',
                'img_width'	 => '145',
                'img_height'     => '65',
                'expiration'      => 100
            );
            $cap = create_captcha($vals);
            $this->session->set_userdata('captcha', $random);
            return '<div class="captcha">' . $cap['image'] . '<br />Case Sensitive<br />' . form_input('captcha', '', 'style="margin-top:5px;" class="' . $txt_class .'"') . '</div>';
	}
	
	function check(){
            if($this->input->post('captcha')){
                if($this->input->post('captcha') == $this->session->userdata('captcha')){
                    return TRUE;
                }
            }
            return FALSE;
	}
}
?>