<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin General_model Model
 * @package Model
 * @subpackage Model
 * Date created: July 19 2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class General_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update($table, $data, $where) {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function getWhere($table, $where, $result = FALSE, $order = '', $limit = '', $start = 0) {
        $ci = & get_instance();
        $ci->db->where($where);
        if ($order != '') {
            $ci->db->order_by($order);
        }
        if ($limit != '') {
            $ci->db->limit($limit, $start);
        }
        $query = $ci->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            if ($result == TRUE) {
                return $query->result();
            } else {
                return $query->row();
            }
        }
    }

    function getAll($table, $where = NULL, $orderBy = NULL, $select = NULL, $limit = NULL, $group_by = NULL) {
        if ($select)
            $this->db->select($select);
        if ($where)
            $this->db->where($where);
        if ($orderBy)
            $this->db->order_by($orderBy);
        if ($limit)
            $this->db->group_by($limit);
        if ($group_by)
            $this->db->group_by($group_by);
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function getAll_Array($table, $where = NULL, $orderBy = NULL, $select = NULL, $limit = NULL, $group_by = NULL) {
        if ($select)
            $this->db->select($select);
        if ($where)
            $this->db->where($where);
        if ($orderBy)
            $this->db->order_by($orderBy);
        if ($limit)
            $this->db->group_by($limit);
        if ($group_by)
            $this->db->group_by($group_by);
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    function getById($table, $fieldId, $id, $array = FALSE, $select = '*') {
        $this->db->select($select);
        $this->db->where($fieldId . " = '" . $id . "'");
        $query = $this->db->get($table);
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            if ($array == TRUE) {
                return $query->row_array();
            } else {
                return $query->row();
            }
        }
    }

    function countTotal($table, $where = NULL) {
        if ($where) {
            $this->db->where($where);
        }
        $this->db->from($table);
        return $this->db->count_all_results();
    }


    /**
     * checks user table for the valid email
     * @return boolean
     */
    function checkEmailExists($email) {
        $prev_email = $this->db->get_where('users', array('email' => $email));
        $num = $prev_email->num_rows();
        if (($num) >= 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }


}

/* End of file General_model.php
 * Location: ./application/modules/admin/models/General_model.php */