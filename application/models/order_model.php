<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Order_Model Model
 * @package Model
 * @subpackage Model
 * Date created: 22 sep  2016
 * @author bkesh maharjan<limited_sky710@yahoo.com>
 */
class Order_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function list_orders($start, $all = FALSE, $user_id = '') {
        if ($all == FALSE) {
            $this->db->where_in('status', array('UNPAID', 'PENDING', 'SUCCESS', 'EXPIRED', 'SHIPPED', 'CANCELLED'));
        }
        if ($user_id != '') {
            $this->db->where('user_id', $user_id);
        }
        $query = $this->db->select('*')
                ->from('checkout')
                ->order_by('id', 'DESC')
                ->limit(ROW_PER_PAGE, $start)
                ->get();
        if ($query->num_rows == 0) {
            return FALSE;
        } else {
            $filtered = $this->filter_orders($query->result());
            return $filtered;
        }
    }

    function filter_orders($orders) {
        $data = array();
        foreach ($orders as $row) {
            $data[$row->site_id][] = $row;
        }
//        dumparray($data);
        return $data;
    }

    function filter_new($id) {
        $this->db->where('id', $id);
        $this->db->update('checkout', array('new_entry' => '0'));
        return TRUE;
    }

    function getOrder($id) {
        $checkout_data = $this->db->get_where('checkout', array('id' => $id))->row();
        $result['checkout_info'] = $checkout_data;
        $product_data = $this->db->get_where('order_product', array('checkout_id' => $id))->result();
        $result['product_info'] = $product_data;
        return $result;
    }

    function get_ordered_products($id) {
        $query = $this->db->select('*,p.id as pid,op.id as opid')
                ->from('order_product as op')
                ->join('products as p', 'p.id=op.product_id')
                ->where('op.checkout_id', $id)
                ->get();
        if ($query->num_rows == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

}

/* End of file order_model.php
 * Location: ./application/modules/admin/models/order_model.php */