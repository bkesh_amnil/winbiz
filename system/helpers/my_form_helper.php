<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * pyrocms
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   pyrocms
 * @author    pyrocms Dev Team
 * @copyright Copyright (c) 2011 - 2012, pyrocms Dev Team
 * @license   http://guides.cipyrocms.com/license.html
 * @link      http://cipyrocms.com
 * @since     Version 1.0
 * @filesource
 */

//--------------------------------------------------------------------

/**
 * Form Helpers
 *
 * Creates HTML5 extensions for the standard CodeIgniter form helper.
 *
 * These functions also wrap the form elements as necessary to create
 * the styling that the Bootstrap-inspired admin theme requires to
 * make it as simple as possible for a developer to maintain styling
 * with the core. Also makes changing the core a snap.
 *
 * All methods (including overridden versions of the originals) now
 * support passing a final 'label' attribute that will create the
 * label along with the field.
 *
 * @package    bootstrap form helper
 * @subpackage Helpers
 * @category   Helpers
 * @author     vianet communication
 * @link       http://vianet.com.np
 *
 */

if ( ! function_exists('_bs_form_common'))
{
	/**
	 * Used by many of the new functions to wrap the input in the correct
	 * tags so that the styling is automatic.
	 *
	 * @access private
	 *
	 * @param string $type    A string with the name of the element type.
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function _bs_form_common($type='text', $data='', $value='', $label='', $extra='', $tooltip = '')
	{
		$defaults = array('type' => $type, 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		// If name is empty at this point, try to grab it from the $data array
		if (empty($defaults['name']) && is_array($data) && isset($data['name']))
		{
			$defaults['name'] = $data['name'];
			unset($data['name']);
		}
		// If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}
		// If label is empty at this point, try to grab it from the $data array
		if (empty($label) && is_array($data) && isset($data['label']))
		{
			$label = $data['label'];
			unset($data['label']);
		}

		// If tooltip is empty at this point, try to grab it from the $data array
		if (empty($tooltip) && is_array($data) && isset($data['tooltip']))
		{
			$tooltip = $data['tooltip'];
			unset($data['tooltip']);
		}

		$error = '';
		//$errorwidth=$spanwidth+3;

		if (function_exists('bs_form_error'))
		{
			if (bs_form_error($defaults['name']))
			{
				$error   = ' has-error';
				$tooltip = '<span class="help-block span4">' . bs_form_error($defaults['name']) . '</span>' . PHP_EOL;
			}
		}

		$output = _parse_form_attributes($data, $defaults);

		$output = <<<EOL

<div class="form-group {$error}">
	<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
	<div class="col-lg-{$spanwidth}">
		 <input class="form-control" {$output} {$extra} />	
		 {$tooltip}	
	</div>
	
	
</div>

EOL;

		return $output;

	}//end _bs_form_common()
}

if ( ! function_exists('bs_form_open')){
	/**
	 * Form Declaration
	 *
	 * Creates the opening portion of the form.
	 *
	 * @param	string	the URI segments of the form destination
	 * @param	array	a key/value pair of attributes
	 * @param	array	a key/value pair hidden data
	 * @return	string
	 */
	function bs_form_open($action = '', $attributes = '', $hidden = array()){
		$CI =& get_instance();
		if ($attributes === ''){
			$attributes = 'method="post"';
		}

		// If an action is not a full URL then turn it into one
		if ($action && strpos($action, '://') === FALSE){
			$action = $CI->config->site_url($action);
		}elseif ( ! $action){
			// If no action is provided then set to the current url
			$action = $CI->config->site_url($CI->uri->uri_string());
		}

		$form = '<form role="form" action="'.$action.'"'._attributes_to_string($attributes, TRUE).">\n";

		// Add CSRF field if enabled, but leave it out for GET requests and requests to external websites
		if ($CI->config->item('csrf_protection') === TRUE && ! (strpos($action, $CI->config->base_url()) === FALSE OR strpos($form, 'method="get"'))){
			$hidden[$CI->security->get_csrf_token_name()] = $CI->security->get_csrf_hash();
		}
		if (is_array($hidden) && count($hidden) > 0){
			$form .= '<div style="display:none;">'.form_hidden($hidden).'</div>';
		}
		return $form;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('bs_form_close')){
	/**
	 * Form Close Tag
	 *
	 * @param	string
	 * @return	string
	 */
	function bs_form_close($extra = ''){
		return '</form>'.$extra;
	}
}


// ------------------------------------------------------------------------

if ( ! function_exists('bs_form_open_multipart')){
	/**
	 * Form Declaration - Multipart type
	 *
	 * Creates the opening portion of the form, but with "multipart/form-data".
	 *
	 * @param	string	the URI segments of the form destination
	 * @param	array	a key/value pair of attributes
	 * @param	array	a key/value pair hidden data
	 * @return	string
	 */
	function bs_form_open_multipart($action = '', $attributes = array(), $hidden = array()){
		if (is_string($attributes)){
			$attributes .= ' enctype="multipart/form-data"';
		}else{
			$attributes['enctype'] = 'multipart/form-data';
		}
		return bs_form_open($action, $attributes, $hidden);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('bs_form_hidden')){
	/**
	 * Hidden Input Field
	 *
	 * Generates hidden fields. You can pass a simple key/value string or
	 * an associative array with multiple values.
	 *
	 * @param	mixed
	 * @param	string
	 * @param	bool
	 * @return	string
	 */
	function bs_form_hidden($name, $value = '', $recursing = FALSE){
		static $form;

		if ($recursing === FALSE){
			$form = "\n";
		}

		if (is_array($name)){
			foreach ($name as $key => $val)	{
				form_hidden($key, $val, TRUE);
			}
			return $form;
		}

		if ( ! is_array($value)){
			$form .= '<input type="hidden" name="'.$name.'" value="'.form_prep($value, $name)."\" />\n";
		}else{
			foreach ($value as $k => $v){
				$k = is_int($k) ? '' : $k;
				form_hidden($name.'['.$k.']', $v, TRUE);
			}
		}
		return $form;
	}
}

// ------------------------------------------------------------------------

/**
 * Textarea field
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('bs_form_textarea'))
{
	function bs_form_textarea($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		$defaults = array('name' => (( ! is_array($data)) ? $data : ''), 'cols' => '40', 'rows' => '10');

		if ( ! is_array($data) OR ! isset($data['value']))
		{
			$val = $value;
		}
		else
		{
			$val = $data['value'];
			unset($data['value']); // textareas don't use the value attribute
		}

		// If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}
		
		if ( ! is_array($data) &&  isset($data['cols'])){
			unset($defaults['cols']);
			$defaults['cols']=$data['cols'];
		}
		if ( ! is_array($data) &&  isset($data['rows'])){
			unset($defaults['rows']);
			$defaults['rows']=$data['rows'];
		}
		
		
		$name = (is_array($data)) ? $data['name'] : $data;
		
		// If label is empty at this point, try to grab it from the $data array
		if (empty($label) && is_array($data) && isset($data['label']))
		{
			$label = $data['label'];
			unset($data['label']);
		}
		
		// If tooltip is empty at this point, try to grab it from the $data array
		if (empty($tooltip) && is_array($data) && isset($data['tooltip']))
		{
			$tooltip = $data['tooltip'];
			unset($data['tooltip']);
		}
		
		$error = '';
		
		if (function_exists('bs_form_error'))
		{
			if (bs_form_error($name))
			{
				$error   = ' has-error';
				$tooltip = '<span class="help-block span4">' . bs_form_error($name) . '</span>' . PHP_EOL;
			}
		}
		$content= form_prep($val, $name);
		$output = _parse_form_attributes($data, $defaults);
		
		$output = <<<EOL
<div class="form-group {$error}">
	<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
	<div class="col-lg-{$spanwidth}">
		 <textarea {$output} {$extra} >{$content}</textarea>
		{$tooltip}
	</div>
</div>				
EOL;
		
				return $output;
		//return "<textarea "._parse_form_attributes($data, $defaults).$extra.">".bs_form_prep($val, $name)."</textarea>";
	}
}
//--------------------------------------------------------------------

// ------------------------------------------------------------------------

/**
 * Editor field
 *
 * @access	public
 * @param	mixed
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('bs_form_editor'))
{
	function bs_form_editor($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		$defaults = array('name' => (( ! is_array($data)) ? $data : ''), 'cols' => '40', 'rows' => '10');

		if ( ! is_array($data) OR ! isset($data['value']))
		{
			$val = $value;
		}
		else
		{
			$val = $data['value'];
			unset($data['value']); // textareas don't use the value attribute
		}
		
		if ( ! is_array($data) &&  isset($data['cols'])){
			unset($defaults['cols']);
			$defaults['cols']=$data['cols'];
		}
		if ( ! is_array($data) &&  isset($data['rows'])){
			unset($defaults['rows']);
			$defaults['rows']=$data['rows'];
		}
		
		// If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}
		
		$name = (is_array($data)) ? $data['name'] : $data;
		
		// If label is empty at this point, try to grab it from the $data array
		if (empty($label) && is_array($data) && isset($data['label']))
		{
			$label = $data['label'];
			unset($data['label']);
		}
		
		// If tooltip is empty at this point, try to grab it from the $data array
		if (empty($tooltip) && is_array($data) && isset($data['tooltip']))
		{
			$tooltip = $data['tooltip'];
			unset($data['tooltip']);
		}
		
		$error = '';
		
		if (function_exists('bs_form_error'))
		{
			if (bs_form_error($name))
			{
				$error   = ' has-error';
				$tooltip = '<span class="help-block span4">' . bs_form_error($name) . '</span>' . PHP_EOL;
			}
		}
		$content=form_prep($val, $name);
		$editortype=form_dropdown('type', array(
							'html' => 'html',
							'markdown' => 'markdown',
							'wysiwyg-simple' => 'wysiwyg-simple',
							'wysiwyg-advanced' => 'wysiwyg-advanced',
						));
		$output = _parse_form_attributes($data, $defaults);
		
		$output = <<<EOL
		<div class="form-group {$error}">
			<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
			<div class="col-lg-{$spanwidth}">
				<div class="editor-choice">{$editortype}</div>
				<div style="clear:both"></div>
				 <textarea {$output} {$extra} >{$content}</textarea>
				{$tooltip}
			</div>
		</div>			
EOL;
		
				return $output;
		//return "<textarea "._parse_form_attributes($data, $defaults).$extra.">".bs_form_prep($val, $name)."</textarea>";
	}
}
//--------------------------------------------------------------------


if ( ! function_exists('bs_form_input'))
{
	/**
	 * Returns a properly templated text input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_input($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('text', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_input()
}


//--------------------------------------------------------------------


if ( ! function_exists('bs_form_checkbox'))
{
    /**
     * Returns a properly templated text input field.
     *
     * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
     * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
     * @param string $label   A string with the label of the element.
     * @param string $extra   A string with any additional items to include, like Javascript.
     * @param string $tooltip A string for inline help or a tooltip icon
     *
     * @return string A string with the formatted input element, label tag and wrapping divs.
     */
    function bs_form_checkbox($data='', $value='', $checked = FALSE, $label='', $extra='', $tooltip = '')
    {
        $defaults = array('type' => 'checkbox', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

        // If name is empty at this point, try to grab it from the $data array
        if (empty($defaults['name']) && is_array($data) && isset($data['name']))
        {
            $defaults['name'] = $data['name'];
            unset($data['name']);
        }
    
		// If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}

        // If label is empty at this point, try to grab it from the $data array
        if (empty($label) && is_array($data) && isset($data['label']))
        {
            $label = $data['label'];
            unset($data['label']);
        }

        // If tooltip is empty at this point, try to grab it from the $data array
        if (empty($tooltip) && is_array($data) && isset($data['tooltip']))
        {
            $tooltip = $data['tooltip'];
            unset($data['tooltip']);
        }

        if ($checked)
        {
            $defaults['checked'] = 'checked';
        }

        $error = '';

        if (function_exists('bs_form_error'))
        {
            if (bs_form_error($defaults['name']))
            {
                $error   = ' has-error';
                $tooltip = '<span class="help-block span4">' . bs_form_error($defaults['name']) . '</span>' . PHP_EOL;
            }
        }

        $output = _parse_form_attributes($data, $defaults);

        $output = <<<EOL

<div class="form-group {$error}">
	<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
	<div class="col-lg-{$spanwidth}">
		 <input class="form-control" {$output} {$extra} />
		{$tooltip}
	</div>
</div>
EOL;

        return $output;

    }//end bs_form_checkbox()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_email'))
{
	/**
	 * Returns a properly templated email input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_email($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('email', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_email()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_password'))
{
	/**
	 * Returns a properly templated password input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_password($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('password', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_password()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_url'))
{
	/**
	 * Returns a properly templated URL input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_url($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('url', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_url()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_telephone'))
{
	/**
	 * Returns a properly templated Telephone input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string withthe label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_telephone($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('tel', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_telephone()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_number'))
{
	/**
	 * Returns a properly templated number input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_number($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('number', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_number()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_color'))
{
	/**
	 * Returns a properly templated color input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_color($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('color', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_color()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_search'))
{
	/**
	 * Returns a properly templated search input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_search($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('search', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_search()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_date'))
{
	/**
	 * Returns a properly templated date input field.
	 *
	 * @param string $data    Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param string $value   Either a string with the value, or blank if an array is passed to the $data param.
	 * @param string $label   A string with the label of the element.
	 * @param string $extra   A string with any additional items to include, like Javascript.
	 * @param string $tooltip A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_date($data='', $value='', $label='', $extra='', $tooltip = '')
	{
		return _bs_form_common('date', $data, $value, $label, $extra, $tooltip);

	}//end bs_form_date()
}

//--------------------------------------------------------------------

if ( ! function_exists('bs_form_dropdown'))
{
	/**
	 * Returns a properly templated date dropdown field.
	 *
	 * @param string $data     Either a string with the element name, or an array of key/value pairs of all attributes.
	 * @param array  $options  Array of options for the drop down list
	 * @param string $selected Either a string of the selected item or an array of selected items
	 * @param string $label    A string with the label of the element.
	 * @param string $extra    A string with any additional items to include, like Javascript.
	 * @param string $tooltip  A string for inline help or a tooltip icon
	 *
	 * @return string A string with the formatted input element, label tag and wrapping divs.
	 */
	function bs_form_dropdown($data, $options=array(), $selected='', $label='', $extra='', $tooltip = '')
	{
		$defaults = array('name' => (( ! is_array($data)) ? $data : ''));

		// If name is empty at this point, try to grab it from the $data array
		if (empty($defaults['name']) && is_array($data) && isset($data['name']))
		{
			$defaults['name'] = $data['name'];
			unset($data['name']);
		}
		// If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}

		$output = _parse_form_attributes($data, $defaults);

		if ( ! is_array($selected))
		{
			$selected = array($selected);
		}

		// If no selected state was submitted we will attempt to set it automatically
		if (count($selected) === 0)
		{
			// If the form name appears in the $_POST array we have a winner!
			if (isset($_POST[$data['name']]))
			{
				$selected = array($_POST[$data['name']]);
			}
		}

		$options_vals = '';
		$count=0;
		if(!empty($options)){
			if(is_object($options[0])){
				
				$optionarr=array();
				foreach($options as $val){
					$optionarr[$val->value]=$val->text;
				}
				$options=$optionarr;
				unset($optionarr);
			}
		}else{
			$options=array();
		}
		
		foreach ($options as $key => $val)
		{
			$count++;
			$key = (string) $key;

			if (is_array($val) && ! empty($val))
			{
				if($count==1){
					$options_vals .= '<option></option>'.PHP_EOL;
					$options_vals .= '<optgroup label="'.$key.'">';
				}else{
					$options_vals .= '<optgroup label="'.$key.'">'.PHP_EOL;
				}

				foreach ($val as $optgroup_key => $optgroup_val)
				{
					$sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

					$options_vals .= '<option value="'.$optgroup_key.'"'.$sel.'>'.(string) $optgroup_val."</option>\n";
				}

				$options_vals .= '</optgroup>'.PHP_EOL;
			}
			else
			{
				$sel = (in_array($key, $selected)) ? ' selected="selected"' : '';

				$options_vals .= '<option value="'.$key.'"'.$sel.'>'.(string) $val."</option>\n";
			}
		}

		$error = '';

		if (function_exists('bs_form_error'))
		{
			if (bs_form_error($defaults['name']))
			{
				$error   = ' has-error';
				$tooltip = '<span class="help-block span4">' . bs_form_error($defaults['name']) . '</span>' . PHP_EOL;
			}
		}

		$output = <<<EOL
<div class="form-group {$error}">
	<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
	<div class="col-lg-{$spanwidth}">
		 <select {$output} {$extra}>
			{$options_vals}
		</select>
		{$tooltip}
	</div>
</div>

EOL;

		return $output;

	}//end bs_form_dropdown()

	if(! function_exists('bs_form_upload')){
		function bs_form_upload($data='', $value='', $label='', $extra='', $tooltip = ''){
			 $defaults = array('type' => 'file', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

        // If name is empty at this point, try to grab it from the $data array
        if (empty($defaults['name']) && is_array($data) && isset($data['name']))
        {
            $defaults['name'] = $data['name'];
            unset($data['name']);
        }

        if (is_array($data) && isset($data['btnclass']))
        {
            $btnclass = $data['btnclass'];
            unset($data['btnclass']);
        }else{
        	$btnclass='default';
        }

        // If width is empty at this point, try to grab it from the $data array
		if (is_array($data) && isset($data['width']))
		{
			$spanwidth = $data['width'];
			unset($data['width']);
		}else{
			$spanwidth='9';
		}

        // If label is empty at this point, try to grab it from the $data array
        if (empty($label) && is_array($data) && isset($data['label']))
        {
            $label = $data['label'];
            unset($data['label']);
        }

        // If tooltip is empty at this point, try to grab it from the $data array
        if (empty($tooltip) && is_array($data) && isset($data['tooltip']))
        {
            $tooltip = $data['tooltip'];
            unset($data['tooltip']);
        }

        $error = '';

        if (function_exists('bs_form_error')){
            if (bs_form_error($defaults['name'])){
                $error   = ' has-error';
                $tooltip = '<span class="help-inline">' . bs_form_error($defaults['name']) . '</span>' . PHP_EOL;
            }
        }

        $output = _parse_form_attributes($data, $defaults);

        $output = <<<EOL
<div class="form-group {$error}">
	<label class="col-lg-3 control-label" for="{$defaults['name']}">{$label} :</label>
	<div class="col-lg-{$spanwidth}">
		<a class='btn btn-{$btnclass}' href='javascript:;'>
			Choose File...
			<input type="file" class="form-control" {$output} {$extra} style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-{$btnclass}' id="upload-file-info"></span>
		{$tooltip}
	</div>
</div>
EOL;

        return $output;
		}
	}
	
	if ( ! function_exists('bs_panel_open')){
		function bs_panel_open($legend=null, $panelclass="panel-default"){
			$output= '<div class="panel '.$panelclass.'">'.
						'<div class="panel-heading">'.$legend.'</div>'.
						'<div class="panel-body">';
									
			return $output;
		}
	}
	if ( ! function_exists('bs_panel_close')){
		function bs_panel_close(){
	
			$output= '</div>'.
					'</div>';
				
			return $output;
		}
	}

	// ------------------------------------------------------------------------


	if ( ! function_exists('bs_form_error')){
		/**
		 * Form Error
		 *
		 * Returns the error for a specific form field. This is a helper for the
		 * form validation class.
		 *
		 * @param	string
		 * @param	string
		 * @param	string
		 * @return	string
		 */
		function bs_form_error($field = '', $prefix = '', $suffix = ''){
			if (FALSE === ($OBJ =& _get_validation_object())){
				return '';
			}
			return $OBJ->error($field, $prefix, $suffix);
		}
	}
}
